//
//  ChangePassword.m
//  QuickLock
//
//  Created by PUNDSK001 on 26/12/15.
//  Copyright © 2015 Bge. All rights reserved.
//

#import "Zwave.h"

@interface Zwave ()
{
    UIAlertView *alert;
    IBOutlet UILabel *lblTitle;
}

@end

@implementation Zwave

- (void)viewDidLoad
{
    [super viewDidLoad];
    

}
- (IBAction)btnBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}

- (IBAction)btnAddConnectpanel:(UIButton *)sender
{
    [self.view makeToast:@"Z-Wave Connect"];
    [quickLockDeviceManage setZwave:(int)sender.tag];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.navigationController popViewControllerAnimated:NO];
    });
}

- (IBAction)btnDisconnectFromPanel:(UIButton *)sender
{
    [self.view makeToast:@"Z-Wave Disconnect"];
    [quickLockDeviceManage setZwave:(int)sender.tag];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.navigationController popViewControllerAnimated:NO];
    });
}

- (IBAction)btnFactoryReset:(UIButton *)sender
{
    alert = [[UIAlertView alloc]initWithTitle:@"Warning!" message:@"You sure you want to Factory reset\nZ-Wave?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
    alert.tag = sender.tag;
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        [self factoryReset:(int)alertView.tag];
    }
}

-(void)factoryReset:(int)sender
{
    [self.view makeToast:@"Z-Wave Factory Reset"];
    [quickLockDeviceManage setZwave:sender];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.navigationController popViewControllerAnimated:NO];
    });
}

-(void)viewDidDisappear:(BOOL)animated
{
    [alert dismissWithClickedButtonIndex:0 animated:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
