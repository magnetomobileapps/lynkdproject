//
//  LockClassCell.h
//  QuickLock
//
//  Created by PUNDSK001 on 23/04/16.
//  Copyright © 2016 Bge. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LockClassCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIButton *btnDelete;
@property (strong, nonatomic) IBOutlet UIButton *btnShareCode;
@property (strong, nonatomic) IBOutlet UILabel *lblName;

@end
