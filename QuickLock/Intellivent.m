//
//  Intellivent.m
//  QuickLock
//
//  Created by PUNDSK001 on 04/01/16.
//  Copyright © 2016 Bge. All rights reserved.
//

#import "Intellivent.h"
#import "MyIntellivents.h"
#import "IntelliventSettings.h"

@interface Intellivent () <SlideNavigationControllerDelegate>

// Properties
@property (weak, nonatomic) IBOutlet UIButton *btnMyVents;
@property (weak, nonatomic) IBOutlet UIButton *btnAddNew;
@property (weak, nonatomic) IBOutlet UIButton *btnSetting;

@end

@implementation Intellivent

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self ShowAPPVersion];

    // Assign drawer menu
    [SlideNavigationController sharedInstance].portraitSlideOffset = appDelegate.portraitDrawer;
    UIButton *btnMenu = [UIButton buttonWithType:UIButtonTypeCustom];
    btnMenu.frame = CGRectMake(10, 28, 35, 35);
    [btnMenu setImage:[UIImage imageNamed:@"navigation.png"] forState:UIControlStateNormal];
    [btnMenu addTarget:[SlideNavigationController sharedInstance] action:@selector(toggleLeftMenu) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnMenu];

    if (screenSize.height == 480)
    {
        _constantLogoBottom.constant = 20;
    }
    
    _btnMyVents.layer.borderWidth = _btnAddNew.layer.borderWidth = 3;
    _btnMyVents.layer.masksToBounds = _btnAddNew.layer.masksToBounds = YES;
    _btnMyVents.layer.borderColor = _btnAddNew.layer.borderColor = darkgray.CGColor;
    _btnMyVents.layer.cornerRadius = _btnAddNew.layer.cornerRadius = _btnMyVents.frame.size.height/2;
    
    // set title of buttons due to multiline
    [_btnMyVents setTitle:@"MY\nVENTS" forState:UIControlStateNormal];
    [_btnAddNew setTitle:@"ADD\nNEW" forState:UIControlStateNormal];
    _btnMyVents.titleLabel.lineBreakMode = _btnAddNew.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    _btnMyVents.titleLabel.numberOfLines = _btnAddNew.titleLabel.numberOfLines = 2;
    _btnMyVents.titleLabel.textAlignment = _btnAddNew.titleLabel.textAlignment = NSTextAlignmentCenter;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showBattery:) name:@"kBattery" object:nil];

    if (quickLockDeviceManage.lockDevice.is_Login_share_Code == 1)
    {
        _btnSetting.hidden = YES;
    }
}

- (IBAction)btnHome:(id)sender
{
    [quickLockDeviceManage initiativeDisConnect];
    [self.navigationController popViewControllerAnimated:NO];
}


// It will slide screen to go to left menu
- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
}

- (void)showBattery:(NSNotification *)notification
{
    int batteryValue = [[notification object] intValue];
    [labelBattery setText:[NSString stringWithFormat:@"Battery : %d%%", batteryValue]];
}

// Get APP Version and assign to Label
- (void)ShowAPPVersion
{
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *APPVersion =[NSString stringWithFormat:@"%@.%@",
                           [infoDictionary objectForKey:@"CFBundleShortVersionString"],
                           [infoDictionary objectForKey:@"CFBundleVersion"]];
    [_labelAPPVersion setText:[NSString stringWithFormat:@"APP Version : %@", APPVersion]];
}

// Navigate to list of MyIntellivents
- (IBAction)btnMyVents:(id)sender
{
    MyIntellivents *myIntellivents = [self.storyboard instantiateViewControllerWithIdentifier:@"MyIntellivents"];
    [self.navigationController pushViewController:myIntellivents animated:NO];
}

// button to add new intellivent
- (IBAction)btnAddNew:(id)sender
{
    NSLog(@"add new");
}

- (IBAction)btnSettings:(id)sender
{
    IntelliventSettings *intelliventSettings = [self.storyboard instantiateViewControllerWithIdentifier:@"IntelliventSettings"];
    [self.navigationController pushViewController:intelliventSettings animated:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
