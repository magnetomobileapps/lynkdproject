//
//  AppDelegate.h
//  QuickLock
//
//  Created by administrator on 14-12-26.
//  Copyright (c) 2014年 Bge. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideNavigationController.h"
#import "LeftMenuViewController.h"
#import <UserNotifications/UserNotifications.h>


@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
}
@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, retain) NSString *strSelectedIndex;
@property (nonatomic, retain) NSMutableArray *arraySubTitle, *arrLockName, *arrPassword, *arrShareCode;
@property (nonatomic) CGFloat portraitDrawer;
// device Address
@property (nonatomic, retain) NSString *lock_Address,*TAG_IMEI, *strSelectedPush, *strSelectedAlertDevice;
@property (nonatomic, assign) int selectedMenu;
@property (strong, nonatomic) NSString *strDeviceId, *strDeviceToken;

@end

