//
//  MyLocksViewController.m
//  QuickLock
//
//  Created by administrator on 15-3-6.
//  Copyright (c) 2015年 Bge. All rights reserved.
//

#import "MyLocksViewController.h"
#import "LockClassCell.h"

#define kHeightForCellAndHeaderView 44

@interface MyLocksViewController ()
{
}

@end

@implementation MyLocksViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Initialize Menu and Menu button
    [SlideNavigationController sharedInstance].portraitSlideOffset = appDelegate.portraitDrawer;
    
    _lblTitle.textColor = titleorange;
    
    UIButton *btnMenu = [UIButton buttonWithType:UIButtonTypeCustom];
    btnMenu.frame = CGRectMake(10, 28, 35, 35);
    [btnMenu setImage:[UIImage imageNamed:@"navigation.png"] forState:UIControlStateNormal];
    [btnMenu addTarget:[SlideNavigationController sharedInstance] action:@selector(toggleLeftMenu) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnMenu];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(btnCancelPasscode:)];
    [_viewBg addGestureRecognizer:tap];
    
    _viewLock.layer.masksToBounds = _btnClose.layer.masksToBounds = YES;
    _viewLock.layer.borderWidth = 4;
    _viewLock.layer.borderColor = darkgray.CGColor;
    _viewLock.layer.cornerRadius = 8;
    _btnClose.layer.cornerRadius = 8;

    _viewPasscode.layer.masksToBounds = YES;
    _viewPasscode.layer.borderColor = darkgray.CGColor;
    _viewPasscode.layer.borderWidth = 3;
    _viewPasscode.layer.cornerRadius = 6;
    [self createInputAccessoryView];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setDataOfSelectedLock) name:@"setDataOfSelectedLock" object:nil];
    _txtPasscode.backgroundColor = lightgray;
    
    isSharecodeNotShown = YES;
}

- (void)viewDidDisappear:(BOOL)animated
{
    [alert dismissWithClickedButtonIndex:0 animated:NO];
    [alertDeleteDevice dismissWithClickedButtonIndex:0 animated:NO];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    //if (screenSize.height <= 568)
    {
        [UIView animateWithDuration:0.3f animations:^{
            _viewPasscode.transform = CGAffineTransformMakeTranslation(0, -80);
        }];
    }
}

-(void)createInputAccessoryView
{
    keyboardToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 44)];
    keyboardToolbar.tintColor = [UIColor darkGrayColor];
    keyboardToolbar.backgroundColor = [UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:1.0];
    
    btnFlexible = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    btnDone = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneTyping)];
    
    NSArray *arr = @[btnFlexible,btnDone];
    [keyboardToolbar setItems:arr];
    
    [self.txtPasscode setInputAccessoryView:keyboardToolbar];
}

-(void)doneTyping
{
    [UIView animateWithDuration:0.3f animations:^{
        _viewPasscode.transform = CGAffineTransformMakeTranslation(0, 0);
    }];
    [_txtPasscode resignFirstResponder];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden = YES;
}

-(void)setDataOfSelectedLock
{
    AppDelegate *appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSLog(@"%d",appD.selectedMenu);
    
    switch (appD.selectedMenu)
    {
        case PADLOCK:
            _lblTitle.text = @"My PadLocks";
            _lblNoFund.text = @"No Padlock Data Found";
            break;
        case DOORLOCK:
            _lblTitle.text = @"My DoorLocks";
            _lblNoFund.text = @"No DoorLock Data Found";
            break;
        case GUNBOX:
            _lblTitle.text = @"My GunBoxes";
            _lblNoFund.text = @"No GunBox Data Found";
            break;
        case QUICKBOX:
            _lblTitle.text = @"My GunBox Echos";
            _lblNoFund.text = @"No GunBox Echo Data Found";
            break;
        case MEDICINEBOX:
            _lblTitle.text = @"My MedicineBoxes";
            _lblNoFund.text = @"No MedicineBox Data Found";
            break;
        case TALLGUNBOX:
            _lblTitle.text = @"My GunBox SK-1";
            _lblNoFund.text = @"No GunBox SK-1 Data Found";
            break;
        case INTELLIVENT:
            _lblTitle.text = @"My Intellivents";
            _lblNoFund.text = @"No Intellivent Data Found";
            break;
        case DEADBOLT:
            _lblTitle.text = @"My DeadBolts";
            _lblNoFund.text = @"No DeadBolt Data Found";
            break;
            
        default:
            break;
    }
    
    arrLock = [NSMutableArray new];
    arrayLock = [dbAccess GetAllLockDevice];
    
    for (int i = 0; i < arrayLock.count; i++)
    {
        // store to array only those devices which are PadLocks
        LockDevice *lockDevice = arrayLock[i];
        if (lockDevice.lock_Type == appD.selectedMenu)
        {
            [arrLock addObject:arrayLock[i]];
        }
    }
    arrayLock = nil;
    // if there is no Lock available then show Label of No data found
    if (arrLock.count == 0)
    {
        _lblNoFund.hidden = NO;
        tableViewLock.hidden = YES;
    }
    else
    {
        _lblNoFund.hidden = YES;
        tableViewLock.hidden = NO;
    }
    
    [tableViewLock reloadData];
}

- (NSInteger)indexFromPixels:(NSInteger)pixels
{
    return 0;
}

- (IBAction)Back:(id)sender
{
    [quickLockDeviceManage stopSearch];
    [self.navigationController popViewControllerAnimated:NO];
}

// It will slide screen to go to left menu
- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
//    return isSharecodeNotShown;
    return YES;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrLock.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cell";
    
    LockClassCell *cell1 = [tableViewLock dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell1)
    {
        cell1 = [[LockClassCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
//    cell1.backgroundColor = [UIColor whiteColor];
    
    // get data from array to lockDevice object which is passed to the cell's labels
    LockDevice *lockDevice = arrLock[indexPath.row];
    
//    UIButton *btnDelete = [UIButton buttonWithType:UIButtonTypeSystem];
//    btnDelete.frame = CGRectMake(screenSize.width-47, -2, 40, 40);
//    [btnDelete setBackgroundImage:[UIImage imageNamed:@"minus-padding.png"] forState:UIControlStateNormal];
    cell1.btnDelete.tag = indexPath.row;
    [cell1.btnDelete addTarget:self action:@selector(btnDeleteLock:) forControlEvents:UIControlEventTouchUpInside];
//    [cell1.contentView addSubview:cell1.btnDelete];
    
    // Till no Share code is available in the lock thatswhy lblShareCode has been put hard coded
//    UIButton *btnShareCode = [UIButton buttonWithType:UIButtonTypeSystem];
//    btnShareCode.frame = CGRectMake(screenSize.width-131, 1, 78, 34);
//    [btnShareCode setTitle:@"Show" forState:UIControlStateNormal];
//    btnShareCode.titleLabel.font = [UIFont systemFontOfSize:14];
//    [lblShareCOde setFont:[UIFont systemFontOfSize:14]];
//    [btnShareCode setTitleColor:darkgray forState:UIControlStateNormal];
    cell1.btnShareCode.layer.masksToBounds = YES;
    cell1.btnShareCode.layer.cornerRadius = 8;
    cell1.btnShareCode.tag = indexPath.row;
    [cell1.btnShareCode addTarget:self action:@selector(btnShareCode:) forControlEvents:UIControlEventTouchUpInside];
//    [cell1.contentView addSubview:btnShareCode];
    
//    UILabel *lblName = [[UILabel alloc]initWithFrame:CGRectMake(4 , 0, btnShareCode.frame.origin.x-10, 36)];
    cell1.lblName.text = [NSString stringWithFormat:@"  %@",lockDevice.lock_Name];
//    [cell1.contentView addSubview:lblName];
//    lblName.font = [UIFont systemFontOfSize:14];
//    btnShareCode.backgroundColor = btnShareCode.backgroundColor = lblName.backgroundColor = cellBgColor;
//    lblName.textColor = darkgray;
    
    // To corner radous of labels from only one side. For 0th row and for arr.count row using UIBezierPath
    if (arrLock.count == 1)
    {
        cell1.btnShareCode.layer.masksToBounds = cell1.btnShareCode.layer.masksToBounds = cell1.lblName.layer.masksToBounds = YES;
        cell1.btnShareCode.layer.cornerRadius = cell1.btnShareCode.layer.cornerRadius = cell1.lblName.layer.cornerRadius = 8;
    }
    else
    {
        UIBezierPath *maskPath;
        if (indexPath.row == 0)
        {
            // For Upper Row
            maskPath = [UIBezierPath bezierPathWithRoundedRect:cell1.lblName.bounds
                                             byRoundingCorners:(UIRectCornerTopLeft|UIRectCornerTopRight)
                                                   cornerRadii:CGSizeMake(8, 8)];
            CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
            maskLayer.frame = cell1.lblName.bounds;
            maskLayer.path = maskPath.CGPath;
            cell1.lblName.layer.mask = maskLayer;
        }
        else if(indexPath.row == arrLock.count-1)
        {
            // For Lowest Row
            maskPath = [UIBezierPath bezierPathWithRoundedRect:cell1.lblName.bounds
                                             byRoundingCorners:(UIRectCornerBottomLeft|UIRectCornerBottomRight)
                                                   cornerRadii:CGSizeMake(8, 8)];
            CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
            maskLayer.frame = cell1.lblName.bounds;
            maskLayer.path = maskPath.CGPath;
            cell1.lblName.layer.mask = maskLayer;
        }
    }
    return cell1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
        [tableViewLock deselectRowAtIndexPath:indexPath animated:YES];
        
        LockDevice *lockDevice = arrLock[indexPath.row];
        [quickLockDeviceManage startSearchWithUUID:lockDevice.lock_UUID];
}

-(void)btnDeleteLock:(UIButton *)sender
{
    [self DeleteDevice:sender];
    [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"passwordCall"];
}

- (void)DeleteDevice:(UIButton *)sender
{
    alertDeleteDevice = [[UIAlertView alloc] initWithTitle:@"Warning!"
                                                   message:@"You sure you want to delete it?"
                                                  delegate:self
                                         cancelButtonTitle:@"Cancel"
                                         otherButtonTitles:@"OK", nil];
    alertDeleteDevice.tag = sender.tag;
    [alertDeleteDevice show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView == alertDeleteDevice)
    {
        if (buttonIndex == 1)
        {
            LockDevice *lockDevice = arrLock[alertView.tag];
            [dbAccess DeleteLockDeviceByUUID:lockDevice.lock_UUID];
            [quickLockDeviceManage initiativeDisConnect];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"setDataOfSelectedLock" object:nil];
        }
    }
}

-(void)btnShareCode:(UIButton *)sender
{
    LockDevice *lockDevice = arrLock[sender.tag];
    
    if (lockDevice.is_Login_share_Code == 1)
    {
        alert = [[UIAlertView alloc]initWithTitle:@"Worning!" message:@"You are logged in with share code" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
        isSharecodeNotShown = NO;
        
        _lblLockName.text = lockDevice.lock_Name;
        _lblPassword.text = lockDevice.lock_Password;
        
        if ([lockDevice.share_Code isEqualToString:@"FFFFFFFF"] || [lockDevice.share_Code isEqualToString:@""] || lockDevice.share_Code == nil)
        {
            //        _lblShareCodeUser.text = [NSString stringWithFormat:@"(User Count) \nShare Code : Not set"];
        }
        else
        {
            _lblUserCountShareCode.text = lockDevice.share_Code;
            _lblUserCountRemaining.text = lockDevice.share_Code_Count;
        }
        
        if ([lockDevice.share_Code_Time isEqualToString:@"FFFFFFFF"] || [lockDevice.share_Code_Time isEqualToString:@""] || lockDevice.share_Code_Time == nil)
        {
            //        _lblShareCodeTime.text = [NSString stringWithFormat:@"(Time Limit) \nShare Code : %@",@"Not set"];
        }
        else
        {
            switch (lockDevice.share_code_time_flag)
            {
                case 0: // 0 only share code set
                    _lblTimeLimitShareCode.text = lockDevice.share_Code_Time;
                    break;
                    
                case 1: // 1 date set
                    _lblTimeLimitShareCode.text = lockDevice.share_Code_Time;
                    _lblTimeLimitStartDate.text = lockDevice.str_share_Code_Stert_time;
                    _lblTimeLimitEndDate.text = lockDevice.str_share_Code_End_time;
                    break;
                    
                case 2: // 2 time set
                    _lblTimeLimitShareCode.text = lockDevice.share_Code_Time;
                    _lblTimeLimitRange.text = [NSString stringWithFormat:@"%@ - %@",lockDevice.share_Code_Time1,lockDevice.share_Code_Time2];
                    break;
                    
                case 3: // 3 time and date set
                    _lblTimeLimitShareCode.text = lockDevice.share_Code_Time;
                    _lblTimeLimitStartDate.text = lockDevice.str_share_Code_Stert_time;
                    _lblTimeLimitEndDate.text = lockDevice.str_share_Code_End_time;
                    _lblTimeLimitRange.text = [NSString stringWithFormat:@"%@ - %@",lockDevice.share_Code_Time1,lockDevice.share_Code_Time2];
                    break;
                    
                default:
                    break;
            }
        }
        
        if (lockDevice.passcode_enable == 1)
        {
            _viewPasscode.hidden = NO;
            _viewBg.hidden = NO;
            [UIView animateWithDuration:0.3 animations:^{
                _viewBg.alpha = 0.6;
                _viewPasscode.alpha = 1.0;
                strPasscode = lockDevice.passcode;
            } completion:^(BOOL finished) {
                [_txtPasscode becomeFirstResponder];
            }];
        }
        else
        {
            _viewLock.hidden = NO;
            _viewBg.hidden = NO;
            [UIView animateWithDuration:0.3 animations:^{
                _viewBg.alpha = 0.6;
                _viewLock.alpha = 1.0;
            }];
        }
    }
}

//// text validation and limit
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    // validation for telephone
    if (textField == _txtPasscode)
    {
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength <= 4);
    }
    return YES;
}

- (IBAction)btnOkPasscode:(id)sender
{
    if (_txtPasscode.text.length == 0)
    {
        [self.view makeToast:@"Enter password" duration:1.0 position:@"center"];
        _txtPasscode.text = @"";
    }
    else if ([_txtPasscode.text isEqualToString:strPasscode])
    {
        [UIView animateWithDuration:0.3 animations:^{
            _viewBg.alpha = 0.0;
            _viewPasscode.alpha = 0.0;
            strPasscode = @"";
            _viewPasscode.transform = CGAffineTransformMakeTranslation(0, 0);

        } completion:^(BOOL finished) {
            _viewPasscode.hidden = YES;
            _viewBg.hidden = YES;
            _txtPasscode.text = @"";

//            ShowShareCode *shareCode = [self.storyboard instantiateViewControllerWithIdentifier:@"ShowShareCode"];
//            [self.navigationController pushViewController:shareCode animated:NO];

//            alert = [[UIAlertView alloc]initWithTitle:strSelectedName message:[NSString stringWithFormat:@"\n%@\n\n%@\n\n%@",strMsgPassword,strMsgShareCodeCount,strMsgShareCodeTime] delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil, nil];
//            [alert show];
            _viewLock.hidden = NO;
            _viewBg.hidden = NO;
            [UIView animateWithDuration:0.3 animations:^{
                _viewBg.alpha = 0.6;
                _viewLock.alpha = 1.0;
            }];
            [_txtPasscode resignFirstResponder];
        }];
    }
    else
    {
        [self.view makeToast:@"Wrong password" duration:1.0 position:@"center"];
        _txtPasscode.text = @"";
    }
    isSharecodeNotShown = YES;
}

- (IBAction)btnCancelPasscode:(id)sender
{
    [UIView animateWithDuration:0.3 animations:^{
        _viewBg.alpha = 0.0;
        _viewPasscode.alpha = 0.0;
        strPasscode = @"";
        _viewLock.alpha = 0.0;
        _viewPasscode.transform = CGAffineTransformMakeTranslation(0, 0);

    } completion:^(BOOL finished) {
        _viewPasscode.hidden = YES;
        _viewBg.hidden = YES;
        [_txtPasscode resignFirstResponder];
        _txtPasscode.text = @"";
        _viewLock.hidden = YES;

    }];
    isSharecodeNotShown = YES;
}

-(NSString *)minutesSinceMidnight:(NSDate *)date
{
    NSCalendar *gregorian = [[NSCalendar alloc]
                             initWithCalendarIdentifier:NSGregorianCalendar];
    unsigned unitFlags =  NSHourCalendarUnit | NSMinuteCalendarUnit;
    NSDateComponents *components = [gregorian components:unitFlags fromDate:date];
    
    return [NSString stringWithFormat:@"%02d:%02d",(int)[components hour],(int)[components minute]];
}

- (IBAction)btnClose:(id)sender
{
    [UIView animateWithDuration:0.3 animations:^{
        _viewBg.alpha = 0.0;
        _viewLock.alpha = 0.0;
    } completion:^(BOOL finished) {
        _viewLock.hidden = YES;
        _viewBg.hidden = YES;
        isSharecodeNotShown = YES;
    }];
}

// Code to adjust line of UITableViewCell from left side
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath*)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)])
    {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    if ([tableViewLock respondsToSelector:@selector(setSeparatorInset:)])
    {
        [tableViewLock setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableViewLock respondsToSelector:@selector(setLayoutMargins:)])
    {
        [tableViewLock setLayoutMargins:UIEdgeInsetsZero];
    }
}

@end
