//
//  BaseDrawView.h
//  QuickLock
//
//  Created by Adam Hathaway on 12/3/15.
//  Copyright © 2015 Bge. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseDrawView : UIView

@property (strong, nonatomic) UIColor* fillColor;
@property (strong, nonatomic) UIColor* fillColor2;
@property (strong, nonatomic) UIColor* strokeColor;
@property (strong, nonatomic) UIColor* textColor;

@end
