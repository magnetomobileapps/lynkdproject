//
//  SetupProfileController.m
//  QuickLock
//
//  Created by Pavan Jadhav on 14/09/17.
//  Copyright © 2017 Bge. All rights reserved.
//

#import "SetupProfileController.h"
#import "TermCondition.h"

@interface SetupProfileController ()<UITextFieldDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@end

@implementation SetupProfileController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
-(void)viewDidLayoutSubviews{
    imgProfile.layer.cornerRadius = imgProfile.frame.size.width/2.0;
    imgProfile.layer.masksToBounds = YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnSubmit:(id)sender {
}

- (IBAction)btnAcceptOurTerms:(id)sender {
    TermCondition *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TermCondition"];
    [self.navigationController pushViewController:viewController animated:NO];
}

- (IBAction)btnRadioSelect:(id)sender {
}

- (IBAction)btnEditProfile:(id)sender {
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Take photo from"
                                 message:@""
                                 preferredStyle:UIAlertControllerStyleActionSheet];
    
    //Add Buttons
    
    UIAlertAction* cameraButton = [UIAlertAction
                                actionWithTitle:@"Camera"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    //Handle your yes please button action here
                                    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                                    picker.delegate = self;
                                    picker.allowsEditing = YES;
                                    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                                    
                                    [self presentViewController:picker animated:YES completion:NULL];
                                }];
    
    UIAlertAction* galleryButton = [UIAlertAction
                               actionWithTitle:@"Gallery"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //Handle no, thanks button
                                   UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                                   picker.delegate = self;
                                   picker.allowsEditing = YES;
                                   picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                                   
                                   [self presentViewController:picker animated:YES completion:NULL];
                               }];
    
    UIAlertAction* cancelButton = [UIAlertAction
                                    actionWithTitle:@"Cancel"
                                    style:UIAlertActionStyleCancel
                                    handler:^(UIAlertAction * action) {
                                        //Handle no, thanks button
                                    }];

    //Add your buttons to alert controller
    
    [alert addAction:cameraButton];
    [alert addAction:galleryButton];
    [alert addAction:cancelButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    imgProfile.image = chosenImage;
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField == txtUserName) {
        [txtFirstName becomeFirstResponder];
    } else if (textField == txtFirstName) {
        [txtLastName becomeFirstResponder];
    }
    else if (textField == txtLastName) {
        [txtAccountName becomeFirstResponder];
    }
    else if (textField == txtAccountName) {
        [txtEmail becomeFirstResponder];
    }
    else if (textField == txtEmail) {
        [txtPassword becomeFirstResponder];
    }
    else if (textField == txtPassword) {
        [txtConfirmPassword becomeFirstResponder];
    }
    else if (textField == txtConfirmPassword){
        [txtConfirmPassword resignFirstResponder];
    }
    return YES;
}

@end
