//
//  HamburgerDrawView.m
//  QuickLock
//
//  Created by Adam Hathaway on 12/3/15.
//  Copyright © 2015 Bge. All rights reserved.
//

#import "HamburgerDrawView.h"
#import "STStyleKit.h"

@implementation HamburgerDrawView

- (void)drawRect:(CGRect)rect {
    [STStyleKit drawHamburgerWithFrame:rect fillColor:self.fillColor];
}

@end
