//
//  ChangePassword.h
//  QuickLock
//
//  Created by PUNDSK001 on 26/12/15.
//  Copyright © 2015 Bge. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ChangePassword;
@protocol ChangePasswordDelegate <NSObject>

- (void)returnPassword:(NSString *)password;
- (void)cancelModifyPassword;

@end

@interface ChangePassword : UIViewController <UITextFieldDelegate>

@property (nonatomic, assign) id<ChangePasswordDelegate> delegate;

@end
