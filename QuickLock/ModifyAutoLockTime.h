//
//  ModifyAutoLockTime.h
//  QuickLock
//
//  Created by administrator on 14-12-28.
//  Copyright (c) 2014年 Bge. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ModifyAutoLockTime <NSObject>

- (void)returnAutoLockTime:(int)time;
- (void)returnLEDTime:(int)time;
- (void)returnAlarmMinutes:(int)time;
- (void)returnSureSet;

@end

@interface ModifyAutoLockTime : UIView <UIPickerViewDelegate, UIPickerViewDataSource>
{
        UIPickerView *pickerTime;
        NSMutableArray *arrayDataSource, *arrshowSelected, *arrLED, *arrAlarm;
        
        int selectIndex,selectedIndexLED,selectedAlarm;
}

@property (nonatomic, assign) id<ModifyAutoLockTime> delegate;

@end
