//
//  DoorLockViewController.m
//  QuickLock
//
//  Created by 王洋 on 14/12/27.
//  Copyright (c) 2014年 Bge. All rights reserved.
//

#import "DoorLockViewController.h"
#import "SettingViewController.h"
#import "Prefer.h"
#define redcolor [UIColor colorWithRed:255.0f/255.0f green:38.0f/255.0f blue:0.0f/255.0f alpha:1.0]
#define redborder [UIColor colorWithRed:111.0f/255.0f green:10.0f/255.0f blue:0.0f/255.0f alpha:1.0]
#define greencolor [UIColor colorWithRed:15.0f/255.0f green:157.0f/255.0f blue:88.0f/255.0f alpha:1.0]
#define greenborder [UIColor colorWithRed:8.0f/255.0f green:110.0f/255.0f blue:61.0f/255.0f alpha:1.0]

@interface DoorLockViewController () <SlideNavigationControllerDelegate>
{
    SettingViewController *settingVC;
}
@property (weak, nonatomic) IBOutlet UIButton *btnSetting;
@property (weak, nonatomic) IBOutlet UIView *bgViewOfPasscode;
@property (weak, nonatomic) IBOutlet UIView *viewPasscode;
@property (weak, nonatomic) IBOutlet UILabel *lblPasscodeTitle;
@property (weak, nonatomic) IBOutlet UITextField *txtPasscode;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;
@property (weak, nonatomic) IBOutlet UILabel *lblPasswordEnabmed;
@property (weak, nonatomic) IBOutlet UIButton *btnPasswordEnabmed;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constantLblPasswordRight;

@end

@implementation DoorLockViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:@"isDeviceConnected"];

    _viewPasscode.layer.masksToBounds = YES;
    _viewPasscode.layer.borderColor = darkgray.CGColor;
    _viewPasscode.layer.borderWidth = 3;
    _viewPasscode.layer.cornerRadius = 6;
    _txtPasscode.backgroundColor = lightgray;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(cancelPass)];
    [_bgViewOfPasscode addGestureRecognizer:tap];
    
    btnState = quickLockDeviceManage.lockDevice.passcode_enable;
    (btnState == 0) ? [_btnPasswordEnabmed setBackgroundImage:[UIImage imageNamed:@"gray-switch-icon.png"] forState:UIControlStateNormal] :
    [_btnPasswordEnabmed setBackgroundImage:[UIImage imageNamed:@"orange-switch-icon.png"] forState:UIControlStateNormal];
    
    [SlideNavigationController sharedInstance].portraitSlideOffset = appDelegate.portraitDrawer;
    UIButton *btnMenu = [UIButton buttonWithType:UIButtonTypeCustom];
    btnMenu.frame = CGRectMake(10, 28, 35, 35);
    [btnMenu setImage:[UIImage imageNamed:@"navigation.png"] forState:UIControlStateNormal];
    [btnMenu addTarget:[SlideNavigationController sharedInstance] action:@selector(toggleLeftMenu) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnMenu];
    
    if (screenSize.height == 480)
    {
        _constantLogoBottom.constant = 20;
        _constantLblPasswordRight.constant = 4;
    }
    
    // Show version at the bottom
    [self ShowAPPVersion];
    
    buttonUnlock.layer.borderWidth = buttonLock.layer.borderWidth = 3;
    buttonUnlock.layer.masksToBounds = buttonLock.layer.masksToBounds = YES;
    buttonUnlock.layer.borderColor = darkgray.CGColor;
    buttonUnlock.layer.cornerRadius= buttonLock.layer.cornerRadius = buttonUnlock.frame.size.height/2;
    buttonLock.layer.borderColor = redborder.CGColor;
    [buttonUnlock setTitleColor:darkgray forState:UIControlStateNormal];
    [buttonLock setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    // Defined Notifications for Lock State & Show battery lavel of Lock
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(initButtonState:) name:@"kLockState" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showBattery:) name:@"kBattery" object:nil];
    isSharecodeNotShown = YES;
    
    
    if (quickLockDeviceManage.lockDevice.is_Login_share_Code == 1)
    {
        _btnSetting.hidden = YES;
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    [self initiateLock];
}
-(void)cancelPass
{
    _txtPasscode.text = @"";
    [_txtPasscode resignFirstResponder];
    [UIView animateWithDuration:0.3 animations:^{
        _viewPasscode.alpha = _bgViewOfPasscode.alpha = 0;
    } completion:^(BOOL finished) {
        _viewPasscode.hidden = _bgViewOfPasscode.hidden = YES;
    }];
    isSharecodeNotShown = YES;
}

//// text validation and limit
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    // validation for telephone
    if (textField == _txtPasscode)
    {
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength <= 4);
    }
    return YES;
}

- (IBAction)btnPasscodeSave:(UIButton *)sender
{
    BOOL canViewHide = NO;
    // save
    if (sender.tag == 0)
    {
        if (_txtPasscode.text.length == 0)
        {
            [self.view makeToast:@"Enter password" duration:1.0 position:@"center"];
            canViewHide = NO;
        }
        else
        {
            if (isUnlockNotPassToggle == 1)
            {
                // Unlock button pressed. so compare entered passcode with database passcode
                if ([quickLockDeviceManage.lockDevice.passcode isEqualToString:_txtPasscode.text])
                {
                    [quickLockDeviceManage openLock:YES];
                    [buttonUnlock setBackgroundColor:[UIColor colorWithRed:15.0f/255.0f green:157.0f/255.0f blue:88.0f/255.0f alpha:1.0f]];
                    [buttonUnlock setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                    buttonUnlock.layer.borderColor = [UIColor colorWithRed:2.0f/255.0f green:112.0f/255.0f blue:62.0f/255.0f alpha:1.0f].CGColor;
                    
                    [_txtPasscode resignFirstResponder];
                    _txtPasscode.text = @"";
                    canViewHide = YES;
                    
                    
                    if (isAutoLockPressed)
                    {
                        quickLockDeviceManage.lockDevice.lock_AutoUnLock = YES;
                        [buttonAutoUnlock setBackgroundImage:[UIImage imageNamed:@"orange-switch-icon.png"] forState:UIControlStateNormal];
                        // If AutoUnlock open, set the automatic lock time
                        [self SetButtonStateUnLock];
//                        [quickLockDeviceManage setAutoLockTime:5];
                    }
                }
                else
                {
                    // not equal
                    [self.view makeToast:@"Wrong password" duration:1.0 position:@"center"];
                    _txtPasscode.text = @"";
                    canViewHide = NO;
                }
            }
            else
            {
                // passcode toggle pressed so input new password or input passcode to disable security
                if (btnState == 1)
                {
                    [_btnPasswordEnabmed setBackgroundImage:[UIImage imageNamed:@"orange-switch-icon.png"] forState:UIControlStateNormal];
                    
                    quickLockDeviceManage.lockDevice.passcode_enable = btnState;
                    quickLockDeviceManage.lockDevice.passcode = _txtPasscode.text;
                    
                    [dbAccess UpdatePasscode:quickLockDeviceManage.lockDevice lock_UUID:quickLockDeviceManage.lockDevice.lock_UUID];
                    [dbAccess GetAllLockDevice];
                    
                    canViewHide = YES;
                    _txtPasscode.text = @"";
                    [_txtPasscode resignFirstResponder];
                }
                else
                {
                    if ([quickLockDeviceManage.lockDevice.passcode isEqualToString:_txtPasscode.text])
                    {
                        [_btnPasswordEnabmed setBackgroundImage:[UIImage imageNamed:@"gray-switch-icon.png"] forState:UIControlStateNormal];
                        
                        quickLockDeviceManage.lockDevice.passcode_enable = btnState;
                        quickLockDeviceManage.lockDevice.passcode = _txtPasscode.text;
                        
                        [dbAccess UpdatePasscode:quickLockDeviceManage.lockDevice lock_UUID:quickLockDeviceManage.lockDevice.lock_UUID];
                        [dbAccess GetAllLockDevice];
                        
                        canViewHide = YES;
                        _txtPasscode.text = @"";
                        [_txtPasscode resignFirstResponder];
                    }
                    else
                    {
                        [self.view makeToast:@"Wrong password" duration:1.0 position:@"center"];
                        _txtPasscode.text = @"";
                        canViewHide = NO;
                    }
                }
                // Set automatic locking time
//                [quickLockDeviceManage setAutoLockTime:quickLockDeviceManage.lockDevice.lock_AutoLockTime];
            }
        }
    }
    else // cancel
    {
        _txtPasscode.text = @"";
        [_txtPasscode resignFirstResponder];
        canViewHide = YES;
    }
    
    if (canViewHide)
    {
        [UIView animateWithDuration:0.3 animations:^{
            _viewPasscode.alpha = _bgViewOfPasscode.alpha = 0;
        } completion:^(BOOL finished) {
            _viewPasscode.hidden = _bgViewOfPasscode.hidden = YES;
        }];
    }
    isSharecodeNotShown = YES;
}

- (IBAction)btnPasswordEnabmed:(id)sender
{
    isSharecodeNotShown = NO;

    if (btnState == 0)
    {
        btnState = 1;
        
        _bgViewOfPasscode.hidden = _viewPasscode.hidden = NO;
        _lblPasscodeTitle.text = @"Set 4 digit password to enable";
        _txtPasscode.placeholder = @"Set password";
        [_btnSave setTitle:@"Save" forState:UIControlStateNormal];
        [UIView animateWithDuration:0.3 animations:^{
            _bgViewOfPasscode.alpha = 0.6;
            _viewPasscode.alpha = 1;
            [_txtPasscode becomeFirstResponder];
        }];
    }
    else
    {
        btnState = 0;
        isUnlockNotPassToggle = 0;
        
        _bgViewOfPasscode.hidden = _viewPasscode.hidden =  NO;
        _lblPasscodeTitle.text = @"Enter 4 digit password to disable";
        _txtPasscode.placeholder = @"Enter password";
        [_btnSave setTitle:@"Ok" forState:UIControlStateNormal];
        [UIView animateWithDuration:0.3 animations:^{
            _bgViewOfPasscode.alpha = 0.6;
            _viewPasscode.alpha = 1;
            [_txtPasscode becomeFirstResponder];
        }];
    }
}

- (void)ShowAPPVersion
{
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *APPVersion =[NSString stringWithFormat:@"%@.%@",
                           [infoDictionary objectForKey:@"CFBundleShortVersionString"],
                           [infoDictionary objectForKey:@"CFBundleVersion"]];
    [_labelAPPVersion setText:[NSString stringWithFormat:@"APP Version : %@", APPVersion]];
}

- (void)showBattery:(NSNotification *)notification
{
    int batteryValue = [[notification object] intValue];
    [labelBattery setText:[NSString stringWithFormat:@"Battery : %d%%", batteryValue]];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden = YES;
}

// It will slide screen to go to left menu
- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return isSharecodeNotShown;
}

-(void)initiateLock
{
    if (quickLockDeviceManage.lockDevice.lock_Type == DOORLOCK)
    {
        // Default DoorLock
    }
    if (quickLockDeviceManage.lockDevice.lock_Type == DEADBOLT)
    {
        labelName.text = @"Quicklock Deadbolt";
        _imgGunbox.image = [UIImage imageNamed:@"deadlock"];
    }
    
    // Get the lock status to determine Lock UnLock can click or can click on one of the two can only point, the other is unavailable.
    [quickLockDeviceManage getLockState];
    
    if (quickLockDeviceManage.lockDevice.lock_AutoUnLock)
    {
        if (quickLockDeviceManage.lockDevice.passcode_enable == 1)
        {
            isUnlockNotPassToggle = 1;
            _bgViewOfPasscode.hidden = _viewPasscode.hidden =  NO;
            _lblPasscodeTitle.text = @"Enter 4 digit password";
            _txtPasscode.placeholder = @"Enter password";
            [_btnSave setTitle:@"Ok" forState:UIControlStateNormal];
            [UIView animateWithDuration:0.3 animations:^{
                _bgViewOfPasscode.alpha = 0.6;
                _viewPasscode.alpha = 1;
                [_txtPasscode becomeFirstResponder];
                isSharecodeNotShown = NO;
            }];
        }
        else
        {
            // If AutoUnlock open
            [self cannotAutoUnLockWhenUnlock];
        }
        [buttonAutoUnlock setBackgroundImage:[UIImage imageNamed:@"orange-switch-icon"] forState:UIControlStateNormal];
    }
    else
    {
        [buttonAutoUnlock setBackgroundImage:[UIImage imageNamed:@"gray-switch-icon.png"] forState:UIControlStateNormal];
//        [quickLockDeviceManage setAutoLockTime:quickLockDeviceManage.lockDevice.lock_AutoLockTime];
    }
    [quickLockDeviceManage getBatteryLevel];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)cannotAutoUnLockWhenUnlock
{
    // If AutoUnlock open
    if(!quickLockDeviceManage.lockIsOpen)
    {
        [quickLockDeviceManage openLock:YES];
//        [quickLockDeviceManage setAutoLockTime:5];
    }
}

- (void)initButtonState:(NSNotification *)notification
{
    BOOL isLockOpen = [[notification object] boolValue];
    if (isLockOpen)
    {
        [self SetButtonStateUnLock];
    }
    else
    {
        [self SetButtonStateLock];
    }
    
    [buttonSet setEnabled:YES];
}

- (void)SetButtonStateUnLock
{
    [buttonUnlock setBackgroundColor:greencolor];
    [buttonUnlock setUserInteractionEnabled:NO];
    [buttonUnlock setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    buttonUnlock.layer.borderColor = greenborder.CGColor;
    
    [buttonLock setBackgroundColor:lightgray];
    [buttonLock setUserInteractionEnabled:YES];
    buttonLock.layer.borderColor = darkgray.CGColor;
    [buttonLock setTitleColor:darkgray forState:UIControlStateNormal];
}

- (void)SetButtonStateLock
{
    [buttonUnlock setBackgroundColor:lightgray];
    [buttonUnlock setUserInteractionEnabled:YES];
    buttonUnlock.layer.borderColor = darkgray.CGColor;
    [buttonUnlock setTitleColor:darkgray forState:UIControlStateNormal];

    [buttonLock setBackgroundColor:redcolor];
    [buttonLock setUserInteractionEnabled:NO];
    buttonLock.layer.borderColor = redborder.CGColor;
    [buttonLock setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}

- (IBAction)btnHome:(id)sender
{
    [quickLockDeviceManage initiativeDisConnect];
    [self.navigationController popViewControllerAnimated:NO];
}

- (IBAction)buttonLockTouch:(id)sender
{
        [quickLockDeviceManage openLock:NO];
        [self SetButtonStateLock];
}

- (IBAction)buttonAutoUnlockTouch:(id)sender
{
    if (quickLockDeviceManage.lockDevice.lock_AutoUnLock)
    {
//        [quickLockDeviceManage setAutoLockTime:quickLockDeviceManage.lockDevice.lock_AutoLockTime];
        quickLockDeviceManage.lockDevice.lock_AutoUnLock = NO;
        [buttonAutoUnlock setBackgroundImage:[UIImage imageNamed:@"gray-switch-icon.png"] forState:UIControlStateNormal];
    }
    else
    {
        if (quickLockDeviceManage.lockDevice.passcode_enable == 1)
        {
            isUnlockNotPassToggle = 1;
            _bgViewOfPasscode.hidden = _viewPasscode.hidden =  NO;
            _lblPasscodeTitle.text = @"Enter 4 digit password";
            _txtPasscode.placeholder = @"Enter password";
            isAutoLockPressed = YES;
            [_btnSave setTitle:@"Ok" forState:UIControlStateNormal];
            [UIView animateWithDuration:0.3 animations:^{
                _bgViewOfPasscode.alpha = 0.6;
                _viewPasscode.alpha = 1;
                [_txtPasscode becomeFirstResponder];
                isSharecodeNotShown = NO;
            }];
        }
        else
        {
            quickLockDeviceManage.lockDevice.lock_AutoUnLock = YES;
            [buttonAutoUnlock setBackgroundImage:[UIImage imageNamed:@"orange-switch-icon"] forState:UIControlStateNormal];
            
            if(!quickLockDeviceManage.lockIsOpen)
            {
                [quickLockDeviceManage openLock:YES];
//                [quickLockDeviceManage setAutoLockTime:5];
            }
        }
    }
    [dbAccess UpdateLockDevice:quickLockDeviceManage.lockDevice WithLockID:quickLockDeviceManage.lockDevice.lock_ID];
}

- (IBAction)buttonUnlockTouch:(id)sender
{
    if (quickLockDeviceManage.lockDevice.passcode_enable == 1)
    {
        isUnlockNotPassToggle = 1;
        _bgViewOfPasscode.hidden = _viewPasscode.hidden =  NO;
        _lblPasscodeTitle.text = @"Enter 4 digit password";
        _txtPasscode.placeholder = @"Enter password";
        [_btnSave setTitle:@"Ok" forState:UIControlStateNormal];
        [UIView animateWithDuration:0.3 animations:^{
            _bgViewOfPasscode.alpha = 0.6;
            _viewPasscode.alpha = 1;
            [_txtPasscode becomeFirstResponder];
            isSharecodeNotShown = NO;
        }];
    }
    else
    {
        [quickLockDeviceManage openLock:YES];
        [self SetButtonStateUnLock];
//        [quickLockDeviceManage setAutoLockTime:quickLockDeviceManage.lockDevice.lock_AutoLockTime];
    }
}

- (IBAction)buttonSetTouch:(id)sender
{
    settingVC = [self.storyboard instantiateViewControllerWithIdentifier:@"setting"];
    [self.navigationController pushViewController:settingVC animated:NO];
}

@end
