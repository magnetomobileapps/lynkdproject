//
//  Webservices.h
//  AFNetworkingDemo
//
//  Created by Pavan Jadhav on 20/12/16.
//  Copyright © 2016 Pavan Jadhav. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Webservices : NSObject
+  (void)requestPostUrl:(NSString *)strURL parameters:(NSDictionary *)dictParams success:(void (^)(NSDictionary *responce))success failure:(void (^)(NSError *error))failure;
+(void)postWithUrlString:(NSString *)urlString parameters:(NSDictionary *)parameters success:(void (^)(NSDictionary *responce))success failure:(void (^)(NSError *error))failure;
+ (void)requestPostUrlWithImage: (NSString *)serviceName parameters:(NSDictionary *)dictParams image:(UIImage *)image  isImageSelected:(BOOL)isImageSelected filePath:(NSString *)videofilePath success:(void (^)(NSDictionary *responce))success failure:(void (^)(NSError *error))failure;
+(void)getWithUrlString:(NSString *)urlString success:(void (^)(NSDictionary *responce))success failure:(void (^)(NSError *error))failure;

+ (void)requestPostUrlWithThumbnailImage: (NSString *)serviceName parameters:(NSDictionary *)dictParams image:(UIImage *)image filePath:(NSString *)videofilePath success:(void (^)(NSDictionary *responce))success failure:(void (^)(NSError *error))failure;
+ (void)requestPostUrlForTwitterUploadWithImage: (NSString *)serviceName parameters:(NSDictionary *)dictParams image:(UIImage *)image filePath:(NSString *)videofilePath success:(void (^)(NSDictionary *responce))success failure:(void (^)(NSError *error))failure;

// other required methods
+(NSString *) stringByStrippingHTML : (NSString* ) str;
+(BOOL)checkForStringValidation:(NSString*)str;
+(NSString*)checkForStringValidationWithReturnSameString:(NSString*)str;
+(NSString *)getCurrentTimeStamp;

// AlertAction
+(void)alertViewWithOneButtonName:(NSString*)btnName WithTitle:(NSString*)title AndMessage:(NSString*)message AndController:(UIViewController*)controller;

+(void)alertViewWithTwoButtonYesName:(NSString*)btnYes ButtonNoName:(NSString*)btnNo WithTitle:(NSString*)title AndMessage:(NSString*)message;
+(BOOL)CheckInternetConnection;
+(NSString *)getCurrentEpochTime;

@end
