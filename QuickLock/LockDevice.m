//
//  LockDevice.m
//  QuickLock
//
//  Created by 王洋 on 14/12/27.
//  Copyright (c) 2014年 Bge. All rights reserved.
//

#import "LockDevice.h"

@implementation LockDevice

@synthesize lock_ID;
@synthesize lock_Name;
@synthesize lock_Type;
@synthesize lock_Password;
@synthesize lock_UUID;
@synthesize lock_AutoUnLock;
@synthesize lock_AutoLockTime;
@synthesize lock_FirmwareRevision;
@synthesize TAG_ID;
@synthesize TAG_TYPE;
@synthesize TAG_IMEI;
@synthesize TAG_USERNAME;
@synthesize TAG_DELETE_IMEI_OR_RFID;
@synthesize share_Code;
@synthesize share_Code_Count;
@synthesize share_Code_Start_Time;
@synthesize share_Code_End_Time;
@synthesize share_Code_Time;
@synthesize share_Code_Time1;
@synthesize share_Code_Time2;
@synthesize is_Login_share_Code;
@synthesize isLED;
@synthesize isAlarm;
@synthesize currentLEDState;
@synthesize box_LED_Time;
@synthesize sure_Set_Percent;
@synthesize alarmType;
@synthesize delayTime;
@synthesize lock_FingerprintCount;
@synthesize box_Alarm_Time;
@synthesize share_Code_End_Time_minutes;
@synthesize share_Code_Start_Time_minutes;
@synthesize share_Code_DATE_Time_FLAG;
@synthesize share_code_time_flag;
@synthesize alarmPrevoiusState;
@synthesize alarmDelay;
@synthesize strEnablePasscode;
@synthesize passcode;
@synthesize passcode_enable;
@synthesize fingerPrintReturn;
@synthesize str_share_Code_Stert_time;
@synthesize str_share_Code_End_time;
@synthesize zwaveState;

// QuickLock Bge 2014-12-29 Modify flash back into the settings interface, in initialization assigned an initial value
- (id)init
{
    self = [super init];
    if (self) {
        // The default data
        lock_ID = 0;
        lock_Name = @"";
        lock_Type = 1;
        lock_Password = @"";
        lock_UUID = @"";
        lock_AutoUnLock = NO;
        lock_AutoLockTime = 3;
        lock_FirmwareRevision = @"";
        TAG_ID = 0;
        TAG_TYPE = @"";
        TAG_IMEI = @"";
        TAG_USERNAME = @"";
        TAG_DELETE_IMEI_OR_RFID = @"";
        share_Code = @"";
        share_Code_Time = @"";
        share_Code_Count = 0;
        share_Code_Start_Time = nil;
        share_Code_End_Time = nil;
        share_code_time_flag = 0;
//        is_Login_share_Code = NO;
        isLED = 0;
        isAlarm = 0;
        box_LED_Time = 30;
        sure_Set_Percent = 60;
        alarmType = 0;
        delayTime = 0;
        lock_FingerprintCount = 0;
        currentLEDState = @"";
        box_Alarm_Time = 0;
        share_Code_End_Time_minutes = 0;
        share_Code_Start_Time_minutes = 0;
        share_Code_DATE_Time_FLAG = 0;
        alarmPrevoiusState = 0;
        alarmDelay = 0;
        strEnablePasscode = @"";
        passcode = @"";
        passcode_enable = 0;
        fingerPrintReturn = 0;
        str_share_Code_Stert_time = @"";
        str_share_Code_End_time = @"";
        zwaveState = 0;
    }
    return self;
}

@end
