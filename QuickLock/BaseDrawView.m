//
//  BaseDrawView.m
//  QuickLock
//
//  Created by Adam Hathaway on 12/3/15.
//  Copyright © 2015 Bge. All rights reserved.
//

#import "BaseDrawView.h"

@implementation BaseDrawView

-(void)setFillColor:(UIColor *)fillColor {
    _fillColor = fillColor;
    
    [self setNeedsDisplay];
}

-(void)setFillColor2:(UIColor *)fillColor2 {
    _fillColor2 = fillColor2;
    
    [self setNeedsDisplay];
}

-(void)setStrokeColor:(UIColor *)strokeColor {
    _strokeColor = strokeColor;
    
    [self setNeedsDisplay];
}

-(void)setTextColor:(UIColor *)textColor {
    _textColor = textColor;
    
    [self setNeedsDisplay];
}

@end
