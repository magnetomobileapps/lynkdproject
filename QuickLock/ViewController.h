//
//  ViewController.h
//  QuickLock
//
//  Created by administrator on 14-12-26.
//  Copyright (c) 2014年 Bge. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "Intellivent.h"

@class MyLocksViewController;
@interface ViewController : UIViewController <UIAlertViewDelegate, UITextFieldDelegate, SlideNavigationControllerDelegate>
{
//    int nextImageIndex;
    IBOutlet UIImageView *imageViewLoad;
    IBOutlet UILabel *labelAPPVersion;
    UILabel* appVersion;
    IBOutlet UIButton *buttonMyLocks;
    IBOutlet UIButton *buttonAddLock;
    IBOutlet UIButton *buttonBack;
    AppDelegate *app;

    BOOL isSearching,isPushViewSet;
    NSTimer *tmrAPPLock;
}
@property (weak, nonatomic) IBOutlet UIView *viewOfLocks;

- (IBAction)ShowMyLocksList:(id)sender;
- (IBAction)AddLock:(id)sender;
- (IBAction)BackToSearchDeviceThatConnected:(id)sender;

@end

