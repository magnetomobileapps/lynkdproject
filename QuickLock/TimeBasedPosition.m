//
//  TimeBasedPosition.m
//  QuickLock
//
//  Created by PUNDSK001 on 04/01/16.
//  Copyright © 2016 Bge. All rights reserved.
//

#import "TimeBasedPosition.h"

@interface TimeBasedPosition ()
{
    UIDatePicker *_datePicker;
    UIView *ViewOfDatepicker;
    UIButton *btnSetDate, *btnDateCancel;
    
    IBOutlet UILabel *lblTitle;
    NSDateFormatter *format;
    NSString *strDate;
    BOOL isDateSelect, isTimeFromSelected;
}
// Properties
@property (weak, nonatomic) IBOutlet UITextField *_timeTo;
@property (weak, nonatomic) IBOutlet UITextField *timeFrom;
@property (weak, nonatomic) IBOutlet UITextField *txtDate;
@property (weak, nonatomic) IBOutlet UITextField *txtPerc;
@property (weak, nonatomic) IBOutlet UIView *viewBg;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) IBOutlet UIView *viewSelectPercentage;

@end

@implementation TimeBasedPosition

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    format = [[NSDateFormatter alloc]init];
}

-(void)viewDidAppear:(BOOL)animated
{
    // Apply Layer design to controls
    [self txtLayer:__timeTo buttonLayer:nil];
    [self txtLayer:_timeFrom buttonLayer:nil];
    [self txtLayer:_txtDate buttonLayer:_btnCancel];
    [self txtLayer:_txtPerc buttonLayer:_btnSave];

    // create dynamic datepicker
    ViewOfDatepicker = [[UIView alloc]initWithFrame:CGRectMake((self.view.frame.size.width-301)/2, (self.view.frame.size.height-210)/2, 301, 210)];
    ViewOfDatepicker.backgroundColor = [UIColor whiteColor];
    ViewOfDatepicker.hidden = YES;
    ViewOfDatepicker.alpha = 0.0;
    ViewOfDatepicker.layer.masksToBounds = YES;
    ViewOfDatepicker.layer.borderWidth = 4;
    ViewOfDatepicker.layer.borderColor = lightgray.CGColor;
    ViewOfDatepicker.layer.cornerRadius = 8;
    [self.view addSubview:ViewOfDatepicker];
    
    _datePicker = [[UIDatePicker alloc]initWithFrame:CGRectMake(0, 0, 301, 162)];
    _datePicker.datePickerMode = UIDatePickerModeDate;
    _datePicker.backgroundColor = [UIColor clearColor];
    [ViewOfDatepicker addSubview:_datePicker];
    
    btnSetDate = [UIButton buttonWithType:UIButtonTypeSystem];
    [btnSetDate addTarget:self action:@selector(btnSetDate:) forControlEvents:UIControlEventTouchUpInside];
    btnSetDate.frame = CGRectMake(8, 167, 138, 35);
    [btnSetDate setTitle:@"Set" forState:UIControlStateNormal];
    [btnSetDate setTintColor:[UIColor whiteColor]];
    btnSetDate.backgroundColor = darkgray;
    btnSetDate.layer.masksToBounds = YES;
    btnSetDate.layer.cornerRadius = 4;
    [ViewOfDatepicker addSubview:btnSetDate];

    btnDateCancel = [UIButton buttonWithType:UIButtonTypeSystem];
    [btnDateCancel addTarget:self action:@selector(btnDateCancel:) forControlEvents:UIControlEventTouchUpInside];
    btnDateCancel.frame = CGRectMake(155, 167, 138, 35);
    [btnDateCancel setTitle:@"Cancel" forState:UIControlStateNormal];
    [btnDateCancel setTintColor:[UIColor whiteColor]];
    btnDateCancel.backgroundColor = darkgray;
    btnDateCancel.layer.masksToBounds = YES;
    btnDateCancel.layer.cornerRadius = 4;
    [ViewOfDatepicker addSubview:btnDateCancel];

}

// Apply Layer design to controls
-(void)txtLayer:(UITextField *)textfield buttonLayer:(UIButton *)buttom
{
    textfield.layer.cornerRadius = ViewOfDatepicker.layer.cornerRadius = 6;
    textfield.layer.borderWidth = ViewOfDatepicker.layer.borderWidth = 4;
    textfield.layer.borderColor = ViewOfDatepicker.layer.borderColor = bordergray.CGColor;
    textfield.backgroundColor = lightgray;
    
    buttom.layer.masksToBounds = YES;
    buttom.layer.cornerRadius = 4;
}

- (IBAction)btnBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}

- (IBAction)btnSave:(id)sender
{
    NSLog(@"btn save");
}

- (IBAction)btnCancel:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}

// button to open popup for set percentage
- (IBAction)btnPercentage:(id)sender
{
    _viewSelectPercentage.hidden = NO;
    _viewBg.hidden = NO;
    [UIView animateWithDuration:0.3f animations:^{
        _viewSelectPercentage.alpha = 1.0f;
        _viewBg.alpha = 0.5f;
        _viewBg.backgroundColor = [UIColor blackColor];
    }];
}

// buton to view popup to select date
- (IBAction)btnSelectDate:(id)sender
{
    [_datePicker setDatePickerMode:UIDatePickerModeDate];
    [format setDateFormat:@"dd-MM-yyyy"];
    isDateSelect = YES;
    
    ViewOfDatepicker.hidden = NO;
    _viewBg.hidden = NO;
    [UIView animateWithDuration:0.3f animations:^{
        ViewOfDatepicker.alpha = 1.0f;
        _viewBg.alpha = 0.5f;
        _viewBg.backgroundColor = [UIColor blackColor];
    } completion:^(BOOL finished) {
        
    }];
}

- (IBAction)btnTimeFrom:(id)sender
{
    // NL to hide AM/PM and show 24Hr in time picker
    NSLocale *locale = [[NSLocale alloc]initWithLocaleIdentifier:@"NL"];
    [_datePicker setLocale:locale];
    [_datePicker setDatePickerMode:UIDatePickerModeTime];
    [format setDateFormat:@"HH:mm"];
    isDateSelect = NO;
    isTimeFromSelected = YES;
    
    ViewOfDatepicker.hidden = NO;
    _viewBg.hidden = NO;
    [UIView animateWithDuration:0.3f animations:^{
        ViewOfDatepicker.alpha = 1.0f;
        _viewBg.alpha = 0.5f;
        _viewBg.backgroundColor = [UIColor blackColor];
    }];
}

- (IBAction)btnTimeTo:(id)sender
{
    NSLocale *locale = [[NSLocale alloc]initWithLocaleIdentifier:@"NL"];
    [_datePicker setLocale:locale];
    [_datePicker setDatePickerMode:UIDatePickerModeTime];
    [format setDateFormat:@"HH:mm"];
    isDateSelect = NO;
    isTimeFromSelected = NO;
    ViewOfDatepicker.hidden = NO;
    _viewBg.hidden = NO;
    [UIView animateWithDuration:0.3f animations:^{
        ViewOfDatepicker.alpha = 1.0f;
        _viewBg.alpha = 0.5f;
        _viewBg.backgroundColor = [UIColor blackColor];
    } completion:^(BOOL finished) {
        
    }];
}

// set date and hide date view
- (IBAction)btnSetDate:(id)sender
{
    strDate = [format stringFromDate:[_datePicker date]];
    
    if (isDateSelect == YES)
    {
        _txtDate.text = strDate;
    }
    else
    {
        if (isTimeFromSelected == YES)
        {
            _timeFrom.text = strDate;
        }
        else
        {
            __timeTo.text = strDate;
        }
    }
    
    [UIView animateWithDuration:0.3f animations:^{
        ViewOfDatepicker.alpha = 0.0f;
        _viewBg.alpha = 0.0f;
    } completion:^(BOOL finished) {
        ViewOfDatepicker.hidden = YES;
    }];
}

- (IBAction)btnDateCancel:(id)sender
{
    [UIView animateWithDuration:0.3f animations:^{
        ViewOfDatepicker.alpha = 0.0f;
        _viewBg.alpha = 0.0f;
    } completion:^(BOOL finished) {
        ViewOfDatepicker.hidden = YES;
        _viewBg.hidden = YES;
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// Select percentage and assign to
- (IBAction)btnSelectPercentage:(UIButton *)sender
{
    [UIView animateWithDuration:0.3f animations:^{
        _viewSelectPercentage.alpha = 0.0f;
        _viewBg.alpha = 0.0f;
    } completion:^(BOOL finished) {
        _viewSelectPercentage.hidden = YES;
        _viewBg.hidden = YES;
        
        switch (sender.tag)
        {
            case 5:
                break;
                
            case 25: // 25%
                _txtPerc.text = @"25%";
                break;
                
            case 50: // 50%
                _txtPerc.text = @"50%";
                break;
                
            case 75: // 75%
                _txtPerc.text = @"75%";
                break;
                
            case 100: // 100%
                _txtPerc.text = @"100%";
                break;
                
            default:
                break;
        }
    }];
}

@end
