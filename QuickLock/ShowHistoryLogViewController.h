//
//  ShowHistoryLogViewController.h
//  QuickLock
//
//  Created by administrator on 15-3-17.
//  Copyright (c) 2015年 Bge. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface ShowHistoryLogViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, MFMailComposeViewControllerDelegate>
{
    IBOutlet UITableView *tableViewHistoryLog;
    
    NSMutableArray *arrayHistoryLog, *arrDate, *arrOfDateFoemat, *arrDateTemp, *sortingArray, *temp;
    NSMutableDictionary *dictForHistory;
    NSMutableString *strLog;
    NSDateFormatter *dateformat;
    NSArray *arrTemp;
    UIAlertView *alert;
}

@end
