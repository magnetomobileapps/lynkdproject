//
//  SettingViewController.m
//  QuickLock
//
//  Created by administrator on 14-12-28.
//  Copyright (c) 2014年 Bge. All rights reserved.
//

#import "SettingViewController.h"
#import "Prefer.h"
#import "DeleteDeviceCell.h"
#import "PublicMarco.h"
#import "WifiSetup.h"

@interface SettingViewController ()
{
    ModifyName *modifyNameView;
    ModifyAutoLockTime *modifyAutoLockTimeView;
    InputPassword *inputPasswordView;
    NSArray *numbers;
    NSMutableArray *arrLED;
}
@property (strong, nonatomic) IBOutlet UIPickerView *pickerTime;
@property (strong, nonatomic) IBOutlet UIView *viewSelectLED;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIView *viewSureSet;
@property (weak, nonatomic) IBOutlet UIButton *btnSaveSure;
@property (weak, nonatomic) IBOutlet UISlider *slide;
@property (weak, nonatomic) IBOutlet UILabel *lbl1;
@property (weak, nonatomic) IBOutlet UIButton *btnCancelSure;
@property (weak, nonatomic) IBOutlet UIView *viewSureBg;
@end

@implementation SettingViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setExtraCellLineHidden:tableViewSet];
    [tableViewSet setSeparatorInset:UIEdgeInsetsZero];
    arrLED = [NSMutableArray new];
    for (int i = 30; i <= 90; i+=5)
    {
        [arrLED addObject:[NSString stringWithFormat:@"%d", i]];
    }
    
    numbers = @[@(1), @(2), @(3), @(4), @(5)];
    
    _viewSureBg.alpha = 0.0f;
    _viewSureBg.hidden = YES;
    
    _viewSureSet.layer.masksToBounds = _viewSelectLED.layer.masksToBounds = _btnCancelSure.layer.masksToBounds = _btnSaveSure.layer.masksToBounds = YES;
    _viewSureSet.layer.borderColor = _viewSelectLED.layer.borderColor = lightgray.CGColor;
    _viewSureSet.layer.borderWidth = _viewSelectLED.layer.borderWidth = 4;
    _viewSureSet.layer.cornerRadius = _viewSelectLED.layer.cornerRadius = 10;
    
    _btnCancelSure.layer.cornerRadius = _btnSaveSure.layer.cornerRadius = 6;
    _lblTitle.textColor = titleorange;

    [_slide setThumbImage:[UIImage imageNamed:@"Screenshot_1"] forState:UIControlStateNormal];
    [_slide setThumbImage:[UIImage imageNamed:@"Screenshot_1"] forState:UIControlStateHighlighted];
    
    NSInteger numberOfSteps = ((float)[numbers count] - 1);
    _slide.maximumValue = numberOfSteps;
    [_slide setMinimumValue:0];
    NSLog(@"val : %d",(quickLockDeviceManage.lockDevice.sure_Set_Percent/20));
    [_slide setValue:(quickLockDeviceManage.lockDevice.sure_Set_Percent/20)-1];
    slidevalue = quickLockDeviceManage.lockDevice.sure_Set_Percent;
    // quickLockDeviceManage.lockDevice.sure_Set_Percent
    _slide.continuous = YES; // NO makes it call only once you let go
    [_slide addTarget:self action:@selector(valueChanged:) forControlEvents:UIControlEventValueChanged];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(ModifyPasswordSuccess:)
                                                 name:@"kModifyPasswordSuccess"
                                               object:nil];
    
    tableViewSet.separatorColor = [UIColor clearColor];
    
    [self applyBasierPath:_lbl1];
    
    if ((quickLockDeviceManage.lockDevice.lock_Type == GUNBOX || quickLockDeviceManage.lockDevice.lock_Type == QUICKBOX || quickLockDeviceManage.lockDevice.lock_Type == MEDICINEBOX || quickLockDeviceManage.lockDevice.lock_Type == TALLGUNBOX) && screenSize.height <= 568)
    {
        tableViewSet.scrollEnabled = YES;
    }
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return arrLED.count;
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [NSString stringWithFormat:@"%@ Seconds",arrLED[row]];
}
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 40;
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    selectedIndexLED = ((int)row*5) + 30;
}

-(void)cancelModifyPassword
{
    
}
-(void)returnAlarmMinutes:(int)time
{
    
}

- (IBAction)btnCancelLED:(id)sender
{
    _viewSelectLED.hidden = YES;
    _viewSureBg.hidden = YES;
}

- (IBAction)btnOkLED:(id)sender
{
    _viewSelectLED.hidden = YES;
    _viewSureBg.hidden = YES;
    
    quickLockDeviceManage.lockDevice.box_LED_Time = selectedIndexLED;
    //Send to hardware, modification LED time
    [quickLockDeviceManage setLEDTime:selectedIndexLED];
    
    [dbAccess UpdateBoxLedTime:quickLockDeviceManage.lockDevice WithLockID:quickLockDeviceManage.lockDevice.lock_ID];
    [self viewDidAppear:YES];
}

- (void)valueChanged:(UISlider *)sender
{
    // round the slider position to the nearest index of the numbers array
    NSUInteger index = (NSUInteger)(_slide.value + 0.5);
    [_slide setValue:index animated:NO];
    NSNumber *number = numbers[index]; // <-- This numeric value you want
    NSInteger *intNumber = [number integerValue]*20;
    slidevalue = intNumber;
}

- (IBAction)slider:(UISlider *)sender
{
//    slidevalue = [sender value];
//    NSLog(@"%d",slidevalue);
    
    //    self.debugLabel.text = [NSString stringWithFormat:@"Value: %f",value];
}

- (IBAction)btnSaveSure:(UIButton *)sender
{
    [UIView animateWithDuration:0.3 animations:^{
        _viewSureSet.alpha = 0.0f;
        _viewSureBg.alpha = 0.0f;
        if (sender.tag == 4)
        {
            // btnSave
            [self returnSureSet];
        }
        else if (sender.tag == 5)
        {
            // btnCancel
        }
    } completion:^(BOOL finished) {
        _viewSureBg.hidden = YES;
        _viewSureSet.hidden = YES;
        
        [_slide setValue:(quickLockDeviceManage.lockDevice.sure_Set_Percent/20)-1];
    }];
    [self viewDidAppear:YES];
}

-(void)applyBasierPath:(UILabel *)label
{
    UIColor *colorRed = [UIColor colorWithRed:229.0f/255.0f green:0.0f/255.0f blue:28.0f/255.0f alpha:10.f];
    UIColor *colorGray = [UIColor colorWithRed:199.0f/255.0f green:201.0f/255.0f blue:202.0f/255.0f alpha:10.f];
    
    CAGradientLayer *layer = [CAGradientLayer layer];
    layer.frame = label.bounds;
    layer.colors = [NSArray arrayWithObjects:(id)colorRed.CGColor,(id)colorGray.CGColor, nil];
    [layer setStartPoint:CGPointMake(0.0, 0.5)];
    [layer setEndPoint:CGPointMake(0.5, 0.5)];
    [label.layer insertSublayer:layer atIndex:0];

    label.layer.masksToBounds = YES;
    label.layer.cornerRadius = 5;
}

- (void)viewWillAppear:(BOOL)animated
{
        [super viewWillAppear:animated];
        
        [inputPasswordView removeFromSuperview];
        inputPasswordView = nil;
        
        self.navigationController.navigationBarHidden = YES;
}

- (void)viewDidDisappear:(BOOL)animated
{
        [inputPasswordView removeFromSuperview];
        
        [alertDeleteDevice dismissWithClickedButtonIndex:0 animated:NO];
        [alertModifyUserName dismissWithClickedButtonIndex:0 animated:NO];
}

- (void)ModifyPasswordSuccess:(NSNotification *)notification
{
        [self viewDidAppear:YES];
}

//Hide excess dividing line
-(void)setExtraCellLineHidden: (UITableView *)tableView
{
        UIView *view = [UIView new];
        view.backgroundColor = [UIColor clearColor];
        [tableView setTableFooterView:view];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    NSString *userName = [[NSUserDefaults standardUserDefaults] valueForKey:@"kUserName"];
    NSString *deviceName = quickLockDeviceManage.lockDevice.lock_Name;
    NSString *password = quickLockDeviceManage.lockDevice.lock_Password;
    NSString *autoLockTime = [NSString stringWithFormat:@"%ds", quickLockDeviceManage.lockDevice.lock_AutoLockTime];
    NSString *LEDTime = [NSString stringWithFormat:@"%ds", quickLockDeviceManage.lockDevice.box_LED_Time];
    NSString *firmwareVersion = quickLockDeviceManage.lockDevice.lock_FirmwareRevision;
    NSString *sureSet = [NSString stringWithFormat:@"%d",quickLockDeviceManage.lockDevice.sure_Set_Percent/20];
    NSString *alarmState;
    if(quickLockDeviceManage.lockDevice.alarmPrevoiusState == 0)
       alarmState = @"OFF";
    else if(quickLockDeviceManage.lockDevice.alarmPrevoiusState == 1)
       alarmState = @"Silent";
    else
       alarmState = @"Audible";
    
    // Z-wave is not in PadLock and DoorLock
    if(quickLockDeviceManage.lockDevice.lock_Type == PADLOCK)
    {
        _lblTitle.text = @"Quicklock Padlock";
        
        arrayImages = @[@"user-icon.png",
                        @"pad-lock-icon.png",
                        //@"wifi.png",
                        @"history-log.png",
                        @"auto-lock-time.png",
                        @"tag-icon.png",
                        @"password-setting.png",
                        @"share-code.png",
                        @"remove-dorlock.png"
                        ];
        arrayTitle = @[@"User Name : ",
                       @"Device Name : ",
                       //@"Wifi Alert Notifications",
                       @"History Log",
                       @"Auto lock time : ",
                       @"RFID Tag Management",
                       @"Password : ",
                       @"Share Code",
                       @"Remove : "];
        arraySubTitle = @[userName ,deviceName, @"", autoLockTime, @"", password, @"", deviceName];
    }
    if(quickLockDeviceManage.lockDevice.lock_Type == RFWRITER)
    {
        _lblTitle.text = @"RF Writer";
        
        arrayImages = @[@"user-icon.png",
                        @"pad-lock-icon.png",
                        //@"wifi.png",
                        @"history-log.png",
                        @"auto-lock-time.png",
                       // @"tag-icon.png",
                        @"password-setting.png",
                      //  @"share-code.png",
                        @"remove-dorlock.png"
                        ];
        arrayTitle = @[@"User Name : ",
                       @"Device Name : ",
                       //@"Wifi Alert Notifications",
                       @"History Log",
                       @"Auto lock time : ",
                      // @"RFID Tag Management",
                       @"Password : ",
                      // @"Share Code",
                       @"Remove : "];
        arraySubTitle = @[userName ,deviceName, @"", autoLockTime, password, deviceName];
    }
    else if (quickLockDeviceManage.lockDevice.lock_Type == DOORLOCK || (quickLockDeviceManage.lockDevice.lock_Type == DEADBOLT))
    {
        NSString *strDeviceImage;

        if(quickLockDeviceManage.lockDevice.lock_Type == DOORLOCK)
        {
            _lblTitle.text = @"Quicklock Doorlock";
            strDeviceImage = @"ic_doorlock-white.png";
        }
        else
        {
            _lblTitle.text = @"Quicklock Deadbolt";
            strDeviceImage = @"ic_doorlock-white.png";

        }
        
        arrayImages = @[@"user-icon.png",
                        strDeviceImage,
                        //@"wifi.png",
                        @"history-log.png",
                        @"tag-icon.png",
                        @"password-setting.png",
                        @"share-code.png",
                        @"remove-dorlock.png"
                        ];
        arrayTitle = @[@"User Name : ",
                       @"Device Name : ",
                       //@"Wifi Alert Notifications",
                       @"History Log",
                       @"RFID Tag Management",
                       @"Password : ",
                       @"Share Code",
                       @"Remove : "];
        arraySubTitle = @[userName, deviceName,@"", @"", password, @"", deviceName];
    }
    else  // all Other Boxes
    {
        NSString *strDeviceImage, *strDeviceName;
        
        if(quickLockDeviceManage.lockDevice.lock_Type == GUNBOX)
        {
            _lblTitle.text = @"Gunbox 2.0";
            strDeviceImage = @"ic_gunbox_name_settings.png";
            strDeviceName = @"Device Name : ";
        }
        else if (quickLockDeviceManage.lockDevice.lock_Type == QUICKBOX)
        {
            _lblTitle.text = @"Gunbox Echo";
            strDeviceImage = @"ic_gunbox_name_settings.png";
            strDeviceName = @"Device Name : ";
        }
//        else if (quickLockDeviceManage.lockDevice.lock_Type == MEDICINEBOX)
//        {
//            _lblTitle.text = @"MEDICINEBOX";
//            //strDeviceImage = @"ic_gunbox_name_settings.png";
//            strDeviceName = @"Medicinebox Name";
//        }
        else if (quickLockDeviceManage.lockDevice.lock_Type == TALLGUNBOX)
        {
            _lblTitle.text = @"Gunbox SK-1";
            strDeviceImage = @"Riflebox-icon.png";
            strDeviceName = @"Device Name : ";
        }
        
        if (quickLockDeviceManage.lockDevice.zwaveState == -1)
        {
            if(quickLockDeviceManage.lockDevice.lock_Type == GUNBOX || quickLockDeviceManage.lockDevice.lock_Type == QUICKBOX)
            {
                arrayImages = @[@"user-icon.png",
                                strDeviceImage,
                                //@"wifi.png",
                                @"history-log.png",
                                //@"ic_sure_set.png",
                                @"ic_led_brightness.png",
                                @"tag-icon.png",
                                @"password-setting.png",
                                @"share-code.png",
                                @"alarm.png",
                                @"fingure.png",
                                @"ic_lock_closed_key.png",
                                @"remove-dorlock.png"
                                ];
                arrayTitle = @[@"User Name : ",
                               strDeviceName,
                               //@"Wifi Alert Notifications",
                               @"History Log",
                               //@"Sure Set : ",
                               @"LED Time Out : ",
                               @"RFID Tag Management",
                               @"Password : ",
                               @"Share Code",
                               @"Alarm",
                               @"Fingerprint",
                               @"Key code Management",
                               @"Remove : "];
                arraySubTitle = @[userName, deviceName, @"", LEDTime, @"", password, @"", @"", @"", @"", deviceName];
            }
            else
            {
                arrayImages = @[@"user-icon.png",
                                strDeviceImage,
                                //@"wifi.png",
                                @"history-log.png",
                                //@"ic_sure_set.png",
                                @"ic_led_brightness.png",
                                @"tag-icon.png",
                                @"password-setting.png",
                                @"share-code.png",
                                @"alarm.png",
                                @"fingure.png",
                                @"remove-dorlock.png"
                                ];
                arrayTitle = @[@"User Name : ",
                               strDeviceName,
                               //@"Wifi Alert Notifications",
                               @"History Log",
                               //@"Sure Set : ",
                               @"LED Time Out : ",
                               @"RFID Tag Management",
                               @"Password : ",
                               @"Share Code",
                               @"Alarm",
                               @"Fingerprint",
                               @"Remove : "];
                arraySubTitle = @[userName, deviceName, @"", LEDTime, @"", password, @"", @"", @"", deviceName]; // alarmState  // sureSet
            }
        }
        else
        {
            if(quickLockDeviceManage.lockDevice.lock_Type == GUNBOX || quickLockDeviceManage.lockDevice.lock_Type == QUICKBOX)
            {
                arrayImages = @[@"user-icon.png",
                                strDeviceImage,
                                @"zwave.png",
                                //@"wifi.png",
                                @"history-log.png",
                                //@"ic_sure_set.png",
                                @"ic_led_brightness.png",
                                @"tag-icon.png",
                                @"password-setting.png",
                                @"share-code.png",
                                @"alarm.png",
                                @"fingure.png",
                                @"ic_lock_closed_key.png",
                                @"remove-dorlock.png"
                                ];
                arrayTitle = @[@"User Name : ",
                               strDeviceName,
                               @"Z-Wave",
                               //@"Wifi Alert Notifications",
                               @"History Log",
                               //@"Sure Set : ",
                               @"LED Time Out : ",
                               @"RFID Tag Management",
                               @"Password : ",
                               @"Share Code",
                               @"Alarm",
                               @"Fingerprint",
                               @"Key code Management",
                               @"Remove : "];
                arraySubTitle = @[userName, deviceName, @"",@"", LEDTime, @"", password, @"", @"", @"", @"", deviceName]; // alarmState  // sureSet
            }
            else
            {
                arrayImages = @[@"user-icon.png",
                                strDeviceImage,
                                @"zwave.png",
                                //@"wifi.png",
                                @"history-log.png",
                                //@"ic_sure_set.png",
                                @"ic_led_brightness.png",
                                @"tag-icon.png",
                                @"password-setting.png",
                                @"share-code.png",
                                @"alarm.png",
                                @"fingure.png",
                                @"remove-dorlock.png"
                                ];
                arrayTitle = @[@"User Name : ",
                               strDeviceName,
                               @"Z-Wave",
                               //@"Wifi Alert Notifications",
                               @"History Log",
                               //@"Sure Set : ",
                               @"LED Time Out : ",
                               @"RFID Tag Management",
                               @"Password : ",
                               @"Share Code",
                               @"Alarm",
                               @"Fingerprint",
                               @"Remove : "];
                arraySubTitle = @[userName, deviceName, @"",@"", LEDTime, @"", password, @"", @"", @"", deviceName]; // alarmState  // sureSet
            }
        }
    }
    
    [tableViewSet reloadData];
    
    [labelFirmwareVersion setText:[NSString stringWithFormat:@"Firmware version : %@",firmwareVersion]];
}

- (void)didReceiveMemoryWarning
{
        [super didReceiveMemoryWarning];
        // Dispose of any resources that can be recreated.
}
- (IBAction)btnBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}

#pragma mark - UITableViewDelegate & UITableViewDataSource Method
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
        return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrayTitle.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cell";
    
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1
                                                   reuseIdentifier:cellIdentifier];
    UIImageView *img = [[UIImageView alloc]initWithFrame:CGRectMake(30, 9, 25, 25)];
    img.image = [UIImage imageNamed:arrayImages[indexPath.row]];
    [img setContentMode:UIViewContentModeScaleAspectFit];
    [cell.contentView addSubview:img];
    
    cell.backgroundColor = [UIColor clearColor];
    [cell.textLabel setText: [NSString stringWithFormat:@"\t\t%@%@",arrayTitle[indexPath.row],arraySubTitle[indexPath.row]]];
    [cell.textLabel setTextColor:[UIColor blackColor]];
    
    DottedView *view = [[DottedView alloc] initWithFrame:CGRectMake(20, -1, self.view.frame.size.width-40, 15)];
    view.backgroundColor = [UIColor clearColor];
    [cell addSubview:view];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSLog(@"indexpath : %d",(int)indexPath.row);
    
    if(quickLockDeviceManage.lockDevice.lock_Type == PADLOCK)
    {
        if (indexPath.row == 0)
        {
            // User Name
            inputUserName *InputUserName = [[inputUserName alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
            NSString *strName = [[NSUserDefaults standardUserDefaults] valueForKey:@"kUserName"];
            [InputUserName setValue:strName forKeyPath:@"oldUserName"];
            [self.view addSubview:InputUserName];
        }
        else if (indexPath.row == 1)
        {
            // Lock Name
            modifyNameView = [[ModifyName alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
            modifyNameView.delegate = self;
            NSString *strName = quickLockDeviceManage.lockDevice.lock_Name;
            [modifyNameView setValue:strName forKeyPath:@"oldName"];
            [self.view addSubview:modifyNameView];
        }
//        else if (indexPath.row == 2)
//        {
//            //Wi-Fi
//            WifiSetup *wifiSetup = [self.storyboard instantiateViewControllerWithIdentifier:@"WifiSetup"];
//            [self.navigationController pushViewController:wifiSetup animated:NO];
//        }
        else if (indexPath.row == 2)
        {
            //open history log
            ShowHistoryLogViewController *showHistoryLogVC = [self.storyboard instantiateViewControllerWithIdentifier:@"showHistoryLogVC"];
            [self.navigationController pushViewController:showHistoryLogVC animated:NO];
        }
        else if (indexPath.row == 3)
        {
            //Modify automatic locking time
            modifyAutoLockTimeView = [[ModifyAutoLockTime alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
            modifyAutoLockTimeView.delegate = self;
            [modifyAutoLockTimeView setValue:[NSNumber numberWithInt:quickLockDeviceManage.lockDevice.lock_AutoLockTime] forKey:@"selectIndex"];
            [self.view addSubview:modifyAutoLockTimeView];
        }
        else if (indexPath.row == 4)
        {
            //Modify the name, set the name to enter the time for the old name
            // RFID Tags
            RFIDTags *rfid = [self.storyboard instantiateViewControllerWithIdentifier:@"RFIDTags"];
            [self.navigationController pushViewController:rfid animated:NO];
        }
        else if (indexPath.row == 5)
        {
            change = [self.storyboard instantiateViewControllerWithIdentifier:@"ChangePassword"];
            [self.navigationController pushViewController:change animated:NO];
        }
        else if (indexPath.row == 6)
        {
            // Share Code
            ShareCode *share = [self.storyboard instantiateViewControllerWithIdentifier:@"ShareCode"];
            [self.navigationController pushViewController:share animated:NO];
        }
        else if (indexPath.row == 7)
        {
            // remove lock
            [self DeleteDevice];
            [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"passwordCall"];
        }
    }
    else if(quickLockDeviceManage.lockDevice.lock_Type == RFWRITER)
    {
        if (indexPath.row == 0)
        {
            // User Name
            inputUserName *InputUserName = [[inputUserName alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
            NSString *strName = [[NSUserDefaults standardUserDefaults] valueForKey:@"kUserName"];
            [InputUserName setValue:strName forKeyPath:@"oldUserName"];
            [self.view addSubview:InputUserName];
        }
        else if (indexPath.row == 1)
        {
            // Lock Name
            modifyNameView = [[ModifyName alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
            modifyNameView.delegate = self;
            NSString *strName = quickLockDeviceManage.lockDevice.lock_Name;
            [modifyNameView setValue:strName forKeyPath:@"oldName"];
            [self.view addSubview:modifyNameView];
        }
        //        else if (indexPath.row == 2)
        //        {
        //            //Wi-Fi
        //            WifiSetup *wifiSetup = [self.storyboard instantiateViewControllerWithIdentifier:@"WifiSetup"];
        //            [self.navigationController pushViewController:wifiSetup animated:NO];
        //        }
        else if (indexPath.row == 2)
        {
            //open history log
            ShowHistoryLogViewController *showHistoryLogVC = [self.storyboard instantiateViewControllerWithIdentifier:@"showHistoryLogVC"];
            [self.navigationController pushViewController:showHistoryLogVC animated:NO];
        }
        else if (indexPath.row == 3)
        {
            //Modify automatic locking time
            modifyAutoLockTimeView = [[ModifyAutoLockTime alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
            modifyAutoLockTimeView.delegate = self;
            [modifyAutoLockTimeView setValue:[NSNumber numberWithInt:quickLockDeviceManage.lockDevice.lock_AutoLockTime] forKey:@"selectIndex"];
            [self.view addSubview:modifyAutoLockTimeView];
        }
//        else if (indexPath.row == 4)
//        {
//            //Modify the name, set the name to enter the time for the old name
//            // RFID Tags
//            RFIDTags *rfid = [self.storyboard instantiateViewControllerWithIdentifier:@"RFIDTags"];
//            [self.navigationController pushViewController:rfid animated:NO];
//        }
        else if (indexPath.row == 4)
        {
            change = [self.storyboard instantiateViewControllerWithIdentifier:@"ChangePassword"];
            [self.navigationController pushViewController:change animated:NO];
        }
//        else if (indexPath.row == 6)
//        {
//            // Share Code
//            ShareCode *share = [self.storyboard instantiateViewControllerWithIdentifier:@"ShareCode"];
//            [self.navigationController pushViewController:share animated:NO];
//        }
        else if (indexPath.row == 5)
        {
            // remove lock
            [self DeleteDevice];
            [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"passwordCall"];
        }
    }
    else if(quickLockDeviceManage.lockDevice.lock_Type == DOORLOCK)
    {
        if (indexPath.row == 0)
        {
            // User Name
            inputUserName *InputUserName = [[inputUserName alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
            NSString *strName = [[NSUserDefaults standardUserDefaults] valueForKey:@"kUserName"];
            [InputUserName setValue:strName forKeyPath:@"oldUserName"];
            [self.view addSubview:InputUserName];
        }
        else if (indexPath.row == 1)
        {
            // Lock Name
            modifyNameView = [[ModifyName alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
            modifyNameView.delegate = self;
            NSString *strName = quickLockDeviceManage.lockDevice.lock_Name;
            [modifyNameView setValue:strName forKeyPath:@"oldName"];
            [self.view addSubview:modifyNameView];
        }
//        else if (indexPath.row == 2)
//        {
//            //Wi-Fi
//            WifiSetup *wifiSetup = [self.storyboard instantiateViewControllerWithIdentifier:@"WifiSetup"];
//            [self.navigationController pushViewController:wifiSetup animated:NO];
//        }
        else if (indexPath.row == 2)
        {
            //open history log
            ShowHistoryLogViewController *showHistoryLogVC = [self.storyboard instantiateViewControllerWithIdentifier:@"showHistoryLogVC"];
            [self.navigationController pushViewController:showHistoryLogVC animated:NO];
        }
        else if (indexPath.row == 3)
        {
            //Modify the name, set the name to enter the time for the old name
            // RFID Tags
            RFIDTags *rfid = [self.storyboard instantiateViewControllerWithIdentifier:@"RFIDTags"];
            [self.navigationController pushViewController:rfid animated:NO];
        }
        else if (indexPath.row == 4)
        {
            change = [self.storyboard instantiateViewControllerWithIdentifier:@"ChangePassword"];
            [self.navigationController pushViewController:change animated:NO];
        }
        else if (indexPath.row == 5)
        {
            // Share Code
            ShareCode *share = [self.storyboard instantiateViewControllerWithIdentifier:@"ShareCode"];
            [self.navigationController pushViewController:share animated:NO];
        }
        else if (indexPath.row == 6)
        {
            // remove lock
            [self DeleteDevice];
            [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"passwordCall"];
        }
    }
    else // all Boxes
    {
        if(quickLockDeviceManage.lockDevice.lock_Type == GUNBOX || quickLockDeviceManage.lockDevice.lock_Type == QUICKBOX)
        {
            if (quickLockDeviceManage.lockDevice.zwaveState == -1)
            {
                if (indexPath.row == 0)
                {
                    // User Name
                    inputUserName *InputUserName = [[inputUserName alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
                    NSString *strName = [[NSUserDefaults standardUserDefaults] valueForKey:@"kUserName"];
                    [InputUserName setValue:strName forKeyPath:@"oldUserName"];
                    [self.view addSubview:InputUserName];
                }
                else if (indexPath.row == 1)
                {
                    // GunBox name
                    modifyNameView = [[ModifyName alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
                    modifyNameView.delegate = self;
                    NSString *strName = quickLockDeviceManage.lockDevice.lock_Name;
                    
                    [modifyNameView setValue:strName forKeyPath:@"oldName"];
                    [self.view addSubview:modifyNameView];
                }
                //            else if (indexPath.row == 2)
                //            {
                //                //Wi-Fi
                //                WifiSetup *wifiSetup = [self.storyboard instantiateViewControllerWithIdentifier:@"WifiSetup"];
                //                [self.navigationController pushViewController:wifiSetup animated:NO];
                //            }
                else if (indexPath.row == 2)
                {
                    // open history log
                    ShowHistoryLogViewController *showHistoryLogVC = [self.storyboard instantiateViewControllerWithIdentifier:@"showHistoryLogVC"];
                    [self.navigationController pushViewController:showHistoryLogVC animated:NO];
                }
                else if (indexPath.row == 3)
                {
                    quickLockDeviceManage.lockDevice.isLED = 1;
                    quickLockDeviceManage.lockDevice.currentLEDState = [NSString stringWithFormat:@"%ds", quickLockDeviceManage.lockDevice.box_LED_Time];
                    modifyAutoLockTimeView = [[ModifyAutoLockTime alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
                    modifyAutoLockTimeView.delegate = self;
                    [modifyAutoLockTimeView setValue:[NSNumber numberWithInt:quickLockDeviceManage.lockDevice.lock_AutoLockTime] forKey:@"selectIndex"];
                    [self.view addSubview:modifyAutoLockTimeView];
                }
                else if (indexPath.row == 4)
                {
                    // RFID Tags
                    RFIDTags *rfid =[self.storyboard instantiateViewControllerWithIdentifier:@"RFIDTags"];
                    [self.navigationController pushViewController:rfid animated:NO];
                }
                else if (indexPath.row == 5)
                {
                    // change Password
                    change = [self.storyboard instantiateViewControllerWithIdentifier:@"ChangePassword"];
                    [self.navigationController pushViewController:change animated:NO];
                }
                else if (indexPath.row == 6)
                {
                    // Share Code
                    ShareCode *share = [self.storyboard instantiateViewControllerWithIdentifier:@"ShareCode"];
                    [self.navigationController pushViewController:share animated:NO];
                }
                else if (indexPath.row == 7)
                {
                    // Alarm
                    Alarm *alarm = [self.storyboard instantiateViewControllerWithIdentifier:@"Alarm"];
                    [self.navigationController pushViewController:alarm animated:NO];
                }
                else if (indexPath.row == 8)
                {
                    // Fingerprint
                    Fingerprint *fingerprint = [self.storyboard instantiateViewControllerWithIdentifier:@"Fingerprint"];
                    [self.navigationController pushViewController:fingerprint animated:NO];
                }
                else if (indexPath.row == 9)
                {
                    // key code
                    KeyCode *keyCode = [self.storyboard instantiateViewControllerWithIdentifier:@"KeyCode"];
                    [self.navigationController pushViewController:keyCode animated:NO];
                }
                else if (indexPath.row == 10)
                {
                    // remove lock
                    [self DeleteDevice];
                    [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"passwordCall"];
                }
            }
            else
            {
                if (indexPath.row == 0)
                {
                    // User Name
                    inputUserName *InputUserName = [[inputUserName alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
                    NSString *strName = [[NSUserDefaults standardUserDefaults] valueForKey:@"kUserName"];
                    [InputUserName setValue:strName forKeyPath:@"oldUserName"];
                    [self.view addSubview:InputUserName];
                }
                else if (indexPath.row == 1)
                {
                    // GunBox name
                    modifyNameView = [[ModifyName alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
                    modifyNameView.delegate = self;
                    NSString *strName = quickLockDeviceManage.lockDevice.lock_Name;
                    
                    [modifyNameView setValue:strName forKeyPath:@"oldName"];
                    [self.view addSubview:modifyNameView];
                }
                else if (indexPath.row == 2)
                {
                    //Z-wave
                    Zwave *zwave = [self.storyboard instantiateViewControllerWithIdentifier:@"Zwave"];
                    [self.navigationController pushViewController:zwave animated:NO];
                }
                //            else if (indexPath.row == 3)
                //            {
                //                //Wi-Fi
                //                WifiSetup *wifiSetup = [self.storyboard instantiateViewControllerWithIdentifier:@"WifiSetup"];
                //                [self.navigationController pushViewController:wifiSetup animated:NO];
                //            }
                else if (indexPath.row == 3)
                {
                    // open history log
                    ShowHistoryLogViewController *showHistoryLogVC = [self.storyboard instantiateViewControllerWithIdentifier:@"showHistoryLogVC"];
                    [self.navigationController pushViewController:showHistoryLogVC animated:NO];
                }
                else if (indexPath.row == 4)
                {
                    quickLockDeviceManage.lockDevice.isLED = 1;
                    quickLockDeviceManage.lockDevice.currentLEDState = [NSString stringWithFormat:@"%ds", quickLockDeviceManage.lockDevice.box_LED_Time];
                    modifyAutoLockTimeView = [[ModifyAutoLockTime alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
                    modifyAutoLockTimeView.delegate = self;
                    [modifyAutoLockTimeView setValue:[NSNumber numberWithInt:quickLockDeviceManage.lockDevice.lock_AutoLockTime] forKey:@"selectIndex"];
                    [self.view addSubview:modifyAutoLockTimeView];
                }
                else if (indexPath.row == 5)
                {
                    // RFID Tags
                    RFIDTags *rfid =[self.storyboard instantiateViewControllerWithIdentifier:@"RFIDTags"];
                    [self.navigationController pushViewController:rfid animated:NO];
                }
                else if (indexPath.row == 6)
                {
                    // change Password
                    change = [self.storyboard instantiateViewControllerWithIdentifier:@"ChangePassword"];
                    [self.navigationController pushViewController:change animated:NO];
                }
                else if (indexPath.row == 7)
                {
                    // Share Code
                    ShareCode *share = [self.storyboard instantiateViewControllerWithIdentifier:@"ShareCode"];
                    [self.navigationController pushViewController:share animated:NO];
                }
                else if (indexPath.row == 8)
                {
                    // Alarm
                    Alarm *alarm = [self.storyboard instantiateViewControllerWithIdentifier:@"Alarm"];
                    [self.navigationController pushViewController:alarm animated:NO];
                }
                else if (indexPath.row == 9)
                {
                    // Fingerprint
                    Fingerprint *fingerprint = [self.storyboard instantiateViewControllerWithIdentifier:@"Fingerprint"];
                    [self.navigationController pushViewController:fingerprint animated:NO];
                }
                else if (indexPath.row == 10)
                {
                    // key code
                    KeyCode *keyCode = [self.storyboard instantiateViewControllerWithIdentifier:@"KeyCode"];
                    [self.navigationController pushViewController:keyCode animated:NO];
                }
                else if (indexPath.row == 11)
                {
                    // remove lock
                    [self DeleteDevice];
                    [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"passwordCall"];
                }

            }
        }
        else
        {
            if (quickLockDeviceManage.lockDevice.zwaveState == -1)
            {
                if (indexPath.row == 0)
                {
                    // User Name
                    inputUserName *InputUserName = [[inputUserName alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
                    NSString *strName = [[NSUserDefaults standardUserDefaults] valueForKey:@"kUserName"];
                    [InputUserName setValue:strName forKeyPath:@"oldUserName"];
                    [self.view addSubview:InputUserName];
                }
                else if (indexPath.row == 1)
                {
                    // GunBox name
                    modifyNameView = [[ModifyName alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
                    modifyNameView.delegate = self;
                    NSString *strName = quickLockDeviceManage.lockDevice.lock_Name;
                    
                    [modifyNameView setValue:strName forKeyPath:@"oldName"];
                    [self.view addSubview:modifyNameView];
                }
                //            else if (indexPath.row == 2)
                //            {
                //                //Wi-Fi
                //                WifiSetup *wifiSetup = [self.storyboard instantiateViewControllerWithIdentifier:@"WifiSetup"];
                //                [self.navigationController pushViewController:wifiSetup animated:NO];
                //            }
                else if (indexPath.row == 2)
                {
                    // open history log
                    ShowHistoryLogViewController *showHistoryLogVC = [self.storyboard instantiateViewControllerWithIdentifier:@"showHistoryLogVC"];
                    [self.navigationController pushViewController:showHistoryLogVC animated:NO];
                }
                else if (indexPath.row == 3)
                {
                    quickLockDeviceManage.lockDevice.isLED = 1;
                    quickLockDeviceManage.lockDevice.currentLEDState = [NSString stringWithFormat:@"%ds", quickLockDeviceManage.lockDevice.box_LED_Time];
                    modifyAutoLockTimeView = [[ModifyAutoLockTime alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
                    modifyAutoLockTimeView.delegate = self;
                    [modifyAutoLockTimeView setValue:[NSNumber numberWithInt:quickLockDeviceManage.lockDevice.lock_AutoLockTime] forKey:@"selectIndex"];
                    [self.view addSubview:modifyAutoLockTimeView];
                }
                else if (indexPath.row == 4)
                {
                    // RFID Tags
                    RFIDTags *rfid =[self.storyboard instantiateViewControllerWithIdentifier:@"RFIDTags"];
                    [self.navigationController pushViewController:rfid animated:NO];
                }
                else if (indexPath.row == 5)
                {
                    // change Password
                    change = [self.storyboard instantiateViewControllerWithIdentifier:@"ChangePassword"];
                    [self.navigationController pushViewController:change animated:NO];
                }
                else if (indexPath.row == 6)
                {
                    // Share Code
                    ShareCode *share = [self.storyboard instantiateViewControllerWithIdentifier:@"ShareCode"];
                    [self.navigationController pushViewController:share animated:NO];
                }
                else if (indexPath.row == 7)
                {
                    // Alarm
                    Alarm *alarm = [self.storyboard instantiateViewControllerWithIdentifier:@"Alarm"];
                    [self.navigationController pushViewController:alarm animated:NO];
                }
                else if (indexPath.row == 8)
                {
                    // Fingerprint
                    Fingerprint *fingerprint = [self.storyboard instantiateViewControllerWithIdentifier:@"Fingerprint"];
                    [self.navigationController pushViewController:fingerprint animated:NO];
                }
                else if (indexPath.row == 9)
                {
                    // remove lock
                    [self DeleteDevice];
                    [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"passwordCall"];
                }
            }
            else
            {
                if (indexPath.row == 0)
                {
                    // User Name
                    inputUserName *InputUserName = [[inputUserName alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
                    NSString *strName = [[NSUserDefaults standardUserDefaults] valueForKey:@"kUserName"];
                    [InputUserName setValue:strName forKeyPath:@"oldUserName"];
                    [self.view addSubview:InputUserName];
                }
                else if (indexPath.row == 1)
                {
                    // GunBox name
                    modifyNameView = [[ModifyName alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
                    modifyNameView.delegate = self;
                    NSString *strName = quickLockDeviceManage.lockDevice.lock_Name;
                    
                    [modifyNameView setValue:strName forKeyPath:@"oldName"];
                    [self.view addSubview:modifyNameView];
                }
                else if (indexPath.row == 2)
                {
                    //Z-wave
                    Zwave *zwave = [self.storyboard instantiateViewControllerWithIdentifier:@"Zwave"];
                    [self.navigationController pushViewController:zwave animated:NO];
                }
                //            else if (indexPath.row == 3)
                //            {
                //                //Wi-Fi
                //                WifiSetup *wifiSetup = [self.storyboard instantiateViewControllerWithIdentifier:@"WifiSetup"];
                //                [self.navigationController pushViewController:wifiSetup animated:NO];
                //            }
                else if (indexPath.row == 3)
                {
                    // open history log
                    ShowHistoryLogViewController *showHistoryLogVC = [self.storyboard instantiateViewControllerWithIdentifier:@"showHistoryLogVC"];
                    [self.navigationController pushViewController:showHistoryLogVC animated:NO];
                }
                else if (indexPath.row == 4)
                {
                    quickLockDeviceManage.lockDevice.isLED = 1;
                    quickLockDeviceManage.lockDevice.currentLEDState = [NSString stringWithFormat:@"%ds", quickLockDeviceManage.lockDevice.box_LED_Time];
                    modifyAutoLockTimeView = [[ModifyAutoLockTime alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
                    modifyAutoLockTimeView.delegate = self;
                    [modifyAutoLockTimeView setValue:[NSNumber numberWithInt:quickLockDeviceManage.lockDevice.lock_AutoLockTime] forKey:@"selectIndex"];
                    [self.view addSubview:modifyAutoLockTimeView];
                }
                else if (indexPath.row == 5)
                {
                    // RFID Tags
                    RFIDTags *rfid =[self.storyboard instantiateViewControllerWithIdentifier:@"RFIDTags"];
                    [self.navigationController pushViewController:rfid animated:NO];
                }
                else if (indexPath.row == 6)
                {
                    // change Password
                    change = [self.storyboard instantiateViewControllerWithIdentifier:@"ChangePassword"];
                    [self.navigationController pushViewController:change animated:NO];
                }
                else if (indexPath.row == 7)
                {
                    // Share Code
                    ShareCode *share = [self.storyboard instantiateViewControllerWithIdentifier:@"ShareCode"];
                    [self.navigationController pushViewController:share animated:NO];
                }
                else if (indexPath.row == 8)
                {
                    // Alarm
                    Alarm *alarm = [self.storyboard instantiateViewControllerWithIdentifier:@"Alarm"];
                    [self.navigationController pushViewController:alarm animated:NO];
                }
                else if (indexPath.row == 9)
                {
                    // Fingerprint
                    Fingerprint *fingerprint = [self.storyboard instantiateViewControllerWithIdentifier:@"Fingerprint"];
                    [self.navigationController pushViewController:fingerprint animated:NO];
                }
                else if (indexPath.row == 10)
                {
                    // remove lock
                    [self DeleteDevice];
                    [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"passwordCall"];
                }
            }
        }
    }
}

- (void)ModifyUserName:(NSString *)strModifyName
{
        alertModifyUserName = [[UIAlertView alloc] initWithTitle:@"Please set your user name"
                                                         message:@"Only support digital,letter,space \nand ' , . - _ and can't be null"
                                                        delegate:self
                                               cancelButtonTitle:@"Cancel"
                                               otherButtonTitles:@"OK", nil];
        [alertModifyUserName setAlertViewStyle:UIAlertViewStylePlainTextInput];
        [alertModifyUserName textFieldAtIndex:0].delegate = self;
        [alertModifyUserName textFieldAtIndex:0].text = strModifyName;
        [[alertModifyUserName textFieldAtIndex:0] setKeyboardType:UIKeyboardTypeASCIICapable];
        [alertModifyUserName show];
}

- (void)DeleteDevice
{
        alertDeleteDevice = [[UIAlertView alloc] initWithTitle:@"Warning!"
                                                       message:@"You sure you want to delete it?"
                                                      delegate:self
                                             cancelButtonTitle:@"Cancel"
                                             otherButtonTitles:@"OK", nil];
        [alertDeleteDevice show];
}

// Limit the input box can only enter UserName Range allowed characters and a maximum length of 14
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if ([string isEqualToString:@"\n"] || [string isEqualToString:@""])
    {
        return YES;
    }
    else if (textField.text.length > 13)
    {
        return NO;
    }
    else
    {
        return [kUserNameRange rangeOfString:string].location != NSNotFound;
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        return;
    }
    
    if (alertView == alertModifyUserName)
    {
        // Modify username, if modification is successful, immediately sent to the hardware.
        NSString *username = [alertView textFieldAtIndex:0].text;
        
        // Voice input prevent all of a sudden more than 14 characters
        if (username.length > 0)
        {
            if (username.length > 14)
            {
                username = [username substringToIndex:14];
            }
            for (int i = 0; i < username.length; i++)
            {
                if ([kUserNameRange rangeOfString:[username substringWithRange:NSMakeRange(i, 1)]].location == NSNotFound)
                {
                    [self ModifyUserName:@""];
                    return;
                }
            }
            [[NSUserDefaults standardUserDefaults] setValue:username forKey:@"kUserName"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [quickLockDeviceManage setUserNameToLock];
            [self viewDidAppear:YES];
        }
        else
        {
            [self ModifyUserName:@""];
        }
    }
    else if (alertView == alertDeleteDevice)
    {
        //DeleteDevice, Delete database information, take the initiative to disconnect
        [dbAccess DeleteLockDevice:quickLockDeviceManage.lockDevice.lock_ID];
        [quickLockDeviceManage initiativeDisConnect];
    }
}

// Returns the modified name.
- (void)ReturnNewName:(NSString *)newName
{
    quickLockDeviceManage.lockDevice.lock_Name = newName;
    [modifyNameView removeFromSuperview];
    
    [dbAccess UpdateLockDevice:quickLockDeviceManage.lockDevice WithLockID:quickLockDeviceManage.lockDevice.lock_ID];
    [self viewDidAppear:YES];
}

// Change the password of return
- (void)returnPassword:(NSString *)password
{
    if (inputPasswordView)
    {
        [inputPasswordView RemoveAllView];
    }
    
    //Send to hardware, change password
    [quickLockDeviceManage changePasssword:password];
    [dbAccess UpdateLockDevice:quickLockDeviceManage.lockDevice WithLockID:quickLockDeviceManage.lockDevice.lock_ID];
}

// Return the modified automatic locking time
- (void)returnAutoLockTime:(int)time
{
    if (time == 0)
    {
        return;
    }
    
    quickLockDeviceManage.lockDevice.lock_AutoLockTime = time;
    //Send to hardware, modification automatic locking time
    [quickLockDeviceManage setAutoLockTime:time];
    
    [dbAccess UpdateLockDevice:quickLockDeviceManage.lockDevice WithLockID:quickLockDeviceManage.lockDevice.lock_ID];
    [self viewDidAppear:YES];
}

- (void)returnLEDTime:(int)time
{
    quickLockDeviceManage.lockDevice.box_LED_Time = time;
    //Send to hardware, modification LED time
    [quickLockDeviceManage setLEDTime:time];
    
    [dbAccess UpdateBoxLedTime:quickLockDeviceManage.lockDevice WithLockID:quickLockDeviceManage.lockDevice.lock_ID];
    [self viewDidAppear:YES];
}

- (void)returnSureSet
{
    quickLockDeviceManage.lockDevice.sure_Set_Percent = slidevalue;
    [quickLockDeviceManage setSureSet:slidevalue];
    [dbAccess UpdateBoxSureSet:quickLockDeviceManage.lockDevice WithLockID:quickLockDeviceManage.lockDevice.lock_ID];
    [self viewDidAppear:YES];
}


@end
