//
//  DashedLine.m
//  GraphDemo
//
//  Created by Arif on 11/19/14.
//  Copyright (c) 2014 MagnetoItSolutions. All rights reserved.
//

#import "DashedLineView.h"

@implementation DashedLineView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(context, 2.0);
    CGFloat dashArray[] = {6,2,6,2};
    CGContextSetLineDash(context, 3, dashArray, 4);
    UIColor *color = [UIColor clearColor];//[UIColor colorWithRed:230.0/255.0 green:73.0/255.0 blue:68.0/255.0 alpha:0.9];
    CGContextSetStrokeColorWithColor(context, color.CGColor);
    CGContextMoveToPoint(context, 5, rect.size.height/2);
    CGContextAddLineToPoint(context, rect.size.width - 5 , rect.size.height/2);
    CGContextStrokePath(context);
}
@end
