//
//  ModifyName.h
//  QuickLock
//
//  Created by administrator on 14-12-28.
//  Copyright (c) 2014年 Bge. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ModifyNameDelegate <NSObject>

- (void)ReturnNewName:(NSString *)newName;

@end

@interface ModifyName : UIView
{
        NSString *oldName;
}

@property (nonatomic, assign) id<ModifyNameDelegate> delegate;

@end
