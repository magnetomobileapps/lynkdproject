//
//  RFIDTags.h
//  QuickLock
//
//  Created by Ankit on 27/12/15.
//  Copyright (c) 2015 Bge. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LockDevice.h"

@interface RFIDTags : UIViewController
{
    NSMutableArray *arrayHistoryLog, *arrDate, *arrName, *arrTagId, *arrLockType, *arrLockAddress;
    NSMutableDictionary *dictForHistory;
    NSArray *arrayLock;

}
@property (nonatomic, retain) LockDevice *lockDevice;

@end
