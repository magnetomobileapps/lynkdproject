//
//  DBClass.m
//  PEDO6
//
//  Created by 中易腾达（常州分公司） on 12-4-13.
//  Copyright (c) 2012年 中易腾达. All rights reserved.
//

#import "DBClass.h"

@implementation DBClass

//  Open Database
- (BOOL)OpenDatabase:(NSString*) dbName
{
    NSArray *documentsPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *databaseFilePath =[[documentsPaths objectAtIndex:0] stringByAppendingPathComponent:dbName];
    if(sqlite3_open([databaseFilePath UTF8String], &database) == SQLITE_OK)
    {
        NSLog(@"open sqlite db ok. Path : %@ ",databaseFilePath);
        return true;
    }
    else 
    {
        return false;
    }
}

// Execute sql statement
- (BOOL)DoSql :(const char*)strSql
{
    char *errorMsg="";
    if(sqlite3_exec(database, strSql, NULL, NULL, &errorMsg)== SQLITE_OK)
    {
        return true;
    }
    else 
    {
        NSLog(@"%@", [[NSString alloc] initWithUTF8String:errorMsg]);
        return false;
    }
}

// Execution with parameters sql statement
- (BOOL)DoSql:(const char *)strSql WithParam:(NSString *)lockName
{
    sqlite3_stmt *statement;
    
    if (sqlite3_prepare_v2(database, strSql, -1, &statement, nil) == SQLITE_OK) {
        sqlite3_bind_text(statement, 1, [lockName UTF8String], -1, NULL);
    }
    if (sqlite3_step(statement) != SQLITE_DONE) {
        NSAssert(0, @"Insert data failed!");
        sqlite3_finalize(statement);
        return NO;
    }
    
    return YES;
}

//  Get set sql statement execution
- (BOOL) Getstmt:(const char*)strSql :(sqlite3_stmt**) statement
{
    if(sqlite3_prepare_v2(database, strSql, -1, statement, nil) == SQLITE_OK)
    {
        return true;
    }
    else 
    {
        return false;
    }
}

//  Close the database
- (void) CloseDatabase
{
    sqlite3_close(database);
}
@end
