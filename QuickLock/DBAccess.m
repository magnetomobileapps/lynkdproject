//
//  DBAccess.m
//  QuickLock
//
//  Created by 王洋 on 14/12/27.
//  Copyright (c) 2014年 Bge. All rights reserved.
//

#import "DBAccess.h"

@implementation DBAccess
{
    NSMutableArray *arrayOfIMEI;
    NSString *strLockUUID;
}
@synthesize database;

// Check and initialize the database
- (BOOL)CheckDatabase
{
    database = [DBClass new];
    char *strSql;
    
    if (![database OpenDatabase:@"QuickLockDB"])
    {
        return NO;
    }
    
    // Create a device table
    strSql = "create table if not exists lock_Device (lock_ID integer primary key autoincrement, lock_Name varchar(20), lock_Type int, lock_Password varchar(20), lock_UUID varchar(50), lock_AutoUnLock int, lock_AutoLockTime int, lock_MacAddress varchar(20), share_code_user varchar(8), share_code_user_count varchar(3), share_code_time varchar(8), share_code_time_start text, share_code_time_end text, share_code_time1 text, share_code_time2 text, share_code_time_flag int, led_Time int, sure_set int, passcode varchar(4), passcode_enable int, login_type int)";
    if(![database DoSql:strSql])
    {
        return NO;
    }

    strSql = "create table if not exists RFID_TAG_TABLE (TAG_ID integer primary key autoincrement, TAG_TYPE varchar(30), TAG_IMEI varchar(30), TAG_USERNAME varchar(30), lock_Type int, lock_MacAddress varchar(50))";
    if(![database DoSql:strSql])
    {
        return NO;
    }

    return YES;
}

// Get all the device information
- (NSArray *)GetAllLockDevice
{
    NSMutableArray *arrayDevice = [NSMutableArray new];
    
    const char *strSql;
    sqlite3_stmt *statement;
    
    strSql = "select * from lock_Device";
    
    if ([database Getstmt:strSql :&statement])
    {
        while (sqlite3_step(statement) == SQLITE_ROW)
        {
            LockDevice *lockDevice = [LockDevice new];
            
            lockDevice.lock_ID = sqlite3_column_int(statement, 0);
            lockDevice.lock_Name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)];
            lockDevice.lock_Type = sqlite3_column_int(statement, 2);
            lockDevice.lock_Password = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 3)];
            lockDevice.lock_UUID = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 4)];
            lockDevice.lock_AutoUnLock = sqlite3_column_int(statement, 5);
            lockDevice.lock_AutoLockTime = sqlite3_column_int(statement, 6);
            
            lockDevice.share_Code = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 8)];
            lockDevice.share_Code_Count = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 9)];
            lockDevice.share_Code_Time = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 10)];
            NSString *strDate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 11)];
            NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss +zzzz"];
            formatter.timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
            formatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
            lockDevice.share_Code_Start_Time = [formatter dateFromString:strDate];
            lockDevice.str_share_Code_Stert_time = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 11)];
            lockDevice.str_share_Code_End_time = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 12)];
            
            
            strDate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 12)];
            lockDevice.share_Code_End_Time = [formatter dateFromString:strDate];
            lockDevice.share_Code_Time1 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 13)];
            lockDevice.share_Code_Time2 = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 14)];
            lockDevice.share_code_time_flag = sqlite3_column_int(statement, 15);
            
            lockDevice.box_LED_Time = sqlite3_column_int(statement, 16);
            lockDevice.sure_Set_Percent = sqlite3_column_int(statement, 17);
            
            lockDevice.passcode = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 18)];
            lockDevice.passcode_enable = sqlite3_column_int(statement, 19);
            
            lockDevice.is_Login_share_Code = sqlite3_column_int(statement, 20);
            
            [arrayDevice addObject:lockDevice];
        }
    }
    return arrayDevice;
}

- (NSArray *)GetRFIDTags
{
    NSMutableArray *arrayDevice = [NSMutableArray new];
    arrayOfIMEI = [NSMutableArray new];
    const char *strSql;
    sqlite3_stmt *statement;
    
    NSString *strQry = [NSString stringWithFormat:@"select * from RFID_TAG_TABLE where lock_MacAddress = '%@'", appDelegate.lock_Address];

    strSql = [strQry cStringUsingEncoding:NSUTF8StringEncoding];
        
    if ([database Getstmt:strSql:&statement])
    {
        while (sqlite3_step(statement) == SQLITE_ROW)
        {
            LockDevice *lockDevice = [LockDevice new];
            
            lockDevice.TAG_ID = sqlite3_column_int(statement, 0);
            lockDevice.TAG_TYPE = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)];
            lockDevice.TAG_IMEI = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 2)];
            lockDevice.TAG_USERNAME = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 3)];
            lockDevice.lock_Type = sqlite3_column_int(statement, 4);
            lockDevice.lock_Address = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 5)];
            [arrayDevice addObject:lockDevice];
            [arrayOfIMEI addObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 2)]];
        }
    }
    
    return arrayDevice;
}

- (NSString *)GetSingleRFIDTags
{
    NSMutableArray *arrayDevice = [NSMutableArray new];
    arrayOfIMEI = [NSMutableArray new];
    NSString *strIMEI;
    const char *strSql;
    sqlite3_stmt *statement;
    
    NSString *strQry = [NSString stringWithFormat:@"select * from RFID_TAG_TABLE where TAG_IMEI = '%@' and lock_MacAddress = '%@'",appDelegate.TAG_IMEI, appDelegate.lock_Address];
    // NSString *strQry = [NSString stringWithFormat:@"select * from RFID_TAG_TABLE where lock_MacAddress = '%@'", appDelegate.lock_Address];
    
    strSql = [strQry cStringUsingEncoding:NSUTF8StringEncoding];
    //  strSql = "select * from RFID_TAG_TABLE";
    
    if ([database Getstmt:strSql :&statement])
    {
        while (sqlite3_step(statement) == SQLITE_ROW)
        {
            LockDevice *lockDevice = [LockDevice new];
            
            lockDevice.TAG_ID = sqlite3_column_int(statement, 0);
            lockDevice.TAG_TYPE = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)];
            lockDevice.TAG_IMEI = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 2)];
            lockDevice.TAG_USERNAME = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 3)];
            lockDevice.lock_Type = sqlite3_column_int(statement, 4);
            lockDevice.lock_Address = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 5)];
            [arrayDevice addObject:lockDevice];
            strIMEI = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 2)];
        }
    }
    
    return strIMEI;
}

- (NSString *)GetSingleRFIDTagsUserName:(NSString*)TAG_IMEI
{
    NSMutableArray *arrayDevice = [NSMutableArray new];
    arrayOfIMEI = [NSMutableArray new];
    NSString *strUserName;
    const char *strSql;
    sqlite3_stmt *statement;
    
    NSString *strQry = [NSString stringWithFormat:@"select * from RFID_TAG_TABLE where TAG_IMEI = '%@' and lock_MacAddress = '%@'",TAG_IMEI, appDelegate.lock_Address];
    // NSString *strQry = [NSString stringWithFormat:@"select * from RFID_TAG_TABLE where lock_MacAddress = '%@'", appDelegate.lock_Address];
    strSql = [strQry cStringUsingEncoding:NSUTF8StringEncoding];
    //  strSql = "select * from RFID_TAG_TABLE";
    
    if ([database Getstmt:strSql :&statement])
    {
        while (sqlite3_step(statement) == SQLITE_ROW)
        {
            LockDevice *lockDevice = [LockDevice new];
            
            lockDevice.TAG_ID = sqlite3_column_int(statement, 0);
            lockDevice.TAG_TYPE = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)];
            lockDevice.TAG_IMEI = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 2)];
            lockDevice.TAG_USERNAME = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 3)];
            lockDevice.lock_Type = sqlite3_column_int(statement, 4);
            lockDevice.lock_Address = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 5)];
            [arrayDevice addObject:lockDevice];
            strUserName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 3)];
        }
    }
    return strUserName;
}


// Insert the new device information
- (int)InsertLockDevice:(LockDevice *)lockDevice
{
    int result = -1;

    const char *strSql = [[NSString stringWithFormat:@"insert into lock_Device (lock_Name, lock_Type, lock_Password, lock_UUID, lock_AutoUnLock, lock_AutoLockTime, share_code_user, share_code_user_count, share_code_time, share_code_time_start, share_code_time_end, share_code_time1, share_code_time2,share_code_time_flag, led_Time, sure_set, passcode, passcode_enable,login_type) values (?, %d, '%@', '%@', %d, %d, '%@', '%@', '%@', '%@', '%@', '%@', '%@', %d, %d, %d, '%@', %d, %d)",
                           lockDevice.lock_Type,
                           lockDevice.lock_Password,
                           lockDevice.lock_UUID,
                           lockDevice.lock_AutoUnLock,
                           lockDevice.lock_AutoLockTime,
                           lockDevice.share_Code,
                           lockDevice.share_Code_Count,
                           lockDevice.share_Code_Time,
                           (NSString*)lockDevice.share_Code_Start_Time,
                           (NSString*)lockDevice.share_Code_End_Time,
                           lockDevice.share_Code_Time1,
                           lockDevice.share_Code_Time2,
                           lockDevice.share_code_time_flag,
                           lockDevice.box_LED_Time,
                           lockDevice.sure_Set_Percent,
                           lockDevice.passcode,
                           lockDevice.passcode_enable,
                           lockDevice.is_Login_share_Code] UTF8String];
    
    NSLog(@"%s",strSql);

    if ([database DoSql:strSql WithParam:lockDevice.lock_Name]) {
        sqlite3_stmt *statement;
        strSql = "select last_insert_rowid()";
        if ([database Getstmt:strSql :&statement])
        {
            while (sqlite3_step(statement) == SQLITE_ROW)
            {
                result = sqlite3_column_int(statement, 0);
            }
        }
    }
    
//    [NSString stringWithFormat:@"%d",lockDevice.lock_Type];
    
//    [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"userId"]]
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self adduserlock:[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"userId"]] withlock_UUID:lockDevice.lock_UUID withlock_username:[[NSUserDefaults standardUserDefaults] valueForKey:@"kUserName"] withlock_address:@"123" withlock_password:lockDevice.lock_Password withlock_type:[NSString stringWithFormat:@"%d",lockDevice.lock_Type] withlock_name:lockDevice.lock_Name withlock_version:[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"APPVersion"]] withlock_autounLock:[NSString stringWithFormat:@"%d", lockDevice.lock_AutoUnLock] withlock_autolocktime:[NSString stringWithFormat:@"%d",lockDevice.lock_AutoLockTime] withled_time:[NSString stringWithFormat:@"%d",lockDevice.box_LED_Time] withsure_set:[NSString stringWithFormat:@"%d",lockDevice.sure_Set_Percent] withpasscode:lockDevice.passcode withpasscode_enable:[NSString stringWithFormat:@"%d",lockDevice.passcode_enable]];
    });
    return result;
}

// Update all the fields when connects 2nd time
- (BOOL)UpdateLockDeviceSecondConnect:(LockDevice *)lockDevice lock_UUID:(NSString *)lock_UUID
{
    NSString *strStDt = [NSString stringWithFormat:@"%@",lockDevice.share_Code_Start_Time];
    NSDateFormatter *dtFormat = [NSDateFormatter new];
    [dtFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss +zzzz"];
    NSDate *dt = [dtFormat dateFromString:strStDt];
    [dtFormat setDateFormat:@"dd MMM yyyy"];
    strStDt = [dtFormat stringFromDate:dt];
    
    NSString *strEdDt = [NSString stringWithFormat:@"%@",lockDevice.share_Code_End_Time];
    dtFormat = [NSDateFormatter new];
    [dtFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss +zzzz"];
    dt = [dtFormat dateFromString:strEdDt];
    [dtFormat setDateFormat:@"dd MMM yyyy"];
    strEdDt = [dtFormat stringFromDate:dt];

    const char *strSql = [[NSString stringWithFormat:@"update lock_Device set lock_Name = ?, lock_Type = %d, lock_Password = '%@', lock_AutoUnLock = %d, lock_AutoLockTime = %d, share_code_user = '%@', share_code_user_count = '%@', share_code_time = '%@', share_code_time_start = '%@', share_code_time_end = '%@', share_code_time1 = '%@', share_code_time2 = '%@',share_code_time_flag = %d, led_Time = %d, sure_set = %d, passcode = '%@', passcode_enable = %d where lock_UUID = '%@'",lockDevice.lock_Type, lockDevice.lock_Password, lockDevice.lock_AutoUnLock, lockDevice.lock_AutoLockTime, lockDevice.share_Code, lockDevice.share_Code_Count, lockDevice.share_Code_Time, strStDt, strEdDt, lockDevice.share_Code_Time1, lockDevice.share_Code_Time2, lockDevice.share_code_time_flag, lockDevice.box_LED_Time, lockDevice.sure_Set_Percent, lockDevice.passcode, lockDevice.passcode_enable, lock_UUID] UTF8String];
/*
//    const char *strSql = [[NSString stringWithFormat:@"update lock_Device set lock_Name = ?, lock_Type = %d, lock_Password = '%@', lock_AutoUnLock = %d, lock_AutoLockTime = %d, share_code_user = '%@', share_code_user_count = '%@', share_code_time = '%@', share_code_time_start = '%@', share_code_time_end = '%@', share_code_time1 = '%@', share_code_time2 = '%@',share_code_time_flag = %d, led_Time = %d, sure_set = %d, passcode = '%@', passcode_enable = %d where lock_UUID = '%@'",
//                           lockDevice.lock_Type,
//                           lockDevice.lock_Password,
//                           lockDevice.lock_AutoUnLock,
//                           lockDevice.lock_AutoLockTime,
//                           lockDevice.share_Code,
//                           lockDevice.share_Code_Count,
//                           lockDevice.share_Code_Time,
//                           strStDt,
//                           strEdDt,
//                           lockDevice.share_Code_Time1,
//                           lockDevice.share_Code_Time2,
//                           lockDevice.share_code_time_flag,
//                           lockDevice.box_LED_Time,
//                           lockDevice.sure_Set_Percent,
//                           lockDevice.passcode,
//                           lockDevice.passcode_enable,
//                           lock_UUID] UTF8String];
    // quickLockDeviceManage.lockDevice.lock_UUID*/
    
//    [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"userId"]]
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self updateuserlock:[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"userId"]] withlock_UUID:lockDevice.lock_UUID withlock_username:[[NSUserDefaults standardUserDefaults] valueForKey:@"kUserName"] withlock_address:@"123" withlock_password:lockDevice.lock_Password withlock_type:[NSString stringWithFormat:@"%d",lockDevice.lock_Type] withlock_name:lockDevice.lock_Name withlock_version:[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"APPVersion"]] withlock_autounLock:[NSString stringWithFormat:@"%d", lockDevice.lock_AutoUnLock] withlock_autolocktime:[NSString stringWithFormat:@"%d",lockDevice.lock_AutoLockTime] withled_time:[NSString stringWithFormat:@"%d",lockDevice.box_LED_Time] withsure_set:[NSString stringWithFormat:@"%d",lockDevice.sure_Set_Percent] withpasscode:lockDevice.passcode withpasscode_enable:[NSString stringWithFormat:@"%d",lockDevice.passcode_enable]];
    });
    NSLog(@"%s",strSql);
    return [database DoSql:strSql WithParam:lockDevice.lock_Name];
}

- (int)InsertRFIDTags:(LockDevice *)lockDevice
{
    int result = -1;
    
    const char *strSql = [[NSString stringWithFormat:@"insert into RFID_TAG_TABLE (TAG_TYPE, TAG_IMEI, TAG_USERNAME, lock_Type, lock_MacAddress) values ('%@', '%@', '%@', %d, '%@')",
                           lockDevice.TAG_TYPE,
                           lockDevice.TAG_IMEI,
                           lockDevice.TAG_USERNAME,
                           lockDevice.lock_Type,
                           appDelegate.lock_Address] UTF8String];
    
    if ([database DoSql:strSql WithParam:lockDevice.TAG_IMEI])
    {
        sqlite3_stmt *statement;
        strSql = "select last_insert_rowid()";
        if ([database Getstmt:strSql :&statement])
        {
            while (sqlite3_step(statement) == SQLITE_ROW)
            {
                result = sqlite3_column_int(statement, 0);
            }
        }
    }

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self AddRFID:[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"userId"]] withlock_UUID:appDelegate.lock_Address withrfid_tag_id:lockDevice.TAG_IMEI withtag_name:lockDevice.TAG_USERNAME];
    });
    return result;
}

// According lock ID to modify the entire lock information
// ID, Type, UUID can not be modified
// Name, Password, AutoUnLock, AutoLockTime be modified

- (BOOL)UpdateLockDevice:(LockDevice *)lockDevice WithLockID:(int)lockID
{   
    const char *strSql = [[NSString stringWithFormat:@"update lock_Device set lock_Name = ?, lock_Password = '%@', lock_AutoUnLock = %d, lock_AutoLockTime = %d where lock_ID = %d",
                           lockDevice.lock_Password,
                           lockDevice.lock_AutoUnLock,
                           lockDevice.lock_AutoLockTime,
                           lockID] UTF8String];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self updateuserlock:[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"userId"]] withlock_UUID:lockDevice.lock_UUID withlock_username:[[NSUserDefaults standardUserDefaults] valueForKey:@"kUserName"] withlock_address:@"123" withlock_password:lockDevice.lock_Password withlock_type:[NSString stringWithFormat:@"%d",lockDevice.lock_Type] withlock_name:lockDevice.lock_Name withlock_version:[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"APPVersion"]] withlock_autounLock:[NSString stringWithFormat:@"%d", lockDevice.lock_AutoUnLock] withlock_autolocktime:[NSString stringWithFormat:@"%d",lockDevice.lock_AutoLockTime] withled_time:[NSString stringWithFormat:@"%d",lockDevice.box_LED_Time] withsure_set:[NSString stringWithFormat:@"%d",lockDevice.sure_Set_Percent] withpasscode:lockDevice.passcode withpasscode_enable:[NSString stringWithFormat:@"%d",lockDevice.passcode_enable]];
    });
    return [database DoSql:strSql WithParam:lockDevice.lock_Name];
}

// share code update user count wise
- (BOOL)UpdateShareCodeUser:(LockDevice *)lockDevice WithLockID:(int)lockID
{
    const char *strSql = [[NSString stringWithFormat:@"update lock_Device set lock_Name = ?, share_code_user = '%@', share_code_user_count= '%@' where lock_ID = %d",
                           lockDevice.share_Code,
                           lockDevice.share_Code_Count,
                           lockID] UTF8String];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
       [self addsharecode:[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"userId"]] withlock_UUID:lockDevice.lock_UUID share_code_type:@"1" share_code:lockDevice.share_Code share_code_user_count:lockDevice.share_Code_Count share_code_time:@"" share_code_date_flag:0 share_code_date_start:@"" share_code_date_end:@"" share_code_time_flag:0 share_code_start_time:@"" share_code_end_time:@""];
    });
    
    
    
    return [database DoSql:strSql WithParam:lockDevice.lock_Name];
}

// share code update time wise
- (BOOL)UpdateShareCodeTime:(LockDevice *)lockDevice WithLockID:(int)lockID
{
    // with date, with time =3
    // with date, no time =1
    //no date, with time = 2
    int Dateflag = 0;
    int Timeflag = 0;
    switch (lockDevice.share_code_time_flag) {
        case 1:
            Dateflag = 1;
            Timeflag = 0;
            break;
            
        case 2:
            Dateflag = 0;
            Timeflag = 1;
            break;
            
        case 3:
            Dateflag = 1;
            Timeflag = 1;
            break;
            
        default:
            break;
    }
    
    NSString *strStDt = [NSString stringWithFormat:@"%@",lockDevice.share_Code_Start_Time];
    NSDateFormatter *dtFormat = [NSDateFormatter new];
    [dtFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss +zzzz"];
    NSDate *dt = [dtFormat dateFromString:strStDt];
    [dtFormat setDateFormat:@"dd MMM yyyy"];
    strStDt = [dtFormat stringFromDate:dt];
    
    NSString *strEdDt = [NSString stringWithFormat:@"%@",lockDevice.share_Code_End_Time];
    dtFormat = [NSDateFormatter new];
    [dtFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss +zzzz"];
    dt = [dtFormat dateFromString:strEdDt];
    [dtFormat setDateFormat:@"dd MMM yyyy"];
    strEdDt = [dtFormat stringFromDate:dt];
    
    const char *strSql = [[NSString stringWithFormat:@"update lock_Device set lock_Name = ?, share_code_time = '%@' , share_code_time_start = '%@' , share_code_time_end = '%@' , share_Code_Time1 = '%@' , share_Code_Time2 = '%@' , share_code_time_flag = %d where lock_ID = %d",
                           lockDevice.share_Code_Time,
                           strStDt,
                           strEdDt,
                           lockDevice.share_Code_Time1,
                           lockDevice.share_Code_Time2,
                           lockDevice.share_code_time_flag,
                           lockID] UTF8String];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self addsharecode:[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"userId"]] withlock_UUID:lockDevice.lock_UUID share_code_type:@"2" share_code:@"" share_code_user_count:@"" share_code_time:lockDevice.share_Code_Time share_code_date_flag:Dateflag share_code_date_start:strStDt share_code_date_end:strEdDt share_code_time_flag:Timeflag share_code_start_time:lockDevice.share_Code_Time1 share_code_end_time:lockDevice.share_Code_Time2];
    });

    
    return [database DoSql:strSql WithParam:lockDevice.lock_Name];
}

// Update BOX LED timeout time
- (BOOL)UpdateBoxLedTime:(LockDevice *)lockDevice WithLockID:(int)lockID
{
    const char *strSql = [[NSString stringWithFormat:@"update lock_Device set lock_Name = ?, led_Time = %d where lock_ID = %d",
                           lockDevice.box_LED_Time,
                           lockID] UTF8String];
    
    return [database DoSql:strSql WithParam:lockDevice.lock_Name];
}

// Update box sure set
- (BOOL)UpdateBoxSureSet:(LockDevice *)lockDevice WithLockID:(int)lockID
{
    const char *strSql = [[NSString stringWithFormat:@"update lock_Device set lock_Name = ?, sure_set = %d where lock_ID = %d",
                           lockDevice.sure_Set_Percent,
                           lockID] UTF8String];
    
    return [database DoSql:strSql WithParam:lockDevice.lock_Name];
}

- (BOOL)UpdatePasscode:(LockDevice *)lockDevice lock_UUID:(NSString *)lock_UUID
{
    const char *strSql = [[NSString stringWithFormat:@"update lock_Device set lock_Name = ?, passcode = '%@', passcode_enable = %d where lock_UUID = '%@'",
                           lockDevice.passcode,
                           lockDevice.passcode_enable,
                           lock_UUID] UTF8String];
    
    return [database DoSql:strSql WithParam:lockDevice.lock_Name];
}


- (BOOL)UpdateRFIDName:(LockDevice *)lockDevice WithIMEI:(NSString *)IMEI
{
    const char *strSql = [[NSString stringWithFormat:@"update RFID_TAG_TABLE set TAG_USERNAME = ? where TAG_IMEI = '%@'",IMEI] UTF8String];
    
    return [database DoSql:strSql WithParam: quickLockDeviceManage.lockDevice.TAG_USERNAME];
}

// To obtain the device information based on ID
- (LockDevice *)GetLockDeviceWithDeviceUUID:(NSString *)deivceUUID
{
    LockDevice *lockDevice = [LockDevice new];
    const char *strSql;
    sqlite3_stmt *statement;
    strSql = [[NSString stringWithFormat:@"select * from lock_Device where lock_UUID = '%@'", deivceUUID] UTF8String];
    
    if ([database Getstmt:strSql :&statement])
    {
        while (sqlite3_step(statement) == SQLITE_ROW)
        {
            lockDevice.lock_ID = sqlite3_column_int(statement, 0);
            lockDevice.lock_Name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)];
            lockDevice.lock_Type = sqlite3_column_int(statement, 2);
            lockDevice.lock_Password = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 3)];
            lockDevice.lock_UUID = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 4)];
            lockDevice.lock_AutoUnLock = sqlite3_column_int(statement, 5);
            lockDevice.lock_AutoLockTime = sqlite3_column_int(statement, 6);
            lockDevice.box_LED_Time = sqlite3_column_int(statement, 16);
            lockDevice.sure_Set_Percent = sqlite3_column_int(statement, 17);
            lockDevice.passcode = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 18)];
            lockDevice.passcode_enable = sqlite3_column_int(statement, 19);
        }
    }
    return lockDevice;
}

// Delete the device according to blockID
- (BOOL)DeleteLockDevice:(int)lockID
{
    const char *strSql = [[NSString stringWithFormat:@"delete from lock_Device where lock_ID = %d",
                           lockID] UTF8String];
    
    return [database DoSql:strSql];
}

- (BOOL)DeleteLockDeviceByUUID:(NSString *)lockUUID
{
    const char *strSql = [[NSString stringWithFormat:@"delete from lock_Device where lock_UUID = '%@'",
                           lockUUID] UTF8String];
    
    return [database DoSql:strSql];
}

- (BOOL)DeleteRFIDTag:(NSString *)IMEI
{
    const char *strSql = [[NSString stringWithFormat:@"delete from RFID_TAG_TABLE where TAG_IMEI = '%@'",
                           IMEI] UTF8String];
    return [database DoSql:strSql];
}

// Code Added By Jayesh
#pragma mark - W.S Methods


-(void)adduserlock:(NSString *)user_id withlock_UUID:(NSString *)lock_UUID withlock_username:(NSString *)lock_username withlock_address:(NSString *)lock_address withlock_password:(NSString *)lock_password withlock_type:(NSString *)lock_type withlock_name:(NSString *)lock_name withlock_version:(NSString *)lock_version withlock_autounLock:(NSString *)lock_autounLock withlock_autolocktime:(NSString *)lock_autolocktime withled_time:(NSString *)led_time withsure_set:(NSString *)sure_set withpasscode:(NSString *)passcode withpasscode_enable:(NSString *)passcode_enable

{
    //http://27.109.19.234/lynkd/backend/web/api/adduserlock?user_id=8&lock_UUID=1254657&lock_username=lock1&lock_address=123&lock_password=123&lock_type=1&lock_name=123&lock_version=1&lock_autounLock=1&lock_autolocktime=30&led_time=30&sure_set=30&passcode=123&passcode_enable=1

    //    device_type = I = iOS and A= Android
    
    NSDictionary *dictParam = @{@"user_id":[self CheckForNullparameter:user_id],
                                @"lock_UUID":[self CheckForNullparameter:lock_UUID],
                                @"lock_username":[self CheckForNullparameter:lock_username],
                                @"lock_address":[self CheckForNullparameter:lock_address],
                                @"lock_password":[self CheckForNullparameter:lock_password],
                                @"lock_type":[self CheckForNullparameter:lock_type],
                                @"lock_name":[self CheckForNullparameter:lock_name],
                                @"lock_version":[self CheckForNullparameter:lock_version],
                                @"lock_autounLock":[self CheckForNullparameter:lock_autounLock],
                                @"lock_autolocktime":[self CheckForNullparameter:lock_autolocktime],
                                @"led_time":[self CheckForNullparameter:led_time],
                                @"sure_set":[self CheckForNullparameter:sure_set],
                                @"passcode":[self CheckForNullparameter:passcode],
                                @"passcode_enable":[self CheckForNullparameter:passcode_enable]};
    
    
    NSString *strUrl = [NSString stringWithFormat:@"%@adduserlock?",BASEURL];
    NSLog(@"strUrl %@  dictParam %@",strUrl,dictParam);
    [Webservices postWithUrlString:strUrl parameters:dictParam success:^(NSDictionary *responce) {
        //Success
        NSLog(@"responce:%@",responce);
        //do code here
        if ([[responce valueForKey:@"status"] isEqualToString:@"1"]) {
            NSLog(@"adduserlock Successful");
        }else{
            NSLog(@"adduserlock Not Successful");
        }
    } failure:^(NSError *error) {
        //error
        NSLog(@"error:%@",error);
    }];
}

-(void)updateuserlock:(NSString *)user_id withlock_UUID:(NSString *)lock_UUID withlock_username:(NSString *)lock_username withlock_address:(NSString *)lock_address withlock_password:(NSString *)lock_password withlock_type:(NSString *)lock_type withlock_name:(NSString *)lock_name withlock_version:(NSString *)lock_version withlock_autounLock:(NSString *)lock_autounLock withlock_autolocktime:(NSString *)lock_autolocktime withled_time:(NSString *)led_time withsure_set:(NSString *)sure_set withpasscode:(NSString *)passcode withpasscode_enable:(NSString *)passcode_enable

{
    //http://27.109.19.234/lynkd/backend/web/api/updateuserlock?user_id=8&lock_UUID=1254657&lock_username=lock1&lock_address=123&lock_password=123&lock_type=1&lock_name=123&lock_version=1&lock_autounLock=1&lock_autolocktime=30&led_time=30&sure_set=30&passcode=123&passcode_enable=1


    //    device_type = I = iOS and A= Android
    NSDictionary *dictParam = @{@"user_id":[self CheckForNullparameter:user_id],
                                @"lock_UUID":[self CheckForNullparameter:lock_UUID],
                                @"lock_username":[self CheckForNullparameter:lock_username],
                                @"lock_address":[self CheckForNullparameter:lock_address],
                                @"lock_password":[self CheckForNullparameter:lock_password],
                                @"lock_type":[self CheckForNullparameter:lock_type],
                                @"lock_name":[self CheckForNullparameter:lock_name],
                                @"lock_version":[self CheckForNullparameter:lock_version],
                                @"lock_autounLock":[self CheckForNullparameter:lock_autounLock],
                                @"lock_autolocktime":[self CheckForNullparameter:lock_autolocktime],
                                @"led_time":[self CheckForNullparameter:led_time],
                                @"sure_set":[self CheckForNullparameter:sure_set],
                                @"passcode":[self CheckForNullparameter:passcode],
                                @"passcode_enable":[self CheckForNullparameter:passcode_enable]};
    
    NSString *strUrl = [NSString stringWithFormat:@"%@updateuserlock?",BASEURL];
    
    NSLog(@"strUrl %@  dictParam %@",strUrl,dictParam);
    
    [Webservices postWithUrlString:strUrl parameters:dictParam success:^(NSDictionary *responce) {
        //Success
        NSLog(@"responce:%@",responce);
        //do code here
        if ([[responce valueForKey:@"status"] isEqualToString:@"1"]) {
            NSLog(@"updateuserlock Successful");
        }else{
            NSLog(@"updateuserlock Not Successful");
        }
    } failure:^(NSError *error) {
        //error
        NSLog(@"error:%@",error);
    }];
}

-(void)AddRFID:(NSString *)user_id withlock_UUID:(NSString *)lock_UUID withrfid_tag_id:(NSString *)rfid_tag_id withtag_name:(NSString *)tag_name
{
    
    //     http://27.109.19.234/lynkd/backend/web/api/addrfid?user_id=8&lock_UUID=1254657&rfid_tag_id=abc123&tag_name=abctag
    
    NSDictionary *dictParam = @{@"user_id":[self CheckForNullparameter:user_id],
                                @"lock_UUID":[self CheckForNullparameter:lock_UUID],
                                @"rfid_tag_id":[self CheckForNullparameter:rfid_tag_id],
                                @"tag_name":[self CheckForNullparameter:tag_name]};
    
    NSString *strUrl = [NSString stringWithFormat:@"%@addrfid?",BASEURL];
    
    NSLog(@"strUrl %@  dictParam %@",strUrl,dictParam);
    
    [Webservices postWithUrlString:strUrl parameters:dictParam success:^(NSDictionary *responce) {
        //Success
        NSLog(@"responce:%@",responce);
        //do code here
        if ([[responce valueForKey:@"status"] isEqualToString:@"1"]) {
            NSLog(@"AddRFID Successful");
        }else{
            NSLog(@"AddRFID Not Successful");
        }
    } failure:^(NSError *error) {
        //error
        NSLog(@"error:%@",error);
    }];
}

-(void)DeleteRFID:(NSString *)user_id withlock_UUID:(NSString *)lock_UUID withrfid_tag_id:(NSString *)rfid_tag_id
{
    //    http://27.109.19.234/lynkd/backend/web/api/deleterfid?user_id=8&lock_UUID=1254657&rfid_tag_id=abc123
    
    NSDictionary *dictParam = @{@"user_id":user_id,@"lock_UUID":lock_UUID,@"rfid_tag_id":rfid_tag_id};
    
    NSString *strUrl = [NSString stringWithFormat:@"%@deleterfid?",BASEURL];
    [Webservices postWithUrlString:strUrl parameters:dictParam success:^(NSDictionary *responce) {
        //Success
        NSLog(@"responce:%@",responce);
        //do code here
        if ([[responce valueForKey:@"status"] isEqualToString:@"1"]) {
            
            NSLog(@"Delete Successful");
        }else{
            NSLog(@"Delete Not Successful");
        }
    } failure:^(NSError *error) {
        //error
        NSLog(@"error:%@",error);
    }];
}


//url – http://ec2-54-186-214-225.us-west-2.compute.amazonaws.com/lynkd/backend/web/api/addsharecode?user_id=8&lock_UUID=UU123&share_code_type=1&share_code=123&share_code_user_count=10&share_code_time=20&share_code_date_flag=0&share_code_date_start=2017-10-10&share_code_date_end=2017-10-10&share_code_time_flag=1&share_code_start_time=10:00&share_code_end_time=20:00


-(void)addsharecode:(NSString *)user_id withlock_UUID:(NSString *)lock_UUID share_code_type:(NSString *)share_code_type  share_code:(NSString *)share_code share_code_user_count:(NSString *)share_code_user_count share_code_time:(NSString *)share_code_time share_code_date_flag:(int)share_code_date_flag share_code_date_start:(NSString *)share_code_date_start  share_code_date_end:(NSString *)share_code_date_end  share_code_time_flag:(int)share_code_time_flag share_code_start_time:(NSString *)share_code_start_time share_code_end_time:(NSString *)share_code_end_time
{
    //    http://27.109.19.234/lynkd/backend/web/api/deleterfid?user_id=8&lock_UUID=1254657&rfid_tag_id=abc123
    
//    NSDictionary *dictParam = @{@"user_id":[self CheckForNullparameter:user_id],@"lock_UUID":[self CheckForNullparameter:lock_UUID],
//                                @"share_code_type":[self CheckForNullparameter:share_code_type],
//                                @"share_code":[self CheckForNullparameter:share_code],
//                                @"share_code_user_count":[self CheckForNullparameter:share_code_user_count],
//                                @"share_code_time":[self CheckForNullparameter:share_code_time],
//                                
//                                @"share_code_date_flag":[self CheckForNullparameter:[NSString stringWithFormat:@"%d",share_code_date_flag]],
//                                @"share_code_date_start"[self CheckForNullparameter:share_code_date_start],
//                                @"share_code_date_end":[self CheckForNullparameter:share_code_date_end],
//                                @"share_code_time_flag":[self CheckForNullparameter:[NSString stringWithFormat:@"%d",share_code_time_flag]],
//                                @"share_code_start_time":[self CheckForNullparameter:share_code_start_time],
//                                @"share_code_end_time":[self CheckForNullparameter:share_code_end_time]
//                                };
    
    //------------------
    
    NSString *strUrl = [NSString stringWithFormat:@"%@addsharecode?lock_UUID=%@&user_id=%@&share_code_type=%@&share_code=%@&share_code_user_count=%@&share_code_time=%@&share_code_date_flag=%@&share_code_date_start=%@&share_code_date_end=%@&share_code_time_flag=%@&share_code_start_time=%@&share_code_end_time=%@",BASEURL,[self CheckForNullparameter:lock_UUID],[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"userId"]],[self CheckForNullparameter:share_code_type],[self CheckForNullparameter:share_code],[self CheckForNullparameter:share_code_user_count],[self CheckForNullparameter:share_code_time],[self CheckForNullparameter:[NSString stringWithFormat:@"%d",share_code_date_flag]],[self CheckForNullparameter:share_code_date_start],[self CheckForNullparameter:share_code_date_end],[self CheckForNullparameter:[NSString stringWithFormat:@"%d",share_code_time_flag]],[self CheckForNullparameter:share_code_start_time],[self CheckForNullparameter:share_code_end_time]];
    NSString* encodedUrl = [strUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    [Webservices getWithUrlString:encodedUrl success:^(NSDictionary *responce) {
        //Success
        NSLog(@"responce:%@",responce);
        //do code here
        if ([[responce valueForKey:@"status"] isEqualToString:@"1"]) {
            NSLog(@"Sharecode Added successfully");
            dispatch_async(dispatch_get_main_queue(), ^{
                NSLog(@"Sharecode Added successfully");
            });
        }else{
            NSLog(@"Share code not added");
        }
    } failure:^(NSError *error) {
        //error
        NSLog(@"error:%@",error.localizedDescription);
    }];

    
    //-------------------
    
//    NSString *strUrl = [NSString stringWithFormat:@"%@addsharecode?",BASEURL];
//    [Webservices postWithUrlString:strUrl parameters:dictParam success:^(NSDictionary *responce) {
//        //Success
//        NSLog(@"responce:%@",responce);
//        //do code here
//        if ([[responce valueForKey:@"status"] isEqualToString:@"1"]) {
//            
//            NSLog(@"Share Code Added successfully");
//        }else{
//            NSLog(@"Share code not added");
//        }
//    } failure:^(NSError *error) {
//        //error
//        NSLog(@"error:%@",error.localizedDescription);
//    }];
}


-(NSString *)CheckForNullparameter:(NSString*)param{
    
    NSString *param1=[NSString stringWithFormat:@"%@",param];
    if ( [param1 isEqual:(id)[NSNull null]] || param1==nil || [param1 isEqualToString:@""] || [param1 isKindOfClass:[NSNull class]]||[param1 isEqualToString:@"<null>"]||[param1 isEqualToString:@"(<null>)"]||[param1 isEqualToString:@"(null)"] || [param1 isEqualToString:@"<nil>"])
    {
        param1=@"";
        return param1;
    }
    else
    {
        return param1;
    }
    return @"";
}




@end
