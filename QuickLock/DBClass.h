//
//  DBClass.h
//  PEDO6
//
//  Created by 中易腾达（常州分公司） on 12-4-13.
//  Copyright (c) 2012年 中易腾达. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "sqlite3.h"


@interface DBClass : NSObject
{
    sqlite3 *database;
}

- (BOOL)OpenDatabase:(NSString*) dbName;
- (BOOL)DoSql :(const char*)strSql;
- (BOOL)DoSql:(const char *)strSql WithParam:(NSString *)lockName;
- (BOOL)Getstmt:(const char*)strSql :(sqlite3_stmt**) statement;
- (void)CloseDatabase;
@end
