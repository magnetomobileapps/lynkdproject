//
//  InputPassword.m
//  QuickLock
//
//  Created by administrator on 15-3-12.
//  Copyright (c) 2015年 Bge. All rights reserved.
//

#import "InputPassword.h"
#import "PublicMarco.h"

@interface InputPassword()
{
        UILabel *labelTitle;
}
@end

@implementation InputPassword

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(PasswordTextBlank:) name:@"PasswordTextBlank" object:nil];
        // register for keyboard notifications
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
       
        [self setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.6f]];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(Cancel:)];
        [self addGestureRecognizer:tap];
        
        viewBackground = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 280, 144)];
        [viewBackground setCenter:self.center];
        [viewBackground setBackgroundColor:[UIColor whiteColor]];
        [viewBackground.layer setCornerRadius:10];
        [viewBackground.layer setMasksToBounds:YES];
        [viewBackground.layer setBorderColor:bordergray.CGColor];
        [viewBackground.layer setBorderWidth:4];
        [self addSubview:viewBackground];
        
        textFieldPassword = [[UITextField alloc] initWithFrame:CGRectMake(10,
                                                                          50,
                                                                          viewBackground.frame.size.width - 20,
                                                                          35)];
        [textFieldPassword setBorderStyle:UITextBorderStyleRoundedRect];
        [textFieldPassword setTextColor:[UIColor darkGrayColor]];
        [textFieldPassword setKeyboardType:UIKeyboardTypeNumberPad];
        textFieldPassword.placeholder = @"Enter password";
        [textFieldPassword addTarget:self
                              action:@selector(textFieldDidEndOnExit:)
                    forControlEvents:UIControlEventEditingDidEndOnExit];
        [viewBackground addSubview:textFieldPassword];
        
        labelTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, viewBackground.frame.size.width, 30)];
        [labelTitle setText:@"Input Password"];
        labelTitle.font = [UIFont boldSystemFontOfSize:17];
        [labelTitle setTextAlignment:NSTextAlignmentCenter];
        [labelTitle setTextColor:titleorange];
//                [labelTitle setTextColor:[UIColor colorWithRed:249.0f/255.0f green:144.0f/255.0f blue:36.0f/255.0f alpha:1.0f]];
        [viewBackground addSubview:labelTitle];
        
        UILabel * leftView = [[UILabel alloc] initWithFrame:CGRectMake(20, 50, 241, 35)];
        leftView.backgroundColor = [UIColor clearColor];
        textFieldPassword.delegate = self;
        textFieldPassword.font = [UIFont systemFontOfSize:13];
        textFieldPassword.leftView = leftView;
        textFieldPassword.layer.cornerRadius = cornerRadius5;
        textFieldPassword.layer.masksToBounds = YES;
        textFieldPassword.layer.borderWidth = borderWidth4;
        textFieldPassword.layer.borderColor = bordergray.CGColor;
        textFieldPassword.backgroundColor = lightgray;
        textFieldPassword.secureTextEntry = YES;
        [textFieldPassword becomeFirstResponder];
        
        buttonOK = [[UIButton alloc] initWithFrame:CGRectMake(40, 95, 90, 35)];
        [buttonOK setTitle:@"OK" forState:UIControlStateNormal];
        [buttonOK setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [buttonOK.titleLabel setFont:[UIFont systemFontOfSize:15]];
        [buttonOK setBackgroundColor:titleorange];
        [self SetButtonCornerRadius5AndWhiteBorder2:buttonOK];
        [buttonOK addTarget:self action:@selector(OK:) forControlEvents:UIControlEventTouchUpInside];
        [viewBackground addSubview:buttonOK];
        
        buttonCancel = [[UIButton alloc] initWithFrame:CGRectMake(150, 95, 90, 35)];
        [buttonCancel setTitle:@"Cancel" forState:UIControlStateNormal];
        [buttonCancel setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [buttonCancel.titleLabel setFont:[UIFont systemFontOfSize:15]];
        [buttonCancel setBackgroundColor:titleorange];
        [self SetButtonCornerRadius5AndWhiteBorder2:buttonCancel];
        [buttonCancel addTarget:self
                         action:@selector(Cancel:)
               forControlEvents:UIControlEventTouchUpInside];
        [viewBackground addSubview:buttonCancel];
    }
    return self;
}

- (void)keyboardWillShow:(NSNotification *)note
{
    NSValue *value = [[note userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect frame = [value CGRectValue];
    
    [self adjustToKeyboardBounds:frame];
}

- (void)keyboardWillHide:(NSNotification *)note
{
    [self adjustToKeyboardBounds:CGRectZero];
}

- (void)adjustToKeyboardBounds:(CGRect)bounds
{
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    CGFloat height = CGRectGetHeight(screenBounds) - CGRectGetHeight(bounds);
    CGRect frame = self.frame;
    frame.origin.y = (height - CGRectGetHeight(self.bounds)) / 2.0;
    
    if (CGRectGetMinY(frame) < 0)
    {
        NSLog(@"warning: dialog is clipped, origin negative (%f)", CGRectGetMinY(frame));
    }
    
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
        self.frame = frame;
    } completion:nil];
}

- (void)drawRect:(CGRect)rect
{
        if (self.isModifyPassword)
        {
                [labelTitle setText:@"Input new password"];
                [buttonOK setTitle:@"Save" forState:UIControlStateNormal];
        }
}

- (void)PasswordTextBlank:(NSNotificationCenter *)center
{
    textFieldPassword.text = @"";
}

- (void)SetButtonCornerRadius5AndWhiteBorder2:(UIButton *)button
{
        [button.layer setCornerRadius:5];
//        [button.layer setBorderWidth:2];
//        [button.layer setBorderColor:[UIColor whiteColor].CGColor];
        [button.layer setMasksToBounds:YES];
}

- (void)textFieldDidEndOnExit:(id)sender
{
        [textFieldPassword resignFirstResponder];
}

- (void)OK:(id)sender
{
        BOOL passwordIsValid = NO;
        
        // Password length is eight digits, if the length is not legitimate, the user is prompted
        if (textFieldPassword.text.length != 8)
        {
                passwordIsValid = NO;
        }
        else
        {
                // Determine whether the password is all digital, if not all, it will pop back up input
                NSString *password = textFieldPassword.text;
                if (password.length != 0)
                {
                        for (int i = 0; i < password.length; i++)
                        {
                                if ([kDigitalRange rangeOfString:[password substringWithRange:NSMakeRange(i, 1)]].location == NSNotFound)
                                {
                                        passwordIsValid = NO;
                                        break;
                                }
                                
                                if (i == password.length - 1)
                                {
                                        passwordIsValid = YES;
                                }
                        }
                }
        }
        
        if (passwordIsValid)
        {
                [self.delegate returnPassword:textFieldPassword.text];
        }
        else
        {
            [self makeToast:@"Warning!\nPassword must be 8 digital" duration:2.0 position:@"center"];
//                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning!"
//                                                                message:@"Password must be 8 digital"
//                                                               delegate:nil
//                                                      cancelButtonTitle:@"OK"
//                                                      otherButtonTitles:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"PasswordTextBlank" object:nil];

//                [alert show];
        }
}

- (void)Cancel:(id)sender
{
        [self RemoveAllView];
        
        // Through different delegate to judge is to enter a password just to connect, or change the password. When you want to cancel the input connection is disconnected, and when to change the password to remove unwanted disconnects.
        [self.delegate cancelModifyPassword];
    
    
    [quickLockDeviceManage initiativeDisConnect];
//    [dbAccess DeleteLockDevice:quickLockDeviceManage.lockDevice.lock_ID];

    [[NSNotificationCenter defaultCenter] postNotificationName:@"startAnimt" object:nil];
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"AddLock" object:nil];
}

- (void)RemoveAllView
{
        [self removeFromSuperview];
}

// Limit the input box can only enter a number, and a maximum length of 8
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
        if ([string isEqualToString:@"\n"] || [string isEqualToString:@""])
        {
                return YES;
        }
        else if (textField.text.length > 7)
        {
                return NO;
        }
        else
        {
                return [kDigitalRange rangeOfString:string].location != NSNotFound;
        }
}

@end
