//
//  ConfigureDevice.m
//  QuickLock
//
//  Created by PUNDSK001 on 14/06/16.
//  Copyright © 2016 Bge. All rights reserved.
//

#import "ConfigureDevice.h"
#import "AlertMyDevices.h"
#import "AlertShareCode.h"

@interface ConfigureDevice ()
{
    AlertMyDevices *alertMyDevices;
    IBOutlet UILabel *lblTitleConfigure;
}
@property (strong, nonatomic) IBOutlet UILabel *lblDeviceName;

@end

@implementation ConfigureDevice

- (void)viewDidLoad
{
    [super viewDidLoad];
    alertMyDevices = [self.storyboard instantiateViewControllerWithIdentifier:@"AlertMyDevices"];
}

-(void)viewWillAppear:(BOOL)animated {
    appDelegate.strSelectedPush = nil;
    _lblDeviceName.text = appDelegate.strSelectedAlertDevice;
}

- (IBAction)btnEvent:(UIButton *)sender
{
    if (sender.tag == 1)
    {
        AlertShareCode *alertShareCode =[self.storyboard instantiateViewControllerWithIdentifier:@"AlertShareCode"];
        [self.navigationController pushViewController:alertShareCode animated:NO];
    }
    else if (sender.tag == 2)
    {
        appDelegate.strSelectedPush = @"phone";
        [self.navigationController popViewControllerAnimated:NO];
    }
    else
    {
        appDelegate.strSelectedPush = @"email";
        [self.navigationController popViewControllerAnimated:NO];
    }
}

- (IBAction)btnBack:(id)sender
{
    appDelegate.strSelectedAlertDevice = nil;
    [self.navigationController popViewControllerAnimated:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
