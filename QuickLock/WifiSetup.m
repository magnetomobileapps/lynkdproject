//
//  WifiSetup.m
//  QuickLock
//
//  Created by PUNDSK001 on 15/06/16.
//  Copyright © 2016 Bge. All rights reserved.
//

#import "WifiSetup.h"

@interface WifiSetup ()
@property (strong, nonatomic) IBOutlet UILabel *lblDevicename;
@property (strong, nonatomic) IBOutlet UILabel *lblTitleWifiSetUp;


@end

@implementation WifiSetup

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    // [quickLockDeviceManage getWifiMacAddress];
    
    _lblDevicename.text = quickLockDeviceManage.lockDevice.lock_Name;
}

- (IBAction)btnChooseNetwork:(id)sender
{
    //open Wi-Fi Settings
    if (&UIApplicationOpenSettingsURLString != NULL)
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"prefs:root=WIFI"]];
    }
}

- (IBAction)btnBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}

- (IBAction)btnEnableShareCode:(UIButton *)sender
{
    if (sender.tag == 0)
    {
        sender.tag = 1;
        [sender setBackgroundImage:[UIImage imageNamed:@"orange-switch-icon"] forState:UIControlStateNormal];
    }
    else
    {
        sender.tag = 0;
        [sender setBackgroundImage:[UIImage imageNamed:@"gray-switch-icon"] forState:UIControlStateNormal];
    }
}

- (IBAction)btnEnablePush:(UIButton *)sender
{
    if (sender.tag == 0)
    {
        sender.tag = 1;
        [sender setBackgroundImage:[UIImage imageNamed:@"orange-switch-icon"] forState:UIControlStateNormal];
    }
    else
    {
        sender.tag = 0;
        [sender setBackgroundImage:[UIImage imageNamed:@"gray-switch-icon"] forState:UIControlStateNormal];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
