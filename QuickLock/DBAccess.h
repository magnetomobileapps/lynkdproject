//
//  DBAccess.h
//  QuickLock
//
//  Created by 王洋 on 14/12/27.
//  Copyright (c) 2014年 Bge. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DBClass.h"
#import "LockDevice.h"

@interface DBAccess : NSObject

@property (nonatomic) DBClass *database;

/*!
 *  @method 检查并初始化数据库
 *  @discussion 返回BOOL值
 */
- (BOOL)CheckDatabase;

/*!
 *  @method 获取所有设备信息
 *  @discussion 返回设备列表数组
 */
- (NSArray *)GetAllLockDevice;

/*!
 *  @method 插入新设备
 *  @discussion 返回设备ID
 */
- (int)InsertLockDevice:(LockDevice *)lockDevice;

/*!
 *  @method 根据设备ID修改设备信息
 *  @discussion 返回是否修改成功
 */

- (BOOL)UpdateLockDeviceSecondConnect:(LockDevice *)lockDevice lock_UUID:(NSString *)lock_UUID;

- (BOOL)UpdateLockDevice:(LockDevice *)lockDevice WithLockID:(int)lockID;

- (BOOL)UpdateShareCodeUser:(LockDevice *)lockDevice WithLockID:(int)lockID;

- (BOOL)UpdateShareCodeTime:(LockDevice *)lockDevice WithLockID:(int)lockID;

- (BOOL)UpdateBoxLedTime:(LockDevice *)lockDevice WithLockID:(int)lockID;

- (BOOL)UpdateBoxSureSet:(LockDevice *)lockDevice WithLockID:(int)lockID;

- (BOOL)UpdatePasscode:(LockDevice *)lockDevice lock_UUID:(NSString *)lock_UUID;

//- (LockDevice *)GetPasscodeWithDeviceUUID:(NSString *)deivceUUID;

/*!
 *  @method 删除设备
 *  @discussion 返回是否删除成功
 */
- (BOOL)DeleteLockDevice:(int)lockID;

- (BOOL)DeleteLockDeviceByUUID:(NSString *)lockUUID;

/*!
 *  @method 根据UUID来获取设备信息
 *  @discussion 返回获取到的设备类实例
 */
- (LockDevice *)GetLockDeviceWithDeviceUUID:(NSString *)deivceUUID;

- (NSArray *)GetRFIDTags;
- (int)InsertRFIDTags:(LockDevice *)lockDevice;
- (BOOL)UpdateRFIDName:(LockDevice *)lockDevice WithIMEI:(NSString *)IMEI;
- (BOOL)DeleteRFIDTag:(NSString *)IMEI;
- (NSString *)GetSingleRFIDTags;
- (NSString *)GetSingleRFIDTagsUserName:(NSString*)TAG_IMEI;

@end
