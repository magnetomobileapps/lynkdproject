//
//  ChangePassword.m
//  QuickLock
//
//  Created by PUNDSK001 on 26/12/15.
//  Copyright © 2015 Bge. All rights reserved.
//

#import "Alarm.h"

@interface Fingerprint ()
{
    NSArray *numbers;
    int slidevalue;
    UIView *viewForFingerIndicate;
    UIImageView *imgFingerIndicate;
    UILabel *lblText;
    BOOL isFingerLearnViewVisible;
    UIAlertView *alert;
}
@property (weak, nonatomic) IBOutlet UILabel *lblNumberOfFingerprints;
@property (weak, nonatomic) IBOutlet UIView *viewSureBg;
@property (weak, nonatomic) IBOutlet UIView *viewSureSet;
@property (weak, nonatomic) IBOutlet UISlider *slide;
@property (weak, nonatomic) IBOutlet UILabel *lbl1;
@property (weak, nonatomic) IBOutlet UILabel *lblMainTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UIButton *btnSureSet;
@property (strong, nonatomic) IBOutlet UILabel *lblNote;

@end

@implementation Fingerprint

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setCount:) name:@"setFingerPrintCount" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fingerPrintResponse:) name:@"fingerPrintResponse" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getCount) name:@"getgetCount" object:nil];
    
    _btnSureSet.layer.borderColor = [UIColor colorWithRed:109.0f/256.0f green:111.0f/256.0f blue:114.0f/256.0f alpha:1.0].CGColor;
    _btnSureSet.layer.borderWidth = 2;
    
    isFingerLearnViewVisible = NO;
    
    [self applyBasierPath:_lbl1];
    
    // These number values represent each slider position
    numbers = @[@(1), @(2), @(3), @(4), @(5)];
    
    _viewSureSet.layer.masksToBounds = YES;
    _viewSureSet.layer.borderColor = lightgray.CGColor;
    _viewSureSet.layer.borderWidth = 4;
    _viewSureSet.layer.cornerRadius = 10;
    
    _viewSureBg.alpha = 0.0f;
    _viewSureBg.hidden = YES;
    
    [_slide setThumbImage:[UIImage imageNamed:@"Screenshot_1"] forState:UIControlStateNormal];
    [_slide setThumbImage:[UIImage imageNamed:@"Screenshot_1"] forState:UIControlStateHighlighted];
   
    NSInteger numberOfSteps = ((float)[numbers count] - 1);
    _slide.maximumValue = numberOfSteps;
    [_slide setMinimumValue:0];
    NSLog(@"val : %d",(quickLockDeviceManage.lockDevice.sure_Set_Percent/20));
    [_slide setValue:(quickLockDeviceManage.lockDevice.sure_Set_Percent/20)-1];
    slidevalue = quickLockDeviceManage.lockDevice.sure_Set_Percent;
    // quickLockDeviceManage.lockDevice.sure_Set_Percent
    _slide.continuous = YES; // NO makes it call only once you let go
    [_slide addTarget:self action:@selector(valueChanged:) forControlEvents:UIControlEventValueChanged];
    
    // Get fingerprint counts
    [quickLockDeviceManage getFingerprintCount];

    viewForFingerIndicate = [[UIView alloc]initWithFrame:self.view.bounds];
    viewForFingerIndicate.backgroundColor = [UIColor whiteColor];
    viewForFingerIndicate.hidden = YES;
    [self.view addSubview:viewForFingerIndicate];
    imgFingerIndicate = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"FingerIndicate.png"]];
    imgFingerIndicate.frame = CGRectMake(0, 150, screenSize.width, screenSize.width/2);
    [viewForFingerIndicate addSubview:imgFingerIndicate];
    lblText = [[UILabel alloc]initWithFrame:CGRectMake(20, imgFingerIndicate.frame.origin.y+imgFingerIndicate.frame.size.height+50, screenSize.width-40, 60)];
    lblText.textAlignment = NSTextAlignmentCenter;
    lblText.textColor = darkgray;
    lblText.text = @"Press and hold finger on reader 3 times.\n(Must click down to activate)";
    lblText.font = [UIFont boldSystemFontOfSize:16];
    lblText.numberOfLines = 3;
    [viewForFingerIndicate addSubview:lblText];
}

- (void)valueChanged:(UISlider *)sender
{
    // round the slider position to the nearest index of the numbers array
    NSUInteger index = (NSUInteger)(_slide.value + 0.5);
    [_slide setValue:index animated:NO];
    NSNumber *number = numbers[index]; // <-- This numeric value you want
    NSInteger *intNumber = [number integerValue]*20;
    slidevalue = intNumber;
}

- (IBAction)btnSaveSure:(UIButton *)sender
{
    [UIView animateWithDuration:0.3 animations:^{
        _viewSureSet.alpha = 0.0f;
        _viewSureBg.alpha = 0.0f;
        if (sender.tag == 4)
        {
            // btnSave
            [self returnSureSet];
        }
        else if (sender.tag == 5)
        {
            // btnCancel
        }
    } completion:^(BOOL finished) {
        _viewSureBg.hidden = YES;
        _viewSureSet.hidden = YES;
        
        [_slide setValue:(quickLockDeviceManage.lockDevice.sure_Set_Percent/20)-1];
    }];
    [self viewDidAppear:YES];
}

-(void)applyBasierPath:(UILabel *)label
{
    UIColor *colorRed = [UIColor colorWithRed:229.0f/255.0f green:0.0f/255.0f blue:28.0f/255.0f alpha:10.f];
    UIColor *colorGray = [UIColor colorWithRed:199.0f/255.0f green:201.0f/255.0f blue:202.0f/255.0f alpha:10.f];
    
    CAGradientLayer *layer = [CAGradientLayer layer];
    layer.frame = label.bounds;
    layer.colors = [NSArray arrayWithObjects:(id)colorRed.CGColor,(id)colorGray.CGColor, nil];
    [layer setStartPoint:CGPointMake(0.0, 0.5)];
    [layer setEndPoint:CGPointMake(0.5, 0.5)];
    [label.layer insertSublayer:layer atIndex:0];
    
    label.layer.masksToBounds = YES;
    label.layer.cornerRadius = 5;
}

-(void)getCount
{
    [quickLockDeviceManage getFingerprintCount];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        viewForFingerIndicate.hidden = YES;
        _lblMainTitle.text = @"FINGERPRINT";
        isFingerLearnViewVisible = NO;
    });
}

- (IBAction)btnSureSet:(id)sender
{
    // Sure set
    _viewSureBg.hidden = NO;
    _viewSureSet.hidden = NO;
    
    [UIView animateWithDuration:0.3 animations:^{
        _viewSureSet.alpha = 1.0f;
        _viewSureBg.alpha = 0.6f;
    }];
}

-(void)setCount:(NSNotification *)notify
{
    int fingerCount = [[notify object] intValue];

    NSLog(@"lock_FingerprintCount : %d",quickLockDeviceManage.lockDevice.lock_FingerprintCount);
    NSLog(@"lock_FingerprintCount : %d", fingerCount);
    [_lblNumberOfFingerprints setText:[NSString stringWithFormat:@"Number of Stored Fingerprints : %d", fingerCount]];
}

- (IBAction)btnClearAllFingerptints:(id)sender
{
    alert = [[UIAlertView alloc]initWithTitle:@"Warning!" message:@"You sure you want to delete all fingerprints?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
    [alert show];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [alert dismissWithClickedButtonIndex:0 animated:NO];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        [quickLockDeviceManage setClearAllFingerPrints];
        [self.view makeToast:@"All fingerprints are cleared."];
        [quickLockDeviceManage getFingerprintCount];
    }
}

- (IBAction)btnCancelLearnNewFingerprint:(id)sender
{
    [quickLockDeviceManage getCancelFingerprintLearn];
    
    [self.view makeToast:@"Fingerprint reading cancelled."];
}

- (IBAction)btnLearnNewFingerprint:(id)sender
{
    [quickLockDeviceManage getFingerprintLearn];

    lblText.text = @"Press and hold finger on reader 3 times.\n(Must click down to activate)";
//    [self.view makeToast:@"Hold a finger to the fingerprint reader." duration:2.0 position:@"bottom"];
    [self.view bringSubviewToFront:_lblMainTitle];
    [self.view bringSubviewToFront:_btnBack];
    
    _lblMainTitle.text = @"Add Fingerprint";
    isFingerLearnViewVisible = YES;
    
    viewForFingerIndicate.hidden = NO;
}

-(void)fingerPrintResponse:(NSNotification *)notifiocation
{
    NSDictionary *userInfo = notifiocation.userInfo;
    NSString *strMsg = [NSString stringWithFormat:@"%@",userInfo[@"msg"]];
    lblText.text = strMsg;
}

- (IBAction)btnBack:(id)sender
{
    if (isFingerLearnViewVisible == YES)
    {
        viewForFingerIndicate.hidden = YES;
        isFingerLearnViewVisible = NO;
    }
    else
    {
        [self.navigationController popViewControllerAnimated:NO];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)returnSureSet
{
    quickLockDeviceManage.lockDevice.sure_Set_Percent = slidevalue;
    
    [quickLockDeviceManage setSureSet:slidevalue];
    
    [dbAccess UpdateBoxSureSet:quickLockDeviceManage.lockDevice WithLockID:quickLockDeviceManage.lockDevice.lock_ID];
    [self viewDidAppear:YES];
}

@end
