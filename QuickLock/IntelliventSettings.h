//
//  IntelliventSettings.h
//  QuickLock
//
//  Created by PUNDSK001 on 04/01/16.
//  Copyright © 2016 Bge. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TimeBasedPosition.h"
#import "FixedTemperature.h"
#import "IntelliventHistoryLog.h"

@interface IntelliventSettings : UIViewController
{
    NSArray *arrayImages, *arrayTitle, *arraySubTitle;
    ModifyName *modifyNameView;
    ChangePassword *change ;
    UIAlertView *alertDeleteDevice;
    UIAlertView *alertModifyUserName;
}
@end
