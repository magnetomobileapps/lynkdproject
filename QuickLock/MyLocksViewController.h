//
//  MyLocksViewController.h
//  QuickLock
//
//  Created by administrator on 15-3-6.
//  Copyright (c) 2015年 Bge. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyLocksViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
{
        IBOutlet UITableView *tableViewLock;
    
        NSArray *arrayLock;
        NSMutableArray *arrLock;
        BOOL isSharecodeNotShown;
        UIAlertView *alertDeleteDevice;
        int viewCalled;
        NSString *strPasscode, *strSelectedName;
        UIToolbar* keyboardToolbar;
        UIBarButtonItem *btnFlexible, *btnDone;
        UIAlertView *alert;
        NSString *strMsgShareCodeCount, *strMsgShareCodeTime, *strMsgPassword;
}
@property (weak, nonatomic) IBOutlet UITextField *txtPasscode;
@property (weak, nonatomic) IBOutlet UIView *viewPasscode;
@property (weak, nonatomic) IBOutlet UILabel *lblNoFund;
@property (weak, nonatomic) IBOutlet UIView *viewBg;
@property (weak, nonatomic) IBOutlet UIView *viewLock;
@property (weak, nonatomic) IBOutlet UIButton *btnClose;
@property (weak, nonatomic) IBOutlet UILabel *lblLockName;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@property (weak, nonatomic) IBOutlet UILabel *lblPassword;
@property (strong, nonatomic) IBOutlet UILabel *lblUserCountShareCode;
@property (strong, nonatomic) IBOutlet UILabel *lblUserCountRemaining;
@property (strong, nonatomic) IBOutlet UILabel *lblTimeLimitShareCode;
@property (strong, nonatomic) IBOutlet UILabel *lblTimeLimitStartDate;
@property (strong, nonatomic) IBOutlet UILabel *lblTimeLimitEndDate;
@property (strong, nonatomic) IBOutlet UILabel *lblTimeLimitRange;

@end
