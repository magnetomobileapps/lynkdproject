//
//  PublicMarco.h
//  QuickLock
//
//  Created by administrator on 14-12-26.
//  Copyright (c) 2014年 Bge. All rights reserved.
//

#ifndef QuickLock_PublicMarco_h
#define QuickLock_PublicMarco_h

#define kRGBColor(r,g,b,a) [UIColor colorWithRed:r/255.f green:g/255.f blue:b/255.f alpha:a/100.f]
#define kViewBackgroundColorTop kRGBColor(55,55,55,100)
#define kViewBackgroundColorBottom kRGBColor(111,111,111,100)

#define kUserNameRange @"0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'-_., "
#define kDigitalRange @"0123456789"

#endif