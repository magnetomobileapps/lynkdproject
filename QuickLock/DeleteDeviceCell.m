//
//  DeleteDeviceCell.m
//  QuickLock
//
//  Created by administrator on 15-3-11.
//  Copyright (c) 2015年 Bge. All rights reserved.
//

#import "DeleteDeviceCell.h"

@implementation DeleteDeviceCell

@synthesize deleteDevice;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
        self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
        if (self)
        {
                [self setBackgroundColor:[UIColor clearColor]];
                [self setSelectionStyle:UITableViewCellSelectionStyleNone];
                
                deleteDevice = [UIButton buttonWithType:UIButtonTypeSystem];
                [deleteDevice setFrame:CGRectMake(100,
                                                  30,
                                                  [UIScreen mainScreen].bounds.size.width - 100*2,
                                                  100 - 30*2)];
                [deleteDevice setBackgroundColor:[UIColor whiteColor]];
                [deleteDevice setTitle:@"Delete device" forState:UIControlStateNormal];
                [deleteDevice setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                [deleteDevice.layer setCornerRadius:10];
                [deleteDevice.layer setMasksToBounds:YES];
                [self addSubview:deleteDevice];
        }
        
        return self;
}

@end
