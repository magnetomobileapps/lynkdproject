//
//  FixedPosition.m
//  QuickLock
//
//  Created by PUNDSK001 on 07/01/16.
//  Copyright © 2016 Bge. All rights reserved.
//

#import "FixedPosition.h"

@interface FixedPosition ()

@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UISlider *slider;
@end

@implementation FixedPosition

- (void)viewDidLoad
{
    [super viewDidLoad];

    CGAffineTransform trans = CGAffineTransformMakeRotation(M_PI_2);
    
    // Label for "Open"
    UILabel *lblOpen = [[UILabel alloc]initWithFrame:CGRectMake((self.view.frame.size.width-46)/2, 110, 46, 20)];
    lblOpen.text = @"Open";
    lblOpen.textColor = [UIColor grayColor];
    lblOpen.font = [UIFont boldSystemFontOfSize:15];
    lblOpen.textAlignment = NSTextAlignmentRight;
    [self.view addSubview:lblOpen];
    
    // create Slider controller
    UILabel *lblSlideBack = [[UILabel alloc]init];
    UISlider *slid = [[UISlider alloc]init];
    if (screenSize.width == 320)
    {
        slid.frame = CGRectMake(self.view.frame.size.width/5, 220, 200, 30);
        lblSlideBack.frame = CGRectMake(self.view.frame.size.width/4, 230, 200, 10);
    }
    else if (screenSize.width == 375)
    {
        slid.frame = CGRectMake(self.view.frame.size.width/4, 220, 200, 30);
        lblSlideBack.frame = CGRectMake(self.view.frame.size.width/4, 230, 200, 10);
    }
    else
    {
        slid.frame = CGRectMake(self.view.frame.size.width/3, 220, 200, 30);
        lblSlideBack.frame = CGRectMake(self.view.frame.size.width/3, 230, 200, 10);
    }
    // make transform to show vertical slider
    slid.transform = trans;
    slid.thumbTintColor = orange;
    [slid setValue:0.5];
    [slid addTarget:self action:@selector(slider:) forControlEvents:UIControlEventValueChanged];
    slid.tintColor = [UIColor lightGrayColor];
    slid.maximumTrackTintColor = [UIColor clearColor];
    slid.minimumTrackTintColor = [UIColor clearColor];
    
    lblSlideBack.transform = trans;
    lblSlideBack.backgroundColor = [UIColor lightGrayColor];
    lblSlideBack.layer.masksToBounds = YES;
    lblSlideBack.layer.cornerRadius = 5;
    [self.view addSubview:lblSlideBack];
    [self.view addSubview:slid];

    // Label to display "Closed"
    UILabel *lblClosed = [[UILabel alloc]initWithFrame:CGRectMake((self.view.frame.size.width-56)/2, 340, 56, 20)];
    lblClosed.text = @"Closed";
    lblClosed.textColor = [UIColor grayColor];
    lblClosed.font = [UIFont boldSystemFontOfSize:15];
    lblClosed.textAlignment = NSTextAlignmentRight;
    [self.view addSubview:lblClosed];
    
    // set Thumb image to slider
    [slid setThumbImage:[UIImage imageNamed:@"Screenshot_1"] forState:UIControlStateNormal];
    [slid setThumbImage:[UIImage imageNamed:@"Screenshot_1"] forState:UIControlStateHighlighted];
    
    // set Max and Min value to slider
    [slid setMaximumValue:100];
    [slid setMinimumValue:0];
    [slid setValue:50];
}

- (IBAction)btnBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}

// Get value of slider on move
- (IBAction)slider:(UISlider *)sender
{
    float value = [sender value];
    NSLog(@"%f",value);
//    self.debugLabel.text = [NSString stringWithFormat:@"Value: %f",value];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
