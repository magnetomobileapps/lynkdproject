//
//  inputUserName.m
//  QuickLock
//
//  Created by PUNDSK001 on 05/03/16.
//  Copyright © 2016 Bge. All rights reserved.
//

#import "inputUserName.h"

@interface inputUserName() <UITextFieldDelegate>
{
    UITextField *txtUserName;
    UIButton *buttonCancel;
}
@end

@implementation inputUserName

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.6f]];
        
        // register for keyboard notifications
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
        
        UIView *viewBackground = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 280, 187)];
        [viewBackground setCenter:self.center];
        [viewBackground setBackgroundColor:[UIColor whiteColor]];
        [viewBackground.layer setCornerRadius:10];
        [viewBackground.layer setMasksToBounds:YES];
        [viewBackground.layer setBorderColor:bordergray.CGColor];
        [viewBackground.layer setBorderWidth:4];
        [self addSubview:viewBackground];
        
        UILabel *labelTitle = [[UILabel alloc] initWithFrame:CGRectMake(20, 14, 241, 60)];
        [labelTitle setText:@"Please set your user name!\nOnly support digital,letter,space and ' , . - _ and can't be null"];
        [labelTitle setTextAlignment:NSTextAlignmentCenter];
        labelTitle.font = [UIFont boldSystemFontOfSize:15];
        labelTitle.numberOfLines = 3;
        labelTitle.textAlignment = NSTextAlignmentLeft;
        [labelTitle setTextColor:orange];
        [viewBackground addSubview:labelTitle];

        txtUserName = [[UITextField alloc] initWithFrame:CGRectMake(20, 88, 241, 35)];
        [txtUserName setBorderStyle:UITextBorderStyleRoundedRect];
        [txtUserName addTarget:self action:@selector(textDidEndOnExit:) forControlEvents:UIControlEventEditingDidEndOnExit];
        [txtUserName becomeFirstResponder];
        [txtUserName setTextColor:[UIColor darkGrayColor]];
        txtUserName.font = [UIFont systemFontOfSize:14];
        txtUserName.layer.cornerRadius = 5;
        txtUserName.layer.masksToBounds = YES;
        txtUserName.layer.borderWidth = 4;
        txtUserName.layer.borderColor = bordergray.CGColor;
        txtUserName.backgroundColor = lightgray;
        txtUserName.placeholder = @"Enter name here";
        txtUserName.returnKeyType = UIReturnKeyDone;
        txtUserName.delegate = self;
        [viewBackground addSubview:txtUserName];
        
        UIButton *buttonOK;
        buttonOK = [[UIButton alloc] initWithFrame:CGRectMake(40, 136, 90, 35)];
        [buttonOK setTitle:@"Save" forState:UIControlStateNormal];
        [buttonOK setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [buttonOK.titleLabel setFont:[UIFont systemFontOfSize:15]];
        [buttonOK setBackgroundColor:titleorange];
        [buttonOK addTarget:self action:@selector(buttonOKTouch:) forControlEvents:UIControlEventTouchUpInside];
        buttonOK.layer.cornerRadius = 5;
        buttonOK.layer.masksToBounds = YES;
        [viewBackground addSubview:buttonOK];
        
        buttonCancel = [[UIButton alloc] initWithFrame:CGRectMake(150, 136, 90, 35)];
        [buttonCancel setTitle:@"Cancel" forState:UIControlStateNormal];
        [buttonCancel setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [buttonCancel.titleLabel setFont:[UIFont systemFontOfSize:15]];
        [buttonCancel setBackgroundColor:titleorange];
        [buttonCancel addTarget:self action:@selector(buttonCancelTouch:) forControlEvents:UIControlEventTouchUpInside];
        buttonCancel.layer.cornerRadius = 5;
        buttonCancel.layer.masksToBounds = YES;
        [viewBackground addSubview:buttonCancel];
        
        NSString *stringUserName = [[NSUserDefaults standardUserDefaults] valueForKey:@"kUserName"];
        if (!stringUserName || [stringUserName isEqualToString:@""])
        {
            buttonOK.frame = CGRectMake(81, 136, 118, 35);
            buttonCancel.hidden = YES;
        }
        else
        {
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(buttonCancelTouch:)];
            [self addGestureRecognizer:tap];
        }
    }
    return self;
}

- (void)buttonOKTouch:(id)sender
{
    NSString *username = txtUserName.text;
    
    // Input prevent all of a sudden more than 14 characters
    if (username.length > 0)
    {
        if (username.length > 14)
        {
            username = [username substringToIndex:14];
        }
        for (int i = 0; i < username.length; i++)
        {
            if ([kUserNameRange rangeOfString:[username substringWithRange:NSMakeRange(i, 1)]].location == NSNotFound)
            {
                return;
            }
        }
        [[NSUserDefaults standardUserDefaults] setValue:username forKey:@"kUserName"];
        [[NSUserDefaults standardUserDefaults] synchronize];

        [self removeFromSuperview];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"AddLock" object:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"startAnimt" object:nil];
        
        if (buttonCancel.hidden != YES)
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"kModifyPasswordSuccess" object:nil];
        }
    }
    else
    {
        [self makeToast:@"Enter username" duration:1.0 position:@"center"];
    }
}

- (void)buttonCancelTouch:(id)sender
{
    [self removeFromSuperview];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"AddLock" object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"startAnimt" object:nil];
}

- (void)textDidEndOnExit:(id)sender
{
    [sender resignFirstResponder];
}

- (void)keyboardWillShow:(NSNotification *)note
{
    NSValue *value = [[note userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect frame = [value CGRectValue];
    
    [self adjustToKeyboardBounds:frame];
}

- (void)keyboardWillHide:(NSNotification *)note
{
    [self adjustToKeyboardBounds:CGRectZero];
}

- (void)adjustToKeyboardBounds:(CGRect)bounds
{
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    CGFloat height = CGRectGetHeight(screenBounds) - CGRectGetHeight(bounds);
    CGRect frame = self.frame;
    frame.origin.y = (height - CGRectGetHeight(self.bounds)) / 2.0;
    
    if (CGRectGetMinY(frame) < 0)
    {
        NSLog(@"warning: dialog is clipped, origin negative (%f)", CGRectGetMinY(frame));
    }
    
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
        self.frame = frame;
    } completion:nil];
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    txtUserName.text = oldUserName;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([string isEqualToString:@"\n"] || [string isEqualToString:@""])
    {
        return YES;
    }
    else if (textField.text.length > 20)
    {
        return NO;
    }
    else
    {
        return [kUserNameRange rangeOfString:string].location != NSNotFound;
    }
}


@end
