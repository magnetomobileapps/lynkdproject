//
//  ChangePassword.m
//  QuickLock
//
//  Created by PUNDSK001 on 26/12/15.
//  Copyright © 2015 Bge. All rights reserved.
//

#import "Alarm.h"

@interface Alarm () <UITextFieldDelegate, ModifyAutoLockTime>
{
    UIBarButtonItem *btnFlexible, *btnDone;
    int selectedAlarmState;
    ModifyAutoLockTime *modifyAutoLockTimeView;
    UIAlertView *alert;
    IBOutlet UILabel *lblTitle;
}
@property (weak, nonatomic) IBOutlet UIButton *btnOff;
@property (weak, nonatomic) IBOutlet UIButton *btnSilentAlarm;
@property (weak, nonatomic) IBOutlet UIButton *btnAudibleAlarm;
@property (weak, nonatomic) IBOutlet UITextField *txtSec;

@end

@implementation Alarm

- (void)viewDidLoad
{
    [super viewDidLoad];
 
    [self borderOfButtons:_btnOff];
    [self borderOfButtons:_btnSilentAlarm];
    [self borderOfButtons:_btnAudibleAlarm];
    _txtSec.backgroundColor = lightgray;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setAlarmState:) name:@"setAlarmState" object:nil];
    
    [quickLockDeviceManage getAlarmState];
}

-(void)setAlarmState:(NSNotification *)notify
{    
    NSLog(@"alarm state : %d",quickLockDeviceManage.lockDevice.alarmPrevoiusState);
    NSLog(@"alarm delay : %d",quickLockDeviceManage.lockDevice.alarmDelay);
    selectedAlarmState = quickLockDeviceManage.lockDevice.alarmPrevoiusState;
    
    UIButton *sender = [[UIButton alloc]init];
    
    if (quickLockDeviceManage.lockDevice.alarmPrevoiusState == _btnOff.tag)
        sender = _btnOff;
    else if (quickLockDeviceManage.lockDevice.alarmPrevoiusState == _btnSilentAlarm.tag)
        sender = _btnSilentAlarm;
    else if (quickLockDeviceManage.lockDevice.alarmPrevoiusState == _btnAudibleAlarm.tag)
        sender = _btnAudibleAlarm;
    
    sender.backgroundColor = [UIColor colorWithRed:161.0f/255.0f green:213.0f/255.0f blue:98.0f/255.0f alpha:1.0f];
    sender.layer.borderColor = [UIColor colorWithRed:137.0/255.0 green:162.0/255.0 blue:76.0/255.0 alpha:1.0f].CGColor;
    
    _txtSec.text = [NSString stringWithFormat:@"%d",quickLockDeviceManage.lockDevice.alarmDelay];
}

-(void)doneTyping
{
    [_txtSec resignFirstResponder];
    self.view.transform = CGAffineTransformMakeTranslation(0, 0);
}

- (IBAction)selectMinutes:(id)sender
{
    quickLockDeviceManage.lockDevice.isAlarm = 1;
    modifyAutoLockTimeView = [[ModifyAutoLockTime alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    modifyAutoLockTimeView.delegate = self;
    [modifyAutoLockTimeView setValue:[NSNumber numberWithInt:quickLockDeviceManage.lockDevice.lock_AutoLockTime] forKey:@"selectIndex"];
    [self.view addSubview:modifyAutoLockTimeView];
}

-(void)borderOfButtons:(UIButton *)sender
{
    sender.layer.borderColor = lightgray.CGColor;
    sender.layer.borderWidth = 3;
}

- (IBAction)btnBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}

// text validation and limit
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    // validation for number
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return newLength <= 3;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)btnEvents:(UIButton *)sender
{
    _btnOff.backgroundColor = _btnSilentAlarm.backgroundColor = _btnAudibleAlarm.backgroundColor = [UIColor whiteColor];
    _btnAudibleAlarm.layer.borderColor = _btnSilentAlarm.layer.borderColor = _btnOff.layer.borderColor = lightgray.CGColor;
    
    sender.backgroundColor = [UIColor colorWithRed:161.0f/255.0f green:213.0f/255.0f blue:98.0f/255.0f alpha:1.0f];
    sender.layer.borderColor = [UIColor colorWithRed:137.0/255.0 green:162.0/255.0 blue:76.0/255.0 alpha:1.0f].CGColor;

    selectedAlarmState = (int)sender.tag;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == _txtSec)
    {
        if (screenSize.height == 568)
            self.view.transform = CGAffineTransformMakeTranslation(0, -30);
        if (screenSize.height == 480)
            self.view.transform = CGAffineTransformMakeTranslation(0, -50);
    }
}

- (void)viewDidDisappear:(BOOL)animated
{
    [alert dismissWithClickedButtonIndex:0 animated:NO];
}

- (IBAction)btnSave:(id)sender
{
    if (([_txtSec.text isEqualToString:@"0"] || _txtSec.text.length == 0) && selectedAlarmState != 0)
    {
        alert = [[UIAlertView alloc]initWithTitle:@"Sorry" message:@"Set delay minutes." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
        if (_txtSec.text.length == 1)
        {
            quickLockDeviceManage.lockDevice.delayTime = [[NSString stringWithFormat:@"0%@",_txtSec.text] intValue];
        }
        else
        {
            quickLockDeviceManage.lockDevice.delayTime = [_txtSec.text intValue];
        }
        
        if (selectedAlarmState == 2)
        {
            [self.view makeToast:@"Alarm set audible"];
        }
        else if (selectedAlarmState == 1)
        {
            [self.view makeToast:@"Alarm set silent"];
        }
        else
        {
            [self.view makeToast:@"Alarm OFF"];
            _txtSec.text = @"0";
            quickLockDeviceManage.lockDevice.delayTime = 0;
        }
        
        quickLockDeviceManage.lockDevice.alarmType = selectedAlarmState;
        [quickLockDeviceManage setAlarm:quickLockDeviceManage.lockDevice.alarmType delay:quickLockDeviceManage.lockDevice.delayTime];
        quickLockDeviceManage.lockDevice.alarmPrevoiusState = selectedAlarmState;
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.navigationController popViewControllerAnimated:NO];
        });
    }
}

- (void)returnAlarmMinutes:(int)time
{
    _txtSec.text = [NSString stringWithFormat:@"%d",time];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
