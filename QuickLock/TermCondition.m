//
//  TermCondition.m
//  QuickLock
//
//  Created by PUNDSK001 on 09/02/17.
//  Copyright © 2017 Bge. All rights reserved.
//

#import "TermCondition.h"
#import "ViewController.h"
#import "AlertLogin.h"

@interface TermCondition () 
{
    IBOutlet UIImageView *imgCheckBox;
    
    IBOutlet UITextView *textFieldTerms;
    IBOutlet UIButton *btnNext;
    IBOutlet UIButton *btnAgreeTermCondition;
}
@end

@implementation TermCondition

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"AgreeTerms"] isEqualToString:@"YES"])
//    {
//        AlertLogin *alert = [self.storyboard instantiateViewControllerWithIdentifier:@"AlertLogin"];
//        [self.navigationController pushViewController:alert animated:NO];
//        
////        ViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"root"];
////        [self.navigationController pushViewController:viewController animated:NO];
//    }
//    else
//    {
//        [btnNext setEnabled:FALSE];
//        [btnNext setBackgroundColor:[UIColor lightGrayColor]];
//
//        // Load T&C data
//    }
}
-(void)viewDidAppear:(BOOL)animated
{
    [textFieldTerms setContentOffset:CGPointZero animated:NO];

}
-(void)viewWillAppear:(BOOL)animated
{
//    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"AgreeTerms"] isEqualToString:@"YES"])
//    {
//        AlertLogin *alert = [self.storyboard instantiateViewControllerWithIdentifier:@"AlertLogin"];
//        [self.navigationController pushViewController:alert animated:NO];
//    }
    
}

- (IBAction)btnAgreeTermCondition:(UIButton *)sender
{
    if (btnAgreeTermCondition.tag == 0)
    {
        imgCheckBox.image = [UIImage imageNamed:@"Selected.png"];
        [btnNext setEnabled:TRUE];
        [btnNext setBackgroundColor:[UIColor blackColor]];
        sender.tag = 1;
    }
    else
    {
        imgCheckBox.image = [UIImage imageNamed:@"Unselected.png"];
        [btnNext setEnabled:FALSE];
        [btnNext setBackgroundColor:[UIColor lightGrayColor]];
        sender.tag = 0;
    }
}

- (IBAction)btnNext:(id)sender
{
    [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:@"AgreeTerms"];
    
    AlertLogin *alert = [self.storyboard instantiateViewControllerWithIdentifier:@"AlertLogin"];
    [self.navigationController pushViewController:alert animated:NO];
    
//    ViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"root"];
//    [self.navigationController pushViewController:viewController animated:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnClose:(id)sender {
    [self.navigationController popViewControllerAnimated:false];
}
@end
