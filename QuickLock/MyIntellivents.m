//
//  MyIntellivents.m
//  QuickLock
//
//  Created by PUNDSK001 on 04/01/16.
//  Copyright © 2016 Bge. All rights reserved.
//

#import "MyIntellivents.h"

@interface MyIntellivents () <UITableViewDelegate, UITableViewDataSource>
{
    NSMutableArray *arrHearTegister;
    IBOutlet UILabel *lblTitle;
}
@property (weak, nonatomic) IBOutlet UITableView *tblMyIntellivents;

@end

@implementation MyIntellivents

- (void)viewDidLoad
{
    [super viewDidLoad];

    arrHearTegister = [[NSMutableArray alloc] initWithObjects:@"Heat Register 1",@"Heat Register 1",@"Heat Register 1",@"Heat Register 1",@"Heat Register 1",@"Heat Register 1", nil];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrHearTegister.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    }
    cell.backgroundColor =[ UIColor clearColor];
    
    UILabel *lblHeatRegister = [[UILabel alloc]initWithFrame:CGRectMake(0, 2, _tblMyIntellivents.frame.size.width, 40)];
    lblHeatRegister.textColor = darkgray;
    lblHeatRegister.backgroundColor = cellBgColor;
    lblHeatRegister.text = [NSString stringWithFormat:@"  %@",arrHearTegister[indexPath.row]];
    lblHeatRegister.layer.masksToBounds = YES;
    lblHeatRegister.layer.cornerRadius = 8;
    lblHeatRegister.font = [UIFont systemFontOfSize:13];
    [cell.contentView addSubview:lblHeatRegister];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (IBAction)btnBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
