//
//  MenuViewController.m
//  SlideMenu
//
//  Created by Aryan Gh on 4/24/13.
//  Copyright (c) 2013 Aryan Ghassemi. All rights reserved.
//

#import "SlideNavigationController.h"
#import "LeftMenuViewController.h"
#import "SlideNavigationContorllerAnimatorFade.h"
#import "SlideNavigationContorllerAnimatorSlide.h"
#import "SlideNavigationContorllerAnimatorScale.h"
#import "SlideNavigationContorllerAnimatorScaleAndFade.h"
#import "SlideNavigationContorllerAnimatorSlideAndFade.h"

@interface LeftMenuViewController () <UITableViewDelegate, UITableViewDataSource>
{
    NSMutableArray *arrMenus, *arrMenuImages;
}
@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, assign) BOOL slideOutAnimationEnabled;

@end

@implementation LeftMenuViewController

- (id)initWithCoder:(NSCoder *)aDecoder
{
	self.slideOutAnimationEnabled = YES;
	return [super initWithCoder:aDecoder];
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	
    arrMenuImages = [NSMutableArray new];
    for (int i = 0; i <= 9; i++)
    {
        NSString *strImgName = [NSString stringWithFormat:@"menu%d.png",i];
        [arrMenuImages addObject:strImgName];
    }

//    arrMenus = [[NSMutableArray alloc]initWithObjects:@"Home",@"Padlock",@"Doorlock",@"Gunbox",@"Gunbox Echo",@"GunBox SK-1",@"Alert Notifications",@"Support",@"Buy Now", nil];

    arrMenus = [[NSMutableArray alloc]initWithObjects:@"Home", @"Padlock", @"Doorlock", @"Deadbolt", @"Gunbox", @"Gunbox Echo", @"Gunbox SK-1", @"Support", @"Buy Now", nil];

    
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    if (screenSize.height == 480)
        _tableView.scrollEnabled = YES;
    else
        _tableView.scrollEnabled = NO;
    
	self.view.layer.borderWidth = .6;
	self.view.layer.borderColor = [UIColor lightGrayColor].CGColor;
}

#pragma mark - UITableView Delegate & Datasrouce -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return arrMenus.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:nil];
	
    if (!cell)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    }
    cell.backgroundColor = [UIColor clearColor];

    UIImageView *img = [[UIImageView alloc]initWithFrame:CGRectMake(13, 10, 25, 25)];
    img.image = [UIImage imageNamed:arrMenuImages[indexPath.row]];
    img.contentMode = UIViewContentModeScaleAspectFit;
    [cell.contentView addSubview:img];
    
    UILabel *lblMenu = [[UILabel alloc]initWithFrame:CGRectMake(50, 10, 200, 25)];
    lblMenu.text = [arrMenus objectAtIndex:indexPath.row];
    lblMenu.font = [UIFont fontWithName:@"OpenSans-Semibold" size:13];
    [cell.contentView addSubview:lblMenu];
    
    DottedView *view = [[DottedView alloc] initWithFrame:CGRectMake(0, 0, 300, 15)];
    view.backgroundColor = [UIColor clearColor];
    [cell addSubview:view];

    
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];

	UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Iphone_5" bundle: nil];
	UIViewController *vc ;
	
    AppDelegate *appD = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appD.selectedMenu = 0;
    
	switch (indexPath.row)
	{
		case 0:
            if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"isDeviceConnected"] isEqualToString:@"YES"])
            {
                if (quickLockDeviceManage.lockDevice.lock_Type == DOORLOCK)
                    vc = [mainStoryboard instantiateViewControllerWithIdentifier: @"doorLockView"];
                else if (quickLockDeviceManage.lockDevice.lock_Type == INTELLIVENT)
                    vc = [mainStoryboard instantiateViewControllerWithIdentifier: @"Intellivent"];
                else
                    vc = [mainStoryboard instantiateViewControllerWithIdentifier: @"padLock"];                
            }
            else
            {
                vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"root"];
            }
			break;
			
		case 1:
            appD.selectedMenu = PADLOCK;
            [[NSNotificationCenter defaultCenter] postNotificationName:@"setDataOfSelectedLock" object:nil];
            vc = [mainStoryboard instantiateViewControllerWithIdentifier: @"myLocks"];
            break;
			
		case 2:
            appD.selectedMenu = DOORLOCK;
            [[NSNotificationCenter defaultCenter] postNotificationName:@"setDataOfSelectedLock" object:nil];
            vc = [mainStoryboard instantiateViewControllerWithIdentifier: @"myLocks"];
			break;
        case 3:
            appDelegate.selectedMenu = DEADBOLT;
            [[NSNotificationCenter defaultCenter] postNotificationName:@"setDataOfSelectedLock" object:nil];
            vc = [mainStoryboard instantiateViewControllerWithIdentifier: @"myLocks"];
            break;

			
		case 4:
            appD.selectedMenu = GUNBOX;
            [[NSNotificationCenter defaultCenter] postNotificationName:@"setDataOfSelectedLock" object:nil];
            vc = [mainStoryboard instantiateViewControllerWithIdentifier: @"myLocks"];
            break;

        case 5:
            appDelegate.selectedMenu = QUICKBOX;
            [[NSNotificationCenter defaultCenter] postNotificationName:@"setDataOfSelectedLock" object:nil];
            vc = [mainStoryboard instantiateViewControllerWithIdentifier: @"myLocks"];
            break;
            
//        case 5:
//            appDelegate.selectedMenu = MEDICINEBOX;
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"setDataOfSelectedLock" object:nil];
//            vc = [mainStoryboard instantiateViewControllerWithIdentifier: @"myLocks"];
//            break;
//
//        case 6:
//            appDelegate.selectedMenu = INTELLIVENT;
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"setDataOfSelectedLock" object:nil];
//            vc = [mainStoryboard instantiateViewControllerWithIdentifier: @"myLocks"];
//            break;
            
        case 6:
            appDelegate.selectedMenu = TALLGUNBOX;
            [[NSNotificationCenter defaultCenter] postNotificationName:@"setDataOfSelectedLock" object:nil];
            vc = [mainStoryboard instantiateViewControllerWithIdentifier: @"myLocks"];
            break;

//        case 6:
//            vc = [mainStoryboard instantiateViewControllerWithIdentifier: @"AlertNotifications"];
//            break;
            
        case 7:
            vc = [mainStoryboard instantiateViewControllerWithIdentifier: @"Support"];
            break;

        case 8:
//            vc = [mainStoryboard instantiateViewControllerWithIdentifier: @"AlertLogin"];
//            break;

            vc = [mainStoryboard instantiateViewControllerWithIdentifier: @"BuyNow"];
            break;

            
//            [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];
//			[[SlideNavigationController sharedInstance] popToRootViewControllerAnimated:YES];
//			return;
//			break;
	}
	
	[[SlideNavigationController sharedInstance] popToRootAndSwitchToViewController:vc
															 withSlideOutAnimation:NO
																	 andCompletion:nil];
}

@end
