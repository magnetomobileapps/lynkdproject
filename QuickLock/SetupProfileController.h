//
//  SetupProfileController.h
//  QuickLock
//
//  Created by Pavan Jadhav on 14/09/17.
//  Copyright © 2017 Bge. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SetupProfileController : UIViewController
{
    __weak IBOutlet UIButton *btnBack;
    __weak IBOutlet UILabel *lblTitle;
    __weak IBOutlet UIImageView *imgProfile;
    __weak IBOutlet MyTextField *txtUserName;
    __weak IBOutlet MyTextField *txtFirstName;
    
    __weak IBOutlet MyTextField *txtConfirmPassword;
    __weak IBOutlet MyTextField *txtPassword;
    __weak IBOutlet MyTextField *txtEmail;
    __weak IBOutlet MyTextField *txtAccountName;
    __weak IBOutlet MyTextField *txtLastName;
}

- (IBAction)btnBack:(id)sender;
- (IBAction)btnSubmit:(id)sender;
- (IBAction)btnAcceptOurTerms:(id)sender;
- (IBAction)btnRadioSelect:(id)sender;
- (IBAction)btnEditProfile:(id)sender;


@end
