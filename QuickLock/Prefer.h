//
//  Prefer.h
//  QuickLock
//
//  Created by administrator on 14-12-27.
//  Copyright (c) 2014年 Bge. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "QuickLockDeviceManage.h"
#import "DBAccess.h"

// Device Management class instance
QuickLockDeviceManage *quickLockDeviceManage;

// Database class instance
DBAccess *dbAccess;

// Password number
int remainInputCount;