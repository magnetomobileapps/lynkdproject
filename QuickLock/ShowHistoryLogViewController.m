//
//  ShowHistoryLogViewController.m
//  QuickLock
//
//  Created by administrator on 15-3-17.
//  Copyright (c) 2015年 Bge. All rights reserved.
//

#import "ShowHistoryLogViewController.h"
#import "Prefer.h"
#import "HistoryCell.h"

@interface ShowHistoryLogViewController()
{
    NSString *strFileName;
    NSMutableArray *arrayOflogHistoryList;
}

// Properties
@property (weak, nonatomic) IBOutlet UIButton *btnCreateLog;
@property (weak, nonatomic) IBOutlet UIButton *btnViewLog;
@property (weak, nonatomic) IBOutlet UITextView *textIVewLog;
@property (weak, nonatomic) IBOutlet UIView *viewLogText;
@property (weak, nonatomic) IBOutlet UIView *viewBg;
@property (weak, nonatomic) IBOutlet UIButton *btnSendFile;
@property (weak, nonatomic) IBOutlet UILabel *lblNoHistoryLog;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;

@end

@implementation ShowHistoryLogViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    arrDate = [NSMutableArray new];
    strLog = [NSMutableString new];
    dictForHistory = [NSMutableDictionary new];
    arrOfDateFoemat = [NSMutableArray new];
    sortingArray = [NSMutableArray new];
    arrDateTemp = [NSMutableArray new];
    temp = [NSMutableArray new];
    
    _lblTitle.textColor = titleorange;
    arrayOflogHistoryList = [[NSMutableArray alloc]init];
    strFileName = [NSString stringWithFormat:@"%@.text",quickLockDeviceManage.lockDevice.lock_Name];
    
    // Define Notification to show Log History
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ShowHistoryLog:) name:@"kReceiveHistoryLog" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopActivity) name:@"stopActivity" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hideMailComposeView) name:@"hideMailComposeView" object:nil];
    
    _viewBg.alpha = _viewLogText.alpha = 0.0;
    _viewBg.hidden = _viewLogText.hidden = YES;
    
    _btnCreateLog.layer.masksToBounds = _btnViewLog.layer.masksToBounds = _btnSendFile.layer.masksToBounds = YES;
    _btnCreateLog.layer.cornerRadius = _btnViewLog.layer.cornerRadius = _btnSendFile.layer.cornerRadius = 6;
    
    _viewLogText.layer.masksToBounds = YES;
    _viewLogText.layer.cornerRadius = 10;
    _viewLogText.layer.borderColor = darkgray.CGColor;
    _viewLogText.layer.borderWidth = 4;
    
    tableViewHistoryLog.separatorColor = [UIColor clearColor];
    
    dateformat = [NSDateFormatter new];
    [dateformat setLocale:[[NSLocale alloc]initWithLocaleIdentifier:@"en_US"]];
    
    self.view.userInteractionEnabled = NO;
    arrayHistoryLog = [NSMutableArray new];
    // Get Log History from Lock

    [quickLockDeviceManage getHistoryLog];
    [quickLockDeviceManage showMessageForBoxHistoryLog];
}

-(void)stopActivity
{
    [tableViewHistoryLog reloadData];
    self.view.userInteractionEnabled = YES;
    [quickLockDeviceManage stopSearchForBoxHistoryLog];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self devicehistorylog:[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"userId"]] withlock_UUID:[NSString stringWithFormat:@"%@",quickLockDeviceManage.lockDevice.lock_UUID] RFIdList:arrayOflogHistoryList];
    });
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
   
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"stopActivity" object:nil];
    [alert dismissWithClickedButtonIndex:0 animated:YES];
}

-(void)hideMailComposeView
{
    [self dismissViewControllerAnimated:NO completion:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"startAnimt" object:nil];
}

// Close the opened log file
- (IBAction)btnCloseLogFile:(id)sender
{
    [UIView animateWithDuration:0.3 animations:^{
        _viewBg.alpha = _viewLogText.alpha = 0.0;
    } completion:^(BOOL finished) {
        _viewBg.hidden = _viewLogText.hidden = YES;
    }];
}

// Open/view the created log file
- (IBAction)btnViewLog:(id)sender
{
    NSError *error;
    NSString *filePath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject] stringByAppendingPathComponent:strFileName];

    NSString *str = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:&error];
    
    [strLog writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:&error];
    if (error)
    {
        alert = [[UIAlertView alloc]initWithTitle:@"Sorry" message:@"The Log file couldn't be opened because it is not created." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
        _textIVewLog.text = str;

        _viewBg.hidden = _viewLogText.hidden = NO;
        [UIView animateWithDuration:0.3 animations:^{
            _viewBg.alpha = 0.6;
            _viewLogText.alpha = 1.0;
        }];
    }
}

// create log file .txt file
- (IBAction)btnCreateLogFile:(id)sender
{
    NSError *error;
    NSString *filePath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject] stringByAppendingPathComponent:strFileName];
    [strLog writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:&error];
    if (error)
    {
        alert = [[UIAlertView alloc]initWithTitle:@"Sorry" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
        [self.view makeToast:@"Log file has been created successfully"];
    }
}

// go Back
- (IBAction)btnBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}

- (void)ShowHistoryLog:(NSNotification *)notification
{
    _lblNoHistoryLog.hidden = YES;
    
    arrDate = [NSMutableArray new];
    dictForHistory = [NSMutableDictionary new];
    
    HistoryLog *historyLog = [notification object];
    
    // insert data in to object to show
    arrayHistoryLog = [self InsertData:historyLog ToArray:arrayHistoryLog];
    strLog = [[NSMutableString alloc]init];

    historyLog = arrayHistoryLog.lastObject;
//    NSString *deviceT = [NSString stringWithFormat:@"%@", historyLog.operateLockDevice];
    [arrayOflogHistoryList removeAllObjects];
    
    for (int i = 0; i < arrayHistoryLog.count; i++)
    {
        historyLog = arrayHistoryLog[arrayHistoryLog.count - i - 1];
        //  NSLog(@"%d, %d",arrayHistoryLog.count,i);
        //  NSLog(@"ccc : %d",(int)(arrayHistoryLog.count - i - 1));
        NSMutableDictionary *dictOfLog = [[NSMutableDictionary alloc]init];
        
        NSString *deviceType = [NSString stringWithFormat:@"%@", historyLog.operateLockDevice];
        
        [arrDate addObject:[self stringFromDate:historyLog.operateLockTime]];
        NSOrderedSet *mySet = [[NSOrderedSet alloc] initWithArray:arrDate];
        arrDate = [[NSMutableArray alloc] initWithArray:[mySet array]];
        
        NSString *strDate  = [self stringFromDate:historyLog.operateLockTime];
        NSString *strTime  = [self stringFromTime:historyLog.operateLockTime];
         NSString *strFullDate  = [self stringFromDateForLogFile:historyLog.operateLockTime];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSString *newDate = [dateFormatter stringFromDate:historyLog.operateLockTime];
        
//        {
//            "unlock_through": "Phone",
//            "name": "Pavan",
//            "date": "2017-08-28 14:55:05"
//        },
        
        [dictOfLog setValue:newDate forKey:@"date"];
        
        // if Retrived date is not available in the Dictionary
        if (![dictForHistory valueForKey:strDate])
        {
            NSMutableArray *array = [[NSMutableArray alloc]init];
            NSMutableDictionary *dictTemp = [[NSMutableDictionary alloc]init];
            
            if (historyLog.operateLockType == NFC)
            {
                [dictTemp setValue:deviceType forKey:@"type"];
                [dictOfLog setValue:deviceType forKey:@"unlock_through"];

                
                NSString *strIMEI = [dbAccess GetSingleRFIDTagsUserName:deviceType];
                if (strIMEI == nil || strIMEI == (id)[NSNull null] || [strIMEI isEqualToString:@"Name"])
                {
                    [dictTemp setValue:@"" forKey:@"name"];
                    [dictOfLog setValue:@"" forKey:@"name"];
                    [strLog appendString:[NSString stringWithFormat:@"ID : %@  Name : %@  Date : %@\n",deviceType ,@"" ,[self stringFromDateForLogFile:historyLog.operateLockTime]]];
                }
                else
                {
                    [dictTemp setValue:strIMEI forKey:@"name"];
                    [dictOfLog setValue:strIMEI forKey:@"name"];
                    [strLog appendString:[NSString stringWithFormat:@"ID : %@  Name : %@  Date : %@\n",deviceType ,strIMEI ,[self stringFromDateForLogFile:historyLog.operateLockTime]]];
                }
            }
            else
            {
                [dictTemp setValue:@"Phone" forKey:@"type"];
                [dictTemp setValue:deviceType forKey:@"name"];
                [dictOfLog setValue:deviceType forKey:@"name"];
                [dictOfLog setValue:@"Phone" forKey:@"unlock_through"];
                [strLog appendString:[NSString stringWithFormat:@"ID : %@  Name : %@  Date : %@\n",@"Phone" ,deviceType ,[self stringFromDateForLogFile:historyLog.operateLockTime]]];
            }
            [dictTemp setValue:strTime forKey:@"time"];
            
            [array insertObject:dictTemp atIndex:0];
            [arrayOflogHistoryList addObject:dictOfLog];
            [dictForHistory setObject:array forKey:strDate];
        }
        else
        {
            NSMutableArray *array = [[NSMutableArray alloc]init];
            array = [dictForHistory valueForKey:strDate];
            NSMutableDictionary *dictTemp = [[NSMutableDictionary alloc]init];
            
            if (historyLog.operateLockType == NFC)
            {
                NSString *strIm = [NSString stringWithFormat:@"%@",deviceType];
                [dictTemp setValue:strIm forKey:@"type"];
                [dictOfLog setValue:strIm forKey:@"unlock_through"];
                NSString *strIMEI = [dbAccess GetSingleRFIDTagsUserName:strIm];
                if (strIMEI == nil || strIMEI == (id)[NSNull null] || [strIMEI isEqualToString:@"Name"])
                {
                    [dictTemp setValue:@"" forKey:@"name"];
                    [dictOfLog setValue:@"" forKey:@"name"];

                    [strLog appendString:[NSString stringWithFormat:@"ID : %@  Name : %@  Date : %@\n",deviceType ,@"" ,[self stringFromDateForLogFile:historyLog.operateLockTime]]];
                }
                else
                {
                    [dictTemp setValue:strIMEI forKey:@"name"];
                    [dictOfLog setValue:@"" forKey:@"name"];

                    [strLog appendString:[NSString stringWithFormat:@"ID : %@  Name : %@  Date : %@\n",deviceType ,strIMEI ,[self stringFromDateForLogFile:historyLog.operateLockTime]]];
                }
            }
            else
            {
                [dictTemp setValue:@"Phone" forKey:@"type"];
                [dictTemp setValue:deviceType forKey:@"name"];
                
                [dictOfLog setValue:@"Phone" forKey:@"unlock_through"];
                [dictOfLog setValue:deviceType forKey:@"name"];

                [strLog appendString:[NSString stringWithFormat:@"ID : %@  Name : %@  Date : %@\n",@"Phone" ,deviceType ,[self stringFromDateForLogFile:historyLog.operateLockTime]]];
            }
            [dictTemp setValue:strTime forKey:@"time"];
            
            [array insertObject:dictTemp atIndex:0];
            [dictForHistory setObject:array forKey:strDate];
            [arrayOflogHistoryList addObject:dictOfLog];

        }
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [arrDate count];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 34)];
    view.backgroundColor = [UIColor whiteColor];
    
    UILabel *lbl = [[UILabel alloc]initWithFrame:CGRectMake(4, 8, self.view.frame.size.width-8, 28)];
    lbl.backgroundColor = orange;
    lbl.layer.masksToBounds = YES;
    lbl.layer.cornerRadius = 6;
    lbl.font = [UIFont systemFontOfSize:13];
    
    lbl.text = [NSString stringWithFormat:@"  %@",arrDate[section]];
    [view addSubview:lbl];
    
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // return array count of different arrays in the dictionary
    NSString *str = [arrDate objectAtIndex:section];
    NSArray *arr = [dictForHistory valueForKey:str];
    
    return  arr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cell";
    
    HistoryCell *cell = [tableViewHistoryLog dequeueReusableCellWithIdentifier:cellIdentifier];
    
    cell.lblLog.layer.cornerRadius = cell.lblName.layer.cornerRadius = cell.lblTime.layer.cornerRadius = 6;
    cell.lblLog.layer.masksToBounds = cell.lblName.layer.masksToBounds = cell.lblTime.layer.masksToBounds = YES;
    cell.lblLog.backgroundColor = cell.lblName.backgroundColor = cell.lblTime.backgroundColor = cellBgColor;
    cell.lblLog.textColor = cell.lblName.textColor = cell.lblTime.textColor = darkgray;
    
    // Show data fatching from dictionary
    NSString *str = [arrDate objectAtIndex:indexPath.section];
    NSArray *type = [dictForHistory valueForKey:str];
   
    cell.lblName.text = [NSString stringWithFormat:@"  %@",[[type valueForKey:@"name"] objectAtIndex:indexPath.row]];
    cell.lblLog.text = [NSString stringWithFormat:@"  %@",[[type valueForKey:@"type"] objectAtIndex:indexPath.row]];
    cell.lblTime.text = [NSString stringWithFormat:@"  %@",[[type valueForKey:@"time"] objectAtIndex:indexPath.row]];
    
    return cell;
}


- (NSString *)stringFromDateForLogFile:(NSDate *)date
{
    [dateformat setDateFormat:@"yyyy-MMM-dd HH:mm:ss"];
    return [dateformat stringFromDate:date];
}

- (NSString *)stringFromDate:(NSDate *)date
{
    [dateformat setDateFormat:@"EEE, dd MMM yyyy"];
    return [dateformat stringFromDate:date];
}

- (NSString *)stringFromTime:(NSDate *)date
{
    [dateformat setDateFormat:@"HH:mm:ss"];
    return [dateformat stringFromDate:date];
}

// Insertion algorithm, according to the time from small to large
- (NSMutableArray *)InsertData:(HistoryLog *)historyLog ToArray:(NSMutableArray *)sourceNumber
{
        if (!sourceNumber || sourceNumber.count == 0)
        {
                [sourceNumber addObject:historyLog];
                return sourceNumber;
        }
        
        //Smaller than the first
        if ([historyLog.operateLockTime timeIntervalSince1970] < [((HistoryLog *)sourceNumber[0]).operateLockTime timeIntervalSince1970])
        {
                [sourceNumber insertObject:historyLog atIndex:0];
                return sourceNumber;
        }
        else
        {
            if (arrayHistoryLog.count == 1)
            {
                [sourceNumber addObject:historyLog];
                return sourceNumber;
            }
        }
        for (int j = 1; j < sourceNumber.count; j++)
        {
                if ([historyLog.operateLockTime timeIntervalSince1970] >=
                    [((HistoryLog *)sourceNumber[j - 1]).operateLockTime timeIntervalSince1970] &&
                    [historyLog.operateLockTime timeIntervalSince1970] <
                    [((HistoryLog *)sourceNumber[j]).operateLockTime timeIntervalSince1970])
                {
                        [sourceNumber insertObject:historyLog atIndex:j];
                        break;
                }
                
                //Than the last big
                if ((j == sourceNumber.count - 1) &&
                    ([historyLog.operateLockTime timeIntervalSince1970] >=
                     [((HistoryLog *)sourceNumber[j]).operateLockTime timeIntervalSince1970]))
                {
                        [sourceNumber addObject:historyLog];
                        break;
                }
        }
        return sourceNumber;
}

- (IBAction)btnSendLogFile:(id)sender
{
    NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* foofile = [documentsPath stringByAppendingPathComponent:strFileName];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:foofile];
    if (fileExists)
    {
        if ([MFMailComposeViewController canSendMail])
        {
            [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:@"mailViewShow"];
            //hideMailComposeView
            
            MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
            controller.mailComposeDelegate = self;
            NSString *str = [NSString stringWithFormat:@"%@ History Log",quickLockDeviceManage.lockDevice.lock_Name];
            [controller setSubject:str];
            NSString *str2 = [NSString stringWithFormat:@"Hi, <br/>  PFA of %@ History Log. <br/><br/>- SafeTech",quickLockDeviceManage.lockDevice.lock_Name];
            [controller setMessageBody:str2 isHTML:YES];
            
            NSString *filePath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject] stringByAppendingPathComponent:strFileName];
            
            NSMutableData *pdfData = [NSMutableData dataWithContentsOfFile:filePath];
            [controller addAttachmentData:pdfData mimeType:@"text/plain"  fileName:str];
            if (controller)
                [self presentViewController:controller animated:YES completion:nil];
        }
        else
        {
            NSLog(@"CANT SEND EMAIL. POP VIEW HERE");
            alert = [[UIAlertView alloc]initWithTitle:@"No Mail Accounts" message:@"Please set up mail account in order to send email." delegate:self cancelButtonTitle:@"Proceed" otherButtonTitles:@"Retry", nil];
            alert.tag = 26;
            [alert show];
        }
    }
    else
    {
        alert = [[UIAlertView alloc]initWithTitle:@"Sorry" message:@"The Log file couldn't be sent because it is not created." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
}

-(void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *) error
{
    if (result == MFMailComposeResultSent)
    {
        NSLog(@"It's away!");
    }
    else if (result == MFMailComposeResultCancelled)
    {
        NSLog(@"Cancelled");
    }
    else if (result == MFMailComposeResultFailed)
    {
        NSLog(@"Failed");
    }

    [self dismissViewControllerAnimated:YES completion:nil];
    [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"mailViewShow"];
}

-(NSString *)CheckForNullparameter:(NSString*)param{
    
    NSString *param1=[NSString stringWithFormat:@"%@",param];
    if ( [param1 isEqual:(id)[NSNull null]] || param1==nil || [param1 isEqualToString:@""] || [param1 isKindOfClass:[NSNull class]]||[param1 isEqualToString:@"<null>"]||[param1 isEqualToString:@"(<null>)"]||[param1 isEqualToString:@"(null)"] || [param1 isEqualToString:@"<nil>"])
    {
        param1=@"";
        return param1;
    }
    else
    {
        return param1;
    }
    return @"";
}

-(void)devicehistorylog:(NSString *)user_id withlock_UUID:(NSString *)lock_UUID RFIdList:(NSArray *)logHistoryList
{
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:logHistoryList
                                                       options:0
                                                         error:&error];
    NSString *jsonString1 = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    NSString *strUrl = [NSString stringWithFormat:@"%@devicehistorylog?lock_UUID=%@&user_id=%@&historyLog=%@",BASEURL,[self CheckForNullparameter:lock_UUID],[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"userId"]],jsonString1];
    NSString* encodedUrl = [strUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    [Webservices getWithUrlString:encodedUrl success:^(NSDictionary *responce) {
        //Success
        NSLog(@"responce:%@",responce);
        //do code here
        if ([[responce valueForKey:@"status"] isEqualToString:@"1"]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSLog(@"History log Added successfully");
            });
        }else{
            NSLog(@"Share code not added");
        }
    } failure:^(NSError *error) {
        //error
        NSLog(@"error:%@",error.localizedDescription);
    }];
}

/*
 url – http://ec2-54-186-214-225.us-west-2.compute.amazonaws.com/lynkd/backend/web/api/devicehistorylog
 
 Request - Json
 {
 "lock_UUID": "UU123",
 "user_id": 8,
 "historyLog": [
 {
 "unlock_through": "Phone",
 "name": "Pavan",
 "date": "2017-08-28 14:55:05"
 },
 {
 "unlock_through": "wifi",
 "name": "Pavan",
 "date": "2017-08-28 14:53:22"
 }
 ]
 }
 */

@end
