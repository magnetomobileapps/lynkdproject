//
//  ViewController.m
//  QuickLock
//
//  Created by administrator on 14-12-26.
//  Copyright (c) 2014年 Bge. All rights reserved.
//

#import "ViewController.h"
#import "PublicMarco.h"
#import "Prefer.h"
#import "InputPassword.h"
#import "PadLockViewController.h"
#import "DoorLockViewController.h"
#import "SettingViewController.h"
#import "MyLocksViewController.h"
#import "MessageBox.h"
#import "AppDelegate.h"
#import "HamburgerDrawView.h"
#import "CircleButtonDrawView.h"
#import <Crashlytics/Crashlytics.h>

// APP lock time (in seconds)
#define kAPPLockTime 180
#define kMessageMoreThanThreeTimes @"Because you input password error more than 3 times, \nAPP already Lock,Please wait"
#define kMessageNoOperateLongTime @"Because you have not operated for a long time, \nAPP already Lock,Please wait"
#define kAPPLockMessage(message, second) [NSString stringWithFormat:@"%@ %d second", message, second]

@interface ViewController () <InputPasswordDelegate, UITextFieldDelegate>
{
    UIView *lockView;
    
    InputPassword *inputPasswordView;
    
    PadLockViewController *padLockVC;
    DoorLockViewController *doorLockVC;
    MyLocksViewController *myLocksVC;
    
    UIImageView *spinner;
    NSArray* images;
    int currentImage;
    float currentImageDisplayTimeSoFar;
    float imageDisplayTime;
    float animationStepTime;
    float animationTransitionTime;
    float imageAlpha;
    NSArray* imageLabels;
    
    UIButton* addButton;
    UIButton* backButton;
    UIView* lockUnlockWrapper;
}
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgPadlock;
@property (weak, nonatomic) IBOutlet UIImageView *imgDoorLock;
@property (strong, nonatomic) IBOutlet UIImageView *imgDeadBolt;
@property (weak, nonatomic) IBOutlet UIImageView *imgGunbox;
@property (weak, nonatomic) IBOutlet UIImageView *imgQuickBox;
@property (weak, nonatomic) IBOutlet UIImageView *imgMedicineBox;
@property (weak, nonatomic) IBOutlet UIImageView *imgIntellivent;
@property (weak, nonatomic) IBOutlet UIImageView *imgBigBartha;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [SlideNavigationController sharedInstance].portraitSlideOffset = appDelegate.portraitDrawer;
    
    [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"passwordCall"];
    
    _viewOfLocks.hidden = YES;
    currentImage = 0;
    currentImageDisplayTimeSoFar = 0.0f;
    animationStepTime = .02f;
    imageDisplayTime = 3.0f;
    animationTransitionTime = .8f;
    imageAlpha = 1.0f;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showSharecode:) name:@"kShareCode" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showSharecodeTimeBase:) name:@"kShareCodeTimeBase" object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(connectSuccess:)
                                                 name:@"kConnectSuccess"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(inputPassword:)
                                                 name:@"kInputPassword"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(passwordError:)
                                                 name:@"kPasswordError"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(disConnect:)
                                                 name:@"kDisConnect"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(RemoveMyLockVC:)
                                                 name:@"kRemoveMyLocksVC"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(BackToSearchDeviceThatConnected:)
                                                 name:@"kCancelSearchDevice"
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(AddLock:)
                                                 name:@"AddLock"
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(stopAnimation)
                                                 name:@"stopAnimation"
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(startAnimt)
                                                 name:@"startAnimt"
                                               object:nil];

    self.view.backgroundColor = [UIColor colorWithRed:241/256.0 green:242/256.0 blue:242/256.0 alpha:1.0]; // #F1F2F2
    
    // hamburgerDrawView
    HamburgerDrawView* hamburgerDrawView = [[HamburgerDrawView alloc] init];
    hamburgerDrawView.fillColor = [UIColor colorWithRed:128/256.0 green:130/256.0 blue:133/256.0 alpha:1.0]; // #808285
    hamburgerDrawView.opaque = NO;
    hamburgerDrawView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:hamburgerDrawView];
    
    // logoWrapper
    UIView* logoWrapper = [[UIView alloc] init];
    logoWrapper.translatesAutoresizingMaskIntoConstraints = NO;
    logoWrapper.hidden = YES;
    [self.view addSubview:logoWrapper];
    
    // logoDrawView
    LogoDrawView* logoDrawView = [[LogoDrawView alloc] init];
    logoDrawView.opaque = NO;
    logoDrawView.hidden = YES;
    logoDrawView.translatesAutoresizingMaskIntoConstraints = NO;
    [logoWrapper addSubview:logoDrawView];
    
    // imagesWrapper
    UIView* imagesWrapper = [[UIView alloc] init];
    imagesWrapper.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:imagesWrapper];
    
    // spinner
    spinner = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"spinnerLgOrange"]];
    spinner.translatesAutoresizingMaskIntoConstraints = NO;
    [imagesWrapper addSubview:spinner];
    
    // padlockLabel
    UILabel* padlockLabel = [[UILabel alloc] init];
//    padlockLabel.text = @"Padlock";
    padlockLabel.textColor = [UIColor colorWithRed:128/256.0 green:130/256.0 blue:133/256.0 alpha:1.0]; // #808285
    padlockLabel.font = [UIFont systemFontOfSize:24 weight:2];
    padlockLabel.textAlignment = NSTextAlignmentCenter;
    padlockLabel.translatesAutoresizingMaskIntoConstraints = NO;
//    padlockLabel.hidden = YES;
    [self.view addSubview:padlockLabel];

    // padlock
    UIImageView* padlock = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"padlock"]];
    padlock.alpha = imageAlpha;
    padlock.translatesAutoresizingMaskIntoConstraints = NO;
    [imagesWrapper addSubview:padlock];
    
    // doorlockLabel
    UILabel* doorlockLabel = [[UILabel alloc] init];
//    doorlockLabel.text = @"Quicklock Doorlock";
    doorlockLabel.textColor = [UIColor colorWithRed:128/256.0 green:130/256.0 blue:133/256.0 alpha:1.0]; // #808285
    doorlockLabel.font = [UIFont systemFontOfSize:24 weight:2];
    doorlockLabel.textAlignment = NSTextAlignmentCenter;
    doorlockLabel.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:doorlockLabel];
    
    // doorlock
    UIImageView* doorlock = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"doorlock"]];
    doorlock.alpha = imageAlpha;
    doorlock.translatesAutoresizingMaskIntoConstraints = NO;
    [imagesWrapper addSubview:doorlock];

    // gunboxLabel
    UILabel* gunboxLabel = [[UILabel alloc] init];
//    gunboxLabel.text = @"Gunbox";
    gunboxLabel.textColor = [UIColor colorWithRed:128/256.0 green:130/256.0 blue:133/256.0 alpha:1.0]; // #808285
    gunboxLabel.font = [UIFont systemFontOfSize:24 weight:2];
    gunboxLabel.textAlignment = NSTextAlignmentCenter;
    gunboxLabel.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:gunboxLabel];
    
    // gunbox
    UIImageView* gunbox = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"gunbox"]];
    gunbox.alpha = imageAlpha;
    gunbox.translatesAutoresizingMaskIntoConstraints = NO;
    [imagesWrapper addSubview:gunbox];

    // intelliventLabel
    UILabel* intelliventLabel = [[UILabel alloc] init];
//    intelliventLabel.text = @"Intellivent";
    intelliventLabel.textColor = [UIColor colorWithRed:128/256.0 green:130/256.0 blue:133/256.0 alpha:1.0]; // #808285
    intelliventLabel.font = [UIFont systemFontOfSize:24 weight:2];
    intelliventLabel.textAlignment = NSTextAlignmentCenter;
    intelliventLabel.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:intelliventLabel];
    
    // intellivent
    UIImageView* intellivent = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"GunboxEcho"]];
    intellivent.alpha = imageAlpha;
    intellivent.translatesAutoresizingMaskIntoConstraints = NO;
    [imagesWrapper addSubview:intellivent];
    
    ///////////////
    // RifleBox
    UILabel* lblRifleBox = [[UILabel alloc] init];
//    lblRifleBox.text = @"RifleBox";
    lblRifleBox.textColor = [UIColor colorWithRed:128/256.0 green:130/256.0 blue:133/256.0 alpha:1.0]; // #808285
    lblRifleBox.font = [UIFont systemFontOfSize:24 weight:2];
    lblRifleBox.textAlignment = NSTextAlignmentCenter;
    lblRifleBox.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:lblRifleBox];
    
    // RifleBox    RifleBox_ISO.png
    UIImageView *imgRifleBox = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"RifleBox"]];
    imgRifleBox.alpha = imageAlpha;
    imgRifleBox.translatesAutoresizingMaskIntoConstraints = NO;
    [imagesWrapper addSubview:imgRifleBox];
    ////////////
   
    // DeadBolt
    UILabel* lblDeadBolt = [[UILabel alloc] init];
    lblDeadBolt.textColor = [UIColor colorWithRed:128/256.0 green:130/256.0 blue:133/256.0 alpha:1.0];
    lblDeadBolt.font = [UIFont systemFontOfSize:24 weight:2];
    lblDeadBolt.textAlignment = NSTextAlignmentCenter;
    lblDeadBolt.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:lblDeadBolt];
    
    UIImageView *imgDeadBolt = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"deadbolt"]];
    imgDeadBolt.alpha = imageAlpha;
    imgDeadBolt.translatesAutoresizingMaskIntoConstraints = NO;
    [imagesWrapper addSubview:imgDeadBolt];
    ////////////
   
    
    // images in images array will be rotated through
    images = [[NSArray alloc] initWithObjects:padlock, doorlock, imgDeadBolt, gunbox, intellivent, imgRifleBox, nil];
    imageLabels = [[NSArray alloc] initWithObjects:padlockLabel, doorlockLabel, lblDeadBolt, gunboxLabel, intelliventLabel, lblRifleBox, nil];
    
    // lockUnlockWrapper
    lockUnlockWrapper = [[UIView alloc] init];
    lockUnlockWrapper.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:lockUnlockWrapper];
    lockUnlockWrapper.hidden = YES;
    
    // unlockDrawView
    CircleButtonDrawView* unlockDrawView = [[CircleButtonDrawView alloc] init];
    unlockDrawView.fillColor = [UIColor colorWithRed:230/256.0 green:231/256.0 blue:232/256.0 alpha:1.0]; // #E6E7E8
    unlockDrawView.strokeColor = [UIColor colorWithRed:128/256.0 green:130/256.0 blue:133/256.0 alpha:1.0]; // #808285
    unlockDrawView.opaque = NO;
    unlockDrawView.userInteractionEnabled = YES;
    [unlockDrawView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(unlockTap)]];
    unlockDrawView.translatesAutoresizingMaskIntoConstraints = NO;
    unlockDrawView.hidden = YES;
    [lockUnlockWrapper addSubview:unlockDrawView];
    
    // unlockLabel
    UILabel* unlockLabel = [[UILabel alloc] init];
    unlockLabel.text = @"UNLOCK";
    unlockLabel.textColor = [UIColor colorWithRed:128/256.0 green:130/256.0 blue:133/256.0 alpha:1.0]; // #808285
    unlockLabel.font = [UIFont systemFontOfSize:20 weight:2];
    unlockLabel.textAlignment = NSTextAlignmentCenter;
    unlockLabel.translatesAutoresizingMaskIntoConstraints = NO;
    unlockLabel.hidden = YES;
    [unlockDrawView addSubview:unlockLabel];
    
    // lockDrawView
    CircleButtonDrawView* lockDrawView = [[CircleButtonDrawView alloc] init];
    lockDrawView.fillColor = [UIColor colorWithRed:230/256.0 green:231/256.0 blue:232/256.0 alpha:1.0]; // #E6E7E8
    lockDrawView.strokeColor = [UIColor colorWithRed:128/256.0 green:130/256.0 blue:133/256.0 alpha:1.0]; // #808285
    lockDrawView.opaque = NO;
    lockDrawView.translatesAutoresizingMaskIntoConstraints = NO;
    lockDrawView.hidden = YES;
    [lockUnlockWrapper addSubview:lockDrawView];
    
    // lockLabel
    UILabel* lockLabel = [[UILabel alloc] init];
    lockLabel.text = @"LOCK";
    lockLabel.textColor = [UIColor colorWithRed:128/256.0 green:130/256.0 blue:133/256.0 alpha:1.0]; // #808285
    lockLabel.font = [UIFont systemFontOfSize:20 weight:2];
    lockLabel.textAlignment = NSTextAlignmentCenter;
    lockLabel.translatesAutoresizingMaskIntoConstraints = NO;
    lockLabel.hidden = YES;
    [lockDrawView addSubview:lockLabel];
    
    // addButton
    addButton = [[UIButton alloc] init];
    [addButton setTitle:@"Add Device" forState:UIControlStateNormal];
    [addButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    addButton.layer.cornerRadius = 5;
    addButton.layer.masksToBounds = YES;
    addButton.backgroundColor = [UIColor colorWithRed:72/255.0 green:140/255.0 blue:202/255.0 alpha:1.0]; // #808285
    addButton.contentEdgeInsets = UIEdgeInsetsMake(14, 0, 14, 0);
    [addButton addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(AddLock2:)]];
    addButton.translatesAutoresizingMaskIntoConstraints = NO;
    
    
    // backButton
    backButton = [[UIButton alloc] init];
    [backButton setTitle:@"Cancel" forState:UIControlStateNormal];
    [backButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    backButton.layer.cornerRadius = 5;
    backButton.layer.masksToBounds = YES;
    backButton.backgroundColor = [UIColor colorWithRed:72/255.0 green:140/255.0 blue:202/255.0 alpha:1.0]; // #808285
    backButton.contentEdgeInsets = UIEdgeInsetsMake(14, 0, 14, 0);
    [backButton addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(BackToSearchDeviceThatConnected:)]];
    backButton.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.view addSubview:backButton];
    [self.view addSubview:addButton];
    
    // appVersion
    appVersion = [[UILabel alloc] init];
//    appVersion.text = @"APP VERSION 1.0";
    appVersion.textColor = [UIColor blackColor];
    appVersion.font = [UIFont systemFontOfSize:11];
    appVersion.textAlignment = NSTextAlignmentCenter;
    appVersion.translatesAutoresizingMaskIntoConstraints = NO;
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *APPVersion =[NSString stringWithFormat:@"%@.%@",
                           [infoDictionary objectForKey:@"CFBundleShortVersionString"],
                           [infoDictionary objectForKey:@"CFBundleVersion"]];
    appVersion.text = [NSString stringWithFormat:@"APP Version : %@", APPVersion];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:[NSString stringWithFormat:@"%@", APPVersion] forKey:@"APPVersion"];
    [defaults synchronize];
    
    [self.view addSubview:appVersion];
    
    /******** Layout ********/
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    int spinnerSize = (int)(screenRect.size.width * 0.75);
    int imageSize = (int)(screenRect.size.width * 0.56);
    int buttonWidth = (int)(screenRect.size.width * 0.5);
    int padding = 20;
    int topPadding = 30;
    int logoWidth = (int)(screenRect.size.width * 0.7);
    int hamburgerSize = 25;
    int lockUnlockSize = (int)(screenRect.size.width * 0.35);
    
    NSDictionary* metrics = @{@"spinnerSize": @(spinnerSize), @"imageSize": @(imageSize), @"buttonWidth": @(buttonWidth), @"padding": @(padding), @"topPadding": @(topPadding), @"logoWidth": @(logoWidth), @"hamburgerSize": @(hamburgerSize), @"lockUnlockSize": @(lockUnlockSize)};
    NSDictionary *views = NSDictionaryOfVariableBindings(hamburgerDrawView, logoWrapper, logoDrawView, imagesWrapper, spinner, lockUnlockWrapper, unlockDrawView, unlockLabel, lockDrawView, lockLabel, addButton, backButton, appVersion);
    
    // #F89D29, 248 157 41
    
    // self.view
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-topPadding-[logoWrapper][imagesWrapper][addButton]-10-[appVersion]-padding-|" options:0 metrics:metrics views:views]];
    
    // hamburgerDrawView & logoDrawView
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|-padding-[hamburgerDrawView(==hamburgerSize)][logoWrapper]|" options:0 metrics:metrics views:views]];
    
    // hamburgerDrawView
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-topPadding-[hamburgerDrawView(==hamburgerSize)]" options:0 metrics:metrics views:views]];
    
    // logoDrawView
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"[logoDrawView(==logoWidth)]" options:0 metrics:metrics views:views]];
    [logoWrapper addConstraint:[NSLayoutConstraint constraintWithItem:logoDrawView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:logoWrapper attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:logoDrawView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:logoDrawView attribute:NSLayoutAttributeWidth multiplier:32/200.0 constant:0.0]];
    [logoWrapper addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[logoDrawView]|" options:0 metrics:metrics views:views]];
    
    // imagesWrapper
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:imagesWrapper attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0]];
    
    // spinner
    [imagesWrapper addConstraint:[NSLayoutConstraint constraintWithItem:spinner attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:imagesWrapper attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0]];
    [imagesWrapper addConstraint:[NSLayoutConstraint constraintWithItem:spinner attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:imagesWrapper attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0]];
    [imagesWrapper addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"[spinner(==spinnerSize)]" options:0 metrics:metrics views:views]];
    [imagesWrapper addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[spinner(==spinnerSize)]" options:0 metrics:metrics views:views]];
    
    // images
    for (int i = 0; i < images.count; i++)
    {
        UIImageView* imageView = (UIImageView*)images[i];
        UILabel* label = (UILabel*)imageLabels[i];
        imageView.hidden = YES;
        label.hidden = YES;
        NSDictionary *imageViews = @{@"imageView": imageView, @"label": label};
        
        // imageView
        [imagesWrapper addConstraint:[NSLayoutConstraint constraintWithItem:imageView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:imagesWrapper attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0]];
        [imagesWrapper addConstraint:[NSLayoutConstraint constraintWithItem:imageView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:imagesWrapper attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0]];
        [imagesWrapper addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"[imageView(==imageSize)]" options:0 metrics:metrics views:imageViews]];
        [imagesWrapper addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[imageView(==imageSize)]" options:0 metrics:metrics views:imageViews]];
        
        
        // label
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:label attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:imageView attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0]];
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[label]-10-[imageView]" options:0 metrics:metrics views:imageViews]];
    }
    
    ((UIImageView*)images[currentImage]).hidden = NO;
    
    // lockUnlockWrapper
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:lockUnlockWrapper attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:lockUnlockWrapper attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:appVersion attribute:NSLayoutAttributeTop multiplier:1.0 constant:-20]];
    [lockUnlockWrapper addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|[unlockDrawView(==lockUnlockSize)]-20-[lockDrawView(==lockUnlockSize)]|" options:0 metrics:metrics views:views]];

    // unlockDrawView
    [lockUnlockWrapper addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[unlockDrawView(==lockUnlockSize)]|" options:0 metrics:metrics views:views]];
    
    // unlockLabel
    [unlockDrawView addConstraint:[NSLayoutConstraint constraintWithItem:unlockLabel attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:unlockDrawView attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0]];
    [unlockDrawView addConstraint:[NSLayoutConstraint constraintWithItem:unlockLabel attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:unlockDrawView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0]];
    
    // lockDrawView
    [lockUnlockWrapper addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[lockDrawView(==lockUnlockSize)]|" options:0 metrics:metrics views:views]];
    
    // lockLabel
    [lockDrawView addConstraint:[NSLayoutConstraint constraintWithItem:lockLabel attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:lockDrawView attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0]];
    [lockDrawView addConstraint:[NSLayoutConstraint constraintWithItem:lockLabel attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:lockDrawView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0]];
    
    // addButton
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:addButton attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"[addButton(==buttonWidth)]" options:0 metrics:metrics views:views]];
   
    // backButton
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:backButton attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"[backButton(==buttonWidth)]" options:0 metrics:metrics views:views]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:backButton attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:addButton attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0]];
    
    // appVersion
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:appVersion attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0]];
    
    UIButton *btnMenu = [UIButton buttonWithType:UIButtonTypeCustom];
    btnMenu.frame = CGRectMake(10, 28, 35, 35);
    [btnMenu setImage:[UIImage imageNamed:@"navigation.png"] forState:UIControlStateNormal];
    [btnMenu addTarget:[SlideNavigationController sharedInstance] action:@selector(toggleLeftMenu) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnMenu];
    
    if ([[UIScreen mainScreen]bounds].size.height != 480)
    {
        _viewOfLocks.hidden = NO;
    }
    
    tmrAPPLock = [NSTimer scheduledTimerWithTimeInterval:.1f
                                                  target:self
                                                selector:@selector(animationLoading)
                                                userInfo:nil
                                                 repeats:NO];
    
//    UIButton *btn = [UIButton buttonWithType:UIButtonTypeSystem];
//    btn.frame = CGRectMake(20, 20, 60, 60);
//    [btn setTitle:@"Crash" forState:UIControlStateNormal];
//    [btn addTarget:self action:@selector(crashButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
//    [self.view addSubview:btn];
}

- (IBAction)crashButtonTapped:(id)sender {
    [[Crashlytics sharedInstance] crash];
}

-(void)showSharecode:(NSNotification *)notification
{
    HistoryLog *historyLog = [notification object];
    
    quickLockDeviceManage.lockDevice.share_Code = historyLog.shareCode;
    quickLockDeviceManage.lockDevice.share_Code_Count = [NSString stringWithFormat:@"%d",historyLog.userCount];
    
    [dbAccess UpdateShareCodeUser:quickLockDeviceManage.lockDevice WithLockID:quickLockDeviceManage.lockDevice.lock_ID];
}

-(void)showSharecodeTimeBase:(NSNotification *)notification
{
    HistoryLog *historyLog = [notification object];
    quickLockDeviceManage.lockDevice.share_Code_Time = historyLog.shareCodeTimeBase;
    
    // get date formate from dates
    quickLockDeviceManage.lockDevice.share_Code_Start_Time = historyLog.shareCodeStartTime;
    quickLockDeviceManage.lockDevice.share_Code_End_Time = historyLog.shareCodeEndTime;
    
    quickLockDeviceManage.lockDevice.str_share_Code_Stert_time = historyLog.strStartDat;
    quickLockDeviceManage.lockDevice.str_share_Code_End_time = historyLog.strEndDat;
    
    // get time from minutes
    quickLockDeviceManage.lockDevice.share_Code_Time1 = historyLog.shareCodeTime1;
    quickLockDeviceManage.lockDevice.share_Code_Time2 = historyLog.shareCodeTime2;
    
    quickLockDeviceManage.lockDevice.share_code_time_flag = historyLog.shareCodeTimeFlag;
    
    [dbAccess UpdateShareCodeTime:quickLockDeviceManage.lockDevice WithLockID:quickLockDeviceManage.lockDevice.lock_ID];
//    quickLockDeviceManage.lockDevice.lock_ID = [dbAccess InsertLockDevice:quickLockDeviceManage.lockDevice];
}

- (IBAction)btnIntellivent:(id)sender
{
    Intellivent *intellivent = [self.storyboard instantiateViewControllerWithIdentifier:@"Intellivent"];
    [self.navigationController pushViewController:intellivent animated:NO];
}

- (NSInteger)indexFromPixels:(NSInteger)pixels
{
    return 0;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return  YES;
}

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    NSString *strUserName = [[NSUserDefaults standardUserDefaults] valueForKey:@"kUserName"];
    if (strUserName == nil) {
        return NO;
    }
    return YES;
}



- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [inputPasswordView removeFromSuperview];
    inputPasswordView = nil;
    
    [_imgPadlock setImage:[UIImage imageNamed:@"0.png"]];
    [_imgDoorLock setImage:[UIImage imageNamed:@"1.png"]];
    [_imgDeadBolt setImage:[UIImage imageNamed:@"1.png"]];
    [_imgGunbox setImage:[UIImage imageNamed:@"2.png"]];
    [_imgQuickBox setImage:[UIImage imageNamed:@"3.png"]];
    [_imgMedicineBox setImage:[UIImage imageNamed:@"4.png"]];
    [_imgIntellivent setImage:[UIImage imageNamed:@"5.png"]];
    [_imgBigBartha setImage:[UIImage imageNamed:@"6.png"]];
    
    self.navigationController.navigationBarHidden = YES;
}

- (void)SetButtonCornerRadius5AndWhiteBorder2:(UIButton *)button
{
    [button.layer setCornerRadius:5];
    [button.layer setBorderWidth:2];
    [button.layer setBorderColor:[UIColor whiteColor].CGColor];
    [button.layer setMasksToBounds:YES];
}

// Enter this interface to start the search, to leave this screen to stop the search
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    animationStepTime = .015f;

    backButton.hidden = YES;
    addButton.hidden = NO;

    // First start requires the user to enter a user name used to make the history of the name
    NSString *stringUserName = [[NSUserDefaults standardUserDefaults] valueForKey:@"kUserName"];
    if (!stringUserName || [stringUserName isEqualToString:@""])
    {
        inputUserName *InputUserName = [[inputUserName alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        NSString *strName = [[NSUserDefaults standardUserDefaults] valueForKey:@"kUserName"];
        [InputUserName setValue:strName forKeyPath:@"oldUserName"];
        [self.view addSubview:InputUserName];

        return;
    }
    
    currentImage = 0;
//    tmrAPPLock =  ;
//    NSLog(@"currentImage : %d",currentImage);

    NSDate *date = [[NSUserDefaults standardUserDefaults] valueForKey:@"APPLockTime"];
    //Analyzing system time of 180 seconds after the lock will prompt the user to prevent lockup time cause the user to manually change the system time is not normal.
    //And in more than 180 seconds after the date will be set to empty, to prevent the above bug.
    if (date == nil
        || [[NSDate date] timeIntervalSinceDate:date] > kAPPLockTime
        || [[NSDate date] timeIntervalSinceDate:date] < 0)
    {
            [[NSUserDefaults standardUserDefaults] setValue:nil forKey:@"APPLockTime"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            quickLockDeviceManage.isSearchConnected = YES;
            [quickLockDeviceManage startSearch];
            [self startAnimation];
    }
    else
    {
            [self stopAnimation];
            
            if ([[NSUserDefaults standardUserDefaults] integerForKey:@"kAPPLockReason"] == 1)
            {
                    [self LockAPPForThreeMinutes:kAPPLockMessage(kMessageNoOperateLongTime, kAPPLockTime - (int)[[NSDate date] timeIntervalSinceDate:date])];
            }
            else
            {
                    [self LockAPPForThreeMinutes:kAPPLockMessage(kMessageMoreThanThreeTimes, kAPPLockTime - (int)[[NSDate date] timeIntervalSinceDate:date])];
            }
    }
}

- (void)viewDidDisappear:(BOOL)animated
{
        [super viewDidDisappear:animated];
        
        [quickLockDeviceManage stopSearch];
    
        [self stopAnimation];
}

- (IBAction)ShowMyLocksList:(id)sender
{
        if ([quickLockDeviceManage getIsConnected])
        {
                return;
        }
        
        myLocksVC = [self.storyboard instantiateViewControllerWithIdentifier:@"myLocks"];
        [self.navigationController pushViewController:myLocksVC animated:NO];
}

- (void)RemoveMyLockVC:(NSNotification *)notification
{
        [myLocksVC.navigationController popViewControllerAnimated:NO];
}

// Called when user presss Add Device button
- (IBAction)AddLock2:(id)sender
{
    if ([quickLockDeviceManage getIsConnected])
    {
        return;
    }
    
    backButton.hidden = NO;
    addButton.hidden = YES;
    quickLockDeviceManage.isSearchConnected = NO;
    [quickLockDeviceManage startSearch];
}

// called everytime when device will automatically get connected
- (IBAction)AddLock:(id)sender
{
    if ([quickLockDeviceManage getIsConnected])
    {
        return;
    }
    
    quickLockDeviceManage.isSearchConnected = NO;
    [quickLockDeviceManage startSearch];
    [self startAnimation];
}

- (IBAction)BackToSearchDeviceThatConnected:(id)sender
{
    backButton.hidden = YES;
    addButton.hidden = NO;
    quickLockDeviceManage.isSearchConnected = YES;
    [quickLockDeviceManage startSearch];
}

- (void)LockAPPForThreeMinutes:(NSString *)messageInfo
{
    if (!lockView)
    {
        lockView = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        [lockView setBackgroundColor:[UIColor blackColor]];
        UILabel *label = [[UILabel alloc] initWithFrame:lockView.frame];
        [label setBackgroundColor:[UIColor clearColor]];
        [label setNumberOfLines:0];
        [label setText:messageInfo];
        [label setTextColor:[UIColor whiteColor]];
        [label setTextAlignment:NSTextAlignmentCenter];
        [lockView addSubview:label];
        [self.view addSubview:lockView];
        [NSTimer scheduledTimerWithTimeInterval:.5f
                                         target:self
                                       selector:@selector(lockAPPTimeOut)
                                       userInfo:nil
                                        repeats:NO];
        [tmrAPPLock invalidate];
        tmrAPPLock = nil;
    }
}

// Lock APP time display
- (void)lockAPPTimeOut
{
    for (id label in lockView.subviews)
    {
        if ([label isKindOfClass:[UILabel class]])
        {
            NSDate *date = [[NSUserDefaults standardUserDefaults] valueForKey:@"APPLockTime"];
            //Analyzing system time of 180 seconds after the lock will prompt the user to prevent lockup time cause the user to manually change the system time is not normal.
            //And in more than 180 seconds after the date will be set to empty, to prevent the above bug.
            if (date == nil
                || [[NSDate date] timeIntervalSinceDate:date] > kAPPLockTime
                || [[NSDate date] timeIntervalSinceDate:date] < 0)
            {
                [[NSUserDefaults standardUserDefaults] setValue:nil forKey:@"APPLockTime"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [lockView removeFromSuperview];
                lockView = nil;
                tmrAPPLock = [NSTimer scheduledTimerWithTimeInterval:.1f
                                                              target:self
                                                            selector:@selector(animationLoading)
                                                            userInfo:nil
                                                             repeats:NO];
                quickLockDeviceManage.isSearchConnected = YES;
                [quickLockDeviceManage startSearch];
                [self startAnimation];
            }
            else
            {
                NSString *message = @"";
                if ([[NSUserDefaults standardUserDefaults] integerForKey:@"kAPPLockReason"] == 1)
                {
                    message = kAPPLockMessage(kMessageNoOperateLongTime, kAPPLockTime - (int)[[NSDate date] timeIntervalSinceDate:date]);
                }
                else
                {
                    message = kAPPLockMessage(kMessageMoreThanThreeTimes, kAPPLockTime - (int)[[NSDate date] timeIntervalSinceDate:date]);
                }
                [((UILabel *)label) setText:message];
                [NSTimer scheduledTimerWithTimeInterval:.5f
                                                 target:self
                                               selector:@selector(lockAPPTimeOut)
                                               userInfo:nil
                                                repeats:NO];
            }
            break;
        }
    }
}

- (void)startAnimation
{
    isSearching = YES;
    spinner.hidden = NO;
}

- (void)stopAnimation
{
    isSearching = NO;
    //spinner.hidden = YES;
    [tmrAPPLock invalidate];
    tmrAPPLock = nil;
    
    [quickLockDeviceManage stopSearch];
}

-(void)startAnimt
{
//    currentImage = 0;
    [tmrAPPLock invalidate];
    tmrAPPLock = [NSTimer scheduledTimerWithTimeInterval:.015f
                                                  target:self
                                                selector:@selector(animationLoading)
                                                userInfo:nil
                                                 repeats:NO];

}

- (void)animationLoading
{
        if (isSearching)
        {
            [spinner setTransform:CGAffineTransformRotate(spinner.transform,  M_PI / 96)];
            // -M_PI / 96
            // images
            currentImageDisplayTimeSoFar += animationStepTime;
            if (currentImageDisplayTimeSoFar > imageDisplayTime)
            {
                // reset current time
                currentImageDisplayTimeSoFar = 0.0f;
                
                int nextImageIndex = (currentImage+1) % (images.count);
                
                NSLog(@"currentImage : %d",currentImage);
                NSLog(@"nextImageIndex : %d",nextImageIndex);
                
                [self fadeImageFromIndex:currentImage toIndex:nextImageIndex isConnected:NO];
                currentImage = nextImageIndex;
            }
            
            tmrAPPLock = [NSTimer scheduledTimerWithTimeInterval:animationStepTime
                                                          target:self
                                                        selector:@selector(animationLoading)
                                                        userInfo:nil
                                                         repeats:NO];
        }
}

- (void)fadeImageFromIndex:(int)fromIndex toIndex:(int)toIndex isConnected:(BOOL)isConnected
{
    // fromImage
    UIView* fromImage = ((UIView*)images[fromIndex]);
    [UIView transitionWithView:fromImage
                      duration:animationTransitionTime
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:NULL
                    completion:NULL];
    fromImage.hidden = YES;
    
    // toImage
        UIView* toImage = ((UIView*)images[toIndex]);
        [UIView transitionWithView:toImage
                          duration:animationTransitionTime
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:NULL
                        completion:NULL];
        toImage.hidden = NO;
        toImage.alpha = isConnected ? 1.0 : imageAlpha;
    
    // toLabel
    if (isConnected) {
        UIView* toLabel = ((UIView*)imageLabels[toIndex]);
        [UIView transitionWithView:toLabel
                          duration:animationTransitionTime
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:NULL
                        completion:NULL];
        toLabel.hidden = NO;
    }
    
}

- (void)inputPassword:(NSNotification *)notification
{
    inputPasswordView = [[InputPassword alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    inputPasswordView.delegate = self;
    inputPasswordView.isModifyPassword = NO;
    [self.view addSubview:inputPasswordView];
}

// Enter the password of return
- (void)returnPassword:(NSString *)password
{
    quickLockDeviceManage.lockDevice.lock_Password = password;
    [quickLockDeviceManage verifyPassword];
}

- (void)cancelModifyPassword
{
    //As long as the password in the home point the Cancel button will return home rather than addlock interface
    [inputPasswordView removeFromSuperview];
    inputPasswordView = nil;
//    [buttonBack setHidden:YES];
    backButton.hidden = YES;
//        [buttonAddLock.superview setHidden:NO];
    addButton.hidden = NO;
    
    [quickLockDeviceManage initiativeDisConnect];
}

// Enter the password wrong
- (void)passwordError:(NSNotification *)notification
{
        // Enter the number of minus one
    if (remainInputCount > 1)
    {
//                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Input error"
//                                                                message:[NSString stringWithFormat:@"You only have %d chance.", --remainInputCount]
//                                                               delegate:nil
//                                                      cancelButtonTitle:@"OK"
//                                                      otherButtonTitles:nil];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                        message:@"WRONG PASSWORD\nTRY AGAIN!"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"PasswordTextBlank" object:nil];
        
        [alert show];
        
    }
    else
    {
        // Entered incorrectly three times in a row, locked APP three minutes
        [[NSUserDefaults standardUserDefaults] setValue:[NSDate date] forKey:@"APPLockTime"];
        [[NSUserDefaults standardUserDefaults] setValue:@2 forKey:@"kAPPLockReason"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [self LockAPPForThreeMinutes:kAPPLockMessage(kMessageMoreThanThreeTimes, kAPPLockTime)];
        [inputPasswordView removeFromSuperview];
        inputPasswordView = nil;
        remainInputCount = 3;
    }
}

// connection succeeded
- (void)connectSuccess:(NSNotification *)notification
{
    NSLog(@"lock_AutoLockTime : %d",quickLockDeviceManage.lockDevice.lock_AutoLockTime);
    [tmrAPPLock invalidate];
    tmrAPPLock = nil;
    
    switch (quickLockDeviceManage.lockDevice.lock_Type)
    {
        case PADLOCK:
        {
            [_imgPadlock setImage:[UIImage imageNamed:@"ic_padlock_second.png"]];
            break;
        }
        case DOORLOCK:
        {
            [_imgDoorLock setImage:[UIImage imageNamed:@"ic_doorlock.png"]];
            break;
        }
        case GUNBOX:
        {
            [_imgGunbox setImage:[UIImage imageNamed:@"ic_gunbox.png"]];
            break;
        }
        case QUICKBOX:
        {
            [_imgQuickBox setImage:[UIImage imageNamed:@"ic_quickbox.png"]];
            break;
        }
//        case MEDICINEBOX:
//        {
//            [_imgMedicineBox setImage:[UIImage imageNamed:@"ic_medicine_box.png"]];
//            break;
//        }
//        case INTELLIVENT:
//        {
//            [_imgIntellivent setImage:[UIImage imageNamed:@"ic_intelliventbox.png"]];
//            break;
//        }
        case TALLGUNBOX:
        {
            [_imgBigBartha setImage:[UIImage imageNamed:@"ic_riflebox_second.png"]];
            break;
        }
        case DEADBOLT:
        {
            [_imgDeadBolt setImage:[UIImage imageNamed:@"ic_deadbolt.png"]];
            break;
        }
        case RFWRITER:
        {
            [_imgPadlock setImage:[UIImage imageNamed:@"ic_padlock_second.png"]];
            break;

        }
            
    }
    // Remove the password screen, enter the appropriate interface based on the type of device
    if (inputPasswordView)
    {
        [inputPasswordView RemoveAllView];
    }
    
    [self performSelector:@selector(InputMainViewController) withObject:nil afterDelay:1.0f];
}

// Late one second to enter the main interface (after first dismiss immediately present may lead to an interface after bomb not out)
- (void)InputMainViewController
{
    [inputPasswordView removeFromSuperview];
    
    // If, before the connection succeeds click to enter the mylock interface, retreated home. Click to prevent excessive lead into the mylocks interface is still a successful connection status.
    if([self.navigationController.topViewController isKindOfClass:[MyLocksViewController class]])
    {
        // Code Created for pop to view Controller previous was poptp root view controller

        BOOL isClassAvailable = YES;
        for (UIViewController *controller in self.navigationController.viewControllers)
        {
            if ([controller isKindOfClass:[ViewController class]])
            {
                [self.navigationController popToViewController:controller animated:NO];
                isClassAvailable = NO;
                break;
            }
        }
        if (isClassAvailable == YES)
        {
            ViewController *disc=[self.storyboard instantiateViewControllerWithIdentifier:@"root"];
            [self.navigationController pushViewController:disc animated:NO];
        }

//        [self.navigationController popToRootViewControllerAnimated:NO];

        [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"isDeviceConnected"];
    }
    
    // Prevent duplication resulting in two consecutive push the stack, if the judgment is not the current top viewcontroller viewcontroller not perform any action
    if(![self.navigationController.topViewController isKindOfClass:[ViewController class]])
    {
        return;
    }

    [self stopAnimation];
//       [self fadeImageFromIndex:currentImage toIndex:quickLockDeviceManage.lockDevice.lock_Type-1 isConnected:YES];
    [self fadeImageFromIndex:currentImage toIndex:0 isConnected:YES];
    backButton.hidden = YES;
    
    switch (quickLockDeviceManage.lockDevice.lock_Type)
    {
        case PADLOCK: // padLock
        case GUNBOX: // GunBox
        case QUICKBOX: // QuickBox
//        case MEDICINEBOX: // MedicineBox
        case TALLGUNBOX: // Big-Bertha
        {
            padLockVC = [self.storyboard instantiateViewControllerWithIdentifier:@"padLock"];
            [self.navigationController pushViewController:padLockVC animated:NO];
            break;
        }
        case DOORLOCK: // DoorLock
        case DEADBOLT: // DeadBolt
        {
            doorLockVC = [self.storyboard instantiateViewControllerWithIdentifier:@"doorLockView"];
            [self.navigationController pushViewController:doorLockVC animated:NO];
            break;
        }
            
        case RFWRITER: // Intellivent  //Pavan
        {
            padLockVC = [self.storyboard instantiateViewControllerWithIdentifier:@"RFWriterViewController"];
            [self.navigationController pushViewController:padLockVC animated:NO];
            break;
        }
//        case INTELLIVENT: // Intellivent  //Pavan
//        {
//            Intellivent *intellivent = [self.storyboard instantiateViewControllerWithIdentifier:@"Intellivent"];
//            [self.navigationController pushViewController:intellivent animated:NO];
//            break;
//        }
    }
}

//Disconnect the interface when the interface is removed
- (void)disConnect:(NSNotification *)notification
{
    // If lockID 0 represents the first connection, if not empty inputPassword View showing still enter the password screen.
    //Meet both represent a user exceeds a certain time (hardware provision, now one minute) do not operate, also locks APP three minutes.
    if (inputPasswordView && quickLockDeviceManage.lockDevice.lock_ID == 0)
    {
        [inputPasswordView removeFromSuperview];
        inputPasswordView = nil;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"AddLock" object:nil];
    }
    
    // When disconnected save all settings data, set up the instance is empty
    [dbAccess UpdateLockDevice:quickLockDeviceManage.lockDevice WithLockID:quickLockDeviceManage.lockDevice.lock_ID];
    quickLockDeviceManage.lockDevice = nil;
    
    [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"passwordCall"];
    
// Code Created for pop to view Controller previous was poptp root view controller
    BOOL isClassAvailable = YES;
    for (UIViewController *controller in self.navigationController.viewControllers)
    {
        if ([controller isKindOfClass:[ViewController class]])
        {
            [self.navigationController popToViewController:controller animated:NO];
            isClassAvailable = NO;
            break;
        }
    }
    if (isClassAvailable == YES)
    {
        ViewController *disc=[self.storyboard instantiateViewControllerWithIdentifier:@"root"];
        [self.navigationController pushViewController:disc animated:NO];
    }
    
//    [self.navigationController popToRootViewControllerAnimated:NO];

    [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"isDeviceConnected"];
}

- (void)didReceiveMemoryWarning
{
        [super didReceiveMemoryWarning];
        // Dispose of any resources that can be recreated.
}

#pragma mark - first entered into the APP username
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *username = [alertView textFieldAtIndex:0].text;
    
    // Voice input prevent all of a sudden more than 14 characters
    if (username.length > 0)
    {
        if (username.length > 14)
        {
            username = [username substringToIndex:14];
        }
        for (int i = 0; i < username.length; i++)
        {
            if ([kUserNameRange rangeOfString:[username substringWithRange:NSMakeRange(i, 1)]].location == NSNotFound)
            {
                [self viewDidAppear:YES];
                return;
            }
        }
        [[NSUserDefaults standardUserDefaults] setValue:username forKey:@"kUserName"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    [self viewDidAppear:YES];
}

// Limit the input box can only enter UserName Range allowed characters and a maximum length of 14
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([string isEqualToString:@"\n"] || [string isEqualToString:@""])
    {
        return YES;
    }
    else if (textField.text.length > 13)
    {
        return NO;
    }
    else
    {
        return [kUserNameRange rangeOfString:string].location != NSNotFound;
    }
}

- (void)unlockTap
{
    [quickLockDeviceManage openLock:YES];
    
    // Set automatic locking time
    [quickLockDeviceManage setAutoLockTime:quickLockDeviceManage.lockDevice.lock_AutoLockTime];
}

@end
