//
//  PadLockViewController.h
//  QuickLock
//
//  Created by 王洋 on 14/12/27.
//  Copyright (c) 2014年 Bge. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PadLockViewController : UIViewController
{
        IBOutlet UILabel *labelName;
        
        IBOutlet UILabel *labelAPPVersion;

        IBOutlet UIButton *buttonAutoUnlock;
        
        IBOutlet UIButton *buttonUnlock;
        
        IBOutlet UIButton *buttonSet;
    BOOL isUnlockNotPassToggle, isSharecodeNotShown, isAutoLockPressed;
    int btnState;
}
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constantLogoBottom;

- (IBAction)buttonAutoUnlockTouch:(id)sender;
- (IBAction)buttonUnlockTouch:(id)sender;
- (IBAction)buttonSetTouch:(id)sender;

@end
