//
//  AlertMyDevices.m
//  QuickLock
//
//  Created by Ankit on 27/12/15.
//  Copyright (c) 2015 Bge. All rights reserved.
//

#import "AlertMyDevices.h"
#import "ConfigureDevice.h"

@interface AlertMyDevices ()<UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>
{
    ConfigureDevice *configureDevice;
}
// Properties
@property (weak, nonatomic) IBOutlet UITableView *tblDevices;
@property (weak, nonatomic) IBOutlet UIButton *btnAddDevice;
@property (weak, nonatomic) IBOutlet UIView *viewBgPop;
@property (weak, nonatomic) IBOutlet UIView *viewPopup;
@property (weak, nonatomic) IBOutlet UIButton *btnPopupYes;
@property (weak, nonatomic) IBOutlet UIButton *btnPopupNo;
@property (weak, nonatomic) IBOutlet UILabel *lblNoDevices;
@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblDeviceName;
@property (strong, nonatomic) IBOutlet UILabel *lblDelete;
@property (weak, nonatomic) IBOutlet UIView *viewOfTagInput;
@property (weak, nonatomic) IBOutlet UIButton *btnSaveTag;
@property (weak, nonatomic) IBOutlet UITextField *txtTagName;
@property (strong, nonatomic) IBOutlet UILabel *lblEnterTextTitle;


@end

@implementation AlertMyDevices
//@synthesize lockDevice;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Defined Notification center to get History Log
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ShowHistoryLogRFID:) name:@"kReceiveHistoryLog" object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(callAfterAddNewRFID:) name:@"kcallAfterAddNewRFID" object:nil];
    
//    lockDevice = [LockDevice new];

    configureDevice = [self.storyboard instantiateViewControllerWithIdentifier:@"ConfigureDevice"];

    _viewBgPop.alpha = 0.0f;
    _viewPopup.alpha = 0.0f;
    _viewBgPop.backgroundColor = [UIColor blackColor];
    
    _viewPopup.layer.masksToBounds = _viewOfTagInput.layer.masksToBounds = YES;
    _viewPopup.layer.cornerRadius = _viewOfTagInput.layer.cornerRadius = 10;
    _viewPopup.layer.borderWidth = _viewOfTagInput.layer.borderWidth= 4;
    _viewPopup.layer.borderColor = _viewOfTagInput.layer.borderColor = darkgray.CGColor;
    _txtTagName.backgroundColor = lightgray;
    
//    arrayHistoryLog = [NSMutableArray new];
}

-(void)viewWillAppear:(BOOL)animated
{
    if (appDelegate.strSelectedPush == nil)
    {
        arrName = [[NSMutableArray alloc]initWithObjects:@"Gunbox Echo",@"Door Lock", nil];
        
        _lblNoDevices.text = @"No Devices Found";
        [_btnAddDevice setTitle:@"Add New Device" forState:UIControlStateNormal];
        _lblName.text = @"Device Name";
        _lblTitle.text = @"MY DEVICES";
        
        _lblDeviceName.text = @"(Select Device to Configure)";//appDelegate.strSelectedAlertDevice;
        _lblDeviceName.font = [UIFont systemFontOfSize:12];
        _lblDelete.text = @"Are you sure you want to delete this device?";
//        _lblEnterTextTitle.text = @"Add Phone Number";
//        _txtTagName.placeholder = @"Phone Number";
//        _txtTagName.keyboardType = UIKeyboardTypePhonePad;
    }
    else if ([appDelegate.strSelectedPush isEqualToString:@"phone"])
    {
        arrName = [[NSMutableArray alloc]initWithObjects:@"8834567891",@"9955889930", nil];
        
        _lblNoDevices.text = @"No Phone Numbers Found";
        [_btnAddDevice setTitle:@"Add Phone Number" forState:UIControlStateNormal];
        _lblName.text = @"Phone Number";
        _lblTitle.text = @"PUSH PHONE NUMBER";
        
        _lblDeviceName.text = appDelegate.strSelectedAlertDevice;
        _lblDeviceName.font = [UIFont boldSystemFontOfSize:18];
        _lblDelete.text = @"Are you sure you want to delete this number?";
        _lblEnterTextTitle.text = @"Add Phone Number";
        _txtTagName.placeholder = @"Phone Number";
        _txtTagName.keyboardType = UIKeyboardTypePhonePad;
    }
    else if ([appDelegate.strSelectedPush isEqualToString:@"email"])
    {
        arrName = [[NSMutableArray alloc]initWithObjects:@"abc@gmail.com",@"defghij@gmail.com", nil];
        
        _lblNoDevices.text = @"No Email IDs Found";
        [_btnAddDevice setTitle:@"Add New Email" forState:UIControlStateNormal];
        _lblName.text = @"Email Address";
        _lblTitle.text = @"PUSH EMAIL";
        
        _lblDeviceName.text = appDelegate.strSelectedAlertDevice;
        _lblDeviceName.font = [UIFont boldSystemFontOfSize:18];
        _lblDelete.text = @"Are you sure you want to delete this email address?";
        _lblEnterTextTitle.text = @"Add Email Address";
        _txtTagName.placeholder = @"Email Address";
        _txtTagName.keyboardType = UIKeyboardTypeEmailAddress;
    }
    [_tblDevices reloadData];
}

- (IBAction)btnSaveTxt:(UIButton *)sender
{
//    NSString *strIMEI = arrTagId[sender.tag];
//    quickLockDeviceManage.lockDevice.TAG_USERNAME = _txtTagName.text;
//    [dbAccess UpdateRFIDName:quickLockDeviceManage.lockDevice WithIMEI:strIMEI];
    
    [UIView animateWithDuration:0.3f animations:^{
        _viewOfTagInput.transform = CGAffineTransformMakeTranslation(0, 0);
        _viewBgPop.alpha = 0.0f;
        _viewOfTagInput.alpha = 0.0f;
    } completion:^(BOOL finished) {
        _viewBgPop.hidden = YES;
        _viewOfTagInput.hidden = YES;
        
        _txtTagName.text = @"";
        [_txtTagName resignFirstResponder];
        
//        appDelegate.TAG_IMEI = @"";
//        arrayLock = [dbAccess GetRFIDTags];
//        [_tblRfidTags reloadData];
    }];
}

- (IBAction)btnCancelTtxt:(id)sender
{
    [UIView animateWithDuration:0.3f animations:^{
        _viewOfTagInput.transform = CGAffineTransformMakeTranslation(0, 0);
        _viewBgPop.alpha = 0.0f;
        _viewOfTagInput.alpha = 0.0f;
        _viewPopup.alpha = 0.0f;
    } completion:^(BOOL finished) {
        _viewBgPop.hidden = YES;
        _viewOfTagInput.hidden = YES;
        _viewPopup.hidden = YES;
        
        [_txtTagName resignFirstResponder];
        _txtTagName.text = @"";
    }];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [UIView animateWithDuration:0.3f animations:^{
        _viewOfTagInput.transform = CGAffineTransformMakeTranslation(0, 0);
    }];
    [textField resignFirstResponder];
    return YES;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    //if (screenSize.height <= 568)
    {
        [UIView animateWithDuration:0.3f animations:^{
            _viewOfTagInput.transform = CGAffineTransformMakeTranslation(0, -60);
        }];
    }
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([appDelegate.strSelectedPush isEqualToString:@"phone"])
    {
        if ([string isEqualToString:@"\n"] || [string isEqualToString:@""])
        {
            return YES;
        }
        else if (textField.text.length > 9)
        {
            return NO;
        }
        else
        {
            return [kDigitalRange rangeOfString:string].location != NSNotFound;
        }
    }
    else if ([appDelegate.strSelectedPush isEqualToString:@"email"])
    {
        if ([string isEqualToString:@"\n"] || [string isEqualToString:@""])
        {
            return YES;
        }
        else if (textField.text.length > 30)
        {
            return NO;
        }
        else
        {
            return YES;
        }
    }
    return NO;
}

- (IBAction)btnaddDevice:(id)sender
{
//    [quickLockDeviceManage getRFIDAdd];
//    [self.view makeToast:@"Place a new RFID tag on the lock to add it."];
    
//    _btnSaveTag.tag = sender.tag;
//    lockDevice = arrayLock[sender.tag];
//    NSString *strName = [NSString stringWithFormat:@"%@",lockDevice.TAG_USERNAME];
//    
//    if ([strName isEqualToString:@"Name"] || [strName isEqualToString:@"name"])
//        _txtTagName.text = @"";
//    else
//        _txtTagName.text = strName;
    
    if ([appDelegate.strSelectedPush isEqualToString:@"phone"] || [appDelegate.strSelectedPush isEqualToString:@"email"])
    {
        _viewBgPop.hidden = NO;
        _viewOfTagInput.hidden = NO;
        [UIView animateWithDuration:0.3f animations:^{
            _viewBgPop.alpha = 0.6f;
            _viewOfTagInput.alpha = 1.0f;
        } completion:^(BOOL finished) {
            [_txtTagName becomeFirstResponder];
        }];
    }
}

-(void)callAfterAddNewRFID:(NSNotification *)notification
{
//    NSDictionary *userInfo = notification.userInfo;
//    NSString *strMsg = [NSString stringWithFormat:@"%@",userInfo[@"msg"]];
//    [self.view makeToast:strMsg];
//    
//    [quickLockDeviceManage getRFIDList];
//    
//    if(arrayLock.count == 0)
//    {
//        _lblNoDevices.hidden = NO;
//    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
  
    // get RFID from lock database
//    [quickLockDeviceManage getRFIDList];
//    
//    if(arrayLock.count == 0)
//    {
//        _lblNoDevices.hidden = NO;
//    }
}

//- (void)ShowHistoryLogRFID:(NSNotification *)notification
//{
//    HistoryLog *historyLog = [notification object];
//    // set data to object
//    arrayHistoryLog = [self InsertData:historyLog ToArray:arrayHistoryLog];
//    
//    historyLog = arrayHistoryLog.lastObject;
//    
//   // NSLog(@"historyLog : %@",historyLog.operateLockType);
//    NSLog(@"operateLockDevice : %@",historyLog.operateLockDevice);
//    NSLog(@"operateLockTime : %@",historyLog.operateLockTime);
//    NSLog(@"isOpenLock : %d",historyLog.isOpenLock);
//
//    NSString *deviceType = @"";
//    // check NFC to get data of only contained RFID not contained name
//    if (historyLog.operateLockType == NFC)
//    {
//        deviceType = [NSString stringWithFormat:@"%@", historyLog.operateLockDevice];
//    }
//    
//    // Shortout unique RFIDs in arrTagId Array
//    [arrTagId addObject:deviceType];
//    NSOrderedSet *mySet = [[NSOrderedSet alloc] initWithArray:arrTagId];
//    arrTagId = [[NSMutableArray alloc] initWithArray:[mySet array]];
//    
//    // if not available RFID in dictForHistory then only it will add.
//    if (![dictForHistory valueForKey:deviceType])
//    {
//        if ([deviceType isEqualToString:@""])
//        {
//            NSLog(@"blank");
//        }
//        else
//        {
//            [arrName addObject:@"Name"];
//            
//            lockDevice.TAG_TYPE = @"NFC";
//            lockDevice.TAG_USERNAME = @"Name";
//            lockDevice.TAG_IMEI = [NSString stringWithFormat:@"%@",deviceType];
//            [dictForHistory setObject:deviceType forKey:deviceType];
//            
//            appDelegate.TAG_IMEI = [NSString stringWithFormat:@"%@",deviceType];
//            // Get Single RFID Tags from database
//            NSString *strIMEI = [dbAccess GetSingleRFIDTags];
//            
//            if ([strIMEI isEqualToString:appDelegate.TAG_IMEI])
//            {
//                // if equal then do nothing
////                [dbAccess DeleteRFIDTag:strIMEI];
//            }
//            else
//            {
//                lockDevice.TAG_ID = [dbAccess InsertRFIDTags:lockDevice];
//            }
//            // Get all RFIDs to show in table
//            arrayLock = [dbAccess GetRFIDTags];
//            [_tblRfidTags reloadData];
//        }
//    }
//    
//    if(arrayLock.count == 0)
//    {
//        _lblNoRfid.hidden = NO;
//    }
//    else
//    {
//        _lblNoRfid.hidden = YES;
//    }
//}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//    return arrayLock.count;
    return arrName.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell= [tableView dequeueReusableCellWithIdentifier:@"cell"];
 
    if (!cell)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    }
//    lockDevice = arrayLock[indexPath.row];

    // minus-icon.png
    UIImageView *imgDel = [[UIImageView alloc]initWithFrame:CGRectMake(screenSize.width-39, 7, 20, 20)];
    imgDel.image = [UIImage imageNamed:@"minus-icon"];
    [cell.contentView addSubview:imgDel];
    
    UIButton *btnDel = [UIButton buttonWithType:UIButtonTypeSystem];
    btnDel.frame = CGRectMake(screenSize.width-46, 0, 46, 36);
    btnDel.tag = indexPath.row;
    [btnDel addTarget:self action:@selector(deleteTag:) forControlEvents:UIControlEventTouchUpInside];
    [cell.contentView addSubview:btnDel];
    
    UILabel *lblDeviceName = [[UILabel alloc]initWithFrame:CGRectMake(4, 0, btnDel.frame.origin.x-10, 36)];
    lblDeviceName.text = [NSString stringWithFormat:@"  %@",arrName[indexPath.row]];
    [cell.contentView addSubview:lblDeviceName];
    
    lblDeviceName.font = [UIFont systemFontOfSize:13];
    lblDeviceName.backgroundColor = cellBgColor;
    lblDeviceName.textColor = darkgray;
    
    if (arrayLock.count == 1)
    {
        lblDeviceName.layer.masksToBounds = YES;
        lblDeviceName.layer.cornerRadius = 8;
    }
    else
    {
        UIBezierPath *maskPath;
        if (indexPath.row == 0)
        {
            maskPath = [UIBezierPath bezierPathWithRoundedRect:lblDeviceName.bounds
                                             byRoundingCorners:(UIRectCornerTopLeft|UIRectCornerTopRight)
                                                   cornerRadii:CGSizeMake(8, 8)];
            CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
            maskLayer.frame = lblDeviceName.bounds;
            maskLayer.path = maskPath.CGPath;
            lblDeviceName.layer.mask = maskLayer;
        }
        else if(indexPath.row == arrName.count-1)
        {
            maskPath = [UIBezierPath bezierPathWithRoundedRect:lblDeviceName.bounds
                                             byRoundingCorners:(UIRectCornerBottomLeft|UIRectCornerBottomRight)
                                                   cornerRadii:CGSizeMake(8, 8)];
            CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
            maskLayer.frame = lblDeviceName.bounds;
            maskLayer.path = maskPath.CGPath;
            lblDeviceName.layer.mask = maskLayer;
        }
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    if (appDelegate.strSelectedPush == nil)
    {
        appDelegate.strSelectedAlertDevice = arrName[indexPath.row];
        [self.navigationController pushViewController:configureDevice animated:NO];
    }
}

//- (NSMutableArray *)InsertData:(HistoryLog *)historyLog ToArray:(NSMutableArray *)sourceNumber
//{
//    if (!sourceNumber || sourceNumber.count == 0)
//    {
//        [sourceNumber addObject:historyLog];
//        return sourceNumber;
//    }
//    
//    //Smaller than the first
//    if ([historyLog.operateLockTime timeIntervalSince1970] < [((HistoryLog *)sourceNumber[0]).operateLockTime timeIntervalSince1970])
//    {
//        [sourceNumber insertObject:historyLog atIndex:0];
//        return sourceNumber;
//    }
//    else
//    {
//        if (arrayHistoryLog.count == 1)
//        {
//            [sourceNumber addObject:historyLog];
//            return sourceNumber;
//        }
//    }
//    for (int j = 1; j < sourceNumber.count; j++)
//    {
//        if ([historyLog.operateLockTime timeIntervalSince1970] >=
//            [((HistoryLog *)sourceNumber[j - 1]).operateLockTime timeIntervalSince1970] &&
//            [historyLog.operateLockTime timeIntervalSince1970] <
//            [((HistoryLog *)sourceNumber[j]).operateLockTime timeIntervalSince1970])
//        {
//            [sourceNumber insertObject:historyLog atIndex:j];
//            break;
//        }
//        
//        //Than the last big
//        if ((j == sourceNumber.count - 1) &&
//            ([historyLog.operateLockTime timeIntervalSince1970] >=
//             [((HistoryLog *)sourceNumber[j]).operateLockTime timeIntervalSince1970]))
//        {
//            [sourceNumber addObject:historyLog];
//            break;
//        }
//    }
//    
//    return sourceNumber;
//}

- (IBAction)btnBack:(id)sender
{
    if (appDelegate.strSelectedPush == nil)
    {
        [self.navigationController popViewControllerAnimated:NO];
    }
    else
    {
        [self.navigationController pushViewController:configureDevice animated:NO];
    }
}


// Code to adjust line of UITableViewCell from left side
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath*)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)])
    {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    if ([_tblDevices respondsToSelector:@selector(setSeparatorInset:)])
    {
        [_tblDevices setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([_tblDevices respondsToSelector:@selector(setLayoutMargins:)])
    {
        [_tblDevices setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 36;
}

-(void)deleteTag:(UIButton *)sender
{
    _viewBgPop.hidden = NO;
    _viewPopup.hidden = NO;
    [UIView animateWithDuration:0.3f animations:^{
        _viewBgPop.alpha = 0.6f;
        _viewPopup.alpha = 1.0f;
    } completion:^(BOOL finished) {
        
    }];
    _btnPopupYes.tag = sender.tag;
}

- (IBAction)btnPopupYes:(UIButton *)sender
{
//    lockDevice = arrayLock[sender.tag];
//    NSString *strIMEI = lockDevice.TAG_IMEI;
//    
//    quickLockDeviceManage.lockDevice.TAG_DELETE_IMEI_OR_RFID = strIMEI;  //TAG_USERNAME = _txtTagName.text;
//    [quickLockDeviceManage deleteRFID];
//
//    [dbAccess DeleteRFIDTag:strIMEI];
    
    [UIView animateWithDuration:0.3f animations:^{
        _viewBgPop.alpha = 0.0f;
        _viewPopup.alpha = 0.0f;
    } completion:^(BOOL finished) {
        _viewBgPop.hidden = YES;
        _viewPopup.hidden = YES;
        
//        arrTagId = [NSMutableArray new];
//        arrName = [NSMutableArray new];
//        dictForHistory = [NSMutableDictionary new];
        
//        appDelegate.TAG_IMEI = @"";
//        arrayLock = [dbAccess GetRFIDTags];
//        if (arrayLock.count > 0)
//        {
//            for (int i = 0; i < arrayLock.count; i++)
//            {
//                lockDevice = arrayLock[i];
//                [arrTagId addObject:lockDevice.TAG_IMEI];
//                
//                if (![dictForHistory valueForKey:lockDevice.TAG_IMEI])
//                {
//                    if ([lockDevice.TAG_IMEI isEqualToString:@""])
//                    {
//                        NSLog(@"blank");
//                    }
//                    else
//                    {
//                        [dictForHistory setObject:lockDevice.TAG_IMEI forKey:lockDevice.TAG_IMEI];
//                        [arrName addObject:lockDevice.TAG_USERNAME];
//                    }
//                }
//            }
//            NSOrderedSet *mySet = [[NSOrderedSet alloc] initWithArray:arrTagId];
//            arrTagId = [[NSMutableArray alloc] initWithArray:[mySet array]];
//        }
//        else
//        {
//            _lblNoRfid.hidden = NO;
//        }
//        [_tblRfidTags reloadData];
    }];
}

- (IBAction)btnPopupNo:(UIButton *)sender
{
    [UIView animateWithDuration:0.3f animations:^{
        _viewBgPop.alpha = 0.0f;
        _viewPopup.alpha = 0.0f;
    } completion:^(BOOL finished) {
        _viewBgPop.hidden = YES;
        _viewPopup.hidden = YES;
    }];
}

@end
