//
//  FirstViewController.m
//  QuickLock
//
//  Created by Pavan Jadhav on 14/09/17.
//  Copyright © 2017 Bge. All rights reserved.
//

#import "FirstViewController.h"
#import "AlertLogin.h"
#import "SetupProfileController.h"

@interface FirstViewController ()

@end

@implementation FirstViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = backgrondcolor;
    btnCreateAccount.layer.borderColor = (__bridge CGColorRef _Nullable)(redBorderColor);
  
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnLogin:(id)sender {
    AlertLogin *alert = [self.storyboard instantiateViewControllerWithIdentifier:@"AlertLogin"];
    [self.navigationController pushViewController:alert animated:NO];

}

- (IBAction)btnCreateAccount:(id)sender {
    SetupProfileController *alert = [self.storyboard instantiateViewControllerWithIdentifier:@"SetupProfileController"];
    [self.navigationController pushViewController:alert animated:NO];
}
@end
