//
//  DottedView.m
//  JordanAnimationSamples
//
//  Created by Jaydeep on 25/02/15.
//  Copyright (c) 2015 MagnetoItSolutions. All rights reserved.
//

#import "DottedView.h"

@implementation DottedView

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    CGFloat thickness = 1.0;
    
    CGContextRef cx = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(cx, thickness);
    CGContextSetStrokeColorWithColor(cx, [UIColor whiteColor].CGColor);
    
    CGFloat ra[] = {4,8};
    CGContextSetLineDash(cx, 0.0, ra, 2); // nb "2" == ra count
    
    CGContextMoveToPoint(cx, 0,0);
    CGContextAddLineToPoint(cx, self.bounds.size.width, 0);
//    CGContextAddLineToPoint(cx, self.bounds.size.width, self.frame.size.height);
//    CGContextAddLineToPoint(cx, 5, self.frame.size.height);
    
    CGContextAddLineToPoint(cx, 0, 0);
    CGContextStrokePath(cx);

}


@end
