//
//  DoorLockViewController.h
//  QuickLock
//
//  Created by 王洋 on 14/12/27.
//  Copyright (c) 2014年 Bge. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DoorLockViewController : UIViewController
{
        IBOutlet UILabel *labelName;
                
        IBOutlet UIButton *buttonLock;
        
        IBOutlet UIButton *buttonAutoUnlock;
        
        IBOutlet UIButton *buttonUnlock;
        
        IBOutlet UIButton *buttonSet;
        
        IBOutlet UILabel *labelBattery;
    
    BOOL isUnlockNotPassToggle, isSharecodeNotShown, isAutoLockPressed;
    int btnState;
}
@property (strong, nonatomic) IBOutlet UIImageView *imgGunbox;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constantLogoBottom;
- (IBAction)buttonLockTouch:(id)sender;
- (IBAction)buttonAutoUnlockTouch:(id)sender;
- (IBAction)buttonUnlockTouch:(id)sender;
- (IBAction)buttonSetTouch:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *labelAPPVersion;

@end
