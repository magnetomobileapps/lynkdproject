//
//  ShareCode.m
//  QuickLock
//
//  Created by PUNDSK001 on 26/12/15.
//  Copyright © 2015 Bge. All rights reserved.
//

#import "ShareCode.h"

@interface ShareCode () <UITextFieldDelegate>
{
    UIDatePicker *_datePicker;
    UIView *ViewOfDatepicker;
    UIButton *btnSetDate, *btnDateCancel;
    UIToolbar* keyboardToolbar;
    UIBarButtonItem *btnFlexible, *btnDone;
    IBOutlet UILabel *lblTitleShareCode;
}
@property (weak, nonatomic) IBOutlet UIScrollView *scroll;
@property (weak, nonatomic) IBOutlet UITextField *txtCode1;
@property (weak, nonatomic) IBOutlet UITextField *txtNumberOfUsers;
@property (weak, nonatomic) IBOutlet UITextField *txtCode2;
@property (weak, nonatomic) IBOutlet UITextField *txtDate;
@property (weak, nonatomic) IBOutlet UITextField *timeFrom;
@property (weak, nonatomic) IBOutlet UITextField *timeTo;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) IBOutlet UIView *viewDatePickerBack;
@property (weak, nonatomic) IBOutlet UITextField *txtDateTo;
-(void)createInputAccessoryView;
@property (weak, nonatomic) IBOutlet UIImageView *imgStartEnable;
@property (weak, nonatomic) IBOutlet UIImageView *imgEndEnable;
@property (weak, nonatomic) IBOutlet UIButton *btnStartDate;
@property (weak, nonatomic) IBOutlet UIButton *btnEndDate;
@property (weak, nonatomic) IBOutlet UIButton *btnStartTime;
@property (weak, nonatomic) IBOutlet UIButton *btnEndTime;

@end

@implementation ShareCode

- (void)viewDidLoad
{
    [super viewDidLoad];

    strNumbers = [NSMutableString new];
    
    TimeEnable = dateEnable = 0;
    lblTitleShareCode.textColor = titleorange;
    
    // Set layer ot all the controlls
    [self txtLayer:_timeTo buttonLayer:nil];
    [self txtLayer:_timeFrom buttonLayer:nil];
    [self txtLayer:_txtNumberOfUsers buttonLayer:nil];
    [self txtLayer:_txtDate buttonLayer:nil];
    [self txtLayer:_txtCode1 buttonLayer:_btnSave];
    [self txtLayer:_txtCode2 buttonLayer:_btnCancel];
    [self txtLayer:_txtDateTo buttonLayer:_btnCancel];
    
    [self createInputAccessoryView];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(btnCancelDate:)];
    [_viewDatePickerBack addGestureRecognizer:tap];
    
    _btnStartDate.userInteractionEnabled = NO;
    _btnEndDate.userInteractionEnabled = NO;
    _btnStartTime.userInteractionEnabled = NO;
    _btnEndTime.userInteractionEnabled = NO;
    
    format = [[NSDateFormatter alloc]init];
    
    if (screenSize.height == 568)
    {
        _scroll.scrollEnabled = NO;
    }
}

-(void)createInputAccessoryView
{
    keyboardToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 44)];
    keyboardToolbar.tintColor = [UIColor darkGrayColor];
    keyboardToolbar.backgroundColor = [UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:1.0];

    btnFlexible = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    btnDone = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneTyping)];
    
    NSArray *arr = @[btnFlexible,btnDone];
    [keyboardToolbar setItems:arr];
    
    [self.txtCode1 setInputAccessoryView:keyboardToolbar];
    [self.txtCode2 setInputAccessoryView:keyboardToolbar];
    [self.txtNumberOfUsers setInputAccessoryView:keyboardToolbar];
}

-(void)doneTyping
{
    self.view.transform = CGAffineTransformMakeTranslation(0, 0);
    [_txtCode1 resignFirstResponder];
    [_txtCode2 resignFirstResponder];
    [_txtNumberOfUsers resignFirstResponder];
}

- (IBAction)btnEnableCheckbox:(UIButton *)sender
{
    if (sender.tag == 3) // Date enable disable
    {
        _imgStartEnable.image = [UIImage imageNamed:@"checked"];
        sender.tag = 4;
        dateEnable = 1;
        _btnStartDate.userInteractionEnabled = YES;
        _btnEndDate.userInteractionEnabled = YES;
    }
    else
    {
        _imgStartEnable.image = [UIImage imageNamed:@"unchecked"];
        sender.tag = 3;
        dateEnable = 0;
        _btnStartDate.userInteractionEnabled = NO;
        _btnEndDate.userInteractionEnabled = NO;
        _txtDate.text = @"";
        _txtDateTo.text = @"";
    }
}

- (IBAction)btnEnableCheckBoxEnd:(UIButton *)sender
{
    if (sender.tag == 3) // tiem enable disable
    {
        _imgEndEnable.image = [UIImage imageNamed:@"checked"];
        sender.tag = 4;
        TimeEnable = 2;
        _btnStartTime.userInteractionEnabled = YES;
        _btnEndTime.userInteractionEnabled = YES;
    }
    else
    {
        _imgEndEnable.image = [UIImage imageNamed:@"unchecked"];
        sender.tag = 3;
        TimeEnable = 0;
        _btnStartTime.userInteractionEnabled = NO;
        _btnEndTime.userInteractionEnabled = NO;
        _timeFrom.text = @"";
        _timeTo.text = @"";
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == _txtNumberOfUsers)
    {
        strNumbers = _txtNumberOfUsers.text;
    }
    if (screenSize.height == 568)
    {
        if (textField == _txtCode2)
        {
            self.view.transform = CGAffineTransformMakeTranslation(0, -30);
        }
        else
        {
            self.view.transform = CGAffineTransformMakeTranslation(0, 0);
        }
    }
    if (screenSize.height == 480)
    {
        if (textField == _txtCode2)
        {
            self.view.transform = CGAffineTransformMakeTranslation(0, -70);
        }
        else
        {
            self.view.transform = CGAffineTransformMakeTranslation(0, 0);
        }
    }
}

// Keyboard will be returned
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    self.view.transform = CGAffineTransformMakeTranslation(0, 0);
    
    [textField resignFirstResponder];
    return YES;
}

-(void)viewDidAppear:(BOOL)animated
{
    // Create Dynamic date picker to set date
    ViewOfDatepicker = [[UIView alloc]initWithFrame:CGRectMake((self.view.frame.size.width-301)/2, (self.view.frame.size.height-210)/2, 301, 210)];
    ViewOfDatepicker.backgroundColor = [UIColor whiteColor];
    ViewOfDatepicker.hidden = YES;
    ViewOfDatepicker.alpha = 0.0;
    ViewOfDatepicker.layer.masksToBounds = YES;
    ViewOfDatepicker.layer.borderWidth = 4;
    ViewOfDatepicker.layer.borderColor = lightgray.CGColor;
    ViewOfDatepicker.layer.cornerRadius = 8;
    [self.view addSubview:ViewOfDatepicker];
    
    _datePicker = [[UIDatePicker alloc]initWithFrame:CGRectMake(0, 0, 301, 162)];
//    _datePicker.datePickerMode = UIDatePickerModeDate;
    _datePicker.backgroundColor = [UIColor clearColor];
    [ViewOfDatepicker addSubview:_datePicker];
    
    // Defined btnDateDone: method to set date
    btnSetDate = [UIButton buttonWithType:UIButtonTypeSystem];
    [btnSetDate addTarget:self action:@selector(btnDateDone:) forControlEvents:UIControlEventTouchUpInside];
    btnSetDate.frame = CGRectMake(8, 167, 138, 35);
    [btnSetDate setTitle:@"Set" forState:UIControlStateNormal];
    [btnSetDate setTintColor:[UIColor whiteColor]];
    btnSetDate.backgroundColor = darkgray;
    btnSetDate.layer.masksToBounds = YES;
    btnSetDate.layer.cornerRadius = 4;
    [ViewOfDatepicker addSubview:btnSetDate];

    // Defined btnCancelDate: method to cancel date set
    btnDateCancel = [UIButton buttonWithType:UIButtonTypeSystem];
    [btnDateCancel addTarget:self action:@selector(btnCancelDate:) forControlEvents:UIControlEventTouchUpInside];
    btnDateCancel.frame = CGRectMake(155, 167, 138, 35);
    [btnDateCancel setTitle:@"Cancel" forState:UIControlStateNormal];
    [btnDateCancel setTintColor:[UIColor whiteColor]];
    btnDateCancel.backgroundColor = darkgray;
    btnDateCancel.layer.masksToBounds = YES;
    btnDateCancel.layer.cornerRadius = 4;
    [ViewOfDatepicker addSubview:btnDateCancel];
}



// Limit the input box can only enter a number, and a maximum length of 8
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([string isEqualToString:@"\n"] || [string isEqualToString:@""])
    {
        return YES;
    }
    else if (textField.text.length > 7)
    {
        return NO;
    }
    else
    {
        if (textField == _txtNumberOfUsers)
        {
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            return (newLength <= 3);
        }
        
        return [kDigitalRange rangeOfString:string].location != NSNotFound;
    }
}

// Go back
- (IBAction)btnBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}

- (IBAction)btnSave:(id)sender
{
    NSString *strShareCode;
    
    if (_txtCode2.text.length != 0 && _txtCode1.text.length != 0)
    {
        [self.view makeToast:@"Warning!\nEnter only one Share Code from Number of uses base and Time base" duration:4.0 position:@"center"];
//        alert = [[UIAlertView alloc] initWithTitle:@"Warning!"
//                                                        message:@"Enter only one Share Code from Number of uses base and Time base"
//                                                       delegate:nil
//                                              cancelButtonTitle:@"OK"
//                                              otherButtonTitles:nil];
//        [alert show];
    }
    else if (_txtCode2.text.length == 0 && _txtCode1.text.length == 0)
    {
        [self.view makeToast:@"Warning!\nEnter Share Code one from Number of users base and Time base" duration:4.0 position:@"center"];

//        alert = [[UIAlertView alloc] initWithTitle:@"Warning!"
//                                                        message:@"Enter Share Code one from Number of users base and Time base"
//                                                       delegate:nil
//                                              cancelButtonTitle:@"OK"
//                                              otherButtonTitles:nil];
//        [alert show];
    }
    else if (_txtCode1.text.length == 0) // for Date and time wise
    {
        strShareCode = _txtCode2.text;
        
        if (strShareCode.length < 8)
        {
            [self.view makeToast:@"Warning!\nShare Code must be 8 digital" duration:2.0 position:@"center"];
            
//            alert = [[UIAlertView alloc] initWithTitle:@"Warning!"
//                                                            message:@"Share Code must be 8 digital"
//                                                           delegate:nil
//                                                  cancelButtonTitle:@"OK"
//                                                  otherButtonTitles:nil];
//            [alert show];
        }
        else
        {
            if ((_txtDate.text.length == 0 || _txtDateTo.text.length == 0) && (dateEnable != 0))
            {
                [self.view makeToast:@"Warning!\nSet Start Date and End Date" duration:2.0 position:@"center"];

//                alert = [[UIAlertView alloc] initWithTitle:@"Warning!"
//                                                                message:@"Set Start Date and End Date"
//                                                               delegate:nil
//                                                      cancelButtonTitle:@"OK"
//                                                      otherButtonTitles:nil];
//                [alert show];
            }
            else if ((_timeFrom.text.length == 0 || _timeTo.text.length == 0) && (TimeEnable != 0))
            {
                [self.view makeToast:@"Warning!\nSet Start Time and End Time" duration:2.0 position:@"center"];

//                alert = [[UIAlertView alloc] initWithTitle:@"Warning!"
//                                                   message:@"Set Start Time and End Time"
//                                                  delegate:nil
//                                         cancelButtonTitle:@"OK"
//                                         otherButtonTitles:nil];
//                [alert show];
            }
            else
            {
                
                quickLockDeviceManage.lockDevice.share_Code_Time = strShareCode;
                int shareCodeOption;
                
                NSDateFormatter *timeFormat = [[NSDateFormatter alloc]init];
                [timeFormat setDateFormat:@"HH:mm"];
                NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
                [dateFormat setDateFormat:@"dd-MM-yyyy"];
                
                if (dateEnable == 0 && TimeEnable == 0) // no date, no time
                {
                    shareCodeOption = 0;
                    
                    NSString *strCurrentDate = [dateFormat stringFromDate:[NSDate date]];
                    NSDate *currentDate = [dateFormat dateFromString:strCurrentDate];
                    
                    quickLockDeviceManage.lockDevice.share_Code_Start_Time = currentDate;
                    quickLockDeviceManage.lockDevice.share_Code_End_Time = currentDate;
                    
                    quickLockDeviceManage.lockDevice.share_Code_Start_Time_minutes = 0;
                    quickLockDeviceManage.lockDevice.share_Code_End_Time_minutes = 0;
                    
                    quickLockDeviceManage.lockDevice.share_Code_Time1 = @"";
                    quickLockDeviceManage.lockDevice.share_Code_Time2 = @"";
                    quickLockDeviceManage.lockDevice.share_code_time_flag = 0;
                }
                else if (dateEnable != 0 && TimeEnable != 0) // with date, with time =3
                {
                    shareCodeOption = 3;
                    
                    NSCalendar *calendar = [NSCalendar currentCalendar];
                    NSDateComponents *dateComponents = [calendar components:NSDayCalendarUnit|NSMonthCalendarUnit|NSYearCalendarUnit fromDate:[dateFormat dateFromString:_txtDate.text]];
                    //NSDateComponents *timeFromComponents = [calendar components:NSHourCalendarUnit|NSMinuteCalendarUnit fromDate:[timeFormat dateFromString:_timeFrom.text]];
                    NSDateComponents *newComponents = [[NSDateComponents alloc]init];
                    newComponents.timeZone = [NSTimeZone systemTimeZone];
                    [newComponents setDay:[dateComponents day]];
                    [newComponents setMonth:[dateComponents month]];
                    [newComponents setYear:[dateComponents year]];
//                    [newComponents setHour:[timeFromComponents hour]];
//                    [newComponents setMinute:[timeFromComponents minute]];
                    NSDate *combDateStart = [calendar dateFromComponents:newComponents];
                    quickLockDeviceManage.lockDevice.share_Code_Start_Time = combDateStart;
                    quickLockDeviceManage.lockDevice.share_Code_Start_Time_minutes = [self minutesSinceMidnight:[timeFormat dateFromString:_timeFrom.text]];
                    
                    NSCalendar *calendar1 = [NSCalendar currentCalendar];
                    NSDateComponents *dateToComponents = [calendar1 components:NSDayCalendarUnit|NSMonthCalendarUnit|NSYearCalendarUnit fromDate:[dateFormat dateFromString:_txtDateTo.text]];
//                    NSDateComponents *timeToComponents = [calendar1 components:NSHourCalendarUnit|NSMinuteCalendarUnit fromDate:[timeFormat dateFromString:_timeTo.text]];
                    NSDateComponents *newComponents1 = [[NSDateComponents alloc]init];
                    newComponents1.timeZone = [NSTimeZone systemTimeZone];
                    [newComponents1 setDay:[dateToComponents day]];
                    [newComponents1 setMonth:[dateToComponents month]];
                    [newComponents1 setYear:[dateToComponents year]];
//                    [newComponents1 setHour:[timeToComponents hour]];
//                    [newComponents1 setMinute:[timeToComponents minute]];
                    NSDate *combDateEnd = [calendar1 dateFromComponents:newComponents1];
                    quickLockDeviceManage.lockDevice.share_Code_End_Time = combDateEnd;
                    quickLockDeviceManage.lockDevice.share_Code_End_Time_minutes = [self minutesSinceMidnight:[timeFormat dateFromString:_timeTo.text]];
                    
                    quickLockDeviceManage.lockDevice.share_Code_Time1 = _timeFrom.text;
                    quickLockDeviceManage.lockDevice.share_Code_Time2 = _timeTo.text;
                    quickLockDeviceManage.lockDevice.share_code_time_flag = 3;
                }
                else if (dateEnable != 0) // with date, no time =1
                {
                    shareCodeOption = 1;
                    
                    NSCalendar *calendar = [NSCalendar currentCalendar];
                    NSDateComponents *dateComponents = [calendar components:NSDayCalendarUnit|NSMonthCalendarUnit|NSYearCalendarUnit fromDate:[dateFormat dateFromString:_txtDate.text]];
                    //NSDateComponents *timeFromComponents = [calendar components:NSHourCalendarUnit|NSMinuteCalendarUnit fromDate:[timeFormat dateFromString:@"00:00"]];
                    NSDateComponents *newComponents = [[NSDateComponents alloc]init];
                    newComponents.timeZone = [NSTimeZone systemTimeZone];
                    [newComponents setDay:[dateComponents day]];
                    [newComponents setMonth:[dateComponents month]];
                    [newComponents setYear:[dateComponents year]];
//                    [newComponents setHour:[timeFromComponents hour]];
//                    [newComponents setMinute:[timeFromComponents minute]];
                    NSDate *combDateStart = [calendar dateFromComponents:newComponents];
                    quickLockDeviceManage.lockDevice.share_Code_Start_Time = combDateStart;
                    
                    NSCalendar *calendar1 = [NSCalendar currentCalendar];
                    NSDateComponents *dateToComponents = [calendar1 components:NSDayCalendarUnit|NSMonthCalendarUnit|NSYearCalendarUnit fromDate:[dateFormat dateFromString:_txtDateTo.text]];
                    //NSDateComponents *timeToComponents = [calendar1 components:NSHourCalendarUnit|NSMinuteCalendarUnit fromDate:[timeFormat dateFromString:@"00:00"]];
                    NSDateComponents *newComponents1 = [[NSDateComponents alloc]init];
                    newComponents1.timeZone = [NSTimeZone systemTimeZone];
                    [newComponents1 setDay:[dateToComponents day]];
                    [newComponents1 setMonth:[dateToComponents month]];
                    [newComponents1 setYear:[dateToComponents year]];
//                    [newComponents1 setHour:[timeToComponents hour]];
//                    [newComponents1 setMinute:[timeToComponents minute]];
                    NSDate *combDateEnd = [calendar1 dateFromComponents:newComponents1];
                    quickLockDeviceManage.lockDevice.share_Code_End_Time = combDateEnd;
                    
                    quickLockDeviceManage.lockDevice.share_Code_Start_Time_minutes = 0;
                    quickLockDeviceManage.lockDevice.share_Code_End_Time_minutes = 0;
                    quickLockDeviceManage.lockDevice.share_Code_Time1 = @"";
                    quickLockDeviceManage.lockDevice.share_Code_Time2 = @"";
                    quickLockDeviceManage.lockDevice.share_code_time_flag = 1;

                }
                else if (TimeEnable != 0) //no date, with time = 2
                {
                    shareCodeOption = 2;
                    
                    NSString *strCurrentDate = [dateFormat stringFromDate:[NSDate date]];
                    NSDate *currentDate = [dateFormat dateFromString:strCurrentDate];

                    quickLockDeviceManage.lockDevice.share_Code_Start_Time = currentDate;
                    quickLockDeviceManage.lockDevice.share_Code_End_Time = currentDate;
                    
                    quickLockDeviceManage.lockDevice.share_Code_Start_Time_minutes = [self minutesSinceMidnight:[timeFormat dateFromString:_timeFrom.text]];
                    quickLockDeviceManage.lockDevice.share_Code_End_Time_minutes = [self minutesSinceMidnight:[timeFormat dateFromString:_timeTo.text]];
                    quickLockDeviceManage.lockDevice.share_Code_Time1 = _timeFrom.text;
                    quickLockDeviceManage.lockDevice.share_Code_Time2 = _timeTo.text;
                    quickLockDeviceManage.lockDevice.share_code_time_flag = 2;
                }
                
                NSLog(@"shareCodeOption : %d",shareCodeOption);
                
                quickLockDeviceManage.lockDevice.share_Code_DATE_Time_FLAG = shareCodeOption;
                [quickLockDeviceManage addSharecodeTimeBase];
                [self.view makeToast:@"Share code added successfully"];
                [dbAccess UpdateShareCodeTime:quickLockDeviceManage.lockDevice WithLockID:quickLockDeviceManage.lockDevice.lock_ID];
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self.navigationController popViewControllerAnimated:NO];
                });
            }
        }
    }
    else if (_txtCode2.text.length == 0)  // For of user wise
    {
        strShareCode = _txtCode1.text;
        
        if (strShareCode.length < 8)
        {
            [self.view makeToast:@"Warning!\nShare Code must be 8 digital" duration:2.0 position:@"center"];

//            alert = [[UIAlertView alloc] initWithTitle:@"Warning!"
//                                                            message:@"Share Code must be 8 digital"
//                                                           delegate:nil
//                                                  cancelButtonTitle:@"OK"
//                                                  otherButtonTitles:nil];
//            [alert show];
        }
        else
        {
            quickLockDeviceManage.lockDevice.share_Code = strShareCode;
            NSString *strUserCount = [NSString stringWithFormat:@"%@",_txtNumberOfUsers.text];
            if ([strUserCount isEqualToString:@"0"] || strUserCount.length == 0)
            {
                [self.view makeToast:@"Warning!\nSet Number of users" duration:2.0 position:@"center"];

//                alert = [[UIAlertView alloc] initWithTitle:@"Warning!"
//                                                                message:@"Set Number of users"
//                                                               delegate:nil
//                                                      cancelButtonTitle:@"OK"
//                                                      otherButtonTitles:nil];
//                [alert show];
            }
            else if (strUserCount.length == 1)
            {
                strUserCount = [NSString stringWithFormat:@"0%@",_txtNumberOfUsers.text];
                quickLockDeviceManage.lockDevice.share_Code_Count = strUserCount;
                [quickLockDeviceManage addSharecode];
                [self.view makeToast:@"Share code added successfully"];
                [dbAccess UpdateShareCodeUser:quickLockDeviceManage.lockDevice WithLockID:quickLockDeviceManage.lockDevice.lock_ID];
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self.navigationController popViewControllerAnimated:NO];
                });
            }
            else
            {
                strUserCount = [NSString stringWithFormat:@"%@",_txtNumberOfUsers.text];
                quickLockDeviceManage.lockDevice.share_Code_Count = strUserCount;
                [quickLockDeviceManage addSharecode];
                [self.view makeToast:@"Share code added successfully"];
                [dbAccess UpdateShareCodeUser:quickLockDeviceManage.lockDevice WithLockID:quickLockDeviceManage.lockDevice.lock_ID];
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [self.navigationController popViewControllerAnimated:NO];
                });
            }
        }
    }
}

-(int)minutesSinceMidnight:(NSDate *)date
{
    NSCalendar *gregorian = [[NSCalendar alloc]
                             initWithCalendarIdentifier:NSGregorianCalendar];
    unsigned unitFlags =  NSHourCalendarUnit | NSMinuteCalendarUnit;
    NSDateComponents *components = [gregorian components:unitFlags fromDate:date];
    
    return 60 * [components hour] + [components minute];
}

// PopUp will be opened set date to textField
- (IBAction)btnSetDate:(id)sender
{
    [_txtCode1 resignFirstResponder];
    [_txtCode2 resignFirstResponder];
    
    _datePicker.minimumDate = [NSDate date];
    _datePicker.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    [_datePicker setDatePickerMode:UIDatePickerModeDate];
    [format setDateFormat:@"dd-MM-yyyy"];
    isDateSelect = YES;
    isToDateSelect = NO;
    
    ViewOfDatepicker.hidden = NO;
    _viewDatePickerBack.hidden = NO;
    [UIView animateWithDuration:0.3f animations:^{
        ViewOfDatepicker.alpha = 1.0f;
        _viewDatePickerBack.alpha = 0.5f;
        _viewDatePickerBack.backgroundColor = [UIColor blackColor];
    }];
}

- (IBAction)btnSetDateTo:(id)sender
{
    [_txtCode1 resignFirstResponder];
    [_txtCode2 resignFirstResponder];

    _datePicker.minimumDate = [NSDate date];
    _datePicker.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    [_datePicker setDatePickerMode:UIDatePickerModeDate];
    [format setDateFormat:@"dd-MM-yyyy"];
    isDateSelect = YES;
    isToDateSelect = YES;
    
    ViewOfDatepicker.hidden = NO;
    _viewDatePickerBack.hidden = NO;
    [UIView animateWithDuration:0.3f animations:^{
        ViewOfDatepicker.alpha = 1.0f;
        _viewDatePickerBack.alpha = 0.5f;
        _viewDatePickerBack.backgroundColor = [UIColor blackColor];
    }];
}

// set Time from which you want to assign ShareCode
- (IBAction)btnTimeFrom:(id)sender
{
    [_txtCode1 resignFirstResponder];
    [_txtCode2 resignFirstResponder];

    _datePicker.minimumDate = nil;
    NSLocale *locale = [[NSLocale alloc]initWithLocaleIdentifier:@"NL"];
    [_datePicker setLocale:locale];
    [_datePicker setDatePickerMode:UIDatePickerModeTime];
    [format setDateFormat:@"HH:mm"];
    isDateSelect = NO;
    isTimeFromSelected = YES;
    
    ViewOfDatepicker.hidden = NO;
    _viewDatePickerBack.hidden = NO;
    [UIView animateWithDuration:0.3f animations:^{
        ViewOfDatepicker.alpha = 1.0f;
        _viewDatePickerBack.alpha = 0.5f;
        _viewDatePickerBack.backgroundColor = [UIColor blackColor];
    }];
}

// set Time up to which you want to assign ShareCode
- (IBAction)btnTo:(id)sender
{
    [_txtCode1 resignFirstResponder];
    [_txtCode2 resignFirstResponder];

    _datePicker.minimumDate = nil;
    NSLocale *locale = [[NSLocale alloc]initWithLocaleIdentifier:@"NL"];
    [_datePicker setLocale:locale];
    [_datePicker setDatePickerMode:UIDatePickerModeTime];
    [format setDateFormat:@"HH:mm"];
    isDateSelect = NO;
    isTimeFromSelected = NO;
    ViewOfDatepicker.hidden = NO;
    _viewDatePickerBack.hidden = NO;
    [UIView animateWithDuration:0.3f animations:^{
        ViewOfDatepicker.alpha = 1.0f;
        _viewDatePickerBack.alpha = 0.5f;
        _viewDatePickerBack.backgroundColor = [UIColor blackColor];
    }];
}

// assign date on textfield and hide datePicker
- (IBAction)btnDateDone:(id)sender
{
    strDate = [format stringFromDate:[_datePicker date]];
    
    if (isDateSelect == YES)
    {
        if (isToDateSelect == YES)
        {
            _txtDateTo.text = strDate;
        }
        else
        {
            _txtDate.text = strDate;
        }
    }
    else
    {
        if (isTimeFromSelected == YES)
        {
            _timeFrom.text = strDate;
        }
        else
        {
            _timeTo.text = strDate;
        }
    }
    
    [UIView animateWithDuration:0.3f animations:^{
        ViewOfDatepicker.alpha = 0.0f;
        _viewDatePickerBack.alpha = 0.0f;
    } completion:^(BOOL finished) {
        ViewOfDatepicker.hidden = YES;
    }];
}

// Cancel date set
- (IBAction)btnCancelDate:(id)sender
{
    [UIView animateWithDuration:0.3f animations:^{
        ViewOfDatepicker.alpha = 0.0f;
        _viewDatePickerBack.alpha = 0.0f;
    } completion:^(BOOL finished) {
        ViewOfDatepicker.hidden = YES;
        _viewDatePickerBack.hidden = YES;
    }];
}

// Apply layer design to controls
-(void)txtLayer:(UITextField *)textfield buttonLayer:(UIButton *)buttom
{
    textfield.backgroundColor = lightgray;
}

// button for increment number of user
- (IBAction)btnUpNumber:(UIButton *)sender
{
    int no = [_txtNumberOfUsers.text intValue];

    if (sender.tag == 1)
    {
        if (no < 255)
        {
            no++;
        }
    }
    else if (sender.tag == 2)
    {
        if (no > 0)
        {
            no--;
        }
    }
    
    _txtNumberOfUsers.text = [NSString stringWithFormat:@"%d",no];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [alert dismissWithClickedButtonIndex:0 animated:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
