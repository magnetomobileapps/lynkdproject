//
//  DeleteDeviceCell.h
//  QuickLock
//
//  Created by administrator on 15-3-11.
//  Copyright (c) 2015年 Bge. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DeleteDeviceCell : UITableViewCell

@property (nonatomic, copy) UIButton *deleteDevice;

@end
