//
//  FixedTemperature.m
//  QuickLock
//
//  Created by PUNDSK001 on 06/01/16.
//  Copyright © 2016 Bge. All rights reserved.
//

#import "FixedTemperature.h"

@interface FixedTemperature () <UITextFieldDelegate>

// Properties
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UITextField *txtTemp;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;
@end

@implementation FixedTemperature

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _txtTemp.layer.cornerRadius = 6;
    _txtTemp.layer.borderWidth = 4;
    _txtTemp.layer.borderColor = bordergray.CGColor;
    _txtTemp.backgroundColor = lightgray;
    
    _btnSave.layer.cornerRadius = 6;
    _btnSave.layer.masksToBounds = YES;
}

- (IBAction)btnBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}

- (IBAction)btnSave:(id)sender
{
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return  YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
