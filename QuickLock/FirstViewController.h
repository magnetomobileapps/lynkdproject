//
//  FirstViewController.h
//  QuickLock
//
//  Created by Pavan Jadhav on 14/09/17.
//  Copyright © 2017 Bge. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "layerTheButton.h"

@interface FirstViewController : UIViewController
{
    __weak IBOutlet layerTheButton *btnLogin;
    __weak IBOutlet layerTheButton *btnCreateAccount;
}
- (IBAction)btnLogin:(id)sender;
- (IBAction)btnCreateAccount:(id)sender;



@end
