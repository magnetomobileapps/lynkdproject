//
//  QuickLockDevice.m
//  QuickLock
//
//  Created by administrator on 14-12-27.
//  Copyright (c) 2014 year Bge. All rights reserved.
//

#import "QuickLockDeviceManage.h"
#import "Prefer.h"
#import "AppDelegate.h"
#import "MessageBox.h"
#import "HistoryLog.h"

@interface QuickLockDeviceManage() <MessageBoxProtocol>
{
        NSString *tempNewPassword;
        MessageBox *hudConnectWithUUID;
}

@end

@implementation QuickLockDeviceManage

@synthesize isSearchConnected;
@synthesize lockDevice;
@synthesize lockIsOpen;

- (id)init
{
        self = [super init];
        if (self)
        {
                [[NSNotificationCenter defaultCenter] addObserver:self
                                                         selector:@selector(removeMBProgressHUD:)
                                                             name:@"kRemoveMBProgressHUD"
                                                           object:nil];
                
                bleUnlock = [[BLEDevice_QuickLock alloc] init:self];
                lockDevice = [LockDevice new];
                isSearchConnected = YES;
        }
        return self;
}

//If you are retreated to the background according to the ID search, back to the front desk not to continue the search
- (void)removeMBProgressHUD:(NSNotification *)notification
{
        [self stopSearch];
}

#pragma mark - Three ways to connect lock device.
- (void)startSearch
{
        [self invalidateTimer:timerSearchDevice];
        timerSearchDevice = [NSTimer scheduledTimerWithTimeInterval:0.0f
                                                             target:self
                                                           selector:@selector(searchDeivceTimer)
                                                           userInfo:nil
                                                            repeats:YES];
}

- (void)stopSearch
{
        [bleUnlock BD_stopAllScan];
        [self invalidateTimer:timerSearchDevice];
        [self invalidateTimer:timerSearchWithUUID];
        [self invalidateTimer:timerSearchWithUUIDTimeOut];
        [hudConnectWithUUID removeFromSuperview];
        hudConnectWithUUID = nil;
}

- (void)searchDeivceTimer
{
        if ([bleUnlock BD_getIsConnected]) {
            return;
        }
        
        if (isSearchConnected)    {
                [self searchDeviceHaveConnected];
        }
        else   {
            [self searchDeviceHaveNotConnected];
        }
}

- (void)searchDeviceHaveConnected{
    [bleUnlock BD_getScanDeviceList:2];
}

- (void)searchDeviceHaveNotConnected
{
    /*
     Gunbox
     Gunbox 2.0
     
     Echo
     Gunbox Echo
     
     Big Bertha
     Gunbox SK-1
     SK-1
     
     Doorlock!
     Quicklock Doorlock
     Doorlock
     
     Padlock!
     Quicklock Padlock
     Padlock
     
     
     Quicklock Deadbolt
     Deadbolt
     
     */
    
    
    NSArray *names, *ranges;
   // names = @[@"Padlock!",@"Padlock", @"Doorlock!", @"Gunbox", @"Echo" , @"BigBertha"];
    
    names = @[@"Padlock",@"Padlock!",@"Quicklock Padlock",
              @"Doorlock",@"Doorlock!",@"Quicklock Doorlock",
              @"Gunbox",@"Gunbox 2.0",
              @"Echo",@"Gunbox Echo",
              @"Gunbox SK-1",@"SK-1",@"BigBertha",@"Big Bertha",
              @"Quicklock Deadbolt",@"Deadbolt",@"RF WRITER",@"Gunbox 3.0",@"Gunbox 3.0 Echo",@"LYNKD"];
    
    ranges = @[[NSNumber numberWithInt:0], [NSNumber numberWithInt:0], [NSNumber numberWithInt:0], [NSNumber numberWithInt:0], [NSNumber numberWithInt:0],[NSNumber numberWithInt:0],[NSNumber numberWithInt:0],[NSNumber numberWithInt:0],[NSNumber numberWithInt:0],[NSNumber numberWithInt:0],[NSNumber numberWithInt:0],[NSNumber numberWithInt:0],[NSNumber numberWithInt:0],[NSNumber numberWithInt:0],[NSNumber numberWithInt:0],[NSNumber numberWithInt:0],[NSNumber numberWithInt:0],[NSNumber numberWithInt:0],[NSNumber numberWithInt:0],[NSNumber numberWithInt:0]];
        [bleUnlock BD_initConnectWithNameOfRanges:@"FFD6" localNames:names isRanges:ranges];  // FFD6
}  

- (void)startSearchWithUUID:(NSString *)uuid
{
        timerSearchWithUUIDTimeOut = [NSTimer scheduledTimerWithTimeInterval:60
                                                                      target:self
                                                                    selector:@selector(searchDeviceWithUUIDTimeOut)
                                                                    userInfo:nil
                                                                     repeats:NO];
        timerSearchWithUUID = [NSTimer scheduledTimerWithTimeInterval:1.f
                                                               target:self
                                                             selector:@selector(connectDeviceWithUUIDTimer:)
                                                             userInfo:uuid
                                                              repeats:YES];
        [self showMessageBox:[self getLockNameWithUUID:uuid]];
}

- (void)showMessageBox:(NSString *)lockName
{
        AppDelegate *appdlg = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        hudConnectWithUUID = [MessageBox showHUDAddedTo:appdlg.window.viewForBaselineLayout animated:YES];
        [hudConnectWithUUID setDelegateMessageBox:self];
        hudConnectWithUUID.color = [UIColor blackColor];
        [hudConnectWithUUID setLabelText:[NSString stringWithFormat:@"Connecting device:%@", lockName]];
        [hudConnectWithUUID hide:YES afterDelay:60];
}

- (void)showMessageForBoxHistoryLog
{
    AppDelegate *appdlg = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    hudConnectWithUUID = [MessageBox showHUDAddedTo:appdlg.window.viewForBaselineLayout animated:YES];
    hudConnectWithUUID.color = [UIColor blackColor];
    [hudConnectWithUUID setLabelText:@"Loading..."];
    [hudConnectWithUUID hide:YES afterDelay:60];
}

- (void)stopSearchForBoxHistoryLog
{
    [hudConnectWithUUID removeFromSuperview];
    hudConnectWithUUID = nil;
}

- (NSString *)getLockNameWithUUID:(NSString *)uuid
{
        NSString *lockName = @"";
        NSArray *arrayTempLockDevice = [dbAccess GetAllLockDevice];
        for (LockDevice *tempLock in arrayTempLockDevice)
        {
                if ([tempLock.lock_UUID isEqualToString:uuid])
                {
                        lockName = tempLock.lock_Name;
                        break;
                }
        }
        
        return lockName;
}

- (void)searchDeviceWithUUIDTimeOut
{
        if (timerSearchWithUUID != nil && ![bleUnlock BD_getIsConnected])
        {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connect failed"
                                                                message:@"Don't find the lock can be connected"
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
//            [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"isDeviceConnected"];
        }
        [self invalidateTimer:timerSearchWithUUID];
        [self invalidateTimer:timerSearchWithUUIDTimeOut];
        [hudConnectWithUUID removeFromSuperview];
        hudConnectWithUUID = nil;
}

- (void)connectDeviceWithUUIDTimer:(NSTimer *)timer
{
        [self connectDeviceWithUUID:timer.userInfo];
}

- (void)connectDeviceWithUUIDForThreeSeconds:(NSString *)uuid
{
        [self stopSearch];
        [NSTimer scheduledTimerWithTimeInterval:0.1f
                                         target:self
                                       selector:@selector(connectDeviceHaveConnectedTimeOut)
                                       userInfo:nil
                                        repeats:NO];
        
        [self performSelectorInBackground:@selector(circleConnected:) withObject:uuid];
}

- (void)connectDeviceHaveConnectedTimeOut
{
        if (![bleUnlock BD_getIsConnected])
        {
                [self stopSearch];
                [self startSearch];
        }
}

- (void)circleConnected:(NSString *)uuid
{
        for (int i = 0; i < 3; i++)
        {
                [self connectDeviceWithUUID:uuid];
                [NSThread sleepForTimeInterval:0];
        }
}

- (void)connectDeviceWithUUID:(NSString *)uuid
{
        [bleUnlock BD_initConnect:uuid serviceUUID:@"" localName:@""];
}

#pragma mark - Device Operate Method
- (void)getLockState
{
        [bleUnlock BD_getLockState];
}

- (void)getPasswordSharecode
{
    [bleUnlock BD_getPasswordSharecode];
}

-(void)getZwave
{
    [bleUnlock BD_getZwave];
}

- (void)getPasswordSharecodeTimeBase
{
    [bleUnlock BD_getPasswordSharecodetimeBase];
}

// Control lock status, Open parameters indicate YES or NO to unlock a locked
- (void)openLock:(BOOL)isOpen
{
        // If the lock state is open, click again to unlock, do not do anything to prevent the hardware unlock records duplicate records.
        if (lockIsOpen && isOpen)
        {
                return;
        }
        [bleUnlock BD_setLockControl:isOpen];
}

- (void)verifyPassword
{
        NSString *password = lockDevice.lock_Password;
        Byte b1, b2, b3, b4;
        
        NSScanner *scan = [NSScanner scannerWithString:[password substringWithRange:NSMakeRange(0, 2)]];
        unsigned int n = 0;
        [scan scanHexInt:&n];
        b1 = n;
        
        scan = [NSScanner scannerWithString:[password substringWithRange:NSMakeRange(2, 2)]];
        [scan scanHexInt:&n];
        b2 = n;
        
        scan = [NSScanner scannerWithString:[password substringWithRange:NSMakeRange(4, 2)]];
        [scan scanHexInt:&n];
        b3 = n;
        
        scan = [NSScanner scannerWithString:[password substringWithRange:NSMakeRange(6, 2)]];
        [scan scanHexInt:&n];
        b4 = n;
        
        [bleUnlock BD_setVerifyPassword:b1 password2:b2 password3:b3 password4:b4];
}

// Share code Date Time Base
-(void)addSharecodeTimeBase
{
    NSString *SHARECODE = lockDevice.share_Code_Time;
    
    Byte b1, b2, b3, b4;//, b5, b6, b7, b8, b9, b10, b11, b12;
    
    NSScanner *scan = [NSScanner scannerWithString:[SHARECODE substringWithRange:NSMakeRange(0, 2)]];
    unsigned int n = 0;
    [scan scanHexInt:&n];
    b1 = n;
    
    scan = [NSScanner scannerWithString:[SHARECODE substringWithRange:NSMakeRange(2, 2)]];
    [scan scanHexInt:&n];
    b2 = n;
    
    scan = [NSScanner scannerWithString:[SHARECODE substringWithRange:NSMakeRange(4, 2)]];
    [scan scanHexInt:&n];
    b3 = n;
    
    scan = [NSScanner scannerWithString:[SHARECODE substringWithRange:NSMakeRange(6, 2)]];
    [scan scanHexInt:&n];
    b4 = n;
    
    [bleUnlock BD_share_Code_Time:b1 CODE2:b2 CODE3:b3 CODE4:b4];
}

// Add share code User Access wise
-(void)addSharecode
{
    NSString *SHARECODE = lockDevice.share_Code;
    NSString *sCount = lockDevice.share_Code_Count;
    
    Byte b1, b2, b3, b4, bc;
    
    NSScanner *scan = [NSScanner scannerWithString:[SHARECODE substringWithRange:NSMakeRange(0, 2)]];
    unsigned int n = 0;
    [scan scanHexInt:&n];
    b1 = n;
    
    scan = [NSScanner scannerWithString:[SHARECODE substringWithRange:NSMakeRange(2, 2)]];
    [scan scanHexInt:&n];
    b2 = n;
    
    scan = [NSScanner scannerWithString:[SHARECODE substringWithRange:NSMakeRange(4, 2)]];
    [scan scanHexInt:&n];
    b3 = n;
    
    scan = [NSScanner scannerWithString:[SHARECODE substringWithRange:NSMakeRange(6, 2)]];
    [scan scanHexInt:&n];
    b4 = n;
    
    scan = [NSScanner scannerWithString:[sCount substringWithRange:NSMakeRange(0, 2)]];
    [scan scanHexInt:&n];
    bc = n;
    
    [bleUnlock BD_share_Code_Count:b1 CODE2:b2 CODE3:b3 CODE4:b4 CODECOUNT:bc];
}

// Delete RFID
-(void)deleteRFID
{
    NSString *RFID = lockDevice.TAG_DELETE_IMEI_OR_RFID;
    Byte b1, b2, b3, b4, b5, b6, b7;
    
    NSScanner *scan = [NSScanner scannerWithString:[RFID substringWithRange:NSMakeRange(0, 2)]];
    unsigned int n = 0;
    [scan scanHexInt:&n];
    b1 = n;
    
    scan = [NSScanner scannerWithString:[RFID substringWithRange:NSMakeRange(2, 2)]];
    [scan scanHexInt:&n];
    b2 = n;
    
    scan = [NSScanner scannerWithString:[RFID substringWithRange:NSMakeRange(4, 2)]];
    [scan scanHexInt:&n];
    b3 = n;
    
    scan = [NSScanner scannerWithString:[RFID substringWithRange:NSMakeRange(6, 2)]];
    [scan scanHexInt:&n];
    b4 = n;
    
    scan = [NSScanner scannerWithString:[RFID substringWithRange:NSMakeRange(8, 2)]];
    [scan scanHexInt:&n];
    b5 = n;
    
    scan = [NSScanner scannerWithString:[RFID substringWithRange:NSMakeRange(10, 2)]];
    [scan scanHexInt:&n];
    b6 = n;
    
    scan = [NSScanner scannerWithString:[RFID substringWithRange:NSMakeRange(12, 2)]];
    [scan scanHexInt:&n];
    b7 = n;
    
    [bleUnlock BD_deleteRFID:b1 RFID2:b2 RFID3:b3 RFID4:b4 RFID5:b5 RFID6:b6 RFID7:b7];
}

- (void)changePasssword:(NSString *)newPassword
{
        NSString *oldPassword = lockDevice.lock_Password;
        
        Byte old1, old2, old3, old4, new1, new2, new3, new4;
        
        NSScanner *scan = [NSScanner scannerWithString:[oldPassword substringWithRange:NSMakeRange(0, 2)]];
        unsigned int n = 0;
        [scan scanHexInt:&n];
        old1 = n;
        
        scan = [NSScanner scannerWithString:[oldPassword substringWithRange:NSMakeRange(2, 2)]];
        [scan scanHexInt:&n];
        old2 = n;
        
        scan = [NSScanner scannerWithString:[oldPassword substringWithRange:NSMakeRange(4, 2)]];
        [scan scanHexInt:&n];
        old3 = n;
        
        scan = [NSScanner scannerWithString:[oldPassword substringWithRange:NSMakeRange(6, 2)]];
        [scan scanHexInt:&n];
        old4 = n;
        
        scan = [NSScanner scannerWithString:[newPassword substringWithRange:NSMakeRange(0, 2)]];
        [scan scanHexInt:&n];
        new1 = n;
        
        scan = [NSScanner scannerWithString:[newPassword substringWithRange:NSMakeRange(2, 2)]];
        [scan scanHexInt:&n];
        new2 = n;
        
        scan = [NSScanner scannerWithString:[newPassword substringWithRange:NSMakeRange(4, 2)]];
        [scan scanHexInt:&n];
        new3 = n;
        
        scan = [NSScanner scannerWithString:[newPassword substringWithRange:NSMakeRange(6, 2)]];
        [scan scanHexInt:&n];
        new4 = n;
        
        // The new password is stored in a variable, replace the original password with variable success after changing passwords, modify the new password failed emptied variables stored
        tempNewPassword = newPassword;
        
        [bleUnlock BD_setModifyPassword:old1
                           oldPassword2:old2
                           oldPassword3:old3
                           oldPassword4:old4
                           newPassword1:new1
                           newPassword2:new2
                           newPassword3:new3
                           newPassword4:new4];
}

- (void)setAutoLockTime:(int)time
{
        if (time == 0) {return;}
        [bleUnlock BD_setLockOpenTime:time];
        
        [bleUnlock BD_getUserName];
}

- (void)setClearAllFingerPrints
{
    [bleUnlock BD_setClearAllFingerPrints];
}

- (void)setLEDTime:(int)time
{
    if (time == 0) {return;}
    [bleUnlock BD_setLEDTime:time];
}

- (void)setSureSet:(int)percent
{
    if (percent == 0) {return;}
    [bleUnlock BD_setSureSet:percent];
}

- (void)setAlarm:(int)time delay:(int)delay
{
    [bleUnlock BD_setAlarm:time delay:delay];
}

- (void)setZwave:(int)param
{
    [bleUnlock BD_setZwave:param];
}

- (void)setUserNameToLock
{
    NSString *userName = [[NSUserDefaults standardUserDefaults] valueForKey:@"kUserName"];
    [bleUnlock BD_setUserName:userName];
    
    [bleUnlock BD_getUserName];
}

- (void)setKeyCode:(NSString *)keyCode
{
    [bleUnlock BD_setPassCode:keyCode];
    
//    keyCode = [NSString stringWithFormat:@"%lu%@",(unsigned long)keyCode.length, keyCode];
//
//    
//    Byte b1, b2, b3;
//    
//        NSScanner *scan = [NSScanner scannerWithString:[keyCode substringWithRange:NSMakeRange(0, 2)]];
//        unsigned int n = 0;
//        [scan scanHexInt:&n];
//        b1 = n;
//        
//        scan = [NSScanner scannerWithString:[keyCode substringWithRange:NSMakeRange(2, 2)]];
//        [scan scanHexInt:&n];
//        b2 = n;
//        
//        scan = [NSScanner scannerWithString:[keyCode substringWithRange:NSMakeRange(4, 2)]];
//        [scan scanHexInt:&n];
//        b3 = n;
//
//    [bleUnlock BD_setPassCode:b1 C2:b2 C3:b3];
    
}

- (void)getWifiMacAddress
{
    [bleUnlock BD_getWifiMacAddress];
}

- (void)initiativeDisConnect
{
    [bleUnlock BD_disConnect];
}

- (void)getHistoryLog
{
    [bleUnlock BD_getHistoryLog];
}

-(void)getRFIDList
{
    [bleUnlock BD_getRFIDList];
}

-(void)getRFIDAdd
{
    [bleUnlock BD_getRFIDAdd];
}

-(void)setUserIDForRFIDCard_UserId:(NSString*)strUserId{
    [bleUnlock BD_setUserIDForRFIDCard_UserId:strUserId];
}

-(void)setRFIDWriter{
    [bleUnlock BD_setRFIDWriter];
}

-(void)getAllRFID{
    [bleUnlock BD_getAllRFID];
}

-(void)getFingerprintLearn
{
    [bleUnlock BD_getFingerprintLearn];
}

// cancel fingerprint learn
-(void)getCancelFingerprintLearn
{
    [bleUnlock BD_getCancelFingerprintLearn];
}

- (BOOL)getIsConnected
{
    return [bleUnlock BD_getIsConnected];
}

- (void)getBatteryLevel
{
    [bleUnlock BD_getBetteryLevel];
}

-(void)getFingerprintCount
{
    [bleUnlock BD_getFingerprintCount];
}

-(void)getAlarmState
{
    [bleUnlock BD_getAlarmState];
}

- (void)checkPasswordShareCode
{
    [bleUnlock BD_getPasswordResult];
}

#pragma mark - MessageBoxProtocol
- (void)cancelShowMessageBox:(MessageBox *)messageBox
{
        if (messageBox == hudConnectWithUUID)
        {
                [self searchDeviceWithUUIDTimeOut];
        }
        [messageBox removeFromSuperview];
        messageBox = nil;
}

#pragma mark - Protocol_BLEDeviceResult
- (void)BR_connectOK:(int)DeviceID
{
        [self stopSearch];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"kRemoveMyLocksVC" object:nil];
        
        lockDevice = [dbAccess GetLockDeviceWithDeviceUUID:[bleUnlock BD_getDeviceUUID]];
    
        // Synchronize phone time to the hardware
        [bleUnlock BD_setCurrentTime];
        // Get firmware version
        [bleUnlock BD_getFirmwareRevision];
        // After successful connection open the lock status is enabled, the lock status will automatically upload
        [bleUnlock BD_enableLockStateUpdate:YES];
        // Open authentication password automatically upload enabled
        [bleUnlock BD_enablePasswordResultUpdate:YES];
        // Open the historical data can be uploaded
        [bleUnlock BD_enableHistoryLogUpdate:YES];
    
        [bleUnlock BD_enableZwave:YES];
    
        [bleUnlock BD_enableShareCode:YES];
    
        [bleUnlock BD_enableRFIDListUpdate:YES];

        [bleUnlock BD_enableRFIDAdd:YES];

        [bleUnlock BD_enableFingerprintLearn:YES];
    
        // Open electricity automatically upload enabled
        [bleUnlock BD_enableBatteryLevel:YES];
        
        if (lockDevice.lock_ID == 0)
        {
            if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"passwordCall"] isEqualToString:@"NO"])
            {
                [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:@"passwordCall"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                // The first connection, direct access to the password screen
                [[NSNotificationCenter defaultCenter] postNotificationName:@"kInputPassword" object:nil];
                remainInputCount = 3;
            }
        }
        else
        {
            if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"passwordCall"] isEqualToString:@"NO"])
            {
                [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:@"passwordCall"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                // Not the first time the connection, verify the password stored in the database
                [self verifyPassword];
            }
        }
}

- (void)BR_connectFail:(int)nFAILETYPE DeviceID:(int)DeviceID
{
        NSLog(@"connectFail");
}

- (void)BR_didDisconnect:(int)DeviceID
{
        // Disconnect when modifying the current state of preservation of the lock is off, to prevent accidental disconnection when the reconnection can not unlock
        lockIsOpen = NO;
    
        // Remove interface to send notifications
        [[NSNotificationCenter defaultCenter] postNotificationName:@"kDisConnect" object:nil];
}

- (void)BR_didUpdateRSSI:(float)value DeviceID:(int)DeviceID
{
        //
}

//  “LocalName,ServiceUUID,UUID,RSSI,TxPower”，Middle separated by commas
- (void)BR_scanDeviceList:(NSMutableArray *)arrayScanDeviceList DeviceID:(int)DeviceID
{
        if ([bleUnlock BD_getIsConnected] || arrayScanDeviceList.count < 1)
        {
                return;
        }
        
        NSArray *arrayUUID = [[dbAccess GetAllLockDevice] valueForKeyPath:@"lock_UUID"];
        
        for (NSString *roadcasting in arrayScanDeviceList)
        {
                NSArray *arrayRoadcastingSeperateByDot = [roadcasting componentsSeparatedByString:@","];
                if (arrayRoadcastingSeperateByDot.count > 2)
                {
                        NSString *scanUUID = arrayRoadcastingSeperateByDot[2];
                        if ([self UUID:scanUUID IsExistInArray:arrayUUID])
                        {
                                [self connectDeviceWithUUIDForThreeSeconds:scanUUID];
                                return;
                        }
                }
        }
}

- (void)BR_getMACAddress:(Byte *)address length:(int)length DeviceID:(int)DeviceID
{
        //
}

#pragma mark - Protocol_BLEDeviceResult_QuickLock
- (void)BR_systemID:(NSString *)systemID DeviceID:(int)DeviceID
{
        //
}

- (void)BR_modelNumber:(NSString *)modelNumber DeviceID:(int)DeviceID
{
        //
}

// Get the firmware version number, just to show that the database does not exist
- (void)BR_firmwareRevision:(NSString *)vision visionDate:(NSString *)visionDate DeviceID:(int)DeviceID
{
        lockDevice.lock_FirmwareRevision = vision;
}


// Get hardware type ：1PadLock，2DoorLock, 3Echo(QuickBox), 4GunBox, 5BigBertha
-(void)BR_protocolVersion:(int)lockID protocolVersion:(NSString *)protocolVersion DeviceID:(int)DeviceID;
{
        // The first time you connect the device only needs to read the device type, and then stored in the database, the next from a database acquisition (device type will not be changed)
        switch (lockID)
    {
        case PADLOCK:
        {
            lockDevice.lock_Name = @"Padlock";
            [bleUnlock BD_setLockOpenTime:lockDevice.lock_AutoLockTime];
        }
            break;
        case DOORLOCK:
        {
            lockDevice.lock_Name = @"Doorlock";
            [bleUnlock BD_setLockOpenTime:lockDevice.lock_AutoLockTime];
        }
            break;
        case QUICKBOX:
        {
            lockDevice.lock_Name = @"Gunbox Echo";
            [bleUnlock BD_setLockOpenTime:lockDevice.lock_AutoLockTime];
        }
            break;
        case GUNBOX:
        {
            lockDevice.lock_Name = @"Gunbox";
            [bleUnlock BD_setLockOpenTime:lockDevice.lock_AutoLockTime];
        }
            break;
        case TALLGUNBOX:
        {
            lockDevice.lock_Name = @"GunBox SK-1";
            [bleUnlock BD_setLockOpenTime:lockDevice.lock_AutoLockTime];
        }
            break;
        case RFWRITER:
        {
            lockDevice.lock_Name = @"RF Writer";
            [bleUnlock BD_setLockOpenTime:lockDevice.lock_AutoLockTime];
        }
            break;
            /*case 5:
             {
             lockDevice.lock_Name = @"Medicine box";
             [bleUnlock BD_setLockOpenTime:lockDevice.lock_AutoLockTime];
             }
             break;
             case 6:
             {
             lockDevice.lock_Name = @"Intellivent";
             [bleUnlock BD_setLockOpenTime:lockDevice.lock_AutoLockTime];
             }
             break;*/
            
        default:
            break;
    }
    
        lockDevice.lock_Type = lockID;
        lockDevice.lock_UUID = [bleUnlock BD_getDeviceUUID];
        lockDevice.lock_AutoUnLock = NO;
        lockDevice.lock_ID = [dbAccess InsertLockDevice:lockDevice];
    
        // get Share code User based
        [quickLockDeviceManage getPasswordSharecode];
        // get Share code Time based
        [quickLockDeviceManage getPasswordSharecodeTimeBase];
    
        [quickLockDeviceManage getZwave];
    
        // Jump to a successful connection interface, according to the device type
        [[NSNotificationCenter defaultCenter] postNotificationName:@"kConnectSuccess" object:nil];
}

-(void)BR_getPasswordSharecode:(int)userCount shareCode:(NSString*)shareCode
{
    HistoryLog *historyLog = [HistoryLog new];
    historyLog.userCount = userCount;
    historyLog.shareCode = shareCode;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"kShareCode" object:historyLog];
}

-(void)BR_getPasswordSharecodeTimeBase:(NSDate *)startDate endDate:(NSDate *)endDate shareCode:(NSString*)shareCode time1:(NSString *)time1 time2:(NSString *)time2 flag:(int)flag
{
    HistoryLog *historyLog = [HistoryLog new];
    historyLog.shareCodeStartTime = startDate;
    historyLog.shareCodeEndTime = endDate;
    historyLog.shareCodeTimeBase = shareCode;
    historyLog.shareCodeTime1 = time1;
    historyLog.shareCodeTime2 = time2;
    historyLog.shareCodeTimeFlag = flag;
    
    NSString *strStDt = [NSString stringWithFormat:@"%@",startDate];
    NSDateFormatter *dtFormat = [NSDateFormatter new];
    [dtFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss +zzzz"];
    NSDate *dt = [dtFormat dateFromString:strStDt];
    [dtFormat setDateFormat:@"dd MMM yyyy"];
    strStDt = [dtFormat stringFromDate:dt];
    historyLog.strStartDat = strStDt;
    
    NSString *strEdDt = [NSString stringWithFormat:@"%@",endDate];
    dtFormat = [NSDateFormatter new];
    [dtFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss +zzzz"];
    dt = [dtFormat dateFromString:strEdDt];
    [dtFormat setDateFormat:@"dd MMM yyyy"];
    strEdDt = [dtFormat stringFromDate:dt];
    historyLog.strEndDat = strEdDt;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"kShareCodeTimeBase" object:historyLog];
}

- (void)BR_batteryLevel:(int)battLevel DeviceID:(int)DeviceID
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"kBattery" object:[NSNumber numberWithInt:battLevel]];
}

- (void)BR_currentTime:(NSDate *)devTime DeviceID:(int)DeviceID{}

-(void)BR_PasswordResult:(BOOL)isModify isValidPassword:(BOOL)isValidPassword DeviceID:(int)DeviceID;
{
    if (isModify)
    {
        [self ModifyPassword:isValidPassword];
    }
    else
    {
        [self CheckPassword:isValidPassword];
    }
}

- (void)BR_lockState:(BOOL)isLockOpened DeviceID:(int)DeviceID
{
        lockIsOpen = isLockOpened;
        
        // Send status tells Door Lock current lock status
        [[NSNotificationCenter defaultCenter] postNotificationName:@"kLockState"
                                                            object:[NSNumber numberWithBool:isLockOpened]];
}

- (void)BR_lockOpenTime:(Byte)lockOpenSecond DeviceID:(int)DeviceID
{
        // Get to automatically lock time, if lock ID of 0 indicates that the first connection, start getting the hardware device type
    int time = lockOpenSecond;
    
    if(time < 3)
    {
        lockDevice.lock_AutoLockTime = 3;
    }else
    {
       lockDevice.lock_AutoLockTime = time;
    }
    
        if (lockDevice.lock_ID == 0)
        {
                [bleUnlock BD_getProtocolVersion];
        }
}

- (void) BR_alarm_count:(Byte)state delay:(Byte)delay DeviceID:(int)DeviceID
{
    int alarmState = state;
    int alarmDelay = delay;
    
    if (lockDevice.lock_ID == 0)
    {
        [bleUnlock BD_getAlarmState];
    }
    
    lockDevice.alarmPrevoiusState =  alarmState;
    lockDevice.alarmDelay = alarmDelay;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"setAlarmState" object:nil];
}

// Zwave
- (void) BR_Zwave: (Byte) zwave DeviceID: (int) DeviceID
{
    if (lockDevice.lock_ID == 0)
    {
        [bleUnlock BD_getZwave];
    }
    else if (zwave == 0XFF)
    {
        // No zwave available
        lockDevice.zwaveState = -1;
    }
    else if (zwave == 0X00)
    {
        // zwave off
        lockDevice.zwaveState = 0;
    }
    else if (zwave == 0X01)
    {
        // zwave on
        lockDevice.zwaveState = 1;
    }
}

- (void) BR_fingerprintCount: (Byte) count DeviceID: (int) DeviceID
{
    int figureCount = count;
    
    if (lockDevice.lock_ID == 0)
    {
        [bleUnlock BD_getFingerprintCount];
    }
    
    lockDevice.lock_FingerprintCount =  figureCount;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"setFingerPrintCount" object:[NSNumber numberWithInt:count]];
}

-(void)BR_userName:(NSString *)userName DeviceID:(int)DeviceID
{
        NSLog(@"username:%@", userName);
}

-(void)BR_historyLog:(int)devType idOrName:(NSString *)idOrName logDate:(NSDate *)logDate isOpenLock:(BOOL)isOpenLock DeviceID:(int)DeviceID {
        HistoryLog *historyLog = [HistoryLog new];
        historyLog.operateLockType = devType;
        historyLog.operateLockDevice = idOrName;
        historyLog.operateLockTime = logDate;
        historyLog.isOpenLock = isOpenLock;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"kReceiveHistoryLog" object:historyLog];
    
   
    
}

- (void)invalidateTimer:(NSTimer *)timer{
        [timer invalidate];
        timer = nil;
}

- (void)ModifyPassword:(BOOL)result
{
        if (result)
        {
                lockDevice.lock_Password = tempNewPassword;
                [[NSNotificationCenter defaultCenter] postNotificationName:@"kModifyPasswordSuccess" object:nil];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                                message:@"Modify password succeed"
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                alert.tag = 5;
                [alert show];
        }
        else
        {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                                message:@"Modify password failed"
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
        }
        
        tempNewPassword = @"";
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 5)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"goBack" object:nil];
    }
}

- (void)CheckPassword:(BOOL)result
{
        // After entering the correct password is set to the hardware username
        if (result)
        {
                [self setUserNameToLock]; // 2 time  &  1 time
        }
        
        if (lockDevice.lock_ID == 0)
        {// The first connection
                if (result)
                {
                    // If the password is entered correctly, read the automatic locking time
                    [bleUnlock BD_getLockOpenTime]; // 1 time
                    [bleUnlock BD_getZwave];
                }
                else
                {
                    // Send notification process user password input error, prompting the user to re-enter
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"kPasswordError" object:nil];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"PasswordTextBlank" object:nil];
                }
        }
        else
        {//Not the first time the connection
                if (result)
                {
                    // If the password is entered correctly, directly into the interface If the connection is successful connection interface
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"kConnectSuccess" object:nil];  //2 time
//                    [bleUnlock BD_getLockOpenTime];
                    [bleUnlock BD_getZwave];
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [quickLockDeviceManage getPasswordSharecode];
                        [quickLockDeviceManage getPasswordSharecodeTimeBase];
                    });
                }
                else {
                        // If the database is stored in the password is wrong, explain lock passwords are other reasons to get rid of, and re-take process first connection, allowing users to re-enter the password
                        [dbAccess DeleteLockDevice:lockDevice.lock_ID];
                        lockDevice = [LockDevice new];
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"kInputPassword" object:nil];
                        
                        // This time the password should be changed 3 times.
                        remainInputCount = 3;
                        
                        UIAlertView *alert = [[UIAlertView alloc]
                                              initWithTitle:@"Warning!"
                                              message:@"The password of the lock was modified.\nInput password please!"
                                              delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
                        [alert show];
                }
        }
}

- (BOOL)UUID:(NSString *)uuid IsExistInArray:(NSArray *)arrayUUID
{
        for (NSString *tempUUID in arrayUUID)
        {
                if ([uuid isEqualToString:tempUUID])
                {
                        return YES;
                }
        }
        
        return NO;
}

@end
