//
//  BuyNow.m
//  QuickLock
//
//  Created by Ankit on 26/12/15.
//  Copyright (c) 2015 Bge. All rights reserved.
//

#import "BuyNow.h"

@interface BuyNow () <SlideNavigationControllerDelegate>
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;

@end

@implementation BuyNow

- (void)viewDidLoad {
    [super viewDidLoad];

    _lblTitle.textColor = titleorange;
    
    // initialize drawer menu & button
    [SlideNavigationController sharedInstance].portraitSlideOffset = appDelegate.portraitDrawer;
    
    UIButton *btnMenu = [UIButton buttonWithType:UIButtonTypeCustom];
    btnMenu.frame = CGRectMake(10, 28, 35, 35);
    [btnMenu setImage:[UIImage imageNamed:@"navigation.png"] forState:UIControlStateNormal];
    [btnMenu addTarget:[SlideNavigationController sharedInstance] action:@selector(toggleLeftMenu) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnMenu];
}

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
}

- (IBAction)btnGoToProduct:(UIButton *)sender
{
    NSString *strUrl;
    
    switch (sender.tag)
    {
        case 1: // 1 padlock
            strUrl = @"https://www.thequicklock.com/cart/category-padlock?product_id=140";
            break;
            
        case 2: // 2 doorlock
            strUrl = @"https://www.thequicklock.com/cart/category-doorlock/quicklock-doorlock";
            break;
            
        case 3: // 3 deadlock
            strUrl = @"https://www.thequicklock.com/cart/category-doorlock/quicklock-doorlock";
            break;
            
        case 4: // 4 Gunbox
            strUrl = @"https://www.thegunbox.com/cart/products/the-gun-box-biometric-2.0";
            break;
            
        case 5: // 5 GunboxEcho
            strUrl = @"https://www.thegunbox.com/cart/products/the-gun-box-echo";
            break;
            
        case 6: // 6 big bertha
            strUrl = @"https://www.thegunbox.com/cart/products/the-gun-box-bertha";
            break;
            
        default:
            break;
    }
    
    NSURL *url = [NSURL URLWithString:strUrl];
    [[UIApplication sharedApplication] openURL:url];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
