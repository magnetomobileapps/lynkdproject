//
//  KeyCode.m
//  QuickLock
//
//  Created by PUNDSK001 on 20/03/17.
//  Copyright © 2017 Bge. All rights reserved.
//

#import "KeyCode.h"

@interface KeyCode ()
{
    IBOutlet UILabel *lblKeyCode;
    NSMutableString *strKeyCode;
    NSMutableArray *arrStr;
    IBOutlet UILabel *lblTitleKeyNote;
    IBOutlet UILabel *lblNote;
}
@end

@implementation KeyCode

- (void)viewDidLoad
{
    [super viewDidLoad];
    strKeyCode = [[NSMutableString alloc]init];
    arrStr = [[NSMutableArray alloc]init];
}

- (IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}

- (IBAction)btnKeyCode:(UIButton *)sender
{
    if (arrStr.count != 0)
    {
        arrStr = [strKeyCode componentsSeparatedByString:@" "];
        [arrStr removeObjectAtIndex:arrStr.count-1];
    }
    
    if (arrStr.count < 10 || (sender.tag > 10 && arrStr.count <= 10))
    {
        switch (sender.tag)
        {
            case 1:
                [strKeyCode appendString:[NSString stringWithFormat:@"%d ",(int)sender.tag]];
                [arrStr addObject:strKeyCode];
                break;
                
            case 2:
                [strKeyCode appendString:[NSString stringWithFormat:@"%d ",(int)sender.tag]];
                [arrStr addObject:strKeyCode];
                break;
                
            case 3:
                [strKeyCode appendString:[NSString stringWithFormat:@"%d ",(int)sender.tag]];
                [arrStr addObject:strKeyCode];
                break;
                
            case 4:
                [strKeyCode appendString:[NSString stringWithFormat:@"%d ",(int)sender.tag]];
                [arrStr addObject:strKeyCode];
                break;
                
            case 5:
                [strKeyCode appendString:[NSString stringWithFormat:@"%d ",(int)sender.tag]];
                [arrStr addObject:strKeyCode];
                break;
            
            case 6:
                [strKeyCode appendString:[NSString stringWithFormat:@"%d ",(int)sender.tag]];
                [arrStr addObject:strKeyCode];
                break;
            
            case 7:
                [strKeyCode appendString:[NSString stringWithFormat:@"%d ",(int)sender.tag]];
                [arrStr addObject:strKeyCode];
                break;
            
            case 8:
                [strKeyCode appendString:[NSString stringWithFormat:@"%d ",(int)sender.tag]];
                [arrStr addObject:strKeyCode];
                break;
            
            case 9:
                [strKeyCode appendString:[NSString stringWithFormat:@"%d ",(int)sender.tag]];
                [arrStr addObject:strKeyCode];
                break;
                
            case 10: // 0
                [strKeyCode appendString:[NSString stringWithFormat:@"%d ",0]];
                [arrStr addObject:strKeyCode];
                break;
                
            default:
                break;
        }
        
        lblKeyCode.text = strKeyCode;
    }
}

-(IBAction)clear:(id)sender
{
//    strKeyCode = [[NSMutableString alloc]init];
//
//    lblKeyCode.text = @"Add Key code";
//    [arrStr removeAllObjects];
    ///////////
    
    if (arrStr.count != 0)
    {
        [arrStr removeLastObject];
        
        [strKeyCode deleteCharactersInRange:NSMakeRange(strKeyCode.length-2, 2)];
        
        lblKeyCode.text = strKeyCode;
        
        if (arrStr.count == 0)
        {
            lblKeyCode.text = @"Add Key code";
        }
    }
}

-(IBAction)Add:(id)sender
{
    if ([lblKeyCode.text isEqualToString:@"Add Key code"])
    {
        [self.view makeToast:@"Please enter Key code"];
    }
    else if (arrStr.count < 5)
    {
        [self.view makeToast:@"Please enter minimum 5 digit and maximum 10 digit key code"];
    }
    else
    {
        [quickLockDeviceManage setKeyCode:strKeyCode];
        [self.view makeToast:@"Successfully added the new key code"];
        strKeyCode = [[NSMutableString alloc]init];
        lblKeyCode.text = @"Add Key code";
        [arrStr removeAllObjects];
    }
}

-(IBAction)disable:(id)sender
{
    strKeyCode = @"0";
    [quickLockDeviceManage setKeyCode:strKeyCode];
    [self.view makeToast:@"Successfully disabled key code"];
    strKeyCode = [[NSMutableString alloc]init];
    lblKeyCode.text = @"Add Key code";
    [arrStr removeAllObjects];
}

@end
