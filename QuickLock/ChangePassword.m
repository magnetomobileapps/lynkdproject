//
//  ChangePassword.m
//  QuickLock
//
//  Created by PUNDSK001 on 26/12/15.
//  Copyright © 2015 Bge. All rights reserved.
//

#import "ChangePassword.h"

@interface ChangePassword () <ChangePasswordDelegate>
{
    ChangePassword *changePwd;
    UIToolbar* keyboardToolbar;
    UIBarButtonItem *btnFlexible, *btnDone;
    IBOutlet UILabel *lblTitle;
}
// Properties
@property (weak, nonatomic) IBOutlet UITextField *txtCurrentPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtNewPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtConfirmPassword;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;

@end

@implementation ChangePassword

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self txtLayer:_txtCurrentPassword buttonLayer:_btnSave];
    [self txtLayer:_txtNewPassword buttonLayer:_btnCancel];
    [self txtLayer:_txtConfirmPassword buttonLayer:_btnCancel];
    
    lblTitle.textColor = titleorange;
    
    changePwd.delegate = self;
    //inputPasswordView = [[InputPassword alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    NSString *password = quickLockDeviceManage.lockDevice.lock_Password;
    _txtCurrentPassword.text = password;
    
    // Declare Notification to go Back
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(btnBack:)
                                                 name:@"goBack"
                                               object:nil];
    [self createInputAccessoryView];

}

-(void)createInputAccessoryView
{
    keyboardToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 44)];
    keyboardToolbar.tintColor = [UIColor darkGrayColor];
    keyboardToolbar.backgroundColor = [UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:1.0];

    btnFlexible = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    btnDone = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneTyping)];
    
    NSArray *arr = @[btnFlexible,btnDone];
    [keyboardToolbar setItems:arr];
    
    [self.txtNewPassword setInputAccessoryView:keyboardToolbar];
    [self.txtConfirmPassword setInputAccessoryView:keyboardToolbar];
}

// apply Layer to apply border
-(void)txtLayer:(UITextField *)textfield buttonLayer:(UIButton *)buttom
{
    textfield.backgroundColor = lightgray;
}

// Limit the input box can only enter a number, and a maximum length of 8
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([string isEqualToString:@"\n"] || [string isEqualToString:@""])
    {
        return YES;
    }
    else if (textField.text.length > 7)
    {
        return NO;
    }
    else
    {
        return [kDigitalRange rangeOfString:string].location != NSNotFound;
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (screenSize.height == 568)
    {
        if (textField == _txtConfirmPassword)
        {
            self.view.transform = CGAffineTransformMakeTranslation(0, -30);
        }
        else
        {
            self.view.transform = CGAffineTransformMakeTranslation(0, 0);
        }
    }
    if (screenSize.height == 480)
    {
        if (textField == _txtConfirmPassword)
        {
            self.view.transform = CGAffineTransformMakeTranslation(0, -70);
        }
        else
        {
            self.view.transform = CGAffineTransformMakeTranslation(0, 0);
        }
    }
}

-(void)doneTyping
{
    self.view.transform = CGAffineTransformMakeTranslation(0, 0);
    [_txtNewPassword resignFirstResponder];
    [_txtConfirmPassword resignFirstResponder];
}

- (IBAction)btnSave:(id)sender
{
    BOOL passwordIsValid = NO;
    
    // Password length is eight digits, if the length is not legitimate, the user is prompted
    if (_txtCurrentPassword.text.length != 8 || _txtNewPassword.text.length != 8 || _txtConfirmPassword.text.length != 8)
    {
        passwordIsValid = NO;
    }
    else if (![_txtConfirmPassword.text isEqualToString:_txtNewPassword.text])
    {
//        passwordIsValid = NO;
        [self.view makeToast:@"Password does not match" duration:2.0 position:@"center"];
        return;
    }
    else
    {
        //Determine whether the password is all digital, if not all, it will pop back up input
        NSString *password = _txtNewPassword.text;
        if (password.length != 0)
        {
            for (int i = 0; i < password.length; i++)
            {
                if ([kDigitalRange rangeOfString:[password substringWithRange:NSMakeRange(i, 1)]].location == NSNotFound)
                {
                    passwordIsValid = NO;
                    break;
                }
                
                if (i == password.length - 1)
                {
                    passwordIsValid = YES;
                }
            }
        }
    }
    
    if (passwordIsValid)
    {
        [self returnPassword:_txtNewPassword.text];
    }
    else
    {
        [self.view makeToast:@"Password must be 8 digital" duration:2.0 position:@"center"];
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning!"
//                                                        message:@"Password must be 8 digital"
//                                                       delegate:nil
//                                              cancelButtonTitle:@"OK"
//                                              otherButtonTitles:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"PasswordTextBlank" object:nil];

//        [alert show];
    }
    [_txtCurrentPassword resignFirstResponder];
    [_txtNewPassword resignFirstResponder];
    [_txtConfirmPassword resignFirstResponder];
}

// Password successfully Modified
- (void)returnPassword:(NSString *)password
{
    [quickLockDeviceManage changePasssword:password];
    [dbAccess UpdateLockDevice:quickLockDeviceManage.lockDevice WithLockID:quickLockDeviceManage.lockDevice.lock_ID];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"kModifyPasswordSuccess" object:nil];
}

// Cancel Modifyig Password
- (void)cancelModifyPassword
{
    [self.delegate cancelModifyPassword];
    [_txtCurrentPassword resignFirstResponder];
    [_txtNewPassword resignFirstResponder];
}

// Keyboard will be returned
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    self.view.transform = CGAffineTransformMakeTranslation(0, 0);
    
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)btnBack:(id)sender
{
    [_txtNewPassword resignFirstResponder];
    [self.navigationController popViewControllerAnimated:NO];
}

- (IBAction)btnCancel:(id)sender
{
    [self.delegate cancelModifyPassword];
    [self.navigationController popViewControllerAnimated:NO];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
