//
//  IntelliventHistoryLog.m
//  QuickLock
//
//  Created by PUNDSK001 on 07/01/16.
//  Copyright © 2016 Bge. All rights reserved.
//

#import "IntelliventHistoryLog.h"
#import "DashedLineView.h"
#import "FSLineChart.h"

@interface IntelliventHistoryLog ()
{
    DashedLineView *viewOfChart, *viewOfTemperatureChart;
    UIFont *fontLabels, *fontPercent;
    IBOutlet UILabel *lblTitle;
}
// properties
@property (weak, nonatomic) IBOutlet UIView *viewPercentChart;
@property (weak, nonatomic) IBOutlet UIView *viewTempChart;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@end

@implementation IntelliventHistoryLog

- (void)viewDidLoad
{
    [super viewDidLoad];
 
    _lblDate.layer.cornerRadius = 6;
    _lblDate.layer.masksToBounds = YES;
    
    fontLabels = [UIFont boldSystemFontOfSize:9];
    fontPercent = [UIFont boldSystemFontOfSize:10];
    
    // call method to draw chart
    [self drawChart];
}
- (IBAction)btnBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}

-(void)drawChart
{
//    _viewPercentChart.layer.cornerRadius = 10;
//    _viewPercentChart.layer.masksToBounds = YES;
//    _viewPercentChart.layer.borderColor = lightgray.CGColor;
//    _viewPercentChart.layer.borderWidth = 6;
    
    ////// Percent Open Chart ///////
    viewOfChart = [[DashedLineView alloc] initWithFrame:CGRectMake(40, 10, [UIScreen mainScreen].bounds.size.width - 60, 120)];
    viewOfChart.backgroundColor = [UIColor clearColor];

    UILabel *lbl1 = [[UILabel alloc]initWithFrame:CGRectMake(40, 10, 2, 120)];
    UILabel *lbl2 = [[UILabel alloc]initWithFrame:CGRectMake(40, 130, self.view.frame.size.width-60, 2)];
    lbl1.backgroundColor = lbl2.backgroundColor = lightgray;
    [_viewPercentChart addSubview:lbl1];
    [_viewPercentChart addSubview:lbl2];
    
    // 100% label on left top
    UILabel *lblHundred = [[UILabel alloc]initWithFrame:CGRectMake(0, 10, 37, 12)];
    lblHundred.text = @"100%";
    lblHundred.textColor = [UIColor grayColor];
    lblHundred.font = fontPercent;
    lblHundred.textAlignment = NSTextAlignmentRight;
    [_viewPercentChart addSubview:lblHundred];
    
    // Percent Open Label in left center
    UILabel *lblOpen = [[UILabel alloc]initWithFrame:CGRectMake(0, (lbl1.frame.origin.y+lbl1.frame.size.height)/2, 39, 22)];
    lblOpen.text = @"Percent\nOpen";
    lblOpen.textColor = [UIColor grayColor];
    lblOpen.numberOfLines = 2;
    lblOpen.font = fontLabels;
    lblOpen.textAlignment = NSTextAlignmentRight;
    [_viewPercentChart addSubview:lblOpen];
    
    // Days of month Label oin bottom center
    UILabel *lblDays = [[UILabel alloc]initWithFrame:CGRectMake((lbl2.frame.origin.x+lbl2.frame.size.width)/2, lbl2.frame.origin.y+2, 70, 14)];
    lblDays.text = @"Days of month";
    lblDays.textColor = [UIColor grayColor];
    lblDays.font = fontLabels;
    lblDays.textAlignment = NSTextAlignmentCenter;
    [_viewPercentChart addSubview:lblDays];
    
    // 0% label on left bottom
    UILabel *lblZero = [[UILabel alloc]initWithFrame:CGRectMake(0, 120, 37, 12)];
    lblZero.text = @"0%";
    lblZero.textColor = [UIColor grayColor];
    lblZero.font = fontPercent;
    lblZero.textAlignment = NSTextAlignmentRight;
    [_viewPercentChart addSubview:lblZero];
    
    [_viewPercentChart addSubview:viewOfChart];
    
    NSMutableArray *arrOfIndX = [[NSMutableArray alloc] init];
    NSMutableArray *arrOfIndY = [[NSMutableArray alloc] init];

    // pass static values to draw chart
    //    for (NSMutableArray *xyVal in arrOfIndustryData) {
    [arrOfIndX addObject:@"0"];
    [arrOfIndY addObject:@"50"];
    
    [arrOfIndX addObject:@"10"];
    [arrOfIndY addObject:@"20"];
    
    [arrOfIndX addObject:@"20"];
    [arrOfIndY addObject:@"80"];
    
    [arrOfIndX addObject:@"30"];
    [arrOfIndY addObject:@"30"];
    
    [arrOfIndX addObject:@"40"];
    [arrOfIndY addObject:@"50"];
    
    [arrOfIndX addObject:@"50"];
    [arrOfIndY addObject:@"20"];
    
    [arrOfIndX addObject:@"60"];
    [arrOfIndY addObject:@"5"];
    
    [arrOfIndX addObject:@"70"];
    [arrOfIndY addObject:@"30"];
    //    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [viewOfChart addSubview:[self chartwithXarray:arrOfIndX andYarray:arrOfIndY]];
    });

    
    ////// Room Temp Chart ///////
    viewOfTemperatureChart = [[DashedLineView alloc] initWithFrame:CGRectMake(40, 10, [UIScreen mainScreen].bounds.size.width - 60, 120)];
    viewOfTemperatureChart.backgroundColor = [UIColor clearColor];
    
    UILabel *lbl11 = [[UILabel alloc]initWithFrame:CGRectMake(40, 10, 2, 120)];
    UILabel *lbl12 = [[UILabel alloc]initWithFrame:CGRectMake(40, 130, self.view.frame.size.width-60, 2)];
    lbl11.backgroundColor = lbl12.backgroundColor = lightgray;
    [_viewTempChart addSubview:lbl11];
    [_viewTempChart addSubview:lbl12];
    
    UILabel *lblHundred1 = [[UILabel alloc]initWithFrame:CGRectMake(0, 10, 37, 12)];
    lblHundred1.text = @"100%";
    lblHundred1.textColor = [UIColor grayColor];
    lblHundred1.font = fontPercent;
    lblHundred1.textAlignment = NSTextAlignmentRight;
    [_viewTempChart addSubview:lblHundred1];
    
    UILabel *lblOpen1 = [[UILabel alloc]initWithFrame:CGRectMake(0, (lbl11.frame.origin.y+lbl11.frame.size.height)/2, 39, 22)];
    lblOpen1.text = @"Room\nTemp";
    lblOpen1.textColor = [UIColor grayColor];
    lblOpen1.numberOfLines = 2;
    lblOpen1.font = fontLabels;
    lblOpen1.textAlignment = NSTextAlignmentRight;
    [_viewTempChart addSubview:lblOpen1];
    
    UILabel *lblDays1 = [[UILabel alloc]initWithFrame:CGRectMake((lbl12.frame.origin.x+lbl12.frame.size.width)/2, lbl12.frame.origin.y+2, 70, 14)];
    lblDays1.text = @"Days of month";
    lblDays1.textColor = [UIColor grayColor];
    lblDays1.font = fontLabels;
    lblDays1.textAlignment = NSTextAlignmentCenter;
    [_viewTempChart addSubview:lblDays1];
    
    UILabel *lblZero1 = [[UILabel alloc]initWithFrame:CGRectMake(0, 120, 37, 12)];
    lblZero1.text = @"0%";
    lblZero1.textColor = [UIColor grayColor];
    lblZero1.font = fontPercent;
    lblZero1.textAlignment = NSTextAlignmentRight;
    [_viewTempChart addSubview:lblZero1];
    
    [_viewTempChart addSubview:viewOfTemperatureChart];
    
    NSMutableArray *arrTempX = [[NSMutableArray alloc] init];
    NSMutableArray *arrTempY = [[NSMutableArray alloc] init];
    
    //    for (NSMutableArray *xyVal in arrOfIndustryData) {
    [arrTempX addObject:@"0"];
    [arrTempY addObject:@"10"];
    
    [arrTempX addObject:@"10"];
    [arrTempY addObject:@"20"];
    
    [arrTempX addObject:@"20"];
    [arrTempY addObject:@"10"];
    
    [arrTempX addObject:@"30"];
    [arrTempY addObject:@"30"];
    
    [arrTempX addObject:@"40"];
    [arrTempY addObject:@"100"];
    
    [arrTempX addObject:@"50"];
    [arrTempY addObject:@"50"];
    
    [arrTempX addObject:@"60"];
    [arrTempY addObject:@"60"];
    
    [arrTempX addObject:@"70"];
    [arrTempY addObject:@"30"];
    
    //    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [viewOfTemperatureChart addSubview:[self chartwithXarray:arrTempX andYarray:arrTempY]];
    });
}

// Draw line  for chart
-(FSLineChart*)chartwithXarray:(NSMutableArray *)arrayX andYarray:(NSMutableArray *)arrayY
{
    FSLineChart* lineChart = [[FSLineChart alloc] initWithFrame:viewOfChart.bounds];
    lineChart.color = [UIColor darkGrayColor];
    lineChart.backgroundColor = [UIColor clearColor];
    lineChart.gridStep = (int)arrayX.count;
    
    lineChart.labelForIndex = ^(NSUInteger item) {
        return [NSString stringWithFormat:@""];
    };
    
    lineChart.labelForValue = ^(CGFloat value) {
        return [NSString stringWithFormat:@""];
    };
    
    [lineChart setChartData:arrayY];
    
    return lineChart;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
