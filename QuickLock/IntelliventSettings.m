//
//  IntelliventSettings.m
//  QuickLock
//
//  Created by PUNDSK001 on 04/01/16.
//  Copyright © 2016 Bge. All rights reserved.
//

#import "IntelliventSettings.h"
#import "FixedPosition.h"

@interface IntelliventSettings () <UITableViewDataSource, UITableViewDelegate, ModifyNameDelegate, UITextFieldDelegate>

// Properties
@property (weak, nonatomic) IBOutlet UITableView *tblIntellivent;
@property (weak, nonatomic) IBOutlet UILabel *labelFirmwareVersion;
@property (weak, nonatomic) IBOutlet UIView *viewBg;
@property (weak, nonatomic) IBOutlet UIView *viewPopupSetting;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (strong, nonatomic) IBOutlet UILabel *lblTitleSetting;
@property (strong, nonatomic) IBOutlet UILabel *lblTitleIntellivent;

@end

@implementation IntelliventSettings

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setExtraCellLineHidden:_tblIntellivent];
    [_tblIntellivent setSeparatorInset:UIEdgeInsetsZero];
    _tblIntellivent.separatorColor = [UIColor clearColor];
    
    _btnCancel.layer.masksToBounds = _viewPopupSetting.layer.masksToBounds = YES;
    _btnCancel.layer.cornerRadius = 8;
    _viewPopupSetting.layer.cornerRadius = 12;
    _viewPopupSetting.layer.borderColor = lightgray.CGColor;
    _viewPopupSetting.layer.borderWidth = 4;
    
    _viewBg.alpha = 0.0f;
    _viewBg.hidden = YES;
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    NSString *userName = [[NSUserDefaults standardUserDefaults] valueForKey:@"kUserName"];
    NSString *deviceName = quickLockDeviceManage.lockDevice.lock_Name;
    NSString *password = quickLockDeviceManage.lockDevice.lock_Password;
//    NSString *autoLockTime = [NSString stringWithFormat:@"%ds", quickLockDeviceManage.lockDevice.lock_AutoLockTime];
    NSString *firmwareVersion = quickLockDeviceManage.lockDevice.lock_FirmwareRevision;
    
    arrayImages = @[@"user-icon.png",
                    @"Intellivent-ico.png",
                    @"wifi.png",
                    @"setting.png",
                    @"history-log.png",
                    @"password-setting.png",
                    @"remove-dorlock.png"
                    ];
    arrayTitle = @[@"User Name : ",
                   @"Device Name : ",
                   @"WiFi / Z-Wave",
                   @"Settings",
                   @"History Log",
                   @"Password : ",
                   @"Remove : "];
    arraySubTitle = @[userName, deviceName, @"", @"", @"",password, deviceName];
    
    [_tblIntellivent reloadData];
    
    [_labelFirmwareVersion setText:[NSString stringWithFormat:@"Firmware version : %@",firmwareVersion]];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrayTitle.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cell";
    
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1
                                                   reuseIdentifier:cellIdentifier];
    UIImageView *img = [[UIImageView alloc]initWithFrame:CGRectMake(30, 9, 25, 25)];
    img.image = [UIImage imageNamed:arrayImages[indexPath.row]];
    [img setContentMode:UIViewContentModeScaleAspectFit];
    [cell.contentView addSubview:img];
    
    cell.backgroundColor = [UIColor clearColor];
    [cell.textLabel setText: [NSString stringWithFormat:@"\t\t%@%@",arrayTitle[indexPath.row],arraySubTitle[indexPath.row]]];
//    [cell.textLabel setText: [NSString stringWithFormat:@"\t\t%@",arrayTitle[indexPath.row]]];
    [cell.textLabel setTextColor:[UIColor blackColor]];
    
    DottedView *view = [[DottedView alloc] initWithFrame:CGRectMake(20, -1, self.view.frame.size.width-40, 15)];
    view.backgroundColor = [UIColor clearColor];
    [cell addSubview:view];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSLog(@"indexpath : %d",(int)indexPath.row);
    
    if (indexPath.row == 0)
    {
        // User Name
        inputUserName *InputUserName = [[inputUserName alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        NSString *strName = [[NSUserDefaults standardUserDefaults] valueForKey:@"kUserName"];
        [InputUserName setValue:strName forKeyPath:@"oldUserName"];
        [self.view addSubview:InputUserName];
    }
    if (indexPath.row == 1)
    {
        modifyNameView = [[ModifyName alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        modifyNameView.delegate = self;
        NSString *strName = [[NSUserDefaults standardUserDefaults] valueForKey:@"kUserName"];
        
        [modifyNameView setValue:strName forKeyPath:@"oldName"];
        [self.view addSubview:modifyNameView];
    }
    else if (indexPath.row == 2)
    {
        if (&UIApplicationOpenSettingsURLString != NULL) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"prefs:root=WIFI"]];
        }
        //open Wi-Fi Settings
//        if (&UIApplicationOpenSettingsURLString != NULL) {
//            NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
//            [[UIApplication sharedApplication] openURL:url];
//        }
//        else {
//            // Present some dialog telling the user to open the settings app.
//        }
    }
    else if (indexPath.row == 3)
    {
        // Settings
        _viewBg.hidden = _viewPopupSetting.hidden = NO;
        [UIView animateWithDuration:0.3 animations:^{
            _viewBg.alpha = 0.6;
            _viewPopupSetting.alpha = 1.0;
        }];
    }
    else if (indexPath.row == 4)
    {
        IntelliventHistoryLog *intelliventHistoryLog = [self.storyboard instantiateViewControllerWithIdentifier:@"IntelliventHistoryLog"];
        [self.navigationController pushViewController:intelliventHistoryLog animated:NO];
    }
    else if (indexPath.row == 5)
    {
        change = [self.storyboard instantiateViewControllerWithIdentifier:@"ChangePassword"];
        [self.navigationController pushViewController:change animated:NO];
    }
    else if (indexPath.row == 6)
    {
        // remove lock
        [self DeleteDevice];
    }
}

- (void)ReturnNewName:(NSString *)newName
{
    quickLockDeviceManage.lockDevice.lock_Name = newName;
    [modifyNameView removeFromSuperview];
    
    [dbAccess UpdateLockDevice:quickLockDeviceManage.lockDevice WithLockID:quickLockDeviceManage.lockDevice.lock_ID];
    [self viewDidAppear:YES];
}

- (void)DeleteDevice
{
    alertDeleteDevice = [[UIAlertView alloc] initWithTitle:@"Warning!"
                                                   message:@"You sure you want to delete it?"
                                                  delegate:self
                                         cancelButtonTitle:@"Cancel"
                                         otherButtonTitles:@"OK", nil];
    [alertDeleteDevice show];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [alertDeleteDevice dismissWithClickedButtonIndex:0 animated:NO];
    [alertModifyUserName dismissWithClickedButtonIndex:0 animated:NO];
}


// Limit the input box can only enter UserName Range allowed characters and a maximum length of 14
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if ([string isEqualToString:@"\n"] || [string isEqualToString:@""])
    {
        return YES;
    }
    else if (textField.text.length > 13)
    {
        return NO;
    }
    else
    {
        return [kUserNameRange rangeOfString:string].location != NSNotFound;
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView == alertDeleteDevice)
    {
        //DeleteDevice, Delete database information, take the initiative to disconnect
        [dbAccess DeleteLockDevice:quickLockDeviceManage.lockDevice.lock_ID];
        [quickLockDeviceManage initiativeDisConnect];
    }
}

- (IBAction)btnBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}

-(void)setExtraCellLineHidden: (UITableView *)tableView
{
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor clearColor];
    [tableView setTableFooterView:view];
}

- (IBAction)btnAllSettings:(UIButton *)sender
{
    [UIView animateWithDuration:0.3 animations:^{
        _viewBg.alpha = 0.0;
        _viewPopupSetting.alpha = 0.0;
    } completion:^(BOOL finished) {
        _viewBg.hidden = _viewPopupSetting.hidden = YES;
        
        if (sender.tag == 1)
        {
            // Fixed Position
            FixedPosition *fixedPosition = [self.storyboard instantiateViewControllerWithIdentifier:@"FixedPosition"];
            [self.navigationController pushViewController:fixedPosition animated:NO];
        }
        else if (sender.tag == 2)
        {
            //Time Based
            TimeBasedPosition *timeBasedPosition =[self.storyboard instantiateViewControllerWithIdentifier:@"TimeBasedPosition"];
            [self.navigationController pushViewController:timeBasedPosition animated:NO];
        }
        else if (sender.tag == 3)
        {
            // Temperature Based
            FixedTemperature *fixedTemperature =[self.storyboard instantiateViewControllerWithIdentifier:@"FixedTemperature"];
            [self.navigationController pushViewController:fixedTemperature animated:NO];
        }
        else if (sender.tag == 4)
        {
            // Cancel
        }
    }];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
