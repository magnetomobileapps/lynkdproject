//
//  CircleButtonDrawView.m
//  QuickLock
//
//  Created by Adam Hathaway on 12/3/15.
//  Copyright © 2015 Bge. All rights reserved.
//

#import "CircleButtonDrawView.h"
#import "STStyleKit.h"

@implementation CircleButtonDrawView

- (void)drawRect:(CGRect)rect {
    [STStyleKit drawCircleButtonWithFrame:rect fillColor:self.fillColor strokeColor:self.strokeColor];
}

@end
