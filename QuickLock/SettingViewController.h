//
//  SettingViewController.h
//  QuickLock
//
//  Created by administrator on 14-12-28.
//  Copyright (c) 2014年 Bge. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InputPassword.h"
#import "ModifyName.h"
#import "ModifyAutoLockTime.h"
#import "ShowHistoryLogViewController.h"
#import "ChangePassword.h"
#import "RFIDTags.h"
#import "ShareCode.h"
#import "Fingerprint.h"
#import "Alarm.h"
#import "Zwave.h"

@interface SettingViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, ModifyNameDelegate, InputPasswordDelegate, ModifyAutoLockTime, UIAlertViewDelegate, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource>
{
        IBOutlet UITableView *tableViewSet;
        
        IBOutlet UILabel *labelFirmwareVersion;
        int slidevalue, selectedIndexLED;
        NSArray *arrayTitle,*arrayImages,*arraySubTitle;
        ChangePassword *change ;
        UIAlertView *alertDeleteDevice;
        UIAlertView *alertModifyUserName;
    
}

@end
