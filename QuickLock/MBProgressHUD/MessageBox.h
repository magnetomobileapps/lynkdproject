//
//  MessageBox.h
//  QuickLock
//
//  Created by administrator on 15-3-9.
//  Copyright (c) 2015年 Bge. All rights reserved.
//

#import "MBProgressHUD.h"

@class MessageBox;
@protocol MessageBoxProtocol <NSObject>

- (void)cancelShowMessageBox:(MessageBox *)messageBox;

@end

@interface MessageBox : MBProgressHUD

@property (assign, nonatomic) id<MessageBoxProtocol> delegateMessageBox;

@end
