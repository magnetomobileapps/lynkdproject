//
//  MessageBox.m
//  QuickLock
//
//  Created by administrator on 15-3-9.
//  Copyright (c) 2015年 Bge. All rights reserved.
//

#import "MessageBox.h"

@implementation MessageBox

+ (id)showHUDAddedTo:(UIView *)view animated:(BOOL)animated
{
    return [super showHUDAddedTo:view animated:animated];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
    
    [self.delegateMessageBox cancelShowMessageBox:self];
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesCancelled:touches withEvent:event];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesMoved:touches withEvent:event];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesEnded:touches withEvent:event];
}

@end
