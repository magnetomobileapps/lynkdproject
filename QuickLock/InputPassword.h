//
//  InputPassword.h
//  QuickLock
//
//  Created by administrator on 15-3-12.
//  Copyright (c) 2015年 Bge. All rights reserved.
//

#import <UIKit/UIKit.h>

@class InputPassword;
@protocol InputPasswordDelegate <NSObject>

- (void)returnPassword:(NSString *)password;
- (void)cancelModifyPassword;

@end

@interface InputPassword : UIView <UITextFieldDelegate>
{
        UIView *viewBackground;
        
        UITextField *textFieldPassword;
        
        UIButton *buttonOK;
        UIButton *buttonCancel;
}

@property (nonatomic, assign) BOOL isModifyPassword;
@property (nonatomic, assign) id<InputPasswordDelegate> delegate;

- (void)RemoveAllView;

@end
