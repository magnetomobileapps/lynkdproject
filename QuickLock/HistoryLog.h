//
//  HistoryLog.h
//  QuickLock
//
//  Created by administrator on 15-3-17.
//  Copyright (c) 2015年 Bge. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, OperateLockType) {
    NFC = 0,
    Phone = 1,
};

@interface HistoryLog : NSObject

@property (nonatomic, assign) OperateLockType operateLockType;
@property (nonatomic, retain) NSString *operateLockDevice;
@property (nonatomic, retain) NSDate *operateLockTime;
@property (nonatomic, assign) BOOL isOpenLock;

// share code user based
@property (nonatomic, assign) int userCount;
@property (nonatomic, retain) NSString *shareCode;

// share code time based
@property (nonatomic, assign) NSDate *shareCodeStartTime;
@property (nonatomic, assign) NSDate *shareCodeEndTime;
@property (nonatomic, retain) NSString *shareCodeTimeBase;
@property (nonatomic, retain) NSString *shareCodeTime1;
@property (nonatomic, retain) NSString *shareCodeTime2;
@property (nonatomic, assign) int shareCodeTimeFlag;
@property (nonatomic, retain) NSString *strStartDat;
@property (nonatomic, retain) NSString *strEndDat;


@end
