//
//  QuickLockDevice.h
//  QuickLock
//
//  Created by administrator on 14-12-27.
//  Copyright (c) 2014年 Bge. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "BLEDevice_QuickLock.h"
#import "Protocol_BLEDeviceResult.h"
#import "Protocol_BLEDeviceResult_QuickLock.h"
#import "LockDevice.h"

@interface QuickLockDeviceManage : NSObject <Protocol_BLEDeviceResult, Protocol_BLEDeviceResult_QuickLock>
{
        NSTimer *timerSearchDevice;             //Home Search timers, two seconds and repeat.
        
        NSTimer *timerSearchWithUUIDTimeOut;    //MyLock interface search timer, one minute timeout.
        NSTimer *timerSearchWithUUID;           //MyLock search for a connection interface with timers, two seconds and repeat.
        
        // Device Instance
        BLEDevice_QuickLock *bleUnlock;
        
//        BOOL lockIsOpen;            //Added lock state of preservation, if the lock is opened again in open case, send the command will not be repeated.
}

// YES indicates that the connection has not even had the equipment, NO indicates that the connection of new devices.
@property (nonatomic, assign) BOOL isSearchConnected;

// Device database instance
@property (nonatomic, retain) LockDevice *lockDevice;

@property(nonatomic,assign) BOOL lockIsOpen;
// Added lock state of preservation, if the lock is opened again in open case, send the command will not be repeated.

- (void)startSearch;
- (void)startSearchWithUUID:(NSString *)uuid;
- (void)connectDeviceWithUUID:(NSString *)uuid;
- (void)stopSearch;
- (void)getLockState;
- (void)showMessageForBoxHistoryLog;
- (void)stopSearchForBoxHistoryLog;
// Control lock status, isOpen parameters indicate YES or NO to unlock a locked
- (void)openLock:(BOOL)isOpen;
- (void)verifyPassword;
- (void)changePasssword:(NSString *)newPassword;
- (void)setAutoLockTime:(int)time;
- (void)setLEDTime:(int)time;
- (void)setSureSet:(int)percent;
- (void)setUserNameToLock;
- (void)setKeyCode:(NSString *)keyCode;
- (void)initiativeDisConnect;
- (void)getHistoryLog;
- (void)getWifiMacAddress;
- (void)getPasswordSharecode;
- (void)getPasswordSharecodeTimeBase;
- (void)setClearAllFingerPrints;

-(void)getRFIDList;
-(void)getRFIDAdd;
-(void)getFingerprintLearn;
-(void)getCancelFingerprintLearn;

-(void)getZwave;

- (BOOL)getIsConnected;
- (void)getBatteryLevel;
-(void)getFingerprintCount;
-(void)getAlarmState;

- (void)deleteRFID;
-(void)addSharecode;
-(void)addSharecodeTimeBase;
-(void)setAlarm:(int)time delay:(int)delay;
- (void)setZwave:(int)param;

//-----------

-(void)setUserIDForRFIDCard_UserId:(NSString*)strUserId;
-(void)setRFIDWriter;
-(void)getAllRFID;

@end
