//
//  RFWriterViewController.h
//  QuickLock
//
//  Created by Pavan Jadhav on 06/09/17.
//  Copyright © 2017 Bge. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Prefer.h"
#import "SettingViewController.h"
#import "HamburgerDrawView.h"

@interface RFWriterViewController : UIViewController
{
    IBOutlet UILabel *labelName;
    
    IBOutlet UILabel *labelAPPVersion;
    
    IBOutlet UIButton *buttonAutoUnlock;
    
    IBOutlet UIButton *buttonUnlock;
    
    IBOutlet UIButton *buttonSet;
    BOOL isUnlockNotPassToggle, isSharecodeNotShown, isAutoLockPressed;
    int btnState;
}
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constantLogoBottom;

- (IBAction)buttonAutoUnlockTouch:(id)sender;
- (IBAction)buttonUnlockTouch:(id)sender;
- (IBAction)buttonSetTouch:(id)sender;

@end
