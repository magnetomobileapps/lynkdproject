//
//  Support.m
//  QuickLock
//
//  Created by Ankit on 26/12/15.
//  Copyright (c) 2015 Bge. All rights reserved.
//

#import "Support.h"

@interface Support () <SlideNavigationControllerDelegate>

// Properties
@property (weak, nonatomic) IBOutlet UILabel *lblUrlOne;
@property (weak, nonatomic) IBOutlet UILabel *lblUrlTwo;
@property (strong, nonatomic) IBOutlet UILabel *lblNeedHelp;

@end

@implementation Support

- (void)viewDidLoad
{
    [super viewDidLoad];

    // initialize drawer menu & button
    [SlideNavigationController sharedInstance].portraitSlideOffset = appDelegate.portraitDrawer;
    
    _lblNeedHelp.textColor = titleorange;
    _lblUrlOne.textColor = titleorange;
    _lblUrlTwo.textColor = titleorange;
    
    UIButton *btnMenu = [UIButton buttonWithType:UIButtonTypeCustom];
    btnMenu.frame = CGRectMake(10, 28, 35, 35);
    [btnMenu setImage:[UIImage imageNamed:@"navigation.png"] forState:UIControlStateNormal];
    [btnMenu addTarget:[SlideNavigationController sharedInstance] action:@selector(toggleLeftMenu) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnMenu];
    
}

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
}

// Navigate to browser by passing clicked link
- (IBAction)gestureUrlOne:(id)sender
{
    NSURL *url = [NSURL URLWithString:_lblUrlOne.text];
    [[UIApplication sharedApplication] openURL:url];
}

- (IBAction)gestureUrlTwo:(id)sender
{
    NSURL *url = [NSURL URLWithString:_lblUrlTwo.text];
    [[UIApplication sharedApplication] openURL:url];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
