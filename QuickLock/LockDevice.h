//
//  LockDevice.h
//  QuickLock
//
//  Created by 王洋 on 14/12/27.
//  Copyright (c) 2014年 Bge. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LockDevice : NSObject

// 锁id，主键
@property (nonatomic, assign) int lock_ID;

// 锁名称（用户可修改）
@property (nonatomic, retain) NSString *lock_Name;

// 锁类型（1：PadLock，2：DoorLock,3：DeadBolt）
@property (nonatomic, assign) int lock_Type;

// 解锁密码
@property (nonatomic, retain) NSString *lock_Password;

// 设备UUID
@property (nonatomic, retain) NSString *lock_UUID;

// 是否自动解锁
@property (nonatomic, assign) BOOL lock_AutoUnLock;

// 自动上锁时间(秒)
@property (nonatomic, assign) int lock_AutoLockTime;

// 固件版本号，不存数据库，只做显示用
@property (nonatomic, retain) NSString *lock_FirmwareRevision;

@property (nonatomic, assign) int lock_FingerprintCount;

// device Address
@property (nonatomic, retain) NSString *lock_Address;

// RFID Tags
@property (nonatomic, assign) int TAG_ID;

@property (nonatomic, retain) NSString *TAG_TYPE;

@property (nonatomic, retain) NSString *TAG_IMEI;

@property (nonatomic, retain) NSString *TAG_USERNAME;

@property (nonatomic, retain) NSString *TAG_DELETE_IMEI_OR_RFID;

@property (nonatomic, retain) NSString *strEnablePasscode;

// Sharecode
@property (nonatomic, retain) NSString *share_Code;
@property (nonatomic, retain) NSString *share_Code_Time;
@property (nonatomic, retain) NSString *share_Code_Count;
@property (nonatomic, assign) int share_Code_Start_Time_minutes;
@property (nonatomic, retain) NSDate *share_Code_Start_Time;
@property (nonatomic, assign) int share_Code_End_Time_minutes;
@property (nonatomic, retain) NSDate *share_Code_End_Time;
@property (nonatomic, assign) int share_Code_DATE_Time_FLAG;
@property (nonatomic, retain) NSString *share_Code_Time1;
@property (nonatomic, retain) NSString *share_Code_Time2;
@property (nonatomic, assign) int share_code_time_flag;
@property (nonatomic, retain) NSString *str_share_Code_Stert_time;
@property (nonatomic, retain) NSString *str_share_Code_End_time;

@property (nonatomic, assign) BOOL is_Login_share_Code;

@property (nonatomic, assign) BOOL isAlarm;
@property (nonatomic, assign) int alarmPrevoiusState;
@property (nonatomic, assign) int alarmDelay;

@property (nonatomic, assign) int zwaveState;

@property (nonatomic, assign) BOOL isLED;
@property (nonatomic, retain) NSString *currentLEDState;
@property (nonatomic, assign) int box_LED_Time;
@property (nonatomic, assign) int box_Alarm_Time;
@property (nonatomic, assign) int sure_Set_Percent;

@property (nonatomic, assign) int alarmType;
@property (nonatomic, assign) int delayTime;

@property (nonatomic, retain) NSString *passcode;
@property (nonatomic, assign) int passcode_enable;

@property (nonatomic, assign) int fingerPrintReturn;
@end
