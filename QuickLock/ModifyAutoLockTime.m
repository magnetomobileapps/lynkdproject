//
//  ModifyAutoLockTime.m
//  QuickLock
//
//  Created by administrator on 14-12-28.
//  Copyright (c) 2014年 Bge. All rights reserved.
//

#import "ModifyAutoLockTime.h"

@implementation ModifyAutoLockTime

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:.6f]];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(buttonCancelTouch:)];
        [self addGestureRecognizer:tap];
        
        UIView *viewPicker = [[UIView alloc]initWithFrame:CGRectMake((frame.size.width-250)/2,
                                                                     (frame.size.height - 216)/2,
                                                                     250,
                                                                     216)];
        selectedIndexLED = [quickLockDeviceManage.lockDevice.currentLEDState intValue];
        [viewPicker setBackgroundColor:[UIColor whiteColor]];
        [viewPicker.layer setCornerRadius:10];
        [viewPicker.layer setMasksToBounds:YES];
        [viewPicker.layer setBorderColor:bordergray.CGColor];
        [viewPicker.layer setBorderWidth:borderWidth4];
        [self addSubview:viewPicker];
        
        pickerTime = [[UIPickerView alloc] initWithFrame:CGRectMake(0 ,0 ,viewPicker.frame.size.width,162)];
        pickerTime.delegate = self;
        pickerTime.dataSource = self;
        pickerTime.backgroundColor = [UIColor clearColor];
        [viewPicker addSubview:pickerTime];
        
        UIButton *buttonCancel = [[UIButton alloc] initWithFrame:CGRectMake(133,
                                                                            pickerTime.frame.size.height+5,
                                                                            80,
                                                                            35)];
        [buttonCancel setTitle:@"Cancel" forState:UIControlStateNormal];
        [buttonCancel setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [buttonCancel.titleLabel setFont:[UIFont systemFontOfSize:15]];
        [buttonCancel setBackgroundColor:titleorange];
        [self SetButtonCornerRadius5AndWhiteBorder2:buttonCancel];
        [buttonCancel addTarget:self action:@selector(buttonCancelTouch:) forControlEvents:UIControlEventTouchUpInside];
        [viewPicker addSubview:buttonCancel];
        
        UIButton *buttonOK = [[UIButton alloc] initWithFrame:CGRectMake(36, pickerTime.frame.size.height+5, 80, 35)];
        [buttonOK setTitle:@"Ok" forState:UIControlStateNormal];
        [buttonOK setBackgroundColor:titleorange];
        [buttonOK.titleLabel setFont:[UIFont systemFontOfSize:15]];
        [buttonOK setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self SetButtonCornerRadius5AndWhiteBorder2:buttonOK];
        [buttonOK addTarget:self action:@selector(buttonOKTouch:) forControlEvents:UIControlEventTouchUpInside];
        [viewPicker addSubview:buttonOK];
        
        arrayDataSource = [NSMutableArray new];
        arrLED = [NSMutableArray new];
        arrAlarm = [NSMutableArray new];
        
        if (quickLockDeviceManage.lockDevice.isLED == 1)
        {
            for (int i = 30; i <= 90; i+=5)
            {
                [arrLED addObject:[NSString stringWithFormat:@"%d", i]];
            }
        }
        else if (quickLockDeviceManage.lockDevice.isAlarm == 1)
        {
            for (int i = 1; i <= 20; i++)
            {
                [arrAlarm addObject:[NSString stringWithFormat:@"%d", i]];
            }
        }
        else
        {
            for (int i = 3; i <= 15; i++)
            {
                [arrayDataSource addObject:[NSString stringWithFormat:@"%d", i]];
            }
        }
    }
    return self;
}

- (void)buttonCancelTouch:(id)sender
{
    quickLockDeviceManage.lockDevice.isLED = 0;
    [self removeFromSuperview];
}

- (void)SetButtonCornerRadius5AndWhiteBorder2:(UIButton *)button
{
    [button.layer setCornerRadius:5];
    [button.layer setMasksToBounds:YES];
}

- (void)buttonOKTouch:(id)sender
{
    if (quickLockDeviceManage.lockDevice.isLED == 1)
    {
        quickLockDeviceManage.lockDevice.isLED = 0;
        [self.delegate returnLEDTime:selectedIndexLED];
    }
    else if (quickLockDeviceManage.lockDevice.isAlarm == 1)
    {
        quickLockDeviceManage.lockDevice.isAlarm = 1;
        [self.delegate returnAlarmMinutes:selectedAlarm];
    }
    else
    {
        [self.delegate returnAutoLockTime:selectIndex];
    }
    [self removeFromSuperview];
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    [pickerTime reloadAllComponents];
    // Selected by default
    
    if(quickLockDeviceManage.lockDevice.isLED == 1)
    {
//        [pickerTime selectRow:0 inComponent:0 animated:NO];
        NSUInteger index = [arrLED indexOfObject:[quickLockDeviceManage.lockDevice.currentLEDState substringToIndex:[quickLockDeviceManage.lockDevice.currentLEDState length] - 1]];
        [pickerTime selectRow:index inComponent:0 animated:NO];
    }
    else if(quickLockDeviceManage.lockDevice.isAlarm == 1)
    {
        NSLog(@"%d",quickLockDeviceManage.lockDevice.alarmDelay);
        if (quickLockDeviceManage.lockDevice.alarmDelay == 0)
        {
            selectedAlarm = 1;
        }
        else
        {
            selectedAlarm = quickLockDeviceManage.lockDevice.alarmDelay;
        }
        [pickerTime selectRow:selectedAlarm - 1 inComponent:0 animated:NO];
    }
    else
    {
        [pickerTime selectRow:selectIndex - 3 inComponent:0 animated:NO];
    }
}

#pragma mark - UIPickerViewDelegate & UIPickerViewDataSource Method
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (quickLockDeviceManage.lockDevice.isLED == 1)
    {
        return arrLED.count;
    }
    if(quickLockDeviceManage.lockDevice.isAlarm == 1)
    {
        return arrAlarm.count;
    }
    return arrayDataSource.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (quickLockDeviceManage.lockDevice.isLED == 1)
    {
        return [NSString stringWithFormat:@"%@ Seconds",arrLED[row]];
    }
    else if(quickLockDeviceManage.lockDevice.isAlarm == 1)
    {
        return [NSString stringWithFormat:@"%@ Minutes",arrAlarm[row]];
    }
    return [NSString stringWithFormat:@"%@ Seconds",arrayDataSource[row]];
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 40;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if(quickLockDeviceManage.lockDevice.isLED == 1)
    {
        selectedIndexLED = ((int)row*5) + 30;
    }
    else if(quickLockDeviceManage.lockDevice.isAlarm == 1)
    {
        selectedAlarm = (int)row + 1;
    }
    else
    {
        selectIndex = (int)row + 3;
    }
}

@end
