//
//  ModifyName.m
//  QuickLock
//
//  Created by administrator on 14-12-28.
//  Copyright (c) 2014年 Bge. All rights reserved.
//

#import "ModifyName.h"

@interface ModifyName() <UITextFieldDelegate>
{
        UITextField *textFieldNewName;
}

@end

@implementation ModifyName

- (id)initWithFrame:(CGRect)frame
{
        self = [super initWithFrame:frame];
        if (self)
        {
                [self setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.6f]];
            
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(buttonCancelTouch:)];
            [self addGestureRecognizer:tap];
            
            // register for keyboard notifications
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
            
            
                UIView *viewBackground = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 280, 144)];
                [viewBackground setCenter:self.center];
                [viewBackground setBackgroundColor:[UIColor whiteColor]];
                [viewBackground.layer setCornerRadius:10];
                [viewBackground.layer setMasksToBounds:YES];
                [viewBackground.layer setBorderColor:bordergray.CGColor];
                [viewBackground.layer setBorderWidth:3];
                [self addSubview:viewBackground];
                
                UILabel *labelTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, viewBackground.frame.size.width, 30)];

            // Match Connected Device and set title as per device
            if(quickLockDeviceManage.lockDevice.lock_Type == PADLOCK)
                [labelTitle setText:@"Name Your Padlock"];
            else if(quickLockDeviceManage.lockDevice.lock_Type == DOORLOCK)
                [labelTitle setText:@"Name Your Doorlock"];
            else if(quickLockDeviceManage.lockDevice.lock_Type == GUNBOX)
                [labelTitle setText:@"Name Your Gunbox"];
            else if(quickLockDeviceManage.lockDevice.lock_Type == QUICKBOX)
                [labelTitle setText:@"Name Your Gunbox Echo"];
            else if(quickLockDeviceManage.lockDevice.lock_Type == MEDICINEBOX)
                [labelTitle setText:@"Name Your Medicinebox"];
            else if(quickLockDeviceManage.lockDevice.lock_Type == INTELLIVENT)
                [labelTitle setText:@"Name Your intellivent"];
            else if(quickLockDeviceManage.lockDevice.lock_Type == TALLGUNBOX)
                [labelTitle setText:@"Name Your GunBox SK-1"];
            
            
                [labelTitle setTextAlignment:NSTextAlignmentCenter];
                labelTitle.font = [UIFont boldSystemFontOfSize:16];
                [labelTitle setTextColor:orange];
                
                [viewBackground addSubview:labelTitle];
                
            textFieldNewName = [[UITextField alloc] initWithFrame:CGRectMake(10,
                                                                             50,
                                                                             viewBackground.frame.size.width - 20,
                                                                             35)];
                [textFieldNewName setBorderStyle:UITextBorderStyleRoundedRect];
                [textFieldNewName addTarget:self
                                     action:@selector(textDidEndOnExit:)
                           forControlEvents:UIControlEventEditingDidEndOnExit];
                [textFieldNewName becomeFirstResponder];
                [textFieldNewName setTextColor:[UIColor darkGrayColor]];
                textFieldNewName.font = [UIFont systemFontOfSize:fontSize13];
                textFieldNewName.layer.cornerRadius = cornerRadius5;
                textFieldNewName.layer.masksToBounds = YES;
                textFieldNewName.layer.borderWidth = borderWidth4;
                textFieldNewName.layer.borderColor = bordergray.CGColor;
                textFieldNewName.delegate = self;
                textFieldNewName.backgroundColor = lightgray;
                textFieldNewName.placeholder = @"Enter Name";
                textFieldNewName.returnKeyType = UIReturnKeyDone;
                [viewBackground addSubview:textFieldNewName];
                
            UIButton *buttonOK = [[UIButton alloc] initWithFrame:CGRectMake(40, 95, 90, 35)];
                [buttonOK setTitle:@"Save" forState:UIControlStateNormal];
                [buttonOK setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                [buttonOK.titleLabel setFont:[UIFont systemFontOfSize:15]];
                [buttonOK setBackgroundColor:titleorange];
                [self SetButtonCornerRadius5AndWhiteBorder2:buttonOK];
                [buttonOK addTarget:self action:@selector(buttonOKTouch:) forControlEvents:UIControlEventTouchUpInside];
                [viewBackground addSubview:buttonOK];
            
            
            UIButton *buttonCancel = [[UIButton alloc] initWithFrame:CGRectMake(150, 95, 90, 35)];
                [buttonCancel setTitle:@"Cancel" forState:UIControlStateNormal];
                [buttonCancel setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                [buttonCancel.titleLabel setFont:[UIFont systemFontOfSize:15]];
                [buttonCancel setBackgroundColor:titleorange];
                [self SetButtonCornerRadius5AndWhiteBorder2:buttonCancel];
                [buttonCancel addTarget:self
                                 action:@selector(buttonCancelTouch:)
                       forControlEvents:UIControlEventTouchUpInside];
                [viewBackground addSubview:buttonCancel];
        }
        return self;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([string isEqualToString:@"\n"] || [string isEqualToString:@""])
    {
        return YES;
    }
    else if (textField.text.length > 20)
    {
        return NO;
    }
    else
    {
        return [kUserNameRange rangeOfString:string].location != NSNotFound;
    }
}

- (void)keyboardWillShow:(NSNotification *)note {
    NSValue *value = [[note userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect frame = [value CGRectValue];
    
    [self adjustToKeyboardBounds:frame];
}

- (void)keyboardWillHide:(NSNotification *)note {
    [self adjustToKeyboardBounds:CGRectZero];
}

- (void)adjustToKeyboardBounds:(CGRect)bounds {
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    CGFloat height = CGRectGetHeight(screenBounds) - CGRectGetHeight(bounds);
    CGRect frame = self.frame;
    frame.origin.y = (height - CGRectGetHeight(self.bounds)) / 2.0;
    
    if (CGRectGetMinY(frame) < 0) {
        NSLog(@"warning: dialog is clipped, origin negative (%f)", CGRectGetMinY(frame));
    }
    
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
        self.frame = frame;
    } completion:nil];
}

- (void)SetButtonCornerRadius5AndWhiteBorder2:(UIButton *)button
{
        [button.layer setCornerRadius:5];
//        [button.layer setBorderWidth:2];
//        [button.layer setBorderColor:[UIColor whiteColor].CGColor];
        [button.layer setMasksToBounds:YES];
}

- (void)textDidEndOnExit:(id)sender
{
        [sender resignFirstResponder];
}

- (void)buttonOKTouch:(id)sender
{
    if (textFieldNewName.text.length <= 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning!"
                                                        message:@"input can't be null"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        return;
    }
    // The maximum name length limit of 20
    else if (textFieldNewName.text.length > 20)
    {
        textFieldNewName.text = [textFieldNewName.text substringToIndex:20];
    }
    [self.delegate ReturnNewName:textFieldNewName.text];
}

- (void)buttonCancelTouch:(id)sender
{
        [self removeFromSuperview];
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
        // Drawing code
        textFieldNewName.text = oldName;
}


@end
