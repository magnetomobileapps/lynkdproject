//
//  layerTheButton.m
//  QuickLock
//
//  Created by PUNDSK001 on 17/02/16.
//  Copyright © 2016 Bge. All rights reserved.
//

#import "layerTheButton.h"

@implementation layerTheButton


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    self.layer.masksToBounds = YES;
    self.layer.cornerRadius = 8;
}

@end
