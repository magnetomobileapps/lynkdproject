//
//  AppDelegate.m
//  QuickLock
//
//  Created by administrator on 14-12-26.
//  Copyright (c) 2014年 Bge. All rights reserved.
//

#import "AppDelegate.h"
#import "Prefer.h"
#import <CoreLocation/CoreLocation.h>

#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>

@interface AppDelegate ()<CLLocationManagerDelegate, UNUserNotificationCenterDelegate>
{
    CLLocationManager * locManger;
    NSString *latitude,*longitude;
}
@end

@implementation AppDelegate
@synthesize selectedMenu;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [Fabric with:@[[Crashlytics class]]];
    
    CGFloat SCREEN_WIDTH = ([[UIScreen mainScreen] bounds].size.width);
    if (SCREEN_WIDTH == 320)
        _portraitDrawer = 120;
    else if (SCREEN_WIDTH == 375)
        _portraitDrawer = 174;
    else if (SCREEN_WIDTH == 414)
        _portraitDrawer = 214;
    
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Iphone_5" bundle: nil];
    
    LeftMenuViewController *leftMenu = (LeftMenuViewController*)[mainStoryboard instantiateViewControllerWithIdentifier: @"LeftMenuViewController"];
    
    [SlideNavigationController sharedInstance].leftMenu = leftMenu;
    [[NSNotificationCenter defaultCenter] addObserverForName:SlideNavigationControllerDidClose object:nil queue:nil usingBlock:^(NSNotification *note) {
        NSString *menu = note.userInfo[@"menu"];
        NSLog(@"Closed %@", menu);
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"setDataOfSelectedLock" object:nil];
    }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:SlideNavigationControllerDidOpen object:nil queue:nil usingBlock:^(NSNotification *note) {
        NSString *menu = note.userInfo[@"menu"];
        NSLog(@"Opened %@", menu);
    }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:SlideNavigationControllerDidReveal object:nil queue:nil usingBlock:^(NSNotification *note) {
        NSString *menu = note.userInfo[@"menu"];
        NSLog(@"Revealed %@", menu);
    }];
    
    // The status bar bright
    // [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    
    // Device instance initialization
    if (!quickLockDeviceManage)
    {
        quickLockDeviceManage = [[QuickLockDeviceManage alloc] init];
    }
    
    // Open Database
    if (!dbAccess)
    {
        dbAccess = [[DBAccess alloc] init];
        [dbAccess CheckDatabase];
    }
    
    locManger = [[CLLocationManager alloc]init];
    locManger.delegate = self;
    locManger.distanceFilter = kCLDistanceFilterNone; //whenever we move
    locManger.desiredAccuracy = kCLLocationAccuracyBest;
    [locManger startUpdatingLocation];
    [locManger requestWhenInUseAuthorization];
    
    // Push Notifications
    application.applicationIconBadgeNumber = 0;
    if( SYSTEM_VERSION_LESS_THAN( @"10.0" ) )
    {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound |    UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else
    {
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error)
         {
             if( !error )
             {
                 [[UIApplication sharedApplication] registerForRemoteNotifications];  // required to get the app to do anything at all about push notifications
                 NSLog( @"Push registration success." );
             }
             else
             {
                 NSLog( @"Push registration FAILED" );
                 NSLog( @"ERROR: %@ - %@", error.localizedFailureReason, error.localizedDescription );
                 NSLog( @"SUGGESTIONS: %@ - %@", error.localizedRecoveryOptions, error.localizedRecoverySuggestion );
             }
         }];
    }
    
    return YES;
}

#pragma mark Notification Delegate

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    NSLog(@"My token is: %@", deviceToken);
    NSString *strToken = [NSString stringWithFormat:@"%@",deviceToken];
    strToken = [strToken stringByReplacingOccurrencesOfString:@" " withString:@""];
    strToken = [strToken substringFromIndex:1];
    strToken = [strToken substringToIndex:strToken.length - 1];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:strToken forKey:@"deviceToken"];
    [defaults synchronize];
    _strDeviceToken = strToken;
    NSLog(@"Device Tokedn %@",_strDeviceToken);
}

-(void) application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void
                                                                                                                               (^)(UIBackgroundFetchResult))completionHandler
{
    // iOS 10 will handle notifications through other methods
    
    if( SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO( @"10.0" ) )
    {
        NSLog( @"iOS version >= 10. Let NotificationCenter handle this one." );
        // set a member variable to tell the new delegate that this is background
        return;
    }
    NSLog( @"HANDLE PUSH, didReceiveRemoteNotification: %@", userInfo );
    
    // custom code to handle notification content
    
    if( [UIApplication sharedApplication].applicationState == UIApplicationStateInactive )
    {
        NSLog( @"INACTIVE" );
        completionHandler( UIBackgroundFetchResultNewData );
    }
    else if( [UIApplication sharedApplication].applicationState == UIApplicationStateBackground )
    {
        NSLog( @"BACKGROUND" );
        completionHandler( UIBackgroundFetchResultNewData );
    }
    else
    {
        NSLog( @"FOREGROUND" );
        completionHandler( UIBackgroundFetchResultNewData );
    }
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler
{
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"RomoteNotification"
     object:self];
    
    NSLog( @"Handle push from foreground" );
    // custom code to handle push while app is in the foreground
    NSLog(@"%@", notification.request.content.userInfo);
    completionHandler(UNNotificationPresentationOptionAlert);
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center
didReceiveNotificationResponse:(UNNotificationResponse *)response
         withCompletionHandler:(void (^)())completionHandler
{
    NSLog( @"Handle push from background or closed" );
    // if you set a member variable in didReceiveRemoteNotification, you  will know if this is from closed or background
    NSLog(@"%@", response.notification.request.content.userInfo);
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error{
    NSLog(@"Failed to get token, error: %@", error);
}

#pragma mark- CLLocationManagerDelegate Method


-(void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    CLLocationCoordinate2D loc = [newLocation coordinate];
    latitude = [NSString stringWithFormat:@"%.8lf",loc.latitude];
    longitude = [NSString stringWithFormat:@"%.8lf",loc.longitude];
    NSLog(@"latitude => %@ || longitude => %@",latitude,longitude);
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:latitude forKey:@"latitude"];
    [defaults setValue:longitude forKey:@"longitude"];
    [defaults synchronize];
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
}

- (void)applicationWillResignActive:(UIApplication *)application
{
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
        // TODO modify because the connection, you need to determine is which way to re-connect
        // Re-enter the front desk to start the search device
    
    if ([quickLockDeviceManage valueForKeyPath:@"timerSearchWithUUIDTimeOut"])
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"kRemoveMBProgressHUD" object:nil];
    }
    else
    {
        [quickLockDeviceManage startSearch];
    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
