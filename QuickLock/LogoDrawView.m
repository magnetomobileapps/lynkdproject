//
//  LogoDrawView.m
//  QuickLock
//
//  Created by Adam Hathaway on 12/3/15.
//  Copyright © 2015 Bge. All rights reserved.
//

#import "LogoDrawView.h"
#import "STStyleKit.h"

@implementation LogoDrawView

- (void)drawRect:(CGRect)rect {
    [STStyleKit drawLogoWithFrame:rect];
}

@end
