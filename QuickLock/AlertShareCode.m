//
//  AlertShareCode.m
//  QuickLock
//
//  Created by PUNDSK001 on 15/06/16.
//  Copyright © 2016 Bge. All rights reserved.
//

#import "AlertShareCode.h"

@interface AlertShareCode () <UITextFieldDelegate>
{
    IBOutlet UILabel *lblTitlePushShareCode;
    UIToolbar* keyboardToolbar;
    UIBarButtonItem *btnFlexible, *btnDone;
    NSDateFormatter *format;
    NSString *strDate;
    BOOL isDateSelect, isTimeFromSelected;
    UIView *ViewOfDatepicker, *_viewDatePickerBack;
    UIDatePicker *_datePicker;
    UIButton *btnSetDate, *btnDateCancel;

}
@property (strong, nonatomic) IBOutlet UITextField *txtShareCode;
@property (strong, nonatomic) IBOutlet UILabel *lblDeviceName;
@property (strong, nonatomic) IBOutlet UITextField *txtSelectDate;
@property (strong, nonatomic) IBOutlet UITextField *txtTime1;
@property (strong, nonatomic) IBOutlet UITextField *txtTime2;
@end

@implementation AlertShareCode

- (void)viewDidLoad
{
    [super viewDidLoad];
  
    _viewDatePickerBack = [[UIView alloc]initWithFrame:[UIScreen mainScreen].bounds];
    _viewDatePickerBack.hidden = YES;
    _viewDatePickerBack.alpha = 0;
    _viewDatePickerBack.backgroundColor = [UIColor blackColor];
    [self.view addSubview:_viewDatePickerBack];
    
    [self createInputAccessoryView];
    format = [[NSDateFormatter alloc]init];
    
    _txtShareCode.backgroundColor = _txtSelectDate.backgroundColor = _txtTime1.backgroundColor = _txtTime2.backgroundColor = lightgray;
}

-(void)viewWillAppear:(BOOL)animated
{
    _lblDeviceName.text = appDelegate.strSelectedAlertDevice;
}

-(void)viewDidAppear:(BOOL)animated
{
    // Create Dynamic date picker to set date
    ViewOfDatepicker = [[UIView alloc]initWithFrame:CGRectMake((self.view.frame.size.width-301)/2, (self.view.frame.size.height-210)/2, 301, 210)];
    ViewOfDatepicker.backgroundColor = [UIColor whiteColor];
    ViewOfDatepicker.hidden = YES;
    ViewOfDatepicker.alpha = 0.0;
    ViewOfDatepicker.layer.masksToBounds = YES;
    ViewOfDatepicker.layer.borderWidth = 4;
    ViewOfDatepicker.layer.borderColor = lightgray.CGColor;
    ViewOfDatepicker.layer.cornerRadius = 8;
    [self.view addSubview:ViewOfDatepicker];
    
    _datePicker = [[UIDatePicker alloc]initWithFrame:CGRectMake(0, 0, 301, 162)];
    _datePicker.datePickerMode = UIDatePickerModeDate;
    _datePicker.backgroundColor = [UIColor clearColor];
    [ViewOfDatepicker addSubview:_datePicker];
    
    // Defined btnDateDone: method to set date
    btnSetDate = [UIButton buttonWithType:UIButtonTypeSystem];
    [btnSetDate addTarget:self action:@selector(btnDateDone:) forControlEvents:UIControlEventTouchUpInside];
    btnSetDate.frame = CGRectMake(8, 167, 138, 35);
    [btnSetDate setTitle:@"Set" forState:UIControlStateNormal];
    [btnSetDate setTintColor:[UIColor whiteColor]];
    btnSetDate.backgroundColor = darkgray;
    btnSetDate.layer.masksToBounds = YES;
    btnSetDate.layer.cornerRadius = 4;
    [ViewOfDatepicker addSubview:btnSetDate];
    
    // Defined btnCancelDate: method to cancel date set
    btnDateCancel = [UIButton buttonWithType:UIButtonTypeSystem];
    [btnDateCancel addTarget:self action:@selector(btnCancelDate:) forControlEvents:UIControlEventTouchUpInside];
    btnDateCancel.frame = CGRectMake(155, 167, 138, 35);
    [btnDateCancel setTitle:@"Cancel" forState:UIControlStateNormal];
    [btnDateCancel setTintColor:[UIColor whiteColor]];
    btnDateCancel.backgroundColor = darkgray;
    btnDateCancel.layer.masksToBounds = YES;
    btnDateCancel.layer.cornerRadius = 4;
    [ViewOfDatepicker addSubview:btnDateCancel];
}

-(void)createInputAccessoryView
{
    keyboardToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 44)];
    keyboardToolbar.tintColor = [UIColor darkGrayColor];
    
    btnFlexible = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    btnDone = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneTyping)];
    
    NSArray *arr = @[btnFlexible,btnDone];
    [keyboardToolbar setItems:arr];
    
    [self.txtShareCode setInputAccessoryView:keyboardToolbar];
}
-(void)doneTyping
{
    [_txtShareCode resignFirstResponder];
}

- (IBAction)btnBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}

- (IBAction)btnSave:(id)sender
{
    if (_txtShareCode.text.length == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning!"
                                                        message:@"Enter Share Code"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
    else if (_txtShareCode.text.length < 8)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning!"
                                                        message:@"Share Code must be 8 digital"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        
    }
}

- (IBAction)btnSetDate:(id)sender
{
    [_datePicker setDatePickerMode:UIDatePickerModeDate];
    [format setDateFormat:@"dd-MM-yyyy"];
    isDateSelect = YES;
    
    ViewOfDatepicker.hidden = NO;
    _viewDatePickerBack.hidden = NO;
    [UIView animateWithDuration:0.3f animations:^{
        ViewOfDatepicker.alpha = 1.0f;
        _viewDatePickerBack.alpha = 0.5f;
        _viewDatePickerBack.backgroundColor = [UIColor blackColor];
    } completion:^(BOOL finished) {
        
    }];
}

// set Time from which you want to assign ShareCode
- (IBAction)btnTimeFrom:(id)sender
{
    NSLocale *locale = [[NSLocale alloc]initWithLocaleIdentifier:@"NL"];
    [_datePicker setLocale:locale];
    [_datePicker setDatePickerMode:UIDatePickerModeTime];
    [format setDateFormat:@"HH:mm"];
    isDateSelect = NO;
    isTimeFromSelected = YES;
    
    ViewOfDatepicker.hidden = NO;
    _viewDatePickerBack.hidden = NO;
    [UIView animateWithDuration:0.3f animations:^{
        ViewOfDatepicker.alpha = 1.0f;
        _viewDatePickerBack.alpha = 0.5f;
        _viewDatePickerBack.backgroundColor = [UIColor blackColor];
    } completion:^(BOOL finished) {
        
    }];
}

// set Time up to which you want to assign ShareCode
- (IBAction)btnTo:(id)sender
{
    NSLocale *locale = [[NSLocale alloc]initWithLocaleIdentifier:@"NL"];
    [_datePicker setLocale:locale];
    [_datePicker setDatePickerMode:UIDatePickerModeTime];
    [format setDateFormat:@"HH:mm"];
    isDateSelect = NO;
    isTimeFromSelected = NO;
    ViewOfDatepicker.hidden = NO;
    _viewDatePickerBack.hidden = NO;
    [UIView animateWithDuration:0.3f animations:^{
        ViewOfDatepicker.alpha = 1.0f;
        _viewDatePickerBack.alpha = 0.5f;
        _viewDatePickerBack.backgroundColor = [UIColor blackColor];
    } completion:^(BOOL finished) {
        
    }];
}

// assign date on textfield and hide datePicker
- (IBAction)btnDateDone:(id)sender
{
    strDate = [format stringFromDate:[_datePicker date]];
    
    if (isDateSelect == YES)
    {
        _txtSelectDate.text = strDate;
    }
    else
    {
        if (isTimeFromSelected == YES)
        {
            _txtTime1.text = strDate;
        }
        else
        {
            _txtTime2.text = strDate;
        }
    }
    
    [UIView animateWithDuration:0.3f animations:^{
        ViewOfDatepicker.alpha = 0.0f;
        _viewDatePickerBack.alpha = 0.0f;
    } completion:^(BOOL finished) {
        ViewOfDatepicker.hidden = YES;
    }];
}

// Cancel date set
- (IBAction)btnCancelDate:(id)sender
{
    [UIView animateWithDuration:0.3f animations:^{
        ViewOfDatepicker.alpha = 0.0f;
        _viewDatePickerBack.alpha = 0.0f;
    } completion:^(BOOL finished) {
        ViewOfDatepicker.hidden = YES;
        _viewDatePickerBack.hidden = YES;
    }];
}

// Limit the input box can only enter a number, and a maximum length of 8
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([string isEqualToString:@"\n"] || [string isEqualToString:@""])
    {
        return YES;
    }
    else if (textField.text.length > 7)
    {
        return NO;
    }
    else
    {
        return [kDigitalRange rangeOfString:string].location != NSNotFound;
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
