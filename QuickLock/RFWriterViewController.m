//
//  RFWriterViewController.m
//  QuickLock
//
//  Created by Pavan Jadhav on 06/09/17.
//  Copyright © 2017 Bge. All rights reserved.
//

#import "RFWriterViewController.h"
#import "UIView+Toast.h"

@interface RFWriterViewController () <SlideNavigationControllerDelegate,UITextFieldDelegate>
{
    SettingViewController *settingVC;
    UIToolbar* keyboardToolbar;
    UIBarButtonItem *btnFlexible, *btnDone;
    
    __weak IBOutlet UIView *viewOfWriteRFTag;
    
    __weak IBOutlet UIButton *btnWriteRFTag;
    __weak IBOutlet MyTextField *txtWriterUserId;
    __weak IBOutlet UILabel *lblWriterInfo;
}
@property (weak, nonatomic) IBOutlet UIImageView *imgGunbox;
@property (weak, nonatomic) IBOutlet UILabel *lblAutoUnlock;
@property (weak, nonatomic) IBOutlet UIButton *btnSetting;
@property (weak, nonatomic) IBOutlet UILabel *labelBattery;
@property (weak, nonatomic) IBOutlet UILabel *lblPasswordEnabmed;
@property (weak, nonatomic) IBOutlet UIButton *btnPasswordEnabmed;
@property (weak, nonatomic) IBOutlet UIView *bgViewOfPasscode;
@property (weak, nonatomic) IBOutlet UIView *viewPasscode;
@property (weak, nonatomic) IBOutlet UILabel *lblPasscodeTitle;
@property (weak, nonatomic) IBOutlet UITextField *txtPasscode;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;

@end

@implementation RFWriterViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:@"isDeviceConnected"];
    [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:@"enteredInView"];
    
    //    _lblPasswordEnabmed.hidden = _btnPasswordEnabmed.hidden = YES;
    
    //    labelName.textColor = titleorange;
    
    [self createInputAccessoryView];
    viewOfWriteRFTag.hidden = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(cancelPass)];
    [_bgViewOfPasscode addGestureRecognizer:tap];
    
    //    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    //    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    _viewPasscode.layer.masksToBounds = YES;
    _viewPasscode.layer.borderColor = darkgray.CGColor;
    _viewPasscode.layer.borderWidth = 3;
    _viewPasscode.layer.cornerRadius = 6;
    
    
    viewOfWriteRFTag.layer.masksToBounds = YES;
    viewOfWriteRFTag.layer.borderColor = darkgray.CGColor;
    viewOfWriteRFTag.layer.borderWidth = 3;
    viewOfWriteRFTag.layer.cornerRadius = 6;
    
    btnState = quickLockDeviceManage.lockDevice.passcode_enable;
    (btnState == 0) ? [_btnPasswordEnabmed setBackgroundImage:[UIImage imageNamed:@"gray-switch-icon.png"] forState:UIControlStateNormal] : [_btnPasswordEnabmed setBackgroundImage:[UIImage imageNamed:@"orange-switch-icon.png"] forState:UIControlStateNormal];
    
    [SlideNavigationController sharedInstance].portraitSlideOffset = appDelegate.portraitDrawer;
    UIButton *btnMenu = [UIButton buttonWithType:UIButtonTypeCustom];
    btnMenu.frame = CGRectMake(10, 28, 35, 35);
    [btnMenu setImage:[UIImage imageNamed:@"navigation.png"] forState:UIControlStateNormal];
    [btnMenu addTarget:[SlideNavigationController sharedInstance] action:@selector(toggleLeftMenu) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnMenu];
    _txtPasscode.backgroundColor = lightgray;
    
    if (screenSize.height == 480)
    {
        _constantLogoBottom.constant = 20;
    }
    
    [self ShowAPPVersion];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(initButtonState:)
                                                 name:@"kLockState"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showBattery:)
                                                 name:@"kBattery"
                                               object:nil];
    
    // hamburgerDrawView
    HamburgerDrawView* hamburgerDrawView = [[HamburgerDrawView alloc] init];
    hamburgerDrawView.fillColor = [UIColor colorWithRed:128/256.0 green:130/256.0 blue:133/256.0 alpha:1.0]; // #808285
    hamburgerDrawView.opaque = NO;
    hamburgerDrawView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:hamburgerDrawView];
    
    isSharecodeNotShown = YES;
    
    // logoWrapper
    UIView* logoWrapper = [[UIView alloc] init];
    logoWrapper.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:logoWrapper];
    
    // logoDrawView
    LogoDrawView* logoDrawView = [[LogoDrawView alloc] init];
    logoDrawView.opaque = NO;
    logoDrawView.translatesAutoresizingMaskIntoConstraints = NO;
    [logoWrapper addSubview:logoDrawView];
    
    buttonUnlock.layer.borderWidth = 3;
    buttonUnlock.layer.masksToBounds = YES;
    buttonUnlock.layer.borderColor = darkgray.CGColor;
    buttonUnlock.layer.cornerRadius = 10.0;
    
    if ([[UIScreen mainScreen]bounds].size.height == 480)
    {
        _constantLogoBottom.constant = 20;
    }
    
    if (quickLockDeviceManage.lockDevice.is_Login_share_Code == 1)
    {
        _btnSetting.hidden = YES;
    }
}

-(void)createInputAccessoryView
{
    keyboardToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 44)];
    keyboardToolbar.tintColor = [UIColor darkGrayColor];
    keyboardToolbar.backgroundColor = [UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:1.0];
    
    btnFlexible = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    btnDone = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneTyping)];
    
    NSArray *arr = @[btnFlexible,btnDone];
    [keyboardToolbar setItems:arr];
    
    [self.txtPasscode setInputAccessoryView:keyboardToolbar];
}
-(void)doneTyping
{
    [UIView animateWithDuration:0.3f animations:^{
        _viewPasscode.transform = CGAffineTransformMakeTranslation(0, 0);
    }];
    [_txtPasscode resignFirstResponder];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    //if (screenSize.height <= 568)
    {
        [UIView animateWithDuration:0.3f animations:^{
            _viewPasscode.transform = CGAffineTransformMakeTranslation(0, -80);
        }];
    }
}

- (void)keyboardWillShow:(NSNotification *)note
{
    NSValue *value = [[note userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect frame = [value CGRectValue];
    
    [self adjustToKeyboardBounds:frame];
}

- (void)keyboardWillHide:(NSNotification *)note {
    [self adjustToKeyboardBounds:CGRectZero];
}

- (void)adjustToKeyboardBounds:(CGRect)bounds {
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    CGFloat height = CGRectGetHeight(screenBounds) - CGRectGetHeight(bounds);
    CGRect frame = self.view.frame;
    frame.origin.y = (height - CGRectGetHeight(self.view.bounds)) / 2.0;
    
    if (CGRectGetMinY(frame) < 0) {
        NSLog(@"warning: dialog is clipped, origin negative (%f)", CGRectGetMinY(frame));
    }
    
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
        self.view.frame = frame;
    } completion:nil];
}

-(void)cancelPass
{
    txtWriterUserId.text = @"";
    [txtWriterUserId resignFirstResponder];
    _txtPasscode.text = @"";
    [_txtPasscode resignFirstResponder];
    [UIView animateWithDuration:0.3 animations:^{
        _viewPasscode.alpha = _bgViewOfPasscode.alpha = viewOfWriteRFTag.alpha =  0;
        _viewPasscode.transform = viewOfWriteRFTag.transform = CGAffineTransformMakeTranslation(0, 0);
    } completion:^(BOOL finished) {
        _viewPasscode.hidden = _bgViewOfPasscode.hidden = viewOfWriteRFTag.hidden = YES;
    }];
    isSharecodeNotShown = YES;
}

- (IBAction)btnWriteRFTag:(id)sender {
    NSString *username = txtWriterUserId.text;
    if (username.length > 0)
    {
        [quickLockDeviceManage setUserIDForRFIDCard_UserId:txtWriterUserId.text];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [quickLockDeviceManage setRFIDWriter];
        });
        
        [self cancelPass];
    }
    else
    {
        [self.view makeToast:@"Enter UserId" duration:1.0 position:@"center"];
    }
    
}

- (IBAction)btnPasscodeSave:(UIButton *)sender
{
    BOOL canViewHide = NO;
    // save
    if (sender.tag == 0)
    {
        if (_txtPasscode.text.length == 0)
        {
            [self.view makeToast:@"Enter password" duration:1.0 position:@"center"];
            canViewHide = NO;
        }
        else
        {
            if (isUnlockNotPassToggle == 1)
            {
                // Unlock button pressed. so compare entered passcode with database passcode
                if ([quickLockDeviceManage.lockDevice.passcode isEqualToString:_txtPasscode.text])
                {
                    [quickLockDeviceManage openLock:YES];
                    [buttonUnlock setBackgroundColor:[UIColor colorWithRed:15.0f/255.0f green:157.0f/255.0f blue:88.0f/255.0f alpha:1.0f]];
                    [buttonUnlock setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                    buttonUnlock.layer.borderColor = [UIColor colorWithRed:2.0f/255.0f green:112.0f/255.0f blue:62.0f/255.0f alpha:1.0f].CGColor;
                    
                    [_txtPasscode resignFirstResponder];
                    _txtPasscode.text = @"";
                    canViewHide = YES;
                    
                    
                    if (isAutoLockPressed)
                    {
                        quickLockDeviceManage.lockDevice.lock_AutoUnLock = YES;
                        [buttonAutoUnlock setBackgroundImage:[UIImage imageNamed:@"orange-switch-icon.png"] forState:UIControlStateNormal];
                        // If AutoUnlock open, set the automatic lock time
                        [quickLockDeviceManage openLock:YES];
                        [quickLockDeviceManage setAutoLockTime:5];
                    }
                }
                else
                {
                    // not equal
                    [self.view makeToast:@"Wrong password" duration:1.0 position:@"center"];
                    _txtPasscode.text = @"";
                    canViewHide = NO;
                }
            }
            else
            {
                // passcode toggle pressed so input new password or input passcode to disable security
                if (btnState == 1)
                {
                    [_btnPasswordEnabmed setBackgroundImage:[UIImage imageNamed:@"orange-switch-icon.png"] forState:UIControlStateNormal];
                    
                    quickLockDeviceManage.lockDevice.passcode_enable = btnState;
                    quickLockDeviceManage.lockDevice.passcode = _txtPasscode.text;
                    
                    [dbAccess UpdatePasscode:quickLockDeviceManage.lockDevice lock_UUID:quickLockDeviceManage.lockDevice.lock_UUID];
                    [dbAccess GetAllLockDevice];
                    
                    canViewHide = YES;
                    _txtPasscode.text = @"";
                    [_txtPasscode resignFirstResponder];
                }
                else
                {
                    if ([quickLockDeviceManage.lockDevice.passcode isEqualToString:_txtPasscode.text])
                    {
                        [_btnPasswordEnabmed setBackgroundImage:[UIImage imageNamed:@"gray-switch-icon.png"] forState:UIControlStateNormal];
                        
                        quickLockDeviceManage.lockDevice.passcode_enable = btnState;
                        quickLockDeviceManage.lockDevice.passcode = _txtPasscode.text;
                        
                        [dbAccess UpdatePasscode:quickLockDeviceManage.lockDevice lock_UUID:quickLockDeviceManage.lockDevice.lock_UUID];
                        [dbAccess GetAllLockDevice];
                        
                        canViewHide = YES;
                        _txtPasscode.text = @"";
                        [_txtPasscode resignFirstResponder];
                    }
                    else
                    {
                        [self.view makeToast:@"Wrong password" duration:1.0 position:@"center"];
                        _txtPasscode.text = @"";
                        canViewHide = NO;
                    }
                }
                // Set automatic locking time
                [quickLockDeviceManage setAutoLockTime:quickLockDeviceManage.lockDevice.lock_AutoLockTime];
            }
        }
    }
    else // cancel
    {
        _txtPasscode.text = @"";
        [_txtPasscode resignFirstResponder];
        canViewHide = YES;
    }
    
    if (canViewHide)
    {
        [UIView animateWithDuration:0.3 animations:^{
            _viewPasscode.alpha = _bgViewOfPasscode.alpha = 0;
            _viewPasscode.transform = CGAffineTransformMakeTranslation(0, 0);
        } completion:^(BOOL finished) {
            _viewPasscode.hidden = _bgViewOfPasscode.hidden = YES;
        }];
    }
    isSharecodeNotShown = YES;
}

- (IBAction)btnPasswordEnabmed:(id)sender
{
    isSharecodeNotShown = NO;
    
    if (btnState == 0)
    {
        btnState = 1;
        
        _bgViewOfPasscode.hidden = _viewPasscode.hidden = NO;
        _lblPasscodeTitle.text = @"Set 4 digit password to enable";
        _txtPasscode.placeholder = @"Set password";
        [_btnSave setTitle:@"Save" forState:UIControlStateNormal];
        [UIView animateWithDuration:0.3 animations:^{
            _bgViewOfPasscode.alpha = 0.6;
            _viewPasscode.alpha = 1;
            [_txtPasscode becomeFirstResponder];
        }];
    }
    else
    {
        btnState = 0;
        isUnlockNotPassToggle = 0;
        
        _bgViewOfPasscode.hidden = _viewPasscode.hidden =  NO;
        _lblPasscodeTitle.text = @"Enter 4 digit password to disable";
        _txtPasscode.placeholder = @"Enter password";
        [_btnSave setTitle:@"Ok" forState:UIControlStateNormal];
        [UIView animateWithDuration:0.3 animations:^{
            _bgViewOfPasscode.alpha = 0.6;
            _viewPasscode.alpha = 1;
            [_txtPasscode becomeFirstResponder];
        }];
    }
}

- (void)ShowAPPVersion
{
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *APPVersion =[NSString stringWithFormat:@"%@.%@",
                           [infoDictionary objectForKey:@"CFBundleShortVersionString"],
                           [infoDictionary objectForKey:@"CFBundleVersion"]];
    [labelAPPVersion setText:[NSString stringWithFormat:@"APP Version : %@", APPVersion]];
}

- (void)initButtonState:(NSNotification *)notification
{
    BOOL isLockOpen = [[notification object] boolValue];
    
    [buttonSet setUserInteractionEnabled:!isLockOpen];
}

- (void)showBattery:(NSNotification *)notification
{
    int batteryValue = [[notification object] intValue];
    [_labelBattery setText:[NSString stringWithFormat:@"Battery : %d%%", batteryValue]];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden = YES;
}

// It will slide screen to go to left menu
- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return isSharecodeNotShown;
}

-(void)hideAutoLock
{
    _lblAutoUnlock.hidden = YES;
    buttonAutoUnlock.hidden = YES;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // get Share code User based
    //    [quickLockDeviceManage getPasswordSharecode];
    // get Share code Time based
    //    [quickLockDeviceManage getPasswordSharecodeTimeBase];
    
    if (quickLockDeviceManage.lockDevice.lock_Type == PADLOCK)
    {
        // default Padlock
    }
    if (quickLockDeviceManage.lockDevice.lock_Type == GUNBOX)
    {
        labelName.text = @"Gunbox";
        _imgGunbox.image = [UIImage imageNamed:@"gunbox"];
        [self hideAutoLock];
    }
    if (quickLockDeviceManage.lockDevice.lock_Type == QUICKBOX)
    {
        labelName.text = @"Gunbox Echo";
        _imgGunbox.image = [UIImage imageNamed:@"GunboxEcho"];
        [self hideAutoLock];
    }
    if (quickLockDeviceManage.lockDevice.lock_Type == MEDICINEBOX)
    {
        labelName.text = @"Medicinebox";
        _imgGunbox.image = [UIImage imageNamed:@"medicinebox"];
        [self hideAutoLock];
    }
    if (quickLockDeviceManage.lockDevice.lock_Type == TALLGUNBOX)
    {
        labelName.text = @"GunBox SK-1";
        _imgGunbox.image = [UIImage imageNamed:@"RifleBox"];
        [self hideAutoLock];
    }
    if (quickLockDeviceManage.lockDevice.lock_Type == RFWRITER)
    {
        labelName.text = @"RF Writer";
        _imgGunbox.image = [UIImage imageNamed:@"rf_Writer1"];
       // _imgGunbox.contentMode = UIViewContentModeScaleToFill;
        [self hideAutoLock];
    }
    
    if (quickLockDeviceManage.lockDevice.lock_AutoUnLock)
    {
        if (quickLockDeviceManage.lockDevice.passcode_enable == 1)
        {
            isUnlockNotPassToggle = 1;
            _bgViewOfPasscode.hidden = _viewPasscode.hidden =  NO;
            _lblPasscodeTitle.text = @"Enter 4 digit password";
            _txtPasscode.placeholder = @"Enter password";
            [_btnSave setTitle:@"Ok" forState:UIControlStateNormal];
            [UIView animateWithDuration:0.3 animations:^{
                _bgViewOfPasscode.alpha = 0.6;
                _viewPasscode.alpha = 1;
                [_txtPasscode becomeFirstResponder];
                isSharecodeNotShown = NO;
            }];
        }
        else
        {
            //            [buttonUnlock setBackgroundColor:[UIColor colorWithRed:15.0f/255.0f green:157.0f/255.0f blue:88.0f/255.0f alpha:1.0f]];
            //            [buttonUnlock setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            //            buttonUnlock.layer.borderColor = [UIColor colorWithRed:2.0f/255.0f green:112.0f/255.0f blue:62.0f/255.0f alpha:1.0f].CGColor;
            
            [quickLockDeviceManage openLock:YES];
            // If AutoUnlock open, set the automatic lock time
            [quickLockDeviceManage setAutoLockTime:5];
        }
        
        [buttonAutoUnlock setBackgroundImage:[UIImage imageNamed:@"orange-switch-icon"] forState:UIControlStateNormal];
    }
    else
    {
        [buttonAutoUnlock setBackgroundImage:[UIImage imageNamed:@"gray-switch-icon.png"] forState:UIControlStateNormal];
        [quickLockDeviceManage setAutoLockTime:quickLockDeviceManage.lockDevice.lock_AutoLockTime];
    }
    [quickLockDeviceManage getBatteryLevel];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)buttonAutoUnlockTouch:(id)sender
{
    if (quickLockDeviceManage.lockDevice.lock_AutoUnLock)
    {
        quickLockDeviceManage.lockDevice.lock_AutoUnLock = NO;
        [quickLockDeviceManage setAutoLockTime:quickLockDeviceManage.lockDevice.lock_AutoLockTime];
        
        [buttonAutoUnlock setBackgroundImage:[UIImage imageNamed:@"gray-switch-icon.png"] forState:UIControlStateNormal];
    }
    else
    {
        if (quickLockDeviceManage.lockDevice.passcode_enable == 1)
        {
            isUnlockNotPassToggle = 1;
            _bgViewOfPasscode.hidden = _viewPasscode.hidden =  NO;
            _lblPasscodeTitle.text = @"Enter 4 digit password";
            _txtPasscode.placeholder = @"Enter password";
            isAutoLockPressed = YES;
            [_btnSave setTitle:@"Ok" forState:UIControlStateNormal];
            [UIView animateWithDuration:0.3 animations:^{
                _bgViewOfPasscode.alpha = 0.6;
                _viewPasscode.alpha = 1;
                [_txtPasscode becomeFirstResponder];
                isSharecodeNotShown = NO;
            }];
        }
        else
        {
            quickLockDeviceManage.lockDevice.lock_AutoUnLock = YES;
            [buttonAutoUnlock setBackgroundImage:[UIImage imageNamed:@"orange-switch-icon.png"] forState:UIControlStateNormal];
            // If AutoUnlock open, set the automatic lock time
            [quickLockDeviceManage openLock:YES];
            [quickLockDeviceManage setAutoLockTime:5];
        }
    }
    
    [dbAccess UpdateLockDevice:quickLockDeviceManage.lockDevice WithLockID:quickLockDeviceManage.lockDevice.lock_ID];
    [dbAccess GetAllLockDevice];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [quickLockDeviceManage initiativeDisConnect];
        [self.navigationController popViewControllerAnimated:NO];
    });
}

- (IBAction)btnHome:(id)sender
{
    [quickLockDeviceManage initiativeDisConnect];
    [self.navigationController popViewControllerAnimated:NO];
}

- (IBAction)buttonUnlockTouch:(id)sender
{
    txtWriterUserId.text = @"";
    [UIView animateWithDuration:0.3 animations:^{
        viewOfWriteRFTag.hidden = NO;
        _bgViewOfPasscode.hidden = NO;
        _bgViewOfPasscode.alpha = 0.6;
        viewOfWriteRFTag.alpha = 1;
        [txtWriterUserId becomeFirstResponder];
    }];
    
   /* if (quickLockDeviceManage.lockDevice.passcode_enable == 1)
    {
        isUnlockNotPassToggle = 1;
        _bgViewOfPasscode.hidden = _viewPasscode.hidden =  NO;
        _lblPasscodeTitle.text = @"Enter 4 digit password";
        _txtPasscode.placeholder = @"Enter password";
        [_btnSave setTitle:@"Ok" forState:UIControlStateNormal];
        [UIView animateWithDuration:0.3 animations:^{
            _bgViewOfPasscode.alpha = 0.6;
            _viewPasscode.alpha = 1;
            [_txtPasscode becomeFirstResponder];
            isSharecodeNotShown = NO;
        }];
    }
    else
    {
        [buttonUnlock setBackgroundColor:[UIColor colorWithRed:15.0f/255.0f green:157.0f/255.0f blue:88.0f/255.0f alpha:1.0f]];
        [buttonUnlock setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        buttonUnlock.layer.borderColor = [UIColor colorWithRed:2.0f/255.0f green:112.0f/255.0f blue:62.0f/255.0f alpha:1.0f].CGColor;
        
        [quickLockDeviceManage openLock:YES];
        
        // Set automatic locking time
        [quickLockDeviceManage setAutoLockTime:quickLockDeviceManage.lockDevice.lock_AutoLockTime];
    }
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [quickLockDeviceManage initiativeDisConnect];
        [self.navigationController popViewControllerAnimated:NO];
    });*/
}

//// text validation and limit
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    // validation for telephone
    if (textField == _txtPasscode)
    {
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength <= 4);
    }
    if (textField == txtWriterUserId)
    {
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength <= 19);
    }
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [txtWriterUserId resignFirstResponder];
    return YES;
}

- (IBAction)buttonSetTouch:(id)sender
{
    settingVC = [self.storyboard instantiateViewControllerWithIdentifier:@"setting"];
    [self.navigationController pushViewController:settingVC animated:NO];
}

@end
