//
//  RFIDTags.m
//  QuickLock
//
//  Created by Ankit on 27/12/15.
//  Copyright (c) 2015 Bge. All rights reserved.
//

#import "RFIDTags.h"

@interface RFIDTags ()<UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>
{
    IBOutlet UILabel *lblAddNameTag;
    IBOutlet UILabel *lblTitleRfidTag;
    NSMutableArray *arrayOfRFIDList;
    
}
// Properties
@property (weak, nonatomic) IBOutlet UITableView *tblRfidTags;
@property (weak, nonatomic) IBOutlet UIButton *btnAddTag;
@property (weak, nonatomic) IBOutlet UIView *viewBgPop;
@property (weak, nonatomic) IBOutlet UIView *viewPopup;
@property (weak, nonatomic) IBOutlet UIButton *btnPopupYes;
@property (weak, nonatomic) IBOutlet UIButton *btnPopupNo;

@property (weak, nonatomic) IBOutlet UILabel *lblNoRfid;
@property (weak, nonatomic) IBOutlet UITextField *txtTagName;
@property (weak, nonatomic) IBOutlet UIButton *btnSaveTag;
@property (weak, nonatomic) IBOutlet UIButton *btnCancelTag;
@property (weak, nonatomic) IBOutlet UIView *viewOfTagInput;
@end

@implementation RFIDTags
@synthesize lockDevice;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    arrayHistoryLog = [NSMutableArray new];
    arrDate = [NSMutableArray new];
    dictForHistory = [NSMutableDictionary new];
    arrName = [NSMutableArray new];
    arrTagId = [NSMutableArray new];
    arrayOfRFIDList = [[NSMutableArray alloc]init];
    
    lblTitleRfidTag.textColor = titleorange;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(btnCancelTagName:)];
    [_viewBgPop addGestureRecognizer:tap];
    
    _txtTagName.returnKeyType = UIReturnKeyDone;
    
    // Defined Notification center to get History Log
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ShowHistoryLogRFID:) name:@"kReceiveHistoryLog" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(callAfterAddNewRFID:) name:@"kcallAfterAddNewRFID" object:nil];
    
    lockDevice = [LockDevice new];
    
    [self btnLayer:_btnPopupYes view:nil];
    [self btnLayer:_btnPopupNo view:_viewPopup];
    
    _viewBgPop.alpha = 0.0f;
    _viewPopup.alpha = 0.0f;
    _viewBgPop.backgroundColor = [UIColor blackColor];
    
    [_viewOfTagInput.layer setCornerRadius:10];
    [_viewOfTagInput.layer setMasksToBounds:YES];
    [_viewOfTagInput.layer setBorderColor:bordergray.CGColor];
    [_viewOfTagInput.layer setBorderWidth:4];
    
    [_txtTagName.layer setCornerRadius:10];
    [_txtTagName.layer setMasksToBounds:YES];
    [_txtTagName.layer setBorderColor:bordergray.CGColor];
    [_txtTagName.layer setBorderWidth:3];
    _txtTagName.backgroundColor = lightgray;
    
    _btnSaveTag.layer.masksToBounds = _btnCancelTag.layer.masksToBounds = YES;
    _btnSaveTag.layer.cornerRadius = _btnCancelTag.layer.cornerRadius = 6;
    arrayHistoryLog = [NSMutableArray new];
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([string isEqualToString:@"\n"] || [string isEqualToString:@""])
    {
        return YES;
    }
    else if (textField.text.length > 20)
    {
        return NO;
    }
    else
    {
        return [kUserNameRange rangeOfString:string].location != NSNotFound;
    }
}

- (IBAction)btnaddTag:(id)sender
{
    [quickLockDeviceManage getRFIDAdd];
    [self.view makeToast:@"Place a new RFID tag on the lock to add it."];
}

-(void)callAfterAddNewRFID:(NSNotification *)notification
{
    NSDictionary *userInfo = notification.userInfo;
    NSString *strMsg = [NSString stringWithFormat:@"%@",userInfo[@"msg"]];
    [self.view makeToast:strMsg];
    
    [quickLockDeviceManage getRFIDList];
    
    if(arrayLock.count == 0)
    {
        _lblNoRfid.hidden = NO;
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // get RFID from lock database
    [quickLockDeviceManage getRFIDList];
    
    if(arrayLock.count == 0)  {
        _lblNoRfid.hidden = NO;
    }
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self update_rfid_list:[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"userId"]] withlock_UUID:[NSString stringWithFormat:@"%@",quickLockDeviceManage.lockDevice.lock_UUID] RFIdList:arrayOfRFIDList];
    });
}

- (void)ShowHistoryLogRFID:(NSNotification *)notification
{
    HistoryLog *historyLog = [notification object];
    // set data to object
    arrayHistoryLog = [self InsertData:historyLog ToArray:arrayHistoryLog];
    
    historyLog = arrayHistoryLog.lastObject;
    
    NSMutableDictionary *dictPfRFIdRecord = [[NSMutableDictionary alloc]init];
    // NSLog(@"historyLog : %@",historyLog.operateLockType);
    NSLog(@"operateLockDevice : %@",historyLog.operateLockDevice);
    NSLog(@"operateLockTime : %@",historyLog.operateLockTime);
    NSLog(@"isOpenLock : %d",historyLog.isOpenLock);
    
    NSString *deviceType = @"";
    // check NFC to get data of only contained RFID not contained name
    if (historyLog.operateLockType == NFC)
    {
        deviceType = [NSString stringWithFormat:@"%@", historyLog.operateLockDevice];
    }
    
    // Shortout unique RFIDs in arrTagId Array
    [arrTagId addObject:deviceType];
    NSOrderedSet *mySet = [[NSOrderedSet alloc] initWithArray:arrTagId];
    arrTagId = [[NSMutableArray alloc] initWithArray:[mySet array]];
    
    // if not available RFID in dictForHistory then only it will add.
    if (![dictForHistory valueForKey:deviceType])
    {
        if ([deviceType isEqualToString:@""])
        {
            NSLog(@"blank");
        }
        else
        {
            [arrName addObject:@"Name"];
            lockDevice.TAG_TYPE = @"NFC";
            lockDevice.TAG_USERNAME = @"Name";
            
            lockDevice.TAG_IMEI = [NSString stringWithFormat:@"%@",deviceType];
            [dictForHistory setObject:deviceType forKey:deviceType];
            
            NSString *isOpenlock;
            if (historyLog.isOpenLock == YES) {
                isOpenlock = @"YES";
            }
            else{
                isOpenlock = @"NO";
            }
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSString *newDate = [dateFormatter stringFromDate:historyLog.operateLockTime];
            
            
            NSString *lockType = [NSString stringWithFormat:@"%lu",(unsigned long)historyLog.operateLockType];
            [dictPfRFIdRecord setObject:lockDevice.TAG_USERNAME forKey:@"rfid_card_name"];
            [dictPfRFIdRecord setObject:isOpenlock forKey:@"isOpenLock"];
            [dictPfRFIdRecord setObject: [self CheckForNullparameter:newDate] forKey:@"unlock_time"];
            [dictPfRFIdRecord setObject:historyLog.operateLockDevice forKey:@"rfid_tag_id"];
            [dictPfRFIdRecord setObject:lockType forKey:@"operateLockType"];
            
            [arrayOfRFIDList addObject:dictPfRFIdRecord];
            
            appDelegate.TAG_IMEI = [NSString stringWithFormat:@"%@",deviceType];
            // Get Single RFID Tags from database
            NSString *strIMEI = [dbAccess GetSingleRFIDTags];
            if ([strIMEI isEqualToString:appDelegate.TAG_IMEI])
            {
                // if equal then do nothing
                //                [dbAccess DeleteRFIDTag:strIMEI];
            }
            else
            {
                lockDevice.TAG_ID = [dbAccess InsertRFIDTags:lockDevice];
            }
            // Get all RFIDs to show in table
            arrayLock = [dbAccess GetRFIDTags];
            [_tblRfidTags reloadData];
        }
    }
    
    if(arrayLock.count == 0)    {
        _lblNoRfid.hidden = NO;
    }
    else    {
        _lblNoRfid.hidden = YES;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrayLock.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell= [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    }
    lockDevice = arrayLock[indexPath.row];
    
    // minus-icon.png
    UIImageView *imgDel = [[UIImageView alloc]initWithFrame:CGRectMake(screenSize.width-39, 7, 20, 20)];
    imgDel.image = [UIImage imageNamed:@"minus-icon"];
    [cell.contentView addSubview:imgDel];
    
    UIButton *btnDel = [UIButton buttonWithType:UIButtonTypeSystem];
    btnDel.frame = CGRectMake(screenSize.width-46, 0, 46, 36);
    btnDel.tag = indexPath.row;
    [btnDel addTarget:self action:@selector(deleteTag:) forControlEvents:UIControlEventTouchUpInside];
    [cell.contentView addSubview:btnDel];
    
    UILabel *lblName = [[UILabel alloc]initWithFrame:CGRectMake(screenSize.width-(46+114+10) , 0, 114, 36)];
    lblName.text = [NSString stringWithFormat:@"  %@",lockDevice.TAG_USERNAME];
    [cell.contentView addSubview:lblName];
    
    UIButton *btnChangeName = [UIButton buttonWithType:UIButtonTypeSystem];
    btnChangeName.frame = CGRectMake(screenSize.width-(46+114+10) , 0, 114, 36);
    btnChangeName.tag = indexPath.row;
    [btnChangeName addTarget:self action:@selector(btnEnterName:) forControlEvents:UIControlEventTouchUpInside];
    [cell.contentView addSubview:btnChangeName];
    
    
    UILabel *lblTagId = [[UILabel alloc]initWithFrame:CGRectMake(4, 0, lblName.frame.origin.x-10, 36)];
    lblTagId.text = [NSString stringWithFormat:@"  %@",lockDevice.TAG_IMEI];
    [cell.contentView addSubview:lblTagId];
    
    lblTagId.font = lblName.font = [UIFont systemFontOfSize:13];
    lblTagId.backgroundColor = lblName.backgroundColor = cellBgColor;
    lblTagId.textColor = lblName.textColor = darkgray;
    
    if (arrayLock.count == 1)
    {
        lblTagId.layer.masksToBounds = lblName.layer.masksToBounds = YES;
        lblTagId.layer.cornerRadius = lblName.layer.cornerRadius = 8;
    }
    else
    {
        UIBezierPath *maskPath;
        if (indexPath.row == 0)
        {
            maskPath = [UIBezierPath bezierPathWithRoundedRect:lblName.bounds
                                             byRoundingCorners:(UIRectCornerTopLeft|UIRectCornerTopRight)
                                                   cornerRadii:CGSizeMake(8, 8)];
            CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
            maskLayer.frame = lblName.bounds;
            maskLayer.path = maskPath.CGPath;
            lblName.layer.mask = maskLayer;
            
            maskPath = [UIBezierPath bezierPathWithRoundedRect:lblTagId.bounds
                                             byRoundingCorners:(UIRectCornerTopLeft|UIRectCornerTopRight)
                                                   cornerRadii:CGSizeMake(8, 8)];
            maskLayer = [[CAShapeLayer alloc] init];
            maskLayer.frame = lblTagId.bounds;
            maskLayer.path = maskPath.CGPath;
            lblTagId.layer.mask = maskLayer;
        }
        else if(indexPath.row == arrTagId.count-1)
        {
            maskPath = [UIBezierPath bezierPathWithRoundedRect:lblName.bounds
                                             byRoundingCorners:(UIRectCornerBottomLeft|UIRectCornerBottomRight)
                                                   cornerRadii:CGSizeMake(8, 8)];
            CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
            maskLayer.frame = lblName.bounds;
            maskLayer.path = maskPath.CGPath;
            lblName.layer.mask = maskLayer;
            
            maskPath = [UIBezierPath bezierPathWithRoundedRect:lblTagId.bounds
                                             byRoundingCorners:(UIRectCornerBottomLeft|UIRectCornerBottomRight)
                                                   cornerRadii:CGSizeMake(8, 8)];
            maskLayer = [[CAShapeLayer alloc] init];
            maskLayer.frame = lblTagId.bounds;
            maskLayer.path = maskPath.CGPath;
            lblTagId.layer.mask = maskLayer;
        }
    }
    return cell;
}

- (NSMutableArray *)InsertData:(HistoryLog *)historyLog ToArray:(NSMutableArray *)sourceNumber
{
    if (!sourceNumber || sourceNumber.count == 0)
    {
        [sourceNumber addObject:historyLog];
        return sourceNumber;
    }
    
    //Smaller than the first
    if ([historyLog.operateLockTime timeIntervalSince1970] < [((HistoryLog *)sourceNumber[0]).operateLockTime timeIntervalSince1970])
    {
        [sourceNumber insertObject:historyLog atIndex:0];
        return sourceNumber;
    }
    else
    {
        if (arrayHistoryLog.count == 1)
        {
            [sourceNumber addObject:historyLog];
            return sourceNumber;
        }
    }
    for (int j = 1; j < sourceNumber.count; j++)
    {
        if ([historyLog.operateLockTime timeIntervalSince1970] >=
            [((HistoryLog *)sourceNumber[j - 1]).operateLockTime timeIntervalSince1970] &&
            [historyLog.operateLockTime timeIntervalSince1970] <
            [((HistoryLog *)sourceNumber[j]).operateLockTime timeIntervalSince1970])
        {
            [sourceNumber insertObject:historyLog atIndex:j];
            break;
        }
        
        //Than the last big
        if ((j == sourceNumber.count - 1) &&
            ([historyLog.operateLockTime timeIntervalSince1970] >=
             [((HistoryLog *)sourceNumber[j]).operateLockTime timeIntervalSince1970]))
        {
            [sourceNumber addObject:historyLog];
            break;
        }
    }
    
    return sourceNumber;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [UIView animateWithDuration:0.3f animations:^{
        _viewOfTagInput.transform = CGAffineTransformMakeTranslation(0, 0);
    }];
    [textField resignFirstResponder];
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    //if (screenSize.height <= 568)
    {
        [UIView animateWithDuration:0.3f animations:^{
            _viewOfTagInput.transform = CGAffineTransformMakeTranslation(0, -60);
        }];
    }
}

- (IBAction)btnSaveTagName:(UIButton *)sender
{
    NSString *strIMEI = arrTagId[sender.tag];
    quickLockDeviceManage.lockDevice.TAG_USERNAME = _txtTagName.text;
    [dbAccess UpdateRFIDName:quickLockDeviceManage.lockDevice WithIMEI:strIMEI];
    
    [UIView animateWithDuration:0.3f animations:^{
        _viewOfTagInput.transform = CGAffineTransformMakeTranslation(0, 0);
        _viewBgPop.alpha = 0.0f;
        _viewOfTagInput.alpha = 0.0f;
    } completion:^(BOOL finished) {
        _viewBgPop.hidden = YES;
        _viewOfTagInput.hidden = YES;
        
        _txtTagName.text = @"";
        [_txtTagName resignFirstResponder];
        
        appDelegate.TAG_IMEI = @"";
        arrayLock = [dbAccess GetRFIDTags];
        [_tblRfidTags reloadData];
    }];
}

- (IBAction)btnCancelTagName:(id)sender
{
    [UIView animateWithDuration:0.3f animations:^{
        _viewOfTagInput.transform = CGAffineTransformMakeTranslation(0, 0);
        _viewBgPop.alpha = 0.0f;
        _viewOfTagInput.alpha = 0.0f;
        _viewPopup.alpha = 0.0f;
    } completion:^(BOOL finished) {
        _viewBgPop.hidden = YES;
        _viewOfTagInput.hidden = YES;
        _viewPopup.hidden = YES;
        
        [_txtTagName resignFirstResponder];
        _txtTagName.text = @"";
    }];
}

-(void)btnEnterName:(UIButton *)sender
{
    _btnSaveTag.tag = sender.tag;
    lockDevice = arrayLock[sender.tag];
    NSString *strName = [NSString stringWithFormat:@"%@",lockDevice.TAG_USERNAME];
    
    if ([strName isEqualToString:@"Name"] || [strName isEqualToString:@"name"])
        _txtTagName.text = @"";
    else
        _txtTagName.text = strName;
    
    _viewBgPop.hidden = NO;
    _viewOfTagInput.hidden = NO;
    [UIView animateWithDuration:0.3f animations:^{
        _viewBgPop.alpha = 0.6f;
        _viewOfTagInput.alpha = 1.0f;
    } completion:^(BOOL finished) {
        [_txtTagName becomeFirstResponder];
    }];
}

-(void)btnLayer:(UIButton *)button view:(UIView *)view
{
    button.layer.masksToBounds = view.layer.masksToBounds = YES;
    button.layer.cornerRadius = view.layer.cornerRadius = 8;
    
    view.layer.borderColor = bordergray.CGColor;
    view.layer.borderWidth = 4;
}

- (IBAction)btnBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}


// Code to adjust line of UITableViewCell from left side
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath*)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)])
    {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}
-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    if ([_tblRfidTags respondsToSelector:@selector(setSeparatorInset:)])
    {
        [_tblRfidTags setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([_tblRfidTags respondsToSelector:@selector(setLayoutMargins:)])
    {
        [_tblRfidTags setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 36;
}

-(void)deleteTag:(UIButton *)sender
{
    _viewBgPop.hidden = NO;
    _viewPopup.hidden = NO;
    [UIView animateWithDuration:0.3f animations:^{
        _viewBgPop.alpha = 0.6f;
        _viewPopup.alpha = 1.0f;
    } completion:^(BOOL finished) {
        
    }];
    _btnPopupYes.tag = sender.tag;
}

- (IBAction)btnPopupYes:(UIButton *)sender
{
    lockDevice = arrayLock[sender.tag];
    NSString *strIMEI = lockDevice.TAG_IMEI;
    
    quickLockDeviceManage.lockDevice.TAG_DELETE_IMEI_OR_RFID = strIMEI;  //TAG_USERNAME = _txtTagName.text;
    [quickLockDeviceManage deleteRFID];
    
    [dbAccess DeleteRFIDTag:strIMEI];
    
    // Call W.S
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self DeleteRFID:[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"userId"]] withlock_UUID:appDelegate.lock_Address withrfid_tag_id:strIMEI];
    });
    
    [UIView animateWithDuration:0.3f animations:^{
        _viewBgPop.alpha = 0.0f;
        _viewPopup.alpha = 0.0f;
    } completion:^(BOOL finished) {
        _viewBgPop.hidden = YES;
        _viewPopup.hidden = YES;
        
        arrTagId = [NSMutableArray new];
        arrName = [NSMutableArray new];
        dictForHistory = [NSMutableDictionary new];
        
        appDelegate.TAG_IMEI = @"";
        arrayLock = [dbAccess GetRFIDTags];
        if (arrayLock.count > 0)
        {
            for (int i = 0; i < arrayLock.count; i++)
            {
                lockDevice = arrayLock[i];
                [arrTagId addObject:lockDevice.TAG_IMEI];
                
                if (![dictForHistory valueForKey:lockDevice.TAG_IMEI])
                {
                    if ([lockDevice.TAG_IMEI isEqualToString:@""])
                    {
                        NSLog(@"blank");
                    }
                    else
                    {
                        [dictForHistory setObject:lockDevice.TAG_IMEI forKey:lockDevice.TAG_IMEI];
                        [arrName addObject:lockDevice.TAG_USERNAME];
                    }
                }
            }
            NSOrderedSet *mySet = [[NSOrderedSet alloc] initWithArray:arrTagId];
            arrTagId = [[NSMutableArray alloc] initWithArray:[mySet array]];
        }
        else
        {
            _lblNoRfid.hidden = NO;
        }
        [_tblRfidTags reloadData];
    }];
}

- (IBAction)btnPopupNo:(UIButton *)sender
{
    [UIView animateWithDuration:0.3f animations:^{
        _viewBgPop.alpha = 0.0f;
        _viewPopup.alpha = 0.0f;
    } completion:^(BOOL finished) {
        _viewBgPop.hidden = YES;
        _viewPopup.hidden = YES;
    }];
}

-(void)DeleteRFID:(NSString *)user_id withlock_UUID:(NSString *)lock_UUID withrfid_tag_id:(NSString *)rfid_tag_id
{
    //    http://27.109.19.234/lynkd/backend/web/api/deleterfid?user_id=8&lock_UUID=1254657&rfid_tag_id=abc123
    
    NSDictionary *dictParam = @{@"user_id":[self CheckForNullparameter:user_id],
                                @"lock_UUID":[self CheckForNullparameter:lock_UUID],
                                @"rfid_tag_id":[self CheckForNullparameter:rfid_tag_id]};
    
    NSString *strUrl = [NSString stringWithFormat:@"%@deleterfid?",BASEURL];
    
    NSLog(@"strUrl %@  dictParam %@",strUrl,dictParam);
    
    [Webservices postWithUrlString:strUrl parameters:dictParam success:^(NSDictionary *responce) {
        //Success
        NSLog(@"responce:%@",responce);
        
        //do code here
        if ([[responce valueForKey:@"status"] isEqualToString:@"1"]) {
            
            NSLog(@"Delete Successful");
        }else{
            NSLog(@"Delete Not Successful");
        }
    } failure:^(NSError *error) {
        //error
        NSLog(@"error:%@",error);
    }];
}

-(NSString *)CheckForNullparameter:(NSString*)param{
    
    NSString *param1=[NSString stringWithFormat:@"%@",param];
    if ( [param1 isEqual:(id)[NSNull null]] || param1==nil || [param1 isEqualToString:@""] || [param1 isKindOfClass:[NSNull class]]||[param1 isEqualToString:@"<null>"]||[param1 isEqualToString:@"(<null>)"]||[param1 isEqualToString:@"(null)"] || [param1 isEqualToString:@"<nil>"])
    {
        param1=@"";
        return param1;
    }
    else
    {
        return param1;
    }
    return @"";
}

-(void)update_rfid_list:(NSString *)user_id withlock_UUID:(NSString *)lock_UUID RFIdList:(NSArray *)RFIdList
{
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:RFIdList
                                                       options:0
                                                         error:&error];
    NSString *jsonString1 = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
 /*    NSString* encodedUrl = [jsonString1 stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];

    
    NSDictionary *dictParam = @{@"RFIdList":jsonString1,@"user_id":[self CheckForNullparameter:user_id],@"lock_UUID":[self CheckForNullparameter:lock_UUID]};
    
   NSString *strUrl = [NSString stringWithFormat:@"%@updaterfidlist?",BASEURL];
    
    NSLog(@"strUrl %@  dictParam %@",strUrl,dictParam);
    
    [Webservices postWithUrlString:strUrl parameters:dictParam success:^(NSDictionary *responce) {
        //Success
        NSLog(@"responce:%@",responce);
        
        //do code here
        if ([[responce valueForKey:@"status"] isEqualToString:@"1"]) {
            
            NSLog(@" Successful");
        }else{
            NSLog(@" Not Successful");
        }
    } failure:^(NSError *error) {
        //error
        NSLog(@"error:%@",error);
    }];*/
    

    NSString *strUrl = [NSString stringWithFormat:@"%@updaterfidlist?lock_UUID=%@&user_id=%@&RFIdList=%@",BASEURL,[self CheckForNullparameter:lock_UUID],[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"userId"]],jsonString1];
    NSString *encodedUrl = [strUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];

    [Webservices getWithUrlString:encodedUrl success:^(NSDictionary *responce) {
        //Success
        NSLog(@"responce:%@",responce);
        //do code here
        if ([[responce valueForKey:@"status"] isEqualToString:@"1"]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSLog(@"RFID Added successfully");
            });
            
            [self.view makeToast:(@"RFID Added successfully")];
        }else{
            NSLog(@"Share code not added");
        }
    } failure:^(NSError *error) {
        //error
        NSLog(@"error:%@",error.localizedDescription);
    }];
}

@end
