//
//  AlertLogin.m
//  QuickLock
//
//  Created by PUNDSK001 on 13/06/16.
//  Copyright © 2016 Bge. All rights reserved.
//

#import "AlertLogin.h"
#import "layerTheButton.h"
#import "SetupProfileController.h"
@interface AlertLogin ()
{
    __weak IBOutlet UIActivityIndicatorView *activity;
    __weak IBOutlet layerTheButton *btnCreateAccount;
}

@property (strong, nonatomic) IBOutlet UITextField *txtUserName;
@property (strong, nonatomic) IBOutlet UITextField *txtPassword;
@property (strong, nonatomic) IBOutlet UILabel *lblTitleLogin;
@end

@implementation AlertLogin

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _txtPassword.backgroundColor = lightgray;
    _txtUserName.backgroundColor = lightgray;
    _lblTitleLogin.textColor = titleorange;
    // [_txtUserName becomeFirstResponder];
    
    _txtUserName.text = @"jaydeep@magnetoitsolutions.com";
    _txtPassword.text = @"12345678";
    
    self.view.backgroundColor = backgrondcolor;
    
    self.navigationController.navigationBarHidden = YES;
}

- (IBAction)btnBack:(id)sender{
    [self.navigationController popViewControllerAnimated:NO];
}

- (IBAction)createAccount:(id)sender {
    SetupProfileController *alert = [self.storyboard instantiateViewControllerWithIdentifier:@"SetupProfileController"];
    [self.navigationController pushViewController:alert animated:NO];

}

- (IBAction)btnLogin:(id)sender
{
    [self.view endEditing:YES];
    
    if (_txtUserName.text.length == 0)
    {
        [self.view makeToast:@"Enter User Name" duration:2.0 position:@"Center"];
    }
    else if (_txtPassword.text.length == 0)
    {
        [self.view makeToast:@"Enter Password" duration:2.0 position:@"Center"];
    }
    //     else if (_txtPassword.text.length < 8)
    //     {
    //     [self.view makeToast:@"Enter 8 digital Password" duration:2.0 position:@"Center"];
    //     }
    else
    {
        [activity startAnimating];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [self LoginwithEmailID:_txtUserName.text withPassword:_txtPassword.text withDeviceToken:@"12546589" withdevice_type:@"I"];
        });
    }
}

//- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
//{
//    if (textField == _txtPassword)
//    {
//        if ([string isEqualToString:@"\n"] || [string isEqualToString:@""])
//        {
//            return YES;
//        }
//        else if (textField.text.length > 7)
//        {
//            return NO;
//        }
//        else
//        {
//            return [kDigitalRange rangeOfString:string].location != NSNotFound;
//        }
//    }
//    if (textField == _txtUserName)
//    {
//        if ([string isEqualToString:@"\n"] || [string isEqualToString:@""])
//        {
//            return YES;
//        }
//        else if (textField.text.length > 25)
//        {
//            return NO;
//        }
//        else
//        {
////            return [kUserNameRange rangeOfString:string].location != NSNotFound;
//            return YES;
//        }
//    }
//    return NO;
//}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == _txtUserName) {
        [_txtPassword becomeFirstResponder];
    }
    else{
        [_txtPassword resignFirstResponder];
    }
    return  YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//-(void)LoginwithEmail:(NSString *)email withPassword:(NSString *)password withDeviceToken:(NSString *)device_token withdevice_type:(NSString *)device_type withlatitude:(NSString *)latitude withlongitude:(NSString *)longitude
//{
//    //http://www.rtvbn.com/api/getInitNews/dashboard/15/1
//
////    http://27.109.19.234/lynkd/backend/web/api/login?email=mphp.magneto@gmail.com&password=123456&device_token=125465&device_type=A&latitude=0.23&longitude=0.368
//
//    NSString *strBaseUrl;
//
//    strBaseUrl = [NSString stringWithFormat:@"%@login?email=%@&password=%@&device_token=%@&device_type=%@&latitude=%@&longitude=%@",BASEURL,email,password,device_token,device_type,latitude,longitude];
//
//    NSURL *url = [NSURL URLWithString:strBaseUrl];
//    NSLog(@"url %@",url);
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
//
//    [request setHTTPMethod:@"GET"];
//    NSURLSession *session = [NSURLSession sharedSession];
//    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
//                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//                                                    if (error)
//                                                    {
//                                                        dispatch_async(dispatch_get_main_queue(), ^{
//                                                            NSLog(@"Errorrrr %@", error);
//                                                        });
//                                                    }
//                                                    else
//                                                    {
//                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
//
//                                                        NSMutableDictionary *DicData = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&error];
//
//                                                        if (DicData != nil) {
//                                                            NSLog(@"Dict = %@",DicData);
//                                                            //                                                        dispatch_async(dispatch_get_main_queue(), ^{
//
//                                                            if ([[DicData valueForKey:@"status"] isEqualToString:@"1"]) {
//                                                                NSLog(@"Login Successful");
//                                                            }else{
//                                                                NSLog(@"Login Not Successful");
//                                                            }
//
////                                                            [self Success];
//                                                            //                                                        });
//                                                        }
//                                                    }
//                                                }];
//    [dataTask resume];
//}

-(NSString *)CheckForNullparameter:(NSString*)param{
    
    NSString *param1=[NSString stringWithFormat:@"%@",param];
    if ( [param1 isEqual:(id)[NSNull null]] || param1==nil || [param1 isEqualToString:@""] || [param1 isKindOfClass:[NSNull class]]||[param1 isEqualToString:@"<null>"]||[param1 isEqualToString:@"(<null>)"]||[param1 isEqualToString:@"(null)"])
    {
        param1=@"";
        return param1;
    }
    else
    {
        return param1;
    }
    return @"";
}

#pragma mark - W.S Methods

-(void)LoginwithEmailID:(NSString *)email withPassword:(NSString *)password withDeviceToken:(NSString *)device_token withdevice_type:(NSString *)device_type

{
    //    http://27.109.19.234/lynkd/backend/web/api/login?email=mphp.magneto@gmail.com&password=123456&device_token=125465&device_type=A&latitude=0.23&longitude=0.368
    
    NSString *latitude = [self CheckForNullparameter:[NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] valueForKey:@"latitude"]]];;
    NSString *longitude = [self CheckForNullparameter:[NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] valueForKey:@"longitude"]]];
    NSString *strDeviceToken = [self CheckForNullparameter:[NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] valueForKey:@"deviceToken"]]];
    
    NSLog(@"deviceToken %@ => ",strDeviceToken);
    //    device_type = 1 = iOS and 2= Android
    
    NSDictionary *dictParam = @{@"email":email,@"password":password,@"device_token":strDeviceToken,@"device_type":device_type,@"latitude":latitude,@"longitude":longitude};
    
    NSString *strUrl = [NSString stringWithFormat:@"%@login?",BASEURL];
    [Webservices postWithUrlString:strUrl parameters:dictParam success:^(NSDictionary *responce) {
        //Success
        NSLog(@"responce:%@",responce);
        //do code here
        if ([[responce valueForKey:@"status"] isEqualToString:@"1"]) {
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSString *struserId = [NSString stringWithFormat:@"%@",[responce valueForKey:@"data"][@"userId"]];
            [defaults setValue:[NSString stringWithFormat:@"%@", struserId] forKey:@"userId"];
            [defaults synchronize];
            NSLog(@"Login Successful");
            dispatch_async(dispatch_get_main_queue(), ^{
                [activity stopAnimating];
                ViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"root"];
                [self.navigationController pushViewController:viewController animated:YES];
            });
            
        }else{
            NSLog(@"%@", [responce valueForKey:@"message"]);
            dispatch_async(dispatch_get_main_queue(), ^{
                [activity stopAnimating];
                [self.view makeToast:[responce valueForKey:@"message"] duration:2.0 position:@"Center"];
                NSLog(@"Login Not Successful");
            });
        }
    } failure:^(NSError *error) {
        //error
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"error:%@",error);
            [activity stopAnimating];
            [self.view makeToast:@"Something went wrong Please try again" duration:2.0 position:@"Center"];
        });
    }];
}


-(void)adduserlock:(NSString *)user_id withlock_UUID:(NSString *)lock_UUID withlock_username:(NSString *)lock_username withlock_address:(NSString *)lock_address withlock_password:(NSString *)lock_password withlock_type:(NSString *)lock_type withlock_name:(NSString *)lock_name withlock_version:(NSString *)lock_version withlock_autounLock:(NSString *)lock_autounLock withlock_autolocktime:(NSString *)lock_autolocktime withled_time:(NSString *)led_time withsure_set:(NSString *)sure_set withpasscode:(NSString *)passcode withpasscode_enable:(NSString *)passcode_enable

{
    //http://27.109.19.234/lynkd/backend/web/api/adduserlock?user_id=8&lock_UUID=1254657&lock_username=lock1&lock_address=123&lock_password=123&lock_type=1&lock_name=123&lock_version=1&lock_autounLock=1&lock_autolocktime=30&led_time=30&sure_set=30&passcode=123&passcode_enable=1
    NSString *strDeviceId = [[NSUserDefaults standardUserDefaults] valueForKey:@"deviceId"];
    NSString *strDeviceToken = [[NSUserDefaults standardUserDefaults] valueForKey:@"deviceToken"];
    NSLog(@"deviceToken %@, strDeviceId %@",strDeviceToken,strDeviceId);
    //    device_type = 1 = iOS and 2= Android
    
    NSDictionary *dictParam = @{@"user_id":user_id,@"lock_UUID":lock_UUID,@"lock_username":lock_username,@"lock_address":lock_address,@"lock_password":lock_password,@"lock_type":lock_type,@"lock_name":lock_name,@"lock_version":lock_version,@"lock_autounLock":lock_autounLock,@"lock_autolocktime":lock_autolocktime,@"led_time":led_time,@"sure_set":sure_set,@"passcode":passcode,@"passcode_enable":passcode_enable};
    
    NSString *strUrl = [NSString stringWithFormat:@"%@adduserlock?",BASEURL];
    [Webservices postWithUrlString:strUrl parameters:dictParam success:^(NSDictionary *responce) {
        //Success
        NSLog(@"responce:%@",responce);
        //do code here
        if ([[responce valueForKey:@"status"] isEqualToString:@"1"]) {
            
            NSLog(@"adduserlock Successful");
        }else{
            NSLog(@"adduserlock Not Successful");
        }
    } failure:^(NSError *error) {
        //error
        NSLog(@"error:%@",error);
    }];
}

-(void)updateuserlock:(NSString *)user_id withlock_UUID:(NSString *)lock_UUID withlock_username:(NSString *)lock_username withlock_address:(NSString *)lock_address withlock_password:(NSString *)lock_password withlock_type:(NSString *)lock_type withlock_name:(NSString *)lock_name withlock_version:(NSString *)lock_version withlock_autounLock:(NSString *)lock_autounLock withlock_autolocktime:(NSString *)lock_autolocktime withled_time:(NSString *)led_time withsure_set:(NSString *)sure_set withpasscode:(NSString *)passcode withpasscode_enable:(NSString *)passcode_enable

{
    //http://27.109.19.234/lynkd/backend/web/api/updateuserlock?user_id=8&lock_UUID=1254657&lock_username=lock1&lock_address=123&lock_password=123&lock_type=1&lock_name=123&lock_version=1&lock_autounLock=1&lock_autolocktime=30&led_time=30&sure_set=30&passcode=123&passcode_enable=1
    NSString *strDeviceId = [[NSUserDefaults standardUserDefaults] valueForKey:@"deviceId"];
    NSString *strDeviceToken = [[NSUserDefaults standardUserDefaults] valueForKey:@"deviceToken"];
    NSLog(@"deviceToken %@, strDeviceId %@",strDeviceToken,strDeviceId);
    //    device_type = 1 = iOS and 2= Android
    
    NSDictionary *dictParam = @{@"user_id":user_id,@"lock_UUID":lock_UUID,@"lock_username":lock_username,@"lock_address":lock_address,@"lock_password":lock_password,@"lock_type":lock_type,@"lock_name":lock_name,@"lock_version":lock_version,@"lock_autounLock":lock_autounLock,@"lock_autolocktime":lock_autolocktime,@"led_time":led_time,@"sure_set":sure_set,@"passcode":passcode,@"passcode_enable":passcode_enable};
    
    NSString *strUrl = [NSString stringWithFormat:@"%@updateuserlock?",BASEURL];
    [Webservices postWithUrlString:strUrl parameters:dictParam success:^(NSDictionary *responce) {
        //Success
        NSLog(@"responce:%@",responce);
        //do code here
        if ([[responce valueForKey:@"status"] isEqualToString:@"1"]) {
            
            NSLog(@"updateuserlock Successful");
        }else{
            NSLog(@"updateuserlock Not Successful");
        }
    } failure:^(NSError *error) {
        //error
        NSLog(@"error:%@",error);
    }];
}

-(void)AddRFID:(NSString *)user_id withlock_UUID:(NSString *)lock_UUID withrfid_tag_id:(NSString *)rfid_tag_id withtag_name:(NSString *)tag_name
{
    
    //     http://27.109.19.234/lynkd/backend/web/api/addrfid?user_id=8&lock_UUID=1254657&rfid_tag_id=abc123&tag_name=abctag
    
    NSDictionary *dictParam = @{@"user_id":user_id,@"lock_UUID":lock_UUID,@"rfid_tag_id":rfid_tag_id,@"tag_name":tag_name};
    
    NSString *strUrl = [NSString stringWithFormat:@"%@addrfid?",BASEURL];
    [Webservices postWithUrlString:strUrl parameters:dictParam success:^(NSDictionary *responce) {
        //Success
        NSLog(@"responce:%@",responce);
        //do code here
        if ([[responce valueForKey:@"status"] isEqualToString:@"1"]) {
            
            NSLog(@"AddRFID Successful");
        }else{
            NSLog(@"AddRFID Not Successful");
        }
    } failure:^(NSError *error) {
        //error
        NSLog(@"error:%@",error);
    }];
}

-(void)DeleteRFID:(NSString *)user_id withlock_UUID:(NSString *)lock_UUID withrfid_tag_id:(NSString *)rfid_tag_id
{
    //    http://27.109.19.234/lynkd/backend/web/api/deleterfid?user_id=8&lock_UUID=1254657&rfid_tag_id=abc123
    
    NSDictionary *dictParam = @{@"user_id":user_id,@"lock_UUID":lock_UUID,@"rfid_tag_id":rfid_tag_id};
    
    NSString *strUrl = [NSString stringWithFormat:@"%@deleterfid?",BASEURL];
    [Webservices postWithUrlString:strUrl parameters:dictParam success:^(NSDictionary *responce) {
        //Success
        NSLog(@"responce:%@",responce);
        //do code here
        if ([[responce valueForKey:@"status"] isEqualToString:@"1"]) {
            
            NSLog(@"Delete Successful");
        }else{
            NSLog(@"Delete Not Successful");
        }
    } failure:^(NSError *error) {
        //error
        NSLog(@"error:%@",error);
    }];
}

@end
