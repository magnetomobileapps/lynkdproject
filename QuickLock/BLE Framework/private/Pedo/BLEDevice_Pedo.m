//
//  BLEDevice_Pedo.m
//
//  ------------------------------------------------------------------
//  计步器子类实现部分
//  ------------------------------------------------------------------

//  ------------------------------------------------------------------
//  计步器子类的常量定义
//  ------------------------------------------------------------------

#ifndef PEDO_TI_ItonBLEDefines_h
#define PEDO_TI_ItonBLEDefines_h

#define TI_KEYFOB_PEDOMETER_SERVICE_UUID                                   0x1814
#define TI_KEYFOB_PEDOMETER_ACCELENABLE_PROPERTY_UUID                      0xFFA1
#define TI_KEYFOB_PEDOMETER_COUNTENABLE_PROPERTY_UUID                      0xFFA6
#define TI_KEYFOB_PEDOMETER_COUNTENABLE_PROPERTY_DESCRIPTOR_UUID           0x2902
#define TI_KEYFOB_PEDOMETER_COUNTE_LENGTH                                  4

#define TI_KEYFOB_TIME_SERVICE_UUID                                        0x1805
#define TI_KEYFOB_TIME_SYNCHRONOUS_PROPERTY_UUID                           0x2A2B
#define TI_KEYFOB_TIME_LENGTH                                              4

#define TI_KEYFOB_OFFLINE_SERVICE_UUID                                     0xFFC0
#define TI_KEYFOB_OFFLINE_SELECTDAY_PROPERTY_UUID                          0xFFC1
#define TI_KEYFOB_OFFLINE_DATA_PROPERTY_UUID                               0xFFC2
#define TI_KEYFOB_OFFLINE_DATA_LENGTH                                      12

#define TI_KEYFOB_SLEEP_SERVICE_UUID                                       0xFFD0
#define TI_KEYFOB_SLEEP_DATA_PROPERTY_UUID                                 0xFFD1
#define TI_KEYFOB_SLEEP_DATA_LENGTH                                        1

//  2013-11-28 by wangyi 读取电量
#define TI_KEYFOB_BATT_SERVICE_UUID                                        0x180F
#define TI_KEYFOB_BATT_STATE_PROPERTY_UUID                                 0x2A19
#define TI_KEYFOB_BATT_STATE_LENGTH                                        1

#endif

#import "BLEDevice_Pedo.h"
#import "BLEManager.h"

int nOffline;

@implementation BLEDevice_Pedo

@synthesize delegateBLEDeviceResult_Pedo;

-(id)init:(id)delegete
{
    [self setDelegateBLEDeviceResult_Pedo:delegete];
    return [super init:delegete];
}


//  ------------------------------------------------------------------
//  内部私有函数部分
//  ------------------------------------------------------------------

//  硬件数据接收函数 - 重载具体实现部分，每个子类不同
- (void)didUpdateValueForCharacteristic_override:(CBCharacteristic *)characteristic
{
    UInt16 characteristicUUID = [BLEPublic CBUUIDToInt:characteristic.UUID];
    switch(characteristicUUID)
    {
        case TI_KEYFOB_PEDOMETER_COUNTENABLE_PROPERTY_UUID:
        {
            int n;
            
            Byte bAddress[TI_KEYFOB_PEDOMETER_COUNTE_LENGTH];
            [characteristic.value getBytes:&bAddress length:TI_KEYFOB_PEDOMETER_COUNTE_LENGTH];
            int b1,b2,b3,b4;
            b1 = (int)bAddress[3];
            b2 = (int)bAddress[2];
            b3 = (int)bAddress[1];
            b4 = (int)bAddress[0];
            n = b4 | (b3 << 8) | (b2 << 16) | (b1 << 24);
            if (AntiLost_Debug)
            {
                NSLog(@"0xffa6-----: %d,%d,%d,%d,%d",b1,b2,b3,b4,n);
            }
            
            [delegateBLEDeviceResult_Pedo BR_pedoCountUpdated:n DeviceID:DeviceID];
            break;
        }
        case TI_KEYFOB_TIME_SYNCHRONOUS_PROPERTY_UUID:
        {
            int n;
            NSDate *da,*date2000;
            NSDateFormatter *fm;
            Byte bAddress[TI_KEYFOB_TIME_LENGTH];
            
            fm = [[NSDateFormatter alloc]init];
//            NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
//            [fm setTimeZone:timeZone];
            [fm setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
            date2000 = [fm dateFromString:@"2000-01-01 00:00:00"];
            [characteristic.value getBytes:&bAddress length:TI_KEYFOB_TIME_LENGTH];
            int b1,b2,b3,b4;
            b1 = (int)bAddress[0];
            b2 = (int)bAddress[1];
            b3 = (int)bAddress[2];
            b4 = (int)bAddress[3];
            n = b1 | (b2 << 8) | (b3 << 16) | (b4 << 24);
            da = [[NSDate alloc]initWithTimeInterval:n sinceDate:date2000];
            
            if (AntiLost_Debug)
            {
                NSLog(@"0x2A2B-----: %d,%d,%d,%d,%d,%@",b1,b2,b3,b4,n,[fm stringFromDate:da]);
            }
            
            [delegateBLEDeviceResult_Pedo BR_currentTime:da DeviceID:DeviceID];
            break;
        }
        case TI_KEYFOB_OFFLINE_DATA_PROPERTY_UUID:
        {
            int n;
            Byte bAddress[TI_KEYFOB_OFFLINE_DATA_LENGTH];
            NSDate *da,*date2000;
            NSDateFormatter *fm;
            int nTimerecord,nPaceCount;
            
            fm = [[NSDateFormatter alloc]init];
            [fm setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
            date2000 = [fm dateFromString:@"2000-01-01 00:00:00"];
            
            [characteristic.value getBytes:&bAddress length:TI_KEYFOB_OFFLINE_DATA_LENGTH];
            int b1,b2,b3,b4,b5,b6,b7,b8,b9,b10,b11,b12;
            b1 = (int)bAddress[0];
            b2 = (int)bAddress[1];
            b3 = (int)bAddress[2];
            b4 = (int)bAddress[3];
            b5 = (int)bAddress[4];
            b6 = (int)bAddress[5];
            b7 = (int)bAddress[6];
            b8 = (int)bAddress[7];
            b9 = (int)bAddress[8];
            b10 = (int)bAddress[9];
            b11 = (int)bAddress[10];
            b12 = (int)bAddress[11];
            
            n = b1 | (b2 << 8) | (b3 << 16) | (b4 << 24);
            da = [[NSDate alloc]initWithTimeInterval:n sinceDate:date2000];
            
            nTimerecord = b5 | (b6 << 8) | (b7 << 16) | (b8 << 24);
            nPaceCount = b9 | (b10 << 8) | (b11 << 16) | (b12 << 24);
            if (AntiLost_Debug)
            {
                NSLog(@"OfflineData-nTimerecord:%d",nTimerecord);
                NSLog(@"OfflineData-nPaceCount:%d",nPaceCount);
            }
//            if((nTimerecord > 0) && (nPaceCount > 0))
//            {
            [delegateBLEDeviceResult_Pedo BR_pedoReceiveOfflineData:da timerecord:nTimerecord paceCount:nPaceCount DeviceID:DeviceID];
//            }
            
            if (AntiLost_Debug)
            {
                NSLog(@"0xFFC2-----: offlineDate - %d,%d,%d,%d,[%d] timerecord - %d,%d,%d,%d,[%d] paceCount - %d,%d,%d,%d,[%d]",b1,b2,b3,b4,n,b5,b6,b7,b8,nTimerecord,b9,b10,b11,b12,nPaceCount);
            }
            break;
        }
        // 电量读取 2013-11-29
        case TI_KEYFOB_BATT_STATE_PROPERTY_UUID:
        {
            int level;
            Byte bAddress[TI_KEYFOB_BATT_STATE_LENGTH];
            [characteristic.value getBytes:&bAddress length:TI_KEYFOB_BATT_STATE_LENGTH];
            level = bAddress[0];
            [delegateBLEDeviceResult_Pedo BR_batteryLevel:level DeviceID:DeviceID];
            break;
        }
        default:
        {
            if(AntiLost_Debug)
            {
                NSLog(@"didUpdateValueForCharacteristic --- default --- characteristicUUID:%d" , characteristicUUID);
            }
            break;
        }
    }
}

//  ------------------------------------------------------------------
//  协议Protocol_BLEDevice_Pedo的实现部分
//  ------------------------------------------------------------------

-(void)BD_enablePedometer:(BOOL)isEnable
{
    char data;
    
    if (isEnable)
    {
        data = 0x01;
    }
    else
    {
        data = 0x00;
    }
    NSData *d = [[NSData alloc] initWithBytes:&data length:1];
    
    [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID serviceUUID:TI_KEYFOB_PEDOMETER_SERVICE_UUID characteristicUUID:TI_KEYFOB_PEDOMETER_ACCELENABLE_PROPERTY_UUID data:d];
    
    [[BLEManager sharedInstance] notification:DeviceID serviceUUID:TI_KEYFOB_PEDOMETER_SERVICE_UUID characteristicUUID:TI_KEYFOB_PEDOMETER_COUNTENABLE_PROPERTY_UUID on:isEnable];
}

-(void)BD_enableOfflineData:(BOOL)isEnable
{
    [[BLEManager sharedInstance] notification:DeviceID serviceUUID:TI_KEYFOB_OFFLINE_SERVICE_UUID characteristicUUID:TI_KEYFOB_OFFLINE_DATA_PROPERTY_UUID on:isEnable];
}

-(void)BD_enableBatteryLevel:(BOOL)isEnable
{
    [[BLEManager sharedInstance] notification:DeviceID serviceUUID:TI_KEYFOB_BATT_SERVICE_UUID characteristicUUID:TI_KEYFOB_BATT_STATE_PROPERTY_UUID on:isEnable];
}

-(void)BD_setCurrentTime
{
    NSDate *date2000;
    uint time;
    NSDateFormatter *fm;
    char bytes[TI_KEYFOB_TIME_LENGTH];
    
    fm = [[NSDateFormatter alloc]init];
    //    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    //    [fm setTimeZone:timeZone];
    [fm setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    date2000 = [fm dateFromString:@"2000-01-01 00:00:00"];
    time = (int)[[NSDate date] timeIntervalSinceDate:date2000];
    if (AntiLost_Debug)
    {
        NSLog(@"---------------%d",time);
    }
    bytes[0] = time;
    bytes[1] = time >> 8;
    bytes[2] = time >> 16;
    bytes[3] = time >> 24;
    
    NSData *d = [[NSData alloc] initWithBytes:bytes length:TI_KEYFOB_TIME_LENGTH];
    [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID serviceUUID:TI_KEYFOB_TIME_SERVICE_UUID characteristicUUID:TI_KEYFOB_TIME_SYNCHRONOUS_PROPERTY_UUID data:d];
    
    time = (int)[[NSDate date] timeIntervalSinceDate:date2000];
    if (AntiLost_Debug)
    {
        NSLog(@"---------------%d,%d,%d,%d,%d",time,(Byte)bytes[0],(Byte)bytes[1],(Byte)bytes[2],(Byte)bytes[3]);
    }
}

-(void)BD_getCurrentTime
{
    [[BLEManager sharedInstance] readValueForCharacteristic:DeviceID serviceUUID:TI_KEYFOB_TIME_SERVICE_UUID characteristicUUID:TI_KEYFOB_TIME_SYNCHRONOUS_PROPERTY_UUID];
}

-(void)BD_clearOfflineData
{
    char data = 0xee;
    NSData *d = [[NSData alloc] initWithBytes:&data length:1];
    
    [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID serviceUUID:TI_KEYFOB_OFFLINE_SERVICE_UUID characteristicUUID:TI_KEYFOB_OFFLINE_SELECTDAY_PROPERTY_UUID data:d];
}

-(void)BD_sleepPeripheral
{
    char data = 0x06;
    NSData *d = [[NSData alloc] initWithBytes:&data length:TI_KEYFOB_SLEEP_DATA_LENGTH];
    
    [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID serviceUUID:TI_KEYFOB_SLEEP_SERVICE_UUID characteristicUUID:TI_KEYFOB_SLEEP_DATA_PROPERTY_UUID data:d];
}

@end
