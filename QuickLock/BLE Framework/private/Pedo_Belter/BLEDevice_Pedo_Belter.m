//
//  BLEDevice_Pedo_Belter.m
//
//  ------------------------------------------------------------------
//  计步器子类实现部分 - 倍泰带屏计步器
//  ------------------------------------------------------------------

//  ------------------------------------------------------------------
//  计步器子类的常量定义 - 倍泰带屏计步器
//  ------------------------------------------------------------------
#ifndef AntiLost_TI_ItonBLEDefines_h
#define AntiLost_TI_ItonBLEDefines_h

#define TI_KEYFOB_PEDOMETER_SERVICE_UUID                                    0x1814
#define TI_KEYFOB_PEDOMETER_ACCELENABLE_PROPERTY_UUID                       0xFFA1
#define TI_KEYFOB_PEDOMETER_COUNTENABLE_PROPERTY_UUID                       0xFFA6
#define TI_KEYFOB_PEDOMETER_COUNTENABLE_PROPERTY_DESCRIPTOR_UUID            0x2902
#define TI_KEYFOB_PEDOMETER_COUNTE_LENGTH                                   4

#define TI_KEYFOB_TIME_SERVICE_UUID                                         0x1805
#define TI_KEYFOB_TIME_SYNCHRONOUS_PROPERTY_UUID                            0x2A2B
#define TI_KEYFOB_TIME_LENGTH                                               4

#define TI_KEYFOB_OFFLINE_SERVICE_UUID                                      0xFFC0
#define TI_KEYFOB_OFFLINE_SELECTDAY_PROPERTY_UUID                           0xFFC1
#define TI_KEYFOB_OFFLINE_DATA_PROPERTY_UUID                                0xFFC2
#define TI_KEYFOB_OFFLINE_DATAO2_PROPERTY_UUID                              0xFFC3

#define TI_KEYFOB_OFFLINE_DATA_LENGTH                                       12

//#define TI_KEYFOB_SLEEP_SERVICE_UUID                                        0xFFD0
//#define TI_KEYFOB_SLEEP_DATA_PROPERTY_UUID                                  0xFFD1
#define TI_KEYFOB_SLEEP_DATA_LENGTH                                         1

#define BELTER_SLEEP_MONITOR_SERVICE_UUID                                   0xFF70
#define BELTER_SLEEP_MONITOR_DATALENGTH_UUID                                0xFF71
#define BELTER_SLEEP_MONITOR_DATA_PROPERTY_UUID                             0xFF72

#define BELTER_USERINFO_SERVICE_UUID                                        0xFF80
#define BELTER_USERINFO_HEIGHT_UUID                                         0xFF81
#define BELTER_USERINFO_WEIGHT_UUID                                         0xFF82
#define BELTER_USERINFO_STEP_UUID                                           0xFF83
#define BELTER_USERINFO_LENGTH                                              2

#define BELTER_CUSTOMER_SERVICE_UUID                                        0xFFD0
#define BELTER_CUSTOMER_PARAMETER_PROPERTY_UUID                             0xFFD1
#define BELTER_CUSTOMER_PARAMETER_LENGTH                                    11

#endif


#import "BLEDevice_Pedo_Belter.h"
#import "BLEManager.h"

int nOffline;
int offlineCount;
//int seelpMonitorDataLength;
Byte sleepData[100];
int recDataCount;

@implementation BLEDevice_Pedo_Belter

@synthesize delegateBLEDeviceResult_Pedo_Belter;
//@synthesize ItonTestProtocolDelegate;

-(id)init:(id)delegete
{
        [self setDelegateBLEDeviceResult_Pedo_Belter:delegete];
//        ItonTestProtocolDelegate = delegete;
        return [super init:delegete];
}

//  ------------------------------------------------------------------
//  内部私有函数部分
//  ------------------------------------------------------------------

//  硬件数据接收函数 - 重载具体实现部分，每个子类不同
- (void)didUpdateValueForCharacteristic_override:(CBCharacteristic *)characteristic
{
        UInt16 characteristicUUID = [BLEPublic CBUUIDToInt:characteristic.UUID];
        switch(characteristicUUID)
        {
                case TI_KEYFOB_PEDOMETER_COUNTENABLE_PROPERTY_UUID:
                {
                        int n;
                        
                        Byte bAddress[TI_KEYFOB_PEDOMETER_COUNTE_LENGTH];
                        [characteristic.value getBytes:&bAddress length:TI_KEYFOB_PEDOMETER_COUNTE_LENGTH];
                        int b1,b2,b3,b4;
                        b1 = (int)bAddress[3];
                        b2 = (int)bAddress[2];
                        b3 = (int)bAddress[1];
                        b4 = (int)bAddress[0];
                        n = b4 | (b3 << 8) | (b2 << 16) | (b1 << 24);
                        if (AntiLost_Debug)
                        {
                                NSLog(@"0xffa6-----: %d,%d,%d,%d,%d",b1,b2,b3,b4,n);
                        }
                        
                        [delegateBLEDeviceResult_Pedo_Belter pedoCountUpdated:n DeviceID:DeviceID];
                        
//                        NSString *s1,*s2,*s3,*s4;
//                        s1 = [NSString stringWithFormat:@"%d", b1];
//                        s2 = [NSString stringWithFormat:@"%d", b2];
//                        s3 = [NSString stringWithFormat:@"%d", b3];
//                        s4 = [NSString stringWithFormat:@"%d", b4];
//                        NSArray *arrData = [NSArray arrayWithObjects: s1,s2,s3,s4, nil];
//                        
//                        [ItonTestProtocolDelegate deviceData_UUID:characteristicUUID DATA:arrData];
                        break;
                }
                case TI_KEYFOB_TIME_SYNCHRONOUS_PROPERTY_UUID:
                {
                        int n;
                        NSDate *da,*date2000;
                        NSDateFormatter *fm;
                        Byte bAddress[TI_KEYFOB_TIME_LENGTH];
                        
                        fm = [[NSDateFormatter alloc]init];
                        //            NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
                        //            [fm setTimeZone:timeZone];
                        [fm setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
                        date2000 = [fm dateFromString:@"2000-01-01 00:00:00"];
                        [characteristic.value getBytes:&bAddress length:TI_KEYFOB_TIME_LENGTH];
                        int b1,b2,b3,b4;
                        b1 = (int)bAddress[0];
                        b2 = (int)bAddress[1];
                        b3 = (int)bAddress[2];
                        b4 = (int)bAddress[3];
                        n = b1 | (b2 << 8) | (b3 << 16) | (b4 << 24);
                        da = [[NSDate alloc]initWithTimeInterval:n sinceDate:date2000];
                        
                        if (AntiLost_Debug)
                        {
                                NSLog(@"0x2A2B-----: %d,%d,%d,%d,%d,%@",b1,b2,b3,b4,n,[fm stringFromDate:da]);
                        }
                        //[[self ItonPedometerProtocolDelegate] pedoCountUpdated:index pedoCountValue:n];
                        break;
                }
                case TI_KEYFOB_OFFLINE_DATA_PROPERTY_UUID:
                {
                        
                        int n;
                        Byte bAddress[16];
                        NSDate *da,*date2000;
                        NSDateFormatter *fm;
                        int nTimerecord,nPaceCount,nPaceCountO2;
                        
                        fm = [[NSDateFormatter alloc]init];
                        [fm setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
                        date2000 = [fm dateFromString:@"2000-01-01 00:00:00"];
                        
                        [characteristic.value getBytes:&bAddress length:16];
                        int b1,b2,b3,b4,b5,b6,b7,b8,b9,b10,b11,b12,b13,b14,b15,b16;
                        b1 = (int)bAddress[0];
                        b2 = (int)bAddress[1];
                        b3 = (int)bAddress[2];
                        b4 = (int)bAddress[3];
                        b5 = (int)bAddress[4];
                        b6 = (int)bAddress[5];
                        b7 = (int)bAddress[6];
                        b8 = (int)bAddress[7];
                        b9 = (int)bAddress[8];
                        b10 = (int)bAddress[9];
                        b11 = (int)bAddress[10];
                        b12 = (int)bAddress[11];
                        b13 = (int)bAddress[12];
                        b14 = (int)bAddress[13];
                        b15 = (int)bAddress[14];
                        b16 = (int)bAddress[15];
                        n = b1 | (b2 << 8) | (b3 << 16) | (b4 << 24);
                        da = [[NSDate alloc]initWithTimeInterval:n sinceDate:date2000];
                        
                        nTimerecord = b5 | (b6 << 8) | (b7 << 16) | (b8 << 24);
                        
                        nPaceCount = b9 | (b10 << 8) | (b11 << 16) | (b12 << 24);
                        
                        nPaceCountO2 = b13 | (b14 << 8) | (b15 << 16) | (b16 << 24);
                        
                        //            if((nTimerecord > 0) && (nPaceCount > 0))
                        //            {
                        [delegateBLEDeviceResult_Pedo_Belter pedoReceiveOfflineData:da
                                                                         timerecord:nTimerecord
                                                                          paceCount:nPaceCount
                                                                       paceConuntO2:nPaceCountO2
                                                                           DeviceID:(int)DeviceID];
                        //            }
                        
                        if (AntiLost_Debug)
                        {
                                NSLog(@"0xFFC2-----: offlineDate - %d,%d,%d,%d,[%d] timerecord - %d,%d,%d,%d,[%d] paceCount - %d,%d,%d,%d,[%d]",b1,b2,b3,b4,n,b5,b6,b7,b8,nTimerecord,b9,b10,b11,b12,nPaceCount);
                        }
                        break;
                }
                case TI_KEYFOB_OFFLINE_DATAO2_PROPERTY_UUID:
                {
                        //            NSString *strCount = [NSString stringWithFormat:@"%d",offlineCount++];
                        //            NSLog(@"offlineO2: %@",strCount);
                        //
                        //            Byte bAddress[4];
                        //            int nPaceCount;
                        //
                        //            [characteristic.value getBytes:&bAddress length:4];
                        //            int b1,b2,b3,b4;
                        //            b1 = (int)bAddress[0];
                        //            b2 = (int)bAddress[1];
                        //            b3 = (int)bAddress[2];
                        //            b4 = (int)bAddress[3];
                        //
                        //            nPaceCount = b1 | (b2 << 8) | (b3 << 16) | (b4 << 24);
                        //
                        //            [[self ItonPedometerProtocolDelegate] pedoReceiveOfflineDataO2:index paceCount:nPaceCount];
                        //
                        //            if (AntiLost_Debug)
                        //            {
                        //                NSLog(@"0xFFC3-----: paceCount - %d,%d,%d,%d,[%d]",b1,b2,b3,b4,nPaceCount);
                        //            }
                        
                        break;
                }
                case BELTER_SLEEP_MONITOR_DATALENGTH_UUID:
                {
                        Byte bAddress[1];
                        [characteristic.value getBytes:&bAddress length:1];
                        [delegateBLEDeviceResult_Pedo_Belter getSleepMonitorDataLength:bAddress[0] DeviceID:(int)DeviceID];
                        //seelpMonitorDataLength = bAddress[0];
                        [self enableSleepMonitorDataInfo:YES];
                        
                        if (AntiLost_Debug)
                        {
                                NSLog(@"0xFF71-----: SLEEP_MONITOR_DATALENGTH - %d",bAddress[0]);
                                
//                                NSString *s1;
//                                s1 = [NSString stringWithFormat:@"%d", bAddress[0]/8];
//                                NSArray *arrData = [NSArray arrayWithObjects: s1,nil];
//                                [ItonTestProtocolDelegate deviceData_UUID:characteristicUUID DATA:arrData];
                        }
                        break;
                }
                case BELTER_SLEEP_MONITOR_DATA_PROPERTY_UUID:
                {
                        int n;
                        
                        Byte bAddress[20];
                        [characteristic.value getBytes:&bAddress];
                        
                        //            NSString *strAddress;
                        
                        //            for (int n = 0; n < 20; n++) {
                        //                strAddress = [NSString stringWithFormat:@"%@ %02x",strAddress,bAddress[n]];
                        //            }
                        //
                        //            NSLog(@"*******bAddress: %@",strAddress);
                        
                        for (int i = 0; i < 20; i++) {
                                sleepData[recDataCount + i] = bAddress[i];
                        }
                        
                        recDataCount += 20;
                        
                        NSDate *onBed,*weakUp,*date2000;
                        NSDateFormatter *fm;
                        
                        
                        if(recDataCount == 100)
                        {
                                fm = [[NSDateFormatter alloc]init];
                                
                                [fm setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
                                date2000 = [fm dateFromString:@"2000-01-01 00:00:00"];
                                
                                n = sleepData[0] | (sleepData[1] << 8) | (sleepData[2] << 16) | (sleepData[3] << 24);
                                onBed = [[NSDate alloc]initWithTimeInterval:n sinceDate:date2000];
                                
                                n = sleepData[4] | (sleepData[5] << 8) | (sleepData[6] << 16) | (sleepData[7] << 24);
                                weakUp = [[NSDate alloc]initWithTimeInterval:n sinceDate:date2000];
                                
                                int slData[184];
                                
                                for (int i = 0; i<46; i++) {
                                        Byte b1,b2;
                                        b1 = sleepData[8+i*2];
                                        b2 = sleepData[9+i*2];
                                        
                                        int hour = (Byte)(b1 & 0x1F);
                                        slData[i*4] = hour;
                                        
                                        int count = (Byte)(b1 >> 5);
                                        slData[i*4+1] = count;
                                        
                                        int minute = (Byte)(b2 & 0x3F);
                                        slData[i*4+2] = minute;
                                        
                                        int type = (Byte)(b2 >> 6);
                                        slData[i*4+3] = type;
                                }
                                
                                NSString *str_SourceData;
                                for (int i = 0; i < 100; i++) {
                                        NSString *str = [NSString stringWithFormat:@"%x", (int)sleepData[i]];
                                        str_SourceData = [str_SourceData stringByAppendingString:str];
                                }
                                
                                [delegateBLEDeviceResult_Pedo_Belter getSleepMonitorDataInfo:onBed
                                                                                      weakUp:weakUp
                                                                                 sleepRecord:slData
                                                                                  sourceData:str_SourceData
                                                                                    DeviceID:(int)DeviceID];
                                
                                recDataCount = 0;
                        }
                        break;
                }
                        
                default:
                {
                        if (AntiLost_Debug)
                        {
                                NSLog(@"didUpdateValueForCharacteristic --- default --- characteristicUUID:%d" , characteristicUUID);
                        }
                        break;
                }
        }
}

//  ------------------------------------------------------------------
//  协议Protocol_BLEDevice_Pedo_Belter的实现部分
//  ------------------------------------------------------------------

-(void)enablePedometer:(BOOL)isEnable
{
        char data;
        
        if (isEnable)
        {
                data = 0x01;
        }
        else
        {
                data = 0x00;
        }
        NSData *d = [[NSData alloc] initWithBytes:&data length:TI_KEYFOB_SLEEP_DATA_LENGTH];
        
        [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID serviceUUID:TI_KEYFOB_PEDOMETER_SERVICE_UUID characteristicUUID:TI_KEYFOB_PEDOMETER_ACCELENABLE_PROPERTY_UUID data:d];
        
        [[BLEManager sharedInstance] notification:DeviceID serviceUUID:TI_KEYFOB_PEDOMETER_SERVICE_UUID characteristicUUID:TI_KEYFOB_PEDOMETER_ACCELENABLE_PROPERTY_UUID on:isEnable];
        
        if (isEnable)
        {
                offlineCount = 0;
        }
}

-(void)enableOfflineData:(BOOL)isEnable
{
        [[BLEManager sharedInstance] notification:DeviceID serviceUUID:TI_KEYFOB_OFFLINE_SERVICE_UUID characteristicUUID:TI_KEYFOB_OFFLINE_DATA_PROPERTY_UUID on:isEnable];
}

- (void)enableOfflineDataO2:(BOOL)isEnable
{
        [[BLEManager sharedInstance] notification:DeviceID serviceUUID:TI_KEYFOB_OFFLINE_SERVICE_UUID characteristicUUID:TI_KEYFOB_OFFLINE_DATAO2_PROPERTY_UUID on:isEnable];
}

-(void)enableSleepMonitorDataLength
{
        [[BLEManager sharedInstance] notification:DeviceID serviceUUID:BELTER_SLEEP_MONITOR_SERVICE_UUID characteristicUUID:BELTER_SLEEP_MONITOR_DATALENGTH_UUID on:YES];
        
        [[BLEManager sharedInstance] readValueForCharacteristic:DeviceID serviceUUID:BELTER_SLEEP_MONITOR_SERVICE_UUID characteristicUUID:BELTER_SLEEP_MONITOR_DATALENGTH_UUID];
}

-(void)enableSleepMonitorDataInfo:(BOOL)isEnable
{
        [[BLEManager sharedInstance] notification:DeviceID serviceUUID:BELTER_SLEEP_MONITOR_SERVICE_UUID characteristicUUID:BELTER_SLEEP_MONITOR_DATA_PROPERTY_UUID on:isEnable];
}

-(void)setCurrentTime
{
        NSDate *date2000;
        uint time;
        NSDateFormatter *fm;
        char bytes[TI_KEYFOB_TIME_LENGTH];
        
        fm = [[NSDateFormatter alloc]init];
        //    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
        //    [fm setTimeZone:timeZone];
        [fm setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
        date2000 = [fm dateFromString:@"2000-01-01 00:00:00"];
        time = (int)[[NSDate date] timeIntervalSinceDate:date2000];
        if (AntiLost_Debug)
        {
                NSLog(@"---------------%d",time);
        }
        bytes[0] = time;
        bytes[1] = time >> 8;
        bytes[2] = time >> 16;
        bytes[3] = time >> 24;
        
        NSData *d = [[NSData alloc] initWithBytes:bytes length:TI_KEYFOB_TIME_LENGTH];
        [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID serviceUUID:TI_KEYFOB_TIME_SERVICE_UUID characteristicUUID:TI_KEYFOB_TIME_SYNCHRONOUS_PROPERTY_UUID data:d];
        
        time = (int)[[NSDate date] timeIntervalSinceDate:date2000];
        if (AntiLost_Debug)
        {
                NSLog(@"---------------%d,%d,%d,%d,%d",time,(Byte)bytes[0],(Byte)bytes[1],(Byte)bytes[2],(Byte)bytes[3]);
        }
}

-(void)getCurrentTime
{
        [[BLEManager sharedInstance] readValueForCharacteristic:DeviceID serviceUUID:TI_KEYFOB_TIME_SERVICE_UUID characteristicUUID:TI_KEYFOB_TIME_SYNCHRONOUS_PROPERTY_UUID];
}

-(void)clearOfflineData
{
        char data = 0xee;
        NSData *d = [[NSData alloc] initWithBytes:&data length:TI_KEYFOB_SLEEP_DATA_LENGTH];
        [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID serviceUUID:TI_KEYFOB_OFFLINE_SERVICE_UUID characteristicUUID:TI_KEYFOB_OFFLINE_SELECTDAY_PROPERTY_UUID data:d];
}

//-(void)sleepPeripheral
//{
//        char data = 0x06;
//        NSData *d = [[NSData alloc] initWithBytes:&data length:TI_KEYFOB_SLEEP_DATA_LENGTH];
//        [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID serviceUUID:TI_KEYFOB_SLEEP_SERVICE_UUID characteristicUUID:TI_KEYFOB_SLEEP_DATA_PROPERTY_UUID data:d];
//}

-(void)setUserHeight:(int)value
{
        Byte high = value >> 8;
        Byte low = value;
        char data[BELTER_USERINFO_LENGTH];
        data[1] = (int)high;
        data[0] = (int)low;
        NSData *d = [[NSData alloc] initWithBytes:data length:BELTER_USERINFO_LENGTH];
        [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID serviceUUID:BELTER_USERINFO_SERVICE_UUID characteristicUUID:BELTER_USERINFO_HEIGHT_UUID data:d];
}

-(void)setUserWeight:(int)value
{
        Byte high = value >> 8;
        Byte low = value;
        char data[BELTER_USERINFO_LENGTH];
        data[1] = (int)high;
        data[0] = (int)low;
        NSData *d = [[NSData alloc] initWithBytes:data length:BELTER_USERINFO_LENGTH];
        [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID serviceUUID:BELTER_USERINFO_SERVICE_UUID characteristicUUID:BELTER_USERINFO_WEIGHT_UUID data:d];
}

-(void)setUserStep:(int)value
{
        Byte high = value >> 8;
        Byte low = value;
        char data[BELTER_USERINFO_LENGTH];
        data[1] = (int)high;
        data[0] = (int)low;
        NSData *d = [[NSData alloc] initWithBytes:data length:BELTER_USERINFO_LENGTH];
        [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID serviceUUID:BELTER_USERINFO_SERVICE_UUID characteristicUUID:BELTER_USERINFO_STEP_UUID data:d];
}

-(void)BD_setParameter:(int)units sex:(int)sex age:(int)age height:(double)height weight:(double)weight stride:(double)stride nAlarmKind:(int)nAlarmKind tCalories:(double)tCalories tDistance:(double)tDistance tPaceCount:(double)tPaceCount tCostTime:(double)tCostTime
{
//        （1个字节）单位制(0:公制；1:英制)
//        （1个字节）性别(0:女；1：男)
//        （1个字节）年龄(1-254)(单位：岁)
//        （1个字节）身高(1-254)（单位：1cm）？？注意：与原定义2字节的BELTER_USERINFO_HEIGHT_UUID不符！！
//        （2个字节）体重(1-65534)（单位：0.1Kg）(低字节在前)
//        （2个字节）步长(1-65534)（单位：0.1cm）(低字节在前)
//        （1个字节）目标项(1-4)
//        （2个字节）目标值(1-65534)(低字节在前)

        char bytes[BELTER_CUSTOMER_PARAMETER_LENGTH];
        double _temp;
        
        if (![self BD_getIsConnected])
        {
                return;
        }
        
        //  1 Byte:单位制
        bytes[0] = units;
        
        //  1 Byte:性别
        bytes[1] = sex;
        
        //  1 Byte:年龄
        bytes[2] = age;
        
        //  1 Byte:身高  //？？注意：与原定义2字节的BELTER_USERINFO_HEIGHT_UUID不符！！
        bytes[3] = (int)(height * 100);
        
        //  2 Byte:体重
        bytes[4] = (int)(weight * 10);
        bytes[5] = (int)(weight * 10) >> 8;
        
        //  2 Byte:步长
        bytes[6] = (int)(stride * 1000);
        bytes[7] = (int)(stride * 1000) >> 8;
        
        //  1 byte:目标项
        bytes[8] = nAlarmKind;
        
        //  2 byte:目标值
        switch (nAlarmKind)
        {
                case 1:
                        _temp = tCalories;
                        break;
                        
                case 2:
                        _temp = tDistance;
                        break;
                        
                case 3:
                        _temp = tPaceCount;
                        break;
                        
                case 4:
                        _temp = tCostTime;
                        break;
                        
                default:
                        return;
        }
        bytes[9] = (int)_temp;
        bytes[10] = (int)_temp >> 8;
        
        NSData *d = [[NSData alloc] initWithBytes:bytes length:BELTER_CUSTOMER_PARAMETER_LENGTH];
        [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID serviceUUID:BELTER_CUSTOMER_SERVICE_UUID characteristicUUID:BELTER_CUSTOMER_PARAMETER_PROPERTY_UUID data:d];
}
@end
