//
//  BLEDevice_AFan.m
//
//  ------------------------------------------------------------------
//  艾美特风扇子类的实现部分
//  ------------------------------------------------------------------
//
//  ------------------------------------------------------------------
//  艾美特子类的常量定义
//  ------------------------------------------------------------------

#ifndef AFan_BC4_ItonBLEDefines_h
#define AFan_BC4_ItonBLEDefines_h

#define TI_KEYFOB_SERVICE_UUID                                     0xFFC0
#define TI_KEYFOB_NOTIFY_WRITE_DATA_PROPERTY_UUID                  0xFFC3
#define TI_KEYFOB_DATA_LENGTH                                      16

#endif

#import "BLEDevice_AFan.h"
#import "BLEManager.h"

@implementation BLEDevice_AFan
@synthesize delegateBLEDeviceResult_AFan;

NSDate *lastDate;

-(id)init:(id)delegete
{
        [self setDelegateBLEDeviceResult_AFan:delegete];
        return [super init:delegete];
}

//  ------------------------------------------------------------------
//  内部私有函数部分
//  ------------------------------------------------------------------

//  Hardware data receiver function - Overload concrete implementation part, different for each subclass
- (void)didUpdateValueForCharacteristic_override:(CBCharacteristic *)characteristic
{
        UInt16 characteristicUUID = [BLEPublic CBUUIDToInt:characteristic.UUID];
        switch(characteristicUUID)
        {
                case TI_KEYFOB_NOTIFY_WRITE_DATA_PROPERTY_UUID:
                {
                        Byte bAddress[TI_KEYFOB_DATA_LENGTH];
                        [characteristic.value getBytes: &bAddress length: TI_KEYFOB_DATA_LENGTH];
                        
                        [delegateBLEDeviceResult_AFan BR_receiveBytes:bAddress length:TI_KEYFOB_DATA_LENGTH DeviceID:DeviceID];
                        break;
                }
                default:
                {
                        if(AntiLost_Debug)
                        {
                                NSLog(@"didUpdateValueForCharacteristic --- default --- characteristicUUID:%d" , characteristicUUID);
                        }
                        break;
                }
        }
}

//  发送14个字节的数据
-(void)sendData:(Byte*)bytes
{
        NSData *_data;
        Byte bAddress[14];
        int i;
        
        //  取出字节
        for (i = 0 ; i < 14; i++)
        {
                bAddress[i] = bytes[i];
        }

        _data = [[NSData alloc] initWithBytes: bAddress length: 14];
        [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID serviceUUID:TI_KEYFOB_SERVICE_UUID characteristicUUID:TI_KEYFOB_NOTIFY_WRITE_DATA_PROPERTY_UUID data:_data];
}

//  ------------------------------------------------------------------
//  协议Protocol_BLEDevice_AFan的实现部分
//  ------------------------------------------------------------------

-(void)BD_enableTransparentData:(BOOL)isEnable
{
        [[BLEManager sharedInstance] notification:DeviceID serviceUUID:TI_KEYFOB_SERVICE_UUID characteristicUUID:TI_KEYFOB_NOTIFY_WRITE_DATA_PROPERTY_UUID on:isEnable];
}

-(void)BD_writeData:(int)b0 byte1:(int)b1 byte2:(int)closehour byte3:(int)reservehour byte4:(int)b4 byte5:(int)closeminute byte6:(int)reserveminute byte13:(int)b13
{
        Byte _byte[14] =
        {
                b0,
                b1,
                closehour,
                reservehour,
                b4,
                closeminute,
                reserveminute,
                0,
                0,
                0,
                0,
                0,
                0,
                b13
        };
        [self sendData:_byte];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateStyle: NSDateFormatterMediumStyle];
        [dateFormatter setTimeStyle: NSDateFormatterShortStyle];
        [dateFormatter setDateFormat: @"yyyy-MM-dd HH:mm:ss.SSS"];
        lastDate = [NSDate date];
}

-(void)BD_writeTimeData:(int)b0 byte1:(int)b1 byte2:(int)b2 byte3:(int)b3 byte4:(int)b4 byte5:(int)b5 byte6:(int)b6 byte13:(int)b13
{
        Byte _byte[14] =
        {
                b0,
                b1,
                b2,
                b3,
                b4,
                b5,
                b6,
                0,
                0,
                0,
                0,
                0,
                0,
                b13
        };
        [self sendData:_byte];
}

-(void)BD_writeDefineData:(int)b0 byte1:(int)b1 byte2:(int)b2 byte3:(int)b3 byte4:(int)b4 byte5:(int)b5 byte6:(int)b6 byte7:(int)b7 byte8:(int)b8 byte9:(int)b9 byte10:(int)b10 byte11:(int)b11 byte12:(int)b12 byte13:(int)b13
{
        Byte _byte[14] =
        {
                b0,
                b1,
                b2,
                b3,
                b4,
                b5,
                b6,
                b7,
                b8,
                b9,
                b10,
                b11,
                b12,
                b13
        };
        [self sendData:_byte];
}

@end
