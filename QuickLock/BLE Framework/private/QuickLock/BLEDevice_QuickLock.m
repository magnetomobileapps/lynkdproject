//
//  BLEDevice_QuickLock.m
//
//  ------------------------------------------------------------------
//  车锁子类的实现部分
//  The realization of the car lock subclass
//  ------------------------------------------------------------------

//  ------------------------------------------------------------------
//  车锁子类的常量定义
//  Subclass constant defines the lock
//  ------------------------------------------------------------------

#ifndef QuickLock_BC4_ItonBLEDefines_h
#define QuickLock_BC4_ItonBLEDefines_h

#define QuickLock_TI_KEYFOB_VERSION_SERVICE_UUID                                     0x180A
#define QuickLock_TI_KEYFOB_SYSTEM_ID_PROPERTY_UUID                                  0x2A23
#define QuickLock_TI_KEYFOB_SYSTEM_ID_LENGTH                                         8
#define QuickLock_TI_KEYFOB_MODEL_NUMBER_PROPERTY_UUID                               0x2A24
#define QuickLock_TI_KEYFOB_MODEL_NUMBER_LENGTH                                      16
#define QuickLock_TI_KEYFOB_FIRMWARE_REVISION_PROPERTY_UUID                          0x2A26
#define QuickLock_TI_KEYFOB_FIRMWARE_REVISION_LENGTH                                 7
#define QuickLock_TI_KEYFOB_PROTOCOL_VERSION_PROPERTY_UUID                           0xFF02
#define QuickLock_TI_KEYFOB_PROTOCOL_VERSION_LENGTH                                  2

#define QuickLock_TI_KEYFOB_BATT_SERVICE_UUID                                        0x180F
#define QuickLock_TI_KEYFOB_BATT_STATE_PROPERTY_UUID                                 0x2A19
#define QuickLock_TI_KEYFOB_BATT_STATE_LENGTH                                        1

#define QuickLock_TI_KEYFOB_TIME_SERVICE_UUID                                        0x1805
#define QuickLock_TI_KEYFOB_TIME_SYNCHRONOUS_PROPERTY_UUID                           0x2A2B
#define QuickLock_TI_KEYFOB_TIME_LENGTH                                              4

#define QuickLock_TI_KEYFOB_CUSTOMER_SERVICE_UUID                                    0xFFD0
#define QuickLock_TI_KEYFOB_CUSTOMER_UNLOCK_PASSWORD_PROPERTY_UUID                   0xFFD6
#define QuickLock_TI_KEYFOB_CUSTOMER_UNLOCK_PASSWORD_LENGTH                          9
#define QuickLock_TI_KEYFOB_CUSTOMER_UNLOCK_PASSWORDRESULT_PROPERTY_UUID             0xFFD7
#define QuickLock_TI_KEYFOB_CUSTOMER_UNLOCK_PASSWORDRESULT_LENGTH                    1
#define QuickLock_TI_KEYFOB_CUSTOMER_UNLOCK_OPENTIME_PROPERTY_UUID                   0xFFD8
#define QuickLock_TI_KEYFOB_CUSTOMER_UNLOCK_OPENTIME_LENGTH                          1
#define QuickLock_TI_KEYFOB_CUSTOMER_UNLOCK_LOCKCONTROL_PROPERTY_UUID                0xFFD9
#define QuickLock_TI_KEYFOB_CUSTOMER_UNLOCK_LOCKCONTROL_LENGTH                       1
#define QuickLock_TI_KEYFOB_CUSTOMER_UNLOCK_STATE_PROPERTY_UUID                      0xFFDA
#define QuickLock_TI_KEYFOB_CUSTOMER_UNLOCK_STATE_LENGTH                             1
#define QuickLock_TI_KEYFOB_CUSTOMER_UNLOCK_LED_LIGHT                                0xFFDB
#define QuickLock_TI_KEYFOB_CUSTOMER_UNLOCK_SURE_SET_MAIN                            0xFFC0
#define QuickLock_TI_KEYFOB_CUSTOMER_UNLOCK_SURE_SET                                 0xFFC2
#define QuickLock_TI_KEYFOB_CUSTOMER_UNLOCK_ALARM_SET                                0xFFDC
#define QuickLock_TI_KEYFOB_CUSTOMER_UNLOCK_ALARM_LENGTH                             2
#define QuickLock_TI_KEYFOB_FINGERPRINT_READ_COUNT                                   0xFFC1
#define QuickLock_TI_KEYFOB_FINGERPRINT_READ_LENGTH                                  1
#define QuickLock_TI_KEYFOB_FINGERPRINT_CLEAR_ALL                                    0xFFC5
#define QuickLock_TI_KEYFOB_FINGERPRINT_LEARN                                        0xFFC3
#define QuickLock_TI_KEYFOB_FINGERPRINT_LEARN_LENGTH                                 1
#define QuickLock_TI_KEYFOB_SET_KEYCODE                                              0xFFDD

#define QuickLock_TI_KEYFOB_HISTORY_SERVICE_UUID                                     0xFFF0
#define QuickLock_TI_KEYFOB_HISTORY_CONTROL_PROPERTY_UUID                            0xFFF1
#define QuickLock_TI_KEYFOB_HISTORY_CONTROL_LENGTH                                   1
#define QuickLock_TI_KEYFOB_HISTORY_UPDATE_PROPERTY_UUID                             0xFFF2
#define QuickLock_TI_KEYFOB_HISTORY_PHONE_ID_PROPERTY_UUID                           0xFFF3
#define QuickLock_TI_KEYFOB_HISTORY_PHONE_ID_LENGTH                                  15

#define QuickLock_TI_KEYFOB_RFID_CONTROL_LENGTH                                      1
#define QuickLock_TI_KEYFOB_RFID_PHONE_ID_LENGTH                                     8
#define QuickLock_TI_KEYFOB_RFID_SERVICE_UUID                                        0xFFE0
#define QuickLock_TI_KEYFOB_RFID_CONTROL_PROPERTY_UUID                               0xFFE1
#define QuickLock_TI_KEYFOB_RFID_UPDATE_PROPERTY_UUID                                0xFFE1
#define QuickLock_TI_KEYFOB_RFID_PHONE_ID_PROPERTY_UUID                              0xFFE3
#define QuickLock_TI_KEYFOB_RFID_ADD_UUID                                            0xFFE4

#define QuickLock_TI_KEYFOB_SHARECODE_MAIN                                           0xFFE0
#define QuickLock_TI_KEYFOB_SHARECODE_COUNT_LENGTH                                   5
#define QuickLock_TI_KEYFOB_SHARECODE_COUNT                                          0xFFE8
#define QuickLock_TI_KEYFOB_SHARECODE_TIME_LENGTH                                    17
#define QuickLock_TI_KEYFOB_SHARECODE_TIME                                           0xFFE9

#define QuickLock_TI_KEYFOB_SHARECODE_ZWAVE_MAIN                                     0xFFB0
#define QuickLock_TI_KEYFOB_SHARECODE_ZWAVE_LENGTH                                   1
#define QuickLock_TI_KEYFOB_SHARECODE_ZWAVE                                          0xFFB2

#define QuickLock_TI_KEYFOB_WIFI_SERVICE_UUID                                        0xFFA0
#define QuickLock_TI_KEYFOB_WIFI_MAC_ADDRESS                                         0xFFA1
#define QuickLock_TI_KEYFOB_WIFI_MAC_ADDRESS_LENGTH                                  6
#define QuickLock_TI_KEYFOB_WIFI_FIND_SSIDS                                          0xFFA2
#define QuickLock_TI_KEYFOB_WIFI_FIND_SSIDS_MIN_LENGTH                               4
#define QuickLock_TI_KEYFOB_WIFI_FIND_SSIDS_MAX_LENGTH                               35
#define QuickLock_TI_KEYFOB_WIFI_JOIN_SSID                                           0xFFA3
#define QuickLock_TI_KEYFOB_WIFI_JOIN_SSID_LENGTH                                    98     // Can be changed to replace with MAC Address
#define QuickLock_TI_KEYFOB_WIFI_CONNECTION_TEST                                     0xFFA4
#define QuickLock_TI_KEYFOB_WIFI_CONNECTION_TEST_LENGTH                              1

// New code for Add multiple RFID - Pavan
#define QuickLock_TI_AaddMultiple_RFID_SERVICE_UUID                                  0xFFD0
#define QuickLock_TI_AaddMultiple_RFID_UserId_LENGTH                                  20
#define QuickLock_TI_AaddMultiple_RFID_UserId_Characteristics_UUID                   0xFFDB
#define QuickLock_TI_WriteControl_RFID_ADD_Multiple_UUID                             0xFFDC
#define QuickLock_TI_Write_Status_RFID_UUID                                          0xFFDD

#endif

#import "BLEDevice_QuickLock.h"
#import "BLEManager.h"       
#import <UIKit/UIKit.h>

@implementation BLEDevice_QuickLock

@synthesize delegateBLEDeviceResult_QuickLock;

-(id)init:(id)delegete
{
    [self setDelegateBLEDeviceResult_QuickLock:delegete];
    return [super init:delegete];
}
// ------------------------------------------------ ------------------
// Internal private function section
// ------------------------------------------------ ------------------

// Hardware data reception function - overloading concrete implementation part, different for each sub-category
- (void)didUpdateValueForCharacteristic_override:(CBCharacteristic *)characteristic
{
    UInt16 characteristicUUID = [BLEPublic CBUUIDToInt:characteristic.UUID];
    int nLength = (int)characteristic.value.length;
    Byte bAddress[nLength];
    [characteristic.value getBytes:&bAddress length:nLength];
    
    switch(characteristicUUID)
    {
        case QuickLock_TI_KEYFOB_SYSTEM_ID_PROPERTY_UUID:
        {
            [self processValue_SYSTEM_ID:bAddress length:nLength];
            break;
        }
        case QuickLock_TI_KEYFOB_MODEL_NUMBER_PROPERTY_UUID:
        {
            [self processValue_MODEL_NUMBER:bAddress length:nLength];
            break;
        }
        case QuickLock_TI_KEYFOB_FIRMWARE_REVISION_PROPERTY_UUID:
        {
            [self processValue_FIRMWARE_REVISION:bAddress length:nLength];
            break;
        }
        case QuickLock_TI_KEYFOB_PROTOCOL_VERSION_PROPERTY_UUID:
        {
            [self processValue_PROTOCOL_VERSION:bAddress length:nLength];
            break;
        }
        case QuickLock_TI_KEYFOB_BATT_STATE_PROPERTY_UUID:
        {
            [self processValue_BATT_STATE:bAddress length:nLength];
            break;
        }
        case QuickLock_TI_KEYFOB_TIME_SYNCHRONOUS_PROPERTY_UUID:
        {
            [self processValue_TIME_SYNCHRONOUS:bAddress length:nLength];
            break;
        }
        case QuickLock_TI_KEYFOB_CUSTOMER_UNLOCK_STATE_PROPERTY_UUID:
        {
            [self processValue_UNLOCK_STATE:bAddress length:nLength];
            break;
        }
        case QuickLock_TI_KEYFOB_CUSTOMER_UNLOCK_OPENTIME_PROPERTY_UUID:
        {
            [self processValue_UNLOCK_OPENTIME:bAddress length:nLength];
            break;
        }
            
        case QuickLock_TI_KEYFOB_CUSTOMER_UNLOCK_ALARM_SET:
        {
            [self processValue_ALARM_STATE:bAddress length:nLength];
            break;
        }
            
        case QuickLock_TI_KEYFOB_SHARECODE_ZWAVE:
        {
            [self processValue_Zwave:bAddress length:nLength];
            break;
        }
            
        case QuickLock_TI_KEYFOB_FINGERPRINT_READ_COUNT:
        {
            [self processValue_READ_FINGERPRINT:bAddress length:nLength];
            break;
        }
            
        case QuickLock_TI_KEYFOB_RFID_ADD_UUID:
        {
            [self processValue_ADD_RFID:bAddress length:nLength];
            break;
        }
//        case QuickLock_TI_WriteControl_RFID_ADD_Multiple_UUID:
//        {
//            [self processValue_ADD_RFID:bAddress length:nLength];
//            break;
//        }
            // learn finger print
        case QuickLock_TI_KEYFOB_FINGERPRINT_LEARN:
        {
            [self processValue_LEARN_FINGERPRINT:bAddress length:nLength];
            break;
        }
            
        case QuickLock_TI_KEYFOB_CUSTOMER_UNLOCK_PASSWORDRESULT_PROPERTY_UUID:
        {
            [self processValue_UNLOCK_PASSWORDRESULT:bAddress length:nLength];
            break;
        }
        case QuickLock_TI_KEYFOB_HISTORY_PHONE_ID_PROPERTY_UUID:
        {
            [self processValue_HISTORY_PHONE_ID:bAddress length:nLength];
            break;
        }
        case QuickLock_TI_KEYFOB_RFID_CONTROL_PROPERTY_UUID:
        {
            [self processValue_RFID_UPDATE:bAddress length:nLength];
            
            break;
        }
        case QuickLock_TI_KEYFOB_HISTORY_UPDATE_PROPERTY_UUID:
        {
            [self processValue_HISTORY_UPDATE:bAddress length:nLength];
            break;
        }
            
        case QuickLock_TI_KEYFOB_SHARECODE_COUNT:
        {
            [self processValue_My_Lock:bAddress length:nLength];
            break;
        }
            
        case QuickLock_TI_KEYFOB_SHARECODE_TIME:
        {
            [self processValue_My_Lock_ShareCodeTimeBase:bAddress length:nLength];
            break;
        }
            
        case QuickLock_TI_KEYFOB_RFID_PHONE_ID_PROPERTY_UUID:
        {
            [self processValue_HISTORY_PHONE_ID:bAddress length:nLength];
            break;
        }
            
        case QuickLock_TI_KEYFOB_WIFI_MAC_ADDRESS:
        {
            [self processValue_WIFI_MAC_ADDRESS:bAddress length:nLength];
            break;
        }
            
            
        default:
        {
            if(AntiLost_Debug)
            {
                NSLog(@"didUpdateValueForCharacteristic --- default --- characteristicUUID:%d" , characteristicUUID);
            }
            break;
        }
            
    }
}


//////////////////////////////////////////////////////////////////
/////           Get values from lock device          /////////////
//////////////////////////////////////////////////////////////////

-(void)processValue_SYSTEM_ID:(Byte *)bAddress length:(int)length
{
    if (length < QuickLock_TI_KEYFOB_SYSTEM_ID_LENGTH)
    {
        return;
    }
    
    NSString *systemID = @"";
    
    systemID = [NSString stringWithFormat:@"%.2X %.2X %.2X %.2X %.2X %.2X %.2X %.2X", bAddress[0], bAddress[1], bAddress[2], bAddress[3], bAddress[4], bAddress[5], bAddress[6], bAddress[7]];
    
    [delegateBLEDeviceResult_QuickLock BR_systemID:systemID DeviceID:DeviceID];
}

-(void)processValue_MODEL_NUMBER:(Byte *)bAddress length:(int)length
{
    if (length < QuickLock_TI_KEYFOB_MODEL_NUMBER_LENGTH)
    {
        return;
    }
    
    NSString *modelNumber = @"";
    
    int i;
    
    for (i = 0; i < QuickLock_TI_KEYFOB_MODEL_NUMBER_LENGTH; i++)
    {
        modelNumber = [NSString stringWithFormat:@"%@%c", modelNumber,  bAddress[i]];
    }
    
    [delegateBLEDeviceResult_QuickLock BR_modelNumber:modelNumber DeviceID:DeviceID];
}

-(void)processValue_FIRMWARE_REVISION:(Byte *)bAddress length:(int)length
{
    if (length < QuickLock_TI_KEYFOB_FIRMWARE_REVISION_LENGTH)
    {
        return;
    }
    
    NSString *firmwareRevision = @"";
    NSString *revisionDate = @"";
    
    firmwareRevision = [NSString stringWithFormat:@"%.2X%.2X", bAddress[0], bAddress[1]];
    revisionDate = [NSString stringWithFormat:@"20%.2X-%.2X-%.2X %.2X:%.2X:00", bAddress[2], bAddress[3], bAddress[4], bAddress[5], bAddress[6]];
    
    [delegateBLEDeviceResult_QuickLock BR_firmwareRevision:firmwareRevision visionDate:revisionDate DeviceID:DeviceID];
}

-(void)processValue_PROTOCOL_VERSION:(Byte *)bAddress length:(int)length
{
    if (length < QuickLock_TI_KEYFOB_PROTOCOL_VERSION_LENGTH)
    {
        return;
    }
    
    int lockID = 0;
    NSString *protocolVersion = @"";
    
    lockID = bAddress[0];
    protocolVersion = [NSString stringWithFormat:@"%X", bAddress[1]];
    [delegateBLEDeviceResult_QuickLock BR_protocolVersion:lockID protocolVersion:protocolVersion DeviceID:DeviceID];
}

-(void)processValue_BATT_STATE:(Byte *)bAddress length:(int)length
{
    if (length < QuickLock_TI_KEYFOB_BATT_STATE_LENGTH)
    {
        return;
    }
    
    int level;
    level = bAddress[0];
    
    [delegateBLEDeviceResult_QuickLock BR_batteryLevel:level DeviceID:DeviceID];
}

-(void)processValue_TIME_SYNCHRONOUS:(Byte *)bAddress length:(int)length
{
    if (length < QuickLock_TI_KEYFOB_TIME_LENGTH)
    {
        return;
    }
    
    int n;
    NSDate *da,*date2000;
    NSDateFormatter *fm;
    
    fm = [[NSDateFormatter alloc]init];
    [fm setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    date2000 = [fm dateFromString:@"2000-01-01 00:00:00"];
    
    int b1,b2,b3,b4;
    b1 = (int)bAddress[0];
    b2 = (int)bAddress[1];
    b3 = (int)bAddress[2];
    b4 = (int)bAddress[3];
    n = b1 | (b2 << 8) | (b3 << 16) | (b4 << 24);
    da = [[NSDate alloc]initWithTimeInterval:n sinceDate:date2000];
    
    if (AntiLost_Debug)
    {
        NSLog(@"0x2A2B-----: %d,%d,%d,%d,%d,%@",b1,b2,b3,b4,n,[fm stringFromDate:da]);
    }
    
    [delegateBLEDeviceResult_QuickLock BR_currentTime:da DeviceID:DeviceID];
}

-(void)processValue_UNLOCK_STATE:(Byte *)bAddress length:(int)length
{
    if (length < QuickLock_TI_KEYFOB_CUSTOMER_UNLOCK_STATE_LENGTH)
    {
        return;
    }
    
    BOOL isLockOpened;
    
    if (bAddress[0] == 0x01)
    {
        isLockOpened = YES;
    }
    else
    {
        isLockOpened = NO;
    }
    
    [delegateBLEDeviceResult_QuickLock BR_lockState:isLockOpened DeviceID:DeviceID];
}

-(void)processValue_UNLOCK_OPENTIME:(Byte *)bAddress length:(int)length
{
    if (length < QuickLock_TI_KEYFOB_CUSTOMER_UNLOCK_OPENTIME_LENGTH)
    {
        return;
    }
    
    Byte lockOpenSecond;
    lockOpenSecond = bAddress[0];
    
    [delegateBLEDeviceResult_QuickLock BR_lockOpenTime:lockOpenSecond DeviceID:DeviceID];
}

-(void)processValue_READ_FINGERPRINT:(Byte *)bAddress length:(int)length
{
    if (length < QuickLock_TI_KEYFOB_FINGERPRINT_READ_LENGTH)
    {
        return;
    }
    
    Byte fingerprintCount;
    fingerprintCount = bAddress[0];
    [delegateBLEDeviceResult_QuickLock BR_fingerprintCount:fingerprintCount DeviceID:DeviceID];
}

-(void)processValue_ALARM_STATE:(Byte *)bAddress length:(int)length
{
    if (length < QuickLock_TI_KEYFOB_CUSTOMER_UNLOCK_ALARM_LENGTH)
    {
        return;
    }
    Byte alarmState, alarmMinutes;
    alarmState = bAddress[0];
    alarmMinutes = bAddress[1];
    
    [delegateBLEDeviceResult_QuickLock BR_alarm_count:alarmState delay:alarmMinutes DeviceID:DeviceID];
}

-(void)processValue_ADD_RFID:(Byte *)bAddress length:(int)length
{
    NSLog(@"%02X",bAddress[0]);
    
    if (length < QuickLock_TI_KEYFOB_RFID_CONTROL_LENGTH)
    {
        return;
    }
    
    NSDictionary *userInfo;
    if (bAddress[0] == 0xFF)
    {
        userInfo = @{@"msg" : @"Time Out"};
    }
    else if (bAddress[0] == 0x00)
    {
        userInfo = @{@"msg" : @"Busy"};
    }
    else if (bAddress[0] == 0x01)
    {
        userInfo = @{@"msg" : @"Successfully added new RFID Tag"};
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"kcallAfterAddNewRFID" object:self userInfo:userInfo];
}

-(void)processValue_LEARN_FINGERPRINT:(Byte *)bAddress length:(int)length
{
    NSLog(@"%02X",bAddress[0]);
    
    if (length < QuickLock_TI_KEYFOB_FINGERPRINT_LEARN_LENGTH)
    {
        return;
    }
    NSDictionary *userInfo;
    
    if (bAddress[0] == 0x01)
    {
        userInfo = @{@"msg" : @"Great!\nTwo more times."};
    }
    else if (bAddress[0] == 0x02)
    {
        userInfo = @{@"msg" : @"Well Done!\nOne more time."};
    }
    else if (bAddress[0] == 0x03)
    {
        userInfo = @{@"msg" : @"Congratulations!\nYour print has been programmed.\nHold your finger this way every time."};
        [[NSNotificationCenter defaultCenter] postNotificationName:@"getgetCount" object:nil];
    }
    else if (bAddress[0] == 0xFE)
    {
        userInfo = @{@"msg" : @"Fingerprint learning failed."};
        [[NSNotificationCenter defaultCenter] postNotificationName:@"getgetCount" object:nil];
        // Fingerprint learning failed, please try again.
    }
    else if (bAddress[0] == 0xFF)
    {
        userInfo = @{@"msg" : @"Fingerprint reader time out."};
        [[NSNotificationCenter defaultCenter] postNotificationName:@"getgetCount" object:nil];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"fingerPrintResponse" object:self userInfo:userInfo];
}


-(void)processValue_UNLOCK_PASSWORDRESULT:(Byte *)bAddress length:(int)length
{
    if (length < QuickLock_TI_KEYFOB_CUSTOMER_UNLOCK_PASSWORDRESULT_LENGTH)
    {
        return;
    }
    
    BOOL isPasswordRight;
    BOOL isModify;
    
    if ((bAddress[0] == 0x00) || (bAddress[0] == 0x01))
    {
        isModify = NO;
        if (bAddress[0] == 0x01)
        {
            isPasswordRight = YES;
            
            if (bAddress[1] == 0x01)
            {
                quickLockDeviceManage.lockDevice.is_Login_share_Code = 1;
            }
            else
            {
                quickLockDeviceManage.lockDevice.is_Login_share_Code = 0;
            }
        }
        else
        {
            isPasswordRight = NO;
        }
    }
    else
    {
        isModify = YES;
        if (bAddress[0] == 0x03)
        {
            isPasswordRight = YES;
        }
        else
        {
            isPasswordRight = NO;
        }
    }
    
    [delegateBLEDeviceResult_QuickLock BR_PasswordResult:isModify isValidPassword:isPasswordRight DeviceID:DeviceID];
}

-(void)processValue_HISTORY_PHONE_ID:(Byte *)bAddress length:(int)length
{
    int n, i;
    NSString *s;
    
    n = bAddress[0];
    
    s = @"";
    if (n > 0)
    {
        for (i = 1; i <= n; i++)
        {
            s = [NSString stringWithFormat:@"%@%c", s, bAddress[i]];
        }
        [delegateBLEDeviceResult_QuickLock BR_userName:s DeviceID:DeviceID];
    }
}

-(void)processValue_RFID_UPDATE:(Byte *)bAddress length:(int)length
{
    int devType, i, n;
    NSString *dID;
    NSDate *da;
    BOOL isOpenLock;
    
    //NFC end or the end of RFID Iphone information, do nothing
    if (((bAddress[0] == 0xF0) &&
         (bAddress[1] == 0xF0) &&
         (bAddress[2] == 0xF0) &&
         (bAddress[3] == 0xF0)) ||
        ((bAddress[0] == 0xE0) &&
         (bAddress[1] == 0xE0) &&
         (bAddress[2] == 0xE0) &&
         (bAddress[3] == 0xE0)))
    {
        return;
    }
    
    //NFC information to determine whether or Phone Information
    if (bAddress[7] == 0xE0)
    {
        //设备类型是NFC
        devType = 0;
        
        //解析ID
        dID = @"";
        for (i = 0; i < 7; i++)
        {
            dID = [NSString stringWithFormat:@"%@%02X", dID, bAddress[i]];
        }
        
        //解析时间
        da = [BLEPublic GetBytesToDate:bAddress[8] b1:bAddress[9] b2:bAddress[10] b3:bAddress[11]];
        
        //开锁还是关锁(0x00:开锁；0x01:开锁；0x02:关锁)
        if (bAddress[12] != 0x02)
        {
            isOpenLock = YES;
        }
        else
        {
            isOpenLock = NO;
        }
        
        [delegateBLEDeviceResult_QuickLock BR_historyLog:devType idOrName:dID logDate:da isOpenLock:isOpenLock DeviceID:DeviceID];
    }
    else
    {
        //设备类型是Phone
        devType = 1;
        
        //解析手机名
        n = bAddress[0];
        
        dID = @"";
        if ((n > 0) && (n < QuickLock_TI_KEYFOB_RFID_PHONE_ID_LENGTH))
        {
            for (i = 1; i <= n; i++)
            {
                dID = [NSString stringWithFormat:@"%@%c", dID, bAddress[i]];
            }
        }
        
        //解析时间
        da = [BLEPublic GetBytesToDate:bAddress[8] b1:bAddress[9] b2:bAddress[10] b3:bAddress[11]];
        
        //开锁还是关锁(0x00:开锁；0x03:开锁；0x04:关锁)
        if (bAddress[12] != 0x04)
        {
            isOpenLock = YES;
        }
        else
        {
            isOpenLock = NO;
        }
        
        [delegateBLEDeviceResult_QuickLock BR_historyLog:devType idOrName:dID logDate:da isOpenLock:isOpenLock DeviceID:DeviceID];
    }
}

-(void)processValue_HISTORY_UPDATE:(Byte *)bAddress length:(int)length
{
    int devType, i, n;
    NSString *dID;
    NSDate *da;
    BOOL isOpenLock;
    
    // Return E0E0E0E0E0E0E0E0 end record or no record
    if ((bAddress[0] == 0xE0) &&
        (bAddress[1] == 0xE0) &&
        (bAddress[2] == 0xE0) &&
        (bAddress[3] == 0xE0) &&
        (bAddress[4] == 0xE0) &&
        (bAddress[5] == 0xE0) &&
        (bAddress[6] == 0xE0) &&
        (bAddress[7] == 0xE0))
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"stopActivity" object:nil];
    }
    
    //NFC historical information end or end Iphone historical information, do nothing
    if (((bAddress[0] == 0xF0) &&
         (bAddress[1] == 0xF0) &&
         (bAddress[2] == 0xF0) &&
         (bAddress[3] == 0xF0) &&
         (bAddress[4] == 0xF0) &&
         (bAddress[5] == 0xF0) &&
         (bAddress[6] == 0xF0) &&
         (bAddress[7] == 0xF0)) ||
        ((bAddress[0] == 0xE0) &&
         (bAddress[1] == 0xE0) &&
         (bAddress[2] == 0xE0) &&
         (bAddress[3] == 0xE0) &&
         (bAddress[4] == 0xE0) &&
         (bAddress[5] == 0xE0) &&
         (bAddress[6] == 0xE0) &&
         (bAddress[7] == 0xE0)))
    {
        return;
    }
    
    //NFC information to determine whether or Phone information
    if (bAddress[7] == 0xE0)
    {
        //Device type is NFC
        devType = 0;
        
        //Resolve ID
        dID = @"";
        for (i = 0; i < 7; i++)
        {
            dID = [NSString stringWithFormat:@"%@%02X", dID, bAddress[i]];
        }
        //Resolution Time
        da = [BLEPublic GetBytesToDate:bAddress[8] b1:bAddress[9] b2:bAddress[10] b3:bAddress[11]];
        
        //Lock or shut (0x00: unlock; 0x01: unlock; 0x02: Off Lock)
        if (bAddress[19] != 0x02)
        {
            isOpenLock = YES;
        }else
        {
            isOpenLock = NO;
        }
        
        [delegateBLEDeviceResult_QuickLock BR_historyLog:devType idOrName:dID logDate:da isOpenLock:isOpenLock DeviceID:DeviceID];
    }
    else
    {
        //Device type Phone
        devType = 1;
        
        //Analytical Phone name
        n = bAddress[0];
        
        dID = @"";
        if ((n > 0) && (n < QuickLock_TI_KEYFOB_HISTORY_PHONE_ID_LENGTH))
        {
            for (i = 1; i <= n; i++)
            {
                dID = [NSString stringWithFormat:@"%@%c", dID, bAddress[i]];
            }
        }
        //Resolution Time
        da = [BLEPublic GetBytesToDate:bAddress[15] b1:bAddress[16] b2:bAddress[17] b3:bAddress[18]];
        
        //Lock or shut (0x00: unlock; 0x03: unlock; 0x04: Off Lock)
        if (bAddress[19] != 0x04)
        {
            isOpenLock = YES;
        }else
        {
            isOpenLock = NO;
        }
        
        [delegateBLEDeviceResult_QuickLock BR_historyLog:devType idOrName:dID logDate:da isOpenLock:isOpenLock DeviceID:DeviceID];
    }
}

///////// Get My PadLocks screen Details /////////
-(void)processValue_My_Lock:(Byte *)bAddress length:(int)length
{
    int userCount, i;
    NSString *dID;
    
    if (((bAddress[0] == 0xF0) &&
         (bAddress[1] == 0xF0) &&
         (bAddress[2] == 0xF0) &&
         (bAddress[3] == 0xF0) &&
         (bAddress[4] == 0xF0)) ||
        ((bAddress[0] == 0xE0) &&
         (bAddress[1] == 0xE0) &&
         (bAddress[2] == 0xE0) &&
         (bAddress[3] == 0xE0) &&
         (bAddress[4] == 0xE0)))
    {
        return;
    }
    
    dID = @"";
    for (i = 0; i < 4; i++)
    {
        dID = [NSString stringWithFormat:@"%@%02X", dID, bAddress[i]];
    }
    
    userCount = (int)bAddress[4];
    
    [delegateBLEDeviceResult_QuickLock BR_getPasswordSharecode:userCount shareCode:dID];
}

-(void)processValue_My_Lock_ShareCodeTimeBase:(Byte *)bAddress length:(int)length
{
    int i;
    NSString *dID;
    NSDate *da1,*da2;
    NSString *t1,*t2;
    
    if (((bAddress[0] == 0xF0) &&
         (bAddress[1] == 0xF0) &&
         (bAddress[2] == 0xF0) &&
         (bAddress[3] == 0xF0) &&
         (bAddress[4] == 0xF0) &&
         (bAddress[5] == 0xF0) &&
         (bAddress[6] == 0xF0) &&
         (bAddress[7] == 0xF0) &&
         (bAddress[8] == 0xF0) &&
         (bAddress[9] == 0xF0) &&
         (bAddress[10] == 0xF0) &&
         (bAddress[11] == 0xF0)) ||
        ((bAddress[0] == 0xE0) &&
         (bAddress[1] == 0xE0) &&
         (bAddress[2] == 0xE0) &&
         (bAddress[3] == 0xE0) &&
         (bAddress[4] == 0xE0) &&
         (bAddress[5] == 0xE0) &&
         (bAddress[6] == 0xE0) &&
         (bAddress[7] == 0xE0) &&
         (bAddress[8] == 0xE0) &&
         (bAddress[9] == 0xE0) &&
         (bAddress[10] == 0xE0) &&
         (bAddress[11] == 0xE0) ))
    {
        return;
    }
    
    dID = @"";
    for (i = 0; i < 4; i++)
    {
        dID = [NSString stringWithFormat:@"%@%02X", dID, bAddress[i]];
    }
    da1 = [BLEPublic GetBytesToDate:bAddress[4] b1:bAddress[5] b2:bAddress[6] b3:bAddress[7]];
    da2 = [BLEPublic GetBytesToDate:bAddress[8] b1:bAddress[9] b2:bAddress[10] b3:bAddress[11]];
    
    t1 = [BLEPublic GetBytesToTime:bAddress[12] b1:bAddress[13]];
    t2 = [BLEPublic GetBytesToTime:bAddress[14] b1:bAddress[15]];
    
    NSLog(@"start date : %02x - %02x - %02x - %02x",bAddress[4], bAddress[5], bAddress[6], bAddress[7]);
    int strFlag = (int)bAddress[16];
    
    [delegateBLEDeviceResult_QuickLock BR_getPasswordSharecodeTimeBase:da1 endDate:da2 shareCode:dID time1:t1 time2:t2 flag:strFlag];
}

-(void)processValue_Zwave:(Byte *)bAddress length:(int)length
{
    if (length < QuickLock_TI_KEYFOB_SHARECODE_ZWAVE_LENGTH)
    {
        return;
    }
    
    Byte zwave;
    zwave = bAddress[0];
    
    [delegateBLEDeviceResult_QuickLock BR_Zwave:zwave DeviceID:DeviceID];
}

-(void)processValue_WIFI_MAC_ADDRESS:(Byte *)bAddress length:(int)length
{
    if (length < QuickLock_TI_KEYFOB_WIFI_MAC_ADDRESS_LENGTH)
    {
        return;
    }
    
}

//  ------------------------------------------------------------------
//  Implement part of the agreement Protocol_BLEDevice_QuickLock
//  ------------------------------------------------------------------

//////////////////////////////////////////////////////////////////
/////         Enable notifications for services      /////////////
//////////////////////////////////////////////////////////////////

-(void)BD_enableModelNumber:(BOOL)isEnable
{
    [[BLEManager sharedInstance] notification:DeviceID
                                  serviceUUID:QuickLock_TI_KEYFOB_VERSION_SERVICE_UUID
                           characteristicUUID:QuickLock_TI_KEYFOB_MODEL_NUMBER_PROPERTY_UUID
                                           on:isEnable];
}


-(void)BD_enableBatteryLevel:(BOOL)isEnable
{
    [[BLEManager sharedInstance] notification:DeviceID
                                  serviceUUID:QuickLock_TI_KEYFOB_BATT_SERVICE_UUID
                           characteristicUUID:QuickLock_TI_KEYFOB_BATT_STATE_PROPERTY_UUID
                                           on:isEnable];
}

-(void)BD_enableTimeUpdate:(BOOL)isEnable
{
    [[BLEManager sharedInstance] notification:DeviceID
                                  serviceUUID:QuickLock_TI_KEYFOB_TIME_SERVICE_UUID
                           characteristicUUID:QuickLock_TI_KEYFOB_TIME_SYNCHRONOUS_PROPERTY_UUID
                                           on:isEnable];
}

-(void)BD_enableShareCode:(BOOL)isEnable
{
    [[BLEManager sharedInstance] notification:DeviceID
                                  serviceUUID:QuickLock_TI_KEYFOB_SHARECODE_MAIN
                           characteristicUUID:QuickLock_TI_KEYFOB_SHARECODE_COUNT
                                           on:isEnable];
}

-(void)BD_enablePasswordResultUpdate:(BOOL)isEnable
{
    [[BLEManager sharedInstance] notification:DeviceID
                                  serviceUUID:QuickLock_TI_KEYFOB_CUSTOMER_SERVICE_UUID
                           characteristicUUID:QuickLock_TI_KEYFOB_CUSTOMER_UNLOCK_PASSWORDRESULT_PROPERTY_UUID
                                           on:isEnable];
}

-(void)BD_enableLockStateUpdate:(BOOL)isEnable
{
    [[BLEManager sharedInstance] notification:DeviceID
                                  serviceUUID:QuickLock_TI_KEYFOB_CUSTOMER_SERVICE_UUID
                           characteristicUUID:QuickLock_TI_KEYFOB_CUSTOMER_UNLOCK_STATE_PROPERTY_UUID
                                           on:isEnable];
}

-(void)BD_enableZwave:(BOOL)isEnable
{
    [[BLEManager sharedInstance] notification:DeviceID
                                  serviceUUID:QuickLock_TI_KEYFOB_SHARECODE_ZWAVE_MAIN
                           characteristicUUID:QuickLock_TI_KEYFOB_SHARECODE_ZWAVE
                                           on:isEnable];
}

-(void)BD_enableHistoryLogUpdate:(BOOL)isEnable
{
    [[BLEManager sharedInstance] notification:DeviceID
                                  serviceUUID:QuickLock_TI_KEYFOB_HISTORY_SERVICE_UUID
                           characteristicUUID:QuickLock_TI_KEYFOB_HISTORY_UPDATE_PROPERTY_UUID
                                           on:isEnable];
}

-(void)BD_enableRFIDListUpdate:(BOOL)isEnable
{
    [[BLEManager sharedInstance] notification:DeviceID
                                  serviceUUID:QuickLock_TI_KEYFOB_RFID_SERVICE_UUID
                           characteristicUUID:QuickLock_TI_KEYFOB_RFID_UPDATE_PROPERTY_UUID
                                           on:isEnable];
}

-(void)BD_enableRFIDAdd:(BOOL)isEnable
{
    [[BLEManager sharedInstance] notification:DeviceID
                                  serviceUUID:QuickLock_TI_KEYFOB_RFID_SERVICE_UUID
                           characteristicUUID:QuickLock_TI_KEYFOB_RFID_ADD_UUID
                                           on:isEnable];
}

-(void)BD_enableFingerprintLearn:(BOOL)isEnable
{
    [[BLEManager sharedInstance] notification:DeviceID
                                  serviceUUID:QuickLock_TI_KEYFOB_CUSTOMER_UNLOCK_SURE_SET_MAIN
                           characteristicUUID:QuickLock_TI_KEYFOB_FINGERPRINT_LEARN
                                           on:isEnable];
}


//////////////////////////////////////////////
///////         Read values      /////////////
//////////////////////////////////////////////

-(void)BD_getSystemID
{
    [[BLEManager sharedInstance] readValueForCharacteristic:DeviceID
                                                serviceUUID:QuickLock_TI_KEYFOB_VERSION_SERVICE_UUID
                                         characteristicUUID:QuickLock_TI_KEYFOB_SYSTEM_ID_PROPERTY_UUID];
}


-(void)BD_getModelNumber
{
    [[BLEManager sharedInstance] readValueForCharacteristic:DeviceID
                                                serviceUUID:QuickLock_TI_KEYFOB_VERSION_SERVICE_UUID
                                         characteristicUUID:QuickLock_TI_KEYFOB_MODEL_NUMBER_PROPERTY_UUID];
}

-(void)BD_getFirmwareRevision
{
    [[BLEManager sharedInstance] readValueForCharacteristic:DeviceID
                                                serviceUUID:QuickLock_TI_KEYFOB_VERSION_SERVICE_UUID
                                         characteristicUUID:QuickLock_TI_KEYFOB_FIRMWARE_REVISION_PROPERTY_UUID];
}

-(void)BD_getProtocolVersion
{
    [[BLEManager sharedInstance] readValueForCharacteristic:DeviceID
                                                serviceUUID:QuickLock_TI_KEYFOB_VERSION_SERVICE_UUID
                                         characteristicUUID:QuickLock_TI_KEYFOB_PROTOCOL_VERSION_PROPERTY_UUID];
}

-(void)BD_getFingerprintCount
{
    [[BLEManager sharedInstance] readValueForCharacteristic:DeviceID
                                                serviceUUID:QuickLock_TI_KEYFOB_CUSTOMER_UNLOCK_SURE_SET_MAIN
                                         characteristicUUID:QuickLock_TI_KEYFOB_FINGERPRINT_READ_COUNT];
}

-(void)BD_getAlarmState
{
    [[BLEManager sharedInstance] readValueForCharacteristic:DeviceID
                                                serviceUUID:QuickLock_TI_KEYFOB_CUSTOMER_SERVICE_UUID
                                         characteristicUUID:QuickLock_TI_KEYFOB_CUSTOMER_UNLOCK_ALARM_SET];
}

-(void)BD_getBetteryLevel
{
    [[BLEManager sharedInstance] readValueForCharacteristic:DeviceID
                                                serviceUUID:QuickLock_TI_KEYFOB_BATT_SERVICE_UUID
                                         characteristicUUID:QuickLock_TI_KEYFOB_BATT_STATE_PROPERTY_UUID];
}

-(void)BD_getCurrentTime
{
    [[BLEManager sharedInstance] readValueForCharacteristic:DeviceID
                                                serviceUUID:QuickLock_TI_KEYFOB_TIME_SERVICE_UUID
                                         characteristicUUID:QuickLock_TI_KEYFOB_TIME_SYNCHRONOUS_PROPERTY_UUID];
}

-(void)BD_getPasswordResult
{
    [[BLEManager sharedInstance] readValueForCharacteristic:DeviceID
                                                serviceUUID:QuickLock_TI_KEYFOB_CUSTOMER_SERVICE_UUID
                                         characteristicUUID:QuickLock_TI_KEYFOB_CUSTOMER_UNLOCK_PASSWORDRESULT_PROPERTY_UUID];
}

-(void)BD_getLockState
{
    [[BLEManager sharedInstance] readValueForCharacteristic:DeviceID
                                                serviceUUID:QuickLock_TI_KEYFOB_CUSTOMER_SERVICE_UUID
                                         characteristicUUID:QuickLock_TI_KEYFOB_CUSTOMER_UNLOCK_STATE_PROPERTY_UUID];
}

-(void)BD_getPasswordSharecode
{
    [[BLEManager sharedInstance] readValueForCharacteristic:DeviceID
                                                serviceUUID:QuickLock_TI_KEYFOB_SHARECODE_MAIN
                                         characteristicUUID:QuickLock_TI_KEYFOB_SHARECODE_COUNT];
}

-(void)BD_getPasswordSharecodetimeBase
{
    [[BLEManager sharedInstance] readValueForCharacteristic:DeviceID
                                                serviceUUID:QuickLock_TI_KEYFOB_SHARECODE_MAIN
                                         characteristicUUID:QuickLock_TI_KEYFOB_SHARECODE_TIME];
}

-(void)BD_getLockOpenTime
{
    [[BLEManager sharedInstance] readValueForCharacteristic:DeviceID
                                                serviceUUID:QuickLock_TI_KEYFOB_CUSTOMER_SERVICE_UUID
                                         characteristicUUID:QuickLock_TI_KEYFOB_CUSTOMER_UNLOCK_OPENTIME_PROPERTY_UUID];
}

-(void)BD_getZwave
{
    [[BLEManager sharedInstance] readValueForCharacteristic:DeviceID
                                                serviceUUID:QuickLock_TI_KEYFOB_SHARECODE_ZWAVE_MAIN
                                         characteristicUUID:QuickLock_TI_KEYFOB_SHARECODE_ZWAVE];
}

-(void)BD_getUserName
{
    [[BLEManager sharedInstance] readValueForCharacteristic:DeviceID
                                                serviceUUID:QuickLock_TI_KEYFOB_HISTORY_SERVICE_UUID
                                         characteristicUUID:QuickLock_TI_KEYFOB_HISTORY_PHONE_ID_PROPERTY_UUID];
}

-(void)BD_getWifiMacAddress
{
    [[BLEManager sharedInstance] readValueForCharacteristic:DeviceID
                                                serviceUUID:QuickLock_TI_KEYFOB_HISTORY_SERVICE_UUID
                                         characteristicUUID:QuickLock_TI_KEYFOB_HISTORY_PHONE_ID_PROPERTY_UUID];
}

//////////////////////////////////////////////
///////        Write values      /////////////
//////////////////////////////////////////////

-(void)BD_setPassCode:(NSString *)keyCode
{
    char bytes[11];
    
    if ([keyCode isEqualToString:@"0"])
    {
        // Disable
        bytes[0] = 0x00;
        bytes[1] = 0x00;
        
        NSData *d = [[NSData alloc] initWithBytes:bytes length:11];
        [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID
                                                     serviceUUID:QuickLock_TI_KEYFOB_CUSTOMER_SERVICE_UUID
                                              characteristicUUID:QuickLock_TI_KEYFOB_SET_KEYCODE
                                                            data:d];
        
        return;
    }
    NSMutableArray *arrStr = [[NSMutableArray alloc]init];
    arrStr = [keyCode componentsSeparatedByString:@" "];
    [arrStr removeObjectAtIndex:arrStr.count-1];
    NSString *s = [NSString stringWithFormat:@"%d",arrStr.count];
    [arrStr insertObject:s atIndex:0];
    
    
    for (int i = 0; i < arrStr.count; i++)
    {
        NSString *str = [arrStr objectAtIndex:i];
        int byte = [str intValue];
        bytes[i] = byte;
    }
    
    NSData *d = [[NSData alloc] initWithBytes:bytes length:11];
    [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID
                                                 serviceUUID:QuickLock_TI_KEYFOB_CUSTOMER_SERVICE_UUID
                                          characteristicUUID:QuickLock_TI_KEYFOB_SET_KEYCODE
                                                        data:d];
}

-(void)BD_setModelNumber:(NSString *)modelNumber
{
    char chars[QuickLock_TI_KEYFOB_MODEL_NUMBER_LENGTH] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    
    [BLEPublic GetStringToBytes:modelNumber count:QuickLock_TI_KEYFOB_MODEL_NUMBER_LENGTH bytes:chars];
    
    NSData *d = [[NSData alloc] initWithBytes:chars length:QuickLock_TI_KEYFOB_MODEL_NUMBER_LENGTH];
    [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID
                                                 serviceUUID:QuickLock_TI_KEYFOB_VERSION_SERVICE_UUID
                                          characteristicUUID:QuickLock_TI_KEYFOB_MODEL_NUMBER_PROPERTY_UUID
                                                        data:d];
}

-(void)BD_setCurrentTime
{
    NSDate *date2000;
    uint time;
    NSDateFormatter *fm;
    char bytes[QuickLock_TI_KEYFOB_TIME_LENGTH];
    
    fm = [[NSDateFormatter alloc]init];
    [fm setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    date2000 = [fm dateFromString:@"2000-01-01 00:00:00"];
    time = (int)[[NSDate date] timeIntervalSinceDate:date2000];
    bytes[0] = time;
    bytes[1] = time >> 8;
    bytes[2] = time >> 16;
    bytes[3] = time >> 24;
    
    NSData *d = [[NSData alloc] initWithBytes:bytes length:QuickLock_TI_KEYFOB_TIME_LENGTH];
    [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID
                                                 serviceUUID:QuickLock_TI_KEYFOB_TIME_SERVICE_UUID
                                          characteristicUUID:QuickLock_TI_KEYFOB_TIME_SYNCHRONOUS_PROPERTY_UUID
                                                        data:d];
    
    if (AntiLost_Debug)
    {
        time = (int)[[NSDate date] timeIntervalSinceDate:date2000];
        NSLog(@"---------------%d,%d,%d,%d,%d", time, (Byte)bytes[0], (Byte)bytes[1], (Byte)bytes[2], (Byte)bytes[3]);
    }
}

-(void)BD_setVerifyPassword:(Byte)password1 password2:(Byte)password2 password3:(Byte)password3 password4:(Byte)password4
{
    char bytes[QuickLock_TI_KEYFOB_CUSTOMER_UNLOCK_PASSWORD_LENGTH];
    
    //  1 Byte:Type: Verify Password
    bytes[0] = 0x00;
    
    //  4 Byte:旧密码 验证密码时，两组密码一致
    bytes[1] = password1;
    bytes[2] = password2;
    bytes[3] = password3;
    bytes[4] = password4;
    
    //  4 Byte:新密码
    bytes[5] = password1;
    bytes[6] = password2;
    bytes[7] = password3;
    bytes[8] = password4;
    
    NSData *d = [[NSData alloc] initWithBytes:bytes length:QuickLock_TI_KEYFOB_CUSTOMER_UNLOCK_PASSWORD_LENGTH];
    [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID
                                                 serviceUUID:QuickLock_TI_KEYFOB_CUSTOMER_SERVICE_UUID
                                          characteristicUUID:QuickLock_TI_KEYFOB_CUSTOMER_UNLOCK_PASSWORD_PROPERTY_UUID
                                                        data:d];
}

-(void)BD_setModifyPassword:(Byte)old1 oldPassword2:(Byte)old2 oldPassword3:(Byte)old3 oldPassword4:(Byte)old4 newPassword1:(Byte)new1 newPassword2:(Byte)new2 newPassword3:(Byte)new3 newPassword4:(Byte)new4
{
    char bytes[QuickLock_TI_KEYFOB_CUSTOMER_UNLOCK_PASSWORD_LENGTH];
    
    //  1 Byte:Type: Change Password
    bytes[0] = 0x01;
    
    //  4 Byte: Old Password
    bytes[1] = old1;
    bytes[2] = old2;
    bytes[3] = old3;
    bytes[4] = old4;
    
    //  4 Byte:new password
    bytes[5] = new1;
    bytes[6] = new2;
    bytes[7] = new3;
    bytes[8] = new4;
    
    NSData *d = [[NSData alloc] initWithBytes:bytes length:QuickLock_TI_KEYFOB_CUSTOMER_UNLOCK_PASSWORD_LENGTH];
    [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID
                                                 serviceUUID:QuickLock_TI_KEYFOB_CUSTOMER_SERVICE_UUID
                                          characteristicUUID:QuickLock_TI_KEYFOB_CUSTOMER_UNLOCK_PASSWORD_PROPERTY_UUID
                                                        data:d];
}

// Set Share code User count wise
-(void)BD_share_Code_Count:(Byte)CODE1 CODE2:(Byte)CODE2 CODE3:(Byte)CODE3 CODE4:(Byte)CODE4 CODECOUNT:(Byte)CODECOUNT
{
    char bytes[QuickLock_TI_KEYFOB_SHARECODE_COUNT_LENGTH];
    
    //  4 Byte:Received share code
    bytes[0] = CODE1;
    bytes[1] = CODE2;
    bytes[2] = CODE3;
    bytes[3] = CODE4;
    
    // User access count
    bytes[4] = CODECOUNT;
    
    NSData *d = [[NSData alloc] initWithBytes:bytes length:QuickLock_TI_KEYFOB_SHARECODE_COUNT_LENGTH];
    [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID
                                                 serviceUUID:QuickLock_TI_KEYFOB_SHARECODE_MAIN
                                          characteristicUUID:QuickLock_TI_KEYFOB_SHARECODE_COUNT
                                                        data:d];
}

// set Share code Time wise
-(void)BD_share_Code_Time:(Byte)CODE1 CODE2:(Byte)CODE2 CODE3:(Byte)CODE3 CODE4:(Byte)CODE4
{
    char bytes[QuickLock_TI_KEYFOB_SHARECODE_TIME_LENGTH];
    
    // Share code
    bytes[0] = CODE1;
    bytes[1] = CODE2;
    bytes[2] = CODE3;
    bytes[3] = CODE4;
    
    NSDate *date2000;
    uint time;
    NSDateFormatter *fm;
    fm = [[NSDateFormatter alloc]init];
    [fm setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    date2000 = [fm dateFromString:@"2000-01-01 00:00:00"];
    
    // start time
    time = (int)[quickLockDeviceManage.lockDevice.share_Code_Start_Time timeIntervalSinceDate:date2000];
    bytes[4] = time;
    bytes[5] = time >> 8;
    bytes[6] = time >> 16;
    bytes[7] = time >> 24;
    
    // end time
    time = (int)[quickLockDeviceManage.lockDevice.share_Code_End_Time timeIntervalSinceDate:date2000];
    bytes[8] = time;
    bytes[9] = time >> 8;
    bytes[10] = time >> 16;
    bytes[11] = time >> 24;
    
    time = quickLockDeviceManage.lockDevice.share_Code_Start_Time_minutes;
    bytes[12] = time;
    bytes[13] = time >> 8;
    
    time = quickLockDeviceManage.lockDevice.share_Code_End_Time_minutes;
    bytes[14] = time;
    bytes[15] = time >> 8;
    
    bytes[16] = quickLockDeviceManage.lockDevice.share_Code_DATE_Time_FLAG;
    
    NSData *d = [[NSData alloc] initWithBytes:bytes length:QuickLock_TI_KEYFOB_SHARECODE_TIME_LENGTH];
    [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID
                                                 serviceUUID:QuickLock_TI_KEYFOB_SHARECODE_MAIN
                                          characteristicUUID:QuickLock_TI_KEYFOB_SHARECODE_TIME
                                                        data:d];
}

-(void)BD_deleteRFID:(Byte)RFID1 RFID2:(Byte)RFID2 RFID3:(Byte)RFID3 RFID4:(Byte)RFID4 RFID5:(Byte)RFID5 RFID6:(Byte)RFID6 RFID7:(Byte)RFID7
{
    char bytes[QuickLock_TI_KEYFOB_RFID_PHONE_ID_LENGTH];
    
    bytes[7] = 0xE0;
    // A8BD482A500104
    //  4 Byte:Received RFID/TAG ID to delete
    bytes[0] = RFID1;
    bytes[1] = RFID2;
    bytes[2] = RFID3;
    bytes[3] = RFID4;
    bytes[4] = RFID5;
    bytes[5] = RFID6;
    bytes[6] = RFID7;
    
    NSLog(@"%@",[NSString stringWithFormat:@"RFID : %02x %02x %02x %02x %02x %02x %02x %02x",bytes[0], bytes[1], bytes[2], bytes[3], bytes[4], bytes[5], bytes[6], bytes[7]]);
    //    NSLog(@"Bytes for TAG : %@",bytes);
    NSData *d = [[NSData alloc] initWithBytes:bytes length:QuickLock_TI_KEYFOB_RFID_PHONE_ID_LENGTH];
    [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID
                                                 serviceUUID:QuickLock_TI_KEYFOB_RFID_SERVICE_UUID
                                          characteristicUUID:QuickLock_TI_KEYFOB_RFID_PHONE_ID_PROPERTY_UUID
                                                        data:d];
}

-(void)BD_setLockControl:(BOOL)isLockOpen
{
    char bytes[QuickLock_TI_KEYFOB_CUSTOMER_UNLOCK_LOCKCONTROL_LENGTH];
    
    if (isLockOpen)
    {
        bytes[0] = 0x01;
    }
    else
    {
        bytes[0] = 0x00;
    }
    
    NSData *d = [[NSData alloc] initWithBytes:bytes length:QuickLock_TI_KEYFOB_CUSTOMER_UNLOCK_LOCKCONTROL_LENGTH];
    [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID
                                                 serviceUUID:QuickLock_TI_KEYFOB_CUSTOMER_SERVICE_UUID
                                          characteristicUUID:QuickLock_TI_KEYFOB_CUSTOMER_UNLOCK_LOCKCONTROL_PROPERTY_UUID
                                                        data:d];
}

-(void)BD_setLockOpenTime:(Byte)lockOpenSecond
{
    char bytes[QuickLock_TI_KEYFOB_CUSTOMER_UNLOCK_OPENTIME_LENGTH];
    
    bytes[0] = lockOpenSecond;
    
    NSData *d = [[NSData alloc] initWithBytes:bytes length:QuickLock_TI_KEYFOB_CUSTOMER_UNLOCK_OPENTIME_LENGTH];
    [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID
                                                 serviceUUID:QuickLock_TI_KEYFOB_CUSTOMER_SERVICE_UUID
                                          characteristicUUID:QuickLock_TI_KEYFOB_CUSTOMER_UNLOCK_OPENTIME_PROPERTY_UUID
                                                        data:d];
}

-(void)BD_setClearAllFingerPrints
{
    char bytes[QuickLock_TI_KEYFOB_FINGERPRINT_READ_LENGTH];
    
    bytes[0] = 0xFF;
    
    NSData *d = [[NSData alloc] initWithBytes:bytes length:QuickLock_TI_KEYFOB_FINGERPRINT_READ_LENGTH];
    [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID
                                                 serviceUUID:QuickLock_TI_KEYFOB_CUSTOMER_UNLOCK_SURE_SET_MAIN
                                          characteristicUUID:QuickLock_TI_KEYFOB_FINGERPRINT_CLEAR_ALL
                                                        data:d];
}

-(void)BD_setLEDTime:(Byte)LEDSecond
{
    char bytes[QuickLock_TI_KEYFOB_CUSTOMER_UNLOCK_OPENTIME_LENGTH];
    
    bytes[0] = LEDSecond;
    
    NSData *d = [[NSData alloc] initWithBytes:bytes length:QuickLock_TI_KEYFOB_CUSTOMER_UNLOCK_OPENTIME_LENGTH];
    [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID
                                                 serviceUUID:QuickLock_TI_KEYFOB_CUSTOMER_SERVICE_UUID
                                          characteristicUUID:QuickLock_TI_KEYFOB_CUSTOMER_UNLOCK_LED_LIGHT
                                                        data:d];
}

-(void)BD_setSureSet:(Byte)percent
{
    char bytes[QuickLock_TI_KEYFOB_CUSTOMER_UNLOCK_OPENTIME_LENGTH];
    
    bytes[0] = percent;
    
    NSData *d = [[NSData alloc] initWithBytes:bytes length:QuickLock_TI_KEYFOB_CUSTOMER_UNLOCK_OPENTIME_LENGTH];
    [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID
                                                 serviceUUID:QuickLock_TI_KEYFOB_CUSTOMER_UNLOCK_SURE_SET_MAIN
                                          characteristicUUID:QuickLock_TI_KEYFOB_CUSTOMER_UNLOCK_SURE_SET
                                                        data:d];
}

-(void)BD_setAlarm:(Byte)type delay:(Byte)delay
{
    char bytes[QuickLock_TI_KEYFOB_CUSTOMER_UNLOCK_ALARM_LENGTH];
    
    bytes[0] = type;
    bytes[1] = delay;
    
    NSData *d = [[NSData alloc] initWithBytes:bytes length:QuickLock_TI_KEYFOB_CUSTOMER_UNLOCK_ALARM_LENGTH];
    [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID
                                                 serviceUUID:QuickLock_TI_KEYFOB_CUSTOMER_SERVICE_UUID
                                          characteristicUUID:QuickLock_TI_KEYFOB_CUSTOMER_UNLOCK_ALARM_SET
                                                        data:d];
}

-(void)BD_setZwave:(Byte)param
{
    char bytes[QuickLock_TI_KEYFOB_SHARECODE_ZWAVE_LENGTH];
    
    bytes[0] = param;
    
    NSData *d = [[NSData alloc] initWithBytes:bytes length:QuickLock_TI_KEYFOB_SHARECODE_ZWAVE_LENGTH];
    [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID
                                                 serviceUUID:QuickLock_TI_KEYFOB_SHARECODE_ZWAVE_MAIN
                                          characteristicUUID:QuickLock_TI_KEYFOB_SHARECODE_ZWAVE
                                                        data:d];
}

-(void)BD_setUserName:(NSString*)userName
{
    char bytes[QuickLock_TI_KEYFOB_HISTORY_PHONE_ID_LENGTH];
    int i;
    
    //字节0的位置，放入手机名的有效长度
    if (userName.length < QuickLock_TI_KEYFOB_HISTORY_PHONE_ID_LENGTH)
    {
        bytes[0] = userName.length;
    }
    else
    {
        bytes[0] = QuickLock_TI_KEYFOB_HISTORY_PHONE_ID_LENGTH - 1;
    }
    
    //1-14字节，写入手机名，无效补0
    for (i = 0; i < QuickLock_TI_KEYFOB_HISTORY_PHONE_ID_LENGTH - 1; i++)
    {
        if (i < userName.length)
        {
            bytes[i + 1] = [userName characterAtIndex:i];
        }
        else
        {
            bytes[i + 1] = (char)0;
        }
    }
    
    NSData *d = [[NSData alloc] initWithBytes:bytes length:QuickLock_TI_KEYFOB_HISTORY_PHONE_ID_LENGTH];
    [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID
                                                 serviceUUID:QuickLock_TI_KEYFOB_HISTORY_SERVICE_UUID
                                          characteristicUUID:QuickLock_TI_KEYFOB_HISTORY_PHONE_ID_PROPERTY_UUID
                                                        data:d];
}

-(void)BD_getHistoryLog
{
    char bytes[QuickLock_TI_KEYFOB_HISTORY_CONTROL_LENGTH];
    
    bytes[0] = 0x01;
    
    NSData *d = [[NSData alloc] initWithBytes:bytes length:QuickLock_TI_KEYFOB_HISTORY_CONTROL_LENGTH];
    [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID
                                                 serviceUUID:QuickLock_TI_KEYFOB_HISTORY_SERVICE_UUID
                                          characteristicUUID:QuickLock_TI_KEYFOB_HISTORY_CONTROL_PROPERTY_UUID
                                                        data:d];
}

-(void)BD_getRFIDList
{
    char bytes[QuickLock_TI_KEYFOB_RFID_CONTROL_LENGTH];
    
    bytes[0] = 0x01;
    
    NSData *d = [[NSData alloc] initWithBytes:bytes length:QuickLock_TI_KEYFOB_RFID_CONTROL_LENGTH];
    
    [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID
                                                 serviceUUID:QuickLock_TI_KEYFOB_RFID_SERVICE_UUID
                                          characteristicUUID:QuickLock_TI_KEYFOB_RFID_CONTROL_PROPERTY_UUID
                                                        data:d];
}

-(void)BD_getRFIDAdd
{
    char bytes[QuickLock_TI_KEYFOB_RFID_CONTROL_LENGTH];
    
    bytes[0] = 0x01;
    
    NSData *d = [[NSData alloc] initWithBytes:bytes length:QuickLock_TI_KEYFOB_RFID_CONTROL_LENGTH];
    [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID
                                                 serviceUUID:QuickLock_TI_KEYFOB_RFID_SERVICE_UUID
                                          characteristicUUID:QuickLock_TI_KEYFOB_RFID_ADD_UUID
                                                        data:d];
}

-(void)BD_setUserIDForRFIDCard_UserId:(NSString*)strUserId{
    NSString *userName = @"";//[[NSUserDefaults standardUserDefaults] valueForKey:@"userId"];
    userName = strUserId;
    char bytes[QuickLock_TI_AaddMultiple_RFID_UserId_LENGTH];
    int i;
    
    //字节0的位置，放入手机名的有效长度
    if (userName.length < QuickLock_TI_AaddMultiple_RFID_UserId_LENGTH)
    {
        bytes[0] = userName.length;
    }
    else
    {
        bytes[0] = QuickLock_TI_AaddMultiple_RFID_UserId_LENGTH - 1;
    }
    //1-14字节，写入手机名，无效补0
    for (i = 0; i < QuickLock_TI_AaddMultiple_RFID_UserId_LENGTH - 1; i++)  {
        if (i < userName.length) {
            bytes[i] = [userName characterAtIndex:i];
        }
        else if (i == userName.length){
            bytes[i] = 0xFE;
        }
        else {
            bytes[i] = (char)0;
        }
    }
    
    NSData *d = [[NSData alloc] initWithBytes:bytes length:QuickLock_TI_AaddMultiple_RFID_UserId_LENGTH];
    [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID
                                                 serviceUUID:QuickLock_TI_AaddMultiple_RFID_SERVICE_UUID
                                          characteristicUUID:QuickLock_TI_AaddMultiple_RFID_UserId_Characteristics_UUID
                                                        data:d];
}

-(void)BD_setRFIDWriter {
    char bytes1[QuickLock_TI_KEYFOB_RFID_CONTROL_LENGTH];
    bytes1[0] = 0x01;
    
    NSData *d1 = [[NSData alloc] initWithBytes:bytes1 length:20];
    [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID
                                                 serviceUUID:QuickLock_TI_AaddMultiple_RFID_SERVICE_UUID
                                          characteristicUUID:QuickLock_TI_WriteControl_RFID_ADD_Multiple_UUID
                                                        data:d1];
}

-(void)BD_getAllRFID {
    char bytes1[QuickLock_TI_KEYFOB_RFID_CONTROL_LENGTH];
    bytes1[0] = 0x01;
    
    NSData *d1 = [[NSData alloc] initWithBytes:bytes1 length:QuickLock_TI_KEYFOB_RFID_CONTROL_LENGTH];
    [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID
                                                 serviceUUID:QuickLock_TI_AaddMultiple_RFID_SERVICE_UUID
                                          characteristicUUID:QuickLock_TI_Write_Status_RFID_UUID
                                                        data:d1];
}

// cancel learning fingerprint
-(void)BD_getCancelFingerprintLearn
{
    char bytes[QuickLock_TI_KEYFOB_FINGERPRINT_LEARN_LENGTH];
    
    //    (WRITE) 1=start learning fingerprint, 0=cancel
    bytes[0] = 0x00;
    
    NSData *d = [[NSData alloc] initWithBytes:bytes length:QuickLock_TI_KEYFOB_FINGERPRINT_LEARN_LENGTH];
    [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID
                                                 serviceUUID:QuickLock_TI_KEYFOB_CUSTOMER_UNLOCK_SURE_SET_MAIN
                                          characteristicUUID:QuickLock_TI_KEYFOB_FINGERPRINT_LEARN
                                                        data:d];
}

// leainger p[rint
-(void)BD_getFingerprintLearn
{
    char bytes[QuickLock_TI_KEYFOB_FINGERPRINT_LEARN_LENGTH];
    
    //    (WRITE) 1=start learning fingerprint, 0=cancel
    bytes[0] = 0x01;
    
    NSData *d = [[NSData alloc] initWithBytes:bytes length:QuickLock_TI_KEYFOB_FINGERPRINT_LEARN_LENGTH];
    [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID
                                                 serviceUUID:QuickLock_TI_KEYFOB_CUSTOMER_UNLOCK_SURE_SET_MAIN
                                          characteristicUUID:QuickLock_TI_KEYFOB_FINGERPRINT_LEARN
                                                        data:d];
}

@end
