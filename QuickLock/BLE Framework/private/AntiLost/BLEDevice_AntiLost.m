//
//  BLEDevice_AntiLost.m
//  
//  ------------------------------------------------------------------
//  防丢器子类实现部分
//  ------------------------------------------------------------------

//  ------------------------------------------------------------------
//  防丢器子类的常量定义
//  ------------------------------------------------------------------

#ifndef AntiLost_TI_ItonBLEDefines_h
#define AntiLost_TI_ItonBLEDefines_h

// Defines for the TI CC2540 keyfob peripheral

#define TI_KEYFOB_PROXIMITY_ALERT_UUID                      0x1802
#define TI_KEYFOB_PROXIMITY_ALERT_PROPERTY_UUID             0x2a06
#define TI_KEYFOB_PROXIMITY_ALERT_ON_VAL                    0x01
#define TI_KEYFOB_PROXIMITY_ALERT_HIGH_VAL                  0x02//Custom
#define TI_KEYFOB_PROXIMITY_ALERT_OFF_VAL                   0x00
#define TI_KEYFOB_PROXIMITY_ALERT_WRITE_LEN                 1

#define TI_KEYFOB_PROXIMITY_TX_PWR_SERVICE_UUID             0x1804
#define TI_KEYFOB_PROXIMITY_TX_PWR_NOTIFICATION_UUID        0x2A07
#define TI_KEYFOB_PROXIMITY_TX_PWR_NOTIFICATION_READ_LEN    1
#define TI_KEYFOB_PROXIMITY_TX_PWR_ENABLER_UUID             0x2902//Custom
#define TI_KEYFOB_PROXIMITY_TX_PWR_LEVEL0_VAL               0xE9//Custom
#define TI_KEYFOB_PROXIMITY_TX_PWR_LEVEL1_VAL               0xFA//Custom
#define TI_KEYFOB_PROXIMITY_TX_PWR_LEVEL2_VAL               0x00//Custom
#define TI_KEYFOB_PROXIMITY_TX_PWR_LEVEL3_VAL               0x04//Custom


//#define TI_KEYFOB_BATT_SERVICE_UUID                         0xFFB0
//#define TI_KEYFOB_LEVEL_SERVICE_UUID                        0xFFB1
#define TI_KEYFOB_BATT_SERVICE_UUID                         0x180F//Custom
#define TI_KEYFOB_LEVEL_SERVICE_UUID                        0x2A19//Custom

#define TI_KEYFOB_POWER_STATE_UUID                          0xFFB2
#define TI_KEYFOB_LEVEL_SERVICE_READ_LEN                    1

#define TI_KEYFOB_ACCEL_SERVICE_UUID                        0xFFA0
#define TI_KEYFOB_ACCEL_ENABLER_UUID                        0xFFA1
#define TI_KEYFOB_ACCEL_RANGE_UUID                          0xFFA2
#define TI_KEYFOB_ACCEL_READ_LEN                            1
#define TI_KEYFOB_ACCEL_X_UUID                              0xFFA3
#define TI_KEYFOB_ACCEL_Y_UUID                              0xFFA4
#define TI_KEYFOB_ACCEL_Z_UUID                              0xFFA5

#define TI_KEYFOB_KEYS_SERVICE_UUID                         0xFFE0
#define TI_KEYFOB_KEYS_NOTIFICATION_UUID                    0xFFE1
#define TI_KEYFOB_KEYS_NOTIFICATION_READ_LEN                1

#define TI_KEYFOB_CUSTOM_SERVICE_UUID                       0xFF00//Custom
#define TI_KEYFOB_SLEEP_ATT_UUID                            0xFF01//Custom
#define TI_KEYFOB_ADDRESS_CHECK_ATT_UUID                    0xFF02//Custom
#define TI_KEYFOB_SLEEP_ON_VAL                              0x06//Custom
#define TI_KEYFOB_ADDRESS_CHECK_ATT_NOTIFICATION_UUID       0xFF05//Custom
//#define TI_KEYFOB_ADDRESS_CHECK_ATT_UUID                    0xFF04//Custom

#define TI_KEYFOB_ADDRESS_SERVICE_UUID                      0x180A//Custom
#define TI_KEYFOB_ADDRESS_READ_UUID                         0x2A23//Custom
#define TI_KEYFOB_ADDRESS_READ_LEN                          8//Custom

#endif

//  ------------------------------------------------------------------
//  防丢器子类校验类
//  ------------------------------------------------------------------

#import "BLEDevice_AntiLost.h"
#import "BLEManager.h"


@interface ItonBLECheck : NSObject

// 读取设备地址计算后获得校验值，向设备获取校验值
@property (nonatomic) int bAddressCheckValue;
@property (nonatomic) int bDeviceCheckValue;
@property (nonatomic) int bCheckValue;

@end

@implementation ItonBLECheck

@synthesize bAddressCheckValue;
@synthesize bDeviceCheckValue;
@synthesize bCheckValue;

@end

//  ------------------------------------------------------------------

@implementation BLEDevice_AntiLost

@synthesize delegateBLEDeviceResult_Antilost;

ItonBLECheck *cbpcheck;

-(id)init:(id)delegete
{
    [self setDelegateBLEDeviceResult_Antilost:delegete];
    return [super init:delegete];
}

//  ------------------------------------------------------------------
//  内部私有函数部分
//  ------------------------------------------------------------------

//  硬件数据接收函数 - 重载具体实现部分，每个子类不同
- (void)didUpdateValueForCharacteristic_override:(CBCharacteristic *)characteristic
{
    UInt16 characteristicUUID = [BLEPublic CBUUIDToInt:characteristic.UUID];
    switch(characteristicUUID)
    {
        case TI_KEYFOB_LEVEL_SERVICE_UUID:      //电池电量数据更新：5s一次，保持程序始终在iOS后台运行不被挂起
        {
            char batlevel;
            [characteristic.value getBytes:&batlevel length:TI_KEYFOB_LEVEL_SERVICE_READ_LEN];
            [delegateBLEDeviceResult_Antilost BR_batteryUpdated:(float)batlevel DeviceID:DeviceID];
            if(AntiLost_Debug)
            {
                NSLog(@"didUpdateValueForCharacteristic --- batteryUpdate --- %f", (float)batlevel);
            }
            
            //[self BD_readRSSI];
            //[self BD_enableBattery:NO];
            break;
        }
        case TI_KEYFOB_KEYS_NOTIFICATION_UUID:  //按键更新
        {
            char keys;
            [characteristic.value getBytes:&keys length:TI_KEYFOB_KEYS_NOTIFICATION_READ_LEN];
            if (keys == 0x12)
            {
                    [delegateBLEDeviceResult_Antilost BR_ringkeyValuesUpdated:DeviceID];
            }
            // 2014-04-30 wangyang
            // New pictures
            else if (keys == 0x01)
            {
                    [delegateBLEDeviceResult_Antilost BR_snap:DeviceID];
            }
            else if (keys == 0x02)
            {
                    [delegateBLEDeviceResult_Antilost BR_cameraOperation:DeviceID];
            }
            break;
        }
        case TI_KEYFOB_PROXIMITY_TX_PWR_NOTIFICATION_UUID:  //发射信号db值读取
        {
            char TXLevel;
            [characteristic.value getBytes:&TXLevel length:TI_KEYFOB_PROXIMITY_TX_PWR_NOTIFICATION_READ_LEN];
            [delegateBLEDeviceResult_Antilost BR_didUpdateTxPower:TXLevel DeviceID:DeviceID];
            break;
        }
        case TI_KEYFOB_ADDRESS_READ_UUID:       //地址读取：校验过程的一部分
        {
            Byte bAddress[TI_KEYFOB_ADDRESS_READ_LEN];
            [characteristic.value getBytes:&bAddress length:TI_KEYFOB_ADDRESS_READ_LEN];
            int b1,b2,b3,b4,b5;
            b1 = (int)bAddress[7];
            b2 = (int)bAddress[6];
            b3 = (int)bAddress[5];
            b4 = (int)bAddress[2];
            b5 = (int)bAddress[1];
            if(AntiLost_Debug)
            {
                NSLog(@"didUpdateValueForCharacteristic --- 180a --- Address: %d,%d,%d,%d,%d,%d,%d,%d \r\n",bAddress[0],bAddress[1],bAddress[2],bAddress[3],bAddress[4],bAddress[5],bAddress[6],bAddress[7]);
            }
            //if(b1 == 0xDC && b2 == 0x2c && b3 == 0x26)
            //{
                Byte bNum1,bNum2;
                bNum1 = b1 + b3 + b5;
                bNum2 = b2 + b4;
                
                Byte bCheck = bNum1 ^ bNum2;
                if(AntiLost_Debug)
                {
                    NSLog(@"didUpdateValueForCharacteristic --- 180a --- CheckValue: %d \r\n",bCheck);
                }
                [self setAddressCheckValue:bCheck];
                
//                UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"" message:strAddress delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//                [alertview show];
            //}
            //else
            //{
            //    [delegateBLEDeviceResult_Antilost BR_verifyItonBLEResult:false DeviceID:DeviceID];
                
//                UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"" message:@"Address Err" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//                [alertview show];
            //}
            break;
        }
        case 0xFF05:    //校验值读取：校验过程的一部分
//        case 0xFF02:
//        case 0xFF01:
        {
            Byte bDevice;
            [characteristic.value getBytes:&bDevice length:TI_KEYFOB_ACCEL_READ_LEN];
            if(AntiLost_Debug)
            {
                NSLog(@"didUpdateValueForCharacteristic --- FF00 --- Device CheckValue: %d \r\n",bDevice);
            }
            [self checkDeviceValue:(int)bDevice];
            
//            UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"" message:[NSString stringWithFormat:@"硬件上传校验值: %d",bDevice] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//            [alertview show];
            break;
        }
        case 0xffa6:    //地址单独读取，无返回？？？
        {
            Byte bAddress[4];
            [characteristic.value getBytes:&bAddress length:4];
            int b1,b2,b3,b4;
            b1 = (int)bAddress[3];
            b2 = (int)bAddress[2];
            b3 = (int)bAddress[1];
            b4 = (int)bAddress[0];
            if(AntiLost_Debug)
            {
                NSLog(@"didUpdateValueForCharacteristic --- 0xffa6 --- : %d,%d,%d,%d",b1,b2,b3,b4);
            }
            break;
        }
        default:
        {
            if(AntiLost_Debug)
            {
                NSLog(@"didUpdateValueForCharacteristic --- default --- characteristicUUID:%d" , characteristicUUID);
            }
            break;
        }
    }
}

// 获取地址校验值
-(void)setAddressCheckValue:(int)cValue
{
    cbpcheck.bCheckValue = arc4random() % 256;
    
    // 如果bCheckValue是偶数则校验值加2次随即数否则加1次随即数
    if(cbpcheck.bCheckValue % 2 == 0)
    {
        cbpcheck.bAddressCheckValue = cValue + cbpcheck.bCheckValue * 2;
    }
    else
    {
        cbpcheck.bAddressCheckValue = cValue + cbpcheck.bCheckValue;
    }
    
    if(AntiLost_Debug)
    {
        NSLog(@"Address CheckValue: %d  random: %d  result: %d", cValue, cbpcheck.bCheckValue, cbpcheck.bAddressCheckValue);
    }
    [self getDeviceCheckValue:cbpcheck.bCheckValue];
}

-(void)getDeviceCheckValue:(int)value
{
    char data;
    data = value;
    NSData *d = [[NSData alloc] initWithBytes:&data length:1];    
    
    [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID serviceUUID:TI_KEYFOB_CUSTOM_SERVICE_UUID characteristicUUID:TI_KEYFOB_ADDRESS_CHECK_ATT_UUID data:d];
    
    if(AntiLost_Debug)
    {
        NSLog(@"getDeviceCheckValue --- ==============");
    }
}


-(void)checkDeviceValue:(int)cValue
{
    cbpcheck.bDeviceCheckValue = cValue;
    if(AntiLost_Debug)
    {
        NSLog(@"bAddressCheckValue: %d  bDeviceCheckValue: %d", cbpcheck.bAddressCheckValue, cbpcheck.bDeviceCheckValue);
    }
    if ((cbpcheck.bAddressCheckValue >= 0) && (cbpcheck.bDeviceCheckValue >= 0) && ((cbpcheck.bAddressCheckValue % 0x100) == cbpcheck.bDeviceCheckValue))
    {
        [delegateBLEDeviceResult_Antilost BR_verifyItonBLEResult:true DeviceID:DeviceID];
    }
    else
    {
        [delegateBLEDeviceResult_Antilost BR_verifyItonBLEResult:true DeviceID:DeviceID];
    }
}

-(void)enableATT
{
    char data = 0x01;
    NSData *d = [[NSData alloc] initWithBytes:&data length:1];
    
    [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID serviceUUID:TI_KEYFOB_CUSTOM_SERVICE_UUID characteristicUUID:TI_KEYFOB_ADDRESS_CHECK_ATT_NOTIFICATION_UUID data:d];

    [[BLEManager sharedInstance] notification:DeviceID serviceUUID:TI_KEYFOB_CUSTOM_SERVICE_UUID characteristicUUID:TI_KEYFOB_ADDRESS_CHECK_ATT_NOTIFICATION_UUID on:YES];
}

//  ------------------------------------------------------------------
//  协议Protocol_BLEDevice_Antilost的实现部分
//  ------------------------------------------------------------------

-(void)BD_startVerifyItonBLE
{
    [self enableATT]; // Before starting calibration, open enabled, otherwise not receive data
    
    cbpcheck = [ItonBLECheck new];
    cbpcheck.bCheckValue = 0;
    cbpcheck.bAddressCheckValue = 0;
    cbpcheck.bDeviceCheckValue = 0;
    
    [[BLEManager sharedInstance] readValueForCharacteristic:DeviceID serviceUUID:TI_KEYFOB_ADDRESS_SERVICE_UUID characteristicUUID:TI_KEYFOB_ADDRESS_READ_UUID];
    
    if(AntiLost_Debug)
    {
        NSLog(@"BD_startVerifyItonBLE --- +++++++++++");
    }
}

-(void)BD_buzzerAlarm:(bool)bLarge
{
    Byte buzVal;
    
    if (bLarge)
    {
        buzVal = TI_KEYFOB_PROXIMITY_ALERT_HIGH_VAL;
    }
    else
    {
        buzVal = TI_KEYFOB_PROXIMITY_ALERT_ON_VAL;
    }
    NSData *d = [[NSData alloc] initWithBytes:&buzVal length:TI_KEYFOB_PROXIMITY_ALERT_WRITE_LEN];
    
    [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID serviceUUID:TI_KEYFOB_PROXIMITY_ALERT_UUID characteristicUUID:TI_KEYFOB_PROXIMITY_ALERT_PROPERTY_UUID data:d];
}

-(void)BD_buzerStop
{
    Byte buzVal = TI_KEYFOB_PROXIMITY_ALERT_OFF_VAL;
    NSData *d = [[NSData alloc] initWithBytes:&buzVal length:TI_KEYFOB_PROXIMITY_ALERT_WRITE_LEN];
    
    [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID serviceUUID:TI_KEYFOB_PROXIMITY_ALERT_UUID characteristicUUID:TI_KEYFOB_PROXIMITY_ALERT_PROPERTY_UUID data:d];
}


-(void)BD_readTXPower
{
    [[BLEManager sharedInstance] readValueForCharacteristic:DeviceID serviceUUID:TI_KEYFOB_PROXIMITY_TX_PWR_SERVICE_UUID characteristicUUID:TI_KEYFOB_PROXIMITY_TX_PWR_NOTIFICATION_UUID];
}


-(void)BD_enableButtons:(BOOL)isEnable
{
    [[BLEManager sharedInstance] notification:DeviceID serviceUUID:TI_KEYFOB_KEYS_SERVICE_UUID characteristicUUID:TI_KEYFOB_KEYS_NOTIFICATION_UUID on:isEnable];
}


-(void)BD_enableTXPower:(BOOL)isEnable
{
    [[BLEManager sharedInstance] notification:DeviceID serviceUUID:TI_KEYFOB_PROXIMITY_TX_PWR_SERVICE_UUID characteristicUUID:TI_KEYFOB_PROXIMITY_TX_PWR_NOTIFICATION_UUID on:isEnable];
}


-(void)BD_enableBattery:(BOOL)isEnable
{
    char data;
    
    if (isEnable)
    {
        data = 0x01;
    }
    else
    {
        data = 0x00;
    }
    NSData *d = [[NSData alloc] initWithBytes:&data length:1];
    
    [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID serviceUUID:TI_KEYFOB_BATT_SERVICE_UUID characteristicUUID:TI_KEYFOB_LEVEL_SERVICE_UUID data:d];
    
    [[BLEManager sharedInstance] notification:DeviceID serviceUUID:TI_KEYFOB_BATT_SERVICE_UUID characteristicUUID:TI_KEYFOB_LEVEL_SERVICE_UUID on:isEnable];
}

-(void)BD_writeTXPower:(int)nLevel
{
    char data;
    NSData *d;
    
    switch (nLevel)
    {
        case 3:
            data = TI_KEYFOB_PROXIMITY_TX_PWR_LEVEL0_VAL;
            break;
        case 2:
            data = TI_KEYFOB_PROXIMITY_TX_PWR_LEVEL1_VAL;
            break;
        case 1:
            data = TI_KEYFOB_PROXIMITY_TX_PWR_LEVEL2_VAL;
            break;
        case 0:
        default:
            data = TI_KEYFOB_PROXIMITY_TX_PWR_LEVEL3_VAL;
            break;
    }
    d = [[NSData alloc] initWithBytes:&data length:1];
    
    [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID serviceUUID:TI_KEYFOB_PROXIMITY_TX_PWR_SERVICE_UUID characteristicUUID:TI_KEYFOB_PROXIMITY_TX_PWR_NOTIFICATION_UUID data:d];

    [[BLEManager sharedInstance] notification:DeviceID serviceUUID:TI_KEYFOB_PROXIMITY_TX_PWR_SERVICE_UUID characteristicUUID:TI_KEYFOB_PROXIMITY_TX_PWR_NOTIFICATION_UUID on:NO];
}

-(void)BD_setPeripheralSleep
{
    char data = TI_KEYFOB_SLEEP_ON_VAL;
    NSData *d = [[NSData alloc] initWithBytes:&data length:1];
    
    [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID serviceUUID:TI_KEYFOB_CUSTOM_SERVICE_UUID characteristicUUID:TI_KEYFOB_SLEEP_ATT_UUID data:d];
}

//  2014-04-30 wangyang
-(void)BD_enableCamera:(BOOL)isEnable
{
        [[BLEManager sharedInstance] notification:DeviceID
                                      serviceUUID:TI_KEYFOB_KEYS_SERVICE_UUID
                               characteristicUUID:TI_KEYFOB_KEYS_NOTIFICATION_UUID
                                               on:isEnable];
}

@end
