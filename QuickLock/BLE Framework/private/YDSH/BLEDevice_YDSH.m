//
//  BLEDevice_YDSH.m
//
//  Copyright (c) 2014, Vicro Technology Limited. All rights reserved.
//
//  ------------------------------------------------------------------
//  运动手环子类实现部分
//  ------------------------------------------------------------------

//  ------------------------------------------------------------------
//  运动手环子类的常量定义
//  ------------------------------------------------------------------

#ifndef YDSH_TI_ItonBLEDefines_h
#define YDSH_TI_ItonBLEDefines_h

#define YDSH_TI_KEYFOB_VERSION_SERVICE_UUID                                     0x180A
#define YDSH_TI_KEYFOB_WAKEUPALERT_PROPERTY_UUID                                0x2A06
#define YDSH_TI_KEYFOB_SYSTEM_ID_PROPERTY_UUID                                  0x2A23
#define YDSH_TI_KEYFOB_SYSTEM_ID_LENGTH                                         8
#define YDSH_TI_KEYFOB_MODEL_NUMBER_PROPERTY_UUID                               0x2A24
#define YDSH_TI_KEYFOB_MODEL_NUMBER_LENGTH                                      16
#define YDSH_TI_KEYFOB_FIRMWARE_REVISION_PROPERTY_UUID                          0x2A26
#define YDSH_TI_KEYFOB_FIRMWARE_REVISION_LENGTH                                 7
#define YDSH_TI_KEYFOB_DISCONNECT_ENABLE_PROPERTY_UUID                          0xFF01
#define YDSH_TI_KEYFOB_DISCONNECT_ENABLE_LENGTH                                 1
#define YDSH_TI_KEYFOB_PROTOCOL_VERSION_PROPERTY_UUID                           0xFF02
#define YDSH_TI_KEYFOB_PROTOCOL_VERSION_LENGTH                                  1

#define YDSH_TI_KEYFOB_BATT_SERVICE_UUID                                        0x180F
#define YDSH_TI_KEYFOB_BATT_STATE_PROPERTY_UUID                                 0x2A19
#define YDSH_TI_KEYFOB_BATT_STATE_LENGTH                                        1

#define YDSH_TI_KEYFOB_TIME_SERVICE_UUID                                        0x1805
#define YDSH_TI_KEYFOB_TIME_SYNCHRONOUS_PROPERTY_UUID                           0x2A2B
#define YDSH_TI_KEYFOB_TIME_LENGTH                                              4
#define YDSH_TI_KEYFOB_CLOCK_CONTROL_PROPERTY_UUID                              0xFF81
#define YDSH_TI_KEYFOB_CLOCK_CONTROL_LENGTH                                     1
#define YDSH_TI_KEYFOB_CLOCK_DATA_PROPERTY_UUID                                 0xFF82
#define YDSH_TI_KEYFOB_CLOCK_DATA_LENGTH                                        4

#define YDSH_TI_KEYFOB_PEDOMETER_SERVICE_UUID                                   0x1814
#define YDSH_TI_KEYFOB_PEDOMETER_ACCELENABLE_PROPERTY_UUID                      0xFFA1
#define YDSH_TI_KEYFOB_PEDOMETER_ACCELENABLE_LENGTH                             1
#define YDSH_TI_KEYFOB_ACCELENABLE_X_PROPERTY_UUID                              0xFFA3
#define YDSH_TI_KEYFOB_ACCELENABLE_Y_PROPERTY_UUID                              0xFFA4
#define YDSH_TI_KEYFOB_ACCELENABLE_Z_PROPERTY_UUID                              0xFFA5
#define YDSH_TI_KEYFOB_ACCELENABLE_DATA_LENGTH                                  1
#define YDSH_TI_KEYFOB_PEDOMETER_COUNTENABLE_PROPERTY_UUID                      0xFFA6
#define YDSH_TI_KEYFOB_PEDOMETER_COUNTE_LENGTH                                  16
#define YDSH_TI_KEYFOB_SPORT_REMINDER_PROPERTY_UUID                             0xFFA7
#define YDSH_TI_KEYFOB_SPORT_REMINDER_LENGTH                                    6

#define YDSH_TI_KEYFOB_OFFLINE_SERVICE_UUID                                     0xFFC0
#define YDSH_TI_KEYFOB_OFFLINE_SELECT_DAY_PROPERTY_UUID                         0xFFC1
#define YDSH_TI_KEYFOB_OFFLINE_DATA_DAY_PROPERTY_UUID                           0xFFC2
#define YDSH_TI_KEYFOB_OFFLINE_SELECT_MINUTEDAY_PROPERTY_UUID                   0xFFC3
#define YDSH_TI_KEYFOB_OFFLINE_DATA_MINUTEDAY_PROPERTY_UUID                     0xFFC4
#define YDSH_TI_KEYFOB_OFFLINE_DATA_DAY_LENGTH                                  20
#define YDSH_TI_KEYFOB_OFFLINE_SELECT_LENGTH                                    1

#define YDSH_TI_KEYFOB_SLEEP_SERVICE_UUID                                       0xFF70
#define YDSH_TI_KEYFOB_SLEEP_CONTROL_PROPERTY_UUID                              0xFF71
#define YDSH_TI_KEYFOB_SLEEP_CONTROL_LENGTH                                     1
#define YDSH_TI_KEYFOB_SLEEP_DATA_PROPERTY_UUID                                 0xFF72
#define YDSH_TI_KEYFOB_SLEEP_DATA_LENGTH                                        20
#define YDSH_TI_KEYFOB_SLEEP_TIMING_PROPERTY_UUID                               0xFF73
#define YDSH_TI_KEYFOB_SLEEP_TIMING_LENGTH                                      12

#define YDSH_TI_KEYFOB_CUSTOMER_SERVICE_UUID                                    0xFFD0
#define YDSH_TI_KEYFOB_CUSTOMER_PARAMETER_PROPERTY_UUID                         0xFFD1
#define YDSH_TI_KEYFOB_CUSTOMER_PARAMETER_LENGTH                                11
#define YDSH_TI_KEYFOB_CUSTOMER_CAMERA_PROPERTY_UUID                            0xFFD2
#define YDSH_TI_KEYFOB_CUSTOMER_CAMERA_LENGTH                                   1
#define YDSH_TI_KEYFOB_CUSTOMER_CALLING_PROPERTY_UUID                           0xFFD3
#define YDSH_TI_KEYFOB_CUSTOMER_CALLING_LENGTH_1                                1
#define YDSH_TI_KEYFOB_CUSTOMER_CALLING_LENGTH_2                                2
#define YDSH_TI_KEYFOB_CUSTOMER_SMS_PROPERTY_UUID                               0xFFD4 //短信提醒，未用到 2014-09-03
#define YDSH_TI_KEYFOB_CUSTOMER_SMS_LENGTH                                      1
#define YDSH_TI_KEYFOB_CUSTOMER_SCREEN_LASTING_PROPERTY_UUID                    0xFFD5 //屏幕点亮时间 2014-09-03
#define YDSH_TI_KEYFOB_CUSTOMER_SCREEN_LASTING_LENGTH                           1


#endif

#import "BLEDevice_YDSH.h"
#import "BLEManager.h"

@implementation BLEDevice_YDSH

@synthesize delegateBLEDeviceResult_YDSH;

//  Data staging points and control
BOOL YDSH_isStartReceiveOfflineMinuteData;
BOOL YDSH_isContinueReceiveOfflineMinuteData;
float YDSH_receiveOverTime = 0.2f;
NSDate *YDSH_curMinuteDayDate;
NSString *YDSH_curMinuteData;
int YDSH_curMinuteCount;

//  Sleep staging data and control
BOOL YDSH_isStartReceiveSleepData;
BOOL YDSH_isContinueReceiveSleepData;
float YDSH_receiveSleepOverTime = 0.2f;
NSDate *YDSH_curSleepEnterDate;
NSDate *YDSH_curSleepExitDate;
NSString *YDSH_curSleepData;
int YDSH_curSleepCount;

-(id)init:(id)delegete
{
        // Historical data sub-control flag initialization
        YDSH_isStartReceiveOfflineMinuteData = FALSE;
        YDSH_isContinueReceiveOfflineMinuteData = FALSE;
        YDSH_curMinuteData = @"";
        YDSH_curMinuteCount = 0;
        
        // Sleep data control initialization flag
        YDSH_isStartReceiveSleepData = FALSE;
        YDSH_isContinueReceiveSleepData = FALSE;
        YDSH_curSleepData = @"";
        YDSH_curSleepCount = 0;
        
        [self setDelegateBLEDeviceResult_YDSH:delegete];
        return [super init:delegete];
}


//  ------------------------------------------------------------------
//  Internal private function part
//  ------------------------------------------------------------------

//  Hardware data receiver function - Overload concrete implementation part, different for each subclass
- (void)didUpdateValueForCharacteristic_override:(CBCharacteristic *)characteristic
{
        UInt16 characteristicUUID = [BLEPublic CBUUIDToInt:characteristic.UUID];
        int nLength = (int)characteristic.value.length;
        Byte bAddress[nLength];
        [characteristic.value getBytes:&bAddress length:nLength];
        
        switch(characteristicUUID)
        {
                case YDSH_TI_KEYFOB_SYSTEM_ID_PROPERTY_UUID:
                {
                        [self processValue_SYSTEM_ID:bAddress length:nLength];
                        break;
                }
                case YDSH_TI_KEYFOB_MODEL_NUMBER_PROPERTY_UUID:
                {
                        [self processValue_MODEL_NUMBER:bAddress length:nLength];
                        break;
                }
                case YDSH_TI_KEYFOB_FIRMWARE_REVISION_PROPERTY_UUID:
                {
                        [self processValue_FIRMWARE_REVISION:bAddress length:nLength];
                        break;
                }
                case YDSH_TI_KEYFOB_DISCONNECT_ENABLE_PROPERTY_UUID:
                {
                        [self processValue_DISCONNECT_ENABLE:bAddress length:nLength];
                        break;
                }
                case YDSH_TI_KEYFOB_PROTOCOL_VERSION_PROPERTY_UUID:
                {
                        [self processValue_PROTOCOL_VERSION:bAddress length:nLength];
                        break;
                }
        
                case YDSH_TI_KEYFOB_BATT_STATE_PROPERTY_UUID:
                {
                        [self processValue_BATT_STATE:bAddress length:nLength];
                        break;
                }
                case YDSH_TI_KEYFOB_TIME_SYNCHRONOUS_PROPERTY_UUID:
                {
                        [self processValue_TIME_SYNCHRONOUS:bAddress length:nLength];
                        break;
                }
                case YDSH_TI_KEYFOB_CLOCK_CONTROL_PROPERTY_UUID:
                {
                        [self processValue_CLOCK_CONTROL:bAddress length:nLength];
                        break;
                }
                case YDSH_TI_KEYFOB_CLOCK_DATA_PROPERTY_UUID:
                {
                        [self processValue_CLOCK_DATA:bAddress length:nLength];
                        break;
                }
                case YDSH_TI_KEYFOB_PEDOMETER_ACCELENABLE_PROPERTY_UUID:
                {
                        [self processValue_PEDOMETER_ACCELENABLE:bAddress length:nLength];
                        break;
                }
                case YDSH_TI_KEYFOB_ACCELENABLE_X_PROPERTY_UUID:
                {
                        [self processValue_ACCELENABLE_X:bAddress length:nLength];
                        break;
                }
                case YDSH_TI_KEYFOB_ACCELENABLE_Y_PROPERTY_UUID:
                {
                        [self processValue_ACCELENABLE_Y:bAddress length:nLength];
                        break;
                }
                case YDSH_TI_KEYFOB_ACCELENABLE_Z_PROPERTY_UUID:
                {
                        [self processValue_ACCELENABLE_Z:bAddress length:nLength];
                        break;
                }
                case YDSH_TI_KEYFOB_PEDOMETER_COUNTENABLE_PROPERTY_UUID:
                {
                        [self processValue_PEDOMETER_COUNTENABLE:bAddress length:nLength];
                        break;
                }
                case YDSH_TI_KEYFOB_SPORT_REMINDER_PROPERTY_UUID:
                {
                        [self processValue_SPORT_REMINDER:bAddress length:nLength];
                        break;
                }
                case YDSH_TI_KEYFOB_OFFLINE_SELECT_DAY_PROPERTY_UUID:
                {
                        [self processValue_OFFLINE_SELECT_DAY:bAddress length:nLength];
                        break;
                }
                case YDSH_TI_KEYFOB_OFFLINE_DATA_DAY_PROPERTY_UUID:
                {
                        [self processValue_OFFLINE_DATA_DAY:bAddress length:nLength];
                        break;
                }
                case YDSH_TI_KEYFOB_OFFLINE_SELECT_MINUTEDAY_PROPERTY_UUID:
                {
                        [self processValue_OFFLINE_SELECT_MINUTEDAY:bAddress length:nLength];
                        break;
                }
                case YDSH_TI_KEYFOB_OFFLINE_DATA_MINUTEDAY_PROPERTY_UUID:
                {
                        [delegateBLEDeviceResult_YDSH BR_offlineData_MinutesOfDay_receiveBytes:bAddress length:nLength DeviceID:DeviceID];
                        [self processValue_OFFLINE_DATA_MINUTEDAY:bAddress length:nLength];
                        break;
                }
                case YDSH_TI_KEYFOB_SLEEP_CONTROL_PROPERTY_UUID:
                {
                        [self processValue_SLEEP_CONTROL:bAddress length:nLength];
                        break;
                }
                case YDSH_TI_KEYFOB_SLEEP_DATA_PROPERTY_UUID:
                {
                        [self processValue_SLEEP_DATA:bAddress length:nLength];
                        break;
                }
                case YDSH_TI_KEYFOB_SLEEP_TIMING_PROPERTY_UUID:
                {
                        [self processValue_SLEEP_TIMING:bAddress length:nLength];
                        break;
                }
                case YDSH_TI_KEYFOB_WAKEUPALERT_PROPERTY_UUID:
                {
                        [self processValue_WAKEUPALERT:bAddress length:nLength];
                        break;
                }
                case YDSH_TI_KEYFOB_CUSTOMER_PARAMETER_PROPERTY_UUID:
                {
                        [self processValue_CUSTOMER_PARAMETER:bAddress length:nLength];
                        break;
                }
                case YDSH_TI_KEYFOB_CUSTOMER_CAMERA_PROPERTY_UUID:
                {
                        // ISW Bge 2014-12-13 测试语句，拍照加临时弹出
                        //                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"收到硬件的拍照命令" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                        //                    [alert show];
                        
                        [self processValue_CUSTOMER_CAMERA:bAddress length:nLength];
                        break;
                }
                case YDSH_TI_KEYFOB_CUSTOMER_SCREEN_LASTING_PROPERTY_UUID:
                {
                        [self processValue_CUSTOMER_SCREEN_LASTING:bAddress length:nLength];
                        break;
                }
                default:
                {
                        if(AntiLost_Debug)
                        {
                                NSLog(@"didUpdateValueForCharacteristic --- default --- characteristicUUID:%d" , characteristicUUID);
                        }
                        break;
                }
        }
}

-(void)processValue_SYSTEM_ID:(Byte *)bAddress length:(int)length
{
        if (length < YDSH_TI_KEYFOB_SYSTEM_ID_LENGTH)
        {
                return;
        }
        
        NSString *systemID = @"";
        
        systemID = [NSString stringWithFormat:@"%.2X %.2X %.2X %.2X %.2X %.2X %.2X %.2X", bAddress[0], bAddress[1], bAddress[2], bAddress[3], bAddress[4], bAddress[5], bAddress[6], bAddress[7]];
        
        [delegateBLEDeviceResult_YDSH BR_systemID:systemID DeviceID:DeviceID];
}

-(void)processValue_MODEL_NUMBER:(Byte *)bAddress length:(int)length
{
        if (length < YDSH_TI_KEYFOB_MODEL_NUMBER_LENGTH)
        {
                return;
        }
        
        NSString *modelNumber = @"";
        
        int i;
        
        for (i = 0; i < YDSH_TI_KEYFOB_MODEL_NUMBER_LENGTH; i++)
        {
                modelNumber = [NSString stringWithFormat:@"%@%c", modelNumber,  bAddress[i]];
        }
        //        modelNumber = [NSString stringWithFormat:@"%.2X %.2X %.2X %.2X %.2X %.2X %.2X %.2X %.2X %.2X %.2X %.2X %.2X %.2X %.2X %.2X", bAddress[0], bAddress[1], bAddress[2], bAddress[3], bAddress[4], bAddress[5], bAddress[6], bAddress[7], bAddress[8], bAddress[9], bAddress[10], bAddress[11], bAddress[12], bAddress[13], bAddress[14], bAddress[15]];
        
        [delegateBLEDeviceResult_YDSH BR_modelNumber:modelNumber DeviceID:DeviceID];
}

-(void)processValue_FIRMWARE_REVISION:(Byte *)bAddress length:(int)length
{
        if (length < YDSH_TI_KEYFOB_FIRMWARE_REVISION_LENGTH)
        {
                return;
        }
        
        NSString *firmwareRevision = @"";
        NSString *revisionDate = @"";
        
        firmwareRevision = [NSString stringWithFormat:@"%.2X%.2X", bAddress[0], bAddress[1]];
        revisionDate = [NSString stringWithFormat:@"20%.2X-%.2X-%.2X %.2X:%.2X:00", bAddress[2], bAddress[3], bAddress[4], bAddress[5], bAddress[6]];
        
        [delegateBLEDeviceResult_YDSH BR_firmwareRevision:firmwareRevision visionDate:revisionDate DeviceID:DeviceID];
}

-(void)processValue_DISCONNECT_ENABLE:(Byte *)bAddress length:(int)length
{
        if (length < YDSH_TI_KEYFOB_DISCONNECT_ENABLE_LENGTH)
        {
                return;
        }
        
        bool isEnable;
        
        if (bAddress[0] == 1)
        {
                isEnable = true;
        }
        else
        {
                isEnable = false;
        }
        [delegateBLEDeviceResult_YDSH BR_disconnectEnable:isEnable DeviceID:DeviceID];
}

-(void)processValue_PROTOCOL_VERSION:(Byte *)bAddress length:(int)length
{
        if (length < YDSH_TI_KEYFOB_PROTOCOL_VERSION_LENGTH)
        {
                return;
        }
        
        NSString *protocolVersion = @"";
        
        protocolVersion = [NSString stringWithFormat:@"%X", bAddress[0]];
        [delegateBLEDeviceResult_YDSH BR_protocolVersion:protocolVersion DeviceID:DeviceID];
}

-(void)processValue_BATT_STATE:(Byte *)bAddress length:(int)length
{
        if (length < YDSH_TI_KEYFOB_BATT_STATE_LENGTH)
        {
                return;
        }
        
        int level;
        level = bAddress[0];
        
        [delegateBLEDeviceResult_YDSH BR_batteryLevel:level DeviceID:DeviceID];
}

-(void)processValue_TIME_SYNCHRONOUS:(Byte *)bAddress length:(int)length
{
        if (length < YDSH_TI_KEYFOB_TIME_LENGTH)
        {
                return;
        }
        
        int n;
        NSDate *da,*date2000;
        NSDateFormatter *fm;
        
        fm = [[NSDateFormatter alloc]init];
        [fm setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
        date2000 = [fm dateFromString:@"2000-01-01 00:00:00"];
        
        int b1,b2,b3,b4;
        b1 = (int)bAddress[0];
        b2 = (int)bAddress[1];
        b3 = (int)bAddress[2];
        b4 = (int)bAddress[3];
        n = b1 | (b2 << 8) | (b3 << 16) | (b4 << 24);
        da = [[NSDate alloc]initWithTimeInterval:n sinceDate:date2000];
        
        if (AntiLost_Debug)
        {
                NSLog(@"0x2A2B-----: %d,%d,%d,%d,%d,%@",b1,b2,b3,b4,n,[fm stringFromDate:da]);
        }
        
        [delegateBLEDeviceResult_YDSH BR_currentTime:da DeviceID:DeviceID];
}

-(void)processValue_CLOCK_CONTROL:(Byte *)bAddress length:(int)length
{
        if (length < YDSH_TI_KEYFOB_CLOCK_CONTROL_LENGTH)
        {
                return;
        }
        
        int clockNumber;
        clockNumber = bAddress[0];
        
        [delegateBLEDeviceResult_YDSH BR_clockNumber:clockNumber DeviceID:DeviceID];
}

-(void)processValue_CLOCK_DATA:(Byte *)bAddress length:(int)length
{
        if (length < YDSH_TI_KEYFOB_CLOCK_DATA_LENGTH)
        {
                return;
        }
        
        int index;
        YDSH_strClock strClock;
        
        //byte0 : 闹钟序号(1-15)
        //byte1 : 闹钟重复
        //        闹钟重复的各Bit依次控制(0:关,1:开)：
        //        Bit0:周日，Bit1:周一，Bit2:周二，Bit3:周三
        //        Bit4:周四，Bit5:周五，Bit6:周六，Bit7:重复
        //byte2 : Bit0-5:闹钟时刻(分),Bit6-7:闹钟类型
        //        闹钟类型分为四种：
        //        0：关闭闹钟，1：闹钟类型A,2：闹钟类型B,3：闹钟类型C
        //byte3 : Bit0-4:闹钟时刻(时),Bit5-7:闹钟持续
        //        闹钟持续分为8种：
        //        0: 1分钟,1: 2分钟,2: 3分钟,3: 5分钟,
        //        4:10分钟,5:20分钟,6:30分钟,7:60分钟
        index = bAddress[0];
        strClock.b0 = bAddress[1] & 0x01;
        strClock.b1 = bAddress[1] & 0x02;
        strClock.b2 = bAddress[1] & 0x04;
        strClock.b3 = bAddress[1] & 0x08;
        strClock.b4 = bAddress[1] & 0x10;
        strClock.b5 = bAddress[1] & 0x20;
        strClock.b6 = bAddress[1] & 0x40;
        strClock.bRepeat = bAddress[1] & 0x80;
        strClock.nHour   = bAddress[3] & 0x1F;
        strClock.nMinute = bAddress[2] & 0x3F;
        strClock.cType   = (bAddress[2] >> 6) & 0x03;
        strClock.cDelay  = (bAddress[3] >> 5) & 0x07;
        
        [delegateBLEDeviceResult_YDSH BR_colckData:index clockData:strClock DeviceID:DeviceID];
}

-(void)processValue_PEDOMETER_ACCELENABLE:(Byte *)bAddress length:(int)length
{
        if (length < YDSH_TI_KEYFOB_PEDOMETER_ACCELENABLE_LENGTH)
        {
                return;
        }
        
        bool isEnable;
        
        if (bAddress[0] == 1)
        {
                isEnable = true;
        }
        else
        {
                isEnable = false;
        }
        [delegateBLEDeviceResult_YDSH BR_gsensorEanble:isEnable DeviceID:DeviceID];
}

-(void)processValue_ACCELENABLE_X:(Byte *)bAddress length:(int)length
{
        if (length < YDSH_TI_KEYFOB_ACCELENABLE_DATA_LENGTH)
        {
                return;
        }
        
        [delegateBLEDeviceResult_YDSH BR_gsensorX:bAddress[0] DeviceID:DeviceID];
}

-(void)processValue_ACCELENABLE_Y:(Byte *)bAddress length:(int)length
{
        if (length < YDSH_TI_KEYFOB_ACCELENABLE_DATA_LENGTH)
        {
                return;
        }
        
        [delegateBLEDeviceResult_YDSH BR_gsensorY:bAddress[0] DeviceID:DeviceID];
}

-(void)processValue_ACCELENABLE_Z:(Byte *)bAddress length:(int)length
{
        if (length < YDSH_TI_KEYFOB_ACCELENABLE_DATA_LENGTH)
        {
                return;
        }
        
        [delegateBLEDeviceResult_YDSH BR_gsensorZ:bAddress[0] DeviceID:DeviceID];
}

-(void)processValue_PEDOMETER_COUNTENABLE:(Byte *)bAddress length:(int)length
{
        //总字节数：16
        //运动步数(步)(2字节，低字节在前)
        //有氧步数(步)(2字节，低字节在前)
        //运动时间(秒)(2字节，低字节在前)
        //有氧时间(秒)(2字节，低字节在前)
        //运动距离(0.01Km)(2字节，低字节在前)（写入时补0x00即可，固件不判断）
        //有氧距离(0.01Km)(2字节，低字节在前)（写入时补0x00即可，固件不判断）
        //运动热量(0.1Kcal)(2字节，低字节在前)（写入时补0x00即可，固件不判断）
        //有氧热量(0.1Kcal)(2字节，低字节在前)（写入时补0x00即可，固件不判断）
        
        if (length < YDSH_TI_KEYFOB_PEDOMETER_COUNTE_LENGTH)
        {
                return;
        }
        
        YDSH_strPedometer strPedo;
        
        strPedo.count       = [self Get2BytesToInt:bAddress[0] b1:bAddress[1]];
        strPedo.count_O2    = [self Get2BytesToInt:bAddress[2] b1:bAddress[3]];
        strPedo.second      = [self Get2BytesToInt:bAddress[4] b1:bAddress[5]];
        strPedo.second_O2   = [self Get2BytesToInt:bAddress[6] b1:bAddress[7]];
        strPedo.distance    = [self Get2BytesToInt:bAddress[8] b1:bAddress[9]] / 100.0f;
        strPedo.distance_O2 = [self Get2BytesToInt:bAddress[10] b1:bAddress[11]] / 100.0f;
        strPedo.calories    = [self Get2BytesToInt:bAddress[12] b1:bAddress[13]] / 10.0f;
        strPedo.calories_O2 = [self Get2BytesToInt:bAddress[14] b1:bAddress[15]] / 10.0f;
        
        [delegateBLEDeviceResult_YDSH BR_currentPedoData:strPedo DeviceID:DeviceID];
}

-(void)processValue_SPORT_REMINDER:(Byte *)bAddress length:(int)length
{
        //总字节数：6
        //提醒重复(1字节)
        //      提醒重复的各Bit依次控制(0:关,1:开)：
        //      Bit0:周日，Bit1:周一，Bit2:周二，Bit3:周三
        //      Bit4:周四，Bit5:周五，Bit6:周六，Bit7:重复
        //提醒开始时刻（LSB:分，MSB:时）(2字节，低字节在前)
        //提醒结束时刻（LSB:分，MSB:时）(2字节，低字节在前)
        //      注：提醒结束时刻不能小于提醒开始时刻！
        //提醒间隔（分钟:1-255）(1字节)
        
        if (length < YDSH_TI_KEYFOB_SPORT_REMINDER_LENGTH)
        {
                return;
        }
        
        YDSH_strSportReminder strReminder;
        
        strReminder.b0 = bAddress[0] & 0x01;
        strReminder.b1 = bAddress[0] & 0x02;
        strReminder.b2 = bAddress[0] & 0x04;
        strReminder.b3 = bAddress[0] & 0x08;
        strReminder.b4 = bAddress[0] & 0x10;
        strReminder.b5 = bAddress[0] & 0x20;
        strReminder.b6 = bAddress[0] & 0x40;
        strReminder.bRepeat = bAddress[0] & 0x80;
        strReminder.nMinute_Start = bAddress[1];
        strReminder.nHour_Start = bAddress[2];
        strReminder.nMinute_End = bAddress[3];
        strReminder.nHour_End = bAddress[4];
        strReminder.nInterval = bAddress[5];
        
        [delegateBLEDeviceResult_YDSH BR_sportReminder:strReminder DeviceID:DeviceID];
}

-(void)processValue_OFFLINE_SELECT_DAY:(Byte *)bAddress length:(int)length
{
        if (length < YDSH_TI_KEYFOB_OFFLINE_SELECT_LENGTH)
        {
                return;
        }
        
        [delegateBLEDeviceResult_YDSH BR_offlineData_DaysNumber:bAddress[0] DeviceID:DeviceID];
}

//  天数据接收，每一个数据包为一个天数据，最大15天
-(void)processValue_OFFLINE_DATA_DAY:(Byte *)bAddress length:(int)length
{
        //总字节数：20
        //4byte: 日期(UTC时间)(低字节在前)【表示2000年1月1日0时0分0秒开始的秒数】
        //2byte: 运动步数(步)(低字节在前)
        //2byte: 有氧步数(步)(低字节在前)
        //2byte: 运动时间(秒)(低字节在前)
        //2byte: 有氧时间(秒)(低字节在前)
        //2byte: 运动距离(0.01Km)(低字节在前)
        //2byte: 有氧距离(0.01Km)(低字节在前)
        //2byte: 运动热量(0.1Kcal)(低字节在前)
        //2byte: 有氧热量(0.1Kcal)(低字节在前)
        
        if (length < YDSH_TI_KEYFOB_OFFLINE_DATA_DAY_LENGTH)
        {
                return;
        }
        
        YDSH_strPedometer strPedo;
        NSDate *da;
        
        //        NSLog(@"processValue_OFFLINE_DATA_DAY - %d, %d, %d, %d", bAddress[0], bAddress[1], bAddress[2], bAddress[3]);
        da = [BLEPublic GetBytesToDate:bAddress[0] b1:bAddress[1] b2:bAddress[2] b3:bAddress[3]];
        strPedo.count       = [self Get2BytesToInt:bAddress[4] b1:bAddress[5]];
        strPedo.count_O2    = [self Get2BytesToInt:bAddress[6] b1:bAddress[7]];
        strPedo.second      = [self Get2BytesToInt:bAddress[8] b1:bAddress[9]];
        strPedo.second_O2   = [self Get2BytesToInt:bAddress[10] b1:bAddress[11]];
        strPedo.distance    = [self Get2BytesToInt:bAddress[12] b1:bAddress[13]] / 100.0f;
        strPedo.distance_O2 = [self Get2BytesToInt:bAddress[14] b1:bAddress[15]] / 100.0f;
        strPedo.calories    = [self Get2BytesToInt:bAddress[16] b1:bAddress[17]] / 10.0f;
        strPedo.calories_O2 = [self Get2BytesToInt:bAddress[18] b1:bAddress[19]] / 10.0f;
        
        if((strPedo.second > 0) && (strPedo.count > 0))
        {
                [delegateBLEDeviceResult_YDSH BR_offlineData_Days:da pedoData:strPedo DeviceID:DeviceID];
        }
}

-(void)processValue_OFFLINE_SELECT_MINUTEDAY:(Byte *)bAddress length:(int)length
{
        if (length < YDSH_TI_KEYFOB_OFFLINE_SELECT_LENGTH)
        {
                return;
        }
        
        [delegateBLEDeviceResult_YDSH BR_offlineData_MinutesDayNumber:bAddress[0] DeviceID:DeviceID];
}

//  分数据接收，一天的历史分数据分1-289次上传，最大16天，因此需要用定时器组包，最后按天返回数据
-(void)processValue_OFFLINE_DATA_MINUTEDAY:(Byte *)bAddress length:(int)length
{
        //分数据的数据包结构
        //每日历史分数据的开始标志（0xF0F0F0F0）
        //日期(UTC时间)(低字节在前)【表示2000年1月1日0时0分0秒开始的秒数】
        //第一组历史分数据(低字节在前)【Byte0：时，Byte1：分，Byte2-3：当天步数】
        //第二组历史分数据(低字节在前)【Byte0：时，Byte1：分，Byte2-3：当天步数】
        //...
        //最后一组历史分数据(低字节在前)【Byte0：时，Byte1：分，Byte2-3：当天步数】
        //如果最后一组数据没有填满20字节，则其余填0，如果一组4个字节全0，表示全部传输完毕
        //根据约定，200ms内没有后续数据，则认为传输结束
        
        
        //分数据接收算法：
        //当首次接收到（YDSH_isStartReceiveOfflineMinuteData为False）数据后，开启接收超时定时器，设置继续接收标志（YDSH_isContinueReceiveOfflineMinuteData）为True
        //超时定时器以200ms为最大超时时间，每200ms循环一次，判断是否要继续等待超时（继续接收标志为True）
        //当每一笔数据来时，继续接收标志被改写为True，并暂存当时的数据到缓存
        //如果下一笔数据从“开始标志（0xF0F0F0F0）”开始，则意味着新的一天的分数据到达，此时将之前的缓存数据总结为一天，并返回一次，清除缓存后重新缓存当前数据
        //当超时定时器下一个循环发现继续接收标志为True，则主动更改为False，并等待再下一次循环
        //如超时定时器发现继续接收标志为False，则意味着100ms内没有数据到达，此时将缓存数据总结为一天，并返回一次，最后关闭接收标志，并且清除缓存
        
        if (length < YDSH_TI_KEYFOB_OFFLINE_DATA_DAY_LENGTH)
        {
                return;
        }
        
        Byte b0, b1, b2, b3;
        BOOL isMinuteInitData;
        int n, i;
        
        n = 0;
        b0 = bAddress[n + 0];
        b1 = bAddress[n + 1];
        b2 = bAddress[n + 2];
        b3 = bAddress[n + 3];
        //  解析起始标志
        isMinuteInitData = (b0 == 0xF0) && (b1 == 0xF0) && (b2 == 0xF0) &&(b3 == 0xF0);
        
        if (!YDSH_isStartReceiveOfflineMinuteData)
        {
                if (!isMinuteInitData)
                {
                        //如果是初次传输，但是又不是一天的第一组数据，说明这个数据可能是超时的数据，因此不予处理，直接丢弃
                        return;
                }
                
                YDSH_isStartReceiveOfflineMinuteData = TRUE;
                YDSH_isContinueReceiveOfflineMinuteData = FALSE;
                
                [NSTimer scheduledTimerWithTimeInterval:YDSH_receiveOverTime
                                                 target:self
                                               selector:@selector(checkReceiveOfflineOverTime)
                                               userInfo:nil
                                                repeats:NO];
        }
        else
        {
                YDSH_isContinueReceiveOfflineMinuteData = TRUE;
        }
        
        
        if (isMinuteInitData) //一天的第一组数据
        {
                //先判断缓存是否不为空，是则意味着有之前的数据，需要先总结返回后，清除后再重新缓存
                if (![YDSH_curMinuteData isEqualToString:@""])
                {
                        [delegateBLEDeviceResult_YDSH BR_offlineData_MinutesOfDay:YDSH_curMinuteDayDate
                                                                 dayMinutesRecord:YDSH_curMinuteData
                                                                      recordCount:YDSH_curMinuteCount
                                                                         DeviceID:DeviceID];
                        
                        YDSH_curMinuteDayDate = [NSDate date];
                        YDSH_curMinuteData = @"";
                        YDSH_curMinuteCount = 0;
                }
                
                //先取日期数据
                n = 4;
                b0 = bAddress[n + 0];
                b1 = bAddress[n + 1];
                b2 = bAddress[n + 2];
                b3 = bAddress[n + 3];
                YDSH_curMinuteDayDate = [BLEPublic GetBytesToDate:b0 b1:b1 b2:b2 b3:b3];
                
                
                //再取后三组计步分数据，如果有任何一组为0，则直接放弃（返回超时等待）
                for (i = 0; i < 3; i++)
                {
                        n = i * 4 + 8;
                        b0 = bAddress[n + 0];
                        b1 = bAddress[n + 1];
                        b2 = bAddress[n + 2];
                        b3 = bAddress[n + 3];
                        
                        if (![self checkBytesZero:b0 b1:b1 b2:b2 b3:b3])
                        {
                                //如果任何一组数据出现结束标志(E0 E0 E0 E0)，则该组数据可以直接返回，并立即进入下一天数据的等待中
                                if ((b0 == 0xE0) && (b1 == 0xE0) && (b2 == 0xE0) && (b3 == 0xE0))
                                {
                                        [delegateBLEDeviceResult_YDSH BR_offlineData_MinutesOfDay:YDSH_curMinuteDayDate
                                                                                 dayMinutesRecord:YDSH_curMinuteData
                                                                                      recordCount:YDSH_curMinuteCount
                                                                                         DeviceID:DeviceID];
                                        // ISW HONESTSKY 2015-01-26
                                        //警告：设置YDSH_isStartReceiveOfflineMinuteData为FALSE，将导致下一次数据到达的时候，重新打开一个新的定时器
                                        //     而不是延续老的定时器，因此将可能导致数据接收不完整！因此，YDSH_isStartReceiveOfflineMinuteData只能在
                                        //     定时器中被自己所关闭！
                                        //YDSH_isStartReceiveOfflineMinuteData = FALSE;
                                        YDSH_isContinueReceiveOfflineMinuteData = FALSE;
                                        YDSH_curMinuteDayDate = [NSDate date];
                                        YDSH_curMinuteData = @"";
                                        YDSH_curMinuteCount = 0;
                                }
                                else
                                {
                                        YDSH_curMinuteData = [self parseBytesToOfflineMinuteData:b0
                                                                                              b1:b1
                                                                                              b2:b2
                                                                                              b3:b3
                                                                                            data:YDSH_curMinuteData];
                                        YDSH_curMinuteCount = YDSH_curMinuteCount + 1;
                                }
                        }
                        else
                        {
                                //如果起始标志（F0 F0 F0 F0）和日期齐全，但是接下来的第一个分数据（i = 0时）即为全0，
                                //则意味着硬件传送了该天的空数据（怀疑可能是硬件出错？），此时必须直接返回当日的空数据结果，保证上层接收到的天数据总条数正确，并立即进入下一天数据的等待中
                                if(i == 0)
                                {
                                        YDSH_curMinuteData = @"";
                                        YDSH_curMinuteCount = 0;
                                        [delegateBLEDeviceResult_YDSH BR_offlineData_MinutesOfDay:YDSH_curMinuteDayDate
                                                                                 dayMinutesRecord:YDSH_curMinuteData
                                                                                      recordCount:YDSH_curMinuteCount
                                                                                         DeviceID:DeviceID];
                                        
                                        YDSH_curMinuteDayDate = [NSDate date];
                                }
                                break;
                        }
                }
        }
        else
        {
                //提取5组计步分数据，如果有任何一组为0，则直接放弃（返回超时等待）
                for (i = 0; i < 5; i++)
                {
                        n = i * 4;
                        b0 = bAddress[n + 0];
                        b1 = bAddress[n + 1];
                        b2 = bAddress[n + 2];
                        b3 = bAddress[n + 3];
                        
                        if (![self checkBytesZero:b0 b1:b1 b2:b2 b3:b3])
                        {
                                //如果任何一组数据出现结束标志E0 E0 E0 E0，则该组数据可以直接返回，并立即进入下一天数据的等待中
                                if ((b0 == 0xE0) && (b1 == 0xE0) && (b2 == 0xE0) && (b3 == 0xE0))
                                {
                                        [delegateBLEDeviceResult_YDSH BR_offlineData_MinutesOfDay:YDSH_curMinuteDayDate
                                                                                 dayMinutesRecord:YDSH_curMinuteData
                                                                                      recordCount:YDSH_curMinuteCount
                                                                                         DeviceID:DeviceID];
                                        
                                        // ISW HONESTSKY 2015-01-26
                                        //警告：设置YDSH_isStartReceiveOfflineMinuteData为FALSE，将导致下一次数据到达的时候，重新打开一个新的定时器
                                        //     而不是延续老的定时器，因此将可能导致数据接收不完整！因此，YDSH_isStartReceiveOfflineMinuteData只能在
                                        //     定时器中被自己所关闭！
                                        //YDSH_isStartReceiveOfflineMinuteData = FALSE;
                                        YDSH_isContinueReceiveOfflineMinuteData = FALSE;
                                        YDSH_curMinuteData = @"";
                                        YDSH_curMinuteCount = 0;
                                        YDSH_curMinuteDayDate = [NSDate date];
                                }
                                else
                                {
                                        YDSH_curMinuteData = [self parseBytesToOfflineMinuteData:b0
                                                                                              b1:b1
                                                                                              b2:b2
                                                                                              b3:b3
                                                                                            data:YDSH_curMinuteData];
                                        YDSH_curMinuteCount = YDSH_curMinuteCount + 1;
                                }
                        }
                        else
                        {
                                break;
                        }
                }
        }
}

-(void)processValue_SLEEP_CONTROL:(Byte *)bAddress length:(int)length
{
        if (length < YDSH_TI_KEYFOB_SLEEP_CONTROL_LENGTH)
        {
                return;
        }
        
        [delegateBLEDeviceResult_YDSH BR_sleepMonitorNumber:bAddress[0] DeviceID:DeviceID];
}

//  睡眠数据接收，一条睡眠数据分5个包（20字节/个）上传，最大15条，因此需要用定时器组包，最后按条返回数据
-(void)processValue_SLEEP_DATA:(Byte *)bAddress length:(int)length
{
        //睡眠数据的数据包结构
        //4bytes : 开始时间(低字节在前)【表示2000年1月1日0时0分0秒开始的秒数】
        //4bytes : 停止时间(低字节在前)【表示2000年1月1日0时0分0秒开始的秒数】
        //2bytes : 第1组睡眠数据(低字节在前)
        //2bytes : 第2组睡眠数据(低字节在前)
        //...
        //2bytes : 第46组睡眠数据(低字节在前)
        //注：每一组睡眠数据为2个字节：（Hour和Minute为监控开始后的计时时间）
        //   低字节的Bit0-Bit4:Hour(0-31)，Bit5-Bit7:Mark(第1组为001，其他为000)
        //   高字节的Bit0-Bit5:Minute(0-59)，Bit6-Bit7:Type(1:清醒,2:轻睡,3:熟睡)
        //注：当睡眠监控数据不足100个Byte时，后面补0x00。
        //每条睡眠数据的起始标志是第一组数据的低字节的Bit5是1（即判断每个包内的第9个字节的Bit5是否为1）
        //根据约定，200ms内没有后续数据，则认为传输结束
        
        
        //睡眠数据接收算法：
        //当首次接收到（YDSH_isStartReceiveSleepData为False）数据后，开启接收超时定时器，设置继续接收标志（YDSH_isContinueReceiveSleepData）为True
        //超时定时器以200ms为最大超时时间，每200ms循环一次，判断是否要继续等待超时（继续接收标志为True）
        //当每一笔数据来时，继续接收标志被改写为True，并暂存当时的数据到缓存，并计数
        //如果下一笔数据从“起始标志（包内的第9个字节的Bit5是否为1）”开始，则意味着新的一次到达，此时将之前的缓存数据总结为一次，并返回一次，清除缓存后重新缓存当前数据
        //如果下一笔数据没有发现“起始标志”则丢弃，直到正确的“起始标志”出现，或者直接超时
        //当超时定时器下一个循环发现继续接收标志为True，则主动更改为False，并等待再下一次循环
        //如超时定时器发现继续接收标志为False，则意味着100ms内没有数据到达，此时将缓存数据总结为一次，并返回一次，最后关闭接收标志，并且清除缓存
        
        if (length < YDSH_TI_KEYFOB_SLEEP_DATA_LENGTH)
        {
                return;
        }
        
        BOOL isSleepInitData;
        Byte b0, b1, b2, b3;
        int n, i;
        
        //  解析起始标志
        isSleepInitData = bAddress[8] & 0x20;
        
        if (!YDSH_isStartReceiveSleepData)
        {
                if (!isSleepInitData)
                {
                        //如果是初次传输，但是又不是一条睡眠数据中的第一组数据，说明这个数据可能是超时的数据，因此不予处理，直接丢弃
                        return;
                }
                
                YDSH_isStartReceiveSleepData = TRUE;
                YDSH_isContinueReceiveSleepData = FALSE;
                
                [NSTimer scheduledTimerWithTimeInterval:YDSH_receiveSleepOverTime
                                                 target:self
                                               selector:@selector(checkReceiveSleepOverTime)
                                               userInfo:nil
                                                repeats:NO];
        }
        else
        {
                YDSH_isContinueReceiveSleepData = TRUE;
        }
        
        if (isSleepInitData) //一条睡眠的第一组数据，需要先解析开始时间和停止时间，再解析6组数据；否则直接解析10组数据
        {
                //先判断缓存是否不为空，是则意味着有之前的数据，需要先总结返回后，清除后再重新缓存
                if (![YDSH_curSleepData isEqualToString:@""])
                {
                        [delegateBLEDeviceResult_YDSH BR_sleepMonitor:YDSH_curSleepEnterDate
                                                         endSleepDate:YDSH_curSleepExitDate
                                                   sleepMonitorRecord:YDSH_curSleepData
                                                          recordCount:YDSH_curSleepCount
                                                             DeviceID:DeviceID];
                        
                        YDSH_curSleepEnterDate = [NSDate date];
                        YDSH_curSleepExitDate = [NSDate date];
                        YDSH_curSleepData = @"";
                        YDSH_curSleepCount = 0;
                }
                
                //先取日期数据
                n = 0;
                b0 = bAddress[n + 0];
                b1 = bAddress[n + 1];
                b2 = bAddress[n + 2];
                b3 = bAddress[n + 3];
                YDSH_curSleepEnterDate = [BLEPublic GetBytesToDate:b0 b1:b1 b2:b2 b3:b3];
                
                n = 4;
                b0 = bAddress[n + 0];
                b1 = bAddress[n + 1];
                b2 = bAddress[n + 2];
                b3 = bAddress[n + 3];
                YDSH_curSleepExitDate = [BLEPublic GetBytesToDate:b0 b1:b1 b2:b2 b3:b3];
                
                
                //再取后6组睡眠数据，如果有任何一组为0，则放弃该组数据，解析下一组，直到结束
                for (i = 0; i < 6; i++)
                {
                        n = i * 2 + 8;
                        b0 = bAddress[n + 0];
                        b1 = bAddress[n + 1];
                        
                        if (!((b0 == 0) && (b1 == 0)))
                        {
                                YDSH_curSleepData = [self parseBytesToSleepData:b0
                                                                             b1:b1
                                                                           data:YDSH_curSleepData];
                                YDSH_curSleepCount = YDSH_curSleepCount + 1;
                        }
                }
        }
        else
        {
                //提取10组计步分数据，如果有任何一组为0，则放弃该组数据，解析下一组，直到结束
                for (i = 0; i < 10; i++)
                {
                        n = i * 2;
                        b0 = bAddress[n + 0];
                        b1 = bAddress[n + 1];
                        
                        if (!((b0 == 0) && (b1 == 0)))
                        {
                                YDSH_curSleepData = [self parseBytesToSleepData:b0
                                                                             b1:b1
                                                                           data:YDSH_curSleepData];
                                YDSH_curSleepCount = YDSH_curSleepCount + 1;
                        }
                }
        }
}

-(void)processValue_SLEEP_TIMING:(Byte *)bAddress length:(int)length
{
        if (length < YDSH_TI_KEYFOB_SLEEP_TIMING_LENGTH)
        {
                return;
        }
        
        YDSH_strSleepTiming strSleep1, strSleep2, strSleep3, strSleep4;
        
        strSleep1 = [self getBytesToSleepTiming:bAddress[0] b1:bAddress[1] b2:bAddress[2]];
        strSleep2 = [self getBytesToSleepTiming:bAddress[3] b1:bAddress[4] b2:bAddress[5]];
        strSleep3 = [self getBytesToSleepTiming:bAddress[6] b1:bAddress[7] b2:bAddress[8]];
        strSleep4 = [self getBytesToSleepTiming:bAddress[9] b1:bAddress[10] b2:bAddress[11]];
        
        [delegateBLEDeviceResult_YDSH BR_sleepTiming:strSleep1
                                         timingData2:strSleep2
                                         timingData3:strSleep3
                                         timingData4:strSleep4
                                            DeviceID:DeviceID];
}

-(void)processValue_WAKEUPALERT:(Byte *)bAddress length:(int)length
{
        bool isStart;
        
        if (length < 1)
        {
                return;
        }
        
        if (bAddress[0] == 0x02)
        {
                isStart = YES;
        }
        else
        {
                isStart = NO;
        }
        [delegateBLEDeviceResult_YDSH BR_wakeUpAlerm:isStart DeviceID:DeviceID];
}

-(void)processValue_CUSTOMER_PARAMETER:(Byte *)bAddress length:(int)length
{
        //（1个字节）单位制(0:公制；1:英制)
        //（1个字节）性别(0:女；1：男)
        //（1个字节）年龄(1-254)(单位：岁)
        //（1个字节）身高(1-254)（单位：1cm）
        //（2个字节）体重(1-65534)（单位：0.1Kg）(低字节在前)
        //（2个字节）步长(1-65534)（单位：0.1cm）(低字节在前)
        //（1个字节）目标项(1-4)
        //（2个字节）目标值(1-65534)(低字节在前)
        
        if (length < YDSH_TI_KEYFOB_CUSTOMER_PARAMETER_LENGTH)
        {
                return;
        }
        
        int units, sex, age, nAlarmKind;
        double height, weight, stride, tAlarmValue;
        
        units = bAddress[0];
        sex = bAddress[1];
        age = bAddress[2];
        height = bAddress[3] / 100.0f;
        weight = [self Get2BytesToInt:bAddress[4] b1:bAddress[5]] / 10.0f;
        stride = [self Get2BytesToInt:bAddress[6] b1:bAddress[7]] / 1000.0f;
        nAlarmKind = bAddress[8];
        tAlarmValue = [self Get2BytesToInt:bAddress[9] b1:bAddress[10]];
        
        [delegateBLEDeviceResult_YDSH BR_parameter:units
                                               sex:sex
                                               age:age
                                            height:height
                                            weight:weight
                                            stride:stride
                                        nAlarmKind:nAlarmKind
                                       tAlarmValue:tAlarmValue
                                          DeviceID:DeviceID];
}

-(void)processValue_CUSTOMER_CAMERA:(Byte *)bAddress length:(int)length
{
        if (length < YDSH_TI_KEYFOB_CUSTOMER_CAMERA_LENGTH)
        {
                return;
        }
        
        switch (bAddress[0])
        {
                case 0x01:
                        [delegateBLEDeviceResult_YDSH BR_openCamera:DeviceID];  // 0x01表示打开摄像头
                        break;
                case 0x02:
                        [delegateBLEDeviceResult_YDSH BR_closeCamera:DeviceID];  // 0x02表示关闭摄像头
                        break;
                case 0x03:
                        [delegateBLEDeviceResult_YDSH BR_snapCamera:DeviceID];   // 0x03表示拍照
                        break;
                default:
                        break;
        }
}

-(void)processValue_CUSTOMER_SCREEN_LASTING:(Byte *)bAddress length:(int)length
{
        if (length < YDSH_TI_KEYFOB_CUSTOMER_SCREEN_LASTING_LENGTH)
        {
                return;
        }
        
        int _screenLasting;
        
        _screenLasting = bAddress[0];
        
        [delegateBLEDeviceResult_YDSH BR_screenLasting:_screenLasting DeviceID:DeviceID];
}

-(int)Get2BytesToInt:(Byte)b0 b1:(Byte)b1
{
        int n;
        
        n = b1;
        n = (b1 << 8) | b0;
        
        return n;
}

-(void)getClockData:(Byte)byte
{
        char chars[YDSH_TI_KEYFOB_CLOCK_CONTROL_LENGTH] = {0};
        chars[0] = byte;
        
        NSData *d = [[NSData alloc] initWithBytes:chars length:YDSH_TI_KEYFOB_CLOCK_CONTROL_LENGTH];
        [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID
                                                     serviceUUID:YDSH_TI_KEYFOB_TIME_SERVICE_UUID
                                              characteristicUUID:YDSH_TI_KEYFOB_CLOCK_CONTROL_PROPERTY_UUID
                                                            data:d];
}

-(void)getDayOfflineData:(Byte)dataCtrl
{
        char chars[YDSH_TI_KEYFOB_OFFLINE_SELECT_LENGTH] = {0};
        chars[0] = dataCtrl;
        
        NSData *d = [[NSData alloc] initWithBytes:&chars length:YDSH_TI_KEYFOB_OFFLINE_SELECT_LENGTH];
        [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID
                                                     serviceUUID:YDSH_TI_KEYFOB_OFFLINE_SERVICE_UUID
                                              characteristicUUID:YDSH_TI_KEYFOB_OFFLINE_SELECT_DAY_PROPERTY_UUID
                                                            data:d];
}

-(void)getMinuteDayOfflineData:(Byte)dataCtrl
{
        char chars[YDSH_TI_KEYFOB_OFFLINE_SELECT_LENGTH] = {0};
        chars[0] = dataCtrl;
        
        NSData *d = [[NSData alloc] initWithBytes:&chars length:YDSH_TI_KEYFOB_OFFLINE_SELECT_LENGTH];
        [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID
                                                     serviceUUID:YDSH_TI_KEYFOB_OFFLINE_SERVICE_UUID
                                              characteristicUUID:YDSH_TI_KEYFOB_OFFLINE_SELECT_MINUTEDAY_PROPERTY_UUID
                                                            data:d];
}

//  分数据接收超时定时器
-(void)checkReceiveOfflineOverTime
{
        if (!YDSH_isContinueReceiveOfflineMinuteData)
        {
                if (![YDSH_curMinuteData isEqualToString:@""])
                {
                        //最终超时后结束接收，传送最后一笔数据
                        [delegateBLEDeviceResult_YDSH BR_offlineData_MinutesOfDay:YDSH_curMinuteDayDate
                                                                 dayMinutesRecord:YDSH_curMinuteData
                                                                      recordCount:YDSH_curMinuteCount
                                                                         DeviceID:DeviceID];
                }
                // ISW HONESTSKY 2015-01-26
                //警告：设置YDSH_isStartReceiveOfflineMinuteData为FALSE，将导致下一次数据到达的时候，重新打开一个新的定时器
                //     而不是延续老的定时器，因此将可能导致数据接收不完整！因此，YDSH_isStartReceiveOfflineMinuteData只能在
                //     定时器中被自己所关闭！
                YDSH_isStartReceiveOfflineMinuteData = FALSE;
                YDSH_isContinueReceiveOfflineMinuteData = FALSE;
                YDSH_curMinuteDayDate = [NSDate date];
                YDSH_curMinuteData = @"";
                YDSH_curMinuteCount = 0;
        }
        else
        {
                //未完成接收，重新打开定时器，继续等待接收超时
                YDSH_isContinueReceiveOfflineMinuteData = FALSE;
                [NSTimer scheduledTimerWithTimeInterval:YDSH_receiveOverTime
                                                 target:self
                                               selector:@selector(checkReceiveOfflineOverTime)
                                               userInfo:nil
                                                repeats:NO];
        }
}

//  判断4字节是否全0
-(BOOL)checkBytesZero:(Byte)b0 b1:(Byte)b1 b2:(Byte)b2 b3:(Byte)b3
{
        return (b0 == 0) && (b1 == 0) && (b2 == 0) && (b3 == 0);
}

//  4字节分计步数据
-(NSString*)parseBytesToOfflineMinuteData:(Byte)b0 b1:(Byte)b1 b2:(Byte)b2 b3:(Byte)b3 data:(NSString*)data
{
        NSString *s;
        int n;
        
        n = b2 | (b3 << 8);
        if (![data isEqualToString:@""])
        {
                s = [NSString stringWithFormat:@"%@,%.2d%.2d:%d", data, b0, b1, n];
        }
        else
        {
                s = [NSString stringWithFormat:@"%.2d%.2d:%d",b0, b1, n];
        }
        
        return s;
}


-(void)getSleepData:(Byte)dataCtrl
{
        char chars[YDSH_TI_KEYFOB_SLEEP_CONTROL_LENGTH] = {0};
        chars[0] = dataCtrl;
        
        NSData *d = [[NSData alloc] initWithBytes:&chars length:YDSH_TI_KEYFOB_SLEEP_CONTROL_LENGTH];
        [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID
                                                     serviceUUID:YDSH_TI_KEYFOB_SLEEP_SERVICE_UUID
                                              characteristicUUID:YDSH_TI_KEYFOB_SLEEP_CONTROL_PROPERTY_UUID
                                                            data:d];
}

-(YDSH_strSleepTiming)getBytesToSleepTiming:(Byte)b0 b1:(Byte)b1 b2:(Byte)b2
{
        //总字节数：12
        //定时睡眠监控1(3字节，低字节在前)
        //定时睡眠监控2(3字节，低字节在前)
        //定时睡眠监控3(3字节，低字节在前)
        //定时睡眠监控4(3字节，低字节在前)
        //注：每组定时睡眠监控有3个字节：
        //      第1个字节的各Bit依次控制(0:关,1:开)：
        //              Bit0:周日，Bit1:周一，Bit2:周二，Bit3:周三
        //              Bit4:周四，Bit5:周五，Bit6:周六，Bit7:重复
        //      第2个字节:
        //              Bit0-Bit5:定时时刻(分)(0-59)
        //              Bit6:马达震动提醒(0:关,1:开)
        //              Bit7:屏幕点亮提醒(0:关,1:开)
        //      第3个字节：
        //              Bit0-Bit5:定时时刻(时)(0-24)
        //              Bit6:睡眠监控控制(0:退出,1:进入)
        //              Bit7:本组使能控制(0:禁用,1:使能)
        
        YDSH_strSleepTiming strSleep;
        
        strSleep.b0       = b0 & 0x01;
        strSleep.b1       = b0 & 0x02;
        strSleep.b2       = b0 & 0x04;
        strSleep.b3       = b0 & 0x08;
        strSleep.b4       = b0 & 0x10;
        strSleep.b5       = b0 & 0x20;
        strSleep.b6       = b0 & 0x40;
        strSleep.bRepeat  = b0 & 0x80;
        strSleep.nMinute      = b1 & 0x3F;
        strSleep.isVibration  = b1 & 0x40;
        strSleep.isShine      = b1 & 0x80;
        strSleep.nHour           = b2 & 0x3F;
        strSleep.isEnterMoniter  = b2 & 0x40;
        strSleep.isEnable        = b2 & 0x80;
        
        return strSleep;
}

//  睡眠数据接收超时定时器
-(void)checkReceiveSleepOverTime
{
        if (!YDSH_isContinueReceiveSleepData)
        {
                if (![YDSH_curSleepData isEqualToString:@""])
                {
                        //最终超时后结束接收，传送最后一笔数据
                        [delegateBLEDeviceResult_YDSH BR_sleepMonitor:YDSH_curSleepEnterDate
                                                         endSleepDate:YDSH_curSleepExitDate
                                                   sleepMonitorRecord:YDSH_curSleepData
                                                          recordCount:YDSH_curSleepCount
                                                             DeviceID:DeviceID];
                }
                YDSH_isStartReceiveSleepData = FALSE;
                YDSH_isContinueReceiveSleepData = FALSE;
                
                YDSH_curSleepEnterDate = [NSDate date];
                YDSH_curSleepExitDate = [NSDate date];
                YDSH_curSleepData = @"";
                YDSH_curSleepCount = 0;
        }
        else
        {
                //未完成接收，重新打开定时器，继续等待接收超时
                YDSH_isContinueReceiveSleepData = FALSE;
                [NSTimer scheduledTimerWithTimeInterval:YDSH_receiveSleepOverTime
                                                 target:self
                                               selector:@selector(checkReceiveSleepOverTime)
                                               userInfo:nil
                                                repeats:NO];
        }
}

//  2字节睡眠数据
-(NSString*)parseBytesToSleepData:(Byte)b0 b1:(Byte)b1 data:(NSString*)data
{
        NSString *s;
        int nHour, nMinute, nType;
        
        nHour = b0 & 0x1F;
        nMinute = b1 & 0x3F;
        nType = b1 & 0xC0;
        nType = nType >> 6;
        
        if (![data isEqualToString:@""])
        {
                s = [NSString stringWithFormat:@"%@,%.2d%.2d:%d", data, nHour, nMinute, nType];
        }
        else
        {
                s = [NSString stringWithFormat:@"%.2d%.2d:%d",nHour, nMinute, nType];
        }
        
        return s;
}



//  ------------------------------------------------------------------
//  协议Protocol_BLEDevice_YDSH的实现部分
//  ------------------------------------------------------------------

-(void)BD_getSystemID
{
        [[BLEManager sharedInstance] readValueForCharacteristic:DeviceID
                                                    serviceUUID:YDSH_TI_KEYFOB_VERSION_SERVICE_UUID
                                             characteristicUUID:YDSH_TI_KEYFOB_SYSTEM_ID_PROPERTY_UUID];
}

-(void)BD_enableModelNumber:(BOOL)isEnable
{
        [[BLEManager sharedInstance] notification:DeviceID
                                      serviceUUID:YDSH_TI_KEYFOB_VERSION_SERVICE_UUID
                               characteristicUUID:YDSH_TI_KEYFOB_MODEL_NUMBER_PROPERTY_UUID
                                               on:isEnable];
}

-(void)BD_getModelNumber
{
        [[BLEManager sharedInstance] readValueForCharacteristic:DeviceID
                                                    serviceUUID:YDSH_TI_KEYFOB_VERSION_SERVICE_UUID
                                             characteristicUUID:YDSH_TI_KEYFOB_MODEL_NUMBER_PROPERTY_UUID];
}

-(void)BD_setModelNumber:(NSString *)modelNumber
{
        char chars[YDSH_TI_KEYFOB_MODEL_NUMBER_LENGTH] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
        
        [BLEPublic GetStringToBytes:modelNumber count:YDSH_TI_KEYFOB_MODEL_NUMBER_LENGTH bytes:chars];
        
        NSData *d = [[NSData alloc] initWithBytes:chars length:YDSH_TI_KEYFOB_MODEL_NUMBER_LENGTH];
        [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID
                                                     serviceUUID:YDSH_TI_KEYFOB_VERSION_SERVICE_UUID
                                              characteristicUUID:YDSH_TI_KEYFOB_MODEL_NUMBER_PROPERTY_UUID
                                                            data:d];
}

-(void)BD_getFirmwareRevision
{
        [[BLEManager sharedInstance] readValueForCharacteristic:DeviceID
                                                    serviceUUID:YDSH_TI_KEYFOB_VERSION_SERVICE_UUID
                                             characteristicUUID:YDSH_TI_KEYFOB_FIRMWARE_REVISION_PROPERTY_UUID];
}

-(void)BD_getDisconnectEnable
{
        [[BLEManager sharedInstance] readValueForCharacteristic:DeviceID
                                                    serviceUUID:YDSH_TI_KEYFOB_VERSION_SERVICE_UUID
                                             characteristicUUID:YDSH_TI_KEYFOB_DISCONNECT_ENABLE_PROPERTY_UUID];
}

-(void)BD_setDisconnectEanble:(BOOL)isEnable
{
        char chars[YDSH_TI_KEYFOB_DISCONNECT_ENABLE_LENGTH] = {0};
        
        if (isEnable)
        {
                chars[0] = 1;
        }
        else
        {
                chars[0] = 0;
        }
        
        NSData *d = [[NSData alloc] initWithBytes:chars length:YDSH_TI_KEYFOB_DISCONNECT_ENABLE_LENGTH];
        [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID
                                                     serviceUUID:YDSH_TI_KEYFOB_VERSION_SERVICE_UUID
                                              characteristicUUID:YDSH_TI_KEYFOB_DISCONNECT_ENABLE_PROPERTY_UUID
                                                            data:d];
}

-(void)BD_getProtocolVersion
{
        [[BLEManager sharedInstance] readValueForCharacteristic:DeviceID
                                                    serviceUUID:YDSH_TI_KEYFOB_VERSION_SERVICE_UUID
                                             characteristicUUID:YDSH_TI_KEYFOB_PROTOCOL_VERSION_PROPERTY_UUID];
}

-(void)BD_getBetteryLevel
{
        [[BLEManager sharedInstance] readValueForCharacteristic:DeviceID
                                                    serviceUUID:YDSH_TI_KEYFOB_BATT_SERVICE_UUID
                                             characteristicUUID:YDSH_TI_KEYFOB_BATT_STATE_PROPERTY_UUID];
}

-(void)BD_enableBatteryLevel:(BOOL)isEnable
{
        [[BLEManager sharedInstance] notification:DeviceID
                                      serviceUUID:YDSH_TI_KEYFOB_BATT_SERVICE_UUID
                               characteristicUUID:YDSH_TI_KEYFOB_BATT_STATE_PROPERTY_UUID
                                               on:isEnable];
}

-(void)BD_getCurrentTime
{
        [[BLEManager sharedInstance] readValueForCharacteristic:DeviceID
                                                    serviceUUID:YDSH_TI_KEYFOB_TIME_SERVICE_UUID
                                             characteristicUUID:YDSH_TI_KEYFOB_TIME_SYNCHRONOUS_PROPERTY_UUID];
}

-(void)BD_setCurrentTime
{
        NSDate *date2000;
        uint time;
        NSDateFormatter *fm;
        char bytes[YDSH_TI_KEYFOB_TIME_LENGTH];
        
        fm = [[NSDateFormatter alloc]init];
        [fm setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
        date2000 = [fm dateFromString:@"2000-01-01 00:00:00"];
        time = (int)[[NSDate date] timeIntervalSinceDate:date2000];
        bytes[0] = time;
        bytes[1] = time >> 8;
        bytes[2] = time >> 16;
        bytes[3] = time >> 24;
        
        NSData *d = [[NSData alloc] initWithBytes:bytes length:YDSH_TI_KEYFOB_TIME_LENGTH];
        [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID
                                                     serviceUUID:YDSH_TI_KEYFOB_TIME_SERVICE_UUID
                                              characteristicUUID:YDSH_TI_KEYFOB_TIME_SYNCHRONOUS_PROPERTY_UUID
                                                            data:d];
        
        if (AntiLost_Debug)
        {
                time = (int)[[NSDate date] timeIntervalSinceDate:date2000];
                NSLog(@"---------------%d,%d,%d,%d,%d", time, (Byte)bytes[0], (Byte)bytes[1], (Byte)bytes[2], (Byte)bytes[3]);
        }
}

-(void)BD_enableTimeUpdate:(BOOL)isEnable
{
        [[BLEManager sharedInstance] notification:DeviceID
                                      serviceUUID:YDSH_TI_KEYFOB_TIME_SERVICE_UUID
                               characteristicUUID:YDSH_TI_KEYFOB_TIME_SYNCHRONOUS_PROPERTY_UUID
                                               on:isEnable];
}

-(void)BD_enableClock:(BOOL)isEnable
{
        [[BLEManager sharedInstance] notification:DeviceID
                                      serviceUUID:YDSH_TI_KEYFOB_TIME_SERVICE_UUID
                               characteristicUUID:YDSH_TI_KEYFOB_CLOCK_DATA_PROPERTY_UUID
                                               on:isEnable];
}

-(void)BD_getClockNumber
{
        [[BLEManager sharedInstance] readValueForCharacteristic:DeviceID
                                                    serviceUUID:YDSH_TI_KEYFOB_TIME_SERVICE_UUID
                                             characteristicUUID:YDSH_TI_KEYFOB_CLOCK_CONTROL_PROPERTY_UUID];
}

-(void)BD_getClockDataAll
{
        [self getClockData:0xFF];
}

-(void)BD_getClockDataAllOfIndex:(int)index
{
        if ((index >= 1) || (index <= 15))
        {
                [self getClockData:(Byte)index];
        }
}

-(void)BD_clearClockData
{
        [self getClockData:0xEE];
}

-(void)BD_setClockData:(int)clockIndex clockData:(YDSH_strClock)clockData
{
        if ((clockIndex < 1) || (clockIndex > 15))
        {
                return;
        }
        
        char chars[YDSH_TI_KEYFOB_CLOCK_DATA_LENGTH] = {0, 0, 0, 0};
        
        chars[0] = (Byte)clockIndex;
        if (clockData.b0) { chars[1] = chars[1] | 0x01; }
        if (clockData.b1) { chars[1] = chars[1] | 0x02; }
        if (clockData.b2) { chars[1] = chars[1] | 0x04; }
        if (clockData.b3) { chars[1] = chars[1] | 0x08; }
        if (clockData.b4) { chars[1] = chars[1] | 0x10; }
        if (clockData.b5) { chars[1] = chars[1] | 0x20; }
        if (clockData.b6) { chars[1] = chars[1] | 0x40; }
        if (clockData.bRepeat)  { chars[1] = chars[1] | 0x80; }
        chars[2] = clockData.cType;
        chars[2] = (chars[2] << 6) | clockData.nMinute;
        chars[3] = clockData.cDelay;
        chars[3] = (chars[3] << 5) | clockData.nHour;
        
        NSData *d = [[NSData alloc] initWithBytes:chars length:YDSH_TI_KEYFOB_CLOCK_DATA_LENGTH];
        [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID
                                                     serviceUUID:YDSH_TI_KEYFOB_TIME_SERVICE_UUID
                                              characteristicUUID:YDSH_TI_KEYFOB_CLOCK_DATA_PROPERTY_UUID
                                                            data:d];
}

-(void)BD_getGSensorEanble
{
        [[BLEManager sharedInstance] readValueForCharacteristic:DeviceID
                                                    serviceUUID:YDSH_TI_KEYFOB_PEDOMETER_SERVICE_UUID
                                             characteristicUUID:YDSH_TI_KEYFOB_PEDOMETER_ACCELENABLE_PROPERTY_UUID];
}

-(void)BD_getGSensorX
{
        [[BLEManager sharedInstance] readValueForCharacteristic:DeviceID
                                                    serviceUUID:YDSH_TI_KEYFOB_PEDOMETER_SERVICE_UUID
                                             characteristicUUID:YDSH_TI_KEYFOB_ACCELENABLE_X_PROPERTY_UUID];
}

-(void)BD_enableGSensorX:(bool)isEnable
{
        [[BLEManager sharedInstance] notification:DeviceID
                                      serviceUUID:YDSH_TI_KEYFOB_PEDOMETER_SERVICE_UUID
                               characteristicUUID:YDSH_TI_KEYFOB_ACCELENABLE_X_PROPERTY_UUID
                                               on:isEnable];
}

-(void)BD_getGSensorY
{
        [[BLEManager sharedInstance] readValueForCharacteristic:DeviceID
                                                    serviceUUID:YDSH_TI_KEYFOB_PEDOMETER_SERVICE_UUID
                                             characteristicUUID:YDSH_TI_KEYFOB_ACCELENABLE_Y_PROPERTY_UUID];
}

-(void)BD_enableGSensorY:(bool)isEnable
{
        [[BLEManager sharedInstance] notification:DeviceID
                                      serviceUUID:YDSH_TI_KEYFOB_PEDOMETER_SERVICE_UUID
                               characteristicUUID:YDSH_TI_KEYFOB_ACCELENABLE_Y_PROPERTY_UUID
                                               on:isEnable];
}

-(void)BD_getGSensorZ
{
        [[BLEManager sharedInstance] readValueForCharacteristic:DeviceID
                                                    serviceUUID:YDSH_TI_KEYFOB_PEDOMETER_SERVICE_UUID
                                             characteristicUUID:YDSH_TI_KEYFOB_ACCELENABLE_Z_PROPERTY_UUID];
}

-(void)BD_enableGSensorZ:(bool)isEnable
{
        [[BLEManager sharedInstance] notification:DeviceID
                                      serviceUUID:YDSH_TI_KEYFOB_PEDOMETER_SERVICE_UUID
                               characteristicUUID:YDSH_TI_KEYFOB_ACCELENABLE_Z_PROPERTY_UUID
                                               on:isEnable];
}

-(void)BD_setOnlinePedoCountEanble:(bool)isEnable
{
        char chars[YDSH_TI_KEYFOB_PEDOMETER_ACCELENABLE_LENGTH] = {0};
        
        if (isEnable)
        {
                chars[0] = 1;
        }
        else
        {
                chars[0] = 0;
        }
        
        NSData *d = [[NSData alloc] initWithBytes:chars length:YDSH_TI_KEYFOB_PEDOMETER_ACCELENABLE_LENGTH];
        [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID
                                                     serviceUUID:YDSH_TI_KEYFOB_PEDOMETER_SERVICE_UUID
                                              characteristicUUID:YDSH_TI_KEYFOB_PEDOMETER_ACCELENABLE_PROPERTY_UUID
                                                            data:d];
}

-(void)BD_getCurrentPedoData
{
        [[BLEManager sharedInstance] readValueForCharacteristic:DeviceID
                                                    serviceUUID:YDSH_TI_KEYFOB_PEDOMETER_SERVICE_UUID
                                             characteristicUUID:YDSH_TI_KEYFOB_PEDOMETER_COUNTENABLE_PROPERTY_UUID];
}

-(void)BD_enableOnlinePedometer:(BOOL)isEnable
{
        [[BLEManager sharedInstance] notification:DeviceID
                                      serviceUUID:YDSH_TI_KEYFOB_PEDOMETER_SERVICE_UUID
                               characteristicUUID:YDSH_TI_KEYFOB_PEDOMETER_COUNTENABLE_PROPERTY_UUID
                                               on:isEnable];
}

-(void)BD_clearCurrentPedoData
{
        //清除即写入0
        char chars[YDSH_TI_KEYFOB_PEDOMETER_COUNTE_LENGTH] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        
        NSData *d = [[NSData alloc] initWithBytes:chars length:YDSH_TI_KEYFOB_PEDOMETER_COUNTE_LENGTH];
        [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID
                                                     serviceUUID:YDSH_TI_KEYFOB_PEDOMETER_SERVICE_UUID
                                              characteristicUUID:YDSH_TI_KEYFOB_PEDOMETER_COUNTENABLE_PROPERTY_UUID
                                                            data:d];
}

-(void)BD_getSportReminder
{
        [[BLEManager sharedInstance] readValueForCharacteristic:DeviceID
                                                    serviceUUID:YDSH_TI_KEYFOB_PEDOMETER_SERVICE_UUID
                                             characteristicUUID:YDSH_TI_KEYFOB_SPORT_REMINDER_PROPERTY_UUID];
}

-(void)BD_setSportReminder:(YDSH_strSportReminder)reminderData
{
        char chars[YDSH_TI_KEYFOB_SPORT_REMINDER_LENGTH] = {0, 0, 0, 0, 0, 0};
        
        if (reminderData.b0) { chars[0] = chars[0] | 0x01; }
        if (reminderData.b1) { chars[0] = chars[0] | 0x02; }
        if (reminderData.b2) { chars[0] = chars[0] | 0x04; }
        if (reminderData.b3) { chars[0] = chars[0] | 0x08; }
        if (reminderData.b4) { chars[0] = chars[0] | 0x10; }
        if (reminderData.b5) { chars[0] = chars[0] | 0x20; }
        if (reminderData.b6) { chars[0] = chars[0] | 0x40; }
        if (reminderData.bRepeat)  { chars[0] = chars[0] | 0x80; }
        chars[1] = (Byte)reminderData.nMinute_Start;
        chars[2] = (Byte)reminderData.nHour_Start;
        chars[3] = (Byte)reminderData.nMinute_End;
        chars[4] = (Byte)reminderData.nHour_End;
        chars[5] = (Byte)reminderData.nInterval;
        
        NSData *d = [[NSData alloc] initWithBytes:chars length:YDSH_TI_KEYFOB_SPORT_REMINDER_LENGTH];
        [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID
                                                     serviceUUID:YDSH_TI_KEYFOB_PEDOMETER_SERVICE_UUID
                                              characteristicUUID:YDSH_TI_KEYFOB_SPORT_REMINDER_PROPERTY_UUID
                                                            data:d];
}

-(void)BD_enableOfflineData_Days:(BOOL)isEnable
{
        [[BLEManager sharedInstance] notification:DeviceID
                                      serviceUUID:YDSH_TI_KEYFOB_OFFLINE_SERVICE_UUID
                               characteristicUUID:YDSH_TI_KEYFOB_OFFLINE_DATA_DAY_PROPERTY_UUID
                                               on:isEnable];
}

-(void)BD_getOfflineData_DaysNumber
{
        [[BLEManager sharedInstance] readValueForCharacteristic:DeviceID
                                                    serviceUUID:YDSH_TI_KEYFOB_OFFLINE_SERVICE_UUID
                                             characteristicUUID:YDSH_TI_KEYFOB_OFFLINE_SELECT_DAY_PROPERTY_UUID];
}

-(void)BD_getOfflineData_DaysAll
{
        [self getDayOfflineData:0xFF];
}

-(void)BD_getOfflineData_DaysOfIndex:(int)dayIndex
{
        if ((dayIndex > 0) && (dayIndex <= 15))
        {
                [self getDayOfflineData:dayIndex];
        }
}

-(void)BD_clearOfflineData_Day
{
        [self getDayOfflineData:0xEE];
}

-(void)BD_enableOfflineData_Minutes:(BOOL)isEnable
{
        [[BLEManager sharedInstance] notification:DeviceID
                                      serviceUUID:YDSH_TI_KEYFOB_OFFLINE_SERVICE_UUID
                               characteristicUUID:YDSH_TI_KEYFOB_OFFLINE_DATA_MINUTEDAY_PROPERTY_UUID
                                               on:isEnable];
}

-(void)BD_getOfflineData_MinutesDayNumber
{
        [[BLEManager sharedInstance] readValueForCharacteristic:DeviceID
                                                    serviceUUID:YDSH_TI_KEYFOB_OFFLINE_SERVICE_UUID
                                             characteristicUUID:YDSH_TI_KEYFOB_OFFLINE_SELECT_MINUTEDAY_PROPERTY_UUID];
}

-(void)BD_getOfflineData_MinutesAllDay
{
        [self getMinuteDayOfflineData:0xFF];
}

-(void)BD_getOfflineData_MinutesOfDayIndex:(int)dayIndex
{
        if ((dayIndex >= 0) && (dayIndex <= 15))
        {
                [self getMinuteDayOfflineData:dayIndex];
        }
}

-(void)BD_clearOfflineData_Minutes
{
        [self getMinuteDayOfflineData:0xEE];
}

-(void)BD_enableSleepMonitor:(BOOL)isEnable
{
        [[BLEManager sharedInstance] notification:DeviceID
                                      serviceUUID:YDSH_TI_KEYFOB_SLEEP_SERVICE_UUID
                               characteristicUUID:YDSH_TI_KEYFOB_SLEEP_DATA_PROPERTY_UUID
                                               on:isEnable];
}

-(void)BD_enableWakeUpAlerm:(BOOL)isEnable
{
        [[BLEManager sharedInstance] notification:DeviceID
                                      serviceUUID:YDSH_TI_KEYFOB_VERSION_SERVICE_UUID
                               characteristicUUID:YDSH_TI_KEYFOB_WAKEUPALERT_PROPERTY_UUID
                                               on:isEnable];
}

-(void)BD_getSleepMonitorNumber
{
        [[BLEManager sharedInstance] readValueForCharacteristic:DeviceID
                                                    serviceUUID:YDSH_TI_KEYFOB_SLEEP_SERVICE_UUID
                                             characteristicUUID:YDSH_TI_KEYFOB_SLEEP_CONTROL_PROPERTY_UUID];
}

-(void)BD_getSleepMonitorAll
{
        [self getSleepData:0xFF];
}

-(void)BD_getSleepMonitorOfIndex:(int)index
{
        if ((index >= 0) && (index <= 15))
        {
                [self getSleepData:index];
        }
}

-(void)BD_clearSleepMonitor
{
        [self getSleepData:0xEE];
}

-(void)BD_getSleepTiming
{
        [[BLEManager sharedInstance] readValueForCharacteristic:DeviceID
                                                    serviceUUID:YDSH_TI_KEYFOB_SLEEP_SERVICE_UUID
                                             characteristicUUID:YDSH_TI_KEYFOB_SLEEP_TIMING_PROPERTY_UUID];
}

-(void)BD_setSleepTiming:(YDSH_strSleepTiming)timingData1 timingData2:(YDSH_strSleepTiming)timingData2 timingData3:(YDSH_strSleepTiming)timingData3 timingData4:(YDSH_strSleepTiming)timingData4
{
        char chars[YDSH_TI_KEYFOB_SLEEP_TIMING_LENGTH] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        int i, n;
        YDSH_strSleepTiming strSleep;
        
        for (i = 0; i < 4; i++)
        {
                switch (i)
                {
                        case 0: strSleep = timingData1; break;
                        case 1: strSleep = timingData2; break;
                        case 2: strSleep = timingData3; break;
                        case 3: strSleep = timingData4; break;
                        default: return; break;
                }
                
                n = i * 3;
                if (strSleep.b0) { chars[n] = chars[n] | 0x01; }
                if (strSleep.b1) { chars[n] = chars[n] | 0x02; }
                if (strSleep.b2) { chars[n] = chars[n] | 0x04; }
                if (strSleep.b3) { chars[n] = chars[n] | 0x08; }
                if (strSleep.b4) { chars[n] = chars[n] | 0x10; }
                if (strSleep.b5) { chars[n] = chars[n] | 0x20; }
                if (strSleep.b6) { chars[n] = chars[n] | 0x40; }
                if (strSleep.bRepeat) { chars[n] = chars[n] | 0x80; }
                
                chars[n + 1] = (Byte)strSleep.nMinute;
                if (strSleep.isVibration)    { chars[n + 1] = chars[n + 1] | 0x40; }
                if (strSleep.isShine)        { chars[n + 1] = chars[n + 1] | 0x80; }
                
                chars[n + 2] = (Byte)strSleep.nHour;
                if (strSleep.isEnterMoniter) { chars[n + 2] = chars[n + 2] | 0x40; }
                if (strSleep.isEnable)       { chars[n + 2] = chars[n + 2] | 0x80; }
        }
        
        NSData *d = [[NSData alloc] initWithBytes:chars length:YDSH_TI_KEYFOB_SLEEP_TIMING_LENGTH];
        [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID
                                                     serviceUUID:YDSH_TI_KEYFOB_SLEEP_SERVICE_UUID
                                              characteristicUUID:YDSH_TI_KEYFOB_SLEEP_TIMING_PROPERTY_UUID
                                                            data:d];
}

-(void)BD_getParameter
{
        [[BLEManager sharedInstance] readValueForCharacteristic:DeviceID
                                                    serviceUUID:YDSH_TI_KEYFOB_CUSTOMER_SERVICE_UUID
                                             characteristicUUID:YDSH_TI_KEYFOB_CUSTOMER_PARAMETER_PROPERTY_UUID];
}

-(void)BD_setParameter:(int)units sex:(int)sex age:(int)age height:(double)height weight:(double)weight stride:(double)stride nAlarmKind:(int)nAlarmKind tCalories:(double)tCalories tDistance:(double)tDistance tPaceCount:(double)tPaceCount tCostTime:(double)tCostTime
{
        char bytes[YDSH_TI_KEYFOB_CUSTOMER_PARAMETER_LENGTH];
        double _temp;
        
        //  1 Byte:单位制
        bytes[0] = units;
        
        //  1 Byte:性别
        bytes[1] = sex;
        
        //  1 Byte:年龄
        bytes[2] = age;
        
        //  1 Byte:身高
        bytes[3] = (int)(height * 100);
        
        //  2 Byte:体重
        bytes[4] = (int)(weight * 10);
        bytes[5] = (int)(weight * 10) >> 8;
        
        //  2 Byte:步长
        bytes[6] = (int)(stride * 1000);
        bytes[7] = (int)(stride * 1000) >> 8;
        
        //  1 byte:目标项
        bytes[8] = nAlarmKind;
        
        //  2 byte:目标值
        switch (nAlarmKind)
        {
                case 1: _temp = tCalories; break;
                case 2: _temp = tDistance; break;
                case 3: _temp = tPaceCount;break;
                case 4: _temp = tCostTime; break;
                default: return;
        }
        bytes[9] = (int)_temp;
        bytes[10] = (int)_temp >> 8;
        
        NSData *d = [[NSData alloc] initWithBytes:bytes length:YDSH_TI_KEYFOB_CUSTOMER_PARAMETER_LENGTH];
        [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID
                                                     serviceUUID:YDSH_TI_KEYFOB_CUSTOMER_SERVICE_UUID
                                              characteristicUUID:YDSH_TI_KEYFOB_CUSTOMER_PARAMETER_PROPERTY_UUID
                                                            data:d];
}

-(void)BD_enableCamera:(BOOL)isEnable
{
        [[BLEManager sharedInstance] notification:DeviceID
                                      serviceUUID:YDSH_TI_KEYFOB_CUSTOMER_SERVICE_UUID
                               characteristicUUID:YDSH_TI_KEYFOB_CUSTOMER_CAMERA_PROPERTY_UUID
                                               on:isEnable];
}

-(void)BD_setComingCall:(BOOL)isRead
{
        //        char data;
        char bytes[2];
        NSData *d;
        
        if (!isRead)
        {
                bytes[0] = 0x01;
                bytes[1] = 0x00; //2014-09-03 根据协议v5.3版本，增加了来电号码（iOS下长度补零，号码内容不填）
                d = [[NSData alloc] initWithBytes:bytes length:YDSH_TI_KEYFOB_CUSTOMER_CALLING_LENGTH_2];
        }
        else
        {
                bytes[0] = 0x02;
                bytes[1] = 0x00; //ISW HONESTSKY 2014-12-25 根据协议v5.3版本，增加了来电号码（iOS下长度补零，号码内容不填）
                //                data = 0x02;
                d = [[NSData alloc] initWithBytes:bytes length:YDSH_TI_KEYFOB_CUSTOMER_CALLING_LENGTH_2];
        }
        
        [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID
                                                     serviceUUID:YDSH_TI_KEYFOB_CUSTOMER_SERVICE_UUID
                                              characteristicUUID:YDSH_TI_KEYFOB_CUSTOMER_CALLING_PROPERTY_UUID
                                                            data:d];
}

-(void)BD_getScreenLasting
{
        [[BLEManager sharedInstance] readValueForCharacteristic:DeviceID
                                                    serviceUUID:YDSH_TI_KEYFOB_CUSTOMER_SERVICE_UUID
                                             characteristicUUID:YDSH_TI_KEYFOB_CUSTOMER_SCREEN_LASTING_PROPERTY_UUID];
}

-(void)BD_setScreenLasting:(int)screenLasting
{
        char data;
        NSData *d;
        
        data = (Byte)screenLasting;
        d = [[NSData alloc] initWithBytes:&data length:YDSH_TI_KEYFOB_CUSTOMER_SCREEN_LASTING_LENGTH];
        
        [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID
                                                     serviceUUID:YDSH_TI_KEYFOB_CUSTOMER_SERVICE_UUID
                                              characteristicUUID:YDSH_TI_KEYFOB_CUSTOMER_SCREEN_LASTING_PROPERTY_UUID
                                                            data:d];
}



@end
