//
//  BLEDevice_ImuAirBox.m
//
//  ------------------------------------------------------------------
//  幻响空气盒子-设备-手机互联的BLE主端测试子类的实现部分
//  ------------------------------------------------------------------
//
//  ------------------------------------------------------------------
//  幻响空气盒子-设备-手机互联的BLE主端测试子类的常量定义
//  ------------------------------------------------------------------

#ifndef ImuAirBox_BC4_ItonBLEDefines_h
#define ImuAirBox_BC4_ItonBLEDefines_h

#define BC4_KEYFOB_CUSTOMER_SERVICE_UUID                0xFF00
#define BC4_KEYFOB_CUSTOMER_SEND_PROPERTY_UUID          0xFF01
#define BC4_KEYFOB_CUSTOMER_SEND_LENGTH                 2
#define BC4_KEYFOB_CUSTOMER_RECV_PROPERTY_UUID          0xFF02
#define BC4_KEYFOB_CUSTOMER_RECV_LENGTH                 20

#endif

#import "BLEDevice_ImuAirBox.h"
#import "BLEManager.h"

@implementation BLEDevice_ImuAirBox

@synthesize delegateBLEDeviceResult_ImuAirBox;

-(id)init:(id)delegete
{
    [self setDelegateBLEDeviceResult_ImuAirBox:delegete];
    return [super init:delegete];
}

//  ------------------------------------------------------------------
//  内部私有函数部分
//  ------------------------------------------------------------------

//  硬件数据接收函数 - 重载具体实现部分，每个子类不同
- (void)didUpdateValueForCharacteristic_override:(CBCharacteristic *)characteristic
{
    UInt16 characteristicUUID = [BLEPublic CBUUIDToInt:characteristic.UUID];
    switch(characteristicUUID)
    {
            //  数据接收
        case BC4_KEYFOB_CUSTOMER_RECV_PROPERTY_UUID:
        {
                int pm, temperature, humidity;
                
                Byte bAddress[BC4_KEYFOB_CUSTOMER_RECV_LENGTH];
                [characteristic.value getBytes:&bAddress length:BC4_KEYFOB_CUSTOMER_RECV_LENGTH];
                
                int b2,b3,b4,b5,b6;
                NSString *str;
                
                b2 = (int)bAddress[2];
                b3 = (int)bAddress[3];
                b4 = (int)bAddress[4];
                b5 = (int)bAddress[5];
                b6 = (int)bAddress[6];
                
//                pm = b3;
//                pm = b2 | (pm << 8);
                // 因为硬件和正式版软件解析数据为高八位和低八位直接拼接，因此demo对策
                // 如 低八位0x63，高八位0x09，实际应为0x0963，即2403，现修改为 999(0x09为9，0x63为99)
                // ANNE 2014-11-14
                str = [NSString stringWithFormat:@"%d%02d", b3, b2];
                
                pm = [str intValue];
                
                temperature = b4;
                if (b5 == 1)
                {
                        temperature = -1 * temperature;
                }
                humidity = b6;
                
                [delegateBLEDeviceResult_ImuAirBox BR_receiveTestData:pm temperature:temperature humidity:humidity];
                break;
        }
        default:
        {
            if(AntiLost_Debug)
            {
                NSLog(@"didUpdateValueForCharacteristic --- default --- characteristicUUID:%d" , characteristicUUID);
            }
            break;
        }
    }
}

//  ------------------------------------------------------------------
//  协议Protocol_BLEDevice_MediaTest的实现部分
//  ------------------------------------------------------------------

-(void)BD_enableTransparentData:(BOOL)isEnable
{
    [[BLEManager sharedInstance] notification:DeviceID serviceUUID:BC4_KEYFOB_CUSTOMER_SERVICE_UUID characteristicUUID:BC4_KEYFOB_CUSTOMER_RECV_PROPERTY_UUID on:isEnable];
}

-(void)BD_sendTestData
{
        char chars[BC4_KEYFOB_CUSTOMER_SEND_LENGTH] = {0x01,0xAA};
        
    NSData *d = [[NSData alloc] initWithBytes:chars length:BC4_KEYFOB_CUSTOMER_SEND_LENGTH];
    [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID serviceUUID:BC4_KEYFOB_CUSTOMER_SERVICE_UUID characteristicUUID:BC4_KEYFOB_CUSTOMER_SEND_PROPERTY_UUID data:d];
}

@end
