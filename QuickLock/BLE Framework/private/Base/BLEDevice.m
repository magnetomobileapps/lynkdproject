//
//  BLEDevice.m
//  
//  ------------------------------------------------------------------
//  对外接口类的实现部分
//  ------------------------------------------------------------------

#import "BLEDevice.h"
#import "BLEManager.h"

// ------------------------------------------------ ------------------
// BLEDevice class hierarchy Description
//
// Base base class BLEDevice
// AntiLost anti lost BLEDevice_AntiLost
// Pedo Pedometer TI without screen BLEDevice_Pedo
// Pedo_Belter pedometer Beitaidaibing BLEDevice_Pedo_Belter
// Pedo_ZYWK Broadcom pedometer with screen BLEDevice_Pedo_ZYWK
// Pedo_YDSH pedometer TI with screen BLEDevice_Pedo_YDSH
// BP136A COMBO sphygmomanometer BLEDevice_BP136A (after changing model BP128A, but the class name remains unchanged)
// AFan Airmate fan BLEDevice_AFan
// UART BLE passthrough BLEDevice_UART
// YDSH sports bracelet BLEDevice_YDSH
// ------------------------------------------------ ------------------

// ------------------------------------------------ ------------------
// BLE Framework Code Imprint
//
// Note: Under normal circumstances, there is no search BLE device to search fails, there is no return! Temporary shortage search failed logic process! !
//
// 0.71 Fixed core classes have been connected to the device, you can continue to try to connect the device to hold the original problem of lost connections to other equipment, resulting in
// 0.70 Fixed lock subclass, processValue_HISTORY_UPDATE NFC function format of ID reading by NSString become two hexadecimal
// 0.69 Fixed lock subclass, the username upload function BR_userName, history DeviceID upload function BR_historyLog missing return value problem
// 0.68 in lock subclass, the new user name input and modify history display function
// 0.67 in lock subclasses, modify the function returns BR_PasswordResult
// 0.66 added anti-lost member Kazi class AntiLost_ClubCard 0.65
// remove the anti-lost subclass of MAC addresses to identify and judge
// 0.64 Added sport bracelet subclass up alarm interface (enabled, and the alarm to get up start and end)
// 0.63 Fixed movement points receiving process of receiving the data (correction might occur timeouts lead to problems receiving data incomplete)
// 0.62T1 increased movement bracelet subclass historical data of the original data points return (for testing)
// 0.62 Upon request, the revised subclass passcode lock and unlock interface
// 0.61 adds lock subclass
// 0.60 increase on the core classes BD_DeviceID method, you can return to the current ID of each BLEDevice
// Callback function all core classes increased DeviceID return value (to distinguish which BLEDevice returned)
// Sports bracelet, anti-lost, Emmett fan, Camber blood pressure monitors, pedometers (TI without a screen, with a screen times Thailand, with a screen Broadcom, TI with screen) all the callbacks, BLE passthrough subclass increase the return value of DeviceID
// Removed without thermometer subclass subclass door control system, the United States from the end of the passthrough test subclass 0.55
// read system adds Bluetooth interface is turned BD_getBLEState 0.54
// add stop searching interface BD_stopAllScan, modify calls to remind the caller ID length setting
// 0.53 modified BD_getScanDeviceList of results returned, returns the contents of string type MutableArray
// The content of each string is "LocalName, ServiceUUID, UUID, RSSI, TxPower, ManufacturerData", in the middle separated by commas;
// Modified ManufacturerData, using a hexadecimal string representation
// Add the BD_enableModelNumber in sports bracelet subclass
// 0.52 RSSI read to increase support for iOS8, the amended motion bracelet subclass of error, increase the air box subclass magic ring, core classes increased equipment without ServiceUUID search
// 0.51 modify the input and output string BD_setModelNumber BR_modelNumber the (no longer is a hexadecimal string of 16 characters but simply of)
// 0.50 increase in the duration of the motion parameters subclass bracelet set and read the screen lit (according to the protocol v5.3 version)
// 0.49 Camber modified sphygmomanometer subclasses, under the verification and calibration mode eCMD_SET_CHECK, eCMD_SET_ADJUST, eCMD_SET_ADJUST_CONFIRM three commands no response decision (to remove a response recognition)
// 0.48 completed the implementation part sports bracelet subclass
// 0.48T1 increased movement bracelet subclass (BLEDevice_YDSH), all functions using motion bracelet 5.0 protocol document (interface definition is complete, to achieve some unfinished)
// 0.47 TI adds pedometer with screen subclass (BLEDevice_Pedo_YDSH), uses motion bracelet 5.0 protocol documents, and some functions pedometer
// 0.46 Camber increases blood pressure in check mode and the correction mode commands and responses BD_sendCheckCommand, BD_sendAdjustCommand, BD_sendConfirmCommand
// 0.45 modify BLE passthrough class and its subclasses, increase the parameter to receive data length
// 0.44 increased test base class for receiving data BLE passthrough function that returns a subclass BR_receiveBaseData
// 0.43 Fixed a loophole Bluetooth connection on the core class (result not connected)
// 0.42 Camber modified sphygmomanometer subclass determined in accordance with the agreement in response to the position command (Get class does not respond)
// 0.41 Camber modified sphygmomanometer subclass, increase command responses
// 0.40T1 modified Camber sphygmomanometer subclass, increase command responses (unfinished)
// 0.39 increase in the key anti-lost class camera BD_enableCamera, BR_snap, BR_cameraOperation
// 0.38 increase in core classes to specify a connection when the search function devices Index BD_initConnectWithScanIndex (revised version mistakenly rolled back, so again amended)
// 0.37 times revised Thai pedometer wrong setting parameter length (revised version mistakenly rolled back, so again amended)
// Increased 0.36-fold Thai unified set parameters function pedometer (an increase of unit settings) BD_setParameter
// 0.35T increase in core classes to read hardware device's Bluetooth MAC address method BD_getMACAddress and return entrust BR_getMACAddress
// 0.34T core classes to increase returns search device list function BD_getScanDeviceList (with BD_initConnectWithScanIndex Use)
// 0.34 increase from the end of the United States pass through the test subclass
// 0.33 adds BLE passthrough subclass, later by BLE passthrough mode class inherits since subclasses, do not write data received and sent, just call the appropriate virtual function can
// 0.32 adds Emmett fan subclass
// 0.31 Camber modified sphygmomanometer subclass, stop sphygmomanometer data upload function (protocol change)
// 0.30T2 modify the operating mode gating system subclasses
// 0.30T1 increase the US air-conditioning - equipment - Mobile interconnected BLE-side test subclass
// 0.30 adds Camber sphygmomanometer subclass, the error code type ERROR_CODE, and the processing content parseData_ERROR
// 0.29 Fixed processing content parseData_ERROR Camber sphygmomanometer subclass, error code
// 0.28 Fixed Camber sphygmomanometer subclass, erroneous measurement data upload process (revised version mistakenly rolled back, so again amended)
// 0.27 increase in the door control system sub-category (including communication data packet gating system analysis)
// 0.26 in Camber sphygmomanometer subclass, an increase of measurement error data reception and upload BR_updateMesureError
// 0.25 in Pedo, increasing the device time to upload callback BR_currentTime
// 0.24 adds Camber sphygmomanometer subclasses (including Camber sphygmomanometer communication packet parsing)
// 0.23 core classes increased BD_checkExistServiceUUID interfaces to determine whether there is a service and characteristic
// 0.22 adds thermometer subclass
// 0.21 increase in hardware power function to read and displayed on the main screen
// 0.20 Fixed BLEDevice_Pedo_ZYWK.m no end in case BC1_KEYFOB_BATT_VERSION_PROPERTY_UUID plus break error
// 0.19 Fixed eCallType_didUpdateRSSI and eCalltype_setDeviceUUID plus break ending no errors (resulting deviceID error)
// 0.18 times increase in the new Pedometer subclass Thailand extended functionality (Pedo_Belter), increase the number of identification of similar equipment localName (partially matching)
// 0.17 increase under a variety of connection and disconnection of the case failed to clear the UUID
// Read the hardware version 0.16 adds support number, delete unwanted pedometer with screen UUID (sleep)
// 0.15 increase BLE held devices to exchange some hardware features, you can use the parent class to hold a BLE hardware, then give it to different subclasses
// 0.14 increase discoverService number of times (three times), ensure that the Bluetooth connection is successful can complete search service
// 0.13 increase in writeValueForCharacteristic when CBCharacteristicWriteType parameters of judgment (to meet the requirements of iOS7)
// 0.12 Amendments to search and connection is first connected device, first connect the device does not have UUID, and therefore need special case, or can not recognize!
// 0.11 increase in the surplus with the new Micro-expansion function pedometer (Pedo_ZYWK) 0.10
// original anti-lost and pedometer underlying connection frame
// ------------------------------------------------ ------------------

NSString *FMWKVERSION = @"0.71";

#ifndef BC_ItonBLEDefines_h
#define BC_ItonBLEDefines_h

#define BC_DEVICEINFOMATION_SERVICE_UUID       0x180A
#define BC_DEVICEINFOMATION_SYSTEMID_UUID      0x2A23

#endif

const int FAILETYPE_FAILTOCONNECT = 0;
const int FAILETYPE_FAILTODISCOVER = 1;
const int FAILETYPE_FAILTOERROR = 2;

@implementation BLEDevice

@synthesize delegateBLEDeviceResult;

-(id)init:(id)delegete
{
        DeviceID = 0;
        DeviceUUID = @"";
        [self setDelegateBLEDeviceResult:delegete];
        [[BLEManager sharedInstance] addBLEDevice:self];
        return self;
}

-(bool)changeBLEDevice:(BLEDevice*)sourceBLEDevice
{
        return [[BLEManager sharedInstance] changeBLEDevice:DeviceID sourceDeviceID:[sourceBLEDevice getDeviceID]];
}

//  ------------------------------------------------------------------
//  To achieve some agreement Protocol BLE Device Call for
//  ------------------------------------------------------------------

-(id)operate:(int)callType params:(NSArray*)params
{
        id result = nil;
        int n;
        NSString *s;
        bool b;
        CBCharacteristic *characteristic;
        float f;
    
        switch (callType) {
                case eCallType_getDeviceID:
                        n = [self getDeviceID];
                        result = [[NSNumber alloc] initWithInt:n];
                        break;
                case eCalltype_setDeviceID:
                        n = [(NSNumber *)[params objectAtIndex:0] intValue];
                        [self setDeviceID:n];
                        break;
                case eCallType_getDeviceUUID:
                        s = [self getDeviceUUID];
                        result = (id)s;
                        break;
                case eCalltype_getPeripheralSuccess:
                        s = (NSString *)[params objectAtIndex:0];
                        [self getPeripheralSuccess:s];
                        break;
                case eCallType_connectResult:
                        b = [(NSNumber *)[params objectAtIndex:0] boolValue];
                        s = (NSString *)[params objectAtIndex:1];
                        [self connectResult:b UUID:s];
                        break;
                case eCalltype_connectOK:
                        [self connectOK];
                        break;
                case eCallType_UpdateValueForCharac:
                        characteristic = (CBCharacteristic *)[params objectAtIndex:0];
                        //  First insert BLEDevice base class itself handler
                        [self didUpdateValueForCharacteristic_base:characteristic];
                        //  Then perform overloaded functions subclass
                        [self didUpdateValueForCharacteristic_override:characteristic];
                        break;
                case eCallType_didDisConnect:
                        [self didDisConnect];
                        break;
                case eCallType_didUpdateRSSI:
                        f = [(NSNumber *)[params objectAtIndex:0] floatValue];
                        [self didUpdateRSSI:f];
                        break;
                case eCalltype_setDeviceUUID:
                        s = (NSString *)[params objectAtIndex:0];
                        [self setDeviceUUID:s];
                        break;
                case eCallType_getScanList:
                        [self scanDeviceList:(NSMutableArray *)[params objectAtIndex:0]];
                        break;
                default:
                        break;
        }
        return result;
}

-(int)getDeviceID
{
        return DeviceID;
}

-(void)setDeviceID:(int)value
{
        DeviceID = value;
}

-(NSString*)getDeviceUUID
{
        return DeviceUUID;
}

-(void)setDeviceUUID:(NSString*)value
{
        DeviceUUID = value;
}

-(void)getPeripheralSuccess:(NSString*)UUID
{
        BOOL isFirstPeripheral;
        if (![UUID isEqualToString:@""])
        {
                [self setDeviceUUID:UUID];
                isFirstPeripheral = false;
        }
        else
        {
                // 2013-08-30 如果是初次连接的设备，给出特殊标识用于查找
                [self setDeviceUUID:@"firstPeripheral"];
                isFirstPeripheral = true;
        }
        if (![[BLEManager sharedInstance] startConnect:DeviceID isFirstPeripheral:isFirstPeripheral])
        {
                [self setDeviceUUID:@""];
                [delegateBLEDeviceResult BR_connectFail:FAILETYPE_FAILTOERROR DeviceID:DeviceID];
        }
}

-(void)connectResult:(BOOL)bResult UUID:(NSString *)UUID
{
        if (bResult)
        {
                [self setDeviceUUID:UUID];
                if (![[BLEManager sharedInstance] startDiscover:DeviceID])
                {
                        [self setDeviceUUID:@""];
                        [delegateBLEDeviceResult BR_connectFail:FAILETYPE_FAILTOERROR DeviceID:DeviceID];
                }
        }
        else
        {
                [self setDeviceUUID:@""];
                [delegateBLEDeviceResult BR_connectFail:FAILETYPE_FAILTOCONNECT DeviceID:DeviceID];
        }
}

-(void)connectOK
{
        [delegateBLEDeviceResult BR_connectOK:DeviceID];
}

-(void)didDisConnect
{
        [self setDeviceUUID:@""];
        [delegateBLEDeviceResult BR_didDisconnect:DeviceID];
}

-(void)didUpdateRSSI:(float)f
{
        [delegateBLEDeviceResult BR_didUpdateRSSI:f DeviceID:DeviceID];
}

-(void)scanDeviceList:(NSMutableArray *)arrayScanList
{
        [delegateBLEDeviceResult BR_scanDeviceList:arrayScanList DeviceID:DeviceID];
    NSLog(@"arrayScanList : %@",arrayScanList);
}

-(void)didUpdateValueForCharacteristic_base:(CBCharacteristic *)characteristic
{
        UInt16 characteristicUUID = [BLEPublic CBUUIDToInt:characteristic.UUID];
        switch(characteristicUUID)
        {
                //  Data received
                case BC_DEVICEINFOMATION_SYSTEMID_UUID:
                {
                        Byte bAddress[8];
                        [characteristic.value getBytes:&bAddress length:8];
                        
                        [delegateBLEDeviceResult BR_getMACAddress:bAddress length:8 DeviceID:DeviceID];
                        break;
                }
                default:
                {
                        if(AntiLost_Debug)
                        {
                                NSLog(@"didUpdateValueForCharacteristic_base --- default --- characteristicUUID:%d" , characteristicUUID);
                        }
                        break;
                }
        }
}

// Hardware data reception function - overloaded function, the actual implementation of the various subclasses
- (void)didUpdateValueForCharacteristic_override:(CBCharacteristic *)characteristic
{
        //  此处不应放置代码！
}

//  ------------------------------------------------------------------
//  协议Protocol_BLEDevice的实现部分
//  ------------------------------------------------------------------

-(BOOL)BD_initConnect:(NSString*)UUID serviceUUID:(NSString*)serviceUUID localName:(NSString*)localName
{
        if (![self BD_getIsConnected])
        {
                if ([UUID isEqualToString:@""])
                {
                        return [[BLEManager sharedInstance] connectDevice:DeviceID
                                                              serviceUUID:serviceUUID
                                                                localName:localName];
                }
                else
                {
                        return [[BLEManager sharedInstance] connectDevice:DeviceID peripheralUUID:UUID];
                }
        }
        else
        {
                return false;
        }
}

-(BOOL)BD_initConnectWithNameOfRanges:(NSString*)serviceUUID localNames:(NSArray*)localNames isRanges:(NSArray*)isRanges
{
        if (![self BD_getIsConnected])
        {
                return [[BLEManager sharedInstance] initConnectWithNameOfRanges:DeviceID
                                                                    serviceUUID:serviceUUID
                                                                     localNames:localNames
                                                                       isRanges:isRanges];
        }
        else
        {
                return false;
        }
}

-(BOOL)BD_initConnectWithScanIndex:(int)scanIndex
{
        if (![self BD_getIsConnected])
        {
                return [[BLEManager sharedInstance] initConnectWithScanIndex:DeviceID scanIndex:scanIndex];        }
        else
        {
                return false;
        }
}

-(BOOL)BD_disConnect
{
        return [[BLEManager sharedInstance] disconnect:DeviceID];
}

-(BOOL)BD_getIsConnected
{
        return [[BLEManager sharedInstance] getIsConnect:DeviceID];
}

-(BOOL)BD_readRSSI
{
        return [[BLEManager sharedInstance] readRSSI:DeviceID];
}

-(NSString *)BD_getDeviceUUID
{
        return DeviceUUID;
}

-(NSString *)BD_getFrameWorkVersion
{
        return FMWKVERSION;
}

-(BOOL)BD_checkExistServiceUUID:(NSString*)serviceUUID characteristicUUID:(NSString*)characteristicUUID
{
        return [[BLEManager sharedInstance] checkExistServiceUUID:DeviceID serviceUUID:serviceUUID characteristicUUID:characteristicUUID];
}

-(BOOL)BD_getScanDeviceList:(float)scanTime
{
        return [[BLEManager sharedInstance] getScanDeviceList:DeviceID scanTime:scanTime];
}

-(BOOL)BD_getMACAddress
{
        return [[BLEManager sharedInstance] getMACAddress:DeviceID];
}

-(int)BD_DeviceID
{
        return DeviceID;
}

// ISW HONESTSKY 2014-12-19 TEST 应对连接闪退
-(void)BD_stopAllScan
{
        return [[BLEManager sharedInstance] stopAllScan];
}

// ISW HONESTSKY
-(BOOL)BD_getBLEState
{
        return [[BLEManager sharedInstance] getBLEState:DeviceID];
}

@end
