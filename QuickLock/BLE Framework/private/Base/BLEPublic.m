//
//  BLEPublic.m
//  
//  ------------------------------------------------------------------
//  公共定义类的实现部分
//  ------------------------------------------------------------------

#import "BLEPublic.h"

@implementation BLEPublic

+(const char *) CBUUIDToString:(CBUUID *) UUID
{
    return [[UUID.data description] cStringUsingEncoding:NSStringEncodingConversionAllowLossy];
}

+(int) compareCBUUID:(CBUUID *) UUID1 UUID2:(CBUUID *)UUID2
{
    char b1[16];
    char b2[16];
    [UUID1.data getBytes:b1];
    [UUID2.data getBytes:b2];
    if (memcmp(b1, b2, UUID1.data.length) == 0)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

+(const char *)UUIDToString:(CFUUIDRef)UUID
{
    if(!UUID)
    {
        return "";
    }
    CFStringRef s = CFUUIDCreateString(NULL, UUID);
    return CFStringGetCStringPtr(s, 0);
}

+(CFUUIDRef)StringToUUID:(const char*)suuid
{
    if (!suuid)
    {
        return nil;
    }
    CFStringRef s = CFStringCreateWithCString(NULL, suuid, 0);
    return CFUUIDCreateFromString(NULL, s);
}

+(BOOL)comparePeripheralUUID:(CBPeripheral *)cbp1 peripheral2:(CBPeripheral *)cbp2
{
    NSString *s1, *s2;
    s1 = [NSString stringWithUTF8String:[self UUIDToString:(__bridge CFUUIDRef)(cbp1.identifier)]];
    s2 = [NSString stringWithUTF8String:[self UUIDToString:(__bridge CFUUIDRef)(cbp2.identifier)]];
    return [s1 isEqualToString:s2];
}

+(UInt16) swap:(UInt16)s
{
    UInt16 temp = s << 8;
    temp |= (s >> 8);
    return temp;
}

+(CBService *) findServiceFromUUID:(CBUUID *)UUID p:(CBPeripheral *)p
{
    for(int i = 0; i < p.services.count; i++)
    {
        CBService *s = [p.services objectAtIndex:i];
        if ([self compareCBUUID:s.UUID UUID2:UUID])
        {
            return s;
        }
    }
    return nil; //Service not found on this peripheral
}

+(CBCharacteristic *) findCharacteristicFromUUID:(CBUUID *)UUID service:(CBService*)service
{
    for(int i=0; i < service.characteristics.count; i++)
    {
        CBCharacteristic *c = [service.characteristics objectAtIndex:i];
        if ([self compareCBUUID:c.UUID UUID2:UUID])
        {
            return c;
        }
    }
    return nil; //Characteristic not found on this service
}

+(CBDescriptor *) findDescriptorFromUUID:(CBUUID *)UUID characteristic:(CBCharacteristic *)characteristic
{
    for (int i = 0; i < characteristic.descriptors.count; i++)
    {
        CBDescriptor *d = [characteristic.descriptors objectAtIndex:i];
        if ([self compareCBUUID:d.UUID UUID2:UUID])
        {
            return d;
        }
    }
    return nil; //Descriptor not found on this Characteristic
}

+(UInt16) CBUUIDToInt:(CBUUID *)UUID
{
    char b1[16];
    [UUID.data getBytes:b1];
    return ((b1[0] << 8) | b1[1]);
}

+(void)GetHexStringToBytes:(NSString *)hexString hexCount:(int)hexCount bytes:(char*)bytes;
{
        NSString *_s;
        char _c;
        int j, n;
        NSScanner* scan;
        unsigned int _un;
        
        _s = @"";
        n = 0;
        
        for (j = 0; j < hexString.length; j++)
        {
                if (n < hexCount)
                {
                        _c = [hexString characterAtIndex:j];
                        
                        if (_c != ' ')
                        {
                                _s = [NSString stringWithFormat:@"%@%c", _s, _c];
                                
                                if (j == hexString.length - 1)
                                {
                                        scan = [NSScanner scannerWithString:_s];
                                        _un = 0;
                                        [scan scanHexInt:&_un];
                                        
                                        bytes[n] = (char)_un;
                                        
                                        _s = @"";
                                        n = n + 1;
                                }
                        }
                        else
                        {
                                if (![_s isEqualToString:@""])
                                {
                                        scan = [NSScanner scannerWithString:_s];
                                        _un = 0;
                                        [scan scanHexInt:&_un];
                                        
                                        bytes[n] = (char)_un;
                                        
                                        _s = @"";
                                        n = n + 1;
                                }
                        }
                }
        }
}

+(void)GetStringToBytes:(NSString *)bString count:(int)count bytes:(char*)bytes
{
        int i, nLength;
        
        nLength = (int)[bString length];
        for (i = 0; i < count; i++)
        {
                if (i <= (nLength - 1))
                {
                        bytes[i] = [bString characterAtIndex:i];
                }
                else
                {
                        bytes[i] = (char)0;
                }
        }
}

//  4 Byte processing time
+(NSDate*)GetBytesToDate:(Byte)b0 b1:(Byte)b1 b2:(Byte)b2 b3:(Byte)b3
{
        int n;
        NSDate *da,*date2000;
        NSDateFormatter *fm;
        
        fm = [[NSDateFormatter alloc]init];
        [fm setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
        date2000 = [fm dateFromString:@"2000-01-01 00:00:00"];
        
        n = b0 | (b1 << 8) | (b2 << 16) | (b3 << 24);
        da = [[NSDate alloc]initWithTimeInterval:n sinceDate:date2000];
        
        return da;
}

+(NSString *)GetBytesToTime:(Byte)b0 b1:(Byte)b1
{
    int minutes;
    NSDateFormatter *fm;
    
    fm = [[NSDateFormatter alloc]init];
    [fm setDateFormat:@"HH:mm"];
    
    minutes = b0 | (b1 << 8);
    
    int hour = minutes / 60;
    int min = minutes % 60;
    
    NSString *timeString = [NSString stringWithFormat:@"%02d:%02d", hour, min];
    
    return timeString;
}

//+(UInt8) Cal_CRC8:(UInt8 *)ptr len:(UInt32)len
//{
//    UInt8 crc;
//    UInt8 i;
//    
//    crc = 0;
//    while(len--)
//    {
//        crc ^= *ptr++;
//        for(i = 0;i < 8;i++)
//        {
//            if(crc & 0x01)
//            {
//                crc = (crc >> 1) ^ 0x8C;
//            }
//            else
//            {
//                crc >>= 1;
//            }
//        }
//    }
//    return crc;
//}

////  C的CRC8校验的源代码
////  CRC8校验
////  X^8 + X^2 + X^1 + 1
//unsigned int CRC8_TAB[256] =
//{
//    0x00,0x07,0x0E,0x09,0x1C,0x1B,0x12,0x15,0x38,0x3F,0x36,0x31,0x24,0x23,0x2A,0x2D,
//    0x70,0x77,0x7E,0x79,0x6C,0x6B,0x62,0x65,0x48,0x4F,0x46,0x41,0x54,0x53,0x5A,0x5D,
//    0xE0,0xE7,0xEE,0xE9,0xFC,0xFB,0xF2,0xF5,0xD8,0xDF,0xD6,0xD1,0xC4,0xC3,0xCA,0xCD,
//    0x90,0x97,0x9E,0x99,0x8C,0x8B,0x82,0x85,0xA8,0xAF,0xA6,0xA1,0xB4,0xB3,0xBA,0xBD,
//    0xC7,0xC0,0xC9,0xCE,0xDB,0xDC,0xD5,0xD2,0xFF,0xF8,0xF1,0xF6,0xE3,0xE4,0xED,0xEA,
//    0xB7,0xB0,0xB9,0xBE,0xAB,0xAC,0xA5,0xA2,0x8F,0x88,0x81,0x86,0x93,0x94,0x9D,0x9A,
//    0x27,0x20,0x29,0x2E,0x3B,0x3C,0x35,0x32,0x1F,0x18,0x11,0x16,0x03,0x04,0x0D,0x0A,
//    0x57,0x50,0x59,0x5E,0x4B,0x4C,0x45,0x42,0x6F,0x68,0x61,0x66,0x73,0x74,0x7D,0x7A,
//    0x89,0x8E,0x87,0x80,0x95,0x92,0x9B,0x9C,0xB1,0xB6,0xBF,0xB8,0xAD,0xAA,0xA3,0xA4,
//    0xF9,0xFE,0xF7,0xF0,0xE5,0xE2,0xEB,0xEC,0xC1,0xC6,0xCF,0xC8,0xDD,0xDA,0xD3,0xD4,
//    0x69,0x6E,0x67,0x60,0x75,0x72,0x7B,0x7C,0x51,0x56,0x5F,0x58,0x4D,0x4A,0x43,0x44,
//    0x19,0x1E,0x17,0x10,0x05,0x02,0x0B,0x0C,0x21,0x26,0x2F,0x28,0x3D,0x3A,0x33,0x34,
//    0x4E,0x49,0x40,0x47,0x52,0x55,0x5C,0x5B,0x76,0x71,0x78,0x7F,0x6A,0x6D,0x64,0x63,
//    0x3E,0x39,0x30,0x37,0x22,0x25,0x2C,0x2B,0x06,0x01,0x08,0x0F,0x1A,0x1D,0x14,0x13,
//    0xAE,0xA9,0xA0,0xA7,0xB2,0xB5,0xBC,0xBB,0x96,0x91,0x98,0x9F,0x8A,0x8D,0x84,0x83,
//    0xDE,0xD9,0xD0,0xD7,0xC2,0xC5,0xCC,0xCB,0xE6,0xE1,0xE8,0xEF,0xFA,0xFD,0xF4,0xF3
//};
//
//+(unsigned int) CRC8: (unsigned char*)ucPtr len:(unsigned char)ucLen
//{
//    unsigned char ucIndex; //CRC8校验表格索引
//    unsigned char ucCRC8 = 0; //CRC8字节初始化
//    
//    //进行CRC8位校验
//    while (ucLen--)
//    {
//        ucIndex = ucCRC8^(*ucPtr++);
//        ucCRC8 = CRC8_TAB[ucIndex];
//    }
//    
//    //返回CRC8校验数据
//    return (~ucCRC8);
//}

@end
