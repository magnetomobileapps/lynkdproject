//
//  Protocol_BLEDeviceCall.h
//
//  ------------------------------------------------------------------
//  BLEDevice类对内接口
//  ------------------------------------------------------------------

@protocol Protocol_BLEDeviceCall <NSObject>

-(id)operate:(int)callType params:(NSArray*)params;

@end
