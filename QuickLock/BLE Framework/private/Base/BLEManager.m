//
//
//  BLEManager.m
//  
//  ------------------------------------------------------------------
//  对内管理类的实现部分
//  ------------------------------------------------------------------
#ifndef BC0_ItonBLEDefines_h
#define BC0_ItonBLEDefines_h

#define BC0_DEVICEINFOMATION_SERVICE_UUID       0x180A
#define BC0_DEVICEINFOMATION_SYSTEMID_UUID      0x2A23

#endif

#import "BLEManager.h"
#import <CoreBluetooth/CoreBluetooth.h>
#import <CoreBluetooth/CBService.h>
#import "Protocol_BLEDeviceCall.h"
#import <UIKit/UIKit.h>

//  BLEManager类（类的自身）的静态实例，单实例存在，外面通过sharedInstance函数调用此类
static BLEManager *this = nil;

//  BLEDevice设备ID计数器，每增加一个BLEDevice则自增1，作为该BLEDevice的DeviceID
static int nBLEDeviceNumber = 0;

//  CBCentralManager类，单实例存在，全部BLE设备的系统底层管理类
CBCentralManager *cm;

//  BLE设备列表，类型是CBPeripheral，通过UUID与BLEDevice进行关联和调用
NSMutableArray *bm_peripherals;

//  BLEDevice列表，类型是id<Protocol_BLEDeviceCall>，通过Protocol_BLEDeviceCall接口进行回调
NSMutableArray *bm_BLEDevices;

//  当前正在做连接操作的设备的DeviceID，临时变量，用于限制同一时刻仅能有一个设备处于连接操作中
int nOpeartingDeviceID = 0;

//  当前正在做连接操作的设备的localName，临时变量
NSString *temp_LocalName = @"";

//  temp_localNames是指定的多个localName列表; temp_Ranges是指定的多个localName的是否需要部分匹配的列表(1-部分匹配,0-完全匹配)
NSArray *temp_localNames;
NSArray *temp_Ranges;

//  当前正在做连接操作的设备的PeripheralUUID，临时变量
NSString *temp_PeripheralUUID = @"";

//  当前正在做连接操作的设备的Service的搜索状态，临时变量
bool bStartDiscover = false;

//  2013-08-30
//  用于首次连接的设备无UUID时，通过临时存储Index进行查找
int firstPeripheralIndex;

//  2013-10-23
//  用于查找发现service的次数，有时第一次无法发现服务，因此需要尝试做三次，如果不行再断开蓝牙
int nDiscoverServiceCount = 0;

//  2014-04-15
//  当前是否正在扫描返回广播设备列表的标志和临时数组变量，搜索结束后，状态和数据全部清空
BOOL bStartGetScanList;
NSMutableArray *arrayScanList;

//  2014-04-16
//  当前是否连接上次搜索到的直到索引的设备，连接结束后，状态和数据清空
//  nConnectScanIndex 指定的连接索引
//  temp_currentScanIndex 当前搜索到的索引
BOOL bConnectSacnListIndex;
int nConnectScanIndex, temp_currentScanIndex;

@implementation BLEManager
@synthesize lockDevice;

//  ------------------------------------------------------------------
//  头文件公开函数部分
//  ------------------------------------------------------------------

+(id)sharedInstance
{
        if (!this)
        {
                this = [[BLEManager alloc] init];
        }
        return this;
}

-(id)init
{
        self = [super init];
        
        bm_BLEDevices = [NSMutableArray new];
        cm = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
        cm.delegate = self;
        bm_peripherals = [NSMutableArray new];
        
        return self;
}

-(void)addBLEDevice:(id)bleDevice
{
        id<Protocol_BLEDeviceCall> _bd;
        
        nBLEDeviceNumber++;
        
        _bd = bleDevice;
        
        [self BLEDevice_setDeviceID:nBLEDeviceNumber bleDevice:_bd];
        
        [self setBLEDeviceToArray:bleDevice];
}

// 2013-10-25 交换两个device所对应的设备uuid
-(BOOL)changeBLEDevice:(int)destDeviceID sourceDeviceID:(int)sourceDeviceID
{
        NSString *_source_uuid, *_dest_uuid;
        id<Protocol_BLEDeviceCall> _source_bd, _dest_bd;
        NSArray *_source_arr, *_dest_arr;
        BOOL result;
        
        result = FALSE;
        _source_bd = [self getBLEDevice:sourceDeviceID];
        if (_source_bd != nil)
        {
                _source_uuid = [self BLEDevice_getDeviceUUID:_source_bd];
                
                _dest_bd = [self getBLEDevice:destDeviceID];
                if (_dest_bd != nil)
                {
                        //_dest_uuid = [self BLEDevice_getDeviceUUID:_dest_bd];
                        //_temp_uuid = _source_uuid;
                        //_source_uuid = _dest_uuid;
                        //_dest_uuid = _temp_uuid;
                        
                        _dest_uuid = _source_uuid;
                        _source_uuid = @"";
                        
                        _source_arr = @[_source_uuid];
                        _dest_arr = @[_dest_uuid];
                        [_source_bd operate:eCalltype_setDeviceUUID params:_source_arr];
                        [_dest_bd operate:eCalltype_setDeviceUUID params:_dest_arr];
                        
                        result = TRUE;
                }
        }
        return result;
}

-(BOOL)connectDevice:(int)bleDeviceID serviceUUID:(NSString*)serviceUUID localName:(NSString*)localName
{
        NSArray *_array;
        double timeOut = 1.0f;  // 2013-08-23 situation by the 1s to 2s, testing Rom
        
        if (nOpeartingDeviceID == 0)
        {
                firstPeripheralIndex = -1;
                nOpeartingDeviceID = bleDeviceID;
                temp_LocalName = localName;
                temp_PeripheralUUID = @"";
                if (![serviceUUID isEqualToString:@""])
                {
                        _array = [NSArray arrayWithObject:[CBUUID UUIDWithString:serviceUUID]];
                        
                        if(AntiLost_Debug)
                        {
                                NSLog(@"scan ------ start");
                        }
                        [cm scanForPeripheralsWithServices:_array options:nil];
                }
                else
                {
                        [cm scanForPeripheralsWithServices:nil options:nil];
                }
                
                [NSTimer scheduledTimerWithTimeInterval:timeOut target:self selector:@selector(stopScanPeripheral) userInfo:nil repeats:NO];
                [NSTimer scheduledTimerWithTimeInterval:timeOut target:self selector:@selector(stopOpearte) userInfo:nil repeats:NO];
                return true;
        }
        else
        {
                return false;
        }
}

-(BOOL)connectDevice:(int)bleDeviceID peripheralUUID:(NSString*)UUID
{
        CFUUIDRef puuid;
        NSArray *_parray;
        double timeOut = 1.0f;  // 2013-08-23 The case by the 1s to 2s, even without BAKCHOD of the test
        
        if (nOpeartingDeviceID == 0)
        {
                nOpeartingDeviceID = bleDeviceID;
                temp_LocalName = @"";
                temp_PeripheralUUID = UUID;
                        
                puuid = [BLEPublic StringToUUID: [UUID UTF8String]];
                _parray = [NSArray arrayWithObject:(id)CFBridgingRelease(puuid)];
            NSMutableArray *arrayOfId = [[NSMutableArray alloc]init];
            [arrayOfId addObject:UUID];
                
                if(AntiLost_Debug)
                {
                        NSLog(@"retire ------ start");
                }
                [cm retrievePeripheralsWithIdentifiers:arrayOfId];
             //   [cm retrievePeripherals:_parray];
            
                [NSTimer scheduledTimerWithTimeInterval:timeOut target:self selector:@selector(stopScanPeripheral) userInfo:nil repeats:NO];
                [NSTimer scheduledTimerWithTimeInterval:timeOut target:self selector:@selector(stopOpearte) userInfo:nil repeats:NO];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"AddLock" object:nil];
                NSLog(@"AddLock 1");
                return true;
        }
        else
        {
                return false;
        }
}

-(BOOL)initConnectWithNameOfRanges:(int)bleDeviceID serviceUUID:(NSString*)serviceUUID localNames:(NSArray*)localNames isRanges:(NSArray*)isRanges
{
        temp_localNames = localNames;
        temp_Ranges = isRanges;
        return [self connectDevice:bleDeviceID serviceUUID:serviceUUID localName:@"[Array]"]; //Expressed special definition, represents the upcoming multi-way localName search
}

-(BOOL)initConnectWithScanIndex:(int)bleDeviceID scanIndex:(int)scanIndex
{
        double timeOut = 1.0f;  // 2013-08-23 situation by the 1s to 2s, testing Rom
        if (nOpeartingDeviceID == 0)
        {
                firstPeripheralIndex = -1;
                nOpeartingDeviceID = bleDeviceID;
                
                bConnectSacnListIndex = YES;
                nConnectScanIndex = scanIndex;
                temp_currentScanIndex = -1;
                
                if(AntiLost_Debug)
                {
                        NSLog(@"scan ------ start");
                }
                [cm scanForPeripheralsWithServices:nil options:nil];
                
                [NSTimer scheduledTimerWithTimeInterval:timeOut target:self selector:@selector(stopScanPeripheral) userInfo:nil repeats:NO];
                [NSTimer scheduledTimerWithTimeInterval:timeOut target:self selector:@selector(stopOpearte) userInfo:nil repeats:NO];
                return true;
        }
        else
        {
                return false;
        }
}

-(BOOL)startConnect:(int)bleDeviceID isFirstPeripheral:(BOOL)isFirstPeripheral
{
        CBPeripheral *_bp;
        bool result;
        
        result = false;
        nDiscoverServiceCount = 3;
        
        if (!isFirstPeripheral)
        {
                _bp = [self getPeripheralofBLEDevice:bleDeviceID];
        }
        else
        {
                _bp = (CBPeripheral *)[bm_peripherals objectAtIndex:firstPeripheralIndex];
        }
        if (_bp != nil)
        {
                if(AntiLost_Debug)
                {
                        NSLog(@"startConnect ------");
                }
                [cm connectPeripheral: _bp options:nil];
                result = true;
        }
        return result;
}

-(BOOL)startDiscover:(int)bleDeviceID
{
        CBPeripheral *_bp;
        bool result;
        NSNumber *num;
        
        result = false;
        _bp = [self getPeripheralofBLEDevice:bleDeviceID];
        if (_bp != nil)
        {
                if(AntiLost_Debug)
                {
                        NSLog(@"StartDiscover ------");
                }
                [_bp discoverServices:nil];
                
                bStartDiscover = true;
                num = [[NSNumber alloc] initWithInt:bleDeviceID];
                //Set a timer 3 seconds discover, if the search is complete, it had not indicate failure, need to re-look
                [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(checkFailDiscover:) userInfo:num repeats:NO];
                
                result = true;
        }
        return result;
}

-(BOOL)disconnect:(int)bleDeviceID
{
        CBPeripheral *_bp;
        bool result;
        
        result = false;
        _bp = [self getPeripheralofBLEDevice:bleDeviceID];
        if (_bp != nil)
        {
                if(AntiLost_Debug)
                {
                        NSLog(@"disconnect ------");
                        arrayScanList = NULL;
                                NSLog(@"AddLock 2");
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"AddLock" object:nil];
//                    [[NSNotificationCenter defaultCenter] postNotificationName:@"startAnimt" object:nil];
                }
                [cm cancelPeripheralConnection:_bp];
                result = true;
        }
        return result;
}

-(BOOL)getIsConnect:(int)bleDeviceID
{
        CBPeripheral *_bp;
        bool result;
        
        result = false;
        _bp = [self getPeripheralofBLEDevice:bleDeviceID];
        if (_bp != nil)
        {
                if(AntiLost_Debug)
                {
                      //  NSLog(@"getIsConnect ------");
                }
//              result = _bp.isConnected;
                result = (_bp.state==CBPeripheralStateConnected);
        }
        return result;
}

-(BOOL)checkExistServiceUUID:(int)bleDeviceID serviceUUID:(NSString*)serviceUUID characteristicUUID:(NSString*)characteristicUUID
{
        CBPeripheral *_bp;
        bool result;
        int i, j;
        CBService *svr;
        CBCharacteristic *crt;
        CBUUID *cus, *cuc;
        
        result = false;
        if (![serviceUUID isEqualToString:@""])
        {
                cus = [CBUUID UUIDWithString:serviceUUID];
                
                _bp = [self getPeripheralofBLEDevice:bleDeviceID];
                
                for (i = 0; i < _bp.services.count; i++)
                {
                        svr = [_bp.services objectAtIndex:i];
                        if ([svr.UUID isEqual:cus])
                        {
                                if (![characteristicUUID isEqualToString:@""])
                                {
                                        cuc = [CBUUID UUIDWithString:characteristicUUID];
                                        
                                        for (j = 0; j < svr.characteristics.count; j++)
                                        {
                                                crt = [svr.characteristics objectAtIndex:j];
                                                if ([crt.UUID isEqual:cuc])
                                                {
                                                        result = true;
                                                }
                                        }
                                }
                                else
                                {
                                        result = true;
                                }
                        }
                }
        }
        return result;
}

-(BOOL)getScanDeviceList:(int)bleDeviceID scanTime:(float)scanTime
{
        if (nOpeartingDeviceID == 0)
        {
                nOpeartingDeviceID = bleDeviceID;
                bStartGetScanList = TRUE;
                arrayScanList = [NSMutableArray new];
                
                if(AntiLost_Debug)
                {
                        NSLog(@"getScanDeviceList ------ start");
                }
                [cm scanForPeripheralsWithServices:nil options:nil];
                
                [NSTimer scheduledTimerWithTimeInterval:scanTime target:self selector:@selector(stopScanPeripheral) userInfo:nil repeats:NO];
                [NSTimer scheduledTimerWithTimeInterval:scanTime target:self selector:@selector(stopOpearte) userInfo:nil repeats:NO];
                return true;
        }
        else
        {
                return false;
        }
}

// ISW HONESTSKY
-(BOOL)getBLEState:(int)bleDeviceID
{
        if (cm.state == CBCentralManagerStatePoweredOn)
        {
                return YES;
        }
        else
        {
                return NO;
        }
}

-(BOOL)getMACAddress:(int)bleDeviceID
{
        [self readValueForCharacteristic:bleDeviceID serviceUUID:BC0_DEVICEINFOMATION_SERVICE_UUID characteristicUUID:BC0_DEVICEINFOMATION_SYSTEMID_UUID];
        
        return true;
}

-(BOOL)readRSSI:(int)bleDeviceID
{
        CBPeripheral *_bp;
        bool result;
        
        result = false;
        _bp = [self getPeripheralofBLEDevice:bleDeviceID];
        if (_bp != nil)
        {
                if(AntiLost_Debug)
                {
                        NSLog(@"readRSSI ------");
                }
                [_bp readRSSI];
                result = true;
        }
        return result;
}

-(void)notification:(int)bleDeviceID serviceUUID:(int)serviceUUID characteristicUUID:(int)characteristicUUID on:(BOOL)on
{
        CBPeripheral *_bp;
        _bp = [self getPeripheralofBLEDevice:bleDeviceID];
        
        UInt16 s = [BLEPublic swap:serviceUUID];
        UInt16 c = [BLEPublic swap:characteristicUUID];
        NSData *sd = [[NSData alloc] initWithBytes:(char *)&s length:2];
        NSData *cd = [[NSData alloc] initWithBytes:(char *)&c length:2];
        CBUUID *su = [CBUUID UUIDWithData:sd];
        CBUUID *cu = [CBUUID UUIDWithData:cd];
        CBService *service = [BLEPublic findServiceFromUUID:su p:_bp];
        if (!service)
        {
                printf("2Could not find service with UUID %s on peripheral with UUID %s\r\n",[BLEPublic CBUUIDToString:su],[BLEPublic UUIDToString:(__bridge CFUUIDRef)(_bp.identifier)]);
                return;
        }
        CBCharacteristic *characteristic = [BLEPublic findCharacteristicFromUUID:cu service:service];
        if (!characteristic)
        {
                printf("Could not find characteristic with UUID %s on service with UUID %s on peripheral with UUID %s\r\n",[BLEPublic CBUUIDToString:cu],[BLEPublic CBUUIDToString:su],[BLEPublic UUIDToString:(__bridge CFUUIDRef)(_bp.identifier)]);
                return;
        }
        [_bp setNotifyValue:on forCharacteristic:characteristic];
}

-(void)readValueForCharacteristic:(int)bleDeviceID serviceUUID:(int)serviceUUID characteristicUUID:(int)characteristicUUID
{
        CBPeripheral *_bp;
        _bp = [self getPeripheralofBLEDevice:bleDeviceID];
        
        UInt16 s = [BLEPublic swap:serviceUUID];
        UInt16 c = [BLEPublic swap:characteristicUUID];
        NSData *sd = [[NSData alloc] initWithBytes:(char *)&s length:2];
        NSData *cd = [[NSData alloc] initWithBytes:(char *)&c length:2];
        CBUUID *su = [CBUUID UUIDWithData:sd];
        CBUUID *cu = [CBUUID UUIDWithData:cd];
        CBService *service = [BLEPublic findServiceFromUUID:su p:_bp];
        if (!service)
        {
                return;
        }
        CBCharacteristic *characteristic = [BLEPublic findCharacteristicFromUUID:cu service:service];
        if (!characteristic)
        {
                return;
        }
        [_bp readValueForCharacteristic:characteristic];
}

-(void)writeValueForCharacteristic:(int)bleDeviceID serviceUUID:(int)serviceUUID characteristicUUID:(int)characteristicUUID data:(NSData *)data
{
        CBPeripheral *_bp;
        _bp = [self getPeripheralofBLEDevice:bleDeviceID];
        
        UInt16 s = [BLEPublic swap:serviceUUID];
        UInt16 c = [BLEPublic swap:characteristicUUID];
        NSData *sd = [[NSData alloc] initWithBytes:(char *)&s length:2];
        NSData *cd = [[NSData alloc] initWithBytes:(char *)&c length:2];
        CBUUID *su = [CBUUID UUIDWithData:sd];
        CBUUID *cu = [CBUUID UUIDWithData:cd];
        CBService *service = [BLEPublic findServiceFromUUID:su p:_bp];
        if (!service)
        {
                if (AntiLost_Debug)
                {
                        printf("3Could not find service with UUID %s on peripheral with UUID %s\r\n",[BLEPublic CBUUIDToString:su],[BLEPublic UUIDToString:(__bridge CFUUIDRef)(_bp.identifier)]);
                }
                return;
        }
        CBCharacteristic *characteristic = [BLEPublic findCharacteristicFromUUID:cu service:service];
        if (!characteristic)
        {
                if (AntiLost_Debug)
                {
                        printf("Could not find characteristic with UUID %s on service with UUID %s on peripheral with UUID %s\r\n",[BLEPublic CBUUIDToString:cu],[BLEPublic CBUUIDToString:su],[BLEPublic UUIDToString:(__bridge CFUUIDRef)(_bp.identifier)]);
                }
                return;
        }
        
        CBCharacteristicWriteType ct;
        if ((characteristic.properties & CBCharacteristicPropertyWriteWithoutResponse) == CBCharacteristicPropertyWriteWithoutResponse)
        {
                ct = CBCharacteristicWriteWithoutResponse;
        }
        else
        {
                ct = CBCharacteristicWriteWithResponse;
        }
        
        [_bp writeValue:data forCharacteristic:characteristic type:ct];
}

-(void)readValueForDescriptor:(int)bleDeviceID serviceUUID:(int)serviceUUID characteristicUUID:(int)characteristicUUID descriptorUUID:(int)descriptorUUID
{
        CBPeripheral *_bp;
        _bp = [self getPeripheralofBLEDevice:bleDeviceID];
        
        UInt16 s = [BLEPublic swap:serviceUUID];
        UInt16 c = [BLEPublic swap:characteristicUUID];
        UInt16 d = [BLEPublic swap:descriptorUUID];
        NSData *sd = [[NSData alloc] initWithBytes:(char *)&s length:2];
        NSData *cd = [[NSData alloc] initWithBytes:(char *)&c length:2];
        NSData *dd = [[NSData alloc] initWithBytes:(char *)&d length:2];
        CBUUID *su = [CBUUID UUIDWithData:sd];
        CBUUID *cu = [CBUUID UUIDWithData:cd];
        CBUUID *du = [CBUUID UUIDWithData:dd];
        
        CBService *service = [BLEPublic findServiceFromUUID:su p:_bp];
        if (!service)
        {
                printf("3Could not find service with UUID %s on peripheral with UUID %s\r\n",[BLEPublic CBUUIDToString:su],[BLEPublic UUIDToString:(__bridge CFUUIDRef)(_bp.identifier)]);
                return;
        }
        
        CBCharacteristic *characteristic = [BLEPublic findCharacteristicFromUUID:cu service:service];
        if (!characteristic)
        {
                printf("Could not find characteristic with UUID %s on service with UUID %s on peripheral with UUID %s\r\n",[BLEPublic CBUUIDToString:cu],[BLEPublic CBUUIDToString:su],[BLEPublic UUIDToString:(__bridge CFUUIDRef)(_bp.identifier)]);
                return;
        }
        
        CBDescriptor *descriptor = [BLEPublic findDescriptorFromUUID:du characteristic:characteristic];
        if (!descriptor)
        {
                printf("Could not find descriptor with UUID %s on characteristic with UUID %s on peripheral with UUID %s\r\n",[BLEPublic CBUUIDToString:du],[BLEPublic CBUUIDToString:cu],[BLEPublic UUIDToString:(__bridge CFUUIDRef)(_bp.identifier)]);
                return;
        }
        
        [_bp readValueForDescriptor:descriptor];
}

-(void)writeValueForDescriptor:(int)bleDeviceID serviceUUID:(int)serviceUUID characteristicUUID:(int)characteristicUUID descriptorUUID:(int)descriptorUUID data:(NSData *)data
{
        CBPeripheral *_bp;
        _bp = [self getPeripheralofBLEDevice:bleDeviceID];
        
        UInt16 s = [BLEPublic swap:serviceUUID];
        UInt16 c = [BLEPublic swap:characteristicUUID];
        UInt16 d = [BLEPublic swap:descriptorUUID];
        NSData *sd = [[NSData alloc] initWithBytes:(char *)&s length:2];
        NSData *cd = [[NSData alloc] initWithBytes:(char *)&c length:2];
        NSData *dd = [[NSData alloc] initWithBytes:(char *)&d length:2];
        CBUUID *su = [CBUUID UUIDWithData:sd];
        CBUUID *cu = [CBUUID UUIDWithData:cd];
        CBUUID *du = [CBUUID UUIDWithData:dd];
        
        CBService *service = [BLEPublic findServiceFromUUID:su p:_bp];
        if (!service)
        {
                if (AntiLost_Debug)
                {
                        printf("3Could not find service with UUID %s on peripheral with UUID %s\r\n",[BLEPublic CBUUIDToString:su],[BLEPublic UUIDToString:(__bridge CFUUIDRef)(_bp.identifier)]);
                }
                return;
        }
    
        CBCharacteristic *characteristic = [BLEPublic findCharacteristicFromUUID:cu service:service];
        if (!characteristic)
        {
                printf("Could not find characteristic with UUID %s on service with UUID %s on peripheral with UUID %s\r\n",[BLEPublic CBUUIDToString:cu],[BLEPublic CBUUIDToString:su],[BLEPublic UUIDToString:(__bridge CFUUIDRef)(_bp.identifier)]);
                return;
        }
        
        CBDescriptor *descriptor = [BLEPublic findDescriptorFromUUID:du characteristic:characteristic];
        if (!descriptor)
        {
                printf("Could not find descriptor with UUID %s on characteristic with UUID %s on peripheral with UUID %s\r\n",[BLEPublic CBUUIDToString:du],[BLEPublic CBUUIDToString:cu],[BLEPublic UUIDToString:(__bridge CFUUIDRef)(_bp.identifier)]);
                return;
        }
        
        [_bp writeValue:data forDescriptor:descriptor];
}

//  ------------------------------------------------------------------
//  内部私有函数部分
//  ------------------------------------------------------------------

-(void)checkFailDiscover:(NSTimer *)timer
{
        int bleDeviceID;
        
        bleDeviceID = [((NSNumber *)timer.userInfo) intValue];
        
        if (bStartDiscover)
        {
                //2013-10-22 如果查找失败，则重复查找，最多3次
                nDiscoverServiceCount--;
                if (nDiscoverServiceCount > 0)
                {
                        if(AntiLost_Debug)
                        {
                                NSLog(@"repeatDiscover ------ : %d", nDiscoverServiceCount);
                        }
                        [self startDiscover:bleDeviceID];
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"AddLock" object:nil];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"startAnimt" object:nil];
                    NSLog(@"AddLock 3");
                }
                else
                {
                        bStartDiscover = false;
                        if(AntiLost_Debug)
                        {
                                NSLog(@"failDiscover ------");
                        }
                        [self BLEDevice_connectResult:false UUID:@"" bleDevice:[self getBLEDevice:bleDeviceID]];
                        [self disconnect:bleDeviceID];
                }
        }
}

-(void)stopScanPeripheral
{
        if(AntiLost_Debug)
        {
                NSLog(@"StopScan ------ ");
        }
        [cm stopScan];
}

-(void)stopOpearte
{
        if (nOpeartingDeviceID != 0)
        {
                if (bStartGetScanList) // Stop operation, if the state is in the search list, the first return to search list, and then clear the status and data
                {
                        [self BLEDevice_getScanList:arrayScanList bleDevice:[self getBLEDevice:nOpeartingDeviceID]];
                        
                        bStartGetScanList = false;
                        arrayScanList = [NSMutableArray new];
                }
                
                if (bConnectSacnListIndex) //停止操作时，如果处于指定连接index状态，则清除所有状态和数据
                {
                        bConnectSacnListIndex = NO;
                        nConnectScanIndex = -1;
                        temp_currentScanIndex = -1;
                }
                
                if(AntiLost_Debug)
                {
                        NSLog(@"stopOpearte ------ ");
                }
                nOpeartingDeviceID = 0;
        }
}

-(void)setBLEDeviceToArray:(id<Protocol_BLEDeviceCall>) bleDevice
{
        int n;
        
        n = [self findBLEDeviceIndexFromArray:[self BLEDevice_getDeviceID:bleDevice]];
        if (n >= 0)
        {
                [bm_BLEDevices replaceObjectAtIndex:n withObject:bleDevice];
        }
        else
        {
                [bm_BLEDevices addObject:bleDevice];
        }
}

-(int)findBLEDeviceIndexFromArray:(int)deviceID
{
        int i, n;
        id<Protocol_BLEDeviceCall> _bd;
        
        n = -1;
        for (i = 0; i < bm_BLEDevices.count; i++)
        {
                _bd = [bm_BLEDevices objectAtIndex:i];
                if ([self BLEDevice_getDeviceID:_bd] == deviceID)
                {
                        n = i;
                        break;
                }
        }
        return n;
}

-(id<Protocol_BLEDeviceCall>)getBLEDevice:(int)deviceID
{
        int n;
        
        n = [self findBLEDeviceIndexFromArray:deviceID];
        if (n >= 0)
        {
                return (id<Protocol_BLEDeviceCall>)[bm_BLEDevices objectAtIndex:n];
        }
        else
        {
                return nil;
        }
}

-(void)setPeripheralToArray:(CBPeripheral *)peripheral
{
        int i, n ;
        BOOL isOver;
        CBPeripheral *_p;
        NSString *_uuid;
        isOver = false;
        n = -1;
        
        for(i = 0; i < bm_peripherals.count; i++)
        {
                _p = [bm_peripherals objectAtIndex:i];
                //if ([BLEPublic comparePeripheralUUID:_p peripheral2:peripheral])
                if ([_p isEqual:peripheral])        //2013-08-30
                {
                        [bm_peripherals replaceObjectAtIndex:i withObject:peripheral];
                        //return;
                        isOver = true;
                        n = i;
                }
        }
        if (!isOver)
        {
                [bm_peripherals addObject:peripheral];
                n  = bm_peripherals.count - 1;
        }
        
        // 2013-08-30
        // 检查当前设备UUID是否为空
        // 如果为空，表示是初次连接的设备，需要额外处理。保留临时的设备Index
        _uuid = [NSString stringWithUTF8String:[BLEPublic UUIDToString:(__bridge CFUUIDRef)(peripheral.identifier)]];
        if ((_uuid == nil) || ([_uuid isEqualToString:@""]))
        {
                firstPeripheralIndex = n;
        }
}

-(int)getPeripheralIndexFromArray:(NSString *)UUID
{
        int i, n;
        CBPeripheral *_bp;
        NSString *_uuid;
        
        n = -1;
        for (i = 0; i < bm_peripherals.count; i++)
        {
                _bp = [bm_peripherals objectAtIndex:i];
                _uuid = [NSString stringWithUTF8String:[BLEPublic UUIDToString:(__bridge CFUUIDRef)(_bp.identifier)]];
                if ([_uuid isEqualToString:UUID])
                {
                        n = i;
                        break;
                }
        }
        return n;
}

-(CBPeripheral *)getPeripheralFromArray:(NSString *)UUID
{
        int n;
        CBPeripheral *_bp;
        
        _bp = nil;
        n = [self getPeripheralIndexFromArray:UUID];
        if (n > -1)
        {
                _bp = [bm_peripherals objectAtIndex:n];
        }
        return _bp;
}


-(id<Protocol_BLEDeviceCall>)getBLEDeviceofPeripheral:(CBPeripheral *)peripheral
{
        NSString *_uuid;
        NSString *s;
        int i, n;
        id<Protocol_BLEDeviceCall> _bd;
        
        _uuid = [NSString stringWithUTF8String:[BLEPublic UUIDToString:(__bridge CFUUIDRef)(peripheral.identifier)]];
        n = [self getPeripheralIndexFromArray:_uuid];
        if ( n >= 0)
        {
                for (i = 0; i < bm_BLEDevices.count; i++)
                {
                        _bd = (id<Protocol_BLEDeviceCall>)[bm_BLEDevices objectAtIndex:i];
                        if ([[self BLEDevice_getDeviceUUID:_bd] isEqualToString:_uuid])
                        {
                                return _bd;
                        }
                }
                // 如果未找到UUID相等的设备，则可能存在初次连接的设备，需要通过特殊标识符进行查找
                for (i = 0; i < bm_BLEDevices.count; i++)
                {
                        
                        _bd = (id<Protocol_BLEDeviceCall>)[bm_BLEDevices objectAtIndex:i];
                        s = [self BLEDevice_getDeviceUUID:_bd];
                        if ([s isEqualToString:@"firstPeripheral"])
                        {
                                return _bd;
                        }
                }
        }
        return nil;
}

-(CBPeripheral *)getPeripheralofBLEDevice:(int)deviceID
{
        id<Protocol_BLEDeviceCall> _bd;
        NSString *_uuid;
        CBPeripheral *_bp;
        
        _bp = nil;
        _bd = [self getBLEDevice:deviceID];
        if (_bd != nil)
        {
                _uuid = [self BLEDevice_getDeviceUUID:_bd];
                if (![_uuid isEqualToString:@""])
                {
                        _bp = [self getPeripheralFromArray:_uuid];
                }
        }
        return _bp;
}

-(BOOL)checkPeripheralLocalName:(NSString*)pLocalName
{
        BOOL isMul; //首先判断是否多localName
    int i ;//, n1, n2, n3;
        NSString *s1, *s2;
    
    long int n1, n2, n3;
        
        if ([temp_LocalName isEqualToString:@"[Array]"])
        {
                isMul = true;
        }
        else
        {
                isMul = false;
        }
        
        if (!isMul) //非多localName,只要比较设备名称是否一致就可以
        {
                return [pLocalName isEqualToString:temp_LocalName];
        }
        else //多localName,则需要逐一比较,只要有任何一个通过都可以
        {
                n1 = [temp_localNames count];
                for (i = 0; i < n1; i++)
                {
                        s1 = (NSString*)[temp_localNames objectAtIndex:i];
                        n2 = [(NSNumber*)[temp_Ranges objectAtIndex:i] intValue];
                        if (n2 == 0)  //0表示完全匹配
                        {
                                if ([pLocalName isEqualToString:s1])
                                {
                                        return true;
                                }
                        }
                        else //1表示不完全匹配,因此需要先根据匹配字符串的长度进行截取,然后与之比较
                        {
                                n3 = [s1 length];
                                s2 = [pLocalName substringToIndex:n3];
                                if ([s2 isEqualToString:s1])
                                {
                                        return true;
                                }
                        }
                }
        }
        return false;
}

//  显示广播数据
- (void)showAdvertisementData:(NSDictionary *)advertisementData
{
        //    CBAdvertisementDataLocalNameKey
        //    CBAdvertisementDataTxPowerLevelKey
        //    CBAdvertisementDataServiceUUIDsKey
        //    CBAdvertisementDataServiceDataKey
        //    CBAdvertisementDataManufacturerDataKey
        //    CBAdvertisementDataOverflowServiceUUIDsKey
        //    CBAdvertisementDataIsConnectable
        //    CBAdvertisementDataSolicitedServiceUUIDsKey
        
        NSString *_LocalNameKey, *sss, *_s1, *_s2, *_s3, *_s4, *_s5, *_s6, *_s7, *_s8, *_s9, *_s10;
        NSNumber *_TxPowerLevelKey, *_IsConnectable;
        int _n1;
        NSArray *_ServiceUUIDsKey, *_OverflowServiceUUIDsKey, *_SolicitedServiceUUIDsKey;
        CBUUID *_cb;
        NSDictionary *_ServiceDataKey;
        NSData *_d1, *_ManufacturerDataKey;
        BOOL _b1;
        
        _LocalNameKey               = [advertisementData objectForKey:CBAdvertisementDataLocalNameKey];
        _TxPowerLevelKey            = [advertisementData objectForKey:CBAdvertisementDataTxPowerLevelKey];
        _ServiceUUIDsKey            = [advertisementData objectForKey:CBAdvertisementDataServiceUUIDsKey];
        _ServiceDataKey             = [advertisementData objectForKey:CBAdvertisementDataServiceDataKey];
        _ManufacturerDataKey        = [advertisementData objectForKey:CBAdvertisementDataManufacturerDataKey];
        _OverflowServiceUUIDsKey    = [advertisementData objectForKey:CBAdvertisementDataOverflowServiceUUIDsKey];
        //    _IsConnectable              = [advertisementData objectForKey:CBAdvertisementDataIsConnectable]; //iOS6.1 未定义
        //    _SolicitedServiceUUIDsKey   = [advertisementData objectForKey:CBAdvertisementDataSolicitedServiceUUIDsKey]; //iOS6.1 未定义
        
        _n1 = [_TxPowerLevelKey intValue];
        
        _s1 = @"";
        _s3 = @"";
        for (int _i = 0; _i < _ServiceUUIDsKey.count; _i++ )
        {
                _cb = [_ServiceUUIDsKey objectAtIndex:_i];
                
                //解析ServiceUUID
                _s2 = [NSString stringWithUTF8String:[BLEPublic CBUUIDToString:_cb]];
                _s1 = [NSString stringWithFormat:@"%@, %@", _s2, _s1];
                
                //解析ServiceData
                _d1 = [_ServiceDataKey objectForKey:_cb];
                _s4 = [[NSString alloc] initWithData:_d1 encoding:NSUTF8StringEncoding];
                _s3 = [NSString stringWithFormat:@"%@, %@", _s4, _s3];
        }
        
        _s5 = [[NSString alloc] initWithData:_ManufacturerDataKey encoding:NSUTF8StringEncoding];
        
        _s6 = @"";
        for (int _i = 0; _i < _OverflowServiceUUIDsKey.count; _i++ )
        {
                _cb = [_OverflowServiceUUIDsKey objectAtIndex:_i];
                _s7 = [NSString stringWithUTF8String:[BLEPublic CBUUIDToString:_cb]];
                _s6 = [NSString stringWithFormat:@"%@, %@", _s7, _s6];
        }
        
        _b1 = [_IsConnectable boolValue];
        _s8 = _b1?@"True":@"False";
        
        _s9 = @"";
        for (int _i = 0; _i < _SolicitedServiceUUIDsKey.count; _i++ )
        {
                _cb = [_SolicitedServiceUUIDsKey objectAtIndex:_i];
                _s10 = [NSString stringWithUTF8String:[BLEPublic CBUUIDToString:_cb]];
                _s9 = [NSString stringWithFormat:@"%@, %@", _s10, _s9];
        }
        
        sss = [NSString stringWithFormat:@"[LocalName] %@;\r\n[TxPowerLevel] %d;\r\n[ServiceUUIDs] %@;\r\n[ServiceData] %@; \r\n[ManufacturerData] %@;\r\n[OverflowServiceUUIDs] %@;\r\n[IsConnectable] %@;\r\n[SolicitedServiceUUIDs] %@", _LocalNameKey, _n1, _s1, _s3, _s5, _s6, _s8, _s9];
        
        UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"advertisementData" message:sss delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertview show];
        
}

// ISW HONESTSKY 2014-11-20
//  Adding data format is to arrayScan List： LocalName,ServiceUUID,UUID,RSSI,TxPower,ManufacturerData
-(void)addSancListArray:(NSString*)localName UUID:(NSString*)uuid RSSI:(NSNumber*)RSSI advertisementData:(NSDictionary *)advertisementData
{
        NSString *_localName, *_serviceUUID, *_UUID, *_rssi, *_txPower, *s, *_s5;
        NSData *_ManufacturerDataKey;
        NSArray *_svrUUIDs;
        NSNumber *_txPowerLevel;
        float f;
        CBUUID *cu;
        int length;
        
        _localName = localName;
        _UUID = uuid;
        
        f = [RSSI floatValue];
        _rssi = [NSString stringWithFormat:@"%.02f", f];
        
        _svrUUIDs = [advertisementData objectForKey:CBAdvertisementDataServiceUUIDsKey];
        if ((_svrUUIDs != NULL) && (_svrUUIDs.count > 0))
        {
                cu = (CBUUID *)[_svrUUIDs objectAtIndex:0];
                _serviceUUID = [NSString stringWithUTF8String: [BLEPublic CBUUIDToString: cu]];
        }
        else
        {
                _serviceUUID = @"";
        }
        
        _txPowerLevel = [advertisementData objectForKey:CBAdvertisementDataTxPowerLevelKey];
        
        _ManufacturerDataKey = [advertisementData objectForKey:CBAdvertisementDataManufacturerDataKey];
        
        // ISW ANNE 2014-11-26 回应信息
        if (_ManufacturerDataKey != nil)
        {
                _s5 = @"";
                length = (int)[_ManufacturerDataKey length];
                Byte testAddress[length];
                [_ManufacturerDataKey getBytes:&testAddress length:length];
                for(int i = 0; i < length; i++)
                {
                        _s5 = [NSString stringWithFormat:@"%@%02x", _s5, testAddress[i]];
                }
        }
        
        f = [_txPowerLevel floatValue];
        _txPower = [NSString stringWithFormat:@"%.02f", f];
        
        s = [NSString stringWithFormat:@"%@,%@,%@,%@,%@,%@", _localName, _serviceUUID, _UUID, _rssi, _txPower, _s5];
        if (arrayScanList == NULL || arrayScanList.count == 0)
        {
                arrayScanList = [NSMutableArray new];
            [arrayScanList addObject:s];
        }
}

//  ------------------------------------------------------------------
//  Protocol_BLEDeviceCall协议的解析部分
//  ------------------------------------------------------------------

-(int)BLEDevice_getDeviceID:(id<Protocol_BLEDeviceCall>) bleDevice
{
        NSNumber *result;
        result = (NSNumber *)[bleDevice operate:eCallType_getDeviceID params:nil];
        return [result intValue];
}

-(void)BLEDevice_setDeviceID:(int)value bleDevice:(id<Protocol_BLEDeviceCall>) bleDevice
{
        NSArray *_arr;
        NSNumber *num;
        id result;
        
        num = [[NSNumber alloc] initWithInt:value];
        _arr = @[num];
        result = [bleDevice operate:eCalltype_setDeviceID params:_arr];
}

-(NSString*)BLEDevice_getDeviceUUID:(id<Protocol_BLEDeviceCall>) bleDevice
{
        id result;
        result = [bleDevice operate:eCallType_getDeviceUUID params:nil];
        return (NSString *)result;
}


-(void)BLEDevice_getPeripheralSuccess:(NSString*)UUID bleDevice:(id<Protocol_BLEDeviceCall>) bleDevice
{
        id result;
        NSArray *_arr;
        
        _arr = @[UUID];
        result = [bleDevice operate:eCalltype_getPeripheralSuccess params:_arr];
}

-(void)BLEDevice_connectResult:(BOOL)bResult UUID:(NSString *)UUID bleDevice:(id<Protocol_BLEDeviceCall>) bleDevice
{
        id result;
        NSArray *_arr;
        NSNumber *num;
        
        num = [[NSNumber alloc] initWithBool:bResult];
        _arr = @[num, UUID];
        result = [bleDevice operate:eCallType_connectResult params:_arr];
        firstPeripheralIndex = -1;
}

-(void)BLEDevice_connectOK:(id<Protocol_BLEDeviceCall>) bleDevice
{
        id result;
        result = [bleDevice operate:eCalltype_connectOK params:nil];
}

-(void)BLEDevice_UpdateValueForCharacteristic:(CBCharacteristic *)characteristic bleDevice:(id<Protocol_BLEDeviceCall>) bleDevice
{
        id result;
        NSArray *_arr;
        
        _arr = @[characteristic];
        result = [bleDevice operate:eCallType_UpdateValueForCharac params:_arr];
}

-(void)BLEDevice_didDisConnect:(id<Protocol_BLEDeviceCall>) bleDevice
{
        id result;
        result = [bleDevice operate:eCallType_didDisConnect params:nil];
}

-(void)BLEDevice_didUpdateRSSI:(float)rssiValue bleDevice:(id<Protocol_BLEDeviceCall>) bleDevice
{
        id result;
        NSArray *_arr;
        NSNumber *num;
        
        num = [NSNumber numberWithFloat:rssiValue];
        _arr = @[num];
        result = [bleDevice operate:eCallType_didUpdateRSSI params:_arr];
}

-(void)BLEDevice_getScanList:(NSMutableArray *)arrayScanList bleDevice:(id<Protocol_BLEDeviceCall>) bleDevice
{
    id result;
    NSArray *_arr;
    
    @try {
        _arr = @[arrayScanList];
        result = [bleDevice operate:eCallType_getScanList params:_arr];
    } @catch (NSException *exception) {
        NSLog(@"Exception : %@",exception);
    } @finally {
        NSLog(@"-BLEDevice_getScanList-");
    }
}

// ISW HONESTSKY 2014-12-19 TEST respond connect flash back
-(void)stopAllScan
{
        [self stopScanPeripheral];
        
        if (nOpeartingDeviceID != 0)
        {
                if (bStartGetScanList) //停止操作时，如果处于搜索列表状态，则清除状态和数据
                {
                        bStartGetScanList = false;
                        arrayScanList = [NSMutableArray new];
                }
                
                if (bConnectSacnListIndex) //停止操作时，如果处于指定连接index状态，则清除所有状态和数据
                {
                        bConnectSacnListIndex = NO;
                        nConnectScanIndex = -1;
                        temp_currentScanIndex = -1;
                }
                
                if(AntiLost_Debug)
                {
                        NSLog(@"stopOpearte ------ ");
                }
                nOpeartingDeviceID = 0;
        }
}

//  ------------------------------------------------------------------
//  CBCentralManagerDelegate协议的实现部分
//  ------------------------------------------------------------------

//  centralManager状态改变时，此方法回调。
//  如状态变为CBCentralManagerStatePoweredOn，意味着scan停止，所有连接断开了。
//  如状态变为CBCentralManagerStatePoweredOff，意味着所有设备实例无效，需要重新检索或者查找。
- (void)centralManagerDidUpdateState:(CBCentralManager *)central
{
        if(AntiLost_Debug)
        {
            NSLog(@"central.state ------ %d", central.state);
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"showView" object:nil];
            if (central.state == 4)
            {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"kDisConnect" object:nil];
            }
        }
        if (central.state != CBCentralManagerStatePoweredOn)
        {
                nOpeartingDeviceID = 0;
        }
        //TODO: Untreated notice temporarily in different states, different states to send data to the interface class instance
}

// The scanning process, the device is found, this method callback.
// Find equipment, it is possible to read a set of broadcast data through CBAdvertisementData
// Find equipment must be saved, otherwise they will be considered useless, so as to be cleared Management Center
- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI
{
        // ISW HONESTSKY 2014-11-26 Test the connection process app flash back
        @try
        {
                NSString *_localName, *_uuid;
                NSString *_attName, *_service;
                NSArray *_serviceUUIDs;
                BOOL isEanbleConnect;
                
                //[self showAdvertisementData:advertisementData]; //Tests show broadcast data
                
                if (nOpeartingDeviceID == 0)
                {
                        return;
                        if(AntiLost_Debug)
                        {
                                NSLog(@"didDiscoverPeripheral --- do not isOperating, exit.");
                        }
                }
                
                
                // ISW HONESTSKY 2014-11-26 测试连接过程中app闪退
                @try
                {
                        _uuid = [NSString stringWithUTF8String:[BLEPublic UUIDToString:(__bridge CFUUIDRef)(peripheral.identifier)]];
                        _localName = [advertisementData objectForKey:CBAdvertisementDataLocalNameKey];
                }
                @catch (NSException *exception)
                {
                        //                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DISCOVER ERROR 2!" message:exception.reason delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                        //                        [alert show];
                        return;
                }
                @finally
                {
                }
                
                
                if(AntiLost_Debug)
                {
                        //                _attName = peripheral.name;
                        //                _serviceUUIDs = [advertisementData objectForKey:CBAdvertisementDataServiceUUIDsKey];
                        //                _service = [NSString stringWithUTF8String: [BLEPublic CBUUIDToString:((CBUUID *)[_serviceUUIDs objectAtIndex:0])]];
                        //
                        //                NSLog(@"localName --- %@, attName --- %@, service --- %@, uuid --- %@, RSSI --- %d", _localName, _attName, _service, _uuid, [RSSI intValue]);
                }
                
                isEanbleConnect = NO;
                
                //If you are in search for a list of state, the only record search list data, not active connection
                if (bStartGetScanList)
                {
                        [self addSancListArray:_localName UUID:_uuid RSSI:RSSI advertisementData:advertisementData];
                        return;
                }
                //If you are in a join index specified state, the only connection to the order search list
                else if (bConnectSacnListIndex)
                {
                        temp_currentScanIndex = temp_currentScanIndex + 1;
                        if ((nConnectScanIndex == temp_currentScanIndex) && (nConnectScanIndex >= 0))
                        {
                                isEanbleConnect = YES;
                        }
                }
                //The normal connection, check localName
                else
                {
                        //Change the identification of many complex algorithms localName
                        if (![self checkPeripheralLocalName:_localName])
                        {
                                isEanbleConnect = NO;
                                //                        return;
                        }
                        else
                        {
                                isEanbleConnect = YES;
                        }
                }
                
                if (isEanbleConnect)
                {
                        //Before attempting to connect, you must hold the device handle
                        peripheral.delegate = self;
                        [self setPeripheralToArray:peripheral];
                        
                        //Notification to the upper layer has been acquired, you can take next action
                        [self BLEDevice_getPeripheralSuccess:_uuid bleDevice:[self getBLEDevice:nOpeartingDeviceID]];
                        
                        //Close the current operation content
                        [self stopOpearte];
                }
        }
        @catch (NSException *exception)
        {
                //                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"DISCOVER ERROR 1!" message:exception.reason delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                //                [alert show];
                return;
        }
        @finally
        {
                
        }
}

// RetrievePeripherals made during device is found, this method callback.
// Returns the list of devices, and equipment is provided UUID retrieved matches.
- (void)centralManager:(CBCentralManager *)central didRetrievePeripherals:(NSArray *)peripherals
{
        CBPeripheral *_peripheral;
        NSString *_uuid;
        
        if (nOpeartingDeviceID == 0)
        {
                return;
                if(AntiLost_Debug)
                {
                        NSLog(@"didRetrievePeripherals ------ do not isOperating, exit.");
                }
        }
        if(AntiLost_Debug)
        {
                NSLog(@"didRetrievePeripherals Count ------ %lu", (unsigned long)peripherals.count);
        }
        
        for(int i = 0; i < peripherals.count; i++)
        {
                _peripheral = (CBPeripheral *)[peripherals objectAtIndex:i];
                _uuid = [NSString stringWithUTF8String:[BLEPublic UUIDToString:(__bridge CFUUIDRef)(_peripheral.identifier)]];
                
                if([_uuid isEqualToString:temp_PeripheralUUID])
                {
                        //Before attempting to connect, you must hold the device handle
                        _peripheral.delegate = self;
                        [self setPeripheralToArray:_peripheral];
                        
                        //Notification to the upper layer has been acquired, you can take next action
                        [self BLEDevice_getPeripheralSuccess:_uuid bleDevice:[self getBLEDevice:nOpeartingDeviceID]];
                        
                        //Close the current operation content
                        [self stopOpearte];
                        break;
                }
        }
}

// RetrieveConnectedPeripherals made during device is found, this method callback.
// Returns the list of devices, it is the system of all the connected devices.
- (void)centralManager:(CBCentralManager *)central didRetrieveConnectedPeripherals:(NSArray *)peripherals
{
        //TODO: Temporarily unprocessed read already connected device may be used on the device after power reconnection, may be invoked in some cases need
}

//  Copula connectPeripheral successful, this method callback.
- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral
{
        NSString *_uuid;
        
        _uuid = [NSString stringWithUTF8String:[BLEPublic UUIDToString:(__bridge CFUUIDRef)(peripheral.identifier)]];
        if(AntiLost_Debug)
        {
            NSLog(@"didConnectPeripheral ------ %@", _uuid);
            appDelegate.lock_Address = _uuid;
            [[NSNotificationCenter defaultCenter] postNotificationName:@"stopAnimation" object:nil];
//            [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:@"isDeviceConnected"];
        }
        [self BLEDevice_connectResult:true UUID:_uuid bleDevice:[self getBLEDeviceofPeripheral:peripheral]];
}

// Connect function connectPeripheral connection fails, this callback method.
// Try to connect at no timeout, this connection error is often atypical and is instantaneous (?)
- (void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
        NSString *_uuid;
        
        _uuid = [NSString stringWithUTF8String:[BLEPublic UUIDToString:(__bridge CFUUIDRef)(peripheral.identifier)]];
        if(AntiLost_Debug)
        {
                NSLog(@"didFailToConnectPeripheral ------ %@", _uuid);
            [[NSNotificationCenter defaultCenter] postNotificationName:@"AddLock" object:nil];
                        NSLog(@"AddLock 4");
        }
        [self BLEDevice_connectResult:false UUID:@"" bleDevice:[self getBLEDeviceofPeripheral:peripheral]];
}

//  Connected device, when disconnected, this method callback.
//  If this is not being initiated cancelPeripheralConnection callback method, you can find the cause of the error in the error.
//  Once this method is the callback, the entire delegate any other functions will no longer be called back (the last one before disconnecting the callback function)
- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
    NSString *_uuid;

    NSString *str = [[NSUserDefaults standardUserDefaults] valueForKey:@"enteredInView"];
    NSLog(@"str : %@",str);
    NSLog(@"errorCode --- %i", (int)error.code);
    if (error.code == 6 && ![str isEqualToString:@"YES"])
    {
        NSLog(@"errorCode1 --- %i", (int)error.code);
        [[NSNotificationCenter defaultCenter] postNotificationName:@"startAnimt" object:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"AddLock" object:nil];
                    NSLog(@"AddLock 5");
    }
    [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"enteredInView"];
    
    _uuid = [NSString stringWithUTF8String:[BLEPublic UUIDToString:(__bridge CFUUIDRef)(peripheral.identifier)]];
    if(AntiLost_Debug)
    {
        NSLog(@"value : %@",[[NSUserDefaults standardUserDefaults] valueForKey:@"mailViewShow"]);
        if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"mailViewShow"] isEqualToString:@"YES"])
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"hideMailComposeView" object:nil];
            [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"mailViewShow"];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [[NSNotificationCenter defaultCenter] postNotificationName:@"startAnimt" object:nil];
            });
        }

        NSLog(@"Disconnect ------ %@, error --- %@", _uuid, error);
        [[NSNotificationCenter defaultCenter] postNotificationName:@"startAnimt" object:nil];
    }
    
    NSString *strErrMsg = [NSString stringWithFormat:@"%@",[error.userInfo valueForKey:@"NSLocalizedDescription"]];
    if ([strErrMsg isEqualToString:@"The connection has timed out unexpectedly."])
    {
        NSLog(@"The connection has timed out unexpectedly.");
        [[NSNotificationCenter defaultCenter] postNotificationName:@"startAnimt" object:nil];
    }
    [self BLEDevice_didDisConnect:[self getBLEDeviceofPeripheral:peripheral]];
    arrayScanList = NULL;
}

//  ------------------------------------------------------------------
//  CBPeripheralDelegate Implement part of the agreement
//  ------------------------------------------------------------------

//  discoverServices The success (or failure), this callback method.
//  Find all CBService After the end, this will be a callback method, then the result can be read by the service properties of the device
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error
{
        if(AntiLost_Debug)
        {
                NSLog(@"services.count ------ %lu", (unsigned long)peripheral.services.count);
        }
        
        for (int i = 0; i < peripheral.services.count; i++)
        {
                CBService *s = [peripheral.services objectAtIndex:i];
                [peripheral discoverCharacteristics:nil forService:s];
        }
}

//  When CBService device is changed, this method callback.
//  Once this method is the callback, which means you must re-search CBService list of devices (call discoverServices)
- (void)peripheralDidInvalidateServices:(CBPeripheral *)peripheral NS_AVAILABLE(NA, 6_0)
{
        NSString *_uuid;
        
        _uuid = [NSString stringWithUTF8String:[BLEPublic UUIDToString:(__bridge CFUUIDRef)(peripheral.identifier)]];
        if(AntiLost_Debug)
        {
                NSLog(@"peripheralDidInvalidateServices --- %@", _uuid);
        }
        //TODO: 暂时未处理
}

//  当设备名称((AttName))改变时，此方法回调。
- (void)peripheralDidUpdateName:(CBPeripheral *)peripheral NS_AVAILABLE(NA, 6_0)
{
        NSString *_uuid;
        
        _uuid = [NSString stringWithUTF8String:[BLEPublic UUIDToString:(__bridge CFUUIDRef)(peripheral.identifier)]];
        if(AntiLost_Debug)
        {
                NSLog(@"peripheralDidUpdateName --- UUID: %@, NAME: %@", _uuid, peripheral.name);
        }
        //TODO: 暂时未处理
}

//  discoverCharacteristics:forService成功(或失败)，此方法回调。
//  指定service下的characteristic查找全部结束之后，才会回调此方法
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{
        if(AntiLost_Debug)
        {
                NSLog(@"Characteristics of service with UUID : %s found ------ %lu\r\n",[BLEPublic CBUUIDToString:service.UUID],(unsigned long)service.characteristics.count);
        }
        
        for(int i = 0; i < service.characteristics.count; i++)
        {
                CBCharacteristic *c = [service.characteristics objectAtIndex:i];
                //[peripheral discoverDescriptorsForCharacteristic:c];
                if (AntiLost_Debug)
                {
                        printf("Found characteristic %s\r\n",[BLEPublic CBUUIDToString:c.UUID]);
                }
        }
        
        CBService *s = [peripheral.services objectAtIndex:(peripheral.services.count - 1)];
        if([BLEPublic compareCBUUID:service.UUID UUID2:s.UUID])
        {
                if(AntiLost_Debug)
                {
                        NSLog(@"Finished discovering characteristics.connectingIndex");
                    NSLog(@"Error : %@",error.localizedDescription);
                }
                
                bStartDiscover = false;
                [self BLEDevice_connectOK:[self getBLEDeviceofPeripheral:peripheral]];
        }
}

//  readRSSI成功(或失败)，此方法回调。
- (void)peripheralDidUpdateRSSI:(CBPeripheral *)peripheral error:(NSError *)error
{
        NSString *_uuid;
        float rssiValue;
        
        _uuid = [NSString stringWithUTF8String:[BLEPublic UUIDToString:(__bridge CFUUIDRef)(peripheral.identifier)]];
        
        if (error == nil)
        {
                rssiValue = [peripheral.RSSI floatValue];
        }
        else
        {
                rssiValue = 9999.0f; //表示本次读取不成功
        }
        [self BLEDevice_didUpdateRSSI:rssiValue bleDevice:[self getBLEDeviceofPeripheral:peripheral]];
        
        if(AntiLost_Debug)
        {
                NSLog(@"peripheralDidUpdateRSSI ------ %@, %f, error --- %@", _uuid, rssiValue, error);
        }
}

// ISW HONESTSKY 2014-11-03
//  readRSSI成功(或失败)，此方法回调。
//上面的方法已过期用这个方法代替，适配iOS8.0
- (void)peripheral:(CBPeripheral *)peripheral didReadRSSI:(NSNumber *)RSSI error:(NSError *)error
{
        NSString *_uuid;
        float rssiValue;
        
        _uuid = [NSString stringWithUTF8String:[BLEPublic UUIDToString:(__bridge CFUUIDRef)(peripheral.identifier)]];
        
        if (error == nil)
        {
                rssiValue = [RSSI floatValue];
        }
        else
        {
                rssiValue = 9999.0f; //表示本次读取不成功
        }
        [self BLEDevice_didUpdateRSSI:rssiValue bleDevice:[self getBLEDeviceofPeripheral:peripheral]];
        
        if(AntiLost_Debug)
        {
                NSLog(@"peripheralDidUpdateRSSI ------ %@, %f, error --- %@", _uuid, rssiValue, error);
        }
}

//  readValueForCharacteristic成功(或失败)，此方法回调。
//  必须在notification/indication的使能状态被打开的前提下才会回调此方法。
- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
        if (!error)
        {
                [self BLEDevice_UpdateValueForCharacteristic:characteristic bleDevice:[self getBLEDeviceofPeripheral:peripheral]];
        }
        else
        {
                if(AntiLost_Debug)
                {
                        NSLog(@"didUpdateValueForCharacteristic --- %@",error);
                }
        }
}

//  writeValue:forCharacteristic成功(或失败)，此方法回调。
- (void)peripheral:(CBPeripheral *)peripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
        if(AntiLost_Debug)
        {
                NSLog(@"didWriteValueForCharacteristic --- %@", error.localizedDescription);
        }
        //TODO: 暂时未处理
}

//  setNotifyValue:forCharacteristic The success (or failure), this callback method.
- (void)peripheral:(CBPeripheral *)peripheral didUpdateNotificationStateForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
        if(AntiLost_Debug)
        {
                NSLog(@"didUpdateNotificationStateForCharacteristic --- %@", error);
        }
        //TODO: Untreated temporarily
}

//  readValueForDescriptor The success (or failure), this callback method.
- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForDescriptor:(CBDescriptor *)descriptor error:(NSError *)error
{
        if(AntiLost_Debug)
        {
                NSLog(@"didUpdateValueForDescriptor --- %@", error);
        }
        //TODO: 暂时未处理
}

//  writeValue:forDescriptor成功(或失败)，此方法回调。
- (void)peripheral:(CBPeripheral *)peripheral didWriteValueForDescriptor:(CBDescriptor *)descriptor error:(NSError *)error
{
        if(AntiLost_Debug)
        {
                NSLog(@"didWriteValueForDescriptor --- %@", error);
        }
        //TODO: 暂时未处理
}

//  discoverIncludedServices:forService成功(或失败)，此方法回调。
//  此时可以通过设备的service属性中的includedServices属性，来读取结果
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverIncludedServicesForService:(CBService *)service error:(NSError *)error
{
        if(AntiLost_Debug)
        {
                NSLog(@"didDiscoverIncludedServicesForService --- %@", error);
        }
        //TODO: 暂时未处理
}

//  discoverDescriptorsForCharacteristic成功(或失败)，此方法回调。
//  此时可以通过设备的service下的characteristic的descriptors属性，来读取结果
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverDescriptorsForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
        if(AntiLost_Debug)
        {
                NSLog(@"didDiscoverDescriptorsForCharacteristic --- %@", error);
        }
        //TODO: 暂时未处理
}

@end
