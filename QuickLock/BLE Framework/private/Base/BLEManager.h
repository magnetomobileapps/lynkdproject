//
//  BLEManager.h
//
//  ------------------------------------------------------------------
//  BLE主端管理器，对内管理类，所有内部处理都在这里进行
//  包括对BLE设备类、BLE管理类、和对外接口类的操作
//  ------------------------------------------------------------------


#import <Foundation/Foundation.h>
#import "BLEPublic.h"

//  Protocol_BLEDeviceCall接口operate函数的callType类型定义
//  通过此函数回调BLEdevice，返回各种硬件操作结果
typedef NS_ENUM(int, eCalltype)
{
        eCallType_getDeviceID = 0,          //-(int)getDeviceID;
        eCalltype_setDeviceID = 1,          //-(void)setDeviceID:(int)value;
        eCallType_getDeviceUUID = 2,        //-(NSString*)getDeviceUUID;
        eCalltype_getPeripheralSuccess = 3, //-(void)getPeripheralSuccess:(NSString*)UUID;
        eCallType_connectResult = 4,        //-(void)connectResult:(BOOL)bResult;
        eCalltype_connectOK = 5,            //-(void)connectOK;
        eCallType_UpdateValueForCharac = 6, //-(void)didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic;
        eCallType_didDisConnect = 7,        //-(void)disconnect;
        eCallType_didUpdateRSSI = 8,        //-(void)didUpdateRSSI:(float)rssiValue;
        eCalltype_setDeviceUUID = 9,        //-(void)setDeviceUUID:(NSString*)UUID;
        eCallType_getScanList = 10          //-(void)getScanList
};

@interface BLEManager : NSObject<CBCentralManagerDelegate, CBPeripheralDelegate>

@property (nonatomic, retain) LockDevice *lockDevice;


//  ------------------------------------------------------------------
//  Examples cited functions and initialization interface class functionsc
//  ------------------------------------------------------------------
+(id)sharedInstance;

-(void)addBLEDevice:(id)bleDevice;

-(BOOL)changeBLEDevice:(int)destDeviceID sourceDeviceID:(int)sourceDeviceID;

//  ------------------------------------------------------------------
//  Bluetooth connection function
//  ------------------------------------------------------------------
-(BOOL)connectDevice:(int)bleDeviceID serviceUUID:(NSString*)serviceUUID localName:(NSString*)localName;

-(BOOL)connectDevice:(int)bleDeviceID peripheralUUID:(NSString*)UUID;

-(BOOL)initConnectWithNameOfRanges:(int)bleDeviceID serviceUUID:(NSString*)serviceUUID localNames:(NSArray*)localNames isRanges:(NSArray*)isRanges;

-(BOOL)initConnectWithScanIndex:(int)bleDeviceID scanIndex:(int)scanIndex;

-(BOOL)startConnect:(int)bleDeviceID isFirstPeripheral:(BOOL)isFirstPeripheral;  // 2013-08-30

-(BOOL)startDiscover:(int)bleDeviceID;

-(BOOL)disconnect:(int)bleDeviceID;

-(BOOL)getIsConnect:(int)bleDeviceID;

-(BOOL)checkExistServiceUUID:(int)bleDeviceID serviceUUID:(NSString*)serviceUUID characteristicUUID:(NSString*)characteristicUUID;

-(BOOL)getScanDeviceList:(int)bleDeviceID scanTime:(float)scanTime;

-(BOOL)getMACAddress:(int)bleDeviceID;

// ISW HONESTSKY 2014-12-19 TEST 应对连接闪退
-(void)stopAllScan;

// ISW HONESTSKY
-(BOOL)getBLEState:(int)bleDeviceID;

//  ------------------------------------------------------------------
//  硬件值读写函数
//  ------------------------------------------------------------------
-(BOOL)readRSSI:(int)bleDeviceID;

-(void)notification:(int)bleDeviceID serviceUUID:(int)serviceUUID characteristicUUID:(int)characteristicUUID on:(BOOL)on;

-(void)readValueForCharacteristic:(int)bleDeviceID serviceUUID:(int)serviceUUID characteristicUUID:(int)characteristicUUID;

-(void)writeValueForCharacteristic:(int)bleDeviceID serviceUUID:(int)serviceUUID characteristicUUID:(int)characteristicUUID data:(NSData *)data;

-(void)readValueForDescriptor:(int)bleDeviceID serviceUUID:(int)serviceUUID characteristicUUID:(int)characteristicUUID descriptorUUID:(int)descriptorUUID;

-(void)writeValueForDescriptor:(int)bleDeviceID serviceUUID:(int)serviceUUID characteristicUUID:(int)characteristicUUID descriptorUUID:(int)descriptorUUID data:(NSData *)data;

@end
