//
//  BLEDevice_BP136A.m
//
//  ------------------------------------------------------------------
//  康贝血压计子类的实现部分
//  ------------------------------------------------------------------
//
//  ------------------------------------------------------------------
//  康贝血压计子类的常量定义
//  ------------------------------------------------------------------

#ifndef BP136A_BC1_ItonBLEDefines_h
#define BP136A_BC1_ItonBLEDefines_h

#define BC1_KEYFOB_CUSTOMER_SERVICE_UUID            0xFF00
#define BC1_KEYFOB_CUSTOMER_SEND_PROPERTY_UUID      0xFF01
#define BC1_KEYFOB_CUSTOMER_RECV_PROPERTY_UUID      0xFF02
#define BC1_KEYFOB_CUSTOMER_RECV_LENGTH             15

#endif

#import "BLEDevice_BP136A.h"
#import "BLEManager.h"

@implementation BLEDevice_BP136A

//  ------------------------------------------------------------------
//  协议文档定义
//  ------------------------------------------------------------------
/*血压计UART  协议GET 获取命令*/
typedef enum
{
        GC_GET_DEVICE_ID,               /*获取设备ID类码*/
        GC_GET_DEVICE_STATUS,           /*获取设备状态*/
        GC_GET_CURRENT_VOLTAGE,         /*获取系统电量*/
        GC_GET_SYSTEM_TIME              /*获取系统日期时间*/
}
GET_COMMAND;

/*收到手机APP 命令类*/
typedef enum
{
	CLASS_SET,                      /*SET 设置类命令标识码*/
	CLASS_GET,                      /*GET 获取类命令标识码*/
        CLASS_RESPONSE
}
COMMAND_CLASS;

/*血压计UART  协议SEND到APP命令*/
typedef enum
{
	SC_SEND_TEST,                   /*设备测试*/
        SC_SEND_DEVICE_ID,              /*发送设备ID类码*/
	SC_SEND_ERROR,                  /*发送错误码*/
	SC_SEND_MEASUREMENT,            /*发送测量数据,包含压力,电池电量*/
	SC_SEND_RESULT,                 /*发送测量结果,高压,低压,脉博,AFIB*/
	SC_SEND_VOLTAGE,                /*发送电池电量*/
	SC_SEND_DEVICE_STATUS,          /*发送设备模式状态*/
	SC_SEND_RESPONSE,               /*发送应答信号*/
	SC_SEND_SYSTEM_TIME             /*发送系统日期时间*/
}
SEND_COMMAND;

/*血压计UART  协议设备ID*/
typedef enum
{
	DI_BLOOD_PRESSURE_METER,        /*血压计*/
	DI_BLOOD_GLUCOSE_METER,         /*血糖仪*/
	DI_NON_FOREHEAD_THERMOMETER,    /*非接触耳温枪*/
	DI_EAR_THERMOMETER,             /*耳温枪*/
	DI_ELECTRONIC_THERMOMETERS,     /*电子体温计*/
	DI_ELECTRONIC_OXIMETER,         /*血氧仪*/
        DI_FAT_METER,                   /*体重计*/
        DI_HAND_CATENARY                /*手链*/
}
DEVICE_ID;

/*血压计UART  协议设备状态*/
typedef enum
{
        DS_READY_STATUS,                /*准备好测量*/
        DS_CALIBRATION_STATUS,          /*校正模式*/
        DS_EXAMINE_STATUS,              /*检验模式*/
        DS_CHARGING_STATUS,             /*充电模式*/
        DS_READYMEMORY_STATUS,          /*读取记忆模式*/
        DS_TIME_SET_STATUS,             /*时间设定模式*/
        DS_MEASUREMENT_STATUS,          /*正在测量...*/
        DS_ERROR_STATUS,                /*错误模式*/
        DS_ZEROTRACING_STATUS           /*归0点*/
}
DEVICE_STATUS;

/*血压计UART  协议 ERROR CODE 错误码*/
typedef enum
{
        EC_CODE_ERR0,                   /*保留*/
 	EC_CODE_ERR1,                   /*ERR1*/
	EC_CODE_ERR2,                   /*ERR2*/
	EC_CODE_ERR3,                   /*ERR3*/
        EC_CODE_ERR4,                   /*保留*/
	EC_CODE_ERR5,                   /*ERR5*/
        EC_CODE_ERR6,                   /*保留*/
        EC_CODE_ERR7,                   /*保留*/
	EC_CODE_ERR8                    /*ERR8*/
}
ERROR_CODE;

/*血压计UART  协议应答状态*/
typedef enum
{
 	RS_FAIL_STATUS,                 /*失败*/
        RS_TRUE_STATUS                  /*成功*/
}
RESPONSE_STATUS;

//  ------------------------------------------------------------------

/*发送命令的枚举（自定义）*/
typedef enum
{
        eCMD_NULL,                       /*无命令*/
        eCMD_SET_START,                  /*开始测量*/
        eCMD_SET_STOP,                   /*停止测量*/
        eCMD_GET_DEVICE_ID,              /*读取设备ID类码*/
        eCMD_GET_DEVICE_STATUS,          /*读取设备状态*/
        eCMD_GET_CURRENT_VOLTAGE,        /*读取系统电量*/
        eCMD_GET_SYSTEM_TIME,            /*读取系统日期时间*/
        eCMD_SET_CHECK,                  /*开始校验*/
        eCMD_SET_ADJUST,                 /*开始校正*/
        eCMD_SET_ADJUST_CONFIRM          /*校正后确认*/
}
APP_COMMAND;

/*命令执行结果的枚举（自定义）*/
typedef enum
{
        eRES_SUCCESS,                   /*命令执行成功*/
        eRES_FAIL,                      /*命令执行失败*/
        eRES_OVERTIME,                  /*命令超时无回应*/
        eRES_BLOCK                      /*命令阻塞，上一条命令还在运行，不能执行本次命令*/
}
APP_COMMAND_RESULT;

//最大命令流水号，在命令发送前，设置当前命令发送流水，在命令发送完成后+1
//可以用来判断当前是否能进行命令发送（命令发送完成后，当前命令流水清零，因此两者相等则不能发送，不等则表示可以发送）
int maxCmdNumber = 0;

//命令超时最大毫秒数
float overTimeMS = 0.384f; //Under the agreement, the longest single command timeout 128ms, retransmission timeout is 3 times the maximum 128 * 3 = 384ms

//因为需要判断超时和重发，因此需要记录当前已经发送的命令的现场数据：流水号、命令类型枚举值、命令数据，命令发送结束后清零
int curCmdNumber;
APP_COMMAND curAppCommand;
NSData *curCmdData;
bool curIsRepeatSend; //当前命令是否正在应硬件要求重发的标志，如果此标志为true，则中途不判断超时定时器，直到为false才开始判断超时
int curRepeatNumber;  //当前超时重发次数

@synthesize delegateBLEDeviceResult_BP136A;

-(id)init:(id)delegete
{
        [self initCmdRecord];
        
        [self setDelegateBLEDeviceResult_BP136A:delegete];
        return [super init:delegete];
}

//  ------------------------------------------------------------------
//  内部私有函数部分
//  ------------------------------------------------------------------

- (void)initCmdRecord
{
        maxCmdNumber = maxCmdNumber + 1;
        curCmdNumber = 0;
        curAppCommand = eCMD_NULL;
        curCmdData = NULL;
        curIsRepeatSend = FALSE;
        curRepeatNumber = 0;
}

//  Hardware data receiver function - Overload concrete implementation part, different for each subclass
- (void)didUpdateValueForCharacteristic_override:(CBCharacteristic *)characteristic
{
        UInt16 characteristicUUID = [BLEPublic CBUUIDToInt:characteristic.UUID];
        switch(characteristicUUID)
        {
                        //  数据接收
                case BC1_KEYFOB_CUSTOMER_RECV_PROPERTY_UUID:
                {
                        Byte bAddress[BC1_KEYFOB_CUSTOMER_RECV_LENGTH];
                        [characteristic.value getBytes:&bAddress length:BC1_KEYFOB_CUSTOMER_RECV_LENGTH];
                        
                        [self processReceiveData:bAddress recvTime:[NSDate date]];
                        break;
                }
                default:
                {
                        if(AntiLost_Debug)
                        {
                                NSLog(@"didUpdateValueForCharacteristic --- default --- characteristicUUID:%d" , characteristicUUID);
                        }
                        break;
                }
        }
}

- (void)processReceiveData:(Byte*)bytes recvTime:(NSDate*)recvTime
{
        int i;
        Byte bAddress[BC1_KEYFOB_CUSTOMER_RECV_LENGTH];
        NSString *str = @"";
        
        //  取出字节
        for (i = 0 ; i < BC1_KEYFOB_CUSTOMER_RECV_LENGTH; i++)
        {
                bAddress[i] = bytes[i];
        }
        
        //  校验CRC
        if (![self checkCRC:bAddress])
        {
                str = @"CRC校验错！";
        }
        else
        {
                //  校验成功，解析数据
                [self parseData:bAddress];
        }
        
        //  测试输出数据
        for (i = 0 ; i < BC1_KEYFOB_CUSTOMER_RECV_LENGTH; i++)
        {
                str = [NSString stringWithFormat:@"%@, %d", str, bAddress[i]];
        }
        [delegateBLEDeviceResult_BP136A BR_recevieTransparentDataTest:str revcTime:recvTime DeviceID:DeviceID];
}

- (BOOL)checkCRC:(Byte*)bAddress
{
        Byte length, crc;
        length = bAddress[1] + 3; //数据包的总长度是数据长度+3 (command,packetLength,[data],crc)
        crc = [self getCheckCode_2:bAddress len:length - 1]; //计算crc时，必须将自身排除在外
        if (crc != bAddress[length - 1])
        {
                return false;
        }
        else
        {
                return true;
        }
}

//  前面字节的累加和的低八位
- (Byte)getCheckCode_1:(char*)bytes len:(int)len
{
        int i, n;
        Byte b;
        
        n = 0;
        for (i = 0; i < len; i++)
        {
                n = n + (Byte)bytes[i];
        }
        b = n & 0x000000FF;
        
        return b;
}

- (Byte)getCheckCode_2:(Byte*)bytes len:(int)len
{
        int i, n;
        Byte b;
        
        n = 0;
        for (i = 0; i < len; i++)
        {
                n = n + bytes[i];
        }
        b = n & 0x000000FF;
        
        return b;
}

- (void)parseData:(Byte*)bAddress
{
        Byte cmd = bAddress[0];
        switch (cmd)
        {
                case (Byte)SC_SEND_TEST:        //设备测试          //2014-05-23 邮件确认：应答通过SC_SEND_TEST执行！
                        [self receiveResponse:bAddress];
                        break;
                case (Byte)SC_SEND_DEVICE_ID:   //设备ID类码
                        [self parseData_DEVICE_ID:bAddress];
                        [self responseResult:TRUE];
                        break;
                case (Byte)SC_SEND_ERROR:       //发送错误码
                        [self parseData_ERROR:bAddress];
                        break;
                case (Byte)SC_SEND_MEASUREMENT: //测量过程数据
                        [self parseData_MEASUREMENT:bAddress];
                        break;
                case (Byte)SC_SEND_RESULT:      //测量结果数据
                        [self parseData_RESULT:bAddress];
                        break;
                case (Byte)SC_SEND_VOLTAGE:     //电量数据
                        [self parseData_VOLTAGE:bAddress];
                        [self responseResult:TRUE];
                        break;
                case (Byte)SC_SEND_DEVICE_STATUS://设备模式状态
                        [self parseData_DEVICE_STATUS:bAddress];
                        [self responseResult:TRUE];
                        break;
                case (Byte)SC_SEND_RESPONSE:    //发送应答信号        //2014-05-23 邮件确认：应答通过SC_SEND_TEST执行，此处无效！
                        break;
                case (Byte)SC_SEND_SYSTEM_TIME: //系统时间数据
                        [self parseData_SYSTEM_TIME:bAddress];
                        [self responseResult:TRUE];
                        break;
                default:
                        break;
        }
}

//  解析测量过程数据
- (void)parseData_MEASUREMENT:(Byte*)bAddress
{
        int _pressure, _battery, _heartBeat;
        Byte b1, b2;
        
        _heartBeat = bAddress[4];
        _battery = bAddress[3] & 0x0000007F;
        if ((bAddress[3] & 0x00000080) == 0x00000080)
        {
                b1 = 0x01;
        }
        else
        {
                b1 = 0x00;
        }
        b2 = bAddress[2];
        _pressure = b1;
        _pressure = (_pressure << 8) | b2;
        
        [delegateBLEDeviceResult_BP136A BR_updateMesuringData:_battery heartBeat:_heartBeat bloodPressure:_pressure DeviceID:DeviceID];
        
        //    NSString *str;
        //    str = [NSString stringWithFormat:@"测量--电池:%d,心跳:%d,血压:%d",_battery,_heartBeat,_pressure];
        //    [delegateBLEDeviceResult_BP136A BR_recevieTransparentDataTest:str revcTime:[NSDate date]];
}

//  解析测量结果数据
- (void)parseData_RESULT:(Byte*)bAddress
{
        int systolicPressure = 0;
        int diastolicPressure = 0;
        int heartRate = 0;
        int AFIB_IHB = 0;
        
        systolicPressure = bAddress[2];
        diastolicPressure = bAddress[3];
        heartRate = bAddress[4];
        AFIB_IHB = bAddress[5];
        
        [delegateBLEDeviceResult_BP136A BR_updateMesureResult:[NSDate date]
                                             systolicPressure:systolicPressure
                                            diastolicPressure:diastolicPressure
                                                    heartRate:heartRate
                                                     AFIB_IHB:AFIB_IHB
                                                     DeviceID:DeviceID];
        
        //    NSString *str;
        //    str = [NSString stringWithFormat:@"结果--高压:%d,低压:%d,心率:%d,心律不齐:%d", systolicPressure, diastolicPressure, heartRate, AFIB_IHB];
        //    [delegateBLEDeviceResult_BP136A BR_recevieTransparentDataTest:str revcTime:[NSDate date]];
}

//  解析电量数据
- (void)parseData_VOLTAGE:(Byte*)bAddress
{
        int battery;
        
        battery = bAddress[2];
        
        [delegateBLEDeviceResult_BP136A BR_recvHardInfo_CurrentVoltage:battery DeviceID:DeviceID];
        
        //    NSString *str;
        //    str = [NSString stringWithFormat:@"电量--%d%%", battery];
        //    [delegateBLEDeviceResult_BP136A BR_recevieTransparentDataTest:str revcTime:[NSDate date]];
}

//  解析系统时间数据
- (void)parseData_SYSTEM_TIME:(Byte*)bAddress
{
        Byte year, month, day, hour, minute, second;
        NSString *sDate;
        NSDate *d;
        NSDateFormatter *fm;
        
        year = bAddress[2];
        month = bAddress[3];
        day = bAddress[4];
        hour = bAddress[5];
        minute = bAddress[6];
        second = bAddress[7];
        sDate = [NSString stringWithFormat:@"20%.2d-%.2d-%.2d %.2d:%.2d:%.2d", year, month, day, hour, minute, second];
        
        fm = [[NSDateFormatter alloc]init];
        [fm setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
        d = [fm dateFromString:sDate];
        
        //    if (isWaitingResultTime) //如果等待测量时间上传标志打开，则上传测量结果数据，否则单独上传系统时间
        //    {
        //        [delegateBLEDeviceResult_BP136A BR_updateMesureResult:d systolicPressure:systolicPressure diastolicPressure:diastolicPressure heartRate:heartRate AFIB_IHB:AFIB_IHB];
        //        isWaitingResultTime = false;
        //
        //        NSString *str;
        //        str = [NSString stringWithFormat:@"结果时间--%@",sDate];
        //        [delegateBLEDeviceResult_BP136A BR_recevieTransparentDataTest:str revcTime:[NSDate date]];
        //    }
        //    else
        //    {
        [delegateBLEDeviceResult_BP136A BR_recvHardInfo_SystemTime:d DeviceID:DeviceID];
        //
        //        NSString *str;
        //        str = [NSString stringWithFormat:@"系统时间--%@",sDate];
        //        [delegateBLEDeviceResult_BP136A BR_recevieTransparentDataTest:str revcTime:[NSDate date]];
        //    }
}

//  解析错误码
-(void)parseData_ERROR:(Byte*)bAddress
{
        Byte _err;
        NSString *_sErr, *_sErrInfo;
        
        _err = bAddress[2];
        
        switch (_err)
        {
                case (Byte)EC_CODE_ERR0:
                        _sErr = @"Err0";
                        _sErrInfo = @"未知错误(0)";
                        break;
                case (Byte)EC_CODE_ERR1:
                        _sErr = @"Err1";
                        _sErrInfo = @"无法侦测到脉搏,脉搏讯号太小或压力骤降";
                        break;
                case (Byte)EC_CODE_ERR2:
                        _sErr = @"Err2";
                        _sErrInfo = @"测量过程中有移动或其他杂讯干扰";
                        break;
                case (Byte)EC_CODE_ERR3:
                        _sErr = @"Err3";
                        _sErrInfo = @"充气时间太长";
                        break;
                case (Byte)EC_CODE_ERR4:
                        _sErr = @"Err4";
                        _sErrInfo = @"未知错误(4)";
                        break;
                case (Byte)EC_CODE_ERR5:
                        _sErr = @"Err5";
                        _sErrInfo = @"测出收缩压（高压）或舒张压（低压）差值过大或过小";
                        break;
                case (Byte)EC_CODE_ERR6:
                        _sErr = @"Err6";
                        _sErrInfo = @"未知错误(6)";
                        break;
                case (Byte)EC_CODE_ERR7:
                        _sErr = @"Err7";
                        _sErrInfo = @"未知错误(7)";
                        break;
                case (Byte)EC_CODE_ERR8:
                        _sErr = @"Err8";
                        _sErrInfo = @"绑带内压力过高";
                        break;
                default:
                        _sErr = @"Err";
                        _sErrInfo = @"未知错误";
                        break;
        }
        
        [delegateBLEDeviceResult_BP136A BR_updateMesureError:_sErr sErrInfo:_sErrInfo DeviceID:DeviceID];
        
        //    NSString *str;
        //    str = [NSString stringWithFormat:@"测量出错--代码：%@；说明：%@", _sErr, _sErrInfo];
        //    [delegateBLEDeviceResult_BP136A BR_recevieTransparentDataTest:str revcTime:[NSDate date]];
}

//  解析设备ID类码
- (void)parseData_DEVICE_ID:(Byte*)bAddress
{
        Byte _deviceID;
        NSString *sDeviceKind;
        
        _deviceID = bAddress[2];
        
        switch (_deviceID)
        {
                case (Byte)DI_BLOOD_PRESSURE_METER:
                        sDeviceKind = @"血压计";
                        break;
                case (Byte)DI_BLOOD_GLUCOSE_METER:
                        sDeviceKind = @"血糖仪";
                        break;
                case (Byte)DI_NON_FOREHEAD_THERMOMETER:
                        sDeviceKind = @"非接触耳温枪";
                        break;
                case (Byte)DI_EAR_THERMOMETER:
                        sDeviceKind = @"耳温枪";
                        break;
                case (Byte)DI_ELECTRONIC_THERMOMETERS:
                        sDeviceKind = @"电子体温计";
                        break;
                case (Byte)DI_ELECTRONIC_OXIMETER:
                        sDeviceKind = @"血氧仪";
                        break;
                case (Byte)DI_FAT_METER:
                        sDeviceKind = @"体重计";
                        break;
                case (Byte)DI_HAND_CATENARY:
                        sDeviceKind = @"手链";
                        break;
                default:
                        sDeviceKind = @"--";
                        break;
        }
        sDeviceKind = [NSString stringWithFormat:@"设备类别码:%d, 类别名称:%@", _deviceID, sDeviceKind];
        
        [delegateBLEDeviceResult_BP136A BR_recvHardInfo_DeviceID:sDeviceKind DeviceID:DeviceID];
        
        //    NSString *str;
        //    str = [NSString stringWithFormat:@"设备类别--%@",sDeviceKind];
        //    [delegateBLEDeviceResult_BP136A BR_recevieTransparentDataTest:str revcTime:[NSDate date]];
}

//  解析设备模式状态
- (void)parseData_DEVICE_STATUS:(Byte*)bAddress
{
        Byte _status;
        NSString *sDeviceStatus;
        
        _status = bAddress[2];
        
        switch (_status)
        {
                case (Byte)DS_READY_STATUS:
                        sDeviceStatus = @"准备好测量";
                        break;
                case (Byte)DS_CALIBRATION_STATUS:
                        sDeviceStatus = @"校正模式";
                        break;
                case (Byte)DS_EXAMINE_STATUS:
                        sDeviceStatus = @"检验模式";
                        break;
                case (Byte)DS_CHARGING_STATUS:
                        sDeviceStatus = @"充电模式";
                        break;
                case (Byte)DS_READYMEMORY_STATUS:
                        sDeviceStatus = @"读取记忆模式";
                        break;
                case (Byte)DS_TIME_SET_STATUS:
                        sDeviceStatus = @"时间设定模式";
                        break;
                case (Byte)DS_MEASUREMENT_STATUS:
                        sDeviceStatus = @"正在测量...";
                        break;
                case (Byte)DS_ERROR_STATUS:
                        sDeviceStatus = @"错误模式";
                        break;
                case (Byte)DS_ZEROTRACING_STATUS:
                        sDeviceStatus = @"归0点";
                        break;
                default:
                        sDeviceStatus = @"--";
                        break;
        }
        sDeviceStatus = [NSString stringWithFormat:@"设备状态:%d, 状态名称:%@", _status, sDeviceStatus];
        
        [delegateBLEDeviceResult_BP136A BR_recvHardInfo_DeviceStatus:sDeviceStatus DeviceID:DeviceID];
        
        //    NSString *str;
        //    str = [NSString stringWithFormat:@"设备状态--%@",sDeviceStatus];
        //    [delegateBLEDeviceResult_BP136A BR_recevieTransparentDataTest:str revcTime:[NSDate date]];
}

-(void)getHardInfo:(GET_COMMAND)gc appCommand:(APP_COMMAND)appCommand
{
        COMMAND_CLASS cc = CLASS_GET;
        char bytes[BC1_KEYFOB_CUSTOMER_RECV_LENGTH] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
        
        bytes[0] = cc;
        bytes[1] = 1;
        bytes[2] = gc;
        bytes[3] = [self getCheckCode_1:bytes len:3];
        NSData *d = [[NSData alloc] initWithBytes:bytes length:BC1_KEYFOB_CUSTOMER_RECV_LENGTH];
        
        [self sendCommand:appCommand data:d];
}

//  发送命令到硬件。发送后，保存现场数据，开启超时定时器300ms，等待回应，在此期间不接受其他硬件命令
-(void)sendCommand:(APP_COMMAND)appCommand data:(NSData *)data
{
        if ((maxCmdNumber != curCmdNumber) || (curIsRepeatSend))
        {
                [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID
                                                             serviceUUID:BC1_KEYFOB_CUSTOMER_SERVICE_UUID
                                                      characteristicUUID:BC1_KEYFOB_CUSTOMER_SEND_PROPERTY_UUID
                                                                    data:data];
                
                //如果并非重发命令，则保持现场数据，并开启超时定时器（重发命令不重复保存和开启超时定时器）
                if (!curIsRepeatSend)
                {
                        //保存现场数据
                        curCmdNumber = maxCmdNumber;
                        curAppCommand = appCommand;
                        curCmdData = data;
                        curIsRepeatSend = FALSE;
                        curRepeatNumber = 0;
                        
                        //开启超时定时器，并将当前命令流水号传入，作为超时判断的依据
                        NSNumber *_cmdNumber = [[NSNumber alloc] initWithInt:curCmdNumber];
                        [NSTimer scheduledTimerWithTimeInterval:overTimeMS
                                                         target:self
                                                       selector:@selector(overTimerProcess:)
                                                       userInfo:_cmdNumber
                                                        repeats:NO];
                }
        }
        else    //上一条命令还在处理中，不接收下一条命令
        {
                [self sendCommnadResult:appCommand cmdResult:eRES_BLOCK];
        }
}

//  命令执行结果的回应，回应的来源可能是超时定时器（命令超时），或者是硬件（执行成功或者失败），或者是本地（命令被堵塞不能执行）
-(void)sendCommnadResult:(APP_COMMAND)appCommnad cmdResult:(APP_COMMAND_RESULT)cmdResult
{
        switch (appCommnad)
        {
                case eCMD_SET_START:
                        [delegateBLEDeviceResult_BP136A BR_result_StartTransparent:cmdResult DeviceID:DeviceID];
                        break;
                case eCMD_SET_STOP:
                        [delegateBLEDeviceResult_BP136A BR_result_stopTransparent:cmdResult DeviceID:DeviceID];
                        break;
                case eCMD_GET_CURRENT_VOLTAGE:
                        [delegateBLEDeviceResult_BP136A BR_result_getHardInfo_CurrentVoltage:cmdResult DeviceID:DeviceID];
                        break;
                case eCMD_GET_DEVICE_ID:
                        [delegateBLEDeviceResult_BP136A BR_result_getHardInfo_DeviceID:cmdResult DeviceID:DeviceID];
                        break;
                case eCMD_GET_DEVICE_STATUS:
                        [delegateBLEDeviceResult_BP136A BR_result_getHardInfo_DeviceStatus:cmdResult DeviceID:DeviceID];
                        break;
                case eCMD_GET_SYSTEM_TIME:
                        [delegateBLEDeviceResult_BP136A BR_result_getHardInfo_SystemTime:cmdResult DeviceID:DeviceID];
                        break;
                case eCMD_SET_CHECK:  
//                        [delegateBLEDeviceResult_BP136A BR_result_sendCheckCommand:cmdResult DeviceID:DeviceID];
                        [delegateBLEDeviceResult_BP136A BR_result_sendCheckCommand:eRES_SUCCESS DeviceID:DeviceID];
                        break;
                case eCMD_SET_ADJUST:
//                        [delegateBLEDeviceResult_BP136A BR_result_sendAdjustCommand:cmdResult DeviceID:DeviceID];
                        [delegateBLEDeviceResult_BP136A BR_result_sendAdjustCommand:eRES_SUCCESS DeviceID:DeviceID];
                        break;
                case eCMD_SET_ADJUST_CONFIRM:
//                        [delegateBLEDeviceResult_BP136A BR_result_sendConfirmCommand:cmdResult DeviceID:DeviceID];
                        [delegateBLEDeviceResult_BP136A BR_result_sendConfirmCommand:eRES_SUCCESS DeviceID:DeviceID];
                        break;
                default:
                        break;
        }
}

//  命令超时处理：通知界面命令超时，并且清零之前的现场命令数据（不管是否重发，按最大超时时间计算）
-(void)overTimerProcess:(NSTimer *)timer
{
        int _cmdNumber;
        
        _cmdNumber = [((NSNumber *)timer.userInfo) intValue];
        
        if ((curCmdNumber > 0) && (curCmdNumber == _cmdNumber)) //如果curCmdNumber为0，表示当前命令已经完成，数据已经清除
        {
                [self sendCommnadResult:curAppCommand cmdResult:eRES_OVERTIME];
                [self initCmdRecord];
        }
}

//  命令应答处理：可能成功、失败或者要求重发
-(void)receiveResponse:(Byte*)bAddress
{
        Byte bRESPONSES_TATUS = bAddress[2];
        
        switch (bRESPONSES_TATUS)
        {
                case RS_TRUE_STATUS:    //命令接收成功(ACK)
                        [self responseResult:TRUE];
                        break;
                case RS_FAIL_STATUS:    //命令接收错误(NACK，要求app重发，最多三次)
                        if (curRepeatNumber < 3)  //要求重发
                        {
                                [self repeatSendCurCommand];
                        }
                        else
                        {
                                [self responseResult:FALSE]; //三次重发都错误，判定命令执行失败
                        }
                        break;
                default:
                        break;
        }
}

//  命令重发
-(void)repeatSendCurCommand
{
        curIsRepeatSend = true;
        curRepeatNumber = curRepeatNumber + 1;
        
        [self sendCommand:curAppCommand data:curCmdData];
}

//  命令发送成功或者失败的操作
-(void)responseResult:(BOOL)bSuccess
{
        if (bSuccess)
        {
                [self sendCommnadResult:curAppCommand cmdResult:eRES_SUCCESS];
        }
        else
        {
                [self sendCommnadResult:curAppCommand cmdResult:eRES_FAIL];
        }
        [self initCmdRecord];
}

//  ------------------------------------------------------------------
//  协议Protocol_BLEDevice_BP136A的实现部分
//  ------------------------------------------------------------------

-(void)BD_enableTransparentData:(BOOL)isEnable
{
        [[BLEManager sharedInstance] notification:DeviceID serviceUUID:BC1_KEYFOB_CUSTOMER_SERVICE_UUID characteristicUUID:BC1_KEYFOB_CUSTOMER_RECV_PROPERTY_UUID on:isEnable];
}

-(void)BD_startTransparent
{
        char bytes[BC1_KEYFOB_CUSTOMER_RECV_LENGTH] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
        bytes[0] = 0x00;  //根据协议文档，按键 开始（停止）: 0x00 0x02 0x03 0x40 0x45
        bytes[1] = 0x02;
        bytes[2] = 0x03;
        bytes[3] = 0x40;
        bytes[4] = [self getCheckCode_1:bytes len:4];
        NSData *d = [[NSData alloc] initWithBytes:bytes length:BC1_KEYFOB_CUSTOMER_RECV_LENGTH];
        
        [self sendCommand:eCMD_SET_START data:d];
}

-(void)BD_stopTransparent
{
        char bytes[BC1_KEYFOB_CUSTOMER_RECV_LENGTH] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
        bytes[0] = 0x00;  //根据协议文档，按键 开始（停止）: 0x00 0x02 0x03 0x44 0x49
        bytes[1] = 0x02;
        bytes[2] = 0x03;
        bytes[3] = 0x44;
        bytes[4] = [self getCheckCode_1:bytes len:4];
        NSData *d = [[NSData alloc] initWithBytes:bytes length:BC1_KEYFOB_CUSTOMER_RECV_LENGTH];
        
        [self sendCommand:eCMD_SET_STOP data:d];
}

-(void)BD_getHardInfo_DeviceID
{
        [self getHardInfo: GC_GET_DEVICE_ID appCommand:eCMD_GET_DEVICE_ID];
}

-(void)BD_getHardInfo_DeviceStatus
{
        [self getHardInfo: GC_GET_DEVICE_STATUS appCommand:eCMD_GET_DEVICE_STATUS];
}

-(void)BD_getHardInfo_CurrentVoltage
{
        [self getHardInfo: GC_GET_CURRENT_VOLTAGE appCommand:eCMD_GET_CURRENT_VOLTAGE];
}

-(void)BD_getHardInfo_SystemTime
{
        [self getHardInfo: GC_GET_SYSTEM_TIME appCommand:eCMD_GET_SYSTEM_TIME];
}

-(void)BD_sendCheckCommand
{
        char bytes[BC1_KEYFOB_CUSTOMER_RECV_LENGTH] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
        bytes[0] = 0x00;  //根据协议文档，校验模式: 0x00 0x01 0x02 0x03
        bytes[1] = 0x01;
        bytes[2] = 0x02;
        bytes[3] = [self getCheckCode_1:bytes len:3];
        NSData *d = [[NSData alloc] initWithBytes:bytes length:BC1_KEYFOB_CUSTOMER_RECV_LENGTH];
        
        [self sendCommand:eCMD_SET_CHECK data:d];
}

-(void)BD_sendAdjustCommand
{
        char bytes[BC1_KEYFOB_CUSTOMER_RECV_LENGTH] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
        bytes[0] = 0x00;  //根据协议文档，校验模式: 0x00 0x01 0x01 0x02
        bytes[1] = 0x01;
        bytes[2] = 0x01;
        bytes[3] = [self getCheckCode_1:bytes len:3];
        NSData *d = [[NSData alloc] initWithBytes:bytes length:BC1_KEYFOB_CUSTOMER_RECV_LENGTH];
        
        [self sendCommand:eCMD_SET_ADJUST data:d];
}

-(void)BD_sendConfirmCommand
{
        char bytes[BC1_KEYFOB_CUSTOMER_RECV_LENGTH] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
        bytes[0] = 0x00;  //根据协议文档，校验模式: 0x00 0x02 0x03 0x10 0x15
        bytes[1] = 0x02;
        bytes[2] = 0x03;
        bytes[3] = 0x10;
        bytes[4] = [self getCheckCode_1:bytes len:4];
        NSData *d = [[NSData alloc] initWithBytes:bytes length:BC1_KEYFOB_CUSTOMER_RECV_LENGTH];
        
        [self sendCommand:eCMD_SET_ADJUST_CONFIRM data:d];
}

@end
