//
//  BLEDevice_Pedo_ZYWK.m
//
//  ------------------------------------------------------------------
//  计步器子类实现部分 - 博通带屏（不带屏）计步器
//  ------------------------------------------------------------------

//  ------------------------------------------------------------------
//  计步器子类的常量定义
//  ------------------------------------------------------------------

#ifndef PEDO_BC1_ItonBLEDefines_h
#define PEDO_BC1_ItonBLEDefines_h

#define BC1_KEYFOB_PEDOMETER_SERVICE_UUID                                   0x1814
#define BC1_KEYFOB_PEDOMETER_ACCELENABLE_PROPERTY_UUID                      0xFFA1
#define BC1_KEYFOB_PEDOMETER_COUNTENABLE_PROPERTY_UUID                      0xFFA6
#define BC1_KEYFOB_PEDOMETER_COUNTENABLE_PROPERTY_DESCRIPTOR_UUID           0x2902
//  2013-12-11 由8改为16，上传累计步数，时间，卡路里，距离
#define BC1_KEYFOB_PEDOMETER_COUNTE_LENGTH                                  16        // 上传累计步数和累计时间，由4 改为8

#define BC1_KEYFOB_TIME_SERVICE_UUID                                        0x1805
#define BC1_KEYFOB_TIME_SYNCHRONOUS_PROPERTY_UUID                           0x2A2B
#define BC1_KEYFOB_TIME_LENGTH                                              4

//  2013-08-05 by wangyi
#define BC1_KEYFOB_CUSTOMER_SERVICE_UUID                                    0xFFD0
#define BC1_KEYFOB_CUSTOMER_PARAMETER_PROPERTY_UUID                         0xFFD1
#define BC1_KEYFOB_CUSTOMER_PARAMETER_LENGTH                                9
#define BC1_KEYFOB_CUSTOMER_CAMERA_PROPERTY_UUID                            0xFFD2
#define BC1_KEYFOB_CUSTOMER_CAMERA_READ_LEN                                 1
#define BC1_KEYFOB_CUSTOMER_COMINGCALL_PROPERTY_UUID                        0xFFD3
//

#define BC1_KEYFOB_OFFLINE_SERVICE_UUID                                     0xFFC0
#define BC1_KEYFOB_OFFLINE_SELECTDAY_PROPERTY_UUID                          0xFFC1
#define BC1_KEYFOB_OFFLINE_DATA_PROPERTY_UUID                               0xFFC2
#define BC1_KEYFOB_OFFLINE_DATA_LENGTH                                      20 //12 修改为20，增加 卡路里 和 距离

//  2013-10-28 撤销此组UUID：新固件不再需要sleep，断开后直接进入休眠
//#define BC1_KEYFOB_SLEEP_SERVICE_UUID                                       0xFFD0
//#define BC1_KEYFOB_SLEEP_DATA_PROPERTY_UUID                                 0xFFD1
//#define BC1_KEYFOB_SLEEP_DATA_LENGTH                                        1

//  2013-10-28 by lincheng
#define BC1_KEYFOB_BATT_SERVICE_UUID                                        0x180F
#define BC1_KEYFOB_BATT_STATE_PROPERTY_UUID                                 0x2A19
#define BC1_KEYFOB_BATT_STATE_LENGTH                                        1
#define BC1_KEYFOB_BATT_VERSION_PROPERTY_UUID                               0xFFF0
#define BC1_KEYFOB_BATT_VERSION_LENGTH                                      10

#endif

#import "BLEDevice_Pedo_ZYWK.h"
#import "BLEManager.h"

int nOffline;


@implementation BLEDevice_Pedo_ZYWK

@synthesize delegateBLEDeviceResult_Pedo_ZYWK;

-(id)init:(id)delegete
{
    [self setDelegateBLEDeviceResult_Pedo_ZYWK:delegete];
    return [super init:delegete];
}


//  ------------------------------------------------------------------
//  内部私有函数部分
//  ------------------------------------------------------------------

//  硬件数据接收函数 - 重载具体实现部分，每个子类不同
- (void)didUpdateValueForCharacteristic_override:(CBCharacteristic *)characteristic
{
    UInt16 characteristicUUID = [BLEPublic CBUUIDToInt:characteristic.UUID];
    switch(characteristicUUID)
    {
        //  2013-09-16 四个字节的累计步数，四个字节的累计时间
        //  2013-12-11 四个字节的累计步数，四个字节的累计时间，四个字节的累计卡路里，四个字节的累计距离
        case BC1_KEYFOB_PEDOMETER_COUNTENABLE_PROPERTY_UUID:
        {
            int n;
            int m;
            int c;    // 卡路里
            int d;    // 距离
            double cal;
            double dis;
            
            Byte bAddress[BC1_KEYFOB_PEDOMETER_COUNTE_LENGTH];
            [characteristic.value getBytes:&bAddress length:BC1_KEYFOB_PEDOMETER_COUNTE_LENGTH];
            int b1,b2,b3,b4,b5,b6,b7,b8,b9,b10,b11,b12,b13,b14,b15,b16;
            b1 = (int)bAddress[0];
            b2 = (int)bAddress[1];
            b3 = (int)bAddress[2];
            b4 = (int)bAddress[3];
            b5 = (int)bAddress[4];
            b6 = (int)bAddress[5];
            b7 = (int)bAddress[6];
            b8 = (int)bAddress[7];
            b9 = (int)bAddress[8];
            b10 = (int)bAddress[9];
            b11 = (int)bAddress[10];
            b12 = (int)bAddress[11];
            b13 = (int)bAddress[12];
            b14 = (int)bAddress[13];
            b15 = (int)bAddress[14];
            b16 = (int)bAddress[15];
            n = b1 | (b2 << 8) | (b3 << 16) | (b4 << 24);
            m = b5 | (b6 << 8) | (b7 << 16) | (b8 << 24);
            c = b9 | (b10 << 8) | (b11 << 16) | (b12 << 24);
            d = b13 | (b14 << 8) | (b15 << 16) | (b16 << 24);
            cal = (double)c / 10;
            dis = (double)d / 100;
            
            if (AntiLost_Debug)
            {
                NSLog(@"0xffa6 PaceCount-----: %d,%d,%d,%d,%d",b1,b2,b3,b4,n);
                NSLog(@"0xffa6 PaceTime-----: %d,%d,%d,%d,%d",b5,b6,b7,b8,m);
                NSLog(@"0xffa6 calorie-----: %d,%d,%d,%d,%f",b9,b10,b11,b12,cal);
                NSLog(@"0xffa6 distance-----: %d,%d,%d,%d,%f",b13,b14,b15,b16,dis);
            }
            
            [delegateBLEDeviceResult_Pedo_ZYWK BR_pedoCountUpdated_ZYWK:n
                                                                   time:m
                                                                calorie:cal
                                                               distance:dis
                                                               DeviceID:DeviceID];
            break;
        }
        case BC1_KEYFOB_TIME_SYNCHRONOUS_PROPERTY_UUID:
        {
            int n;
            NSDate *da,*date2000;
            NSDateFormatter *fm;
            Byte bAddress[BC1_KEYFOB_TIME_LENGTH];
            
            fm = [[NSDateFormatter alloc]init];
            [fm setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
            date2000 = [fm dateFromString:@"2000-01-01 00:00:00"];
            [characteristic.value getBytes:&bAddress length:BC1_KEYFOB_TIME_LENGTH];
            int b1,b2,b3,b4;
            b1 = (int)bAddress[0];
            b2 = (int)bAddress[1];
            b3 = (int)bAddress[2];
            b4 = (int)bAddress[3];
            n = b1 | (b2 << 8) | (b3 << 16) | (b4 << 24);
            da = [[NSDate alloc]initWithTimeInterval:n sinceDate:date2000];
            
            if (AntiLost_Debug)
            {
                NSLog(@"0x2A2B-----: %d,%d,%d,%d,%d,%@",b1,b2,b3,b4,n,[fm stringFromDate:da]);
            }
            break;
        }
        case BC1_KEYFOB_OFFLINE_DATA_PROPERTY_UUID:
        {
            int n;
            Byte bAddress[BC1_KEYFOB_OFFLINE_DATA_LENGTH];
            NSDate *da,*date2000;
            NSDateFormatter *fm;
            int nTimerecord,nPaceCount;
            // 同时接收 卡路里(0.1Kcal)和距离(0.01Km)数据
            int nCalorie;
            int nDistance;
            double cal;
            double dis;
            
            fm = [[NSDateFormatter alloc]init];
            [fm setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
            date2000 = [fm dateFromString:@"2000-01-01 00:00:00"];
            
            [characteristic.value getBytes:&bAddress length:BC1_KEYFOB_OFFLINE_DATA_LENGTH];
            int b1,b2,b3,b4,b5,b6,b7,b8,b9,b10,b11,b12,b13,b14,b15,b16,b17,b18,b19,b20;
            b1 = (int)bAddress[0];
            b2 = (int)bAddress[1];
            b3 = (int)bAddress[2];
            b4 = (int)bAddress[3];
            b5 = (int)bAddress[4];
            b6 = (int)bAddress[5];
            b7 = (int)bAddress[6];
            b8 = (int)bAddress[7];
            b9 = (int)bAddress[8];
            b10 = (int)bAddress[9];
            b11 = (int)bAddress[10];
            b12 = (int)bAddress[11];
            b13 = (int)bAddress[12];
            b14 = (int)bAddress[13];
            b15 = (int)bAddress[14];
            b16 = (int)bAddress[15];
            b17 = (int)bAddress[16];
            b18 = (int)bAddress[17];
            b19 = (int)bAddress[18];
            b20 = (int)bAddress[19];
            
            n = b1 | (b2 << 8) | (b3 << 16) | (b4 << 24);
            da = [[NSDate alloc]initWithTimeInterval:n sinceDate:date2000];
            nTimerecord = b5 | (b6 << 8) | (b7 << 16) | (b8 << 24);
            nPaceCount = b9 | (b10 << 8) | (b11 << 16) | (b12 << 24);
            nCalorie = b13 | (b14 << 8) | (b15 << 16) | (b16 << 24);
            nDistance = b17 | (b18 << 8) | (b19 << 16) | (b20 << 24);
            cal = (double)nCalorie / 10;
            dis = (double)nDistance / 100;
//            if (AntiLost_Debug)
//            {
                NSLog(@"offlineData - %d, %d, %f, %f", nTimerecord, nPaceCount, cal, dis);
//            }
            [delegateBLEDeviceResult_Pedo_ZYWK BR_pedoReceiveOfflineData_ZYWK:da
                                                                   timerecord:nTimerecord
                                                                    paceCount:nPaceCount
                                                                      calorie:cal
                                                                     distance:dis
                                                                     DeviceID:DeviceID];
            
            if (AntiLost_Debug)
            {
                NSLog(@"0xFFC2-----: offlineDate - %d,%d,%d,%d,[%d] timerecord - %d,%d,%d,%d,[%d] paceCount - %d,%d,%d,%d,[%d]",b1,b2,b3,b4,n,b5,b6,b7,b8,nTimerecord,b9,b10,b11,b12,nPaceCount);
            }
            break;
        }
        //  接收硬件过来的数据
        case BC1_KEYFOB_CUSTOMER_CAMERA_PROPERTY_UUID:
        {
            char keys;
            [characteristic.value getBytes:&keys length:BC1_KEYFOB_CUSTOMER_CAMERA_READ_LEN];
            // 2013-09-06
            if (keys == 0x01)       // 0x01表示打开摄像头
            {
                if (AntiLost_Debug)
                {
                    NSLog(@"openCamera : 0x01");
                }
                [delegateBLEDeviceResult_Pedo_ZYWK BR_openCamera_ZYWK :DeviceID];
            }
            else if (keys == 0x02)  // 0x02表示关闭摄像头
            {
                if (AntiLost_Debug)
                {
                    NSLog(@"closCamera : 0x02");
                }
                [delegateBLEDeviceResult_Pedo_ZYWK BR_closeCamera_ZYWK :DeviceID];
            }
            else                    // 0x03表示拍照
            {
                if (AntiLost_Debug)
                {
                    NSLog(@"snaCamera : 0x03");
                }
                [delegateBLEDeviceResult_Pedo_ZYWK BR_snap_ZYWK :DeviceID];
            }
            break;
        }
        //  版本号读取
        case BC1_KEYFOB_BATT_VERSION_PROPERTY_UUID:
        {
            NSString *ver;
            
            Byte bAddress[BC1_KEYFOB_BATT_VERSION_LENGTH];
            [characteristic.value getBytes:&bAddress length:BC1_KEYFOB_BATT_VERSION_LENGTH];
            
            //  硬件版本号规则是：XX.XXXXXX.XXXX
            ver = [NSString stringWithFormat:@"%02x.%02x%02x%02x.%02x%02x", bAddress[0], bAddress[1], bAddress[2], bAddress[3], bAddress[4], bAddress[5]];
            
            [delegateBLEDeviceResult_Pedo_ZYWK BR_hardwareVersion_ZYWK:ver DeviceID:DeviceID];
            break;
        }
        // 接收电量数据
        case BC1_KEYFOB_BATT_STATE_PROPERTY_UUID:
        {
            int level;
            Byte bAddress[BC1_KEYFOB_BATT_STATE_LENGTH];
            [characteristic.value getBytes:&bAddress length:BC1_KEYFOB_BATT_STATE_LENGTH];
            level = bAddress[0];
            [delegateBLEDeviceResult_Pedo_ZYWK BR_batteryLevel_ZYWK:level DeviceID:DeviceID];
            break;
        }
        default:
        {
            if(AntiLost_Debug)
            {
                NSLog(@"didUpdateValueForCharacteristic --- default --- characteristicUUID:%d" , characteristicUUID);
            }
            break;
        }
    }
}

//  ------------------------------------------------------------------
//  协议Protocol_BLEDevice_Pedo_ZYWK的实现部分
//  ------------------------------------------------------------------

-(void)BD_enablePedometer:(BOOL)isEnable
{
    char data;
    
    if (isEnable)
    {
        data = 0x01;
    }
    else
    {
        data = 0x00;
    }
    NSData *d = [[NSData alloc] initWithBytes:&data length:1];
    
    [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID serviceUUID:BC1_KEYFOB_PEDOMETER_SERVICE_UUID characteristicUUID:BC1_KEYFOB_PEDOMETER_ACCELENABLE_PROPERTY_UUID data:d];
    
    [[BLEManager sharedInstance] notification:DeviceID serviceUUID:BC1_KEYFOB_PEDOMETER_SERVICE_UUID characteristicUUID:BC1_KEYFOB_PEDOMETER_COUNTENABLE_PROPERTY_UUID on:isEnable];
}

-(void)BD_enableOfflineData:(BOOL)isEnable
{
    [[BLEManager sharedInstance] notification:DeviceID serviceUUID:BC1_KEYFOB_OFFLINE_SERVICE_UUID characteristicUUID:BC1_KEYFOB_OFFLINE_DATA_PROPERTY_UUID on:isEnable];
}

-(void)BD_enableBatteryLevel_ZYWK:(BOOL)isEnable
{
    [[BLEManager sharedInstance] notification:DeviceID serviceUUID:BC1_KEYFOB_BATT_SERVICE_UUID characteristicUUID:BC1_KEYFOB_BATT_STATE_PROPERTY_UUID on:isEnable];
}

-(void)BD_setCurrentTime
{
    NSDate *date2000;
    uint time;
    NSDateFormatter *fm;
    char bytes[BC1_KEYFOB_TIME_LENGTH];
    
    fm = [[NSDateFormatter alloc]init];
    //    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    //    [fm setTimeZone:timeZone];
    [fm setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    date2000 = [fm dateFromString:@"2000-01-01 00:00:00"];
    time = (int)[[NSDate date] timeIntervalSinceDate:date2000];
    if (AntiLost_Debug)
    {
        NSLog(@"---------------%d",time);
    }
    bytes[0] = time;
    bytes[1] = time >> 8;
    bytes[2] = time >> 16;
    bytes[3] = time >> 24;
    
    NSData *d = [[NSData alloc] initWithBytes:bytes length:BC1_KEYFOB_TIME_LENGTH];
    [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID serviceUUID:BC1_KEYFOB_TIME_SERVICE_UUID characteristicUUID:BC1_KEYFOB_TIME_SYNCHRONOUS_PROPERTY_UUID data:d];
    
    time = (int)[[NSDate date] timeIntervalSinceDate:date2000];
    if (AntiLost_Debug)
    {
        NSLog(@"---------------%d,%d,%d,%d,%d",time,(Byte)bytes[0],(Byte)bytes[1],(Byte)bytes[2],(Byte)bytes[3]);
    }
}

-(void)BD_getCurrentTime
{
    [[BLEManager sharedInstance] readValueForCharacteristic:DeviceID serviceUUID:BC1_KEYFOB_TIME_SERVICE_UUID characteristicUUID:BC1_KEYFOB_TIME_SYNCHRONOUS_PROPERTY_UUID];
}

-(void)BD_clearOfflineData
{
    char data = 0xee;
    NSData *d = [[NSData alloc] initWithBytes:&data length:1];
    
    [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID serviceUUID:BC1_KEYFOB_OFFLINE_SERVICE_UUID characteristicUUID:BC1_KEYFOB_OFFLINE_SELECTDAY_PROPERTY_UUID data:d];
}

-(void)BD_clearPeripheralData
{
    if (![self BD_getIsConnected])
    {
        return;
    }
    char bytes[BC1_KEYFOB_PEDOMETER_COUNTE_LENGTH];
    bytes[0] = 0;
    bytes[1] = 0;
    bytes[2] = 0;
    bytes[3] = 0;
    bytes[4] = 0;
    bytes[5] = 0;
    bytes[6] = 0;
    bytes[7] = 0;
    bytes[8] = 0;
    bytes[9] = 0;
    bytes[10] = 0;
    bytes[11] = 0;
    bytes[12] = 0;
    bytes[13] = 0;
    bytes[14] = 0;
    bytes[15] = 0;
    NSData *d = [[NSData alloc] initWithBytes:bytes length:BC1_KEYFOB_PEDOMETER_COUNTE_LENGTH];
    [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID serviceUUID:BC1_KEYFOB_PEDOMETER_SERVICE_UUID characteristicUUID:BC1_KEYFOB_PEDOMETER_COUNTENABLE_PROPERTY_UUID data:d];
}

//  2013-08-05 set parameter by wangyi
-(void)BD_setParameter:(int)units height:(double)height weight:(double)weight stride:(double)stride nAlarmKind:(int)nAlarmKind tCalories:(double)tCalories tDistance:(double)tDistance tPaceCount:(double)tPaceCount tCostTime:(double)tCostTime
{
    if (![self BD_getIsConnected])
    {
        return;
    }

    char bytes[BC1_KEYFOB_CUSTOMER_PARAMETER_LENGTH];
    
    if (AntiLost_Debug)
    {
        NSLog(@"---------------%d",units);
    }
    //  1 Byte:单位制
    bytes[0] = units;
    //  1 Byte:身高
    bytes[1] = (int)(height * 100) ;
    //  2 Byte:体重
    bytes[2] = (int)(weight * 10);
    bytes[3] = (int)(weight * 10) >> 8;
    //  2 Byte:步长
    bytes[4] = (int)(stride * 1000);
    bytes[5] = (int)(stride * 1000) >> 8;
    //  1 byte:目标项
    bytes[6] = nAlarmKind;
    //  2 byte:目标值
    switch (nAlarmKind)
    {
        case 1:
            bytes[7] = (int)tCalories;
            bytes[8] = (int)tCalories >> 8;
            break;
            
        case 2:
            bytes[7] = (int)tDistance;
            bytes[8] = (int)tDistance >> 8;
            break;
            
        case 3:
            bytes[7] = (int)tPaceCount;
            bytes[8] = (int)tPaceCount >> 8;
            break;
            
        case 4:
            bytes[7] = (int)tCostTime;
            bytes[8] = (int)tCostTime >> 8;
            break;
            
        default:
            return;
    }
    
    NSData *d = [[NSData alloc] initWithBytes:bytes length:BC1_KEYFOB_CUSTOMER_PARAMETER_LENGTH];
    [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID serviceUUID:BC1_KEYFOB_CUSTOMER_SERVICE_UUID characteristicUUID:BC1_KEYFOB_CUSTOMER_PARAMETER_PROPERTY_UUID data:d];
}

//  coming call
-(void)BD_comingCall:(BOOL)isRead
{
    char data;
    if (!isRead)
    {
        data = 0x01;
    }
    else
    {
        data = 0x02;
    }
    NSData *d = [[NSData alloc] initWithBytes:&data length:1];
    
    [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID serviceUUID:BC1_KEYFOB_CUSTOMER_SERVICE_UUID characteristicUUID:BC1_KEYFOB_CUSTOMER_COMINGCALL_PROPERTY_UUID data:d];
}

-(void)BD_enableCamera:(BOOL)isEnable
{
    [[BLEManager sharedInstance] notification:DeviceID serviceUUID:BC1_KEYFOB_CUSTOMER_SERVICE_UUID characteristicUUID:BC1_KEYFOB_CUSTOMER_CAMERA_PROPERTY_UUID on:isEnable];
}

-(void)BD_enableHardwareVersion:(BOOL)isEnable
{
    [[BLEManager sharedInstance] notification:DeviceID serviceUUID:BC1_KEYFOB_BATT_SERVICE_UUID characteristicUUID:BC1_KEYFOB_BATT_VERSION_PROPERTY_UUID on:isEnable];
}

-(void)BD_getHardwareVersion
{
    [[BLEManager sharedInstance] readValueForCharacteristic:DeviceID serviceUUID:BC1_KEYFOB_BATT_SERVICE_UUID characteristicUUID:BC1_KEYFOB_BATT_VERSION_PROPERTY_UUID];
}

-(void)BD_getAccumulationData
{
    [[BLEManager sharedInstance] readValueForCharacteristic:DeviceID serviceUUID:BC1_KEYFOB_PEDOMETER_SERVICE_UUID characteristicUUID:BC1_KEYFOB_PEDOMETER_COUNTENABLE_PROPERTY_UUID];
}


@end
