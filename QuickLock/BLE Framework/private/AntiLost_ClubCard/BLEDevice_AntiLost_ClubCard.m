//
//  BLEDevice_AntiLost_ClubCard.m
//
//  ------------------------------------------------------------------
//  防丢器子类实现部分
//  ------------------------------------------------------------------

//  ------------------------------------------------------------------
//  防丢器子类的常量定义
//  ------------------------------------------------------------------

#ifndef AntiLost_TI_ItonBLEDefines_h
#define AntiLost_TI_ItonBLEDefines_h

// Defines for the TI CC2540 keyfob peripheral

#define TI_KEYFOB_PROXIMITY_ALERT_UUID                      0x1802
#define TI_KEYFOB_PROXIMITY_ALERT_PROPERTY_UUID             0x2a06
#define TI_KEYFOB_PROXIMITY_ALERT_ON_VAL                    0x01
#define TI_KEYFOB_PROXIMITY_ALERT_HIGH_VAL                  0x02
#define TI_KEYFOB_PROXIMITY_ALERT_OFF_VAL                   0x00
#define TI_KEYFOB_PROXIMITY_ALERT_WRITE_LEN                 1

#define TI_KEYFOB_BATT_SERVICE_UUID                         0x180F
#define TI_KEYFOB_LEVEL_SERVICE_UUID                        0x2A19

#define TI_KEYFOB_POWER_STATE_UUID                          0xFFB2
#define TI_KEYFOB_LEVEL_SERVICE_READ_LEN                    1

#define TI_KEYFOB_KEYS_SERVICE_UUID                         0xFFE0
#define TI_KEYFOB_KEYS_NOTIFICATION_UUID                    0xFFE1
#define TI_KEYFOB_KEYS_NOTIFICATION_READ_LEN                1

#define TI_KEYFOB_CUSTOM_SERVICE_UUID                       0xFF00
// HONESTSKY 2015-02-06 originally anti lost Change of FF01 FF04, accommodate the increased need for transparent transmission (transparent transmission will need FF01 FF02)
#define TI_KEYFOB_SLEEP_ATT_UUID                            0xFF04
#define TI_KEYFOB_SLEEP_ON_VAL                              0x06
//HONESTSKY 2015-02-06 增加透传部分
#define BC_KEYFOB_CUSTOMER_SEND_PROPERTY_UUID               0xFF01
#define BC_KEYFOB_CUSTOMER_RECV_PROPERTY_UUID               0xFF02
#define BC_KEYFOB_CUSTOMER_RECV_LENGTH                      20


#endif


#import "BLEDevice_AntiLost_ClubCard.h"
#import "BLEManager.h"
//  SID 20150207
#define LENGTHOFFSET 2
#define COMMANDOFFSET 3
#define PAYLOADOFFSET 4
#define DATAOFFSET 5
NSTimer *sendCmdTimer;
NSMutableData *receiveData;

@implementation BLEDevice_AntiLost_ClubCard

@synthesize delegateBLEDeviceResult_Antilost_ClubCard;

-(id)init:(id)delegete
{
        [self setDelegateBLEDeviceResult_Antilost_ClubCard:delegete];
        receiveData = [NSMutableData new];
        return [super init:delegete];
}

//  ------------------------------------------------------------------
//  内部私有函数部分
//  ------------------------------------------------------------------

//  硬件数据接收函数 - 重载具体实现部分，每个子类不同
- (void)didUpdateValueForCharacteristic_override:(CBCharacteristic *)characteristic
{
        UInt16 characteristicUUID = [BLEPublic CBUUIDToInt:characteristic.UUID];
        switch(characteristicUUID)
        {
                //电池电量数据更新：5s一次，保持程序始终在iOS后台运行不被挂起
                case TI_KEYFOB_LEVEL_SERVICE_UUID:
                {
                        char batlevel;
                        [characteristic.value getBytes:&batlevel length:TI_KEYFOB_LEVEL_SERVICE_READ_LEN];
                        [delegateBLEDeviceResult_Antilost_ClubCard BR_batteryUpdated:(float)batlevel DeviceID:DeviceID];
                        if(AntiLost_Debug)
                        {
                                NSLog(@"didUpdateValueForCharacteristic --- batteryUpdate --- %f", (float)batlevel);
                        }
                        break;
                }
                //按键更新：防丢器反向查找手机
                case TI_KEYFOB_KEYS_NOTIFICATION_UUID:
                {
                        char keys;
                        [characteristic.value getBytes:&keys length:TI_KEYFOB_KEYS_NOTIFICATION_READ_LEN];
                        if (keys == 0x12)
                        {
                                [delegateBLEDeviceResult_Antilost_ClubCard BR_ringkeyValuesUpdated:DeviceID];
                        }
                        break;
                }
                //  透传数据接收
                case BC_KEYFOB_CUSTOMER_RECV_PROPERTY_UUID:
                {
                        int nLength = (int)characteristic.value.length;
                        Byte bAddress[nLength];
                        [characteristic.value getBytes:&bAddress length:nLength];
                        
//                        [delegateBLEDeviceResult_Antilost_ClubCard BR_receiveBaseData:bAddress
//                                                                               length:nLength
//                                                                             recvTime:[NSDate date]
//                                                                             DeviceID:DeviceID];
                        //  SID 20150207
                        [self receiveBaseData:bAddress
                                       length:nLength
                                     recvTime:[NSDate date]];

                        break;
                }

                default:
                {
                        if(AntiLost_Debug)
                        {
                                NSLog(@"didUpdateValueForCharacteristic --- default --- characteristicUUID:%d" , characteristicUUID);
                        }
                        break;
                }
        }
}

//  ------------------------------------------------------------------
//  协议Protocol_BLEDevice_Antilost_ClubCard的实现部分
//  ------------------------------------------------------------------

#pragma mark
#pragma mark 防丢器部分
#pragma mark

-(void)BD_buzzerAlarm:(bool)bLarge
{
        Byte buzVal;
        
        if (bLarge)
        {
                buzVal = TI_KEYFOB_PROXIMITY_ALERT_HIGH_VAL;
        }
        else
        {
                buzVal = TI_KEYFOB_PROXIMITY_ALERT_ON_VAL;
        }
        NSData *d = [[NSData alloc] initWithBytes:&buzVal length:TI_KEYFOB_PROXIMITY_ALERT_WRITE_LEN];
        
        [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID serviceUUID:TI_KEYFOB_PROXIMITY_ALERT_UUID characteristicUUID:TI_KEYFOB_PROXIMITY_ALERT_PROPERTY_UUID data:d];
}

-(void)BD_buzerStop
{
        Byte buzVal = TI_KEYFOB_PROXIMITY_ALERT_OFF_VAL;
        NSData *d = [[NSData alloc] initWithBytes:&buzVal length:TI_KEYFOB_PROXIMITY_ALERT_WRITE_LEN];
        
        [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID serviceUUID:TI_KEYFOB_PROXIMITY_ALERT_UUID characteristicUUID:TI_KEYFOB_PROXIMITY_ALERT_PROPERTY_UUID data:d];
}

-(void)BD_enableButtons:(BOOL)isEnable
{
        [[BLEManager sharedInstance] notification:DeviceID serviceUUID:TI_KEYFOB_KEYS_SERVICE_UUID characteristicUUID:TI_KEYFOB_KEYS_NOTIFICATION_UUID on:isEnable];
}

-(void)BD_enableBattery:(BOOL)isEnable
{
        char data;
        
        if (isEnable)
        {
                data = 0x01;
        }
        else
        {
                data = 0x00;
        }
        NSData *d = [[NSData alloc] initWithBytes:&data length:1];
        
        [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID serviceUUID:TI_KEYFOB_BATT_SERVICE_UUID characteristicUUID:TI_KEYFOB_LEVEL_SERVICE_UUID data:d];
        
        [[BLEManager sharedInstance] notification:DeviceID serviceUUID:TI_KEYFOB_BATT_SERVICE_UUID characteristicUUID:TI_KEYFOB_LEVEL_SERVICE_UUID on:isEnable];
}

-(void)BD_setPeripheralSleep
{
        char data = TI_KEYFOB_SLEEP_ON_VAL;
        NSData *d = [[NSData alloc] initWithBytes:&data length:1];
        
        [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID serviceUUID:TI_KEYFOB_CUSTOM_SERVICE_UUID characteristicUUID:TI_KEYFOB_SLEEP_ATT_UUID data:d];
}

#pragma mark
#pragma mark 透传部分
#pragma mark

-(void)BD_enableTransparentData:(BOOL)isEnable
{
        [[BLEManager sharedInstance] notification:DeviceID serviceUUID:TI_KEYFOB_CUSTOM_SERVICE_UUID characteristicUUID:BC_KEYFOB_CUSTOMER_RECV_PROPERTY_UUID on:isEnable];
}

-(void)BD_sendFullLengthData:(char*)bytes length:(int)length
{
        char chars[BC_KEYFOB_CUSTOMER_RECV_LENGTH] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
        int i;
        
        for (i = 0; i < BC_KEYFOB_CUSTOMER_RECV_LENGTH; i++)
        {
                if (i < length)
                {
                        chars[i] = bytes[i];
                }
        }
        
        NSData *d = [[NSData alloc] initWithBytes:chars length:BC_KEYFOB_CUSTOMER_RECV_LENGTH];
        [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID serviceUUID:TI_KEYFOB_CUSTOM_SERVICE_UUID characteristicUUID:BC_KEYFOB_CUSTOMER_SEND_PROPERTY_UUID data:d];
}

-(int)BD_getDataFullLength
{
        return BC_KEYFOB_CUSTOMER_RECV_LENGTH;
}

-(void)BD_enableClubCardData:(BOOL)isEnable
{
        [self BD_enableTransparentData:isEnable];
}

//  SID 20150207
-(void)BD_getFixedID:(int)index
{
    Byte cmd[6];
    //Header
    cmd[0] = 0xFF;
    cmd[1] = 0x55;
    //Length
    cmd[2] = 0x02;
    //Command
    cmd[3] = 0x01;
    //Payload
    cmd[4] = (Byte)index;
    //Checksum
    cmd[5] = [self getChecksum:cmd];
    [self sendData:cmd length:6];
    sendCmdTimer = [NSTimer scheduledTimerWithTimeInterval:3.0 target:self selector:@selector(sendTimeOut) userInfo:nil repeats:NO];
}

//  SID 20150207
-(void)BD_setFixedID:(int)index fixedID:(NSString *)fixedID
{
    switch (index) {
        case 0:
        {
            //Get public ID data
            Byte cmd[22];
            //Header
            cmd[0] = 0xFF;
            cmd[1] = 0x55;
            //Length
            cmd[2] = 0x12;
            //Command
            cmd[3] = 0x81;
            //Payload
            cmd[4] = (Byte)index;
            const char* data = [fixedID UTF8String];
            for (int i = 0; i < 16; i++) {
                cmd[5 + i] = data[i];
            }
            //Checksum
            cmd[21] = [self getChecksum:cmd];
            [self sendData:cmd length:22];
            break;
        }
        case 1:
        case 2:
        {
            Byte cmd[70];
            //Header
            cmd[0] = 0xFF;
            cmd[1] = 0x55;
            //Length
            cmd[2] = 0x42;
            //Command
            cmd[3] = 0x81;
            //Payload
            cmd[4] = (Byte)index;
            const char* data = [fixedID UTF8String];
            for (int i = 0; i < 64; i++) {
                cmd[5 + i] = data[i];
            }
            //Checksum
            cmd[69] = [self getChecksum:cmd];
            [self sendData:cmd length:70];
            break;
        }
        default:
            break;
    }
    sendCmdTimer = [NSTimer scheduledTimerWithTimeInterval:3.0 target:self selector:@selector(sendTimeOut) userInfo:nil repeats:NO];
}

//  SID 20150207
-(void)BD_getFloatingID:(int)index
{
    Byte cmd[6];
    //Header
    cmd[0] = 0xFF;
    cmd[1] = 0x55;
    //Length
    cmd[2] = 0x02;
    //Command
    cmd[3] = 0x02;
    //Payload
    cmd[4] = (Byte)index;
    //Checksum
    cmd[5] = [self getChecksum:cmd];
    [self sendData:cmd length:6];
    sendCmdTimer = [NSTimer scheduledTimerWithTimeInterval:3.0 target:self selector:@selector(sendTimeOut) userInfo:nil repeats:NO];
}

//  SID 20150207
-(void)BD_setFloatingID:(int)index floatingID:(NSString *)floatingID
{
    Byte cmd[22];
    //Header
    cmd[0] = 0xFF;
    cmd[1] = 0x55;
    //Length
    cmd[2] = 0x12;
    //Command
    cmd[3] = 0x82;
    //Payload
    cmd[4] = (Byte)index;
    const char* data = [floatingID UTF8String];
    for (int i = 0; i < 16; i++) {
        cmd[5 + i] = data[i];
    }
    //Checksum
    cmd[21] = [self getChecksum:cmd];
    [self sendData:cmd length:22];
    sendCmdTimer = [NSTimer scheduledTimerWithTimeInterval:3.0 target:self selector:@selector(sendTimeOut) userInfo:nil repeats:NO];
}

//  SID 20150207
- (void)sendTimeOut
{
    [delegateBLEDeviceResult_Antilost_ClubCard BR_ACK:2 DeviceID:DeviceID];
}

//  SID 20150207
-(void)receiveBaseData:(Byte*)bytes length:(int)length recvTime:(NSDate*)recvTime
{
    [receiveData appendBytes:bytes length:length];
    
    Byte *tmpData = (Byte*)[receiveData bytes];
    int headerPosition = [self rangeOfHeader:0xFF header2:0x33 data:tmpData length:(int)[receiveData length]];
    while (headerPosition != -1) {
        int length = (int)tmpData[headerPosition + LENGTHOFFSET];
        int checksumPosition = headerPosition + 2 + length + 1;//起始位置+头长度+数据长度
        if ([receiveData length] >= checksumPosition) {
            if (tmpData[checksumPosition] == [self getChecksum:&tmpData[headerPosition]]) {
                switch (tmpData[headerPosition + COMMANDOFFSET]) {
                    case 0x01://RetFixedID
                    {
                        int index = tmpData[headerPosition + PAYLOADOFFSET];
                        NSData *fixID = [NSData dataWithBytes:&tmpData[headerPosition + DATAOFFSET] length:length - 2];
                        [delegateBLEDeviceResult_Antilost_ClubCard BR_GetFixedID:index fixedID:[[NSString alloc]initWithData:fixID encoding:NSASCIIStringEncoding] DeviceID:DeviceID];
                        [sendCmdTimer invalidate];
                        [self Ack:0x00];
                        break;
                    }
                    case 0x02://RetFloatingID
                    {
                        int index = tmpData[headerPosition + PAYLOADOFFSET];
                        NSData *floatingID = [NSData dataWithBytes:&tmpData[headerPosition + DATAOFFSET] length:length - 2];
                        [delegateBLEDeviceResult_Antilost_ClubCard BR_GetFloatingID:index floatingID:[[NSString alloc]initWithData:floatingID encoding:NSASCIIStringEncoding] DeviceID:DeviceID];
                        [sendCmdTimer invalidate];
                        [self Ack:0x00];
                        break;
                    }
                    case 0x03://AuthEnableStatus
                    {
                        if (tmpData[headerPosition + PAYLOADOFFSET] == 0x00) {
                            [delegateBLEDeviceResult_Antilost_ClubCard BR_AuthEnableStatus:TRUE DeviceID:DeviceID];
                        }else{
                            [delegateBLEDeviceResult_Antilost_ClubCard BR_AuthEnableStatus:FALSE DeviceID:DeviceID];
                        }
                        break;
                    }
                    case 0x20://Ack
                    {
                        [sendCmdTimer invalidate];
                        if (tmpData[headerPosition + PAYLOADOFFSET] == 0x00) {
                            [delegateBLEDeviceResult_Antilost_ClubCard BR_ACK:0 DeviceID:DeviceID];
                        }else{
                            [delegateBLEDeviceResult_Antilost_ClubCard BR_ACK:1 DeviceID:DeviceID];
                        }
                        break;
                    }
                    default:
                        break;
                }
            }
            receiveData = [[NSMutableData alloc]initWithBytes:&tmpData[checksumPosition + 1] length:[receiveData length] - checksumPosition -1];
            headerPosition = [self rangeOfHeader:0xFF header2:0x33 data:tmpData length:(int)[receiveData length]];
        }
        else
        {
            break;
        }
    }
}

//  SID 20150207
- (int)rangeOfHeader:(Byte)h1 header2:(Byte)h2 data:(Byte*)bytes length:(int)length
{
    int result = -1;
    for (int i = 0; i < length - 1; i++) {
        if (bytes[i] == h1 && bytes[i + 1] == h2) {
            result = i;
            break;
        }
    }
    return result;
}

//  SID 20150207
- (Byte)getChecksum:(Byte*)cmd
{
    Byte checksum = 0x00;
    int length = cmd[LENGTHOFFSET];
    for (int i = 0; i < length; i++) {
        checksum = checksum + cmd[i + COMMANDOFFSET];
    }
    return checksum & 0xFF;
}

//  SID 20150207
- (void)Ack:(char)type
{
    Byte cmd[6];
    //Header
    cmd[0] = 0xFF;
    cmd[1] = 0x55;
    //Length
    cmd[2] = 0x02;
    //Command
    cmd[3] = 0x20;
    //Payload
    cmd[4] = type;
    //Checksum
    cmd[5] = [self getChecksum:cmd];
    [self sendData:cmd length:6];
}

//  SID 20150207
- (void)sendData:(Byte*)cmd length:(int)length
{
    //如果长度大于最大长度,分多次发送
    int maxLength = [self BD_getDataFullLength];
    int sendCount = ceil((float)length / maxLength);
    for (int i = 0; i < sendCount; i++)
    {
        int sendLength;
        if (i == sendCount - 1)
        {
            sendLength = length % maxLength;
        }
        else
        {
            sendLength = maxLength;
        }
        Byte data[sendLength];
        
        for (int j = 0; j < sendLength; j++)
        {
            data[j] = cmd[i * maxLength + j];
        }
        [self BD_sendFullLengthData:(char*)data length:sendLength];
    }
}

@end

