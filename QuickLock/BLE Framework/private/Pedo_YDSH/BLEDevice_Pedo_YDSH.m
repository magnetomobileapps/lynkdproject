//
//  BLEDevice_Pedo_YDSH.m
//
//  Copyright (c) 2014, Vicro Technology Limited. All rights reserved.
//
//  ------------------------------------------------------------------
//  新计步器子类实现部分
//  ------------------------------------------------------------------

//  ------------------------------------------------------------------
//  新计步器子类的常量定义
//  ------------------------------------------------------------------

#ifndef PEDO_YDSH_TI_ItonBLEDefines_h
#define PEDO_YDSH_TI_ItonBLEDefines_h

#define YDSH_TI_KEYFOB_PEDOMETER_SERVICE_UUID                                   0x1814
#define YDSH_TI_KEYFOB_PEDOMETER_ACCELENABLE_PROPERTY_UUID                      0xFFA1
#define YDSH_TI_KEYFOB_PEDOMETER_COUNTENABLE_PROPERTY_UUID                      0xFFA6
#define YDSH_TI_KEYFOB_PEDOMETER_COUNTE_LENGTH                                  16

#define YDSH_TI_KEYFOB_TIME_SERVICE_UUID                                        0x1805
#define YDSH_TI_KEYFOB_TIME_SYNCHRONOUS_PROPERTY_UUID                           0x2A2B
#define YDSH_TI_KEYFOB_TIME_LENGTH                                              4

#define YDSH_TI_KEYFOB_OFFLINE_SERVICE_UUID                                     0xFFC0
#define YDSH_TI_KEYFOB_OFFLINE_SELECT_DAY_PROPERTY_UUID                         0xFFC1
#define YDSH_TI_KEYFOB_OFFLINE_DATA_DAY_PROPERTY_UUID                           0xFFC2
#define YDSH_TI_KEYFOB_OFFLINE_SELECT_MINUTEDAY_PROPERTY_UUID                   0xFFC3
#define YDSH_TI_KEYFOB_OFFLINE_DATA_MINUTEDAY_PROPERTY_UUID                     0xFFC4
#define YDSH_TI_KEYFOB_OFFLINE_DATA_DAY_LENGTH                                  20
#define YDSH_TI_KEYFOB_OFFLINE_SELECT_LENGTH                                    1

#define YDSH_TI_KEYFOB_BATT_SERVICE_UUID                                        0x180F
#define YDSH_TI_KEYFOB_BATT_STATE_PROPERTY_UUID                                 0x2A19
#define YDSH_TI_KEYFOB_BATT_STATE_LENGTH                                        1

#define YDSH_TI_KEYFOB_VERSION_SERVICE_UUID                                     0x180A
#define YDSH_TI_KEYFOB_PROTOCOL_VERSION_PROPERTY_UUID                           0xFF02
#define YDSH_TI_KEYFOB_PROTOCOL_VERSION_LENGTH                                  1

#endif

#import "BLEDevice_Pedo_YDSH.h"
#import "BLEManager.h"


@implementation BLEDevice_Pedo_YDSH

@synthesize delegateBLEDeviceResult_Pedo_YDSH;

//int nOffline;

//  Data staging points and control flags
BOOL isStartReceiveOfflineMinuteData;
BOOL isContinueReceiveOfflineMinuteData;
float receiveOverTime = 0.2f;
NSDate *curMinuteDataDay;
NSString *curMinuteData;
int curMinuteDataCount;

-(id)init:(id)delegete
{
        isStartReceiveOfflineMinuteData = FALSE;
        isContinueReceiveOfflineMinuteData = FALSE;
        curMinuteData = @"";
        curMinuteDataCount = 0;
        
        [self setDelegateBLEDeviceResult_Pedo_YDSH:delegete];
        return [super init:delegete];
}


//  ------------------------------------------------------------------
//  内部私有函数部分
//  ------------------------------------------------------------------

//  硬件数据接收函数 - 重载具体实现部分，每个子类不同
- (void)didUpdateValueForCharacteristic_override:(CBCharacteristic *)characteristic
{
        UInt16 characteristicUUID = [BLEPublic CBUUIDToInt:characteristic.UUID];
        switch(characteristicUUID)
        {
                case YDSH_TI_KEYFOB_PROTOCOL_VERSION_PROPERTY_UUID:
                {
                        Byte bAddress[YDSH_TI_KEYFOB_PROTOCOL_VERSION_LENGTH];
                        [characteristic.value getBytes:&bAddress length:YDSH_TI_KEYFOB_PROTOCOL_VERSION_LENGTH];
                        
                        NSString *protocolVersion = @"";
                        protocolVersion = [NSString stringWithFormat:@"%x", bAddress[0]];
                        [delegateBLEDeviceResult_Pedo_YDSH BR_FirmwareVersion:protocolVersion DeviceID:DeviceID];
                        break;
                        
                }
                case YDSH_TI_KEYFOB_PEDOMETER_COUNTENABLE_PROPERTY_UUID:
                {
                        //总字节数：16
                        //运动步数(步)(2字节，低字节在前)
                        //有氧步数(步)(2字节，低字节在前)
                        //运动时间(秒)(2字节，低字节在前)
                        //有氧时间(秒)(2字节，低字节在前)
                        //运动距离(0.01Km)(2字节，低字节在前)（写入时补0x00即可，固件不判断）
                        //有氧距离(0.01Km)(2字节，低字节在前)（写入时补0x00即可，固件不判断）
                        //运动热量(0.1Kcal)(2字节，低字节在前)（写入时补0x00即可，固件不判断）
                        //有氧热量(0.1Kcal)(2字节，低字节在前)（写入时补0x00即可，固件不判断）
                        
                        Byte bAddress[YDSH_TI_KEYFOB_PEDOMETER_COUNTE_LENGTH];
                        [characteristic.value getBytes:&bAddress length:YDSH_TI_KEYFOB_PEDOMETER_COUNTE_LENGTH];
                        
                        int n, b0, b1;
                        b1 = (int)bAddress[1];
                        b0 = (int)bAddress[0];
                        n = b0 | (b1 << 8);    //注意：2014-6-7 此API仅仅提供基础计步功能，并且不设置个人参数到硬件，因此其他数据均不提供，仅提供基础计步数
                        
                        if (AntiLost_Debug)
                        {
                                NSLog(@"0xffa6-----: %d,%d,%d",b0, b1, n);
                        }
                        
                        [delegateBLEDeviceResult_Pedo_YDSH BR_pedoCountUpdated:n DeviceID:DeviceID];
                        break;
                }
                case YDSH_TI_KEYFOB_TIME_SYNCHRONOUS_PROPERTY_UUID:
                {
                        int n;
                        NSDate *da,*date2000;
                        NSDateFormatter *fm;
                        Byte bAddress[YDSH_TI_KEYFOB_TIME_LENGTH];
                        
                        fm = [[NSDateFormatter alloc]init];
                        //            NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
                        //            [fm setTimeZone:timeZone];
                        [fm setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
                        date2000 = [fm dateFromString:@"2000-01-01 00:00:00"];
                        [characteristic.value getBytes:&bAddress length:YDSH_TI_KEYFOB_TIME_LENGTH];
                        int b1,b2,b3,b4;
                        b1 = (int)bAddress[0];
                        b2 = (int)bAddress[1];
                        b3 = (int)bAddress[2];
                        b4 = (int)bAddress[3];
                        n = b1 | (b2 << 8) | (b3 << 16) | (b4 << 24);
                        da = [[NSDate alloc]initWithTimeInterval:n sinceDate:date2000];
                        
                        if (AntiLost_Debug)
                        {
                                NSLog(@"0x2A2B-----: %d,%d,%d,%d,%d,%@",b1,b2,b3,b4,n,[fm stringFromDate:da]);
                        }
                        
                        [delegateBLEDeviceResult_Pedo_YDSH BR_currentTime:da DeviceID:DeviceID];
                        break;
                }
                case YDSH_TI_KEYFOB_OFFLINE_SELECT_DAY_PROPERTY_UUID:
                {
                        int n;
                        Byte bAddress[YDSH_TI_KEYFOB_OFFLINE_SELECT_LENGTH];
                        [characteristic.value getBytes:&bAddress length:YDSH_TI_KEYFOB_OFFLINE_SELECT_LENGTH];
                        n = bAddress[0];
                        [delegateBLEDeviceResult_Pedo_YDSH BR_OfflineData_DaysNumber:n DeviceID:DeviceID];
                        break;
                }
                case YDSH_TI_KEYFOB_OFFLINE_SELECT_MINUTEDAY_PROPERTY_UUID:
                {
                        int n;
                        Byte bAddress[YDSH_TI_KEYFOB_OFFLINE_SELECT_LENGTH];
                        [characteristic.value getBytes:&bAddress length:YDSH_TI_KEYFOB_OFFLINE_SELECT_LENGTH];
                        n = bAddress[0];
                        [delegateBLEDeviceResult_Pedo_YDSH BR_OfflineData_MinutesDayNumber:n DeviceID:DeviceID];
                        break;
                }
                        //天数据接收，每一个数据包为一个天数据，最大15天
                case YDSH_TI_KEYFOB_OFFLINE_DATA_DAY_PROPERTY_UUID:
                {
                        //日期(UTC时间)(低字节在前)【表示2000年1月1日0时0分0秒开始的秒数】
                        //运动步数(步)(低字节在前)
                        //有氧步数(步)(低字节在前)
                        //运动时间(秒)(低字节在前)
                        //有氧时间(秒)(低字节在前)
                        //运动距离(0.01Km)(低字节在前)
                        //有氧距离(0.01Km)(低字节在前)
                        //运动热量(0.1Kcal)(低字节在前)
                        //有氧热量(0.1Kcal)(低字节在前)
                        
                        Byte bAddress[YDSH_TI_KEYFOB_OFFLINE_DATA_DAY_LENGTH];
                        int nTimerecord,nPaceCount, n;
                        NSDate *da;
                        
                        [characteristic.value getBytes:&bAddress length:YDSH_TI_KEYFOB_OFFLINE_DATA_DAY_LENGTH];
                        int b1,b2,b3,b4,b5,b6,b9,b10;
                        
                        //注意：2014-6-9 此API仅仅提供基础计步功能，并且不设置个人参数到硬件，因此其他数据均不提供，仅提供日期、计步数、运动时间
                        b1 = (int)bAddress[0];
                        b2 = (int)bAddress[1];
                        b3 = (int)bAddress[2];
                        b4 = (int)bAddress[3];
                        
                        b5 = (int)bAddress[4];
                        b6 = (int)bAddress[5];
                        
                        b9 = (int)bAddress[8];
                        b10 = (int)bAddress[9];
                        
                        n = b1 | (b2 << 8) | (b3 << 16) | (b4 << 24);
                        da = [self parseBytesToDate:b1 b1:b2 b2:b3 b3:b4];
                        
                        nPaceCount = b5 | (b6 << 8);
                        nTimerecord = b9 | (b10 << 8);
                        
                        if (AntiLost_Debug)
                        {
                                NSLog(@"OfflineData_Day-nTimerecord:%d",nTimerecord);
                                NSLog(@"OfflineData_Day-nPaceCount:%d",nPaceCount);
                        }
                        if((nTimerecord > 0) && (nPaceCount > 0))
                        {
                                [delegateBLEDeviceResult_Pedo_YDSH BR_pedoReceiveOfflineData_Days:da
                                                                                       timerecord:nTimerecord
                                                                                        paceCount:nPaceCount
                                                                                         DeviceID:DeviceID];
                        }
                        
                        if (AntiLost_Debug)
                        {
                                NSLog(@"0xFFC2-----: offlineDateDay - %d,%d,%d,%d,[%d] timerecord - %d,%d,[%d] paceCount - %d,%d,[%d]",b1,b2,b3,b4,n,b5,b6,nTimerecord,b9,b10,nPaceCount);
                        }
                        break;
                }
                        //分数据接收，一天的历史分数据分1-289次上传，最大16天，因此需要用定时器组包，最后按天返回数据
                case YDSH_TI_KEYFOB_OFFLINE_DATA_MINUTEDAY_PROPERTY_UUID:
                {
                        //分数据的数据包结构
                        //每日历史分数据的开始标志（0xF0F0F0F0）
                        //日期(UTC时间)(低字节在前)【表示2000年1月1日0时0分0秒开始的秒数】
                        //第一组历史分数据(低字节在前)【Byte0：时，Byte1：分，Byte2-3：当天步数】
                        //第二组历史分数据(低字节在前)【Byte0：时，Byte1：分，Byte2-3：当天步数】
                        //...
                        //最后一组历史分数据(低字节在前)【Byte0：时，Byte1：分，Byte2-3：当天步数】
                        //如果最后一组数据没有填满20字节，则其余填0，如果一组4个字节全0，表示全部传输完毕
                        //根据约定，200ms内没有后续数据，则认为传输结束
                        
                        
                        //分数据接收算法：
                        //当首次接收到（isStartReceiveOfflineMinuteData为False）数据后，开启接收超时定时器，设置继续接收标志（isContinueReceiveOfflineMinuteData）为True
                        //超时定时器以200ms为最大超时时间，每200ms循环一次，判断是否要继续等待超时（继续接收标志为True）
                        //当每一笔数据来时，继续接收标志被改写为True，并暂存当时的数据到缓存
                        //如果下一笔数据从“开始标志（0xF0F0F0F0）”开始，则意味着新的一天的分数据到达，此时将之前的缓存数据总结为一天，并返回一次，清除缓存后重新缓存当前数据
                        //当超时定时器下一个循环发现继续接收标志为True，则主动更改为False，并等待再下一次循环
                        //如超时定时器发现继续接收标志为False，则意味着100ms内没有数据到达，此时将缓存数据总结为一天，并返回一次，最后关闭接收标志，并且清除缓存
                        
                        Byte bAddress[YDSH_TI_KEYFOB_OFFLINE_DATA_DAY_LENGTH];
                        Byte b0, b1, b2, b3;
                        BOOL isMinuteInitData;
                        int n, i;
                        
                        [characteristic.value getBytes:&bAddress length:YDSH_TI_KEYFOB_OFFLINE_DATA_DAY_LENGTH];
                        n = 0;
                        b0 = bAddress[n + 0];
                        b1 = bAddress[n + 1];
                        b2 = bAddress[n + 2];
                        b3 = bAddress[n + 3];
                        isMinuteInitData = (b0 == 0xF0) && (b1 == 0xF0) && (b2 == 0xF0) &&(b3 == 0xF0);
                        
                        if (!isStartReceiveOfflineMinuteData)
                        {
                                if (!isMinuteInitData)
                                {
                                        //如果是初次传输，但是又不是一天的第一组数据，说明这个数据可能是超时的数据，因此不予处理，直接丢弃
                                        return;
                                }
                                
                                isStartReceiveOfflineMinuteData = TRUE;
                                isContinueReceiveOfflineMinuteData = FALSE;
                                
                                [NSTimer scheduledTimerWithTimeInterval:receiveOverTime
                                                                 target:self
                                                               selector:@selector(checkReceiveOfflineOverTime)
                                                               userInfo:nil
                                                                repeats:NO];
                        }
                        else
                        {
                                isContinueReceiveOfflineMinuteData = TRUE;
                        }
                        
                        
                        if (isMinuteInitData) //一天的第一组数据
                        {
                                //先判断缓存是否不为空，是则意味着有之前的数据，需要先总结返回后，清除后再重新缓存
                                if (![curMinuteData isEqualToString:@""])
                                {
                                        [delegateBLEDeviceResult_Pedo_YDSH BR_pedoReceiveOfflineData_Minutes:curMinuteDataDay
                                                                                            dayMinutesRecord:curMinuteData
                                                                                                 recordCount:curMinuteDataCount
                                                                                                    DeviceID:DeviceID];
                                        
                                        curMinuteDataDay = [NSDate date];
                                        curMinuteData = @"";
                                        curMinuteDataCount = 0;
                                }
                                
                                //先取日期数据
                                n = 4;
                                b0 = bAddress[n + 0];
                                b1 = bAddress[n + 1];
                                b2 = bAddress[n + 2];
                                b3 = bAddress[n + 3];
                                curMinuteDataDay = [self parseBytesToDate:b0 b1:b1 b2:b2 b3:b3];
                                
                                
                                //再取后三组计步分数据，如果有任何一组为0，则直接放弃（返回超时等待）
                                for (i = 0; i < 3; i++)
                                {
                                        n = i * 4 + 8;
                                        b0 = bAddress[n + 0];
                                        b1 = bAddress[n + 1];
                                        b2 = bAddress[n + 2];
                                        b3 = bAddress[n + 3];
                                        
                                        if (![self checkBytesZero:b0 b1:b1 b2:b2 b3:b3])
                                        {
                                                curMinuteData = [self parseBytesToOfflineMinuteData:b0
                                                                                                 b1:b1
                                                                                                 b2:b2
                                                                                                 b3:b3
                                                                                               data:curMinuteData];
                                                curMinuteDataCount = curMinuteDataCount + 1;
                                        }
                                        else
                                        {
                                                break;
                                        }
                                }
                        }
                        else
                        {
                                //提取5组计步分数据，如果有任何一组为0，则直接放弃（返回超时等待）
                                for (i = 0; i < 5; i++)
                                {
                                        n = i * 4;
                                        b0 = bAddress[n + 0];
                                        b1 = bAddress[n + 1];
                                        b2 = bAddress[n + 2];
                                        b3 = bAddress[n + 3];
                                        
                                        if (![self checkBytesZero:b0 b1:b1 b2:b2 b3:b3])
                                        {
                                                curMinuteData = [self parseBytesToOfflineMinuteData:b0
                                                                                                 b1:b1
                                                                                                 b2:b2
                                                                                                 b3:b3
                                                                                               data:curMinuteData];
                                                curMinuteDataCount = curMinuteDataCount + 1;
                                        }
                                        else
                                        {
                                                break;
                                        }
                                }
                        }
                        break;
                }
                case YDSH_TI_KEYFOB_BATT_STATE_PROPERTY_UUID:
                {
                        int level;
                        Byte bAddress[YDSH_TI_KEYFOB_BATT_STATE_LENGTH];
                        [characteristic.value getBytes:&bAddress length:YDSH_TI_KEYFOB_BATT_STATE_LENGTH];
                        level = bAddress[0];
                        [delegateBLEDeviceResult_Pedo_YDSH BR_batteryLevel:level DeviceID:DeviceID];
                        break;
                }
                default:
                {
                        if(AntiLost_Debug)
                        {
                                NSLog(@"didUpdateValueForCharacteristic --- default --- characteristicUUID:%d" , characteristicUUID);
                        }
                        break;
                }
        }
}

//  分数据接收超时定时器
-(void)checkReceiveOfflineOverTime
{
        if (!isContinueReceiveOfflineMinuteData)
        {
                if (![curMinuteData isEqualToString:@""])
                {
                        [delegateBLEDeviceResult_Pedo_YDSH BR_pedoReceiveOfflineData_Minutes:curMinuteDataDay
                                                                            dayMinutesRecord:curMinuteData
                                                                                 recordCount:curMinuteDataCount
                                                                                    DeviceID:DeviceID];
                }
                isStartReceiveOfflineMinuteData = FALSE;
                isContinueReceiveOfflineMinuteData = FALSE;
                curMinuteDataDay = [NSDate date];
                curMinuteData = @"";
                curMinuteDataCount = 0;
        }
        else
        {
                isContinueReceiveOfflineMinuteData = FALSE;
                [NSTimer scheduledTimerWithTimeInterval:receiveOverTime
                                                 target:self
                                               selector:@selector(checkReceiveOfflineOverTime)
                                               userInfo:nil
                                                repeats:NO];
        }
}

//  4字节时间处理
-(NSDate*)parseBytesToDate:(Byte)b0 b1:(Byte)b1 b2:(Byte)b2 b3:(Byte)b3
{
        int n;
        NSDate *da,*date2000;
        NSDateFormatter *fm;
        
        fm = [[NSDateFormatter alloc]init];
        [fm setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
        date2000 = [fm dateFromString:@"2000-01-01 00:00:00"];
        
        n = b0 | (b1 << 8) | (b2 << 16) | (b3 << 24);
        da = [[NSDate alloc]initWithTimeInterval:n sinceDate:date2000];
        
        return da;
}

//  4字节分计步数据
-(NSString*)parseBytesToOfflineMinuteData:(Byte)b0 b1:(Byte)b1 b2:(Byte)b2 b3:(Byte)b3 data:(NSString*)data
{
        NSString *s;
        int n;
        
        n = b2 | (b3 << 8);
        if (![data isEqualToString:@""])
        {
                s = [NSString stringWithFormat:@"%@,%.2d%.2d:%d", data, b0, b1, n];
        }
        else
        {
                s = [NSString stringWithFormat:@"%.2d%.2d:%d",b0, b1, n];
        }
        
        return s;
}

//  判断4字节是否全0
-(BOOL)checkBytesZero:(Byte)b0 b1:(Byte)b1 b2:(Byte)b2 b3:(Byte)b3
{
        return (b0 == 0) && (b1 == 0) && (b2 == 0) && (b3 == 0);
}

-(void)getDayOfflineData:(Byte)dataCtrl
{
        char data = dataCtrl;
        NSData *d = [[NSData alloc] initWithBytes:&data length:1];
        
        [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID
                                                     serviceUUID:YDSH_TI_KEYFOB_OFFLINE_SERVICE_UUID
                                              characteristicUUID:YDSH_TI_KEYFOB_OFFLINE_SELECT_DAY_PROPERTY_UUID
                                                            data:d];
}

-(void)getMinuteDayOfflineData:(Byte)dataCtrl
{
        char data = dataCtrl;
        NSData *d = [[NSData alloc] initWithBytes:&data length:1];
        
        [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID
                                                     serviceUUID:YDSH_TI_KEYFOB_OFFLINE_SERVICE_UUID
                                              characteristicUUID:YDSH_TI_KEYFOB_OFFLINE_SELECT_MINUTEDAY_PROPERTY_UUID
                                                            data:d];
}

//  ------------------------------------------------------------------
//  协议Protocol_BLEDevice_Pedo_YDSH的实现部分
//  ------------------------------------------------------------------

-(void)BD_enablePedometer:(BOOL)isEnable
{
        char data;
        
        if (isEnable)
        {
                data = 0x01;
        }
        else
        {
                data = 0x00;
        }
        NSData *d = [[NSData alloc] initWithBytes:&data length:1];
        
        [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID
                                                     serviceUUID:YDSH_TI_KEYFOB_PEDOMETER_SERVICE_UUID
                                              characteristicUUID:YDSH_TI_KEYFOB_PEDOMETER_ACCELENABLE_PROPERTY_UUID
                                                            data:d];
        
        [[BLEManager sharedInstance] notification:DeviceID
                                      serviceUUID:YDSH_TI_KEYFOB_PEDOMETER_SERVICE_UUID
                               characteristicUUID:YDSH_TI_KEYFOB_PEDOMETER_COUNTENABLE_PROPERTY_UUID
                                               on:isEnable];
}

-(void)BD_enableBatteryLevel:(BOOL)isEnable
{
        [[BLEManager sharedInstance] notification:DeviceID
                                      serviceUUID:YDSH_TI_KEYFOB_BATT_SERVICE_UUID
                               characteristicUUID:YDSH_TI_KEYFOB_BATT_STATE_PROPERTY_UUID
                                               on:isEnable];
}

-(void)BD_setCurrentTime
{
        NSDate *date2000;
        uint time;
        NSDateFormatter *fm;
        char bytes[YDSH_TI_KEYFOB_TIME_LENGTH];
        
        fm = [[NSDateFormatter alloc]init];
        [fm setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
        date2000 = [fm dateFromString:@"2000-01-01 00:00:00"];
        time = (int)[[NSDate date] timeIntervalSinceDate:date2000];
        if (AntiLost_Debug)
        {
                NSLog(@"---------------%d",time);
        }
        bytes[0] = time;
        bytes[1] = time >> 8;
        bytes[2] = time >> 16;
        bytes[3] = time >> 24;
        
        NSData *d = [[NSData alloc] initWithBytes:bytes length:YDSH_TI_KEYFOB_TIME_LENGTH];
        [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID
                                                     serviceUUID:YDSH_TI_KEYFOB_TIME_SERVICE_UUID
                                              characteristicUUID:YDSH_TI_KEYFOB_TIME_SYNCHRONOUS_PROPERTY_UUID
                                                            data:d];
        
        if (AntiLost_Debug)
        {
                time = (int)[[NSDate date] timeIntervalSinceDate:date2000];
                NSLog(@"---------------%d,%d,%d,%d,%d",
                      time,
                      (Byte)bytes[0],
                      (Byte)bytes[1],
                      (Byte)bytes[2],
                      (Byte)bytes[3]);
        }
}

-(void)BD_getCurrentTime
{
        [[BLEManager sharedInstance] readValueForCharacteristic:DeviceID
                                                    serviceUUID:YDSH_TI_KEYFOB_TIME_SERVICE_UUID
                                             characteristicUUID:YDSH_TI_KEYFOB_TIME_SYNCHRONOUS_PROPERTY_UUID];
}

-(void)BD_readFirmwareVersion
{
        [[BLEManager sharedInstance] readValueForCharacteristic:DeviceID
                                                    serviceUUID:YDSH_TI_KEYFOB_VERSION_SERVICE_UUID
                                             characteristicUUID:YDSH_TI_KEYFOB_PROTOCOL_VERSION_PROPERTY_UUID];
}

//------------天数据----------------
-(void)BD_enableOfflineData_Days:(BOOL)isEnable
{
        [[BLEManager sharedInstance] notification:DeviceID
                                      serviceUUID:YDSH_TI_KEYFOB_OFFLINE_SERVICE_UUID
                               characteristicUUID:YDSH_TI_KEYFOB_OFFLINE_DATA_DAY_PROPERTY_UUID
                                               on:isEnable];
}

-(void)BD_readOfflineData_DaysNumber
{
        [[BLEManager sharedInstance] readValueForCharacteristic:DeviceID
                                                    serviceUUID:YDSH_TI_KEYFOB_OFFLINE_SERVICE_UUID
                                             characteristicUUID:YDSH_TI_KEYFOB_OFFLINE_SELECT_DAY_PROPERTY_UUID];
}

-(void)BD_clearOfflineData_Day
{
        [self getDayOfflineData:0xEE];
}

-(void)BD_getOfflineData_DaysAll
{
        [self getDayOfflineData:0xFF];
}

-(void)BD_getOfflineData_DaysOfIndex:(int)dayIndex
{
        if ((dayIndex > 0) && (dayIndex <= 15))
        {
                [self getDayOfflineData:dayIndex];
        }
}

//------------分数据----------------
-(void)BD_enableOfflineData_Minutes:(BOOL)isEnable
{
        [[BLEManager sharedInstance] notification:DeviceID
                                      serviceUUID:YDSH_TI_KEYFOB_OFFLINE_SERVICE_UUID
                               characteristicUUID:YDSH_TI_KEYFOB_OFFLINE_DATA_MINUTEDAY_PROPERTY_UUID
                                               on:isEnable];
}

-(void)BD_readOfflineData_MinutesDayNumber
{
        [[BLEManager sharedInstance] readValueForCharacteristic:DeviceID
                                                    serviceUUID:YDSH_TI_KEYFOB_OFFLINE_SERVICE_UUID
                                             characteristicUUID:YDSH_TI_KEYFOB_OFFLINE_SELECT_MINUTEDAY_PROPERTY_UUID];
}

-(void)BD_clearOfflineData_Minutes
{
        [self getMinuteDayOfflineData:0xEE];
}

-(void)BD_getOfflineData_MinutesAllDay
{
        [self getMinuteDayOfflineData:0xFF];
}

-(void)BD_getOfflineData_MinutesOfDayIndex:(int)dayIndex
{
        if ((dayIndex >= 0) && (dayIndex <= 15))
        {
                [self getMinuteDayOfflineData:dayIndex];
        }
}
@end
