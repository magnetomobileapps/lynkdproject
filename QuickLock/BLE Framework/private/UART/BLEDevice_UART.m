//
//  BLEDevice_UART.m
//
//  ------------------------------------------------------------------
//  BLE透传子类的实现部分
//  以后通过BLE透传方式的类都继承自此子类，不必再写数据接收和发送，仅仅调用相应的虚函数就可以
//  ------------------------------------------------------------------
//
//  ------------------------------------------------------------------
//  BLE透传子类的常量定义
//  ------------------------------------------------------------------

#ifndef UART_BC_ItonBLEDefines_h
#define UART_BC_ItonBLEDefines_h

#define BC_KEYFOB_CUSTOMER_SERVICE_UUID            0xFF00
#define BC_KEYFOB_CUSTOMER_SEND_PROPERTY_UUID      0xFF01
#define BC_KEYFOB_CUSTOMER_RECV_PROPERTY_UUID      0xFF02
#define BC_KEYFOB_CUSTOMER_RECV_LENGTH             20

#endif

#import "BLEDevice_UART.h"
#import "BLEManager.h"

@implementation BLEDevice_UART
@synthesize delegateBLEDeviceResult_UART;

-(id)init:(id)delegete
{
        [self setDelegateBLEDeviceResult_UART:delegete];
        return [super init:delegete];
}

//  ------------------------------------------------------------------
//  内部私有函数部分
//  ------------------------------------------------------------------

//  硬件数据接收函数 - 重载具体实现部分，每个子类不同
- (void)didUpdateValueForCharacteristic_override:(CBCharacteristic *)characteristic
{
        UInt16 characteristicUUID = [BLEPublic CBUUIDToInt:characteristic.UUID];
        switch(characteristicUUID)
        {
                //  数据接收
                case BC_KEYFOB_CUSTOMER_RECV_PROPERTY_UUID:
                {
                        int nLength = characteristic.value.length;
                        Byte bAddress[nLength];
                        [characteristic.value getBytes:&bAddress length:nLength];

                        [self processReceiveData_override:bAddress length:nLength recvTime:[NSDate date]];
                        break;
                }
                default:
                {
                        if(AntiLost_Debug)
                        {
                                NSLog(@"didUpdateValueForCharacteristic --- default --- characteristicUUID:%d" , characteristicUUID);
                        }
                        break;
                }
        }
}

//  接收透传数据的抽象函数 - 重载具体实现部分，每个子类不同
//  bytes是接收定义的最大长度数组
-(void)processReceiveData_override:(Byte*)bytes length:(int)length recvTime:(NSDate*)recvTime
{
    //接收透传数据的抽象函数 - 重载具体实现部分，每个子类不同
    
    //  如果直接使用基类,则此方法提供直接输出接收到的透传数组内容
    //  如果子类重载此方法,则直接输出无效!
    [delegateBLEDeviceResult_UART BR_receiveBaseData:bytes length:length recvTime:recvTime DeviceID:DeviceID];
}

//  ------------------------------------------------------------------
//  协议Protocol_BLEDevice_UART的实现部分
//  ------------------------------------------------------------------

-(void)BD_enableTransparentData:(BOOL)isEnable
{
        [[BLEManager sharedInstance] notification:DeviceID serviceUUID:BC_KEYFOB_CUSTOMER_SERVICE_UUID characteristicUUID:BC_KEYFOB_CUSTOMER_RECV_PROPERTY_UUID on:isEnable];
}

-(void)BD_sendFullLengthData:(char*)bytes length:(int)length
{
        char chars[BC_KEYFOB_CUSTOMER_RECV_LENGTH] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
        int i;
        
        for (i = 0; i < BC_KEYFOB_CUSTOMER_RECV_LENGTH; i++)
        {
                if (i < length)
                {
                        chars[i] = bytes[i];
                }
        }
        
        NSData *d = [[NSData alloc] initWithBytes:chars length:BC_KEYFOB_CUSTOMER_RECV_LENGTH];
        [[BLEManager sharedInstance] writeValueForCharacteristic:DeviceID serviceUUID:BC_KEYFOB_CUSTOMER_SERVICE_UUID characteristicUUID:BC_KEYFOB_CUSTOMER_SEND_PROPERTY_UUID data:d];
}

-(int)BD_getDataFullLength
{
        return BC_KEYFOB_CUSTOMER_RECV_LENGTH;
}

@end
