//
//  Protocol_BLEDevice_Pedo.h
//
//  ------------------------------------------------------------------
//  计步器子类对外接口
//  ------------------------------------------------------------------

#import <Foundation/Foundation.h>

@protocol Protocol_BLEDevice_Pedo <NSObject>

//  启用/禁用联机计步数据上传
//  isEnable 是否启用
-(void)BD_enablePedometer:(BOOL)isEnable;

//  启用/禁用脱机计步数据上传
//  isEnable 是否启用
-(void)BD_enableOfflineData:(BOOL)isEnable;

//  启用/禁用电量数据上传 2013-11-28
-(void)BD_enableBatteryLevel:(BOOL)isEnable;

//  向设备同步当前系统时间
-(void)BD_setCurrentTime;

//  从设备读取时间
-(void)BD_getCurrentTime;

//  清楚设备脱机计步数据
-(void)BD_clearOfflineData;

//  使计步器设备进入休眠状态
-(void)BD_sleepPeripheral;


@end