//
//  Protocol_BLEDeviceResult_Pedo.h
//
//  ------------------------------------------------------------------
//  计步器子类的对外结果返回
//  ------------------------------------------------------------------

#import <Foundation/Foundation.h>

@protocol Protocol_BLEDeviceResult_Pedo <NSObject>

//  联机计步数据上传更新
-(void)BR_pedoCountUpdated:(int)pValue DeviceID:(int)DeviceID;

//  脱机计步数据上传更新
-(void)BR_pedoReceiveOfflineData:(NSDate *)oValue timerecord:(int)tValue paceCount:(int)pValue DeviceID:(int)DeviceID;

-(void)BR_batteryLevel:(int)battLevel DeviceID:(int)DeviceID;

//  设备时间数据上传
-(void)BR_currentTime:(NSDate*)devTime DeviceID:(int)DeviceID;

@end