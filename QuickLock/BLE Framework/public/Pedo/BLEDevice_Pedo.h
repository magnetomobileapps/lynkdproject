//
//  BLEDevice_Pedo.h
//
//  ------------------------------------------------------------------
//  计步器子类
//  ------------------------------------------------------------------

#import "BLEDevice.h"
#import "Protocol_BLEDevice_Pedo.h"
#import "Protocol_BLEDeviceResult_Pedo.h"

@interface BLEDevice_Pedo : BLEDevice<Protocol_BLEDevice_Pedo>

@property (nonatomic, assign) id<Protocol_BLEDeviceResult_Pedo> delegateBLEDeviceResult_Pedo;

-(id)init:(id)delegete;

@end
