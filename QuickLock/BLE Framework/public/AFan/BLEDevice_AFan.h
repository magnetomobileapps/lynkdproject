//
//  BLEDevice_AFan.h
//
//  ------------------------------------------------------------------
//  艾美特风扇子类
//  ------------------------------------------------------------------
//

#import "BLEDevice.h"
#import "Protocol_BLEDevice_AFan.h"
#import "Protocol_BLEDeviceResult_AFan.h"

@interface BLEDevice_AFan : BLEDevice<Protocol_BLEDevice_AFan>

@property (nonatomic, assign) id<Protocol_BLEDeviceResult_AFan> delegateBLEDeviceResult_AFan;

-(id)init:(id)delegete;

@end
