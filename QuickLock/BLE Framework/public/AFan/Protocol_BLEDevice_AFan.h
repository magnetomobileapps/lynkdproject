//
//  Protocol_BLEDevice_AFan.h
//
//  ------------------------------------------------------------------
//  艾美特风扇子类对外接口
//  ------------------------------------------------------------------

#import <Foundation/Foundation.h>

@protocol Protocol_BLEDevice_AFan <NSObject>

//  启用/禁用数据上传
//  isEnable 是否启用
-(void)BD_enableTransparentData:(BOOL)isEnable;

-(void)BD_writeData:(int)n0 byte1:(int)n1 byte2:(int)closehour byte3:(int)reservehour byte4:(int)n4 byte5:(int)closeminute byte6:(int)reserveminute byte13:(int)n13;

-(void)BD_writeTimeData:(int)b0 byte1:(int)b1 byte2:(int)b2 byte3:(int)b3 byte4:(int)b4 byte5:(int)b5 byte6:(int)b6 byte13:(int)b13;

-(void)BD_writeDefineData:(int)b0 byte1:(int)b1 byte2:(int)b2 byte3:(int)b3 byte4:(int)b4 byte5:(int)b5 byte6:(int)b6 byte7:(int)b7 byte8:(int)b8 byte9:(int)b9 byte10:(int)b10 byte11:(int)b11 byte12:(int)b12 byte13:(int)b13;

@end
