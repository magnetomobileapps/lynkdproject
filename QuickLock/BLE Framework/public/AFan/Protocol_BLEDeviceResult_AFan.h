//
//  Protocol_BLEDeviceResult_AFan.h
//
//  ------------------------------------------------------------------
//  艾美特风扇子类对外接口的结果返回
//  ------------------------------------------------------------------

#import <Foundation/Foundation.h>

@protocol Protocol_BLEDeviceResult_AFan <NSObject>

-(void)BR_receiveBytes:(Byte*)bytes length:(int)length DeviceID:(int)DeviceID;

@end
