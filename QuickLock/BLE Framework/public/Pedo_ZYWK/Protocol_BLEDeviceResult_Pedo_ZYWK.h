//
//  Protocol_BLEDeviceResult_Pedo_ZYWK.h
//
//  ------------------------------------------------------------------
//  计步器子类的对外结果返回 - 博通带屏（不带屏）计步器
//  ------------------------------------------------------------------

#import <Foundation/Foundation.h>

@protocol Protocol_BLEDeviceResult_Pedo_ZYWK <NSObject>

//  联机计步数据上传更新
//  2013-09-16 上传累计步数和累计时间
//  2013-12-17 app显示数据以硬件为准
-(void)BR_pedoCountUpdated_ZYWK:(int)pValue time:(int)tValue calorie:(double)cValue distance:(double)dValue DeviceID:(int)DeviceID;

//  脱机计步数据上传更新
//  2013-12-17 app显示数据以硬件为准
-(void)BR_pedoReceiveOfflineData_ZYWK:(NSDate *)oValue timerecord:(int)tValue paceCount:(int)pValue calorie:(double)cValue distance:(double)dValue DeviceID:(int)DeviceID;

//  启动Camera
//-(void)BR_pedoCamera;   // 2013-09-05 修改为双击使用，仅限打开和关闭摄像头
-(void)BR_snap_ZYWK:(int)DeviceID;           // 2013-09-05 单击拍照
-(void)BR_openCamera_ZYWK:(int)DeviceID;     // 2013-09-06
-(void)BR_closeCamera_ZYWK:(int)DeviceID;    // 2013-09-06

-(void)BR_hardwareVersion_ZYWK:(NSString*)strVersion DeviceID:(int)DeviceID;

-(void)BR_batteryLevel_ZYWK:(int)battLevel DeviceID:(int)DeviceID;

@end