//
//  Protocol_BLEDevice_Pedo_ZYWK.h
//
//  ------------------------------------------------------------------
//  计步器子类对外接口 - 博通带屏（不带屏）计步器
//  增加中盈微科扩展功能：写参数，相机，来电提醒
//  ------------------------------------------------------------------

#import <Foundation/Foundation.h>

@protocol Protocol_BLEDevice_Pedo_ZYWK <NSObject>

//  启用/禁用联机计步数据上传
//  isEnable 是否启用
-(void)BD_enablePedometer:(BOOL)isEnable;

//  启用/禁用脱机计步数据上传
//  isEnable 是否启用
-(void)BD_enableOfflineData:(BOOL)isEnable;

//  启用/禁用电量上传 2013-11-28
-(void)BD_enableBatteryLevel_ZYWK:(BOOL)isEnable;

//  向设备同步当前系统时间
-(void)BD_setCurrentTime;

//  从设备读取时间
-(void)BD_getCurrentTime;

//  清除设备脱机计步数据
-(void)BD_clearOfflineData;

//  清除设备数据
-(void)BD_clearPeripheralData;

//  向设备同步参数
//  2013-08-05
-(void)BD_setParameter:(int)units height:(double)height weight:(double)weight stride:(double)stride nAlarmKind:(int)nAlarmKind tCalories:(double)tCalories tDistance:(double)tDistance tPaceCount:(double)tPaceCount tCostTime:(double)tCostTime;

//  启用/禁用camera功能
//  isEnable 是否启用
-(void)BD_enableCamera:(BOOL)isEnable;

//  通知硬件是否有来电
//  isRead 为false表示有未读来电 true表示已处理
//  2013-08-05 by wangyi
-(void)BD_comingCall:(BOOL)isRead;

//  启用/禁用版本号读取功能
//  读取到的版本号，用来区分计步器的种类
-(void)BD_enableHardwareVersion:(BOOL)isEnable;

//  从硬件读取版本号
-(void)BD_getHardwareVersion;

//  START之前读取计步器数据
-(void)BD_getAccumulationData;


@end
