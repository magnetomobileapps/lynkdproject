//
//  BLEDevice_Pedo_ZYWK.h
//
//  ------------------------------------------------------------------
//  计步器子类 - 博通带屏（不带屏）计步器
//  ------------------------------------------------------------------

#import "BLEDevice.h"
#import "Protocol_BLEDevice_Pedo_ZYWK.h"
#import "Protocol_BLEDeviceResult_Pedo_ZYWK.h"

@interface BLEDevice_Pedo_ZYWK : BLEDevice<Protocol_BLEDevice_Pedo_ZYWK>

@property (nonatomic, assign) id<Protocol_BLEDeviceResult_Pedo_ZYWK> delegateBLEDeviceResult_Pedo_ZYWK;

-(id)init:(id)delegete;

@end
