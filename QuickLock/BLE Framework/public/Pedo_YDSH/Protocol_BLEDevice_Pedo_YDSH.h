//
//  Protocol_BLEDevice_Pedo_YDSH.h
//
//  Copyright (c) 2014, Vicro Technology Limited. All rights reserved.
//
//  ------------------------------------------------------------------
//  New Pedometer Interface
//  ------------------------------------------------------------------

#import <Foundation/Foundation.h>

@protocol Protocol_BLEDevice_Pedo_YDSH <NSObject>

//  Start/Stop online step count date upload
//  when "isEnable" is "yes"，when the step-count varies，pedometer will through the Callback Function "BR_pedoCountUpdated" real-time upload automatically
//  If online step count is no need，then set "isEnable" to "no"
//  This function has no result return
//  isEnable : yes - Start, no - stop
//  Reference：BR_pedoCountUpdated
-(void)BD_enablePedometer:(BOOL)isEnable;

//  Sync the current time of mobile phone with pedometer's
//  This function has no result return
-(void)BD_setCurrentTime;

//  Read the current time of pedometer,its time will upload through the Callback Function "BR_currentTime"
//  Reference：BR_currentTime
-(void)BD_getCurrentTime;

//  Start/Stop pedometer battery level data upload
//  When "isEnable" is "yes"，pedometer battery level will upload automatically through "BR_batteryLevel"，one time per second
//  If battery level data upload is no need，then set "isEnable" to "no"
//  isEnable : yes - Start, no - stop
//  Reference：BR_batteryLevel
-(void)BD_enableBatteryLevel:(BOOL)isEnable;

//  Read pedometer firmware version number，version number will upload through the Callback Function "BR_FirmwareVersion"
//  The current firmware version number should be“50”，if the version number is different，may the funtion be different either！
//  Reference：BR_FirmwareVersion
-(void)BD_readFirmwareVersion;

//  -------------------Function of [offline everyday step-count data] ---------------------------------------

//  Start/Stop upload offline everyday step-count data
//  When "isEnable" is "yes"，offline everyday step-count data will be allowed to upload，or is not allowed to do
//  When "isEnable" is "yes"，it can start uploading offline everyday step-count datathrough function "BD_getOfflineData_DaysAll" or "BD_getOfflineData_DaysOfIndex"
//  This function has no result return
//  isEnable : yes - start, no - stop
//  Reference：BD_getOfflineData_DaysAll,BD_getOfflineData_DaysOfIndex
-(void)BD_enableOfflineData_Days:(BOOL)isEnable;

//  Read the amount of offline everyday step-count data in the pedometer（maximum is 15, namely maximum 15 days' history data)
//  The result can be used to estimate the completeness of received data，or to invoke the fuction "BD_getOfflineData_DaysOfIndex" to read offline everyday step-count data seperately
//  The result returns thourgh "BR_OfflineData_DaysNumber"
//  Reference：BR_OfflineData_DaysNumber
-(void)BD_readOfflineData_DaysNumber;

//  Read all the offline step-count data
//  Before invoking this function，first set "BD_enableOfflineData_Days"' "isEnable" to "yes"
//  The data returns through "BR_pedoReceiveOfflineData_Days"
//  Reference：BD_enableOfflineData_Days,BR_pedoReceiveOfflineData_Days
-(void)BD_getOfflineData_DaysAll;

//  Read the special day's offline everyday step-count data
//  Before invoking this function，first set "BD_enableOfflineData_Days" "isEnable" to "yes"
//  The data returns through "BR_pedoReceiveOfflineData_Days"
//  This function is not used normally（the normal function is "BD_getOfflineData_DaysAll"）
//  dayIndex : specify read the special day index（1-15，maximum is 15，1 indicates yesterday，15 indicates the fifteenth day before today）
//  Reference：BD_enableOfflineData_Days,BR_pedoReceiveOfflineData_Days,BD_getOfflineData_DaysAll
-(void)BD_getOfflineData_DaysOfIndex:(int)dayIndex;

//  Remove all offline everyday step-count data
//  Invoking this function will make the offline everyday step-count data removed！
//  If do not invoke this function，maximum 15 days'(start from yesterday) offline everyday step-count history data will be kept in pedometer
//  So normally，after confirmed the history data had read by APP，it needs to invoke this fuction to remove，or may APP read hostory data repeatly
//  This function has no result return
-(void)BD_clearOfflineData_Day;

//---------------------Function of [Offline per 5 minutes step-count  data]-------------------------------

//  Start/Stop uopload offline per 5 minutes step-count data
//  When "isEnable" is "yes"，it is allowed to upload offline per 5 minutes step-count data，or not be allowed
//  When "isEnable" is "yes"，it can be start uploading offline per 5 minutes step-count data through the function "BD_getOfflineData_MinutesAllDay" or "BD_getOfflineData_MinutesOfDayIndex"
//  This function has no result return
//  isEnable : yes - start, no - stop
//  Reference：BD_getOfflineData_MinutesAllDay,BD_getOfflineData_MinutesOfDayIndex
-(void)BD_enableOfflineData_Minutes:(BOOL)isEnable;

//  Read number of days for offline per 5 minutes step-count data（Maximum 16, namely maximum 16 days' history data, including that day）
//  The result can be used to estimate the completeness of received data，or to invoke the fuction "BD_getOfflineData_DaysOfIndex" to read the special day's offline per 5 minutes step-count data seperately
//  The data returns through "BR_OfflineData_MinutesDayNumber"
//  Reference：BR_OfflineData_MinutesDayNumber
-(void)BD_readOfflineData_MinutesDayNumber;

//  Read all offline per 5 minutes step-count data
//  Before invoking this function，first set "BD_enableOfflineData_Minutes" "isEnable" to "yes"
//  The data returns through "BR_pedoReceiveOfflineData_Minutes"
//  Reference：BD_enableOfflineData_Minutes,BR_pedoReceiveOfflineData_Minutes
-(void)BD_getOfflineData_MinutesAllDay;

//  Read specified day's offline per 5 minutes step-count data
//  Before invoking this function，first set "BD_enableOfflineData_Minutes" "isEnable" to "yes"
//  The data returns through "BR_pedoReceiveOfflineData_Minutes"
//  This function is not used normally（the normal function is "BD_getOfflineData_MinutesAllDay"）
//  dayIndex : specify read the special day index（1-15，maximum is 15，0 indicates today, 1 indicates yesterday，15 indicates the fifteenth day before today）
//  Reference：BD_enableOfflineData_Minutes,BR_pedoReceiveOfflineData_Minutes,BD_getOfflineData_MinutesAllDay
-(void)BD_getOfflineData_MinutesOfDayIndex:(int)dayIndex;

//  Remove all offline per 5 minutes step-count data
//  Invoking this function will make the offline per minute step-count data removed！
//   If do not invoke this function，maximum 16 days'(start from today) offline per minute step-count history data will be kept in pedometer
//  So normally，after confirmed the history data had read by APP，it needs to invoke this fuction to remove，or may APP read hostory data repeatly
//  This function has no result return
-(void)BD_clearOfflineData_Minutes;

//------------------------------------------------------------------------------

@end
