//
//  BLEDevice_Pedo_YDSH.h
//
//  Copyright (c) 2014, Vicro Technology Limited. All rights reserved.
//
//  ------------------------------------------------------------------
//  the new pedometer class
//  ------------------------------------------------------------------

#import "BLEDevice.h"
#import "Protocol_BLEDevice_Pedo_YDSH.h"
#import "Protocol_BLEDeviceResult_Pedo_YDSH.h"

@interface BLEDevice_Pedo_YDSH : BLEDevice<Protocol_BLEDevice_Pedo_YDSH>

@property (nonatomic, assign) id<Protocol_BLEDeviceResult_Pedo_YDSH> delegateBLEDeviceResult_Pedo_YDSH;

//  init
//  delegete : self
-(id)init:(id)delegete;

@end
