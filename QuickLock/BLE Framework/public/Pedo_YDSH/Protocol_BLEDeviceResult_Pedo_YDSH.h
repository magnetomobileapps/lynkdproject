//
//  Protocol_BLEDeviceResult_Pedo_YDSH.h
//
//  Copyright (c) 2014, Vicro Technology Limited. All rights reserved.
//
//  ------------------------------------------------------------------
//  New Pedometer Callback Function
//  ------------------------------------------------------------------

#import <Foundation/Foundation.h>

@protocol Protocol_BLEDeviceResult_Pedo_YDSH <NSObject>

//  Pedometer firmware version number upload
//  ProtocolVersion : Pedometer firmware version number
//  Reference：BD_readHardwareVersion
-(void)BR_FirmwareVersion:(NSString*)ProtocolVersion DeviceID:(int)DeviceID;

//  Pedometer's current time upload
//  devTime : Pedometer's current time
//  Reference：BD_getCurrentTime
-(void)BR_currentTime:(NSDate*)devTime DeviceID:(int)DeviceID;

//  Pedometer battery level upload，upload interval time：1 second
//  battLevel : battery level，0-100（percentage，for example 50 indicates battery level is 50% ）
//  Reference：BD_enableBatteryLevel
-(void)BR_batteryLevel:(int)battLevel DeviceID:(int)DeviceID;

//  Online step-count data upload
//  When step-count data is changing，it will be uploaded automatically（need set "BD_enablePedometer' "isEnable" to "yes"）
//  pValue : Step-count value，it is an accumulated value，indicates the maximum steps quantity from that day to now，so the steps quantity of this time = current time steps quantity - last time 's steps quantity
//  Reference：BD_enablePedometer
-(void)BR_pedoCountUpdated:(int)pValue DeviceID:(int)DeviceID;

//  -------------------Function of [Offline everyday step-count data]---------------------------------------

//  The number of days of offline everyday step-count data upload
//  daysNumber : Total number of days' offline everyday step-count data（maximum is 15, namely maximum 15 days'(start from yesterday) history data）
//  Reference：BD_readOfflineData_DaysNumber
-(void)BR_OfflineData_DaysNumber:(int)daysNumber DeviceID:(int)DeviceID;

//  offline everyday step-count data
//  Upload automatically after invoking the function "BD_getOfflineData_DaysAll" or "BD_getOfflineData_DaysOfIndex"
//  Upload a day's data everytime，maximum 15 days，upload interval time is about 50ms
//  oValue : Date of offline data;  tValue : the total step-count time of that day（second）; pValue : The total steps number at that day
//  Reference：BD_getOfflineData_DaysAll,BD_getOfflineData_DaysOfIndex
-(void)BR_pedoReceiveOfflineData_Days:(NSDate *)oValue timerecord:(int)tValue paceCount:(int)pValue DeviceID:(int)DeviceID;

//---------------------Function of [Offline per 5 minutes step-count  data]-------------------------------

//  The number of days of offline per 5 minutes step-count data upload
//  daysNumber : the total number of days of offline per 5 minutes step-count data（Maximum is 15,namely maximum 16 days'（start from today）history data）
//  Reference：BD_readOfflineData_MinutesDayNumber
-(void)BR_OfflineData_MinutesDayNumber:(int)daysNumber DeviceID:(int)DeviceID;

//  Offline per 5 minutes step-count data upload
//  Upload automatically after invoking the function "BD_getOfflineData_MinutesAllDay" or "BD_getOfflineData_MinutesOfDayIndex"
//  Upload a day's per 5 minutes data everytime，maximum 16 days，the upload interval time of everyday data is between 50ms-8.8s（That's because that per 5 minutes data of a day may as big as 5K Bytes or bigger，it needs some time to transfer，if everyday step-count time is an hour, then the time of data uploading for that day is about 400ms）
//  oValue : Date of offline data;
//  dayMinutesRecord : The collection of all offline per 5 minutes step-count data in that day（NSString）
//       data format : "HHmm:totalCount,HHmm:totalCount,...(etc)". (HHmm：indicates hour and minute ；totalCount：indicates at the end of this minute, the total accumulated steps number )
//           EXAMPLE : "0001:1,...,1200:30000,...,2359:60000" ha
//           meaning : 00:01 the total steps number in that day is 1 step; ... 12:00 the total steps number in that day is 30000 steps; ... 23:59 the total steps number in that day is  60000 steps
//  recordCount : The total number of per 5 minutes' data in that day
//  Reference：BD_getOfflineData_MinutesAllDay,BD_getOfflineData_MinutesOfDayIndex
-(void)BR_pedoReceiveOfflineData_Minutes:(NSDate *)oValue dayMinutesRecord:(NSString *)dayMinutesRecord recordCount:(int)recordCount DeviceID:(int)DeviceID;
//------------------------------------------------------------------------------

@end
