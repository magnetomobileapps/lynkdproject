//
//  Protocol_BLEDeviceResult_QuickLock.h
//
//  ------------------------------------------------------------------
//  车锁子类外接口的结果返回
//  ------------------------------------------------------------------

#import <Foundation/Foundation.h>

@protocol Protocol_BLEDeviceResult_QuickLock <NSObject>

// Upload SystemID
// SystemID: 8-byte string in hexadecimal representation, each number separated by space
- (void) BR_systemID: (NSString *) systemID DeviceID: (int) DeviceID;

// Upload ModelNumber
// ModelNumber: 16-bit string
- (void) BR_modelNumber: (NSString *) modelNumber DeviceID: (int) DeviceID;

- (void) BR_fingerprintCount: (Byte) count DeviceID: (int) DeviceID;

- (void) BR_alarm_count:(Byte)state  delay:(Byte)delay DeviceID:(int)DeviceID;

- (void) BR_Zwave:(Byte)zwave DeviceID:(int)DeviceID;

// Upload firmware version
// Vision: firmware version number
// VisionDate: Firmware Version Time
- (void) BR_firmwareRevision: (NSString *) vision visionDate: (NSString *) visionDate DeviceID: (int) DeviceID;

// Upload GATT protocol version number
// ProtocolVersion
- (void) BR_protocolVersion: (int) lockID protocolVersion: (NSString *) protocolVersion DeviceID: (int) DeviceID;

// Upload battery power (when the power changes will automatically upload)
// BattLevel: battery power, 0-100 (expressed as a percentage of battery power)
- (void) BR_batteryLevel: (int) battLevel DeviceID: (int) DeviceID;

// Upload hardware time
// DevTime: hardware time valueBR_historyLogBR_historyLog
- (void) BR_currentTime: (NSDate *) devTime DeviceID: (int) DeviceID;

// Upload password status
// IsModify: whether to change the password status, if it is false, which means that a password state
// IsValidPassword: the password is correct
- (void) BR_PasswordResult: (BOOL) isModify isValidPassword: (BOOL) isValidPassword DeviceID: (int) DeviceID;

// Upload lock status
// IsPasswordRight: the password is correct, isLockOpened: lock is already open
- (void) BR_lockState: (BOOL) isLockOpened DeviceID: (int) DeviceID;

// Upload lock open time
// LockOpenSecond: lock open time, seconds
- (void) BR_lockOpenTime: (Byte) lockOpenSecond DeviceID: (int) DeviceID;

// Username Upload
// UserName: user name
- (void) BR_userName: (NSString *) userName DeviceID: (int) DeviceID;

// Upload unlock records
// DevType: unlock the device type 0 - NFC 1 - Phone
// IdOrName: unlock the device ID or name
// LogDate: unlocking time
-(void)BR_historyLog:(int)devType idOrName:(NSString*)idOrName logDate:(NSDate*)logDate isOpenLock:(BOOL)isOpenLock DeviceID:(int)DeviceID;

-(void)BR_getPasswordSharecode:(int)userCount shareCode:(NSString*)shareCode;

-(void)BR_getPasswordSharecodeTimeBase:(NSDate *)startDate endDate:(NSDate *)endDate shareCode:(NSString*)shareCode time1:(NSString *)time1 time2:(NSString *)time2 flag:(int)flag;

@end
