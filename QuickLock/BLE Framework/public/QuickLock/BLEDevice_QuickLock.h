//
//  BLEDevice_QuickLock.h
//
//  ------------------------------------------------------------------
//  车锁子类
//  ------------------------------------------------------------------
//

#import "BLEDevice.h"
#import "Protocol_BLEDevice_QuickLock.h"
#import "Protocol_BLEDeviceResult_QuickLock.h"

@interface BLEDevice_QuickLock : BLEDevice<Protocol_BLEDevice_QuickLock>

@property (nonatomic, assign)id<Protocol_BLEDeviceResult_QuickLock> delegateBLEDeviceResult_QuickLock;

-(id)init:(id)delegete;

@end
