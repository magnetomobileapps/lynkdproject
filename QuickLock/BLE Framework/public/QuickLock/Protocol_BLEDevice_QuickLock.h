//
//  Protocol_BLEDevice_QuickLock.h
//
//  ------------------------------------------------------------------
//  车锁子类对外接口
//  ------------------------------------------------------------------

#import <Foundation/Foundation.h>

@protocol Protocol_BLEDevice_QuickLock <NSObject>

//  读取SystemID
//  读取到的SystemID值，通过BR_systemID返回
-(void)BD_getSystemID;

//  自动上传的用户ID，通过BR_modelNumber返回
//  isEnable : 是否允许用户ID自动上传, true - 允许上传, false - 禁止上传
-(void)BD_enableModelNumber:(BOOL)isEnable;

//  读取ModelNumber
//  读取到的Model Number值，通过BR_modelNumber返回
-(void)BD_getModelNumber;

//  设置ModelNumber
//  modelNumber:16个字节的字符串，用十六进制表示，每个数字用空格分隔，如“0F 8A BC 00”
-(void)BD_setModelNumber:(NSString *)modelNumber;

-(void)BD_setPassCode:(NSString *)passCode;

//  读取固件版本
//  读取到的固件版本值，通过BR_firmwareRevision返回
-(void)BD_getFirmwareRevision;

//  读取GATT通讯协议版本号
//  读取到的通讯协议版本值，通过BR_protocolVersion返回
-(void)BD_getProtocolVersion;

//  读取电池电量
//  读取到的电池电量值，通过BR_betteryLevel返回
-(void)BD_getBetteryLevel;

-(void)BD_getZwave;

//  电池电量自动上传使能
//  如果使能为true,则电量每一秒钟自动上传一次（作为心跳包）
//  自动上传的电池电量值，通过BR_betteryLevel返回
//  isEnable : 是否允许电池电量自动上传, true - 允许上传, false - 禁止上传
-(void)BD_enableBatteryLevel:(BOOL)isEnable;

//  读取当前硬件内的时间
//  读取到的硬件时间值，通过BR_currentTime返回
-(void)BD_getCurrentTime;

//  设置当前系统时间到硬件
-(void)BD_setCurrentTime;

//  硬件时间自动上传使能
//  如果使能为true，则硬件每秒自动上传一次硬件时间
//  自动上传的硬件时间值，通过BR_currentTime返回
//  isEnable : 是否允许电池电量自动上传, true - 允许上传, false - 禁止上传
-(void)BD_enableTimeUpdate:(BOOL)isEnable;

//  验证密码
//  密码的有效性会通过BR_PasswordResult返回
-(void)BD_setVerifyPassword:(Byte)password1 password2:(Byte)password2 password3:(Byte)password3 password4:(Byte)password4;

//  修改密码，参数包括旧密码和新密码
-(void)BD_setModifyPassword:(Byte)old1 oldPassword2:(Byte)old2 oldPassword3:(Byte)old3 oldPassword4:(Byte)old4 newPassword1:(Byte)new1 newPassword2:(Byte)new2 newPassword3:(Byte)new3 newPassword4:(Byte)new4;

//  读取锁密码有效性
//  读取到的锁状态通过BR_PasswordResult返回
-(void)BD_getPasswordResult;

//  锁密码有效性自动上传使能
//  如果使能为true，则当输入密码的时候会自动上传当前的密码有效性
//  isEnable : 是否允许锁状态自动上传, true - 允许上传, false - 禁止上传
-(void)BD_enablePasswordResultUpdate:(BOOL)isEnable;

-(void)BD_enableShareCode:(BOOL)isEnable;
//  锁控制
//  isLockOpen : 是否开锁, true - 开锁, false - 关锁
-(void)BD_setLockControl:(BOOL)isLockOpen;

//  读取锁状态
//  读取到的锁状态通过BR_lockState返回
-(void)BD_getLockState;

//  锁状态自动上传使能
//  如果使能为true，则当状态改变的时候会自动上传当前的锁状态
//  isEnable : 是否允许锁状态自动上传, true - 允许上传, false - 禁止上传
-(void)BD_enableLockStateUpdate:(BOOL)isEnable;

//  读取锁打开的时间（秒）
//  读取到的锁打开时间通过BR_lockOpenTime上传
-(void)BD_getLockOpenTime;

-(void)BD_getFingerprintCount;

-(void)BD_getAlarmState;

//  设置锁打开的时间（秒）
//  openSecond : 锁打开的时间，秒
-(void)BD_setLockOpenTime:(Byte)lockOpenSecond;

-(void)BD_setLEDTime:(Byte)LEDSecond;

-(void)BD_setSureSet:(Byte)percent;

-(void)BD_setAlarm:(Byte)type delay:(Byte)delay;

-(void)BD_setZwave:(Byte)param;

-(void)BD_setClearAllFingerPrints;

//  设置用户名
//  userName : 用户名，只能是英文、数字、标点符号
-(void)BD_setUserName:(NSString*)userName;

//  读取用户名
//  读取到的用户名，通过BR_userName上传
-(void)BD_getUserName;
-(void)BD_getWifiMacAddress;

//  历史记录自动上传使能
//  如果使能为true，则当读取历史记录的时候，会自动上传
//  isEnable : 是否允许历史记录自动上传, true - 允许上传, false - 禁止上传
-(void)BD_enableHistoryLogUpdate:(BOOL)isEnable;

-(void)BD_enableZwave:(BOOL)isEnable;

-(void)BD_enableRFIDListUpdate:(BOOL)isEnable;

-(void)BD_enableRFIDAdd:(BOOL)isEnable;

//  读取历史记录
//  读取到的历史，通过BR_historyLog上传
-(void)BD_getHistoryLog;

-(void)BD_getPasswordSharecode;
-(void)BD_getPasswordSharecodetimeBase;

-(void)BD_deleteRFID:(Byte)RFID1 RFID2:(Byte)RFID2 RFID3:(Byte)RFID3 RFID4:(Byte)RFID4 RFID5:(Byte)RFID5 RFID6:(Byte)RFID6 RFID7:(Byte)RFID7;

-(void)BD_share_Code_Count:(Byte)CODE1 CODE2:(Byte)CODE2 CODE3:(Byte)CODE3 CODE4:(Byte)CODE4 CODECOUNT:(Byte)CODECOUNT;

-(void)BD_share_Code_Time:(Byte)CODE1 CODE2:(Byte)CODE2 CODE3:(Byte)CODE3 CODE4:(Byte)CODE4;

// Get RFID List
-(void)BD_getRFIDList;
-(void)BD_getRFIDAdd;

-(void)BD_setUserIDForRFIDCard_UserId:(NSString*)strUserId;
-(void)BD_setRFIDWriter;
-(void)BD_getAllRFID;

// fingerprint
-(void)BD_getFingerprintLearn;
-(void)BD_getCancelFingerprintLearn;

-(void)BD_enableFingerprintLearn:(BOOL)isEnable;

@end
