//
//  Protocol_BLEDeviceResult_Pedo_Belter.h
//
//  ------------------------------------------------------------------
//  计步器子类的对外结果返回 - 倍泰带屏计步器
//  ------------------------------------------------------------------

#import <Foundation/Foundation.h>

@protocol Protocol_BLEDeviceResult_Pedo_Belter <NSObject>

-(void)pedoCountUpdated:(int)pValue DeviceID:(int)DeviceID;

-(void)pedoReceiveOfflineData:(NSDate *)oValue timerecord:(int)tValue paceCount:(int)pValue paceConuntO2:(int)pValueO2 DeviceID:(int)DeviceID;

//  获取睡眠监控数据包个数
-(void)getSleepMonitorDataLength:(int)len DeviceID:(int)DeviceID;

//  获取睡眠监控数据
-(void)getSleepMonitorDataInfo:(NSDate *)bValue weakUp:(NSDate *)uValue sleepRecord:(int[])sValue sourceData:(NSString *)sourceData DeviceID:(int)DeviceID;

@end
