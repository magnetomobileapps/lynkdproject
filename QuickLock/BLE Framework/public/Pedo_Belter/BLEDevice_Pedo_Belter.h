//
//  BLEDevice_Pedo_Belter.h
//
//  ------------------------------------------------------------------
//  计步器子类 - 倍泰带屏计步器
//  ------------------------------------------------------------------

#import "BLEDevice.h"
#import "Protocol_BLEDevice_Pedo_Belter.h"
#import "Protocol_BLEDeviceResult_Pedo_Belter.h"

@interface BLEDevice_Pedo_Belter : BLEDevice<Protocol_BLEDevice_Pedo_Belter>

@property (nonatomic, assign) id<Protocol_BLEDeviceResult_Pedo_Belter> delegateBLEDeviceResult_Pedo_Belter;

-(id)init:(id)delegete;

@end