//
//  Protocol_BLEDevice_Pedo_Belter.h
//
//  ------------------------------------------------------------------
//  计步器子类对外接口 - 倍泰带屏计步器
//  ------------------------------------------------------------------

#import <Foundation/Foundation.h>

@protocol Protocol_BLEDevice_Pedo_Belter <NSObject>

-(void)enablePedometer:(BOOL)isEnable;

-(void)enableOfflineData:(BOOL)isEnable;

- (void)enableOfflineDataO2:(BOOL)isEnable;

-(void)enableSleepMonitorDataLength;

-(void)enableSleepMonitorDataInfo:(BOOL)isEnable;

-(void)setCurrentTime;

-(void)getCurrentTime;

-(void)clearOfflineData;

//-(void)sleepPeripheral;

//  设置个人参数-身高
-(void)setUserHeight:(int)value;

//  设置个人参数－体重
-(void)setUserWeight:(int)value;

//  设置个人参数－步长
-(void)setUserStep:(int)value;

//  向设备同步参数（老设置函数不变，保持兼容）
//  内容定义参见通讯协议文档《Belter_UUID_1103（增加公英制）.xls》
//  2014-04-22
-(void)BD_setParameter:(int)units sex:(int)sex age:(int)age height:(double)height weight:(double)weight stride:(double)stride nAlarmKind:(int)nAlarmKind tCalories:(double)tCalories tDistance:(double)tDistance tPaceCount:(double)tPaceCount tCostTime:(double)tCostTime;

@end

