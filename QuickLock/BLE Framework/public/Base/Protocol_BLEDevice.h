//
//  Protocol_BLEDevice.h
//
//  ------------------------------------------------------------------
//  BLEDevice类对外操作的基础接口
//  负责连接和断开等基本BLE操作内容，
//  不直接使用，仅作为不同的BLE子接口的父接口存在
//  ------------------------------------------------------------------

@protocol Protocol_BLEDevice <NSObject>

//  初始化连接一个BLE设备，连接的时候会根据初始化的参数进行选择性连接
//  如果UUID存在，则以UUID为准，查找新设备（serviceUUID和localName此时可以为空）
//  如果UUID为空，则以serviceUUID和localName为准，查找新设备
//  如果UUID存在，并且查找不到设备，则查找已经连接的设备中是否有该UUID存在
//  连接操作开始，返回true；连接操作失败（如底层连接操作正在被其他设备占用时），则返回false
//  连接操作的结果通过Protocol_BLEDeviceConnectResult委托回调
-(BOOL)BD_initConnect:(NSString*)UUID serviceUUID:(NSString*)serviceUUID localName:(NSString*)localName;

//  初始化连接一个BLE设备，连接的时候会根据serviceUUID和指定的多个localName(允许部分匹配)进行选择性连接
//  localNames是指定的多个localName列表; isRanges是指定的多个localName的是否需要部分匹配的列表(1-部分匹配,0-完全匹配)
-(BOOL)BD_initConnectWithNameOfRanges:(NSString*)serviceUUID localNames:(NSArray*)localNames isRanges:(NSArray*)isRanges;

//  初始化连接当前搜索状态下指定搜索顺序的设备
//  scanIndex 通过BD_getScanDeviceList返回的搜索设备结果列表中的索引
//  注意：因为不同时刻下，搜索结果的可变动性，尽量不要使用此方法直接连接设备，以免连接不到，或者连接到其他类型的BLE设备上！
//       在搜索设备结果列表中，如果列出了设备UUID，请尽量通过UUID进行连接！
-(BOOL)BD_initConnectWithScanIndex:(int)scanIndex;

//  断开设备
//  断开操作的结果通过Protocol_BLEDeviceConnectResult委托回调
-(BOOL)BD_disConnect;

//  查询设备当前是否连接
//  查询结果直接返回
-(BOOL)BD_getIsConnected;

//  读取RSSI值
//  结果通过Protocol_BLEDeviceConnectResult委托回调
-(BOOL)BD_readRSSI;

//  读取设备UUID值
-(NSString *)BD_getDeviceUUID;

//  获取当前FrameWork的版本号
-(NSString *)BD_getFrameWorkVersion;

//  检查某个service和characteristic是否存在
-(BOOL)BD_checkExistServiceUUID:(NSString*)serviceUUID characteristicUUID:(NSString*)characteristicUUID;

//  启动搜索，并返回搜索设备列表
//  scanSecond scan秒数
//  scan结果通过Protocol_BLEDeviceConnectResult委托回调
-(BOOL)BD_getScanDeviceList:(float)scanTime;

//  读取硬件蓝牙地址
-(BOOL)BD_getMACAddress;

//  读取BLEDevice的唯一标识（仅在程序运行期间有效，下次程序启动后会发生改变，不可持久化保存！）
//  如果试图在同一个地方使用多个BLEDevice，需要通过此DeviceID进行区分！
-(int)BD_DeviceID;

// ISW HONESTSKY 2014-12-19 TEST 应对连接闪退
//  停止所有的搜索过程，和搜索结果返回
-(void)BD_stopAllScan;

// ISW HONESTSKY
//  读取系统蓝牙是否打开
-(BOOL)BD_getBLEState;

@end
