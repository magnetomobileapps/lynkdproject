//
//  BLEPublic.h
//
//  ------------------------------------------------------------------
//  公共定义类，无需初始化，静态使用
//  各种公共常量、公共函数的集合
//  ------------------------------------------------------------------

//  ------------------------------------------------------------------
//  是否开启debug调试输出信息的开关：0 - 否； 1 - 是
//  注意：
//      在软件提交AppStore前，或者封装为框架供外部程序使用时，必须检查是否关闭！
//      否则生成的App将有闪退或者崩溃的可能，会导致无法通过上架审核！！！
//  ------------------------------------------------------------------
#ifndef AntiLost_Debug
#define AntiLost_Debug 1

#endif

#import <CoreBluetooth/CoreBluetooth.h>

//  ------------------------------------------------------------------
//  BLEDevice_YDSH类定义的数据结构
//  ------------------------------------------------------------------

//  闹铃类型枚举
typedef enum
{
        eColckTypeNone = 0,
        eColckTypeTypeA = 1,
        eColckTypeTypeB = 2,
        eColckTypeTypeC = 3
        
} YDSH_eColckType;

//  闹铃持续时间枚举
typedef enum
{
        eClockDelayMinute1 = 0,
        eClockDelayMinute2 = 1,
        eClockDelayMinute3 = 2,
        eClockDelayMinute5 = 3,
        eClockDelayMinute10 = 4,
        eClockDelayMinute20 = 5,
        eClockDelayMinute30 = 6,
        eClockDelayMinute60 = 7
        
} YDSH_eClockDelay;

//  闹铃结构体
typedef struct
{
        BOOL b0, b1, b2, b3, b4, b5, b6;        //从周日到周六是否启用的标志（如是一次性闹铃，则表示可设置的最大日期为一周内）
        BOOL bRepeat;                           //是否重复闹铃的标志（如果不重复，表示是一次性闹铃）
        int nHour;                              //闹铃小时
        int nMinute;                            //闹铃分钟
        YDSH_eColckType cType;                  //闹铃类型（默认为eColckTypeTypeA，暂时无用）
        YDSH_eClockDelay cDelay;                //闹铃持续时间
        
} YDSH_strClock;

//  运动数据结构体
typedef struct
{
        int count;                              //运动步数(步)
        int count_O2;                           //有氧步数(步)
        int second;                             //运动时间(秒)
        int second_O2;                          //有氧时间(秒)(低字节在前)
        double distance;                        //运动距离(Km)
        double distance_O2;                     //有氧距离(Km)
        double calories;                        //运动热量(Kcal)
        double calories_O2;                     //有氧热量(Kcal)
        
} YDSH_strPedometer;

//  运动提醒结构体
typedef struct
{
        BOOL b0, b1, b2, b3, b4, b5, b6;        //从周日到周六是否启用的标志（如是一次性提醒，则表示可设置的最大日期为一周内）
        BOOL bRepeat;                           //是否重复提醒的标志（如果不重复，表示是一次性提醒）
        int nHour_Start;                        //开始提醒的小时
        int nMinute_Start;                      //开始提醒的分钟
        int nHour_End;                          //结束提醒的小时
        int nMinute_End;                        //结束提醒的分钟
        int nInterval;                          //在提醒区间内的提醒间隔（1-255分钟）
        
} YDSH_strSportReminder;

//  睡眠监控定时控制结构体
//  注意：这个不是睡眠提醒，而是睡眠监控控制，类似代替手工按下按钮进入或者退出睡眠监控状态
typedef struct
{
        BOOL isEnable;                          //是否启用该组睡眠监控的定时控制
        BOOL b0, b1, b2, b3, b4, b5, b6;        //从周日到周六是否启用的标志（如是一次性提醒，则表示可设置的最大日期为一周内）
        BOOL bRepeat;                           //是否重复的标志（如果不重复，表示是一次性提醒）
        BOOL isEnterMoniter;                    //是否进入睡眠监控（false为退出睡眠监控）
        int nHour;                              //睡眠监控开始小时
        int nMinute;                            //睡眠监控开始分钟
        BOOL isVibration;                       //是否马达震动提醒（一般为false）
        BOOL isShine;                           //是否屏幕点亮提醒（一般为false）
        
} YDSH_strSleepTiming;

//  ------------------------------------------------------------------

@interface BLEPublic : NSObject

+(const char *) CBUUIDToString:(CBUUID *) UUID;

+(int) compareCBUUID:(CBUUID *) UUID1 UUID2:(CBUUID *)UUID2;

+(const char *)UUIDToString:(CFUUIDRef)UUID;

+(CFUUIDRef)StringToUUID:(const char*)suuid;

+(BOOL)comparePeripheralUUID:(CBPeripheral *)cbp1 peripheral2:(CBPeripheral *)cbp2;

+(UInt16) swap:(UInt16)s;

+(CBService *) findServiceFromUUID:(CBUUID *)UUID p:(CBPeripheral *)p;

+(CBCharacteristic *) findCharacteristicFromUUID:(CBUUID *)UUID service:(CBService*)service;

+(CBDescriptor *) findDescriptorFromUUID:(CBUUID *)UUID characteristic:(CBCharacteristic *)characteristic;

+(UInt16) CBUUIDToInt:(CBUUID *)UUID;

//  将十六进制字符串转换为字节数组
//  hexString : 十六进制字符串，中间需要用空格分隔，如“0F 8A BC 00”
//  hexCount  : 十六进制数字长度
//  bytes     : 转换出的字节数组，长度为hexCount
+(void)GetHexStringToBytes:(NSString *)hexString hexCount:(int)hexCount bytes:(char*)bytes;

+(void)GetStringToBytes:(NSString *)bString count:(int)count bytes:(char*)bytes;

//  4字节时间处理，将4个字节转换成日期时间格式，4字节表示2000年1月1日0时0分0秒开始的秒数(UTC时间)(低字节在前)
+(NSDate*)GetBytesToDate:(Byte)b0 b1:(Byte)b1 b2:(Byte)b2 b3:(Byte)b3;

+(NSString *)GetBytesToTime:(Byte)b0 b1:(Byte)b1;

@end
