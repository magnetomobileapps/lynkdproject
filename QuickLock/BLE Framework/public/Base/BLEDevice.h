//
//  BLEDevice.h
//
//  ------------------------------------------------------------------
//  对外接口类，外部需要使用BLE设备的时候，就初始化此类
//  所有对BLE设备的输入输出，都从该类操作
//  ------------------------------------------------------------------

#import <Foundation/Foundation.h>
#import "Protocol_BLEDevice.h"
#import "Protocol_BLEDeviceResult.h"
#import "Protocol_BLEDeviceCall.h"

@interface BLEDevice : NSObject<Protocol_BLEDevice, Protocol_BLEDeviceCall>
{
    @protected
    int DeviceID;
    NSString *DeviceUUID;
}

@property (nonatomic, assign)id<Protocol_BLEDeviceResult> delegateBLEDeviceResult;

-(id)init:(id)delegete;

-(bool)changeBLEDevice:(BLEDevice*)sourceBLEDevice;

@end
