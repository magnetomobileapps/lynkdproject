//
//  Protocol_BLEDeviceResult.h
//
//  ------------------------------------------------------------------
//  返回Protocol_BLEDevice操作结果的接口
//  不直接使用，仅作为不同的BLE子接口的父接口存在
//  ------------------------------------------------------------------

@protocol Protocol_BLEDeviceResult <NSObject>

//  连接BLE设备成功
-(void)BR_connectOK:(int)DeviceID;

//  连接BLE设备失败，
//  失败原因是 0 - FAILETYPE_FAILTOCONNECT 连接不上设备
//           1 - FAILETYPE_FAILTODISCOVER 查找不到Sevrce
-(void)BR_connectFail:(int)nFAILETYPE DeviceID:(int)DeviceID;

//  断开BLE设备
-(void)BR_didDisconnect:(int)DeviceID;

//  读取RSSI成功，返回读取的RSSI值
-(void)BR_didUpdateRSSI:(float)value DeviceID:(int)DeviceID;

// ISW HONESTSKY 2014-11-20
//  BD_getScanDeviceList的结果返回
//  返回内容为string类型的MutableArray，每个string的内容是
//  “LocalName,ServiceUUID,UUID,RSSI,TxPower,ManufacturerData”，中间用逗号分隔
-(void)BR_scanDeviceList:(NSMutableArray *)arrayScanDeviceList DeviceID:(int)DeviceID;

//  BD_getMACAddress的结果返回
-(void)BR_getMACAddress:(Byte *)address length:(int)length DeviceID:(int)DeviceID;

@end
