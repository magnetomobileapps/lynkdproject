//
//  BLEDevice_YDSH.h
//
//  Copyright (c) 2014, Vicro Technology Limited. All rights reserved.
//
//  ------------------------------------------------------------------
//  运动手环子类
//  ------------------------------------------------------------------

#import "BLEDevice.h"
#import "Protocol_BLEDevice_YDSH.h"
#import "Protocol_BLEDeviceResult_YDSH.h"

@interface BLEDevice_YDSH : BLEDevice<Protocol_BLEDevice_YDSH>

@property (nonatomic, assign) id<Protocol_BLEDeviceResult_YDSH> delegateBLEDeviceResult_YDSH;

//  init
//  delegete : self
-(id)init:(id)delegete;

@end
