//
//  Protocol_BLEDeviceResult_YDSH.h
//
//  Copyright (c) 2014, Vicro Technology Limited. All rights reserved.
//
//  ------------------------------------------------------------------
//  运动手环子类对外接口的结果返回
//  ------------------------------------------------------------------

#import <Foundation/Foundation.h>
#import "BLEPublic.h"

@protocol Protocol_BLEDeviceResult_YDSH <NSObject>

#pragma mark  ------------------------------------------------------------------
#pragma mark  蓝牙底层服务
#pragma mark  ------------------------------------------------------------------

//  上传SystemID
//  systemID:8个字节的字符串，用十六进制表示，每个数字用空格分隔
-(void)BR_systemID:(NSString *)systemID DeviceID:(int)DeviceID;

//  上传ModelNumber
//  modelNumber:16位的字符串
-(void)BR_modelNumber:(NSString *)modelNumber DeviceID:(int)DeviceID;

//  上传固件版本
//  vision:    固件版本号
//  visionDate:固件版本时间
-(void)BR_firmwareRevision:(NSString *)vision visionDate:(NSString *)visionDate DeviceID:(int)DeviceID;

//  上传自动断开连接使能状态（连接成功后20S内读写有效）
//  isEnable: 当前的自动断开连接使能状态
-(void)BR_disconnectEnable:(BOOL)isEnable DeviceID:(int)DeviceID;

//  上传GATT通讯协议版本号
//  protocolVersion
-(void)BR_protocolVersion:(NSString *)protocolVersion DeviceID:(int)DeviceID;

#pragma mark  ------------------------------------------------------------------
#pragma mark  电池、时间服务
#pragma mark  ------------------------------------------------------------------

//  上传电池电量（电量变化时会自动上传）
//  battLevel : 电池电量，0-100（表示电池电量的百分比）
-(void)BR_batteryLevel:(int)battLevel DeviceID:(int)DeviceID;

//  上传硬件时间
//  devTime : 硬件时间值
-(void)BR_currentTime:(NSDate*)devTime DeviceID:(int)DeviceID;

//  上传硬件内闹钟条数
//  clockNumber : 闹钟条数（0-15）
-(void)BR_clockNumber:(int)clockNumber DeviceID:(int)DeviceID;

//  上传硬件闹钟数据
//  colckData : 硬件闹钟数据值，结构体
-(void)BR_colckData:(int)clockIndex clockData:(YDSH_strClock)clockData DeviceID:(int)DeviceID;

#pragma mark  ------------------------------------------------------------------
#pragma mark  运动数据
#pragma mark  ------------------------------------------------------------------

//  上传加速度计能状态（连接成功后20S内读写有效）
//  isEnable: 当前的加速度计使能状态
-(void)BR_gsensorEanble:(BOOL)isEnable DeviceID:(int)DeviceID;

//  上传加速度计X轴数据
//  xdata : 加速度计X轴数值
-(void)BR_gsensorX:(int)xdata DeviceID:(int)DeviceID;

//  上传加速度计Y轴数据
//  ydata : 加速度计Y轴数值
-(void)BR_gsensorY:(int)yata DeviceID:(int)DeviceID;

//  上传加速度计Z轴数据
//  zdata : 加速度计Z轴数值
-(void)BR_gsensorZ:(int)zdata DeviceID:(int)DeviceID;

//  上传当日运动数据
//  pedoData : 运动数据，结构体
-(void)BR_currentPedoData:(YDSH_strPedometer)pedoData DeviceID:(int)DeviceID;

//  上传运动提醒数据
//  reminderData : 运动提醒数据，结构体
-(void)BR_sportReminder:(YDSH_strSportReminder)reminderData DeviceID:(int)DeviceID;

#pragma mark  ------------------------------------------------------------------
#pragma mark  历史数据
#pragma mark  ------------------------------------------------------------------

//  上传运动历史天数据条数
//  daysNumber : 数据条数（最大15条，即15天数据）
-(void)BR_offlineData_DaysNumber:(int)daysNumber DeviceID:(int)DeviceID;

//  上传运动历史天数据
//  每次上传一条，最大传输15条（15天），传输间隔50ms左右
//  pedoDate : 运动历史日期
//  pedoData : 运动历史数据，结构体
-(void)BR_offlineData_Days:(NSDate *)pedoDate pedoData:(YDSH_strPedometer)pedoData DeviceID:(int)DeviceID;

//  上传运动历史分数据条数
//  daysNumber : 数据条数（最大16条，即16天数据，0表示今天数据）
-(void)BR_offlineData_MinutesDayNumber:(int)daysNumber DeviceID:(int)DeviceID;

//  上传运动历史分数据（最小间隔5分钟数据）
//  每次上传一条数据，最大16条（包括今天）的数据，每条数据的上传周期大约在50ms-8.8s之间（按照一天内，每5分钟一个运动历史分数据，最大24小时计算，一条数据可能超过5K Bytes以上，因此传输时间可能很长）
//  pedoDate         : 运动历史日期
//  dayMinutesRecord : 一天内所有运动历史分数据的集合，NSString形式存放
//           数据格式 : "HHmm:totalCount,HHmm:totalCount,...(以此类推)"。（HHmm：小时分钟；totalCount：到该时刻为止的当日运动计步总数）
//           格式举例 : "0001:1,...,1200:30000,...,2359:60000"
//           格式含义 : 到00点01分，总共计步1步；...到12点00分，总共计步30000步；...到23点59分，总共计步60000步
//  recordCount      : 一天的运动历史分数据的总数（5分钟数据的总数，可用于辅助解析dayMinutesRecord）
-(void)BR_offlineData_MinutesOfDay:(NSDate *)pedoDate dayMinutesRecord:(NSString *)dayMinutesRecord recordCount:(int)recordCount DeviceID:(int)DeviceID;

//  上传历史运动分数据的原始字节数据（供测试分析使用）
-(void)BR_offlineData_MinutesOfDay_receiveBytes:(Byte*)bytes length:(int)length DeviceID:(int)DeviceID;

#pragma mark  ------------------------------------------------------------------
#pragma mark  睡眠监控
#pragma mark  ------------------------------------------------------------------

//  上传睡眠监控数据条数
//  number : 数据条数（最大15条）
-(void)BR_sleepMonitorNumber:(int)number DeviceID:(int)DeviceID;

//  上传睡眠监控数据
//  每次上传一条数据，最大15条数据，每条数据的上传周期大约在205ms左右
//  beginSleepDate     : 开始睡眠的记录时间
//  endSleepDate       : 退出睡眠的记录时间
//  sleepMonitorRecord : 一次内所有睡眠监控数据的集合，NSString形式存放
//           数据格式   : "HHmm:sleepStatus,HHmm:sleepStatus,...(以此类推)"。(HHmm：小时分钟；sleepStatus：该时间点进入的睡眠状态(1:清醒,2:轻睡,3:熟睡))
//           格式举例   : "0001:2,...,0030:3,...,0700:1"
//           格式含义   : 00点01分，进入轻睡状态；...00点30分，进入熟睡状态；...07点00分，进入清醒状态）
//  recordCount        : 一次内所有睡眠监控数据的总数（可用于辅助解析sleepMonitorRecord）
-(void)BR_sleepMonitor:(NSDate *)beginSleepDate endSleepDate:(NSDate *)endSleepDate sleepMonitorRecord:(NSString *)sleepMonitorRecord recordCount:(int)recordCount DeviceID:(int)DeviceID;

//  上传睡眠监控定时控制数据
//  timingData1 : 第1组睡眠监控定时控制数据，结构体
//  timingData2 : 第2组睡眠监控定时控制数据，结构体
//  timingData3 : 第3组睡眠监控定时控制数据，结构体
//  timingData4 : 第4组睡眠监控定时控制数据，结构体
-(void)BR_sleepTiming:(YDSH_strSleepTiming)timingData1 timingData2:(YDSH_strSleepTiming)timingData2 timingData3:(YDSH_strSleepTiming)timingData3 timingData4:(YDSH_strSleepTiming)timingData4 DeviceID:(int)DeviceID;

//  上传睡眠起床闹铃的控制
//  isStartAlerm : 是否开始闹铃 true - 开始闹铃; false - 停止闹铃
-(void)BR_wakeUpAlerm:(BOOL)isStartAlerm DeviceID:(int)DeviceID;

#pragma mark  ------------------------------------------------------------------
#pragma mark  用户定制
#pragma mark  ------------------------------------------------------------------

//  上传个人运动参数
//  units      : 单位(0:公制；1:英制)
//  sex        : 性别(0:女；1：男)
//  age        : 年龄(1-254)(单位：岁)
//  height     : 身高(1-254)(单位：m，精确到0.01m)
//  weight     : 体重(1-65534)(单位：Kg，精确到0.1Kg)
//  stride     : 步长(1-65534)(单位：m，精确到0.001m）
//  nAlarmKind : 运动目标类型(1:热量(Kcal)，2:距离(Km)，3:步数(步)，4:时间(分钟))
//  tAlarmValue: 运动目标值（与相应的nAlarmKind类型对应的值）
-(void)BR_parameter:(int)units sex:(int)sex age:(int)age height:(double)height weight:(double)weight stride:(double)stride nAlarmKind:(int)nAlarmKind tAlarmValue:(double)tAlarmValue DeviceID:(int)DeviceID;

//  打开照相机
//  注：仅是一条硬件指令，真正的打开camera的操作需要由App内完成
-(void)BR_openCamera :(int)DeviceID;

//  单击拍照
//  注：仅是一条硬件指令，真正的拍照操作需要由App内完成
-(void)BR_snapCamera :(int)DeviceID;

//  关闭照相机
//  注：仅是一条硬件指令，真正的关闭开camera的操作需要由App内完成
-(void)BR_closeCamera :(int)DeviceID;

//  上传查看屏幕内容时，屏幕点亮的持续时间
//  screenLasting : 屏幕点亮的持续时间（单位：s）
-(void)BR_screenLasting:(int)screenLasting DeviceID:(int)DeviceID;

@end
