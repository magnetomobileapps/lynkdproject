//
//  Protocol_BLEDevice_YDSH.h
//
//  Copyright (c) 2014, Vicro Technology Limited. All rights reserved.
//
//  ------------------------------------------------------------------
//  运动手环子类对外接口
//  ------------------------------------------------------------------

#import <Foundation/Foundation.h>
#import "BLEPublic.h"

@protocol Protocol_BLEDevice_YDSH <NSObject>

//  ------------------------------------------------------------------
//  蓝牙底层服务
//  ------------------------------------------------------------------

#pragma mark
#pragma mark 蓝牙底层服务
#pragma mark

//  读取SystemID
//  读取到的SystemID值，通过BR_systemID返回
-(void)BD_getSystemID;

// ISW ANNE 2014-11-21
//  用户ID自动上传使能
//  如果使能为true,则在设备显示Binding...时按下设备按钮，自动上传设备中的用户ID
//  自动上传的用户ID，通过BR_modelNumber返回
//  isEnable : 是否允许用户ID自动上传, true - 允许上传, false - 禁止上传
-(void)BD_enableModelNumber:(BOOL)isEnable;

//  读取ModelNumber
//  读取到的Model Number值，通过BR_modelNumber返回
-(void)BD_getModelNumber;

//  设置ModelNumber
//  modelNumber:16个字节的字符串，用十六进制表示，每个数字用空格分隔，如“0F 8A BC 00”
-(void)BD_setModelNumber:(NSString *)modelNumber;

//  读取固件版本
//  读取到的固件版本值，通过BR_firmwareRevision返回
-(void)BD_getFirmwareRevision;

//  读取自动断开连接使能状态（连接成功后20S内读写有效）
//  读取到的使能值，通过BR_disconnectEnable返回
-(void)BD_getDisconnectEnable;

//  设置自动断开连接使能状态（连接成功后20S内读写有效）
//  isEnable: 自动断开连接使能，true - 自动断开，false - 不自动断开
-(void)BD_setDisconnectEanble:(BOOL)isEnable;

//  读取GATT通讯协议版本号
//  读取到的通讯协议版本值，通过BR_protocolVersion返回
-(void)BD_getProtocolVersion;

//  ------------------------------------------------------------------
//  电池、时间服务
//  ------------------------------------------------------------------

#pragma mark
#pragma mark 电池、时间服务
#pragma mark

//  读取电池电量
//  读取到的电池电量值，通过BR_betteryLevel返回
-(void)BD_getBetteryLevel;

//  电池电量自动上传使能
//  如果使能为true,则电量变化时会自动上传电量
//  自动上传的电池电量值，通过BR_betteryLevel返回
//  isEnable : 是否允许电池电量自动上传, true - 允许上传, false - 禁止上传
-(void)BD_enableBatteryLevel:(BOOL)isEnable;

//  读取当前硬件内的时间
//  读取到的硬件时间值，通过BR_currentTime返回
-(void)BD_getCurrentTime;

//  设置当前系统时间到硬件
-(void)BD_setCurrentTime;

//  硬件时间自动上传使能
//  如果使能为true，则硬件每秒自动上传一次硬件时间
//  自动上传的硬件时间值，通过BR_currentTime返回
//  isEnable : 是否允许电池电量自动上传, true - 允许上传, false - 禁止上传
//  注意：此功能仅用于特殊场合，例如：持续保持硬件连接状态；或持续保持App的后台激活状态（如在来电时APP被后台挂起，但是又需要保持激活以监听来电状态）
-(void)BD_enableTimeUpdate:(BOOL)isEnable;

//  闹钟数据自动上传使能
//  如果使能为true，则可以使用BD_getClockNumber、BD_getClockDataAll、BD_getClockDataAllOfIndex函数接收闹钟数据
//  isEnable : 是否允许闹钟数据自动上传, true - 允许上传, false - 禁止上传
-(void)BD_enableClock:(BOOL)isEnable;

//  读取现有的闹钟条数（最大15条）
//  读取到的闹钟条数值，通过BR_clockNumber返回
-(void)BD_getClockNumber;

//  读取全部闹钟数据（最大15条）
//  读取到的闹钟数据值，通过BR_colckData返回，每次返回一条闹钟数据，间隔大约50ms
-(void)BD_getClockDataAll;

//  读取指定的某一条闹钟数据
//  读取到的闹钟数据值，通过BR_colckData返回
//  index : 指定的某一条闹钟数据的索引（1-15）
-(void)BD_getClockDataAllOfIndex:(int)index;

//  清除所有闹铃数据
-(void)BD_clearClockData;

//  设置闹铃数据到硬件
//  clockIndex: 指定的某一条闹钟数据的索引（1-15）
//  clockData : 闹铃数据，结构体
-(void)BD_setClockData:(int)clockIndex clockData:(YDSH_strClock)clockData;

//  ------------------------------------------------------------------
//  运动数据
//  ------------------------------------------------------------------

#pragma mark
#pragma mark 运动数据
#pragma mark

//  读取加速度计使能状态（仅针对联机状态，脱机下一直有效）
//  读取到的使能值，通过BR_gsensorEanble返回
-(void)BD_getGSensorEanble;

//  读取加速度计X轴数据
//  读取到的数据值，通过BR_gsensorX返回
-(void)BD_getGSensorX;

//  加速度计X轴数据自动上传使能
//  如果使能为true，在启用加速度使能的前提下，当X轴数据发生改变时会自动上传
//  isEnable : 是否允许加速度计X轴数据自动上传, true - 允许上传, false - 禁止上传
-(void)BD_enableGSensorX:(bool)isEnable;

//  读取加速度计Y轴数据
//  读取到的数据值，通过BR_gsensorY返回
-(void)BD_getGSensorY;

//  加速度计Y轴数据自动上传使能
//  如果使能为true，在启用加速度使能的前提下，当Y轴数据发生改变时会自动上传
//  isEnable : 是否允许加速度计Y轴数据自动上传, true - 允许上传, false - 禁止上传
-(void)BD_enableGSensorY:(bool)isEnable;

//  读取加速度计Z轴数据
//  读取到的数据值，通过BR_gsensorZ返回
-(void)BD_getGSensorZ;

//  加速度计Z轴数据自动上传使能
//  如果使能为true，在启用加速度使能的前提下，当Z轴数据发生改变时会自动上传
//  isEnable : 是否允许加速度计Z轴数据自动上传, true - 允许上传, false - 禁止上传
-(void)BD_enableGSensorZ:(bool)isEnable;

//  设置联机计步使能状态（仅针对联机状态，脱机下一直有效）
//  在联机状态下，如果设置禁用，则不再计步（脱机后恢复启用状态），设置启用则重新开始计步
//  isEnable: 联机计步使能，true - 启用，false - 禁用
//  注意：此设置仅针对联机计步起作用，主要是为了兼容旧的计步器软件，如果无联机计步功能，无需使用此函数
-(void)BD_setOnlinePedoCountEanble:(bool)isEnable;

//  读取当日的运动数据
//  读取到的运动数据值，通过BR_currentPedoData返回
-(void)BD_getCurrentPedoData;

//  联机计步数据自动上传使能
//  如果使能为true，当联机计步数据发生改变时会自动上传，并通过BR_currentPedoData返回
//  isEnable : 是否允许联机计步数据自动上传, true - 允许上传, false - 禁止上传
//  注意：此设置仅针对联机计步起作用，主要是为了兼容旧的计步器软件，如果无联机计步功能，无需使用此函数
-(void)BD_enableOnlinePedometer:(BOOL)isEnable;

//  清除当日运动数据
//  注意：当日运动数据的清除不会引起当日历史分数据被清除，因此此函数需要配合清除历史数据使用，才能达到全部清零的效果，一般不用
-(void)BD_clearCurrentPedoData;

//  读取运动提醒数据
//  读取到的运动提醒数据，将通过BR_sportReminder返回
-(void)BD_getSportReminder;

//  设置运动提醒数据到硬件
//  reminderData : 运动提醒数据，结构体
-(void)BD_setSportReminder:(YDSH_strSportReminder)reminderData;

//  ------------------------------------------------------------------
//  历史数据
//  ------------------------------------------------------------------

#pragma mark
#pragma mark 历史数据
#pragma mark

//  运动历史天数据自动上传使能
//  如果使能为true，则可以使用BD_getOfflineData_DaysNumber、BD_getOfflineData_DaysAll、BD_getOfflineData_DaysOfIndex函数接收运动历史天数据
//  isEnable : 运动历史天数据自动上传, true - 允许上传, false - 禁止上传
-(void)BD_enableOfflineData_Days:(BOOL)isEnable;

//  读取现有的运动历史天数据条数（最大15条，即15天数据）
//  读取到的运动历史天数据条数值，通过BR_offlineData_DaysNumber返回
-(void)BD_getOfflineData_DaysNumber;

//  读取全部运动历史天数据（最大15条，即15天数据）
//  读取到的运动历史天数据值，通过BR_offlineData_Days返回，每次返回一条运动历史天数据，间隔大约50ms
-(void)BD_getOfflineData_DaysAll;

//  读取指定的某一条运动历史天数据
//  读取到的运动历史天数据值，通过BR_offlineData_Days返回
//  dayIndex : 指定的某一条运动历史天数据的索引（1-15）
-(void)BD_getOfflineData_DaysOfIndex:(int)dayIndex;

//  清除硬件内所有运动历史天数据
//  注意：在接收所有运动历史天数据之后，需要调用此函数进行清除，否则新旧数据会混合在一起，造成混淆
-(void)BD_clearOfflineData_Day;

//  运动历史分数据自动上传使能
//  如果使能为true，则可以使用BD_getOfflineData_MinutesDayNumber、BD_getOfflineData_MinutesAllDay、BD_getOfflineData_MinutesOfDayIndex函数接收运动历史天数据
//  isEnable : 运动历史分数据自动上传, true - 允许上传, false - 禁止上传
-(void)BD_enableOfflineData_Minutes:(BOOL)isEnable;

//  读取现有的运动历史分数据条数（最大16条，即前15天数据 + 今天的数据）
//  读取到的运动历史分数据条数值，通过BR_offlineData_MinutesDayNumber返回
-(void)BD_getOfflineData_MinutesDayNumber;

//  读取全部运动历史分数据（最大16条，即前15天数据 + 今天的数据）
//  读取到的运动历史分数据值，通过BR_offlineData_MinutesOfDay返回，每次返回一条运动历史分数据数据，间隔大约50ms
-(void)BD_getOfflineData_MinutesAllDay;

//  读取指定的某一条运动历史分数据
//  读取到的运动历史分数据值，通过BR_offlineData_MinutesOfDay返回
//  dayIndex : 指定的某一条运动历史分数据的索引（0-15，0表示今天）
-(void)BD_getOfflineData_MinutesOfDayIndex:(int)dayIndex;

//  清除硬件内所有运动历史分数据
//  注意：在接收所有运动历史分数据之后，需要调用此函数进行清除，否则新旧数据会混合在一起，造成混淆
-(void)BD_clearOfflineData_Minutes;

//  ------------------------------------------------------------------
//  睡眠监控
//  ------------------------------------------------------------------

#pragma mark
#pragma mark 睡眠监控
#pragma mark

//  睡眠监控数据自动上传使能
//  如果使能为true，则可以使用BD_getSleepMonitorNumber、BD_getSleepMonitorAll、BD_getSleepMonitorOfIndex函数接收睡眠监控数据
//  isEnable : 睡眠监控数据自动上传, true - 允许上传, false - 禁止上传
-(void)BD_enableSleepMonitor:(BOOL)isEnable;

//  读取现有的睡眠监控数据条数（最大15条，即15次睡眠监控数据）
//  读取到的睡眠监控数据条数值，通过BR_sleepMonitorNumber返回
-(void)BD_getSleepMonitorNumber;

//  读取全部睡眠监控数据（最大15条，即15次睡眠监控数据）
//  读取到的睡眠监控数据值，通过BR_sleepMonitor返回，每次返回一条睡眠监控数据，间隔大约250ms（100Bytes）
-(void)BD_getSleepMonitorAll;

//  读取指定的某一次睡眠监控数据
//  读取到的睡眠监控数据值，通过BR_sleepMonitor返回
//  index : 指定的某一次睡眠监控数据的索引（1-15）
-(void)BD_getSleepMonitorOfIndex:(int)index;

//  清除硬件内所有睡眠监控数据
//  注意：在接收所有睡眠监控数据之后，需要调用此函数进行清除，否则新旧数据会混合在一起，造成混淆
-(void)BD_clearSleepMonitor;

//  读取睡眠监控定时控制数据
//  读取到的睡眠监控定时控制数据，将通过BR_sleepTiming返回
-(void)BD_getSleepTiming;

//  设置睡眠监控定时控制数据到硬件
//  timingData1 : 第1组睡眠监控定时控制数据，结构体
//  timingData2 : 第2组睡眠监控定时控制数据，结构体
//  timingData3 : 第3组睡眠监控定时控制数据，结构体
//  timingData4 : 第4组睡眠监控定时控制数据，结构体
-(void)BD_setSleepTiming:(YDSH_strSleepTiming)timingData1 timingData2:(YDSH_strSleepTiming)timingData2 timingData3:(YDSH_strSleepTiming)timingData3 timingData4:(YDSH_strSleepTiming)timingData4;

//  起床闹铃使能
//  isEnable : 是否允许起床闹铃, true - 允许, false - 禁止
-(void)BD_enableWakeUpAlerm:(BOOL)isEnable;

//  ------------------------------------------------------------------
//  用户定制
//  ------------------------------------------------------------------

#pragma mark
#pragma mark 用户定制
#pragma mark

//  读取个人运动参数
//  读取到的个人运动参数，通过BR_parameter返回
-(void)BD_getParameter;

//  设置个人运动参数到硬件
//  units      : 单位(0:公制；1:英制)
//  sex        : 性别(0:女；1：男)
//  age        : 年龄(1-254)(单位：岁)
//  height     : 身高(1-254)(单位：m，精确到0.01m)
//  weight     : 体重(1-65534)(单位：Kg，精确到0.1Kg)
//  stride     : 步长(1-65534)(单位：m，精确到0.001m）
//  nAlarmKind : 目标类型(1:热量(Kcal)，2:距离(Km)，3:步数(步)，4:时间(分钟))
//  tCalories  : 热量目标值(Kcal)
//  tDistance  : 距离目标值(Km)
//  tPaceCount : 步数目标值(步)
//  tCostTime  : 时间目标值(分钟)
-(void)BD_setParameter:(int)units sex:(int)sex age:(int)age height:(double)height weight:(double)weight stride:(double)stride nAlarmKind:(int)nAlarmKind tCalories:(double)tCalories tDistance:(double)tDistance tPaceCount:(double)tPaceCount tCostTime:(double)tCostTime;

//  启用/禁用硬件遥控拍照功能
//  必须先启用此设置，才能接收到硬件遥控拍照的控制命令BR_openCamera、BR_snapCamera、BR_closeCamera
//  isEnable 是否启用硬件遥控拍照功能, true - 启用, false - 禁用
-(void)BD_enableCamera:(BOOL)isEnable;

//  通知硬件是否有来电
//  isRead : 是否有来电, false - 有未读来电, true - 来电已处理(已接入、已经挂断或已拒绝）
//  注：对来电的监听和判断需要在APP内完成，此函数仅仅对硬件发起提醒而已
-(void)BD_setComingCall:(BOOL)isRead;

//  读取查看屏幕内容时，屏幕点亮的持续时间（单位：S）
//  读取到的屏幕点亮持续时间，通过BR_screenLasting返回
-(void)BD_getScreenLasting;

//  设置读取查看屏幕内容时，屏幕点亮的持续时间
//  screenLasting : 屏幕点亮的持续时间（单位：s）
-(void)BD_setScreenLasting:(int)screenLasting;

@end
