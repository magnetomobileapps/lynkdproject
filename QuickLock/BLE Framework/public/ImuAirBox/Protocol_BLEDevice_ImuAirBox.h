//
//  Protocol_BLEDevice_ImuAirBox.h
//
//  ------------------------------------------------------------------
//  幻响空气盒子-设备-手机互联的BLE主端测试子类对外接口
//  ------------------------------------------------------------------

#import <Foundation/Foundation.h>

@protocol Protocol_BLEDevice_ImuAirBox <NSObject>

//  启用/禁用数据上传
//  isEnable 是否启用
-(void)BD_enableTransparentData:(BOOL)isEnable;

//  发送测试数据到设备
-(void)BD_sendTestData;

@end
