//
//  Protocol_BLEDeviceResult_ImuAirBox.h
//
//  ------------------------------------------------------------------
//  幻响空气盒子-设备-手机互联的BLE主端测试子类外接口的结果返回
//  ------------------------------------------------------------------

#import <Foundation/Foundation.h>

@protocol Protocol_BLEDeviceResult_ImuAirBox <NSObject>

//  接收测试数据
-(void)BR_receiveTestData:(int)pm temperature:(int)temperature humidity:(int)humidity;

@end
