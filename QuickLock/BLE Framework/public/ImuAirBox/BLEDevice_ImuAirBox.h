//
//  BLEDevice_ImuAirBox.h
//
//  ------------------------------------------------------------------
//  幻响空气盒子-设备-手机互联的BLE主端测试子类
//  ------------------------------------------------------------------
//
#import "BLEDevice.h"
#import "Protocol_BLEDevice_ImuAirBox.h"
#import "Protocol_BLEDeviceResult_ImuAirBox.h"

@interface BLEDevice_ImuAirBox : BLEDevice<Protocol_BLEDevice_ImuAirBox>

@property (nonatomic, assign)id<Protocol_BLEDeviceResult_ImuAirBox> delegateBLEDeviceResult_ImuAirBox;

-(id)init:(id)delegete;

@end
