//
//  BLEDevice_BP136A.h
//
//  ------------------------------------------------------------------
//  康贝血压计子类
//  ------------------------------------------------------------------
//

#import "BLEDevice.h"
#import "Protocol_BLEDevice_BP136A.h"
#import "Protocol_BLEDeviceResult_BP136A.h"

@interface BLEDevice_BP136A : BLEDevice<Protocol_BLEDevice_BP136A>

@property (nonatomic, assign) id<Protocol_BLEDeviceResult_BP136A> delegateBLEDeviceResult_BP136A;

-(id)init:(id)delegete;

@end
