//
//  Protocol_BLEDevice_BP136A.h
//
//  ------------------------------------------------------------------
//  康贝血压计子类对外接口
//  ------------------------------------------------------------------

#import <Foundation/Foundation.h>

@protocol Protocol_BLEDevice_BP136A <NSObject>

//  启用/禁用血压数据上传
//  isEnable 是否启用
-(void)BD_enableTransparentData:(BOOL)isEnable;

//  开始血压数据上传
-(void)BD_startTransparent;

//  停止血压数据上传
-(void)BD_stopTransparent;

//  读取硬件信息：设备ID类码
-(void)BD_getHardInfo_DeviceID;

//  读取硬件信息：设备状态
-(void)BD_getHardInfo_DeviceStatus;

//  读取硬件信息：系统电量
-(void)BD_getHardInfo_CurrentVoltage;

//  读取硬件信息：系统日期时间
-(void)BD_getHardInfo_SystemTime;

//  发送检验指令
-(void)BD_sendCheckCommand;

//  发送校正指令
-(void)BD_sendAdjustCommand;

//  发送校正后确认指令
-(void)BD_sendConfirmCommand;

@end
