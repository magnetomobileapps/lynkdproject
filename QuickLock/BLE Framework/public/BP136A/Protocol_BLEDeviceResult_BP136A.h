//
//  Protocol_BLEDeviceResult_BP136A.h
//
//  ------------------------------------------------------------------
//  康贝血压计子类对外接口的结果返回
//  ------------------------------------------------------------------

#import <Foundation/Foundation.h>

@protocol Protocol_BLEDeviceResult_BP136A <NSObject>

-(void)BR_recevieTransparentDataTest:(NSString *)demoStr revcTime:(NSDate *)recvTime DeviceID:(int)DeviceID;

//  返回硬件信息：设备ID类码
-(void)BR_recvHardInfo_DeviceID:(NSString*)sDeviceKind DeviceID:(int)DeviceID;

//  返回硬件信息：设备状态
-(void)BR_recvHardInfo_DeviceStatus:(NSString*)sDeviceStatus DeviceID:(int)DeviceID;

//  返回硬件信息：系统电量
-(void)BR_recvHardInfo_CurrentVoltage:(int)nCurrentVoltage DeviceID:(int)DeviceID;

//  返回硬件信息：系统日期时间
-(void)BR_recvHardInfo_SystemTime:(NSDate*)dSystemTime DeviceID:(int)DeviceID;

//  测量数据上传 每23ms上传一次，包括：
//  电量 格式：(取值范围(0~100)%)
//  心率 格式：??
//  血压 格式：(取值范围:0mmHg~300mmHg)
-(void)BR_updateMesuringData:(int)nBattery heartBeat:(int)nHeartBeat bloodPressure:(int)nPressure DeviceID:(int)DeviceID;

//  测量结果上传 在测量结束时上传
//  测量时间
//  收缩压 (systolic blood pressure)  格式：(取值范围:0mmHg~300mmHg)
//  舒张压 (diastolic blood pressure) 格式：(取值范围:0mmHg~300mmHg)
//  心  率 (Heart rate)               格式：bpm
//  心房纤颤（AFIB）或者 心率不齐(IHB)   格式：??
-(void)BR_updateMesureResult:(NSDate*)musureTime systolicPressure:(int)nSystolicPressure diastolicPressure:(int)nDiastolicPressure heartRate:(int)nHeartRate AFIB_IHB:(int)nAFIB_IHB DeviceID:(int)DeviceID;

//  测量过程中错误信息上传
//  sErr：错误代码
//  sErrInfo：错误信息
-(void)BR_updateMesureError:(NSString*)sErr sErrInfo:(NSString*)sErrInfo DeviceID:(int)DeviceID;

//  命令返回值：开始血压数据上传
-(void)BR_result_StartTransparent:(int)commandResult DeviceID:(int)DeviceID;

//  命令返回值：停止血压数据上传
-(void)BR_result_stopTransparent:(int)commandResult DeviceID:(int)DeviceID;

//  命令返回值：读取硬件信息：设备ID类码
-(void)BR_result_getHardInfo_DeviceID:(int)commandResult DeviceID:(int)DeviceID;

//  命令返回值：读取硬件信息：设备状态
-(void)BR_result_getHardInfo_DeviceStatus:(int)commandResult DeviceID:(int)DeviceID;

//  命令返回值：读取硬件信息：系统电量
-(void)BR_result_getHardInfo_CurrentVoltage:(int)commandResult DeviceID:(int)DeviceID;

//  命令返回值：读取硬件信息：系统日期时间
-(void)BR_result_getHardInfo_SystemTime:(int)commandResult DeviceID:(int)DeviceID;

//  命令返回值：发送检验指令
-(void)BR_result_sendCheckCommand:(int)commandResult DeviceID:(int)DeviceID;

//  命令返回值：发送校正指令
-(void)BR_result_sendAdjustCommand:(int)commandResult DeviceID:(int)DeviceID;

//  命令返回值：发送校正后确认指令
-(void)BR_result_sendConfirmCommand:(int)commandResult DeviceID:(int)DeviceID;
@end
