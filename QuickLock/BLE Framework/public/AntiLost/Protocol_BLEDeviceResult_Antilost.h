//
//  Protocol_BLEDeviceResult_Antilost.h
//  
//  ------------------------------------------------------------------
//  防丢器子类的对外结果返回
//  ------------------------------------------------------------------

#import <Foundation/Foundation.h>

@protocol Protocol_BLEDeviceResult_Antilost <NSObject>

//  startVerifyItonBLE函数返回设备是否是Iton设备的校验结果
//  bSuccess 是否校验成功
-(void)BR_verifyItonBLEResult:(bool)bSuccess DeviceID:(int)DeviceID;

//  自动更新设备的电池电量值,需要设置电池电量自动上报使能
//  bValue 设备电量值
-(void)BR_batteryUpdated:(float)bValue DeviceID:(int)DeviceID;

//  要求手机响铃键值更新,长时间按键后触发一次
-(void)BR_ringkeyValuesUpdated:(int)DeviceID;

//  readTXPower函数返回设备发射功率值
//  tValue 功率值
-(void)BR_didUpdateTxPower:(char)tValue DeviceID:(int)DeviceID;

//  2014-04-30 wangyang
//  单击硬件按钮拍照
-(void)BR_snap:(int)DeviceID;

//  2014-04-30 wangyang
//  双击硬件按钮打开/关闭相机
//  双向切换状态，如果当前系统相机为打开，则关闭；反之则打开系统相机
-(void)BR_cameraOperation:(int)DeviceID;

@end
