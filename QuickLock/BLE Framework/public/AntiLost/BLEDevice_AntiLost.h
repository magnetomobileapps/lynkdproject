//
//  BLEDevice_AntiLost.h
//
//  ------------------------------------------------------------------
//  防丢器子类
//  ------------------------------------------------------------------

#import "BLEDevice.h"
#import "Protocol_BLEDevice_Antilost.h"
#import "Protocol_BLEDeviceResult_Antilost.h"

@interface BLEDevice_AntiLost : BLEDevice<Protocol_BLEDevice_Antilost>

@property (nonatomic, assign) id<Protocol_BLEDeviceResult_Antilost> delegateBLEDeviceResult_Antilost;

-(id)init:(id)delegete;

@end
