//
//  Protocol_BLEDevice_Antilost.h
//  
//  ------------------------------------------------------------------
//  防丢器子类对外接口
//  ------------------------------------------------------------------

#import <Foundation/Foundation.h>

@protocol Protocol_BLEDevice_Antilost <NSObject>

//  开始校验是否Iton设备
-(void)BD_startVerifyItonBLE;

//  蜂鸣器告警
//  bLargh 是否高声
-(void)BD_buzzerAlarm:(bool)bLarge;

//  停止蜂鸣器告警
-(void)BD_buzerStop;

//  读取设备发射功率值
-(void)BD_readTXPower;

//  启用/禁用按键功能
//  isEnable 是否启用
-(void)BD_enableButtons:(BOOL)isEnable;

//  启用/禁用发射功率上传
//  isEnable 是否启用
-(void)BD_enableTXPower:(BOOL)isEnable;

//  启用/禁用电池电量自动上传
//  isEnable 是否启用
-(void)BD_enableBattery:(BOOL)isEnable;

//  向设备写入发射功率
//  nLevel 发射功率等级 0:4dbm, 1: 0dbm, 2: -6dbm, 3:-23dbm
-(void)BD_writeTXPower:(int)nLevel;

//  使防丢器设备进入休眠状态
-(void)BD_setPeripheralSleep;

//  2014-04-30 wangyang
//  启用/禁用camera功能
-(void)BD_enableCamera:(BOOL)isEnable;

@end
