//
//  Protocol_BLEDevice_UART.h
//
//  ------------------------------------------------------------------
//  BLE透传子类 对外接口
//  本子类作为透传类的基类，仅有启用或者关闭数据上传的接口，其他操作放在下级子类中
//  ------------------------------------------------------------------

#import <Foundation/Foundation.h>

@protocol Protocol_BLEDevice_UART <NSObject>

//  启用/禁用数据上传
//  isEnable 是否启用
-(void)BD_enableTransparentData:(BOOL)isEnable;

//  发送数据到硬件，不超过最大长度
-(void)BD_sendFullLengthData:(char*)bytes length:(int)length;

//  获取数据定义的最大长度
-(int)BD_getDataFullLength;

@end
