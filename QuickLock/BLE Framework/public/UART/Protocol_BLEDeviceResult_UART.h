//
//  Protocol_BLEDeviceResult_UART.h
//
//  ------------------------------------------------------------------
//  BLE透传子类 对外接口的结果返回
//  本类作为透传类的基类，其下级子类有各自的数据返回
//  ------------------------------------------------------------------

#import <Foundation/Foundation.h>

@protocol Protocol_BLEDeviceResult_UART <NSObject>

//  测试基类接收数据返回
-(void)BR_receiveBaseData:(Byte*)bytes length:(int)length recvTime:(NSDate*)recvTime DeviceID:(int)DeviceID;

@end
