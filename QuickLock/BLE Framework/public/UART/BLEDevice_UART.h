//
//  BLEDevice_UART.h
//
//  ------------------------------------------------------------------
//  BLE透传子类
//  ------------------------------------------------------------------
//
#import "BLEDevice.h"
#import "Protocol_BLEDevice_UART.h"
#import "Protocol_BLEDeviceResult_UART.h"

@interface BLEDevice_UART: BLEDevice<Protocol_BLEDevice_UART>

@property (nonatomic, assign)id<Protocol_BLEDeviceResult_UART> delegateBLEDeviceResult_UART;

-(id)init:(id)delegete;

@end
