//
//  BLEDevice_AntiLost_ClubCard.h
//
//  ------------------------------------------------------------------
//  防丢器会员卡子类
//  ------------------------------------------------------------------

#import "BLEDevice.h"
#import "Protocol_BLEDevice_Antilost_ClubCard.h"
#import "Protocol_BLEDeviceResult_Antilost_ClubCard.h"

@interface BLEDevice_AntiLost_ClubCard : BLEDevice<Protocol_BLEDevice_Antilost_ClubCard>

@property (nonatomic, assign) id<Protocol_BLEDeviceResult_Antilost_ClubCard> delegateBLEDeviceResult_Antilost_ClubCard;

-(id)init:(id)delegete;

@end
