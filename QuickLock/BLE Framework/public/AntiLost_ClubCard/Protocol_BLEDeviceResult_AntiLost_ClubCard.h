//
//  Protocol_BLEDeviceResult_AntiLost_ClubCard.h
//
//  ------------------------------------------------------------------
//  防丢器会员卡子类的对外结果返回
//  ------------------------------------------------------------------

#import <Foundation/Foundation.h>

@protocol Protocol_BLEDeviceResult_Antilost_ClubCard <NSObject>

#pragma mark
#pragma mark 防丢器部分
#pragma mark

//  自动更新设备的电池电量值,需要设置电池电量自动上报使能
//  bValue 设备电量值
-(void)BR_batteryUpdated:(float)bValue DeviceID:(int)DeviceID;

//  要求手机响铃键值更新,长时间按键后触发一次
-(void)BR_ringkeyValuesUpdated:(int)DeviceID;

//#pragma mark
//#pragma mark 透传部分
//#pragma mark
//
//-(void)BR_receiveBaseData:(Byte*)bytes length:(int)length recvTime:(NSDate*)recvTime DeviceID:(int)DeviceID;

#pragma mark
#pragma mark 会员卡部分
#pragma mark
//  SID 20150207
//  获取FixedID返回
//  index FixedID序号
//  fixedID fixedID值
-(void)BR_GetFixedID:(int)index fixedID:(NSString *)fixedID DeviceID:(int)DeviceID;

//  获取FixedID返回
//  index FloatingID序号
//  floatingID floatingID值
-(void)BR_GetFloatingID:(int)index floatingID:(NSString *)floatingID DeviceID:(int)DeviceID;

//  设备应答
//  result 0:成功, 1:失败, 2:超时
-(void)BR_ACK:(int)result DeviceID:(int)DeviceID;

//  返回AuthEnableStatus状态
//  isPress true按下 false 弹起
-(void)BR_AuthEnableStatus:(BOOL)isPress DeviceID:(int)DeviceID;
@end
