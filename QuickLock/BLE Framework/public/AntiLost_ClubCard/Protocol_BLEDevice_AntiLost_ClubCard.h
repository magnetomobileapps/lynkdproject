//
//  Protocol_BLEDevice_AntiLost_ClubCard.h
//
//  ------------------------------------------------------------------
//  防丢器会员卡子类对外接口
//  ------------------------------------------------------------------

#import <Foundation/Foundation.h>

@protocol Protocol_BLEDevice_Antilost_ClubCard <NSObject>

#pragma mark
#pragma mark 防丢器部分
#pragma mark

//  蜂鸣器告警
//  bLargh 是否高声
-(void)BD_buzzerAlarm:(bool)bLarge;

//  停止蜂鸣器告警
-(void)BD_buzerStop;

//  启用/禁用按键功能
//  isEnable 是否启用
-(void)BD_enableButtons:(BOOL)isEnable;

//  启用/禁用电池电量自动上传
//  isEnable 是否启用
-(void)BD_enableBattery:(BOOL)isEnable;

//  使防丢器设备进入休眠状态
-(void)BD_setPeripheralSleep;

//#pragma mark
//#pragma mark 透传部分
//#pragma mark

////  启用/禁用数据透明上传
////  isEnable 是否启用
//-(void)BD_enableTransparentData:(BOOL)isEnable;
//
////  发送数据到硬件，不超过最大长度
//-(void)BD_sendFullLengthData:(char*)bytes length:(int)length;
//
////  获取数据定义的最大长度
//-(int)BD_getDataFullLength;

#pragma mark
#pragma mark 会员卡部分
#pragma mark

//  启用/禁用会员卡数据上传
//  isEnable 是否启用
-(void)BD_enableClubCardData:(BOOL)isEnable;

//  SID 20150207
//  获取FixedID
//  index FixedID序号
-(void)BD_getFixedID:(int)index;

//  设置FixedID
//  index FixedID序号
//  fixedID fixedID字符串
-(void)BD_setFixedID:(int)index fixedID:(NSString *)fixedID;

//  获取FloatingID
//  index FloatingID序号
-(void)BD_getFloatingID:(int)index;

//  设置FloatingID
//  index FloatingID序号
//  floatingID floatingID字符串
-(void)BD_setFloatingID:(int)index floatingID:(NSString *)floatingID;

@end
