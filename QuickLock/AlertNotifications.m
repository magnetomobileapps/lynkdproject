//
//  AlertNotifications.m
//  QuickLock
//
//  Created by PUNDSK001 on 11/06/16.
//  Copyright © 2016 Bge. All rights reserved.
//

#import "AlertNotifications.h"
#import "AlertLogin.h"
#import "AlertMyDevices.h"

@interface AlertNotifications () <SlideNavigationControllerDelegate>

@property (strong, nonatomic) IBOutlet UILabel *lblWelcome;
@property (strong, nonatomic) IBOutlet UILabel *lblNotUser;
@property (strong, nonatomic) IBOutlet UIButton *btnMyDevices;
@end

@implementation AlertNotifications

- (void)viewDidLoad
{
    [super viewDidLoad];

    [SlideNavigationController sharedInstance].portraitSlideOffset = appDelegate.portraitDrawer;
    
    UIButton *btnMenu = [UIButton buttonWithType:UIButtonTypeCustom];
    btnMenu.frame = CGRectMake(10, 28, 35, 35);
    [btnMenu setImage:[UIImage imageNamed:@"navigation.png"] forState:UIControlStateNormal];
    [btnMenu addTarget:[SlideNavigationController sharedInstance] action:@selector(toggleLeftMenu) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnMenu];
    
    _lblWelcome.font = _lblNotUser.font = [UIFont fontWithName:@"HelveticaNeue-MediumItalic" size:20];
    
    _btnMyDevices.layer.borderWidth = 3;
    _btnMyDevices.layer.masksToBounds = YES;
    _btnMyDevices.layer.borderColor = darkgray.CGColor;
    // set title of buttons due to multiline
    [_btnMyDevices setTitle:@"MY\nDEVICES" forState:UIControlStateNormal];
    _btnMyDevices.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    _btnMyDevices.titleLabel.numberOfLines = 2;
    _btnMyDevices.titleLabel.textAlignment = NSTextAlignmentCenter;
}

-(void)viewDidAppear:(BOOL)animated
{
    _btnMyDevices.layer.cornerRadius = _btnMyDevices.frame.size.height/2;
}

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
}

- (IBAction)btnMyDevices:(id)sender
{
    AlertMyDevices *alertMyDevices = [self.storyboard instantiateViewControllerWithIdentifier:@"AlertMyDevices"];
    [self.navigationController pushViewController:alertMyDevices animated:NO];
}

- (IBAction)btnLoginHere:(id)sender
{
    AlertLogin *alertLogin = [self.storyboard instantiateViewControllerWithIdentifier:@"AlertLogin"];
    [self.navigationController pushViewController:alertLogin animated:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
