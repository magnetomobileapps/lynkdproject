//
//  HistoryCell.h
//  QuickLock
//
//  Created by Ankit on 27/12/15.
//  Copyright (c) 2015 Bge. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HistoryCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblLog;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (weak, nonatomic) IBOutlet UILabel *lblName;

@end
