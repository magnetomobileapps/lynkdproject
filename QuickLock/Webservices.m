//
//  Webservices.m
//  AFNetworkingDemo
//
//  Created by Pavan Jadhav on 20/12/16.
//  Copyright © 2016 Pavan Jadhav. All rights reserved.
//

#import "Webservices.h"
#import "AFHTTPSessionManager.h"
#import "AppDelegate.h"
#import "NewReachability.h"

@class AFNetworking;
@implementation Webservices

#pragma mark webservice methods
+  (void)requestPostUrl:(NSString *)strURL parameters:(NSDictionary *)dictParams success:(void (^)(NSDictionary *responce))success failure:(void (^)(NSError *error))failure {
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager setResponseSerializer:[AFHTTPResponseSerializer serializer]];
    
    [manager POST:strURL parameters:dictParams progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if([responseObject isKindOfClass:[NSDictionary class]]) {
            if(success) {
                success(responseObject);
            }
        }
        else {
            NSDictionary *response = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:nil];
            if(success) {
                success(response);
            }
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if(failure) {
            failure(error);
        }
        
    }];
}

//POST请求
+(void)postWithUrlString:(NSString *)urlString parameters:(NSDictionary *)parameters success:(void (^)(NSDictionary *responce))success failure:(void (^)(NSError *error))failure{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    AppDelegate  * appDelegateObj =(AppDelegate *) [UIApplication sharedApplication].delegate;
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"text/json",@"text/javascript",@"text/html", nil];
    [manager POST:urlString parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        NSLog(@"share api Wrote %f", uploadProgress.fractionCompleted);
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
     {
         NSLog(@"Response: %@",responseObject);
         NSError *error;
         NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseObject
                                                              options:NSJSONReadingAllowFragments
                                                                error:&error];
        success(json);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error)
    {
        NSLog(@"Error: %@ \n and %@",error,error.localizedDescription);
        failure(error);
    }];
}

+(void)getWithUrlString:(NSString *)urlString success:(void (^)(NSDictionary *responce))success failure:(void (^)(NSError *error))failure{
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
//    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"text/json",@"text/javascript",@"text/html", nil];
    [manager GET:urlString parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSError *error;
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseObject
                                                             options:kNilOptions
                                                               error:&error];
        success(json);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failure(error);
    }];
}

+ (void)requestPostUrlWithImage: (NSString *)serviceName parameters:(NSDictionary *)dictParams image:(UIImage *)image  isImageSelected:(BOOL)isImageSelected filePath:(NSString *)videofilePath success:(void (^)(NSDictionary *responce))success failure:(void (^)(NSError *error))failure
{
  AppDelegate  * appDelegateObj =(AppDelegate *) [UIApplication sharedApplication].delegate;
    UIImage *smallImage = [UIImage imageWithCGImage:image.CGImage scale:0.1 orientation:image.imageOrientation];
    NSData *imageData = UIImageJPEGRepresentation(smallImage, 0.1);
    NSURL *filePath = [NSURL URLWithString:videofilePath];
//    NSURL *filePathImage = [self storeImageAndGetUrlFromName:@"pavan12345" andformId:@"32" withData:imageData];
    NSString *strVideoName = [self getCurrentTimeStamp];
    strVideoName = [strVideoName stringByAppendingString:@"screenCapture.mp4"];
    // NSString *fileName = [filePath absoluteString];
    NSString *strService = [NSString stringWithFormat:@"%@",serviceName];
    //  NSData *fileData = image?UIImageJPEGRepresentation(image, 0.5):nil;
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:strService  parameters:dictParams constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        NSError *error;
        if (isImageSelected == true) {
           [formData appendPartWithFileURL:filePath name:@"fileToUpload" fileName:strVideoName mimeType: @"application/json" error:&error];
        }
        
        
    } error:nil];
    // text/html   @"application/json"   @"image/jpeg"     @"customerImage"    @"userfile"  @"text/json"
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.responseSerializer = [AFImageResponseSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSURLSessionUploadTask *uploadTask;
    uploadTask = [manager uploadTaskWithStreamedRequest:request progress:^(NSProgress * _Nonnull uploadProgress) {
        NSLog(@"Wrote %f", uploadProgress.fractionCompleted);
    }
                                      completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                                          if (error)
                                          {
                                              NSLog(@"error  : %@ and  %@",error,error.localizedDescription);
                                              failure(error);
                                          }
                                          else
                                          {
                                              NSLog(@"POST Response  : %@",responseObject);
                                              NSError* error;
                                              NSDictionary * json = [NSJSONSerialization JSONObjectWithData:responseObject
                                                                                                    options:kNilOptions
                                                                                                      error:&error];
                                              NSLog(@"POST json Response  : %@",json);
                                              success(json);
                                          }
                                      }];
    [uploadTask resume];
}


+ (void)requestPostUrlWithThumbnailImage: (NSString *)serviceName parameters:(NSDictionary *)dictParams image:(UIImage *)image filePath:(NSString *)videofilePath success:(void (^)(NSDictionary *responce))success failure:(void (^)(NSError *error))failure
{
    UIImage *smallImage = [UIImage imageWithCGImage:image.CGImage scale:1.0 orientation:image.imageOrientation];
    NSData *imageData = UIImageJPEGRepresentation(smallImage, 1.0);
//    NSURL *filePath = [NSURL URLWithString:videofilePath];
    NSURL *filePathImage = [self storeImageAndGetUrlFromName:@"pavan12345" andformId:@"32" withData:imageData];
//    NSString *strVideoName = [self getCurrentTimeStamp];
//    strVideoName = [strVideoName stringByAppendingString:@"screenCapture.mp4"];
    // NSString *fileName = [filePath absoluteString];
    NSString *strService = [NSString stringWithFormat:@"%@",serviceName];
    //  NSData *fileData = image?UIImageJPEGRepresentation(image, 0.5):nil;
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:strService  parameters:dictParams constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        NSError *error;
//        [formData appendPartWithFileURL:filePath name:@"fileToUpload" fileName:strVideoName mimeType: @"application/json" error:&error];
        
        NSString *strThumbImageName = [self getCurrentEpochTime];
        strThumbImageName = [strThumbImageName stringByAppendingString:@"ThumbnailImage.png"];
        
        [formData appendPartWithFileURL:filePathImage name:@"fileToUpload" fileName:strThumbImageName mimeType: @"application/json" error:&error];
        
        
    } error:nil];
    // text/html   @"application/json"   @"image/jpeg"     @"customerImage"    @"userfile"  @"text/json"
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.responseSerializer = [AFImageResponseSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSURLSessionUploadTask *uploadTask;
    uploadTask = [manager uploadTaskWithStreamedRequest:request progress:^(NSProgress * _Nonnull uploadProgress) {
        NSLog(@"Wrote %f", uploadProgress.fractionCompleted);
    }
                                      completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                                          if (error)
                                          {
                                              NSLog(@"error  : %@ and  %@",error,error.localizedDescription);
                                              failure(error);
                                          }
                                          else
                                          {
                                              NSLog(@"POST Response  : %@",responseObject);
                                              NSError* error;
                                              NSDictionary * json = [NSJSONSerialization JSONObjectWithData:responseObject
                                                                                                    options:kNilOptions
                                                                                                      error:&error];
                                              NSLog(@"POST json Response  : %@",json);
                                              
                                              
                                              success(json);
                                          }
                                      }];
    [uploadTask resume];
}

+(NSURL *)storeImageAndGetUrlFromName:(NSString *)formtype andformId:(NSString *)formId withData:(NSData *)imageData
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_%@.png",formtype,formId]];
    NSLog(@"Path : %@",savedImagePath);
    [imageData writeToFile:savedImagePath atomically:NO];
    NSURL *filePath = [NSURL fileURLWithPath:savedImagePath];
    return filePath;
}

+(void)deleteImageFromDocumentDirectory:(NSString *)formtype andformId:(NSString *)formId
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_%@.png",formtype,formId]];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    [fileManager removeItemAtPath:filePath error:NULL];
}

#pragma mark Other Required methods
+(NSString *) stringByStrippingHTML : (NSString* ) str
{
    NSRange r;
    NSString *s = [str copy];
    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
        s = [s stringByReplacingCharactersInRange:r withString:@"\n"];
    s = [s stringByReplacingOccurrencesOfString:@"&#39;" withString:@"'"];
    return s;
}

+(BOOL)checkForStringValidation:(NSString*)str
{
    BOOL flag = YES;
    if (str == (id)[NSNull null] || str.length == 0 || [str isEqualToString:@""] ||[str isEqualToString:@"<null>"]||[str isEqualToString:@"(<null>)"] || str == nil ||[str isEqualToString:@"(null)"]) {
        flag = NO;
    }
    return flag;
}

+(NSString*)checkForStringValidationWithReturnSameString:(NSString*)str
{
    BOOL flag = YES;
    if (str == (id)[NSNull null] || str.length == 0 || [str isEqualToString:@""] ||[str isEqualToString:@"<null>"]||[str isEqualToString:@"(<null>)"] || str == nil ||[str isEqualToString:@"(null)"])
        NSLog(@"");
    else
        str = [self stringByStrippingHTML:str];
    
    if (str == (id)[NSNull null] || str.length == 0 || [str isEqualToString:@""] ||[str isEqualToString:@"<null>"]||[str isEqualToString:@"(<null>)"] || str == nil ||[str isEqualToString:@"(null)"]) {
        str = @"";
        flag = NO;
    }
    return str;
}

+(NSString *)getCurrentTimeStamp
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSDate *currentTime = [NSDate date];
    NSString *resultString = [dateFormat stringFromDate:currentTime];
    NSLog(@"resultString = %@",resultString);
    return resultString;
}

+(void)alertViewWithTwoButtonYesName:(NSString*)btnYes ButtonNoName:(NSString*)btnNo WithTitle:(NSString*)title AndMessage:(NSString*)message
{
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:title
                                          message:message
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(btnNo, @"NO action")
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction *action)
                                   {
                                       NSLog(@"NO action");
                                   }];
    
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(btnYes, @"YES action")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   
                               }];
    
    [alertController addAction:cancelAction];
    [alertController addAction:okAction];
    
    UIWindow *alertWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    alertWindow.rootViewController = [[UIViewController alloc] init];
    alertWindow.windowLevel = UIWindowLevelAlert + 1;
    [alertWindow makeKeyAndVisible];
    [alertWindow.rootViewController presentViewController:alertController animated:YES completion:nil];
}

+(BOOL)CheckInternetConnection{
    NewReachability *reachability = [NewReachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus != NotReachable) {
        NSLog(@"Connected");
        return YES;
    }
    else {
        NSLog(@"NOT Connected");
        return NO;
    }
}
+(NSString *)getCurrentEpochTime
{
    NSDate *date = [NSDate date];
    NSLog(@"Time: %f", floor([date timeIntervalSince1970] * 1000));
    NSLog(@"Time: %f", floor([date timeIntervalSince1970]));
    NSLog(@"Time: %lli", [@(floor([date timeIntervalSince1970] * 1000)) longLongValue]);
    NSString *strOfCurntEpochTime = [NSString stringWithFormat:@"%lli",[@(floor([date timeIntervalSince1970])) longLongValue]];
    NSLog(@"Time: %@",strOfCurntEpochTime);
    return strOfCurntEpochTime;
}
@end
