//
//  MyTextField.m
//  LabTest
//
//  Created by Magneto on 3/28/14.
//  Copyright (c) 2014 Magneto. All rights reserved.
//

#import "MyTextField.h"

@implementation MyTextField

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        // Not work
//        self.backgroundColor = lightgray;
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    

}

- (void)drawPlaceholderInRect:(CGRect)rect
{
    // set font, color, tint
   // self.textColor = textColorBrown;
    self.backgroundColor = [UIColor colorWithRed:216.0/255.0 green:216.0/255.0 blue:216.0/255.0 alpha:1.0];
    self.layer.masksToBounds = YES;
    self.layer.cornerRadius = 5;
  //  self.font = [UIFont fontWithName:@"Futura-Medium" size:15];
   // self.tintColor = textFieldTintColor;
    
    // set left right view to set space
    UILabel *viewLeftRight = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 5, self.frame.size.height)];
    self.leftViewMode = UITextFieldViewModeAlways;
    self.rightViewMode = UITextFieldViewModeAlways;
    self.leftView = viewLeftRight;
    self.rightView = viewLeftRight;
    
    // set place holder color
    UIColor *colour = [UIColor colorWithRed:172.0/255.0 green:172.0/255.0 blue:172.0/255.0 alpha:1.0];
    
    if ([self.placeholder respondsToSelector:@selector(drawInRect:withAttributes:)])
    { // iOS7 and later
        NSDictionary *attributes = @{NSForegroundColorAttributeName: colour, NSFontAttributeName: self.font};
        CGRect boundingRect = [self.placeholder boundingRectWithSize:rect.size options:0 attributes:attributes context:nil];
        [self.placeholder drawAtPoint:CGPointMake(0, (rect.size.height/2)-boundingRect.size.height/2) withAttributes:attributes];
    }
    else
    { // iOS 6
        [colour setFill];
        [self.placeholder drawInRect:rect withFont:self.font lineBreakMode:NSLineBreakByTruncatingTail alignment:self.textAlignment];
    }

}

@end
